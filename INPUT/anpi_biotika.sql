--
-- OBM database create from csv
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE anpi_biotika(
    obm_id integer NOT NULL,
    obm_geometry geometry,
    obm_datum timestamp with time zone DEFAULT now(),
    obm_uploading_id integer,
    obm_validation numeric,
    obm_comments text[],
    obm_modifier_id integer,
    obm_files_id character varying(32),
    CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
    CONSTRAINT enforce_geotype_obm_geometry CHECK (((((geometrytype(obm_geometry) = 'POINT'::text) OR (geometrytype(obm_geometry) = 'LINE'::text)) OR (geometrytype(obm_geometry) = 'POLYGON'::text)) OR (obm_geometry IS NULL))),
    CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
);

ALTER TABLE anpi_biotika OWNER TO gisadmin;

--
-- Name: TABLE anpi_biotika; Type: COMMENT; Schema: public; Owner: gisadmin
--

COMMENT ON TABLE anpi_biotika IS 'user defined table:banm';

--
-- Name: anpi_biotika_obm_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE anpi_biotika_obm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE anpi_biotika_obm_id_seq OWNER TO gisadmin;

--
-- Name: anpi_biotika_obm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE anpi_biotika_obm_id_seq OWNED BY anpi_biotika.obm_id;

--
-- Name: obm_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY anpi_biotika ALTER COLUMN obm_id SET DEFAULT nextval('anpi_biotika_obm_id_seq'::regclass);

--
-- Name: anpi_biotika_pkey; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY anpi_biotika
    ADD CONSTRAINT anpi_biotika_pkey PRIMARY KEY (obm_id);

--
-- Name: obm_uploading_id; Type: FK CONSTRAINT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY anpi_biotika
    ADD CONSTRAINT obm_uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES uploadings(id);

--
-- Name: anpi_biotika; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE anpi_biotika FROM PUBLIC;
REVOKE ALL ON TABLE anpi_biotika FROM gisadmin;
GRANT ALL ON TABLE anpi_biotika TO gisadmin;
GRANT ALL ON TABLE anpi_biotika TO anpi_biotika_admin;

--
-- Name: anpi_biotika_obm_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE anpi_biotika_obm_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE anpi_biotika_obm_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE anpi_biotika_obm_id_seq TO gisadmin;
GRANT SELECT,USAGE ON SEQUENCE anpi_biotika_obm_id_seq TO anpi_biotika_admin;

--
-- OBM add processed columns
--    

ALTER TABLE anpi_biotika ADD COLUMN "fkod" integer;
ALTER TABLE anpi_biotika ADD COLUMN "kod" integer;
ALTER TABLE anpi_biotika ADD COLUMN "felteljes_fajnev" character varying(53);
ALTER TABLE anpi_biotika ADD COLUMN "taxonkod" character varying(8);
ALTER TABLE anpi_biotika ADD COLUMN "taxoncsoport" character varying(17);
ALTER TABLE anpi_biotika ADD COLUMN "ervenyes" boolean;
ALTER TABLE anpi_biotika ADD COLUMN "erv_fkod" integer;
ALTER TABLE anpi_biotika ADD COLUMN "magyarnev" character varying(51);
