\COPY anpi_biotika (fkod,kod,felteljes_fajnev,taxonkod,taxoncsoport,ervenyes,erv_fkod,magyarnev) FROM 'anpi_biotika.csv' WITH DELIMITER ',' CSV HEADER;

\COPY bnpi_flora (ogc_fid,field_1,objectid,soo_no,taxonkod,genus,species,latin_nev,auct,nemzetsegnev,fajnev,magyar_nev,ssp,sspauct,simon_1992_genus,simon_1992_species,simon_1992_ssp,simon_1992_auct,simon_1992_sspauct,simon_2000_genus,simon_2000_species,simon_2000_ssp,simon_2000_auct,simon_2000_sspauct,tir_genus,tir_species,tir_ssp,tir_auct,tir_sspauct,fuvesz_no,fuvesz_genus,fuvesz_species,fuvesz_ssp,fuvesz_auct,fuvesz_sspauct,tv,protect,mvk,voros_lista,corine,bern,iucn,washingtoni,natura2000,tz,wz,rz,fs,ts,rs,ns,tb,wb,rb,nb,lb,kb,sb,life,vert,fle,tvk,sbt,val,familia,bnpi_fontossag,egyseg,setup) FROM 'bnpi_flora.csv' WITH DELIMITER ',' CSV HEADER;

\COPY bnpi_fauna (ogc_fid,field_1,objectid,taxonkod,genus,species,auctor,ssp,sspauct,latin_nev,fajnev_jogszabaly,staxon,staxon1,tipus,magyar_nev,gyakkod,vedkod,eszmei_ertek,nat2000_ii,nat2000_iv,nat2000_v,mvk,bonn,washington,bern,iucn,area,karpatmedence,megjegyzes) FROM 'bnpi_fauna.csv' WITH DELIMITER ',' CSV HEADER;

\COPY szitakotok (fajnev,status,elfogadott_nev) FROM 'szitakotok.csv'  WITH DELIMITER ',' CSV HEADER;


