\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_international_agreements DROP CONSTRAINT ssp_international_agreements_species_name_fkey;
--ALTER TABLE ONLY public.ssp_international_agreements DROP CONSTRAINT ssp_international_agreements_pkey;
--DROP TABLE public.ssp_international_agreements;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_international_agreements; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_international_agreements (
    species_name character varying(64) NOT NULL,
    "values" character varying(64)[] NOT NULL
);


ALTER TABLE public.ssp_international_agreements OWNER TO ssp_admin;

--
-- Data for Name: ssp_international_agreements; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_international_agreements (species_name, "values") FROM stdin;
Lathyrus pannonicus	{mvk:!}
Clematis integrifolia	{mvk:PV}
Trollius europaeus	{mvk:!}
Lathyrus pannonicus subsp. collinus	{mvk:AV}
Batrachium radians	{mvk:PV}
Pulsatilla grandis	{mvk:PV}
Ranunculus parviflorus	{mvk:PV}
Thalictrum minus	{mvk:!}
Thalictrum minus subsp. pseudominus	{mvk:PV}
Adonis vernalis	{mvk:PV}
Sorbus aria agg.	{mvk:!}
Crataegus curvisepala subsp. calycina	{mvk:PV}
Potentilla wiemanniana var. javorkae	{mvk:PV}
Potentilla pedata	{mvk:PV}
Agrimonia procera	{mvk:PV}
Rosa × reversa	{mvk:PV}
Sempervivum marmoreum	{mvk:PV}
Saxifraga granulata	{mvk:PV}
Ribes rubrum	{mvk:!}
Ribes rubrum subsp. sylvestre	{mvk:PV}
Chamaecytisus supinus	{mvk:!}
Chamaecytisus supinus subsp. pseudorochelii	{mvk:PV}
Trifolium resupinatum	{mvk:PV}
Trifolium medium	{mvk:!}
Trifolium arvense	{mvk:!}
Vitis vinifera	{mvk:AV}
Cornus sanguinea	{mvk:!}
Vicia oroboides	{mvk:PV}
Epilobium dodonaei	{mvk:PV}
Polygala major	{mvk:PV}
Polygala comosa	{mvk:!}
Angelica archangelica	{mvk:PV}
Gentiana asclepiadea	{mvk:PV}
Aster amellus	{mvk:PV}
Crepis capillaris	{mvk:PV}
Cyclamen purpurascens	{mvk:PV}
Rumex aquaticus	{mvk:PV}
Euphorbia humifusa	{mvk:PV}
Veronica peregrina	{mvk:PV}
Melampyrum nemorosum	{mvk:!}
Melampyrum pratense	{mvk:!}
Teucrium montanum	{mvk:!}
Teucrium montanum subsp. subvillosum	{mvk:PV}
Teucrium scordium	{mvk:PV}
Scutellaria columnae	{mvk:PV}
Phlomis tuberosa	{mvk:PV}
Calamintha einseleana	{mvk:K}
Mentha × carinthiaca	{mvk:PV}
Verbascum lychnitis	{mvk:!}
Veronica anagallis-aquatica	{mvk:!}
Orobanche loricata	{mvk:PV}
Cardamine pratensis	{mvk:!}
Utricularia australis	{mvk:PV}
Plantago schwarzenbergiana	{mvk:PV}
Plantago major	{mvk:!}
Plantago major subsp. winteri	{mvk:PV}
Glaucium flavum	{mvk:PV}
Isatis tinctoria subsp. praecox	{mvk:PV}
Thlaspi montanum	{mvk:KV}
Lunaria annua	{mvk:PV}
Lunaria annua subsp. pachyrrhiza	{mvk:PV}
Alyssum alyssoides	{mvk:!}
Erysimum odoratum	{mvk:!}
Viola suavis	{mvk:PV}
Ecballium elaterium	{mvk:PV}
Hypericum maculatum	{mvk:!}
Aster sedifolius	{mvk:!}
Erigeron acris	{mvk:!}
Pyrola chlorantha	{mvk:PV}
Inula helenium	{mvk:PV}
Carpesium abrotanoides	{mvk:PV}
Chrysanthemum corymbosum	{mvk:!}
Centaurea salonitana var. taurica	{mvk:PV}
Centaurea arenaria	{mvk:!}
Centaurea arenaria subsp. borysthenica	{mvk:PV}
Cerastium arvense	{mvk:!}
Minuartia verna	{mvk:!}
Minuartia verna subsp. ramosissima	{mvk:PV}
Picris hieracioides	{mvk:!}
Picris hieracioides subsp. spinulosa	{mvk:PV}
Stellaria media	{mvk:!}
Cerastium subtetrandrum	{mvk:PV}
Paronychia cephalotes	{mvk:PV}
Amaranthus paniculatus	{mvk:PV}
Potamogeton × zizii	{mvk:PV}
Allium sphaerocephalon	{mvk:!}
Allium sphaerocephalon subsp. amethystinum	{mvk:PV}
Ruscus hypoglossum	{mvk:PV}
Polygonatum verticillatum	{mvk:PV}
Juncus articulatus	{mvk:!}
Schoenus ferrugineus	{mvk:K}
Carex buekii	{mvk:PV}
Carex pilulifera	{mvk:PV}
Carex strigosa	{mvk:PV}
Bromus rigidus	{mvk:PV}
Festuca pallens	{mvk:!}
Poa pannonica subsp. scabra	{mvk:PV}
Hordeum marinum	{mvk:PV}
Gaudinia fragilis	{mvk:PV}
Stipa tirsa	{mvk:PV}
Stipa eriocaulis	{mvk:PV}
Sparganium erectum	{mvk:!}
Sparganium erectum subsp. microcarpum	{mvk:AV}
Dryopteris dilatata	{mvk:PV}
Dryopteris × tavelii	{mvk:PV}
Azolla caroliniana	{mvk:PV}
Dryopteris × sarvelii	{mvk:PV}
Aeshna juncea	{mvk:K}
Abrostola agnorista	{mvk:AV}
Acinopus ammophilus	{mvk:AV}
Acrida ungarica	{mvk:PV}
Acrotylus longipes	{mvk:AV}
Agrilus guerini	{mvk:KV}
Ampedus quadrisignatus	{mvk:K}
Anax parthenope	{mvk:AV}
Anthaxia hungarica	{mvk:AV}
Anthaxia tuerki	{mvk:KV}
Apalus necydaleus	{mvk:K}
Apamea rubrirena	{mvk:PV}
Arcyptera microptera	{mvk:AV}
Arytrura musculus	{mvk:KV}
Asteroscopus syriaca decipulae	{mvk:KV}
Batazonellus lacerticida	{mvk:PV}
Bombus argillaceus	{mvk:KV}
Bombus fragrans	{mvk:K}
Byrrhus gigas	{mvk:AV}
Calliptamus barbarus	{mvk:AV}
Callisthenes reticulatus	{mvk:K}
Camptorhinus statua	{mvk:AV}
Canis aureus moreoticus	{mvk:K}
Carabus auronitens	{mvk:AV}
Carabus auronitens kraussi	{mvk:AV}
Carabus irregularis	{mvk:AV}
Carabus irregularis cephalotes	{mvk:AV}
Carabus linnaei	{mvk:AV}
Carabus linnaei transdanubialis	{mvk:AV}
Carabus marginalis	{mvk:PV}
Carinatodorcadion fulvum	{mvk:AV}
Catopta thrips	{mvk:AV}
Protaetia aeruginosa	{mvk:AV}
Chaetopteryx rugulosa	{mvk:PV}
Charopus philoctetes	{mvk:PV}
Chesias legatella	{mvk:PV}
Agostenus sulcicollis	{mvk:K}
Chondrosoma fiduciaria	{mvk:KV}
Claviger nitidus	{mvk:KV}
Cochylimorpha obliquana	{mvk:PV}
Coenagrion hastulatum	{mvk:AV}
Coenagrion lunulatum	{mvk:K}
Coenonympha hero	{mvk:K}
Coenonympha tullia	{mvk:K}
Cordulegaster bidentata	{mvk:AV}
Cucullia formosa	{mvk:KV}
Cucullia mixta lorica	{mvk:AV}
Cymindis budensis	{mvk:K}
Dapsa fodori	{mvk:K}
Dasypoda suripes	{mvk:AV}
Dichonia aeruginea	{mvk:AV}
Dioszeghyana schmidti	{mvk:AV}
Divaena haywardi	{mvk:PV}
Dolichosoma femorale	{mvk:PV}
Drusus trifidus	{mvk:AV}
Duvalius gebhardti	{mvk:AV}
Duvalius hungaricus	{mvk:AV}
Entephria cyanata	{mvk:KV}
Epipsilia latens	{mvk:AV}
Erannis ankeraria	{mvk:AV}
Ergates faber	{mvk:AV}
Eupithecia actaeata	{mvk:AV}
Eupithecia denticulata	{mvk:AV}
Euxoa recussa	{mvk:AV}
Gasterocercus depressirostris	{mvk:AV}
Glyphipterix loricatella	{mvk:KV}
Gortyna borelii lunata	{mvk:AV}
Hadena christophi	{mvk:KV}
Herophila tristis	{mvk:K}
Herpes porcellus	{mvk:AV}
Hydraecia ultima	{mvk:PV}
Hymenalia morio	{mvk:AV}
Isophya brevipennis	{mvk:PV}
Isophya costata	{mvk:AV}
Isophya modesta	{mvk:AV}
Isophya modestior	{mvk:AV}
Isophya stysi	{mvk:AV}
Jordanita chloros	{mvk:AV}
Kisanthobia ariasi	{mvk:KV}
Lasiommata petropolitana	{mvk:AV}
Lasionycta proxima	{mvk:AV}
Leistus terminatus	{mvk:AV}
Lignyoptera fumidaria	{mvk:AV}
Limnephilus elegans	{mvk:AV}
Lioderina linearis	{mvk:AV}
Locusta migratoria	{mvk:KV}
Lycaena helle	{mvk:K}
Lygephila ludicra	{mvk:K}
Lymnastis dieneri	{mvk:K}
Macronychus quadrituberculatus	{mvk:K}
Megascolia maculata	{mvk:KV}
Megascolia maculata flavifrons	{mvk:KV}
Melampophylax nepos	{mvk:AV}
Melanargia russiae	{mvk:K}
Meloe autumnalis	{mvk:AV}
Meloe decorus	{mvk:K}
Meloe hungarus	{mvk:AV}
Mesotype didymata	{mvk:AV}
Methorasa latreillei	{mvk:AV}
Morimus asper funereus	{mvk:AV}
Myrmoecia perezi	{mvk:K}
Nehalennia speciosa	{mvk:K}
Oligotricha striata	{mvk:AV}
Oxytripia orbiculosa	{mvk:K}
Paracaloptenus caloptenoides	{mvk:AV}
Parazuphium chevrolati praepannonicum	{mvk:K}
Parnopes grandior	{mvk:AV}
Perizoma minorata	{mvk:AV}
Phlogophora scita	{mvk:PV}
Pholidoptera transsylvanica	{mvk:AV}
Phyllometra culminaria	{mvk:KV}
Platyphylax frauenfeldi	{mvk:AV}
Platyscelis hungarica	{mvk:K}
Plebeius sephirus	{mvk:KV}
Polymixis rufocincta isolata	{mvk:KV}
Polyommatus damon	{mvk:KV}
Polyommatus escheri	{mvk:K}
Porphyrophora polonica	{mvk:PV}
Probaticus subrugosus	{mvk:KV}
Protolampra sobrina	{mvk:AV}
Ptilophorus dufouri	{mvk:K}
Pyrois cinnamomea	{mvk:K}
Rhyacophila hirticornis	{mvk:AV}
Satrapes sartorii	{mvk:KV}
Scopula nemoraria	{mvk:AV}
Scotochrosta pulla	{mvk:PV}
Sesia bembeciformis	{mvk:K}
Sesia melanocephala	{mvk:K}
Somatochlora metallica	{mvk:PV}
Sphex rufocinctus	{mvk:PV}
Stenolophus steveni	{mvk:K}
Stenus kiesenwetteri	{mvk:AV}
Sterrhopterix fusca	{mvk:PV}
Stilbum cyanurum	{mvk:KV}
Sympetrum danae	{mvk:AV}
Tettigonia caudata	{mvk:AV}
Trichoferus pallidus	{mvk:KV}
Xerasia meschniggi	{mvk:K}
Zygaena brizae	{mvk:PV}
Zygaena cynarae	{mvk:AV}
Zygaena punctum	{mvk:AV}
Bufo bufo	{bern:V}
Larus cachinnans	{bern:V}
Himantopus himantopus	{mvk:KV,bonn:II,bern:FV}
Cettia cetti	{bonn:II,bern:V}
Sterna caspia	{bonn:II,bern:FV}
Charadrius hiaticula tundrae	{bonn:II,bern:FV}
Anthus pratensis	{bern:FV}
Carduelis flammea	{bern:FV}
Motacilla flava	{bern:FV}
Calandrella brachydactyla hungarica	{mvk:KV,bern:FV}
Aythya ferina	{bern:V}
Emberiza schoeniclus ukrainae	{bern:FV}
Pluvialis squatarola	{bonn:II,bern:V}
Gelochelidon nilotica	{mvk:K,bonn:II,bern:FV}
Leucaspius delineatus	{bern:V}
Lepus europaeus	{bern:V}
Anser albifrons	{bern:V}
Muscicapa striata	{bonn:II,bern:FV}
Melanocorypha calandra	{bern:FV}
Euphydryas aurinia	{bern:FV}
Dendrocopos major	{bern:FV}
Acrocephalus melanopogon	{bonn:II,bern:V}
Coturnix coturnix	{mvk:AV,bonn:II,bern:V}
Sorex alpinus	{bern:V}
Phoenicurus phoenicurus	{bonn:II,bern:FV}
Plecotus austriacus	{bonn:II,bern:FV}
Turdus iliacus	{bonn:II,bern:V}
Sylvia melanocephala	{bonn:II,bern:FV}
Larus glaucoides	{bern:V}
Triturus alpestris	{mvk:AV,bern:V}
Aegithalos caudatus	{bern:FV}
Eremophila alpestris	{bern:FV}
Turdus philomelos	{bonn:II,bern:V}
Sorex minutus	{bern:V}
Silurus glanis	{bern:V}
Rissa tridactyla	{bern:V}
Sterna albifrons	{mvk:K,bonn:II,bern:FV}
Astacus astacus	{bern:V}
Limicola falcinellus	{bonn:II,bern:FV}
Telestes souffia	{bern:V}
Luscinia luscinia	{mvk:AV,bonn:II,bern:FV}
Fratercula arctica	{bern:V}
Melanitta fusca	{bonn:II,bern:V}
Emberiza schoeniclus tschusii	{bern:FV}
Anthus trivialis	{bern:FV}
Phylloscopus collybita abietinus	{bonn:II,bern:V}
Phasianus colchicus torquatus	{bern:V}
Rana arvalis wolterstorffi	{bern:FV}
Chlidonias leucopterus	{mvk:KV,bern:FV}
Ablepharus kitaibelii fitzingeri	{mvk:KV,bern:FV}
Rana lessonae	{bern:V}
Eptesicus nilssoni	{bonn:II,bern:FV}
Emberiza schoeniclus	{bern:FV}
Stercorarius longicaudus	{bern:V}
Lymnocryptes minimus	{bonn:II,bern:V}
Turdus pilaris	{bonn:II,bern:V}
Clangula hyemalis	{bonn:II,bern:V}
Anguis fragilis	{bern:V}
Dendrocopos minor	{bern:FV}
Triturus vulgaris	{bern:V}
Haematopus ostralegus longipes	{bern:V}
Tringa ochropus	{bonn:II,bern:FV}
Porzana porzana	{bonn:II,bern:FV}
Somateria mollissima	{bonn:II,bern:V}
Cygnus olor	{bonn:II,bern:V}
Prunella modularis	{bern:FV}
Luscinia svecica	{mvk:AV,bonn:II,bern:FV}
Plectrophenax nivalis vlasowae	{bern:FV}
Limosa lapponica	{bonn:II,bern:V}
Tringa flavipes	{bonn:II,bern:V}
Thymallus thymallus	{bern:V}
Acrocephalus schoenobaenus	{bonn:II,bern:V}
Locustella naevia	{bonn:II,bern:V}
Scolopax rusticola	{bern:V}
Phylloscopus inornatus	{bonn:II,bern:V}
Oxyura jamaicensis	{bern:V}
Plegadis falcinellus	{mvk:KV,bonn:II,bern:FV}
Tachybaptus ruficollis	{bern:FV}
Monticola saxatilis	{mvk:AV,bonn:II,bern:FV}
Carpodacus erythrinus	{bern:FV}
Turdus torquatus alpestris	{bonn:II,bern:FV}
Hippolais icterina	{bonn:II,bern:V}
Erinaceus concolor roumanicus	{bern:V}
Austropotamobius torrentium	{bern:V}
Rupicapra rupicapra	{mvk:PV,bern:V}
Mergus serrator	{bonn:II,bern:V}
Serinus serinus	{bern:FV}
Leucorrhinia pectoralis	{bern:FV}
Prunella collaris	{bern:FV}
Locustella fluviatilis	{bonn:II,bern:V}
Erithacus rubecula	{bonn:II,bern:FV}
Calidris temminckii	{bonn:II,bern:FV}
Anthus campestris	{bern:FV}
Coluber caspius	{mvk:KV,bern:FV}
Emberiza calandra	{bern:V}
Anser fabalis rossicus	{bern:V}
Coregonus albula	{bern:V}
Mustela erminea	{bern:V}
Phylloscopus proregulus	{bonn:II,bern:V}
Delichon urbicum	{bern:FV}
Rana temporaria	{bern:V}
Motacilla cinerea	{bern:FV}
Botaurus stellaris	{bonn:II,bern:FV}
Parus cristatus	{mvk:AV,bern:FV}
Bombina bombina × variegata	{bern:FV}
Rhodeus amarus	{bern:V}
Emberiza hortulana	{mvk:AV,bern:V}
Parus montanus	{bern:FV}
Regulus ignicapillus	{mvk:PV,bonn:II,bern:FV}
Haematopus ostralegus	{bern:V}
Cygnus columbianus	{bonn:II,bern:FV}
Arenaria interpres	{bonn:II,bern:FV}
Lullula arborea	{bern:V}
Troglodytes troglodytes	{bern:FV}
Parus ater	{bern:FV}
Anthus spinoletta	{bern:FV}
Alburnoides bipunctatus	{bern:V}
Regulus regulus	{bonn:II,bern:FV}
Pinicola enucleator	{bern:FV}
Triturus carnifex	{bern:FV}
Coregonus lavaretus	{bern:V}
Phylloscopus trochilus acredula	{bonn:II,bern:V}
Emberiza cia	{mvk:AV,bern:FV}
Emberiza melanocephala	{bern:FV}
Eptesicus serotinus	{bonn:II,bern:FV}
Alauda arvensis dulcivox	{bern:V}
Lanius senator	{mvk:AV,bern:FV}
Ovis aries musimon	{bern:V}
Larus ichthyaetus	{bonn:II,bern:V}
Meles meles	{bern:V}
Lacerta agilis	{bern:FV}
Charadrius dubius	{bonn:II,bern:FV}
Myotis brandtii	{bonn:II,bern:FV}
Melanitta nigra	{bonn:II,bern:V}
Gavia stellata	{bonn:II,bern:FV}
Tetrao tetrix	{mvk:K,bern:V}
Ixobrychus minutus	{bonn:II,bern:FV}
Cottus poecilopus	{bern:V}
Jynx torquilla	{bern:FV}
Chettusia leucura	{bonn:II,bern:V}
Plecotus auritus	{bonn:II,bern:FV}
Parus montanus borealis	{bern:FV}
Bonasa bonasia rupestris	{mvk:KV,bern:V}
Accipiter brevipes	{mvk:PV,bonn:II,bern:FV}
Asio otus	{bern:FV}
Carduelis cannabina	{bern:FV}
Mustela nivalis vulgaris	{bern:V}
Upupa epops	{bern:FV}
Charadrius hiaticula	{bonn:II,bern:FV}
Emberiza pusilla	{bern:FV}
Rosalia alpina	{mvk:AV,bern:V}
Oenanthe hispanica	{bonn:II,bern:FV}
Picus canus	{bern:FV}
Erinaceus concolor	{bern:V}
Turdus naumanni	{bonn:II,bern:V}
Rana kl. esculenta	{bern:V}
Gallinula chloropus	{bern:V}
Remiz pendulinus	{bern:FV}
Barbus peloponnesius	{mvk:KV,bern:V}
Motacilla citreola	{bern:FV}
Apus apus	{bern:V}
Calandrella brachydactyla	{mvk:KV,bern:FV}
Chloris chloris	{bern:FV}
Sylvia communis	{bonn:II,bern:FV}
Bufo viridis	{bern:FV}
Abramis sapa	{bern:V}
Accipiter gentilis	{bonn:II,bern:FV}
Tringa glareola	{bonn:II,bern:FV}
Marmaronetta angustirostris	{bonn:I,bern:FV}
Nucifraga caryocatactes	{bern:FV}
Emberiza citrinella	{bern:FV}
Motacilla flava thunbergi	{bern:FV}
Charadrius morinellus	{bonn:II,bern:FV}
Pyrrhocorax pyrrhocorax	{bern:FV}
Phylloscopus collybita	{bonn:II,bern:V}
Oriolus oriolus	{bern:FV}
Ficedula parva	{bonn:II,bern:FV}
Pipistrellus nathusii	{bonn:II,bern:FV}
Zerynthia polyxena	{bern:FV}
Streptopelia orientalis	{bern:V}
Ciconia ciconia	{mvk:AV,bonn:II,bern:FV}
Iduna pallida	{mvk:AV,bonn:II,bern:V}
Ardea purpurea	{bonn:II,bern:FV}
Neogobius syrman	{bern:V}
Vipera berus	{bern:V}
Phasianus colchicus	{bern:V}
Dendrocopos medius	{mvk:AV,bern:FV}
Gavia immer	{bonn:II,bern:FV}
Anser fabalis	{bern:V}
Carduelis spinus	{bern:FV}
Xenus cinereus	{bonn:II,bern:FV}
Porzana pusilla	{mvk:AV,bonn:II,bern:FV}
Gavia arctica	{bonn:II,bern:FV}
Sylvia atricapilla	{bonn:II,bern:FV}
Philomachus pugnax	{mvk:PV,bonn:II,bern:V}
Loxia curvirostra	{bern:FV}
Gomphus flavipes	{mvk:AV,bern:FV}
Larus minutus	{bern:FV}
Larus canus heinei	{bern:V}
Abramis ballerus	{bern:V}
Anthus cervinus	{bern:FV}
Oenanthe deserti	{bonn:II,bern:V}
Glareola pratincola	{mvk:KV,bonn:II,bern:FV}
Alcedo atthis	{bern:FV}
Tadorna tadorna	{bonn:II,bern:FV}
Bombycilla garrulus	{bern:FV}
Picus viridis	{bern:FV}
Corvus corax	{mvk:AV,bern:V}
Calcarius lapponicus	{bern:FV}
Neomys fodiens	{bern:V}
Cuculus canorus	{bern:V}
Pyrrhula pyrrhula	{bern:V}
Cygnus cygnus	{bonn:II,bern:FV}
Merops apiaster	{mvk:AV,bonn:II,bern:FV}
Ardeola ralloides	{mvk:AV,bern:FV}
Larus hyperboreus	{bern:V}
Elaphe longissima	{bern:FV}
Gallinago gallinago	{bonn:II,bern:V}
Larus melanocephalus	{mvk:AV,bonn:II,bern:FV}
Larus michahellis	{bern:V}
Myotis mystacinus	{bonn:II,bern:FV}
Hirudo medicinalis	{bern:FV}
Lopinga achine	{bern:FV}
Chlidonias hybridus	{mvk:KV,bern:FV}
Burhinus oedicnemus	{mvk:AV,bonn:II,bern:FV}
Pluvialis apricaria	{bonn:II,bern:V}
Numenius phaeopus	{bonn:II,bern:V}
Calidris fuscicollis	{bonn:II,bern:V}
Bonasa bonasia	{mvk:KV,bern:V}
Phylloscopus trochilus	{bonn:II,bern:V}
Aythya fuligula	{bonn:II,bern:V}
Panurus biarmicus	{bern:FV}
Hirundo rustica	{bern:FV}
Saxicola torquata	{bonn:II,bern:FV}
Motacilla alba	{bern:FV}
Vanellus vanellus	{bonn:II,bern:V}
Oenanthe pleschanka	{bonn:II,bern:FV}
Lanius excubitor	{bern:FV}
Somateria spectabilis	{bonn:II,bern:FV}
Tringa erythropus	{bonn:II,bern:V}
Apus pallidus	{bern:FV}
Certhia brachydactyla	{bern:FV}
Galerida cristata	{bern:V}
Leucorrhinia caudalis	{mvk:AV,bern:FV}
Sylvia borin	{bonn:II,bern:FV}
Mergus merganser	{bonn:II,bern:V}
Ardea cinerea	{bern:V}
Myotis nattereri	{bonn:II,bern:FV}
Parus major	{bern:FV}
Tadorna ferruginea	{bonn:II,bern:FV}
Pelecanus onocrotalus	{mvk:K,bonn:I,bern:FV}
Pipistrellus kuhlii	{bonn:II,bern:FV}
Lacerta viridis	{bern:FV}
Tetrao urogallus	{mvk:K,bern:V}
Rana dalmatina	{bern:FV}
Sterna paradisaea	{bonn:II,bern:FV}
Branta canadensis	{bern:V}
Emberiza leucocephalos	{bern:FV}
Nycticorax nycticorax	{bern:FV}
Coccothraustes coccothraustes	{bern:FV}
Pterocles exustus	{bern:FV}
Carduelis hornemanni	{bern:FV}
Tringa stagnatilis	{mvk:K,bonn:II,bern:FV}
Numenius arquata	{mvk:AV,bonn:II,bern:V}
Charadrius alexandrinus	{mvk:KV,bonn:II,bern:FV}
Calidris alba	{bonn:II,bern:FV}
Anser anser	{bonn:II,bern:V}
Podarcis muralis	{bern:FV}
Acrocephalus agricola	{bonn:II,bern:V}
Podiceps grisegena	{mvk:AV,bonn:II,bern:FV}
Lanius minor	{mvk:AV,bern:FV}
Galerida cristata tenuirostris	{bern:V}
Larus sabini	{bern:FV}
Chlidonias niger	{bonn:II,bern:FV}
Calidris ferruginea	{bonn:II,bern:FV}
Podarcis taurica	{bern:FV}
Accipiter gentilis buteoides	{bonn:II,bern:FV}
Sterna hirundo	{bonn:II,bern:FV}
Tryngites subruficollis	{bonn:I,bern:V}
Lanius collurio	{bern:FV}
Hypsugo savii	{bonn:II,bern:FV}
Numenius arquata orientalis	{mvk:AV,bonn:II,bern:V}
Turdus merula	{bonn:II,bern:V}
Sylvia curruca	{bonn:II,bern:FV}
Carduelis carduelis	{bern:FV}
Dryocopus martius	{mvk:AV,bern:FV}
Certhia familiaris	{bern:FV}
Calidris minuta	{bonn:II,bern:FV}
Sorex fodiens	{bern:V}
Hoplopterus spinosus	{bonn:II,bern:FV}
Columba oenas	{bern:V}
Anthus richardi	{bern:FV}
Alca torda	{bern:V}
Sturnus roseus	{mvk:PV,bern:FV}
Vimba vimba	{bern:V}
Stercorarius pomarinus	{bern:V}
Cerambyx cerdo	{mvk:AV,bern:V}
Stercorarius parasiticus	{bern:V}
Podiceps nigricollis	{bern:FV}
Tetrax tetrax	{mvk:K,bern:FV}
Martes martes	{bern:V}
Emberiza cirlus	{mvk:AV,bern:FV}
Catharacta skua	{bern:V}
Larus pipixcan	{bern:V}
Nyctalus noctula	{bonn:II,bern:FV}
Larus canus	{bern:V}
Limosa limosa	{bonn:II,bern:V}
Riparia riparia	{bern:FV}
Phalaropus fulicarius	{bonn:II,bern:FV}
Syrrhaptes paradoxus	{bern:FV}
Montifringilla nivalis	{bern:FV}
Cobitis elongatoides	{bern:V}
Salamandra salamandra	{bern:V}
Actites hypoleucos	{bonn:II,bern:FV}
Mergellus albellus	{bonn:II,bern:V}
Phoenicurus ochruros	{bonn:II,bern:FV}
Dendrocopos leucotos	{mvk:AV,bern:FV}
Larus ridibundus	{bern:V}
Motacilla flava feldegg	{bern:FV}
Bucephala clangula	{bern:V}
Anguis colchica	{bern:V}
Fringilla montifringilla	{bern:V}
Ficedula hypoleuca	{bonn:II,bern:FV}
Coracias garrulus	{mvk:AV,bonn:II,bern:FV}
Calidris alpina schinzii	{bonn:II,bern:FV}
Myotis blythii oxygnathus	{bonn:II,bern:FV}
Apatura metis	{bern:FV}
Passer montanus	{bern:V}
Fulica atra	{bern:V}
Chettusia gregaria	{bonn:I,bern:V}
Palingenia longicauda	{bern:FV}
Recurvirostra avosetta	{mvk:AV,bonn:II,bern:FV}
Anser fabalis johanseni	{bern:V}
Parnassius mnemosyne	{bern:FV}
Fringilla coelebs	{bern:V}
Crocidura leucodon	{bern:V}
Ophiogomphus cecilia	{bern:FV}
Crocidura suaveolens	{bern:V}
Alauda arvensis cantarella	{bern:V}
Branta bernicla	{bonn:II,bern:V}
Dendrocopos syriacus	{bern:FV}
Acrocephalus palustris	{bonn:II,bern:V}
Tringa totanus	{bonn:II,bern:V}
Larus genei	{bonn:II,bern:FV}
Coronella austriaca	{bern:FV}
Acrocephalus scirpaceus	{bonn:II,bern:V}
Carduelis flavirostris	{bern:FV}
Tachymarptis melba	{bern:FV}
Bombina variegata	{bern:FV}
Emys orbicularis	{bern:FV,iucn:LR}
Aythya marila	{bonn:II,bern:V}
Podiceps cristatus	{bern:V}
Anas platyrhynchos	{bern:V}
Calidris maritima	{bonn:II,bern:FV}
Porzana parva	{bonn:II,bern:FV}
Egretta gularis	{bern:V}
Luscinia megarhynchos	{bonn:II,bern:FV}
Streptopelia decaocto	{bern:V}
Larus delawarensis	{bern:V}
Calidris alpina	{bonn:II,bern:FV}
Mustela putorius	{bern:V}
Emberiza schoeniclus stresemanni	{bern:FV}
Podiceps auritus	{bonn:II,bern:FV}
Parus caeruleus	{bern:FV}
Limnodromus scolopaceus	{bonn:II,bern:V}
Parus palustris	{bern:FV}
Caprimulgus europaeus meridionalis	{bern:FV}
Calidris canutus	{bonn:II,bern:V}
Loxia leucoptera	{bern:FV}
Tichodroma muraria	{bern:FV}
Phylloscopus bonelli	{bonn:II,bern:V}
Acrocephalus arundinaceus	{bonn:II,bern:V}
Luscinia svecica cyanecula	{mvk:AV,bonn:II,bern:FV}
Phalaropus lobatus	{bonn:II,bern:FV}
Chondrostoma nasus	{bern:V}
Vespertilio murinus	{bonn:II,bern:FV}
Porphyrio porphyrio	{bern:FV}
Anser anser rubrirostris	{bonn:II,bern:V}
Alauda arvensis	{bern:V}
Branta leucopsis	{bonn:II,bern:FV}
Martes foina	{bern:V}
Carpodacus roseus	{bern:V}
Charadrius vociferus	{bonn:II,bern:V}
Accipiter nisus	{bonn:II,bern:FV}
Cygnus bewickii	{bonn:II,bern:FV}
Nucifraga caryocatactes macrorhynchos	{bern:FV}
Calidris melanotos	{bonn:II,bern:V}
Sitta europaea	{bern:FV}
Perdix perdix	{bern:V}
Netta rufina	{bonn:II,bern:V}
Turdus viscivorus	{bonn:II,bern:V}
Saxicola rubetra	{bonn:II,bern:FV}
Proterorhinus marmoratus	{bern:V}
Emberiza schoeniclus intermedia	{bern:FV}
Plectrophenax nivalis	{bern:FV}
Pyrrhocorax graculus	{bern:FV}
Natrix tessellata	{bern:V}
Natrix natrix	{bern:V}
Cinclus cinclus	{mvk:KV,bern:FV}
Locustella luscinioides	{bonn:II,bern:V}
Ficedula albicollis	{bonn:II,bern:FV}
Rallus aquaticus	{bern:V}
Turdus torquatus	{bonn:II,bern:FV}
Phylloscopus sibilatrix	{bonn:II,bern:V}
Pelobates fuscus	{bern:FV}
Rana ridibunda	{bern:V}
Oenanthe oenanthe	{bonn:II,bern:FV}
Barbus carpathicus	{mvk:KV,bern:V}
Phylloscopus collybita tristis	{bonn:II,bern:V}
Cricetus cricetus	{bern:FV}
Charadrius leschenaultii	{bonn:II,bern:FV}
Pipistrellus pipistrellus	{bonn:II,bern:V}
Phalacrocorax carbo	{bern:V}
Parus palustris stagnatilis	{bern:FV}
Tringa nebularia	{bonn:II,bern:V}
Caprimulgus europaeus	{bern:FV}
Sterna sandvicensis	{bonn:II,bern:FV}
Galanthus nivalis	{bern:A-V}
Linum hirsutum	{bern:!}
Linum hirsutum subsp. glabrescens	{bern:!}
Pyrus nivalis	{mvk:AV,bern:J-C}
Ruscus aculeatus	{bern:A-V}
Sedum sartorianum subsp. hillebrandtii	{mvk:PV,bern:J-C}
Acipenser baeri	{washington:II}
Aegolius funereus	{bern:FV,washington:II}
Aegypius monachus	{bonn:II,bern:FV,washington:II}
Anas acuta	{mvk:AV,bonn:II,bern:V,washington:III}
Anas clypeata	{bonn:II,bern:V,washington:III}
Anas crecca	{bern:V,washington:III}
Anas penelope	{bonn:II,bern:V,washington:III}
Anas querquedula	{bern:V,washington:III}
Anas strepera	{mvk:KV,bonn:II,bern:V,washington:III}
Anthropoides virgo	{bonn:II,bern:FV,washington:II}
Aquila chrysaetos	{mvk:PV,bonn:II,bern:FV,washington:II}
Aquila nipalensis	{bonn:II,bern:FV,washington:II}
Aquila nipalensis orientalis	{bonn:II,bern:FV,washington:II}
Aquila pomarina	{mvk:KV,bonn:II,bern:FV,washington:II}
Asio flammeus	{mvk:PV,bern:FV,washington:II}
Athene noctua	{bern:FV,washington:II}
Bubo bubo	{mvk:KV,bern:FV,washington:II}
Bubo scandiaca	{bern:FV,washington:II}
Bubulcus ibis	{bern:FV,washington:III}
Buteo buteo	{bonn:II,bern:FV,washington:II}
Buteo buteo vulpinus	{bonn:II,bern:FV,washington:II}
Buteo lagopus	{bonn:II,bern:FV,washington:II}
Buteo rufinus	{bonn:II,bern:FV,washington:II}
Canis lupus	{mvk:K,bern:FV,washington:II}
Casmerodius albus	{mvk:AV,bonn:II,bern:FV,washington:III}
Ciconia nigra	{mvk:KV,bonn:II,bern:FV,washington:II}
Circaetus gallicus	{mvk:KV,bonn:II,bern:FV,washington:II}
Circus aeruginosus	{bonn:II,bern:FV,washington:II}
Circus cyaneus	{bonn:II,bern:FV,washington:II}
Circus macrourus	{bonn:II,bern:FV,washington:II}
Circus pygargus	{mvk:AV,bonn:II,bern:FV,washington:II}
Columba livia	{bern:V,washington:III}
Egretta garzetta	{mvk:AV,bern:FV,washington:III}
Falco cherrug	{mvk:KV,bonn:II,bern:FV,washington:II}
Falco cherrug cyanopus	{mvk:KV,bonn:II,bern:FV,washington:II}
Falco columbarius	{bonn:II,bern:FV,washington:II}
Falco eleonorae	{bonn:II,bern:FV,washington:II}
Falco peregrinus	{mvk:K,bonn:II,bern:FV,washington:I}
Falco peregrinus calidus	{mvk:K,bonn:II,bern:FV,washington:I}
Falco subbuteo	{bonn:II,bern:FV,washington:II}
Falco tinnunculus	{bonn:II,bern:FV,washington:II}
Falco vespertinus	{bonn:II,bern:FV,washington:II}
Felis silvestris	{mvk:AV,bern:FV,washington:II}
Glaucidium passerinum	{bern:FV,washington:II}
Grus grus	{mvk:K,bonn:II,bern:FV,washington:II}
Gyps fulvus	{bonn:II,bern:FV,washington:II}
Hieraaetus fasciatus	{bonn:II,bern:FV,washington:II}
Hieraaetus pennatus	{mvk:KV,bonn:II,bern:FV,washington:II}
Lutra lutra	{mvk:AV,bern:FV,washington:I}
Lynx lynx	{mvk:K,bern:V,washington:II}
Milvus migrans	{mvk:AV,bonn:II,bern:FV,washington:II}
Milvus milvus	{mvk:KV,bonn:II,bern:FV,washington:II}
Neophron percnopterus	{bonn:II,bern:FV,washington:II}
Otus scops	{mvk:AV,bern:FV,washington:II}
Pandion haliaetus	{mvk:K,bonn:II,bern:FV,washington:II}
Pelecanus crispus	{mvk:K,bonn:I,bern:FV,washington:I}
Pernis apivorus	{mvk:AV,bonn:II,bern:FV,washington:II}
Phoenicopterus ruber	{bonn:II,bern:FV,washington:II}
Platalea leucorodia	{mvk:AV,bonn:II,bern:FV,washington:II}
Streptopelia turtur	{bern:V,washington:III}
Strix aluco	{bern:FV,washington:II}
Strix uralensis	{mvk:AV,bern:FV,washington:II}
Surnia ulula	{bern:FV,washington:II}
Tyto alba	{mvk:AV,bern:FV,washington:II}
Tyto alba guttata	{mvk:AV,bern:FV,washington:II}
Ursus arctos	{mvk:K,bern:FV,washington:II}
Orchis × hybrida	{mvk:PV,washington:II}
Epipactis × graberi	{washington:II}
Cephalanthera damasonium	{washington:II}
Orchis militaris	{mvk:PV,washington:II}
X Orchidactyla Drudei	{mvk:PV,washington:II}
Neottia nidus-avis	{washington:II}
Orchis purpurea	{mvk:PV,washington:II}
Epipactis helleborine	{washington:II}
Orchis ustulata subsp. aestivalis	{washington:II}
Orchis × timbalii	{mvk:PV,washington:II}
Epipactis helleborine subsp. orbicularis	{mvk:AV,washington:II}
Cephalanthera × schulzei	{mvk:PV,washington:II}
Orchis × angusticruris	{mvk:PV,washington:II}
Orchis morio	{mvk:PV,washington:II}
Listera ovata	{washington:II}
Platanthera × hybrida	{mvk:PV,washington:II}
Cephalanthera rubra	{washington:II}
Platanthera bifolia	{washington:II}
Epipactis helleborine subsp. minor	{washington:II}
Dactylorhiza × aschersoniana	{washington:II}
Cephalanthera longifolia	{washington:II}
Ophrys × nelsonii	{mvk:AV,washington:II}
Epipactis helleborine agg.	{washington:II}
Orobanche cernua subsp. cumana	{iucn:DD}
Arctium tomentosum	{iucn:DD}
Silene borysthenica	{iucn:DD}
Pyrola media	{mvk:PV,iucn:DD}
Dianthus arenarius subsp. borussicus	{mvk:PV,iucn:DD}
Geum aleppicum	{mvk:PV,iucn:DD}
Scleranthus verticillatus	{iucn:DD}
Camelina sativa	{iucn:DD}
Rumex kerneri	{iucn:DD}
Campanula xylocarpa	{mvk:PV,iucn:DD}
Sagina ciliata	{iucn:DD}
Petrorhagia glumacea var. obcordata	{iucn:DD}
Verbascum densiflorum	{iucn:DD}
Cotoneaster matrensis	{iucn:DD}
Odontites vernus	{iucn:DD}
Cardamine pratensis subsp. dentata	{iucn:DD}
Brachypodium pinnatum subsp. rupestre	{iucn:DD}
Atriplex rosea	{iucn:DD}
Puccinellia peisonis	{mvk:PV,iucn:DD}
Rosa glauca	{iucn:DD}
Dianthus barbatus	{iucn:DD}
Galeopsis bifida	{iucn:DD}
Scabiosa triandra subsp. agrestis	{iucn:DD}
Cornus sanguinea subsp. hungarica	{mvk:PV,iucn:DD}
Koeleria javorkae	{mvk:AV,bern:J-C,iucn:DD}
Achillea distans subsp. stricta	{iucn:DD}
Viola tricolor	{iucn:DD}
Myosotis sylvatica	{iucn:DD}
Hieracium piloselloides	{iucn:DD}
Nicandra physalodes	{iucn:DD}
Potentilla collina	{iucn:DD}
Orobanche ramosa	{iucn:DD}
Malva pusilla	{iucn:DD}
Myosotis stenophylla	{mvk:AV,iucn:DD}
Hieracium caesium	{iucn:DD}
Rosa livescens	{iucn:DD}
Orobanche teucrii	{mvk:PV,iucn:DD}
Cynoglossum hungaricum	{iucn:DD}
Agrostis vinealis	{iucn:DD}
Taraxacum bessarabicum	{iucn:DD}
Erigeron acris subsp. macrophyllus	{mvk:PV,iucn:DD}
Podospermum laciniatum	{iucn:DD}
Sorbus × latissima	{iucn:DD}
Digitaria ischaemum	{iucn:DD}
Solanum alatum	{iucn:DD}
Rosa szaboi	{iucn:DD}
Centaurea pseudophrygia	{iucn:DD}
Helictotrichon compressum	{mvk:AV,iucn:DD}
Sorbus × tobani	{mvk:KV,iucn:CR}
Senecio aquaticus	{mvk:PV,iucn:DD}
Pulsatilla pratensis subsp. zimmermannii	{mvk:PV,iucn:DD}
Rosa micrantha	{iucn:DD}
Anthemis cotula	{iucn:DD}
Rhinanthus alectorolophus	{iucn:DD}
Rosa stylosa	{mvk:PV,iucn:DD}
Hieracium bifidum	{iucn:DD}
Diphasium tristachyum	{mvk:AV,iucn:DD}
Scleranthus polycarpos	{iucn:DD}
Haynaldia villosa	{mvk:PV,iucn:DD}
Campanula moravica	{mvk:PV,iucn:DD}
Rosa gizellae	{iucn:DD}
Eleocharis austriaca	{mvk:PV,iucn:DD}
Orobanche coerulescens subsp. occidentalis	{mvk:AV,iucn:DD}
Arenaria leptoclados	{iucn:DD}
Camelina rumelica	{iucn:DD}
Jurinea mollis subsp. dolomitica	{iucn:DD}
Pyrus nivalis subsp. salviifolia	{mvk:AV,iucn:DD}
Rosa elliptica	{iucn:DD}
Koeleria grandis	{mvk:PV,iucn:DD}
Dryopteris expansa	{mvk:AV,iucn:DD}
Festuca vaginata subsp. dominii	{iucn:DD}
Festuca pallens subsp. pannonica	{mvk:PV,iucn:DD}
Fumaria parviflora	{iucn:DD}
Festuca × wagneri	{mvk:PV,iucn:DD}
Linaria angustissima	{iucn:DD}
Myosotis caespitosa	{mvk:AV,iucn:DD}
Rhinanthus wagneri	{iucn:DD}
Rosa inodora	{iucn:DD}
Myosotis sicula	{iucn:DD}
Rosa polyacantha	{iucn:DD}
Rosa subcollina	{iucn:DD}
Rosa caesia	{iucn:DD}
Cardaminopsis petraea	{mvk:PV,iucn:DD}
Melampyrum bihariense prol. romanicum	{mvk:PV,iucn:DD}
Linum perenne	{iucn:DD}
Cardamine pratensis subsp. paludosus	{mvk:PV,iucn:DD}
Trollius europaeus subsp. tatrae	{iucn:DD}
Eruca sativa	{iucn:DD}
Crataegus curvisepala	{mvk:PV,iucn:DD}
Achillea tuzsonii	{iucn:DD}
Silene gallica	{iucn:DD}
Chrysanthemum lanceolatum	{iucn:DD}
Poa supina	{mvk:PV,iucn:DD}
Rosa hungarica	{iucn:DD}
Melilotus altissimus subsp. macrorrhizus	{iucn:DD}
Sagina micropetala	{iucn:DD}
Arabis hirsuta subsp. gerardii	{iucn:DD}
Polycnemum majus	{iucn:DD}
Trifolium pallidum	{iucn:DD}
Solanum luteum	{iucn:DD}
Rosa kmetiana	{iucn:DD}
Bromus racemosus	{iucn:DD}
Matricaria tenuifolia	{iucn:DD}
Cerastium arvense subsp. calcicola	{mvk:PV,iucn:DD}
Chenopodium murale	{iucn:DD}
Holcus mollis	{iucn:DD}
Brassica nigra	{iucn:DD}
Orobanche minor	{iucn:DD}
Callitriche cophocarpa	{iucn:DD}
Koeleria majoriflora	{mvk:AV,iucn:DD}
Hieracium lactucella	{iucn:DD}
Dianthus giganteiformis	{mvk:PV,iucn:DD}
Rosa subcanina	{iucn:DD}
Trifolium medium subsp. sarosiense	{mvk:PV,iucn:DD}
Quercus farnetto	{mvk:PV,iucn:DD}
Melittis melissophyllum	{mvk:PV,iucn:DD}
Valerianella carinata	{iucn:DD}
Eleocharis mamillata	{iucn:DD}
Diplotaxis muralis	{iucn:DD}
Rosa zagrabiensis	{iucn:DD}
Rumex pulcher	{iucn:DD}
Setaria × decipiens	{mvk:PV,iucn:DD}
Festuca ovina	{iucn:DD}
Alyssum montanum subsp. gmelinii	{iucn:DD}
Polycnemum heuffelii	{iucn:DD}
Potamogeton filiformis	{mvk:PV,iucn:DD}
Rhinanthus rumelicus	{iucn:DD}
Hieracium laevigatum	{iucn:DD}
Bromus pannonicus	{iucn:DD}
Quercus polycarpa	{iucn:DD}
Luzula pallescens	{iucn:DD}
Rosa sherardii	{iucn:DD}
Erophila praecox	{iucn:DD}
Ononis spinosiformis	{iucn:DD}
Polygala nicaeensis subsp. carniolica	{mvk:PV,iucn:DD}
Veronica scardica	{iucn:DD}
Ranunculus strigulosus	{mvk:PV,iucn:DD}
Rosa agrestis	{iucn:DD}
Rosa tomentosa	{iucn:DD}
Galium lucidum	{iucn:DD}
Sedum neglectum subsp. sopianae	{mvk:PV,iucn:DD}
Rosa obtusifolia	{iucn:DD}
Polypodium interjectum	{iucn:DD}
Jovibarba sobolifera	{iucn:DD}
Jurinea mollis subsp. macrocalathia	{mvk:PV,iucn:DD}
Euclidium syriacum	{iucn:DD}
Anthyllis vulneraria subsp. alpestris	{mvk:PV,iucn:DD}
Acipenser gueldenstaedtii	{bonn:II,washington:II,iucn:EN}
Acipenser nudiventris	{bonn:II,washington:II,iucn:CR}
Acipenser ruthenus	{bonn:II,bern:V,washington:II,iucn:VU}
Acipenser stellatus	{bonn:II,bern:V,washington:II,iucn:EN}
Acrocephalus paludicola	{mvk:AV,bonn:I,bern:V,iucn:VU}
Aeshna viridis	{mvk:AV,bern:FV,iucn:LR}
Alburnus chalcoides mento	{bern:V,iucn:DD}
Alosa pontica	{bern:V,iucn:DD}
Anser erythropus	{mvk:AV,bonn:I,bern:FV,iucn:VU}
Aquila clanga	{mvk:PV,bonn:I,bern:FV,washington:II,iucn:VU}
Aquila heliaca	{mvk:KV,bonn:I,bern:FV,washington:I,iucn:VU}
Aspius aspius	{bern:V,iucn:DD}
Aythya nyroca	{bonn:I,bern:V,washington:III,iucn:VU}
Barbastella barbastellus	{mvk:KV,bonn:II,bern:FV,iucn:VU}
Bombina bombina	{bern:FV,iucn:LR}
Branta ruficollis	{mvk:AV,bonn:I,bern:FV,washington:II,iucn:VU}
Carabus intricatus	{iucn:LR}
Carassius carassius	{iucn:LR}
Castor fiber	{mvk:K,bern:V,iucn:LR}
Coenonympha oedippus	{mvk:KV,bern:FV,iucn:NT}
Crex crex	{mvk:AV,bonn:II,bern:FV,iucn:VU}
Cucujus cinnaberinus	{bern:V,iucn:VU}
Cyprinus carpio	{iucn:DD}
Dryomys nitedula	{mvk:AV,bern:V,iucn:LR}
Dytiscus latissimus	{mvk:AV,bern:V,iucn:VU}
Eriogaster catax	{bern:FV,iucn:DD}
Eudontomyzon danfordi	{bern:V,iucn:LR}
Eudontomyzon mariae	{bern:V,iucn:DD}
Euphydryas maturna	{bern:FV,iucn:DD}
Falco naumanni	{mvk:PV,bonn:I,bern:FV,washington:II,iucn:VU}
Gallinago media	{bonn:II,bern:FV,iucn:LR}
Glareola nordmanni	{mvk:KV,bonn:II,bern:FV,iucn:LR}
Glaucopsyche alcon	{iucn:NT}
Glaucopsyche arion	{bern:FV,iucn:NT}
Glaucopsyche nausithous	{bern:FV,iucn:NT}
Glaucopsyche teleius	{bern:FV,iucn:NT}
Graphoderus bilineatus	{bern:V,iucn:VU}
Gymnocephalus baloni	{bern:V,iucn:DD}
Gymnocephalus schraetser	{bern:V,iucn:VU}
Haliaeetus albicilla	{mvk:KV,bonn:I,bern:FV,washington:I,iucn:LR}
Hucho hucho	{bern:V,iucn:EN}
Huso huso	{bonn:II,bern:V,washington:II,iucn:EN}
Hyla arborea	{bern:FV,iucn:LR}
Lucanus cervus	{mvk:AV,bern:V,iucn:VU}
Lycaena dispar rutila	{bern:FV,iucn:NT}
Micromys minutus	{iucn:LR}
Microtus oeconomus mehelyi	{mvk:KV,bern:V,iucn:LR}
Misgurnus fossilis	{bern:V,iucn:LR}
Muscardinus avellanarius	{mvk:AV,bern:V,iucn:LR}
Mus spicilegus	{iucn:LR}
Myotis bechsteinii	{mvk:KV,bonn:II,bern:FV,iucn:VU}
Myotis dasycneme	{bonn:II,bern:FV,iucn:VU}
Myotis emarginatus	{mvk:KV,bonn:II,bern:FV,iucn:VU}
Myotis myotis	{bonn:II,bern:FV,iucn:LR}
Nannospalax leucodon	{mvk:KV,iucn:VU}
Neogobius fluviatilis	{bern:V,iucn:DD}
Neogobius kessleri	{bern:V,iucn:DD}
Numenius tenuirostris	{mvk:PV,bonn:I,bern:FV,washington:I,iucn:CR}
Nyctalus lasiopterus	{bonn:II,bern:FV,iucn:LR}
Nyctalus leisleri	{mvk:AV,bonn:II,bern:FV,iucn:LR}
Odontopodisma rubripes	{iucn:VU}
Osmoderma eremita	{bern:V,iucn:VU}
Otis tarda	{mvk:KV,bonn:I,bern:FV,washington:II,iucn:VU}
Oxyura leucocephala	{mvk:K,bonn:I,bern:FV,washington:II,iucn:VU}
Parnassius apollo	{mvk:K,bern:FV,washington:II,iucn:VU}
Pelecus cultratus	{bern:V,iucn:DD}
Phalacrocorax pygmeus	{mvk:K,bonn:II,bern:FV,iucn:LR}
Proserpinus proserpina	{bern:FV,iucn:DD}
Rhinolophus euryale	{mvk:AV,bonn:II,bern:FV,iucn:VU}
Rhinolophus ferrumequinum	{bonn:II,bern:FV,iucn:LR}
Romanogobio kesslerii	{bern:V,iucn:DD}
Romanogobio uranoscopus	{bern:V,iucn:DD}
Romanogobio vladykovi	{bern:V,iucn:DD}
Rutilus virgo	{bern:V,iucn:DD}
Sabanejewia aurata	{bern:V,iucn:DD}
Sabanejewia aurata balcanica	{bern:V,iucn:DD}
Sabanejewia aurata bulgarica	{bern:V,iucn:DD}
Saga pedo	{mvk:AV,bern:FV,iucn:VU}
Sander volgensis	{bern:V,iucn:DD}
Sicista subtilis trizona	{mvk:KV,bern:FV,iucn:LR}
Spermophilus citellus	{bern:FV,iucn:VU}
Stenobothrus eurasius	{mvk:AV,iucn:VU}
Triturus cristatus	{bern:FV,iucn:LR}
Triturus dobrogicus	{bern:FV,iucn:DD}
Umbra krameri	{mvk:KV,bern:FV,iucn:VU}
Vipera ursinii rakosiensis	{mvk:KV,bern:FV,iucn:EN}
Zingel streber	{bern:V,iucn:VU}
Zingel zingel	{bern:V,iucn:VU}
Zootoca vivipara pannonica	{mvk:PV,bern:V,iucn:VU}
Hesperis sylvestris	{iucn:NT,voros_lista:PV}
Limodorum abortivum	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Festuca amethystina	{mvk:PV,iucn:EN,voros_lista:AV}
Asperula arvensis	{mvk:PV,iucn:EX,voros_lista:K}
Aruncus sylvestris	{mvk:PV,iucn:NT,voros_lista:PV}
Crocus vittatus	{iucn:EN,voros_lista:AV}
Dryopteris pseudo-mas	{mvk:PV,iucn:NT,voros_lista:PV}
Cnidium dubium	{iucn:VU,voros_lista:PV}
Sorbus × javorkae	{iucn:VU,voros_lista:PV}
Filago vulgaris subsp. lutescens	{iucn:VU,voros_lista:PV}
Trigonella gladiata	{mvk:AV,iucn:NT,voros_lista:PV}
Ribes petraeum	{mvk:PV,iucn:EX,voros_lista:K}
Sorbus incisa	{iucn:VU,voros_lista:PV}
Hippophaë rhamnoides subsp. carpatica	{mvk:KV,iucn:CR,voros_lista:KV}
Allium atropurpureum	{iucn:NT,voros_lista:PV}
Orthilia secunda	{mvk:PV,iucn:NT,voros_lista:PV}
Thladiantha dubia	{iucn:NT,voros_lista:PV}
Nardus stricta	{iucn:NT,voros_lista:PV}
Nymphoides peltata	{mvk:PV,iucn:NT,voros_lista:PV}
Dactylorhiza maculata subsp. transsylvanica	{washington:II,voros_lista:KV}
Iris graminea subsp. pseudocyperus	{mvk:PV,iucn:NT,voros_lista:PV}
Scilla paratheticum	{mvk:PV,iucn:NT,voros_lista:PV}
Juncus capitatus	{mvk:PV,iucn:CR,voros_lista:KV}
Coronilla emerus	{mvk:PV,iucn:VU,voros_lista:PV}
Orchis ustulata	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Doronicum austriacum	{mvk:AV,iucn:VU,voros_lista:PV}
Ajuga laxmannii	{mvk:AV,bern:J-C,iucn:NT,voros_lista:PV}
Bassia sedoides	{iucn:NT,voros_lista:PV}
Petasites albus	{iucn:NT,voros_lista:PV}
Chaerophyllum aureum	{mvk:PV,iucn:NT,voros_lista:PV}
Senecio rivularis	{mvk:AV,iucn:VU,voros_lista:PV}
Scopolia carniolica	{mvk:PV,iucn:VU,voros_lista:PV}
Polystichum braunii	{mvk:PV,iucn:EN,voros_lista:AV}
Rubus saxatilis	{mvk:AV,iucn:NT,voros_lista:PV}
Nymphaea alba	{iucn:NT,voros_lista:PV}
Lathyrus palustris	{iucn:NT,voros_lista:PV}
Viola stagnina	{iucn:EN,voros_lista:AV}
Dianthus plumarius subsp. regis-stephani	{bern:J-C,iucn:NT,voros_lista:PV}
Batrachium baudotii	{mvk:PV,iucn:NT,voros_lista:PV}
Sempervivum tectorum	{mvk:PV,iucn:CR,voros_lista:KV}
Pedicularis palustris	{mvk:AV,iucn:CR,voros_lista:KV}
Cuscuta epilinum	{mvk:PV,iucn:EW,voros_lista:K}
Daphne cneorum subsp. arbusculoides	{mvk:PV,iucn:EN,voros_lista:AV}
Crocus tommasinianus	{mvk:PV,iucn:EN,voros_lista:AV}
Groenlandia densa	{mvk:PV,iucn:EX,voros_lista:K}
Agrostemma githago	{iucn:NT,voros_lista:PV}
Dentaria glandulosa	{mvk:PV,iucn:NT,voros_lista:PV}
Jurinea mollis	{mvk:!,iucn:NT,voros_lista:PV}
Knautia dipsacifolia	{mvk:PV,iucn:CR,voros_lista:KV}
Bifora radians	{iucn:NT,voros_lista:PV}
Stellaria nemorum	{iucn:NT,voros_lista:PV}
Senecio umbrosus	{mvk:AV,iucn:CR,voros_lista:KV}
Bryonia dioica	{mvk:PV,iucn:NT,voros_lista:PV}
Scandix pecten-veneris	{mvk:PV,iucn:EX,voros_lista:K}
Vaccaria hispanica	{iucn:EN,voros_lista:AV}
Sorbus × pseudograeca	{iucn:NT,voros_lista:PV}
Crepis pannonica	{mvk:PV,iucn:EN,voros_lista:AV}
Lolium remotum	{iucn:EX,voros_lista:K}
Dactylorhiza incarnata var. hyphaematodes	{washington:II,iucn:NT,voros_lista:PV}
Orchis simia	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Stipa bromoides	{mvk:AV,iucn:CR,voros_lista:KV}
Carex umbrosa	{mvk:PV,iucn:VU,voros_lista:PV}
Amygdalus nana	{mvk:PV,iucn:NT,voros_lista:PV}
Aconitum anthora	{iucn:NT,voros_lista:PV}
Nasturtium officinale	{mvk:PV,iucn:EN,voros_lista:AV}
Gymnadenia conopsea subsp. densiflora	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Salix eleagnos	{iucn:EN,voros_lista:AV}
Gladiolus imbricatus	{mvk:AV,iucn:EN,voros_lista:AV}
Geranium bohemicum	{mvk:PV,iucn:EX,voros_lista:K}
Bidens cernua	{iucn:NT,voros_lista:PV}
Potentilla neumanniana	{mvk:PV,iucn:EX,voros_lista:K}
Physocaulis nodosus	{mvk:PV,iucn:NT,voros_lista:PV}
Dactylorhiza incarnata	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Primula farinosa subsp. alpigena	{mvk:KV,bern:J-B,iucn:CR,voros_lista:KV}
Melampyrum arvense	{iucn:NT,voros_lista:PV}
Schoenoplectus americanus subsp. triangularis	{mvk:PV,iucn:VU,voros_lista:PV}
Anchusa azurea	{iucn:VU,voros_lista:PV}
Anchusa ochroleuca subsp. pustulata	{mvk:PV,iucn:CR,voros_lista:KV}
Alchemilla crinita	{mvk:PV,iucn:CR,voros_lista:KV}
Ribes nigrum	{mvk:KV,iucn:NT,voros_lista:PV}
Orobanche purpurea	{iucn:NT,voros_lista:PV}
Veronica acinifolia	{iucn:EN,voros_lista:AV}
Lysimachia nemorum	{iucn:CR,voros_lista:KV}
Batrachium fluitans	{mvk:PV,iucn:EN,voros_lista:AV}
Cicuta virosa	{mvk:AV,iucn:EN,voros_lista:AV}
Allium atroviolaceum	{iucn:NT,voros_lista:PV}
Ceratocephalus testiculatus	{mvk:PV,iucn:EN,voros_lista:AV}
Hypericum elegans	{iucn:NT,voros_lista:PV}
Epilobium collinum	{iucn:NT,voros_lista:PV}
Centunculus minimus	{iucn:NT,voros_lista:PV}
Zannichellia palustris	{iucn:NT,voros_lista:PV}
Bulbocodium versicolor	{mvk:KV,bern:J-B,iucn:EN,voros_lista:AV}
Carlina acaulis	{mvk:PV,iucn:NT,voros_lista:PV}
Anchusa barrelieri	{iucn:NT,voros_lista:PV}
Aphanes microcarpa	{mvk:PV,iucn:VU,voros_lista:PV}
Bupleurum pachnospermum	{iucn:VU,voros_lista:PV}
Cirsium erisithales	{mvk:PV,iucn:VU,voros_lista:PV}
Alchemilla gracilis	{iucn:VU,voros_lista:PV}
Digitalis ferruginea	{mvk:KV,iucn:EN,voros_lista:AV}
Sorbus × barthae	{iucn:CR,voros_lista:KV}
Iris arenaria	{mvk:PV,iucn:NT,voros_lista:PV}
Carex lasiocarpa	{mvk:AV,iucn:EN,voros_lista:AV}
Carex davalliana	{iucn:VU,voros_lista:PV}
Triglochin maritimum	{iucn:NT,voros_lista:PV}
Taeniatherum asperum	{mvk:PV,iucn:EN,voros_lista:AV}
Calamagrostis stricta	{mvk:AV,iucn:EN,voros_lista:AV}
Serratula radiata	{mvk:PV,iucn:VU,voros_lista:PV}
Alnus incana	{iucn:NT,voros_lista:PV}
Lathyrus linifolius var. montanus	{mvk:PV,iucn:NT,voros_lista:PV}
Linaria × kocianovichii	{iucn:NT,voros_lista:PV}
Thlaspi kovatsii subsp. schudichii	{iucn:CR,voros_lista:KV}
Crepis nicaeensis	{iucn:VU,voros_lista:PV}
Galium parisiense	{iucn:EN,voros_lista:AV}
Parnassia palustris	{mvk:AV,iucn:VU,voros_lista:PV}
Equisetum variegatum	{mvk:AV,iucn:NT,voros_lista:PV}
Biscutella laevigata subsp. illyrica	{iucn:NT,voros_lista:PV}
Thelypteris palustris	{iucn:NT,voros_lista:PV}
Lathyrus transsilvanicus	{mvk:AV,bern:J-C,iucn:CR,voros_lista:KV}
Catabrosa aquatica	{iucn:NT,voros_lista:PV}
Asyneuma canescens subsp. salicifolium	{mvk:PV,iucn:NT,voros_lista:PV}
Sorbus × simonkaiana	{iucn:VU,voros_lista:PV}
Biscutella laevigata subsp. austriaca	{iucn:NT,voros_lista:PV}
Aira caryophyllea	{iucn:NT,voros_lista:PV}
Androsace maxima	{iucn:EN,voros_lista:AV}
Lysimachia thyrsiflora	{mvk:K,iucn:EX,voros_lista:K}
Conringia orientalis	{iucn:VU,voros_lista:PV}
Carex appropinquata	{iucn:NT,voros_lista:PV}
Juncus maritimus	{mvk:AV,iucn:VU,voros_lista:PV}
Aster sedifolius subsp. canus	{mvk:PV,iucn:NT,voros_lista:PV}
Carex paniculata	{iucn:NT,voros_lista:PV}
Iris graminea	{mvk:PV,iucn:NT,voros_lista:PV}
Helictotrichon pratense	{iucn:EN,voros_lista:AV}
Festuca altissima	{iucn:NT,voros_lista:PV}
Sorbus carpinifolia	{iucn:NT,voros_lista:PV}
Apera interrupta	{iucn:EN,voros_lista:AV}
Tofieldia calyculata	{mvk:K,iucn:EX,voros_lista:K}
Hieracium aurantiacum	{mvk:KV,iucn:CR,voros_lista:KV}
Utricularia minor	{mvk:AV,iucn:CR,voros_lista:KV}
Vitis sylvestris	{iucn:EN,voros_lista:AV}
Carex nigra	{iucn:NT,voros_lista:PV}
Alchemilla glabra	{mvk:AV,iucn:EN,voros_lista:AV}
Alchemilla hungarica	{iucn:VU,voros_lista:PV}
Dianthus armeria subsp. armeriastrum	{iucn:NT,voros_lista:PV}
Hieracium bupleuroides subsp. tatrae	{mvk:AV,iucn:CR,voros_lista:KV}
Sorbus × borosiana	{iucn:CR,voros_lista:KV}
Taxus baccata	{mvk:PV,iucn:VU,voros_lista:PV}
Polystichum lonchitis	{mvk:AV,iucn:EN,voros_lista:AV}
Scabiosa columbaria	{mvk:PV,iucn:EN,voros_lista:AV}
Centaurea stenolepis	{iucn:NT,voros_lista:PV}
Chamaecytisus hirsutus subsp. leucotrichus	{iucn:NT,voros_lista:PV}
Corydalis intermedia	{mvk:PV,iucn:NT,voros_lista:PV}
Pleurospermum austriacum	{mvk:PV,iucn:EN,voros_lista:AV}
Crataegus nigra	{mvk:AV,iucn:EN,voros_lista:AV}
Chrysanthemum serotinum	{mvk:PV,iucn:NT,voros_lista:PV}
Senecio ovirensis subsp. gaudinii	{mvk:AV,iucn:CR,voros_lista:KV}
Physospermum cornubiense	{mvk:PV,iucn:EN,voros_lista:AV}
Sorbus × andreanszkyana	{iucn:EN,voros_lista:AV}
Carex fritschii	{iucn:NT,voros_lista:PV}
Sesleria varia	{mvk:AV,bern:J-C,iucn:EN,voros_lista:AV}
Papaver argemone	{iucn:NT,voros_lista:PV}
Biscutella laevigata	{iucn:NT,voros_lista:PV}
Polystichum setiferum	{mvk:PV,iucn:NT,voros_lista:PV}
Gentianella ciliata	{iucn:NT,voros_lista:PV}
Blysmus compressus	{iucn:EN,voros_lista:AV}
Helianthemum nummularium	{iucn:NT,voros_lista:PV}
Astragalus contortuplicatus	{iucn:EN,voros_lista:AV}
Epipactis placentina	{mvk:AV,washington:II,iucn:CR,voros_lista:KV}
Sorbus × pannonica	{iucn:VU,voros_lista:PV}
Carex bohemica	{mvk:AV,iucn:EN,voros_lista:AV}
Schoenoplectus triqueter	{iucn:EN,voros_lista:AV}
Senecio paludosus	{iucn:NT,voros_lista:PV}
Alchemilla glaucescens	{mvk:PV,iucn:CR,voros_lista:KV}
Ophrys sphecodes	{mvk:AV,washington:II,iucn:NT,voros_lista:PV}
Silene dichotoma	{iucn:NT,voros_lista:PV}
Crocus albiflorus	{mvk:KV,iucn:CR,voros_lista:KV}
Anemone trifolia	{mvk:AV,bern:J-B,iucn:EN,voros_lista:AV}
Orchis pallens	{mvk:AV,washington:II,iucn:NT,voros_lista:PV}
Carex limosa	{mvk:K,iucn:EX,voros_lista:K}
Cuscuta lupuliformis	{iucn:NT,voros_lista:PV}
Trifolium strictum	{iucn:NT,voros_lista:PV}
Trifolium ornithopodioides	{mvk:PV,iucn:EN,voros_lista:AV}
Lolium temulentum	{iucn:EW,voros_lista:K}
Drosera rotundifolia	{mvk:AV,iucn:EN,voros_lista:AV}
Thesium dollineri	{mvk:!,iucn:NT,voros_lista:PV}
Tragopogon floccosus	{iucn:NT,voros_lista:PV}
Lilium martagon subsp. alpinum	{iucn:CR,voros_lista:KV}
Aster oleifolius	{mvk:AV,bern:J-C,iucn:EN,voros_lista:AV}
Alnus viridis	{mvk:PV,iucn:EN,voros_lista:AV}
Inula germanica	{iucn:NT,voros_lista:PV}
Asphodelus albus	{mvk:PV,iucn:VU,voros_lista:PV}
Minuartia viscosa	{iucn:EN,voros_lista:AV}
Cotoneaster tomentosus	{iucn:NT,voros_lista:PV}
Lotus angustissimus	{iucn:NT,voros_lista:PV}
Carex caespitosa	{mvk:PV,iucn:EN,voros_lista:AV}
Iris sibirica	{mvk:PV,iucn:NT,voros_lista:PV}
Astragalus sulcatus	{mvk:K,iucn:EN,voros_lista:AV}
Aethionema saxatile	{iucn:NT,voros_lista:PV}
Artemisia austriaca	{iucn:NT,voros_lista:PV}
Bupleurum longifolium	{iucn:EN,voros_lista:AV}
Alchemilla acutiloba	{mvk:PV,iucn:EN,voros_lista:PV}
Senecio aurantiacus	{mvk:KV,iucn:EN,voros_lista:AV}
Linaria arvensis	{mvk:K,iucn:EX,voros_lista:K}
Vicia narbonensis subsp. serratifolia	{mvk:PV,iucn:VU,voros_lista:PV}
Sagina nodosa	{iucn:EX,voros_lista:K}
Legousia speculum-veneris	{iucn:NT,voros_lista:PV}
Epipactis tallosii	{washington:II,iucn:NT,voros_lista:PV}
Salsola soda	{mvk:PV,iucn:EN,voros_lista:AV}
Galium divaricatum	{iucn:EN,voros_lista:AV}
Goodyera repens	{mvk:PV,washington:II,iucn:CR,voros_lista:KV}
Crocus heuffelianus	{mvk:PV,iucn:VU,voros_lista:PV}
Orobanche reticulata	{iucn:NT,voros_lista:PV}
Ranunculus lingua	{mvk:PV,iucn:NT,voros_lista:PV}
Gymnadenia odoratissima	{mvk:AV,washington:II,iucn:EN,voros_lista:AV}
Scabiosa columbaria subsp. pseudobanatica	{mvk:PV,iucn:EN,voros_lista:AV}
Alchemilla monticola	{iucn:NT,voros_lista:PV}
Pulmonaria angustifolia	{mvk:PV,iucn:NT,voros_lista:PV}
Rhamnus saxatilis	{mvk:PV,iucn:EN,voros_lista:AV}
Huperzia selago	{mvk:AV,iucn:CR,voros_lista:KV}
Schoenoplectus setaceus	{mvk:PV,iucn:VU,voros_lista:PV}
Spergula pentandra	{iucn:NT,voros_lista:PV}
Lunaria rediviva	{iucn:NT,voros_lista:PV}
Trifolium micranthum	{iucn:NT,voros_lista:PV}
Pinguicula vulgaris	{mvk:KV,iucn:CR,voros_lista:KV}
Geranium sylvaticum	{mvk:PV,iucn:CR,voros_lista:KV}
Armeria elongata	{mvk:AV,iucn:EN,voros_lista:AV}
Ceterach javorkaeanum	{mvk:PV,iucn:NT,voros_lista:PV}
Sesleria uliginosa	{mvk:PV,iucn:VU,voros_lista:PV}
Botrychium lunaria	{mvk:PV,iucn:NT,voros_lista:PV}
Anacamptis pyramidalis	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Sorbus × gerecseensis	{iucn:EN,voros_lista:AV}
Asplenium lepidum	{mvk:KV,iucn:CR,voros_lista:KV}
Cardamine trifolia	{mvk:PV,iucn:CR,voros_lista:KV}
Samolus valerandi	{iucn:NT,voros_lista:PV}
Vicia biennis	{iucn:EN,voros_lista:AV}
Pisum elatius	{iucn:NT,voros_lista:PV}
Eleocharis quinqueflora	{iucn:NT,voros_lista:PV}
Echium russicum	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis microphylla	{washington:II,iucn:NT,voros_lista:PV}
Montia fontana subsp. minor	{mvk:AV,iucn:NT,voros_lista:PV}
Trigonella procumbens	{iucn:NT,voros_lista:PV}
Vulpia bromoides	{iucn:VU,voros_lista:PV}
Orchis mascula subsp. signifera	{mvk:AV,washington:II,iucn:NT,voros_lista:PV}
Gagea spathacea	{mvk:PV,iucn:VU,voros_lista:PV}
Platanthera chlorantha	{washington:II,iucn:NT,voros_lista:PV}
Saxifraga paniculata	{mvk:PV,iucn:NT,voros_lista:PV}
Stachys alpina	{mvk:PV,iucn:NT,voros_lista:PV}
Calamagrostis villosa	{iucn:CR,voros_lista:KV}
Rumex pseudonatronatus	{mvk:PV,iucn:EN,voros_lista:AV}
Trifolium vesiculosum	{mvk:PV,iucn:CR,voros_lista:KV}
Calamagrostis canescens	{iucn:NT,voros_lista:PV}
Dianthus diutinus	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV}
Senecio fluviatilis	{iucn:VU,voros_lista:PV}
Arabis alpina	{mvk:PV,iucn:EN,voros_lista:AV}
Valerianella pumila	{mvk:PV,iucn:NT,voros_lista:PV}
Najas minor	{iucn:NT,voros_lista:PV}
Sorbus × gayeriana	{iucn:VU,voros_lista:PV}
Stellaria palustris	{iucn:EN,voros_lista:AV}
Gnaphalium luteo-album	{iucn:NT,voros_lista:PV}
Schoenoplectus mucronatus	{iucn:NT,voros_lista:PV}
Minuartia frutescens	{mvk:PV,iucn:NT,voros_lista:PV}
Sorbus × bakonyensis	{iucn:VU,voros_lista:PV}
Cotoneaster integerrimus	{mvk:PV,iucn:NT,voros_lista:PV}
Polygonum graminifolium	{iucn:EN,voros_lista:AV}
Pinguicula alpina	{mvk:K,iucn:EX,voros_lista:K}
Peucedanum palustre	{iucn:NT,voros_lista:PV}
Veronica pallens	{mvk:AV,iucn:CR,voros_lista:KV}
Carex rostrata	{iucn:EN,voros_lista:AV}
Valeriana tripteris subsp. austriaca	{mvk:PV,iucn:VU,voros_lista:PV}
Viola pumila	{iucn:NT,voros_lista:PV}
Glaux maritima	{mvk:PV,iucn:EN,voros_lista:AV}
Ophrys fuciflora	{mvk:KV,bern:J-C,washington:II,iucn:CR,voros_lista:KV}
Sagina subulata	{iucn:NT,voros_lista:PV}
Muscari botryoides subsp. kerneri	{mvk:AV,iucn:NT,voros_lista:PV}
Buphthalmum salicifolium	{mvk:PV,iucn:NT,voros_lista:PV}
Valerianella coronata	{iucn:NT,voros_lista:PV}
Himantoglossum caprinum	{mvk:AV,bern:J-B,washington:II,iucn:EN,voros_lista:AV}
Silene flavescens	{mvk:KV,iucn:CR,voros_lista:KV}
Althaea cannabina	{iucn:NT,voros_lista:PV}
Orobanche nana	{mvk:KV,iucn:CR,voros_lista:KV}
Allium carinatum	{iucn:NT,voros_lista:PV}
Cimicifuga europaea	{mvk:AV,iucn:CR,voros_lista:KV}
Festuca dalmatica	{mvk:AV,iucn:NT,voros_lista:PV}
Orobanche caesia	{mvk:K,iucn:CR,voros_lista:KV}
Oxytropis pilosa subsp. hungarica	{mvk:PV,iucn:NT,voros_lista:PV}
Malaxis monophyllos	{mvk:K,washington:II,iucn:EX,voros_lista:K}
Thesium bavarum	{iucn:CR,voros_lista:KV}
Urtica kioviensis	{mvk:AV,iucn:NT,voros_lista:PV}
Silaum peucedanoides	{mvk:PV,iucn:EN,voros_lista:AV}
Calamintha thymifolia	{mvk:KV,iucn:CR,voros_lista:KV}
Micropus erectus	{iucn:NT,voros_lista:PV}
Vicia lutea	{iucn:EN,voros_lista:AV}
Trifolium diffusum	{iucn:NT,voros_lista:PV}
Spiranthes spiralis	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Polygonum bistorta	{mvk:PV,iucn:NT,voros_lista:PV}
Galium austriacum	{iucn:NT,voros_lista:PV}
Coronilla coronata	{iucn:NT,voros_lista:PV}
Cheilanthes marantae	{mvk:AV,iucn:CR,voros_lista:KV}
Campanula latifolia	{mvk:KV,iucn:CR,voros_lista:KV}
Rosa scarbriuscula	{iucn:EN,voros_lista:AV}
Ranunculus nemorosus	{iucn:NT,voros_lista:PV}
Dactylorhiza incarnata var. ochrantha	{washington:II,iucn:NT,voros_lista:PV}
Aquilegia vulgaris subsp. nigricans	{mvk:PV,iucn:VU,voros_lista:PV}
Alyssum saxatile	{mvk:PV,iucn:NT,voros_lista:PV}
Geranium pratense	{iucn:NT,voros_lista:PV}
Cardaminopsis halleri	{iucn:CR,voros_lista:KV}
Potamogeton gramineus	{iucn:EN,voros_lista:AV}
Hieracium staticifolium	{mvk:AV,iucn:EX,voros_lista:K}
Woodsia alpina	{mvk:AV,iucn:EX,voros_lista:K}
Valeriana simplicifolia	{iucn:CR,voros_lista:KV}
Carex lepidocarpa	{iucn:NT,voros_lista:PV}
Dactylorhiza maculata	{mvk:PV,washington:II,iucn:EX,voros_lista:K}
Arnica montana	{mvk:KV,bern:A-V,iucn:EW,voros_lista:K}
Digitalis lanata	{mvk:KV,iucn:NT,voros_lista:PV}
Senecio paludosus subsp. lanatus	{iucn:NT,voros_lista:PV}
Vicia sylvatica	{iucn:EN,voros_lista:AV}
Hieracium caespitosum	{iucn:NT,voros_lista:PV}
Onosma arenarium	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis nordeniorum	{washington:II,iucn:NT,voros_lista:PV}
Sorbus × pseudovertesensis	{iucn:VU,voros_lista:PV}
Fritillaria meleagris	{mvk:AV,iucn:NT,voros_lista:PV}
Diphasium complanatum	{mvk:PV,iucn:VU,voros_lista:PV}
Moneses uniflora	{mvk:PV,iucn:VU,voros_lista:PV}
Lonicera nigra	{mvk:PV,iucn:CR,voros_lista:KV}
Carex hostiana	{iucn:NT,voros_lista:PV}
Rhynchospora alba	{mvk:K,iucn:EX,voros_lista:K}
Primula elatior	{mvk:PV,iucn:VU,voros_lista:PV}
Sorbus domestica	{iucn:NT,voros_lista:PV}
Epipactis muelleri	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Vaccinium vitis-idaea	{mvk:PV,iucn:VU,voros_lista:PV}
Himantoglossum adriaticum	{mvk:AV,bern:J-B,washington:II,iucn:EN,voros_lista:AV}
Salix aurita	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis latina	{washington:II,voros_lista:PV}
Carex ericetorum	{iucn:VU,voros_lista:PV}
Stratiotes aloides	{iucn:NT,voros_lista:PV}
Euphorbia angulata	{iucn:NT,voros_lista:PV}
Medicago orbicularis	{mvk:AV,iucn:CR,voros_lista:KV}
Matteuccia struthiopteris	{mvk:AV,iucn:VU,voros_lista:PV}
Eriophorum vaginatum	{mvk:AV,iucn:CR,voros_lista:KV}
Sorbus × balatonica	{iucn:VU,voros_lista:PV}
Corallorhiza trifida	{mvk:PV,washington:II,iucn:EN,voros_lista:AV}
Coronilla vaginalis	{mvk:PV,iucn:NT,voros_lista:PV}
Dactylorhiza incarnata subsp. serotina	{washington:II,iucn:NT,voros_lista:PV}
Lamium orvala	{mvk:AV,bern:J-C,iucn:EN,voros_lista:AV}
Epipactis purpurata var. rosea	{washington:II,iucn:NT,voros_lista:PV}
Potamogeton trichoides	{mvk:PV,iucn:NT,voros_lista:PV}
Galium pumilum	{iucn:NT,voros_lista:PV}
Helminthia echioides	{iucn:NT,voros_lista:PV}
Sorbus × pseudobakonyensis	{iucn:EN,voros_lista:AV}
Antennaria dioica	{iucn:NT,voros_lista:PV}
Scorzonera humilis	{iucn:NT,voros_lista:PV}
Globularia cordifolia	{mvk:KV,iucn:CR,voros_lista:KV}
Aconitum variegatum subsp. gracilis	{mvk:PV,iucn:VU,voros_lista:PV}
Centaurea mollis	{mvk:PV,iucn:EN,voros_lista:AV}
Osmunda regalis	{mvk:AV,bern:J-C,iucn:CR,voros_lista:KV}
Comarum palustre	{mvk:AV,iucn:EN,voros_lista:AV}
Lycopsis arvensis	{mvk:PV,iucn:VU,voros_lista:PV}
Daphne mezereum	{iucn:NT,voros_lista:PV}
Dryopteris cristata	{mvk:KV,iucn:CR,voros_lista:KV}
Thlaspi coerulescens	{mvk:AV,iucn:CR,voros_lista:KV}
Asperula taurina subsp. leucanthera	{mvk:PV,iucn:NT,voros_lista:PV}
Carex transsylvanica	{iucn:EN,voros_lista:AV}
Dianthus plumarius subsp. praecox	{mvk:KV,bern:J-C,iucn:CR,voros_lista:KV}
Rumex confertus	{iucn:NT,voros_lista:PV}
Chlorocyperus glaber	{mvk:PV,iucn:EN,voros_lista:AV}
Salix pentandra	{mvk:AV,iucn:CR,voros_lista:KV}
Cuscuta australis	{iucn:VU,voros_lista:PV}
Orchis tridentata	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Sorbus × buekkensis	{iucn:VU,voros_lista:PV}
Angelica palustris	{mvk:AV,bern:A-II,iucn:EN,voros_lista:AV}
Carex hordeistichos	{iucn:NT,voros_lista:PV}
Acorus calamus	{iucn:NT,voros_lista:PV}
Sedum caespitosum	{mvk:PV,iucn:NT,voros_lista:PV}
Erysimum pallidiflorum	{mvk:AV,bern:J-B,iucn:EN,voros_lista:AV}
Rosa pendulina	{mvk:PV,iucn:NT,voros_lista:PV}
Knautia kitaibelii subsp. tomentella	{mvk:PV,bern:J-A,iucn:EN,voros_lista:AV}
Hydrocotyle vulgaris	{mvk:AV,iucn:EN,voros_lista:AV}
Dianthus superbus	{mvk:AV,iucn:NT,voros_lista:PV}
Daphne laureola	{mvk:PV,iucn:NT,voros_lista:PV}
Plantago argentea	{mvk:PV,iucn:NT,voros_lista:PV}
Arenaria procera subsp. glabra	{iucn:NT,voros_lista:PV}
Carthamus lanatus	{iucn:NT,voros_lista:PV}
Sternbergia colchiciflora	{mvk:AV,iucn:NT,voros_lista:PV}
Carex diandra	{mvk:AV,iucn:CR,voros_lista:KV}
Sorbus × redliana	{iucn:VU,voros_lista:PV}
Hypericum barbatum	{mvk:PV,iucn:CR,voros_lista:KV}
Juncus tenageia	{mvk:PV,iucn:EN,voros_lista:AV}
Cirsium furiens	{mvk:PV,iucn:EN,voros_lista:AV}
Drosera anglica	{mvk:K,iucn:EX,voros_lista:K}
Verbena supina	{iucn:NT,voros_lista:PV}
Aegilops cylindrica	{iucn:NT,voros_lista:PV}
Gentianella austriaca	{iucn:EN,voros_lista:AV}
Lathyrus aphaca	{iucn:NT,voros_lista:PV}
Astragalus varius	{mvk:PV,iucn:VU,voros_lista:PV}
Orobanche alsatica	{mvk:PV,iucn:NT,voros_lista:PV}
Asplenium adiantum-nigrum	{iucn:NT,voros_lista:PV}
Chamaecytisus heuffelii	{mvk:AV,iucn:CR,voros_lista:KV}
Epilobium palustre	{iucn:EN,voros_lista:AV}
Juncus bulbosus	{mvk:PV,iucn:EX,voros_lista:K}
Althaea hirsuta	{iucn:VU,voros_lista:PV}
Thlaspi goesingense	{mvk:AV,iucn:CR,voros_lista:KV}
Polygala amarella subsp. austriaca	{iucn:NT,voros_lista:PV}
Eranthis hyemalis	{iucn:NT,voros_lista:PV}
Polycnemum verrucosum	{iucn:EN,voros_lista:AV}
Oreopteris limbosperma	{mvk:PV,iucn:NT,voros_lista:PV}
Taraxacum serotinum	{iucn:NT,voros_lista:PV}
Eriophorum angustifolium	{mvk:PV,iucn:NT,voros_lista:PV}
Hesperis matronalis subsp. spontanea	{iucn:NT,voros_lista:PV}
Sorbus × adamii	{iucn:CR,voros_lista:KV}
Asplenium viride	{mvk:PV,iucn:EN,voros_lista:AV}
Astrantia major	{iucn:NT,voros_lista:PV}
Viscum album subsp. abietis	{iucn:NT,voros_lista:PV}
Equisetum sylvaticum	{iucn:VU,voros_lista:PV}
Potamogeton obtusifolius	{mvk:PV,iucn:EX,voros_lista:K}
Sparganium emersum	{iucn:VU,voros_lista:PV}
Misopates orontium	{iucn:NT,voros_lista:PV}
Sorbus × decipientiformis	{iucn:EN,voros_lista:AV}
Ophioglossum vulgatum	{mvk:PV,iucn:NT,voros_lista:PV}
Selaginella helvetica	{mvk:PV,iucn:EN,voros_lista:AV}
Pyrola minor	{iucn:NT,voros_lista:PV}
Dactylorhiza majalis	{mvk:AV,washington:II,iucn:NT,voros_lista:PV}
Gymnocarpium robertianum	{iucn:NT,voros_lista:PV}
Epipactis purpurata	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Iris spuria	{mvk:AV,iucn:NT,voros_lista:PV}
Elatine hydropiper	{mvk:PV,iucn:EN,voros_lista:AV}
Teucrium scorodonia	{iucn:EN,voros_lista:AV}
Leontodon incanus	{mvk:PV,iucn:NT,voros_lista:PV}
Agropyron elongatum	{iucn:NT,voros_lista:PV}
Trifolium subterraneum var. brachycladum	{mvk:AV,iucn:EN,voros_lista:AV}
Crepis polymorpha	{iucn:EN,voros_lista:AV}
Orchis laxiflora subsp. elegans	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Epipactis pontica	{washington:II,iucn:NT,voros_lista:PV}
Gagea bohemica	{iucn:NT,voros_lista:PV}
Dactylorhiza ochroleuca	{washington:II,iucn:CR,voros_lista:KV}
Scilla autumnalis	{mvk:PV,iucn:NT,voros_lista:PV}
Trigonella monspeliaca	{iucn:NT,voros_lista:PV}
Hierochloe australis	{iucn:NT,voros_lista:PV}
Thesium dollineri subsp. simplex	{mvk:PV,iucn:NT,voros_lista:PV}
Utricularia bremii	{mvk:AV,iucn:CR,voros_lista:KV}
Veronica opaca	{iucn:CR,voros_lista:KV}
Ludwigia palustris	{mvk:AV,iucn:VU,voros_lista:PV}
Epipactis voethii	{washington:II,iucn:NT,voros_lista:PV}
Lycopodium annotinum	{iucn:EN,voros_lista:AV}
Draba lasiocarpa	{mvk:PV,iucn:NT,voros_lista:PV}
Biscutella laevigata subsp. kerneri	{iucn:NT,voros_lista:PV}
Scilla spetana	{iucn:CR,voros_lista:KV}
Ceterach officinarum	{mvk:PV,iucn:VU,voros_lista:PV}
Pyramidula tetragona	{iucn:EN,voros_lista:AV}
Chlorocyperus longus	{mvk:PV,iucn:EN,voros_lista:AV}
Muscari botryoides	{mvk:PV,iucn:NT,voros_lista:PV}
Coronilla elegans	{mvk:PV,iucn:EN,voros_lista:AV}
Ribes alpinum	{mvk:AV,iucn:VU,voros_lista:PV}
Juncus subnodulosus	{iucn:NT,voros_lista:PV}
Thlaspi alliaceum	{iucn:VU,voros_lista:PV}
Puccinellia pannonica	{mvk:K,iucn:EX,voros_lista:K}
Hypericum maculatum subsp. obtusiusculum	{mvk:PV,iucn:NT,voros_lista:PV}
Erodium ciconium	{iucn:NT,voros_lista:PV}
Narcissus angustifolius	{mvk:AV,bern:J-B,iucn:EN,voros_lista:AV}
Artemisia scoparia	{iucn:NT,voros_lista:PV}
Dianthus collinus subsp. glabriusculus	{iucn:NT,voros_lista:PV}
Verbascum pulverulentum	{iucn:NT,voros_lista:PV}
Blackstonia acuminata	{iucn:NT,voros_lista:PV}
Phleum paniculatum	{iucn:NT,voros_lista:PV}
Carex disticha	{iucn:NT,voros_lista:PV}
Ornithogalum degenianum	{iucn:VU,voros_lista:PV}
Menyanthes trifoliata	{mvk:AV,iucn:EN,voros_lista:AV}
Conringia austriaca	{iucn:VU,voros_lista:PV}
Sorbus × pseudolatifolia	{iucn:EN,voros_lista:AV}
Traunsteinera globosa	{mvk:KV,washington:II,iucn:EN,voros_lista:AV}
Juncus atratus	{iucn:NT,voros_lista:PV}
Anemone sylvestris	{mvk:PV,iucn:NT,voros_lista:PV}
Kochia prostrata	{iucn:NT,voros_lista:PV}
Ranunculus psilostachys	{iucn:EN,voros_lista:AV}
Phyllitis scolopendrium	{iucn:NT,voros_lista:PV}
Vaccinium oxycoccos	{mvk:KV,iucn:CR,voros_lista:KV}
Dactylorhiza lapponica	{washington:II,iucn:CR,voros_lista:KV}
Epipactis atrorubens	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Anthriscus nitida	{mvk:PV,iucn:NT,voros_lista:PV}
Glyceria declinata	{mvk:PV,iucn:NT,voros_lista:PV}
Amelanchier ovalis	{mvk:PV,iucn:VU,voros_lista:PV}
Batrachium peltatum	{iucn:CR,voros_lista:KV}
Peucedanum arenarium	{iucn:NT,voros_lista:PV}
Filago minima	{iucn:NT,voros_lista:PV}
Orobanche picridis	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis palustris	{mvk:AV,washington:II,iucn:NT,voros_lista:PV}
Succisella inflexa	{iucn:NT,voros_lista:PV}
Hippuris vulgaris	{iucn:VU,voros_lista:PV}
Dracocephalum ruyschiana	{mvk:KV,bern:J-B,iucn:CR,voros_lista:KV}
Ophrys holubyana	{washington:II,iucn:CR,voros_lista:KV}
Sorbus × vertesensis	{iucn:VU,voros_lista:PV}
Helichrysum arenarium	{iucn:NT,voros_lista:PV}
Calamagrostis varia	{mvk:PV,iucn:NT,voros_lista:PV}
Geranium palustre	{iucn:NT,voros_lista:PV}
Trinia ramosissima	{iucn:VU,voros_lista:PV}
Inula spiraeifolia	{mvk:PV,iucn:NT,voros_lista:PV}
Neslea paniculata	{iucn:NT,voros_lista:PV}
Sorbus × eugenii-kelleri	{iucn:VU,voros_lista:PV}
Phyteuma spicatum	{mvk:PV,iucn:NT,voros_lista:PV}
Silene nemoralis	{mvk:PV,iucn:NT,voros_lista:PV}
Daphne cneorum	{mvk:PV,iucn:VU,voros_lista:PV}
Lappula patula	{mvk:PV,iucn:EN,voros_lista:AV}
Circaea alpina	{iucn:EN,voros_lista:AV}
Hemerocallis lilio-asphodelus	{mvk:AV,bern:J-B,iucn:EN,voros_lista:AV}
Alchemilla subcrenata	{iucn:VU,voros_lista:PV}
Armoracia macrocarpa	{mvk:PV,bern:J-A,iucn:NT,voros_lista:PV}
Erysimum crepidifolium	{mvk:PV,iucn:VU,voros_lista:PV}
Lathyrus pallescens	{mvk:PV,iucn:CR,voros_lista:KV}
Alisma gramineum	{iucn:NT,voros_lista:PV}
Dianthus collinus	{iucn:NT,voros_lista:PV}
Veronica paniculata	{mvk:PV,iucn:VU,voros_lista:PV}
Primula auricula subsp. hungarica	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV}
Veronica agrestis	{mvk:PV,iucn:CR,voros_lista:KV}
Crepis paludosa	{iucn:NT,voros_lista:PV}
Allium paniculatum subsp. marginatum	{iucn:NT,voros_lista:PV}
Sesleria heuflerana	{mvk:PV,iucn:VU,voros_lista:PV}
Euphorbia villosa	{iucn:NT,voros_lista:PV}
Elatine triandra	{iucn:NT,voros_lista:PV}
Calla palustris	{mvk:K,iucn:EX,voros_lista:K}
Pycreus flavescens	{iucn:NT,voros_lista:PV}
Echinops ruthenicus	{mvk:PV,iucn:NT,voros_lista:PV}
Onosma visianii	{mvk:PV,iucn:NT,voros_lista:PV}
Equisetum hyemale	{mvk:PV,iucn:NT,voros_lista:PV}
Ceratoides latens	{mvk:K,iucn:EX,voros_lista:K}
Omphalodes scorpioides	{iucn:NT,voros_lista:PV}
Dactylorhiza incarnata subsp. haematodes	{washington:II,iucn:NT,voros_lista:PV}
Scirpus radicans	{mvk:PV,iucn:VU,voros_lista:PV}
Ostrya carpinifolia	{mvk:K,iucn:EX,voros_lista:K}
Hesperis matronalis subsp. candida	{iucn:NT,voros_lista:PV}
Chenopodium botryoides	{iucn:NT,voros_lista:PV}
Anogramma leptophylla	{iucn:CR,voros_lista:KV}
Allium victorialis	{mvk:PV,iucn:EN,voros_lista:AV}
Orobanche flava	{mvk:PV,iucn:VU,voros_lista:PV}
Sedum hispanicum	{iucn:NT,voros_lista:PV}
Achillea crithmifolia	{mvk:PV,iucn:NT,voros_lista:PV}
Viola collina	{mvk:AV,iucn:NT,voros_lista:PV}
Centaurea calcitrapa	{iucn:EN,voros_lista:AV}
Aira elegantissima	{iucn:NT,voros_lista:PV}
Carex echinata	{mvk:PV,iucn:EN,voros_lista:AV}
Sorbus × semiincisa	{iucn:VU,voros_lista:PV}
Galium rivale	{iucn:NT,voros_lista:PV}
Juncus sphaerocarpus	{mvk:PV,iucn:NT,voros_lista:PV}
Chimaphila umbellata	{mvk:AV,iucn:EN,voros_lista:AV}
Lycopodium clavatum	{iucn:NT,voros_lista:PV}
Spiraea salicifolia	{mvk:AV,iucn:EN,voros_lista:AV}
Doronicum orientale	{mvk:AV,iucn:VU,voros_lista:PV}
Sambucus racemosa	{iucn:NT,voros_lista:PV}
Reseda inodora	{iucn:EN,voros_lista:AV}
Sorbus aria	{iucn:NT,voros_lista:PV}
Epipactis bugacensis	{washington:II,iucn:NT,voros_lista:PV}
Carex buxbaumii	{iucn:EN,voros_lista:AV}
Thalictrum foetidum	{mvk:AV,iucn:CR,voros_lista:KV}
Calamagrostis purpurea	{iucn:CR,voros_lista:KV}
Myosotis discolor	{mvk:PV,iucn:NT,voros_lista:PV}
Pyrus × austriaca	{mvk:PV,iucn:VU,voros_lista:PV}
Astragalus asper	{mvk:PV,iucn:NT,voros_lista:PV}
Linum trigynum	{iucn:CR,voros_lista:KV}
Aremonia agrimonioides	{mvk:PV,iucn:NT,voros_lista:PV}
Sorbus × degenii	{iucn:VU,voros_lista:PV}
Leucojum aestivum	{mvk:PV,iucn:NT,voros_lista:PV}
Orobanche arenaria	{iucn:NT,voros_lista:PV}
Hierochloe repens	{iucn:NT,voros_lista:PV}
Caucalis latifolia	{iucn:CR,voros_lista:KV}
Poa remota	{mvk:PV,iucn:EN,voros_lista:AV}
Potamogeton acutifolius	{iucn:VU,voros_lista:PV}
Hottonia palustris	{mvk:PV,iucn:NT,voros_lista:PV}
Ranunculus lateriflorus	{iucn:NT,voros_lista:PV}
Euphorbia verrucosa	{mvk:PV,iucn:EN,voros_lista:AV}
× Asplenoceterach badense	{mvk:KV,voros_lista:KV}
Eriophorum latifolium	{mvk:PV,iucn:NT,voros_lista:PV}
Trollius europaeus subsp. demissorum	{mvk:AV,iucn:EN,voros_lista:AV}
Linum flavum	{mvk:PV,iucn:NT,voros_lista:PV}
Erythronium dens-canis	{mvk:AV,iucn:VU,voros_lista:PV}
Glyceria nemoralis	{mvk:PV,iucn:NT,voros_lista:PV}
Moehringia muscosa	{mvk:PV,iucn:NT,voros_lista:PV}
Aconitum moldavicum	{mvk:PV,iucn:NT,voros_lista:PV}
Ophrys apifera	{mvk:KV,bern:J-C,washington:II,iucn:VU,voros_lista:PV}
Sagina sabuletorum	{mvk:PV,iucn:VU,voros_lista:PV}
Centaurea solstitialis	{mvk:PV,iucn:NT,voros_lista:PV}
Gymnadenia conopsea	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Laserpitium prutenicum	{iucn:NT,voros_lista:PV}
Torilis ucranica	{iucn:NT,voros_lista:PV}
Sorbus graeca	{iucn:NT,voros_lista:PV}
Teesdalia nudicaulis	{mvk:PV,iucn:VU,voros_lista:PV}
Asperula orientalis	{mvk:PV,iucn:EX,voros_lista:K}
Galium tricornutum	{iucn:VU,voros_lista:PV}
Colchicum hungaricum	{mvk:AV,bern:J-B,iucn:EN,voros_lista:AV}
Dactylorhiza fuchsii subsp. sooiana	{mvk:PV,washington:II,iucn:VU,voros_lista:PV}
Zannichellia palustris subsp. polycarpa	{iucn:NT,voros_lista:PV}
Teucrium botrys	{iucn:VU,voros_lista:PV}
Cirsium boujarti	{mvk:K,iucn:EX,voros_lista:PV}
Sorbus × karpatii	{iucn:EN,voros_lista:AV}
Lathyrus pisiformis	{mvk:PV,iucn:EN,voros_lista:AV}
Potentilla rupestris	{iucn:NT,voros_lista:PV}
Scleranthus perennis	{mvk:PV,iucn:CR,voros_lista:KV}
Carex brevicollis	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis leptochila subsp. neglecta	{washington:II,iucn:NT,voros_lista:PV}
Allium rotundum subsp. waldsteinii	{iucn:NT,voros_lista:PV}
Verbascum speciosum	{iucn:NT,voros_lista:PV}
Lepidium graminifolium	{iucn:EN,voros_lista:AV}
Epipactis leptochila	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Gentianella livonica	{mvk:PV,iucn:CR,voros_lista:KV}
Fumaria rostellata	{iucn:NT,voros_lista:PV}
Potentilla patula	{iucn:NT,voros_lista:PV}
Isatis tinctoria	{mvk:PV,iucn:NT,voros_lista:PV}
Juncus alpinus	{iucn:EN,voros_lista:AV}
Trichophorum alpinum	{mvk:K,iucn:EX,voros_lista:K}
Muscari tenuiflorum	{iucn:NT,voros_lista:PV}
Achillea ptarmica	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis futakii	{mvk:AV,washington:II,voros_lista:AV}
Heliotropium supinum	{mvk:PV,iucn:NT,voros_lista:PV}
Stellaria uliginosa	{iucn:NT,voros_lista:PV}
Peucedanum officinale	{iucn:NT,voros_lista:PV}
Saxifraga adscendens	{mvk:AV,iucn:CR,voros_lista:KV}
Salix nigricans	{mvk:K,iucn:CR,voros_lista:KV}
Cirsium × candolleanum	{mvk:PV,voros_lista:PV}
Cardamine flexuosa	{iucn:NT,voros_lista:PV}
Papaver hybridum	{iucn:VU,voros_lista:PV}
Silaum silaus	{iucn:NT,voros_lista:PV}
Alchemilla xanthochlora	{mvk:PV,iucn:EN,voros_lista:AV}
Ophrys scolopax subsp. cornuta	{mvk:KV,bern:J-C,washington:II,iucn:EN,voros_lista:AV}
Silene longiflora	{mvk:PV,iucn:NT,voros_lista:PV}
Woodsia ilvensis	{mvk:PV,iucn:EN,voros_lista:AV}
Potamogeton coloratus	{iucn:CR,voros_lista:KV}
Acorellus pannonicus	{iucn:NT,voros_lista:PV}
Oenanthe banatica	{iucn:VU,voros_lista:PV}
Senecio ovirensis	{mvk:AV,iucn:CR,voros_lista:KV}
Valeriana excelsa	{mvk:AV,iucn:EN,voros_lista:AV}
Medicago arabica	{mvk:PV,iucn:NT,voros_lista:PV}
Schoenoplectus litoralis	{mvk:PV,iucn:EN,voros_lista:AV}
Carpinus orientalis	{mvk:PV,iucn:EN,voros_lista:AV}
Medicago rigidula	{mvk:AV,iucn:VU,voros_lista:PV}
Eragrostis megastachya	{iucn:NT,voros_lista:PV}
Sorbus × pseudosemiincisa	{iucn:EN,voros_lista:AV}
Chenopodium bonus-henricus	{iucn:NT,voros_lista:PV}
Allium moschatum	{iucn:NT,voros_lista:PV}
Galium tenuissimum	{mvk:PV,iucn:EN,voros_lista:AV}
Bromus secalinus	{mvk:!,iucn:EN,voros_lista:AV}
Asyneuma canescens	{mvk:PV,iucn:NT,voros_lista:PV}
Erodium neilreichii	{mvk:PV,iucn:EN,voros_lista:AV}
Epilobium obscurum	{iucn:VU,voros_lista:PV}
Coronopus squamatus	{iucn:NT,voros_lista:PV}
Clematis alpina	{mvk:AV,iucn:EN,voros_lista:AV}
Phegopteris connectilis	{mvk:PV,iucn:NT,voros_lista:PV}
Epipactis albensis	{washington:II,iucn:EN,voros_lista:AV}
Erysimum hieracifolium	{mvk:PV,iucn:VU,voros_lista:PV}
Vincetoxicum pannonicum	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV}
Carex elongata	{iucn:NT,voros_lista:PV}
Glaucium corniculatum	{mvk:PV,iucn:VU,voros_lista:PV}
Sorbus × subdanubialis	{iucn:CR,voros_lista:KV}
Suaeda prostrata subsp. prostrata	{iucn:NT,voros_lista:PV}
Sorbus × danubialis	{iucn:NT,voros_lista:PV}
Peucedanum verticillare	{mvk:AV,iucn:VU,voros_lista:PV}
Ephedra distachya	{mvk:PV,bern:J-C,iucn:NT,voros_lista:PV}
Epilobium lanceolatum	{iucn:NT,voros_lista:PV}
Ornithogalum comosum	{iucn:NT,voros_lista:PV}
Epipactis atrorubens subsp. borbasii	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Sorbus aucuparia	{iucn:NT,voros_lista:PV}
Eleocharis ovata	{iucn:NT,voros_lista:PV}
Muscari botryoides subsp. transsilvanicum	{mvk:PV,iucn:NT,voros_lista:PV}
Ophrys insectifera	{mvk:AV,bern:J-B,washington:II,iucn:EN,voros_lista:AV}
Bupleurum rotundifolium	{iucn:NT,voros_lista:PV}
Blechnum spicant	{mvk:AV,iucn:CR,voros_lista:KV}
Dactylorhiza sambucina	{mvk:PV,washington:II,iucn:NT,voros_lista:PV}
Zannichellia palustris subsp. pedicellata	{iucn:NT,voros_lista:PV}
Chaerophyllum hirsutum	{mvk:PV,iucn:CR,voros_lista:KV}
Sparganium minimum	{mvk:AV,iucn:CR,voros_lista:KV}
Biscutella laevigata subsp. hungarica	{iucn:NT,voros_lista:PV}
Doronicum hungaricum	{mvk:PV,iucn:NT,voros_lista:PV}
Peltaria perennis	{mvk:PV,iucn:EN,voros_lista:AV}
Gentiana pneumonanthe	{mvk:PV,iucn:NT,voros_lista:PV}
Wolffia arrhiza	{iucn:NT,voros_lista:PV}
Polygala amarella	{iucn:NT,voros_lista:PV}
Lathyrus sphaericus	{iucn:NT,voros_lista:PV}
Rosa sancti-andreae	{mvk:AV,iucn:CR,voros_lista:KV}
Plantago altissima	{iucn:NT,voros_lista:PV}
Gentiana cruciata	{mvk:PV,iucn:NT,voros_lista:PV}
Carex hartmannii	{mvk:KV,iucn:VU,voros_lista:PV}
Gladiolus palustris	{mvk:KV,bern:J-B,iucn:EN,voros_lista:AV}
Potentilla pusilla	{mvk:PV,iucn:VU,voros_lista:PV}
Orobanche hederae	{mvk:PV,iucn:VU,voros_lista:PV}
Calamagrostis pseudophragmites	{iucn:EN,voros_lista:AV}
Carduus hamulosus	{iucn:NT,voros_lista:PV}
Nepeta parviflora	{iucn:EN,voros_lista:AV}
Chamaecytisus ciliatus	{mvk:PV,iucn:VU,voros_lista:PV}
Geum rivale	{iucn:CR,voros_lista:KV}
Herniaria incana	{mvk:PV,iucn:EN,voros_lista:AV}
Dentaria trifolia	{mvk:AV,iucn:CR,voros_lista:KV}
Sium sisaroideum	{mvk:PV,iucn:CR,voros_lista:KV}
Asplenium fontanum	{mvk:K,iucn:CR,voros_lista:KV}
Sesleria sadlerana	{mvk:PV,bern:J-B,iucn:NT,voros_lista:PV}
Salvia nutans	{mvk:KV,iucn:CR,voros_lista:KV}
Oenanthe fistulosa	{mvk:PV,iucn:NT,voros_lista:PV}
Betula pubescens	{iucn:VU,voros_lista:PV}
Lythrum linifolium	{mvk:PV,iucn:EX,voros_lista:K}
Carduus glaucus	{mvk:AV,iucn:EN,voros_lista:AV}
Chamaecytisus albus	{iucn:NT,voros_lista:PV}
Epipactis gracilis	{washington:II,iucn:CR,voros_lista:KV}
Salicornia prostrata subsp. simonkaiana	{mvk:K,iucn:NT,voros_lista:PV}
Chenopodium vulvaria	{iucn:NT,voros_lista:PV}
Phyteuma orbiculare	{iucn:NT,voros_lista:PV}
Medicago nigra	{iucn:CR,voros_lista:KV}
Lappula heteracantha	{mvk:PV,iucn:NT,voros_lista:PV}
Andromeda polifolia	{mvk:K,iucn:EX,voros_lista:K}
Crocus reticulatus	{mvk:AV,bern:J-C,iucn:NT,voros_lista:PV}
Ornithogalum refractum	{mvk:PV,iucn:NT,voros_lista:PV}
Sisymbrium polymorphum	{mvk:PV,iucn:EN,voros_lista:AV}
Carex canescens	{mvk:PV,iucn:CR,voros_lista:KV}
Radiola linoides	{mvk:PV,iucn:EX,voros_lista:K}
Iris aphylla subsp. hungarica	{mvk:AV,bern:J-A,iucn:VU,voros_lista:PV}
Anthericum liliago	{mvk:PV,iucn:VU,voros_lista:PV}
Myricaria germanica	{mvk:PV,iucn:CR,voros_lista:KV}
Senecio rupestris	{mvk:K,iucn:EX,voros_lista:K}
Pyrola rotundifolia	{iucn:NT,voros_lista:PV}
Lilium bulbiferum	{mvk:KV,iucn:CR,voros_lista:KV}
Telekia speciosa	{mvk:KV,iucn:EN,voros_lista:AV}
Schoenus nigricans	{iucn:NT,voros_lista:PV}
Alchemilla filicaulis	{iucn:EN,voros_lista:AV}
Onosma arenarium subsp. tuberculatum	{mvk:PV,bern:J-C,iucn:DD,corine:E}
Leucojum vernum	{mvk:PV,iucn:NT,voros_lista:PV,corine:E}
Elatine alsinastrum	{iucn:NT,voros_lista:PV,corine:E}
Myosotis palustris	{corine:R}
Dactylorhiza fuchsii	{mvk:PV,washington:II,iucn:VU,voros_lista:PV,corine:E}
Allium suaveolens	{mvk:AV,iucn:VU,voros_lista:PV,corine:E}
Pyrus magyarica	{mvk:KV,bern:J-A,iucn:CR,voros_lista:KV,corine:R}
Orchis laxiflora subsp. palustris	{mvk:PV,washington:II,iucn:NT,voros_lista:PV,corine:E}
Carex stenophylla	{corine:R}
Thlaspi jankae	{bern:J-C,corine:R}
Viola biflora	{mvk:AV,iucn:EN,voros_lista:AV,corine:E}
Hammarbya paludosa	{washington:II,iucn:CR,voros_lista:KV,corine:E}
Diphasium issleri	{mvk:AV,iucn:EX,voros_lista:K,corine:E}
Carex repens	{mvk:PV,iucn:EN,voros_lista:AV,corine:R}
Trapa natans	{corine:E}
Bupleurum falcatum	{corine:!}
Sesleria hungarica	{mvk:PV,bern:J-C,iucn:NT,voros_lista:PV,corine:E}
Bupleurum falcatum subsp. dilatatum	{corine:E}
Dianthus plumarius subsp. lumnitzeri	{mvk:AV,bern:J-C,iucn:DD,corine:E}
Achillea ochroleuca	{corine:E}
Hesperis matronalis subsp. vrabelyiana	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV,corine:E}
Aldrovanda vesiculosa	{mvk:AV,bern:A-II,iucn:CR,voros_lista:KV,corine:R}
Alyssum montanum	{mvk:!,corine:!}
Typha shuttleworthii	{mvk:K,iucn:EX,voros_lista:K,corine:E}
Dracocephalum austriacum	{mvk:KV,bern:A-II,iucn:CR,voros_lista:KV,corine:E}
Spiranthes aestivalis	{mvk:PV,bern:A-IV,washington:II,iucn:EX,voros_lista:K,corine:E}
Potentilla thyrsiflora	{corine:R}
Alyssum montanum subsp. brymii	{mvk:PV,iucn:DD,corine:E}
Lythrum thesioides	{mvk:K,iucn:EX,voros_lista:K,corine:E}
Vicia sparsiflora	{mvk:PV,corine:E}
Salicornia prostrata	{mvk:!,iucn:NT,voros_lista:PV,corine:R}
Marsilea quadrifolia	{mvk:PV,bern:A-II,iucn:EN,voros_lista:AV,corine:E}
Colchicum arenarium	{mvk:PV,bern:J-C,iucn:NT,voros_lista:PV,corine:E}
Serratula lycopifolia	{mvk:AV,iucn:EN,voros_lista:AV,corine:E}
Lythrum tribracteatum	{iucn:NT,voros_lista:PV,corine:R}
Pulsatilla pratensis subsp. hungarica	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV,corine:E}
Astragalus dasyanthus	{mvk:AV,bern:J-C,iucn:VU,voros_lista:AV,corine:E}
Silene vulgaris	{corine:E}
Camelina alyssum	{iucn:EW,voros_lista:K,corine:R}
Crambe tataria	{mvk:AV,bern:J-C,iucn:EN,voros_lista:AV,corine:R}
Linum dolomiticum	{mvk:AV,bern:J-A,iucn:CR,voros_lista:KV,corine:E}
Coeloglossum viride	{mvk:PV,washington:II,iucn:VU,voros_lista:PV,corine:E}
Eriophorum gracile	{mvk:PV,iucn:EX,voros_lista:K,corine:E}
Spiraea media	{iucn:DD,corine:R}
Eleocharis carniolica	{mvk:AV,bern:A-II,iucn:NT,voros_lista:PV,corine:E}
Ligularia sibirica	{mvk:K,bern:A-II,iucn:EX,voros_lista:K,corine:E}
Achillea horanszkyi	{mvk:KV,iucn:CR,voros_lista:KV,corine:E}
Campanula macrostachya	{mvk:PV,iucn:EN,voros_lista:AV,corine:R}
Sorbus austriaca subsp. hazslinszkyana	{mvk:AV,iucn:CR,voros_lista:KV,corine:E}
Astragalus vesicarius subsp. albidus	{mvk:PV,iucn:NT,voros_lista:PV,corine:J-C}
Liparis loeselii	{mvk:KV,bern:A-II,washington:II,iucn:CR,voros_lista:KV,corine:E}
Dianthus serotinus	{iucn:NT,voros_lista:PV,corine:E}
Spiraea crenata	{mvk:K,iucn:CR,voros_lista:KV,corine:E}
Orchis coriophora	{mvk:PV,washington:II,iucn:NT,voros_lista:PV,corine:E}
Elatine hungarica	{mvk:PV,iucn:NT,voros_lista:PV,corine:E}
Ranunculus polyphyllus	{mvk:PV,iucn:NT,voros_lista:PV,corine:R}
Stipa dasyphylla	{mvk:AV,corine:E}
Onosma tornense	{mvk:KV,bern:J-A,iucn:CR,voros_lista:KV,corine:E}
Adenophora liliifolia	{mvk:AV,iucn:CR,voros_lista:KV,corine:R}
Seseli leucospermum	{mvk:PV,iucn:NT,voros_lista:PV,corine:E}
Elatine hexandra	{iucn:NT,voros_lista:PV,corine:E}
Paeonia officinalis subsp. banatica	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV,corine:R}
Brassica elongata	{iucn:NT,voros_lista:PV,corine:R}
Astragalus exscapus	{mvk:AV,iucn:NT,voros_lista:PV,corine:R}
Corispermum canescens	{mvk:PV,iucn:VU,voros_lista:PV,corine:E}
Apium repens	{mvk:PV,bern:A-II,iucn:EN,voros_lista:AV,corine:E}
Botrychium virginianum subsp. europaeum	{mvk:AV,iucn:CR,voros_lista:KV,corine:E}
Epipogium aphyllum	{mvk:PV,washington:II,iucn:CR,voros_lista:KV,corine:E}
Typha minima	{mvk:K,iucn:EX,voros_lista:K,corine:E}
Helleborus dumetorum	{corine:R}
Botrychium matricariifolium	{mvk:PV,iucn:EX,voros_lista:K,corine:E}
Ferula sadleriana	{mvk:AV,bern:J-A,iucn:EN,voros_lista:AV,corine:R}
Adonis flammea	{iucn:NT,voros_lista:PV,corine:R}
Cypripedium calceolus	{mvk:AV,bern:A-II,washington:II,iucn:EN,voros_lista:AV,corine:E}
Agropyron pectinatum	{corine:R}
Herniaria hirsuta	{mvk:PV,iucn:NT,voros_lista:PV,corine:R}
Pulsatilla patens	{mvk:KV,bern:A-II,iucn:CR,voros_lista:KV,corine:E}
Ornithogalum sphaerocarpum	{iucn:NT,voros_lista:PV,corine:R}
Hesperis matronalis	{mvk:!,bern:!,iucn:NT,voros_lista:PV,corine:!}
Adonis × hybrida	{mvk:KV,bern:J-A,voros_lista:KV,corine:R}
Lotus borbasii	{iucn:DD,corine:R}
Najas marina	{corine:E}
Herminium monorchis	{mvk:K,washington:II,corine:E}
Suaeda maritima subsp. salinaria	{corine:R}
Plantago maxima	{mvk:PV,iucn:CR,voros_lista:KV,corine:R}
Botrychium multifidum	{mvk:AV,iucn:CR,voros_lista:KV,corine:E}
Lindernia procumbens	{mvk:PV,bern:A-IV,corine:E}
Berula erecta	{corine:E}
Caldesia parnassifolia	{mvk:KV,bern:A-II,iucn:CR,voros_lista:KV,corine:E}
\.


--
-- Name: ssp_international_agreements ssp_international_agreements_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_international_agreements
    ADD CONSTRAINT ssp_international_agreements_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_international_agreements ssp_international_agreements_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_international_agreements
    ADD CONSTRAINT ssp_international_agreements_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

