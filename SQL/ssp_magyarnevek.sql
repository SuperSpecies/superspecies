\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_magyarnevek DROP CONSTRAINT ssp_magyarnevek_species_name_fkey;
--ALTER TABLE ONLY public.ssp_magyarnevek DROP CONSTRAINT ssp_magyarnevek_pkey;
--DROP TABLE public.ssp_magyarnevek;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_magyarnevek; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_magyarnevek (
    species_name character varying(64) NOT NULL,
    magyarnev character varying(64)[] NOT NULL
);


ALTER TABLE public.ssp_magyarnevek OWNER TO ssp_admin;

--
-- Data for Name: ssp_magyarnevek; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_magyarnevek (species_name, magyarnev) FROM stdin;
Abax carinatus	{"bordás szélesfutó"}
Abax ovalis	{"kerek szélesfutó"}
Abax parallelepipedus	{"félbordás szélesfutó"}
Abax parallelus	{"karcsú szélesfutó"}
Abax schueppeli	{"északi kárpáti szélesfutó"}
Abax schueppeli rendschmidtii	{"északi kárpáti szélesfutó"}
Abdera affinis	{fenyves-komorka}
Abdera quadrifasciata	{"négysávos komorka"}
Abdera triguttata	{"háromcseppes komorka"}
Abemus chloropterus	{"ékes holyva"}
Ablattaria laevigata	{"sima csigarabló"}
Abraeus granulum	{"lapátlábú gömbsutabogár"}
Abraeus perpusillus	{"közönséges gömbsutabogár"}
Abraeus roubali	{"vonalas gömbsutabogár"}
Abramis ballerus	{laposkeszeg}
Abramis brama	{dévérkeszeg}
Abramis sapa	{bagolykeszeg}
Abraxas grossulariata	{köszméte-araszoló}
Abraxas sylvata	{"sárgafoltos araszoló"}
Abrostola agnorista	{"sziklalakó csalánbagoly"}
Abrostola tripartita	{"szürketövű csalánbagoly"}
Abrostola triplasia	{"közönséges csalánbagoly"}
Acalles camelus	{"négypupú zártormányúbogár"}
Acalles commutatus	{"közönséges zártormányúbogár"}
Acalles parvulus	{"kétpupú zártormányúbogár"}
Acallocrates denticollis	{"fogasnyakú ormányos"}
Acalypta gracilis	{"réti kucsmás-csipkéspoloska"}
Acalypta marginata	{"kucsmás csipkéspoloska"}
Acalyptris loranthella	{fagyöngy-törpemoly}
Acalyptus carpini	{"selymes gyertyánormányos"}
Acalyptus sericeus	{"selymes fűzormányos"}
Acanthinula aculeata	{"tüskés csiga"}
Acanthobodilus immundus	{"viaszsárga trágyabogár"}
Acanthococcus aceris	{"juharrontó pajzstetű"}
Acanthococcus devoniensis	{hanga-pajzstetű}
Acanthococcus insignis	{pázsit-tüskéspajzstetű}
Acanthococcus roboris	{tölgy-tüskéspajzstetű}
Acanthopsyche atra	{"fekete zsákhordólepke"}
Acanthopsyche ecksteini	{"fenyőtűs zsákhordólepke"}
Acanthopsyche siederi	{"budai zsákhordólepke"}
Acanthosoma haemorrhoidale	{"nagy címerespoloska"}
Acartauchenius scurrilis	{"süvegfejű pók"}
Formica rufa	{"fészeképítő hangyafajok védett fészkei","erdei vöröshangya","erdei vöröshangya"}
Acentria ephemerella	{"törpe vízimoly"}
Acentrotypus brunnipes	{penészvirág-cickányormányos}
Acetropis carinata	{"bordás mezeipoloska"}
Acetropis longirostris	{"hosszúcsőrű mezeipoloska"}
Achenium ephippium	{"sókedvelő pocsolyaholyva"}
Achenium humile	{"lapos pocsolyaholyva"}
Acheta domesticus	{házitücsök}
Achlya flavicornis	{"sárgacsápú pihésszövő"}
Achroia grisella	{"kis viaszmoly"}
Achyra nudalis	{"sziki tűzmoly"}
Acidota cruentata	{"téli felemásholyva"}
Acinopus picipes	{"kis aknásfutó"}
Acipenser baeri	{"lénai tok"}
Acipenser ruthenus	{kecsege}
Acipenser stellatus	{sőregtok}
Aclerda subterranea	{fű-rovatkáltpajzstetű}
Acleris bergmanniana	{"rozsdarácsos levélmoly"}
Acleris cristana	{"pamacsos levélmoly"}
Acleris emargana	{"kivágottszegélyű levélmoly"}
Acleris ferrugana	{"rozsdás levélmoly"}
Acleris fimbriana	{"áfonyasodró levélmoly"}
Acleris forsskaleana	{juharos-levélmoly}
Acleris hastiana	{fűzfa-levélmoly}
Acleris holmiana	{"fehérfoltos levélmoly"}
Acleris kochiella	{"szürkés levélmoly"}
Acleris lipsiana	{"lápi levélmoly"}
Acleris literana	{"zöldesfehér levélmoly"}
Acleris logiana	{"hószínű levélmoly"}
Acleris lorquiniana	{"mocsári levélmoly"}
Acleris notana	{nyírfa-levélmoly}
Acleris permutana	{"díszes levélmoly"}
Acleris quercinana	{"tölgysodró levélmoly"}
Acleris rhombana	{"cifra levélmoly"}
Acleris roscidana	{rezgőnyár-levélmoly}
Acleris rufana	{"vörhenyes levélmoly"}
Acleris scabrana	{"fűzsodró levélmoly"}
Acleris schalleriana	{nadálytő-levélmoly}
Acleris shepherdana	{"ritka levélmoly"}
Acleris sparsana	{"hamvas levélmoly"}
Acleris umbrana	{"őszi sodrómoly"}
Acleris variegana	{"tarka levélmoly"}
Aclypea undata	{"fekete répabogár"}
Acmaeodera degener	{"sárgafoltos zömökdíszbogár"}
Acmaeoderella flavofasciata	{"sárgacsíkos zömökdíszbogár"}
Acmaeoderella mimonti	{"homoki zömökdíszbogár"}
Acompsia cinerella	{"szürke sarlósmoly"}
Acompsia tripunctella	{oroszlánszáj-sarlósmoly}
Acompus rufipes	{gyászbodobács}
Acontia lucida	{"fehér nappalibagoly"}
Acrida ungarica	{"sisakos sáska"}
Acritus homoeopathicus	{"fénytelen paránysutabogár"}
Acritus minutus	{"simanyakú paránysutabogár"}
Acritus nigricornis	{"fényes paránysutabogár"}
Acrobasis consociella	{"szalagos karcsúmoly"}
Acrobasis glaucella	{"hamvasfoltú karcsúmoly"}
Acrobasis obtusella	{"körtelevélfonó karcsúmoly"}
Acrobasis sodalella	{"tölgyfonó karcsúmoly"}
Acrocercops brongniardella	{"tölgyaknázó hólyagosmoly"}
Acrolepia autumnitella	{ebszőlő-tarkamoly}
Acrolepiopsis assectella	{hagymavirág-tarkamoly}
Acrolepiopsis tauricella	{"fehérfoltos tarkamoly"}
Acrolocha minuta	{"apró barázdásholyva"}
Acroloxus lacustris	{pajzscsiga}
Acronicta aceris	{juhar-szigonyosbagoly}
Acronicta alni	{"éger szigonyosbagoly"}
Acronicta auricoma	{kökény-szigonyosbagoly}
Acronicta cuspis	{"lápi szigonyosbagoly"}
Acronicta euphorbiae	{kutyatej-szigonyosbagoly}
Acronicta leporina	{"fehér szigonyosbagoly"}
Acronicta megacephala	{"ligeti szigonyosbagoly"}
Acronicta psi	{"szürkés szigonyosbagoly"}
Acronicta rumicis	{sóska-szigonyosbagoly}
Acronicta strigosa	{"kis szigonyosbagoly "}
Acronicta tridens	{"barnás szigonyosbagoly"}
Acrossus depressus	{"lapos trágyabogár"}
Acrossus luridus	{"tarka trágyabogár"}
Acrossus rufipes	{"vörösbarna trágyabogár"}
Acrotona aterrima	{"selymes komposztholyva"}
Acrotona benicki	{"apró komposztholyva"}
Acrotona convergens	{"kicsiny komposztholyva"}
Acrotona muscorum	{"barnás komposztholyva"}
Acrotona obfuscata	{"ligeti komposztholyva"}
Acrotona parens	{"erdei komposztholyva"}
Acrotona parvula	{"mezei komposztholyva"}
Acrotona pygmaea	{"kis komposztholyva"}
Acrotona troglodytes	{"hegyi komposztholyva"}
Acrotylus insubricus	{"közönséges önbeásó sáska"}
Acrulia inflata	{"pufók barázdásholyva"}
Actebia praecox	{"homoki zöldbagoly"}
Actenia brunnealis	{"barna fényilonca"}
Actenia honestalis	{"karsztlakó fényilonca"}
Actenicerus siaelandicus	{"márványos pattanó"}
Actinotia polyodon	{"soksugarú sugarasbagoly"}
Actinotia radiosa	{"apró sugarasbagoly"}
Actites hypoleucos	{billegetőcankó}
Aculepeira ceropegia	{"fehérsávos keresztespók"}
Acupalpus brunnipes	{"fakó törpefutonc"}
Acupalpus dubius	{"sárga törpefutonc"}
Acupalpus elegans	{"pompás törpefutonc"}
Acupalpus exiguus	{"fekete törpefutonc"}
Acupalpus flavicollis	{"vörösnyakú törpefutonc"}
Acupalpus interstitialis	{"fényes törpefutonc"}
Acupalpus luteatus	{"barna törpefutonc"}
Acupalpus maculatus	{"vállfoltos törpefutonc"}
Acupalpus meridianus	{"feketenyakú törpefutonc"}
Acupalpus notatus	{"sziki törpefutonc"}
Acupalpus parvulus	{"sárganyakú törpefutonc"}
Acupalpus suturalis	{"sárgavarratú törpefutonc"}
Acylophorus glaberrimus	{"térdescsápú lápholyva"}
Adaina microdactyla	{sédkender-tollasmoly}
Adalia bipunctata	{"kétpettyes katica"}
Adalia conglomerata	{"rajzos katica"}
Adalia decempunctata	{"tízpettyes katica"}
Adela croesella	{"díszes tőrösmoly"}
Adela cuprella	{"aranyszájú tőrösmoly"}
Adela mazzolella	{"aranyfoltú tőrösmoly"}
Adela paludicolella	{"díszes tőrösmoly"}
Adela reaumurella	{"smaragdfényű tőrösmoly"}
Adela violella	{"ibolyafényű tőrösmoly"}
Adelphocoris lineolatus	{lucernapoloska}
Adelphocoris seticornis	{"szegélyes mezeipoloska"}
Adelphocoris vandalicus	{"feketevállú mezeipoloska"}
Aderus populneus	{"selymes korhóbogár"}
Adomerus biguttatus	{"kétpettyes földipoloska"}
Adoxophyes orana	{almailonca}
Adrastus axillaris	{"vállfoltos cserjepattanó"}
Adrastus kryshtali	{"sötétbarna cserjepattanó"}
Adscita statices	{"közönséges fémlepke"}
Aedia funesta	{folyófű-bagoly}
Aegle kaekeritziana	{"felemásszárnyú bagoly"}
Aegopinella pura	{"törékeny csiga"}
Aeletes atomarius	{"erdei paránysutabogár"}
Aeletes hopffgarteni	{"vonalas paránysutabogár"}
Aelia acuminata	{szipolypoloska}
Aelia klugii	{"csíkos szipolypoloska"}
Aelia rostrata	{"csőrös szipolypoloska"}
Aellopus atratus	{cigánybodobács}
Aelurillus m-nigrum	{"m-betűs macskapók"}
Aelurillus v-insignitus	{"v-betűs macskapók"}
Aesalus scarabaeoides	{"szőrös szarvasbogár"}
Aeshna affinis	{"gyakori acsa"}
Aeshna cyanea	{"sebes acsa"}
Aeshna grandis	{"nagy acsa"}
Aeshna isoceles	{"lápi acsa"}
Aeshna juncea	{"fatörzsvadász acsa"}
Aeshna mixta	{"nádi acsa"}
Aeshna viridis	{"zöld acsa"}
Aethalura punctulata	{"szürke égeraraszoló"}
Aethes beatricella	{"angol sárgamoly"}
Aethes bilbaensis	{"csillámos sárgamoly"}
Aethes cnicana	{bogáncsvirág-fúrómoly}
Aethes dilucidana	{"apró sárgamoly"}
Aethes flagellana	{"mezei sárgamoly"}
Aethes francillana	{"réti sárgamoly"}
Aethes hartmanniana	{ördögszem-fúrómoly}
Aethes kindermanniana	{ürömvirág-sárgamoly}
Aethes margaritana	{"fényes fúrómoly"}
Aethes margarotana	{"változékony fúrómoly"}
Aethes moribundana	{"sárgásszürke fúrómoly"}
Aethes nefandana	{"parlagi sárgamoly"}
Aethes rubigana	{bojtorjánvirág-fúrómoly}
Aethes rutilana	{"aranyló sárgamoly"}
Aethes sanguinana	{"vércsíkos sárgamoly"}
Aethes smeathmanniana	{fészkesvirág-sárgamoly}
Aethes tesserana	{"rácsos fúrómoly"}
Aethes tornella	{"rozsdacsíkos sárgamoly"}
Aethes triangulana	{"tarka fúrómoly"}
Aethes williana	{gyopár-fúrómoly}
Agalenatea redii	{"széles keresztespók"}
Agapeta hamana	{"közönséges sárgamoly"}
Agapeta largana	{"magyar sárgamoly"}
Agapeta zoegana	{"barnacsíkos sárgamoly"}
Agaricochara latissima	{"hosszúcsápú taplóholyva"}
Agdistis adactyla	{"közönséges egytollúmoly"}
Agdistis heydeni	{Heyden-egytollúmolya}
Agdistis tamaricis	{tamariska-egytollúmoly}
Agelena labyrinthica	{"illó tölcsérpók"}
Aglenus brunneus	{"vak szénabogár"}
Aglia tau	{"t-betűs pávaszem"}
Aglossa caprealis	{"kis zsiradékmoly"}
Aglossa pinguinalis	{zsiradékmoly}
Aglossa signicostalis	{hangyailonca}
Agnathus decoratus	{éger-bíborbogár}
Agnetina elegantula	{"díszes nagyálkérész"}
Agoliinus nemoralis	{"hegyi trágyabogár"}
Agonopterix adspersella	{sárgarépa-laposmoly}
Agonopterix alstromeriana	{bürökmoly}
Agonopterix angelicella	{angyalgyökér-laposmoly}
Agonopterix arenella	{ördögszem-laposmoly}
Agonopterix assimilella	{seprőzanót-laposmoly}
Agonopterix astrantiae	{völgycsillag-laposmoly}
Agonopterix atomella	{"pettyegetett laposmoly"}
Agonopterix capreolella	{"aggófűrágó laposmoly"}
Agonopterix carduella	{"aszatfúró laposmoly"}
Agonopterix ciliella	{"nagy laposmoly"}
Agonopterix cnicella	{"ördögszekérfúró laposmoly"}
Agonopterix curvipunctosa	{"hamvasvörös laposmoly"}
Agonopterix doronicella	{zergevirág-laposmoly}
Agonopterix furvella	{ezerjófű-laposmoly}
Agonopterix heracliana	{"közönséges laposmoly"}
Agonopterix hippomarathri	{gurgolya-laposmoly}
Agonopterix kaekeritziana	{"mézszínű laposmoly"}
Agonopterix laterella	{búzavirág-laposmoly}
Agonopterix liturosa	{orbáncfű-laposmoly}
Agonopterix nanatella	{bábakalács-laposmoly}
Agonopterix nervosa	{"okkerszínű laposmoly"}
Agonopterix ocellana	{"szemes laposmoly"}
Agonopterix oinochroa	{kocsord-laposmoly}
Agonopterix pallorella	{"sápadt laposmoly"}
Agonopterix parilella	{"nyúlköményfonó laposmoly"}
Agonopterix petasitis	{"acsalapu laposmoly"}
Agonopterix propinquella	{aszatmoly}
Agonopterix purpurea	{"bíborszínű laposmoly"}
Agonopterix putridella	{"kocsordfonó laposmoly"}
Agonopterix rotundella	{"porszürke laposmoly"}
Agonopterix selini	{nyúlkömény-laposmoly}
Agonopterix senecionis	{aggófű-laposmoly}
Agonopterix subpropinquella	{"imolafúró laposmoly"}
Agonopterix thapsiella	{"mediterrán laposmoly"}
Agonopterix yeatiana	{"köményszövő laposmoly"}
Agonum afrum	{"közönséges kisfutó"}
Agonum angustatum	{"keskenyhátú kisfutó"}
Agonum antennarium	{"kékes kisfutó"}
Agonum duftschmidi	{"széleshátú kisfutó"}
Agonum fuliginosum	{"füstös kisfutó"}
Agonum gracile	{"karcsú kisfutó"}
Agonum gracilipes	{"kecses kisfutó"}
Agonum hypocrita	{"lápi kisfutó"}
Agonum impressum	{"gödörkés kisfutó"}
Agonum longicorne	{"berki kisfutó"}
Agonum lugens	{"mocsári kisfutó"}
Agonum marginatum	{"szegélyes kisfutó"}
Agonum micans	{"ligeti kisfutó"}
Agonum muelleri	{Müller-kisfutó}
Agonum nigrum	{"szerecsen kisfutó"}
Agonum permoestum	{"nyurga kisfutó"}
Agonum piceum	{"szurkos kisfutó"}
Agonum scitulum	{"nyugati kisfutó"}
Agonum sexpunctatum	{"pompás kisfutó"}
Agonum thoreyi	{"hosszúnyakú kisfutó"}
Agonum versutum	{"szélesnyakú kisfutó"}
Agonum viduum	{"érces kisfutó"}
Agonum viridicupreum	{"rezeszöld kisfutó"}
Agostenus sulcicollis	{"szomorú bűzfutó"}
Agramma confusum	{"sziki csipkéspoloska"}
Agrilinus ater	{"kormos trágyabogár"}
Agrilinus convexus	{"domború trágyabogár"}
Agrilinus rufus	{"sárgásvörös trágyabogár"}
Agrilinus sordidus	{"homoki trágyabogár"}
Agrilus albogularis	{üröm-karcsúdíszbogár}
Agrilus angustulus	{"közönséges karcsúdíszbogár"}
Agrilus ater	{"hatpettyes karcsúdíszbogár"}
Agrilus auricollis	{szil-karcsúdíszbogár}
Agrilus betuleti	{nyír-karcsúdíszbogár}
Agrilus biguttatus	{"kétpettyes karcsúdíszbogár"}
Agrilus convexicollis	{kőris-karcsúdíszbogár}
Agrilus convexifrons	{sárgafagyöngy-karcsúdíszbogár}
Agrilus croaticus	{"horvát karcsúdíszbogár"}
Agrilus cuprescens	{földiszeder-karcsúdíszbogár}
Agrilus cyanescens	{lonc-karcsúdíszbogár}
Agrilus delphinensis	{kosárfűz-karcsúdíszbogár}
Agrilus derasofasciatus	{szőlő-karcsúdíszbogár}
Agrilus graminis	{"fűrészescsápú karcsúdíszbogár"}
Agrilus guerini	{Guerin-karcsúdíszbogár}
Agrilus hastulifer	{"ólmosfényű karcsúdíszbogár"}
Agrilus hyperici	{orbáncfű-karcsúdíszbogár}
Agrilus integerrimus	{boroszlán-karcsúdíszbogár}
Agrilus laticornis	{"szélescsápú karcsúdíszbogár"}
Agrilus lineola	{fűz-karcsúdíszbogár}
Agrilus litura	{tölgy-karcsúdíszbogár}
Agrilus macroderus	{rózsa-karcsúdíszbogár}
Agrilus obscuricollis	{"sötétnyakú karcsúdíszbogár"}
Agrilus olivicolor	{"olívzöld karcsúdíszbogár"}
Agrilus populneus	{nyárfa-karcsúdíszbogár}
Agrilus pratensis	{"kétszínű karcsúdíszbogár"}
Agrilus roscidus	{benge-karcsúdíszbogár}
Agrilus salicis	{cinegefűz-karcsúdíszbogár}
Agrilus sericans	{"sziki karcsúdíszbogár"}
Agrilus sinuatus	{galagonya-karcsúdíszbogár}
Agrilus subauratus	{kecskefűz-karcsúdíszbogár}
Agrilus sulcicollis	{"hengeres karcsúdíszbogár"}
Agrilus viridis	{"változékony karcsúdíszbogár"}
Agrilus viscivorus	{fehérfagyöngy-karcsúdíszbogár}
Agriopis aurantiaria	{"aranysárga tavasziaraszoló"}
Agriopis bajaria	{kökény-tavasziaraszoló}
Agriopis leucophaearia	{tölgy-tavasziaraszoló}
Agriopis marginaria	{"sárgás tavasziaraszoló"}
Agriotes acuminatus	{"sötétvarratos pattanó"}
Agriotes brevis	{"rövidnyakú pattanó"}
Agriotes lineatus	{"vetési pattanó"}
Agriotes medvedevi	{"sziki pattanó"}
Agriotes modestus	{"szürke pattanó"}
Agriotes obscurus	{"sötét pattanó"}
Agriotes pallidulus	{"sápadt pattanó"}
Agriotes pilosellus	{"erdei pattanó"}
Agriotes proximus	{"csíkos pattanó"}
Agriotes rufipalpis	{"sárgaszájú pattanó"}
Agriotes sputator	{"réti pattanó"}
Agriotes ustulatus	{"mezei pattanó"}
Agriphila brioniellus	{"hegyi fűgyökérmoly"}
Agriphila deliella	{"őszi fűgyökérmoly"}
Agriphila geniculea	{"parlagi fűgyökérmoly"}
Agriphila hungaricus	{"magyar fűgyökérmoly"}
Agriphila inquinatella	{"közönséges fűgyökérmoly"}
Agriphila poliellus	{"okkerszínű fűgyökérmoly"}
Agriphila selasella	{"fakó fűgyökérmoly"}
Agriphila straminella	{"aranyrojtú fűgyökérmoly"}
Agriphila tolli	{"karszterdei fűgyökérmoly"}
Agriphila tristella	{"gyászos fűgyökérmoly"}
Agrius convolvuli	{folyófűszender}
Agrochola circellaris	{"világosbarna őszibagoly"}
Agrochola helvola	{fűzfa-őszibagoly}
Agrochola humilis	{"olajbarna őszibagoly"}
Agrochola laevis	{"könnyű őszibagoly"}
Agrochola litura	{kökény-őszibagoly}
Agrochola lota	{kecskefűz-őszibagoly}
Agrochola lychnidis	{búzavirág-őszibagoly}
Agrochola macilenta	{gyertyán-őszibagoly}
Agrochola nitida	{"rozsdabarna őszibagoly"}
Agrotera nemoralis	{"ligeti tűzmoly"}
Agrotis bigramma	{"fésűs földibagoly"}
Agrotis cinerea	{"szürke földibagoly"}
Agrotis clavis	{"zömök földibagoly"}
Agrotis exclamationis	{"felkiáltójeles földibagoly"}
Agrotis ipsilon	{ipszilon-földibagoly}
Agrotis segetum	{"vetési földibagoly"}
Agrotis vestigialis	{"őszi földibagoly"}
Agrypnus murinus	{"egérszínű pattanó"}
Ahasverus advena	{"alomlakó fogasnyakú-lapbogár"}
Aiolopus strepens	{"áttelelő sáska"}
Aiolopus thalassinus	{"tengerzöld sáska"}
Airaphilus elongatus	{"hosszúkás fogasnyakú-lapbogár"}
Aix galericulata	{mandarinréce}
Aizobius sedi	{varjúháj-cickányormányos}
Cyanistes caeruleus	{"kék cinege",kékcinke}
Alabonia staintoniella	{"hárfajegyű díszmoly"}
Alaobia scapularis	{"vörhenyes penészholyva"}
Alastor biegelebeni	{"nyelessejtű darázs"}
Alboglossiphonia heteroclita	{"kis csigapióca"}
Alburnoides bipunctatus	{"sujtásos küsz"}
Alburnus alburnus	{küsz}
Alburnus chalcoides	{"kaszpi küsz"}
Alburnus chalcoides mento	{"állas küsz"}
Alces alces	{jávorszarvas}
Alcis jubata	{"zuzmórágó faaraszoló"}
Alcis repandata	{"foltos faaraszoló"}
Alebra albostriella	{fakabóca}
Aleimma loeflingiana	{tölgylevél-sodrómoly}
Aleochara binotata	{"komposztlakó fürkészholyva"}
Aleochara bipustulata	{"kétfoltos fürkészholyva"}
Aleochara breiti	{"ürgevendég fürkészholyva"}
Aleochara brevipennis	{"mocsári fürkészholyva"}
Aleochara clavicornis	{"bunkóscsápú fürkészholyva"}
Aleochara curtula	{"nagy fürkészholyva"}
Aleochara erythroptera	{"vörösszárnyú fürkészholyva"}
Aleochara fumata	{"gombakedvelő fürkészholyva"}
Aleochara haematoptera	{"partlakó fürkészholyva"}
Aleochara inconspicua	{"fényes fürkészholyva"}
Aleochara intricata	{"reszelős fürkészholyva"}
Aleochara laevigata	{"sima fürkészholyva"}
Aleochara lanuginosa	{"gyapjas fürkészholyva"}
Aleochara lata	{"gyászos fürkészholyva"}
Aleochara laticornis	{"vöröscsápú fürkészholyva"}
Aleochara lygaea	{"magashegyi fürkészholyva"}
Aleochara milleri	{"pikkelyes fürkészholyva"}
Aleochara moesta	{"kétszínű fürkészholyva"}
Aleochara ruficornis	{"vörös fürkészholyva"}
Aleochara sanguinea	{"vöröses fürkészholyva"}
Aleochara sparsa	{"közönséges fürkészholyva"}
Aleochara spissicornis	{"orsócsápú fürkészholyva"}
Aleochara tristis	{"érdes fürkészholyva"}
Algedonia terrealis	{"barnásszürke tűzmoly"}
Alianta incana	{"hamuszínű bibircsesholyva"}
Allagelena gracilens	{"kis tölcsérpók"}
Allandrus undulatus	{"hullámos orrosbogár"}
Allecula atterima	{"feketés alkonybogár"}
Allecula morio	{"karcsú alkonybogár"}
Alloeorhynchus flavipes	{"sárgalábú tolvajpoloska"}
Allomalia quadrivirgata	{"apró tamariska-ormányos"}
Allonyx quadrimaculatus	{"négyfoltos szúfarkas"}
Allophyes oxyacanthae	{galagonyabagoly}
Allygus mixtus	{"nagy tölgyfakabóca"}
Alocentron curvirostre	{mályvaszár-cickányormányos}
Aloconota cambrica	{"hegyi humuszholyva"}
Aloconota gregaria	{"gödröshátú humuszholyva"}
Aloconota insecta	{"ligeti humuszholyva"}
Aloconota languida	{"nyurga humuszholyva"}
Aloconota sulcifrons	{"berki humuszholyva"}
Alopecosa cuneata	{"dagadtlábú farkaspók"}
Alopecosa trabalis	{"lándzsás farkaspók"}
Alophia combustella	{pisztáciamoly}
Alopia livida	{"ólomszínű csiga"}
Saxicola torquata	{cigánycsuk,Cigánycsuk,"cigány csaláncsúcs"}
Alosa kessleri	{"dunai nagyhering"}
Alosa pontica	{"dunai nagyhering"}
Alosimus syriacus	{"osztrák hólyaghúzó"}
Alosimus syriacus austriacus	{"osztrák hólyaghúzó"}
Alphitobius diaperinus	{"penészevő gyászbogár"}
Alphitophagus bifasciatus	{"alomlakó gyászbogár"}
Alsophila aescularia	{vadgesztenye-araszoló}
Altenia scriptella	{"patkós borzasmoly"}
Alucita cymatodactyla	{"nagy soktollúmoly"}
Alucita desmodactyla	{tisztesfű-soktollúmoly}
Alucita grammodactyla	{ördögszem-soktollúmoly}
Alucita hexadactyla	{"kis soktollúmoly"}
Alucita huebneri	{imolavirág-soktollúmoly}
Alydus calcaratus	{"szőrös karimáspoloska"}
Alysson spinosus	{"foltos darázs"}
Amalorrhynchus melanarius	{vízitorma-ormányos}
Amalus scortillum	{"barna keserűfű-ormányos"}
Amara aenea	{"érces közfutó"}
Amara anthobia	{"réti közfutó"}
Amara apricaria	{"rozsdás közfutó"}
Amara aulica	{"fekete óriásközfutó"}
Amara bifrons	{"homoki közfutó"}
Amara brunnea	{"kereknyakú közfutó"}
Amara chaudoiri	{"balkáni mocsári karmosközfutó"}
Amara chaudoiri incognita	{"balkáni mocsári karmosközfutó"}
Amara communis	{"mezei közfutó"}
Amara concinna	{"simanyakú karmosközfutó"}
Amara consularis	{"zömök közfutó"}
Amara convexior	{"erdei közfutó"}
Amara convexiuscula	{"sziki óriásközfutó"}
Amara crenata	{"keskeny közfutó"}
Amara cursitans	{"termetes közfutó"}
Amara curta	{"hegyi közfutó"}
Amara equestris	{"eurázsiai vastagszegélyű közfutó"}
Amara eurynota	{"széles közfutó"}
Amara famelica	{"sötétcsápú közfutó"}
Amara familiaris	{"kerti közfutó"}
Amara fulva	{"sápadt közfutó"}
Amara fulvipes	{"nagy karmosközfutó"}
Amara gebleri	{Gebler-óriásközfutó}
Amara ingenua	{"parlagi közfutó"}
Amara littorea	{"berki közfutó"}
Amara lucida	{"fénylő közfutó"}
Amara lunicollis	{"sarkantyús közfutó"}
Amara majuscula	{"bronzos közfutó"}
Amara montivaga	{"pajzsnyakú közfutó"}
Amara municipalis	{"ligeti közfutó"}
Amara ovata	{"ovális közfutó"}
Amara plebeja	{"karcsú karmosközfutó"}
Amara proxima	{"rézbarna közfutó"}
Amara sabulosa	{"nádi közfutó"}
Amara saginata	{"pusztai közfutó"}
Amara saphyrea	{"azúrkék közfutó"}
Amara similata	{"közönséges közfutó"}
Amara sollicita	{"déli közfutó"}
Amara tibialis	{"apró közfutó"}
Amara tricuspidata	{"mezei karmosközfutó"}
Amarochara bonnairei	{"hangyakedvelő vastagcsápúholyva"}
Amarochara forticornis	{"fészeklakó vastagcsápúholyva"}
Amata phegea	{"fehérpettyes álcsüngőlepke"}
Amaurobius erberi	{"kis eretnekpók"}
Amaurobius fenestralis	{"ablakos eretnekpókablakos eretnekpók"}
Amaurobius ferox	{"nagy eretnekpók"}
Amaurophanes stigmosalis	{"balkáni tűzmoly"}
Amblyptilia acanthadactyla	{"díszes tollasmoly"}
Amblyptilia punctidactyla	{"pettyesszárnyú tollasmoly"}
Amblystomus metallescens	{"érces árvafutonc"}
Amblystomus niger	{"fekete árvafutonc"}
Amblytylus nasutus	{"csőrös mezeipoloska"}
Amegilla quadrifasciata	{"négycsíkos bundásméh"}
Ameiurus melas	{"fekete törpeharcsa"}
Ameiurus nebulosus	{törpeharcsa}
Ameiurus nebulosus pannonicus	{törpeharcsa}
Ametropus fragilis	{"vágotthasú kérész"}
Amischa analis	{"közönséges trapézfejűholyva"}
Amischa bifoveolata	{"gödörkés trapézfejűholyva"}
Amischa decipiens	{"barnás trapézfejűholyva"}
Amischa forcipata	{"ollós trapézfejűholyva"}
Amischa nigrofusca	{"suta trapézfejűholyva"}
Ammobates punctatus	{"bundás rontóméh"}
Ammoconia caecimacula	{"szürke őszibagoly"}
Ammoecius brevis	{"zömök trágyabogár"}
Ammophila campestris	{"mezei hernyóölő"}
Ammophila sabulosa	{"homoki hernyóölő"}
Saxicola torquatus	{"afrikai cigánycsuk"}
Ammoplanus handlirschi	{"törpe kaparódarázs"}
Ampedus balteatus	{"feketeöves pattanó"}
Ampedus cardinalis	{kardinális-pattanó}
Ampedus cinnaberinus	{"cinóbervörös pattanó"}
Ampedus elegantulus	{"csinos pattanó"}
Ampedus forticornis	{"vastagcsápú pattanó"}
Ampedus glycereus	{"hosszúkás pattanó"}
Ampedus nigerrimus	{"fekete pattanó"}
Ampedus nigroflavus	{"feketesárga pattanó"}
Ampedus pomonae	{"ráncosnyakú pattanó"}
Ampedus pomorum	{"rozsdás pattanó"}
Ampedus praeustus	{"rőtvörös pattanó"}
Ampedus quadrisignatus	{"négyfoltos pattanó"}
Ampedus rufipennis	{"vörösszárnyú pattanó"}
Ampedus sanguineus	{"vérvörös pattanó"}
Ampedus sanguinolentus	{"középfoltos pattanó"}
Ampedus sinuatus	{"felemásnyakú pattanó"}
Amphimallon assimile	{"kis sárgacserebogár"}
Amphimallon solstitiale	{"bordás sárgacserebogár"}
Amphimelania holandrii	{"szávai vízicsiga"}
Amphipoea fucosa	{"sárgafoltos fűgyökérbagoly"}
Amphipoea oculea	{"foltos fűgyökérbagoly"}
Amphipyra livida	{zsírfényűbagoly}
Amphipyra perflua	{"pompás zsírosbagoly"}
Amphipyra pyramidea	{"fahéjszínű zsírosbagoly"}
Amphipyra tetra	{"hegyi fahéjbagoly"}
Amphipyra tragopoginis	{"hárompontos zsírosbagoly"}
Amphisbatis incongruella	{áfonyás-díszmoly}
Amphotis marginata	{"hangyász fénybogár"}
Anacampsis blattariella	{"nyírsodró sarlósmoly"}
Anacampsis obscurella	{"feketesávos sarlósmoly"}
Anacampsis populella	{nyárlevél-sarlósmoly}
Anacampsis scintillella	{napvirág-sarlósmoly}
Anacampsis timidella	{"tölgysodró sarlósmoly"}
Anania funebris	{"aranyvessző-kormosmoly "}
Anania verbascalis	{"aranyszínű dudvamoly "}
Anaplectoides prasina	{"zöld áfonya-földibagoly"}
Anaproutia comitella	{"szürkésbarna zsákhordólepke"}
Anarsia lineatella	{"barackrágó sarlósmoly"}
Anarsia spartiella	{seprőzanótmoly}
Anas carolinensis	{"zöldszárnyú réce"}
Anasphaltis renigerellus	{citromfű-sarlósmoly}
Anaspis brunnipes	{"barnalábú árvabogár"}
Anaspis flava	{"sárga árvabogár"}
Anaspis frontalis	{"sárgahomlokú árvabogár"}
Anaspis pulicaria	{"bolhaszerű árvabogár"}
Anaspis rufilabris	{"rőtajkú árvabogár"}
Anaspis thoracica	{"kétszínű árvabogár"}
Anaspis varians	{"változó árvabogár"}
Anatis ocellata	{"szemfoltos katica"}
Anaulacaspis nigra	{"érdeshátú karcsúholyva"}
Anax imperator	{"óriás szitakötő"}
Anax parthenope	{"tavi szitakötő"}
Anchinia cristalis	{"ibolyszín boroszlánmoly"}
Anchinia daphnella	{"nagy boroszlánmoly"}
Anchinia laureolella	{henyeboroszlánmoly}
Anchomenus dorsalis	{"foltos kisfutó"}
Ancistrocerus nigricornis	{"laposhasú kürtősdarázs"}
Ancistrocerus parietinus	{"fali kürtősdarázs"}
Ancylis achatana	{"márványos horgasmoly"}
Ancylis apicella	{"bengesodró horgasmoly"}
Ancylis badiana	{"bükkönysodró horgasmoly"}
Ancylis comptana	{szamóca-horgasmoly}
Ancylis diminutana	{"apró horgasmoly"}
Ancylis geminana	{"hullámos horgasmoly"}
Ancylis laetana	{"fehér horgasmoly"}
Ancylis mitterbacheriana	{"tölgysodró horgasmoly"}
Ancylis myrtillana	{"áfonyasodró horgasmoly"}
Ancylis obtusana	{kutyabenge-horgasmoly}
Ancylis paludana	{"turjáni horgasmoly"}
Ancylis selenana	{"sötét horgasmoly"}
Ancylis subarcuana	{cinegefűz-horgasmoly}
Ancylis tineana	{"galagonyasodró horgasmoly"}
Ancylis uncella	{"hangarágó horgasmoly"}
Ancylis unculana	{"szedersodró horgasmoly"}
Ancylis unguicella	{"csarabos horgasmoly"}
Ancylis upupana	{"szilsodró horgasmoly"}
Ancylolomia palpella	{"magyar csőrösmoly"}
Ancylolomia pectinatellus	{"fésűscsápú csőrösmoly"}
Ancylosis albidella	{"balkáni karcsúmoly"}
Ancylosis cinnamomella	{"fahéjszínű karcsúmoly"}
Ancylosis oblitella	{"hamuszürke karcsúmoly"}
Ancylosis roscidella	{"dolomitlakó karcsúmoly"}
Ancylosis sareptalla	{sztyeppmoly}
Ancylus fluviatilis	{"pataki sapkacsiga"}
Ancyrosoma leucogrammes	{"vállas poloska"}
Andrena cineraria	{"őszfejű bányászméh"}
Andrena flavipes	{"szalagos bányászméh"}
Andrena haemorrhoa	{"sárgatorú bányászméh"}
Andrena hattorfiana	{"karcsú bányászméh"}
Andrena morio	{"fekete bányászméh"}
Andrena ovatula	{"lucerna bányászméh"}
Andrena scita	{"pirosas bányászméh"}
Andrena taraxaci	{"pitypang bányászméh"}
Andrena thoracica	{"vöröstorú bányászméh"}
Anerastia dubia	{homokifű-karcsúmoly}
Anerastia lotella	{"gabonarágó karcsúmoly"}
Aneurus laevis	{"rücskös poloska"}
Angerona prunaria	{"sárga kökényaraszoló"}
Anguilla anguilla	{angolna}
Anidorus nigrinus	{"fekete korhóbogár"}
Anisodactylus binotatus	{"mocsári foltosfutó"}
Anisodactylus nemorivagus	{"ligeti foltosfutó"}
Anisodactylus poeciloides	{"európai sziki foltosfutó"}
Anisodactylus signatus	{"nagy foltosfutó"}
Anisoplia agricola	{"keresztes szipoly"}
Anisoplia austriaca	{"osztrák szipoly"}
Anisoplia bromicola	{rozsnokszipoly}
Anisoplia lata	{"széles szipoly"}
Anisoplia tempestiva	{gabonaszipoly}
Anisosticta novemdecimpunctata	{"tizenkilencpettyes katica"}
Anisosticta quatuordecimpustulata	{"feketesárga katóka"}
Anisosticta sinuatomarginata	{"oldalsávos katóka"}
Anisosticta vigintiguttata	{"húszcseppes füsskata"}
Anisoxya fuscula	{"barna komork"}
Anisus spirorbis	{vízicsiga}
Anisus vortex	{lemezcsiga}
Anobium punctatum	{"kis kopogóbogár"}
Anogcodes ferrugineus	{"rozsdás álcincér"}
Anogcodes fulvicollis	{"fekete álcincér"}
Anogcodes ruficollis	{"sárganyakú álcincér"}
Anogcodes rufiventris	{"vöröshasú álcincér"}
Anogcodes seladonius	{"azúrkék álcincér"}
Anogcodes seladonius azureus	{"azúrkék álcincér"}
Anogcodes ustulatus	{"fogascombú álcincér"}
Anomala dubia	{"kis fináncbogár"}
Anomala solida	{"déli fináncbogár"}
Anomala vitis	{"nagy fináncbogár"}
Anommatus duodecimstriatus	{"csipkés humuszbogár"}
Anommatus hungaricus	{"magyar humuszbogár"}
Anommatus jelinecki	{"dombóvári humuszbogár"}
Anommatus pannonicus	{"pannon humuszbogár"}
Anommatus reitteri	{"sima humuszbogár"}
Anommatus stilleri	{"szegedi humuszbogár"}
Anomognathus cuspidatus	{"lapos kérgészholyva"}
Anopleta corvina	{"nagyfejű penészholyva"}
Anopleta kochi	{"fenyves penészholyva"}
Anoplius concinnus	{"sertefarkú útonálló"}
Anoplius nigerrimus	{"fekete útonálló"}
Anoplius viaticus	{"közönséges útonálló"}
Anoplotrupes stercorosus	{"erdei álganéjtúró"}
Anoscopus histrionicus	{"feketeerű kabóca"}
Anostirus castaneus	{"feketevégű pattanó"}
Anostirus purpureus	{"tűzvörös pattanó"}
Anotylus complanatus	{"érdes korhóholyva"}
Anotylus hamatus	{"tüskésfarú korhóholyva"}
Anotylus hybridus	{"erdei korhóholyva"}
Anotylus insecatus	{"csipkéshátú korhóholyva"}
Anotylus nitidulus	{"fényes korhóholyva"}
Anotylus rugifrons	{"recésfejű korhóholyva"}
Anotylus rugosus	{"rovátkáshátú korhóholyva"}
Anotylus saulcyi	{"fészeklakó korhóholyva"}
Anotylus tetracarinatus	{"rovátkás korhóholyva"}
Anotylus tetratoma	{"barázdás korhóholyva"}
Anoxia orientalis	{"keleti cserebogár"}
Anoxia pilosa	{"pusztai cserebogár"}
Antepipona deflenda	{"kicsiny kürtősdarázs"}
Anthaxia candens	{cseresznyefa-virágdíszbogár}
Anthaxia cichorii	{katáng-virágdíszbogár}
Anthaxia deaurata	{szil-virágdíszbogár}
Anthaxia fulgurans	{"közönséges virágdíszbogár"}
Anthaxia funerula	{zanót-virágdíszbogár}
Anthaxia hackeri	{Hacker-virágdíszbogár}
Anthaxia helvetica	{"helvét virágdíszbogár"}
Anthaxia hungarica	{"magyar virágdíszbogár"}
Anthaxia istriana	{"isztriai virágdíszbogár"}
Anthaxia manca	{"sávosnyakú virágdíszbogár"}
Anthaxia nitidula	{"ragyogó virágdíszbogár"}
Anthaxia nitidula signaticollis	{"keleti virágdíszbogár"}
Anthaxia olympica	{"olümposzi virágdíszbogár"}
Anthaxia plicata	{"redős virágdíszbogár"}
Anthaxia podolica	{"podóliai virágdíszbogár"}
Anthaxia quadripunctata	{"fekete virágdíszbogár"}
Anthaxia salicis	{"bíboros virágdíszbogár"}
Anthaxia semicuprea	{"rezes virágdíszbogár"}
Anthaxia similis	{fenyő-virágdíszbogár}
Anthaxia tuerki	{Türk-virágdíszbogár}
Anthelephila pedestris	{"hangyaszerű fürgebogár"}
Antheminia lunulata	{"fakó gyümölcspoloska"}
Antheraea yamamai	{tölgy-selyemlepke}
Antherophagus pallens	{dongóméhbogár}
Anthicus antherinus	{"közönséges fürgebogár"}
Anthicus ater	{"rövidszőrű fürgebogár"}
Anthicus axillaris	{"vállfoltos fürgebogár"}
Anthicus bimaculatus	{"kétfoltos fürgebogár"}
Anthicus fenestratus	{"ablakos fürgebogár"}
Anthicus flavipes	{"sárgalábú fürgebogár"}
Anthicus schmidtii	{schmidt-fürgebogár}
Anthidium florentinum	{"foltos pelyhesméh"}
Anthidium laterale	{"nagy pelyhesméh"}
Anthidium loti	{"kockás pelyhesméh"}
Anthidium manicatum	{"rajzos pelyhesméh"}
Anthobium atrocephalum	{"feketésfejű felemásholyva"}
Anthobium melanocephalum	{"feketefejű felemásholyva"}
Anthocharis cardamines	{kakukktorma-hajnalpírlepke}
Anthocharis gruneri	{grüner-hajnalpírlepke}
Anthocoris gallarumulmi	{"szilfa virágpoloska"}
Anthocoris minki	{"kőrisfa virágpoloska"}
Anthocoris nemoralis	{"fakó virágpoloska"}
Anthocoris nemorum	{"mezei virágpoloska"}
Anthonomus bituberculatus	{zelnice-rügylikasztó}
Anthonomus conspersus	{madárberkenye-bimbólikasztó}
Anthonomus humeralis	{törökmeggy-bimbólikasztó}
Anthonomus pedicularius	{galagonya-bimbólikasztó}
Anthonomus phyllocola	{fenyőbarkarontó-ormányos}
Anthonomus pinivorax	{fenyőhajtásrontó-ormányos}
Anthonomus piri	{rügyfúróormányos}
Anthonomus pomorum	{alma-bimbólikasztó}
Anthonomus rectirostris	{alma-bimbólikasztó}
Anthonomus rubi	{szamóca-bimbólikasztó}
Anthonomus rubripes	{pimpó-bimbólikasztó}
Anthonomus rufus	{kökény-bimbólikasztó}
Anthonomus sorbi	{berkenye-bimbólikasztó}
Anthonomus spilotus	{körte-rügylikasztó}
Anthonomus ulmi	{szilfa-bimbólikasztó}
Anthonomus undulatus	{"hullámos bimbólikasztó"}
Anthophagus angusticollis	{"keskenyhátú felemásholyva"}
Anthophagus caraboides	{"futóbogárszerű felemásholyva"}
Anthophila fabriciana	{"lápréti levélmoly"}
Anthophora crinipes	{"tavaszi bundásméh"}
Anthophora plagiata	{"fali bundásméh"}
Anthophora plumipes	{"gyakori bundásméh"}
Anthophora pubescens	{"kis bundásméh"}
Anthophora quadrimaculata	{"sárga bundásméh"}
Anthracus consputus	{"nyerges kisdedfutó"}
Anthracus longicornis	{"karcsú kisdedfutó"}
Anthracus transversalis	{"szélesnyakú kisdedfutó"}
Anthrenus fuscus	{"fakó múzeumbogár"}
Anthrenus goliath	{óriás-múzeumbogár}
Anthrenus museorum	{"közönséges múzeumbogár"}
Anthrenus olgae	{"kis múzeumbogár"}
Anthrenus pimpinellae	{"virágjáró múzeumbogár"}
Anthrenus polonicus	{"lengyel múzeumbogár"}
Anthrenus scrophulariae	{"nagy múzeumbogár"}
Anthrenus verbasci	{"pusztító múzeumbogár"}
Anthribus fasciatus	{alma-bimbólikasztó}
Anthribus nebulosus	{"ködfoltos pajzstetvész"}
Anticlea badiata	{"nagy mályvaaraszoló"}
Anticlea derivata	{vadrózsa-araszoló}
Anticollix sparsata	{lizinka-törpearaszoló}
Antigastra catalaunalis	{gyöngymoly}
Antispila metallella	{gyűrűssom-fényesmoly}
Antispila treitschkiella	{"somaknázó fényesmoly"}
Antitype chi	{"fehéres őszibagoly"}
Apalus bimaculatus	{"kétfoltos élősdibogár"}
Apalus bipunctatus	{"mezei élősdibogár"}
Apalus necydaleus	{"pusztai élősdibogár"}
Apamea anceps	{"barna dudvabagoly"}
Apamea crenata	{"változékony dudvabagoly"}
Apamea epomidion	{"hússzínű dudvabagoly"}
Apamea illyria	{"hegyi dudvabagoly"}
Apamea lateritia	{"bíborbarna dudvabagoly"}
Apamea lithoxylaea	{"sárgás dudvabagoly"}
Apamea monoglypha	{"nagy dudvabagoly"}
Apamea ophiogramma	{"mocsári dudvabagoly"}
Apamea remissa	{"szürke dudvabagoly"}
Apamea sicula	{Tallós-fűgyökérbagoly}
Apamea sordens	{"füstös dudvabagoly"}
Apamea sublustris	{"rozsdás dudvabagoly"}
Apatema mediopallidum	{"fehérsávos avarmoly"}
Apatema whalleyi	{"erdélyi avarmoly"}
Apatetris trivittellum	{"törpe sarlósmoly"}
Apeira syringaria	{orgona-hegyiaraszoló}
Aphanisticus elongatus	{"nyurga törpedíszbogár"}
Aphanisticus emarginatus	{"szegélyes törpedíszbogár"}
Aphanisticus pusillus	{"zömök törpedíszbogár"}
Aphantaulax cincta	{"négyfoltos kövipók"}
Aphantopus hyperantus	{"közönséges ökörszemlepke"}
Aphanus rolandri	{"kormos bodobács"}
Aphelia ferugana	{"okkersárga sodrómoly"}
Aphelia paleana	{"sápadt sodrómoly"}
Aphelia viburnana	{"parlagi sodrómoly"}
Aphelocheirus aestivalis	{"fenékjáró poloska"}
Aphidecta obliterata	{"sárga katica"}
Aphodius fimetarius	{"feketehasú trágyabogár"}
Aphodius foetens	{"vöröshasú trágyabogár"}
Aphomia foedella	{"déli koldusmoly"}
Aphomia sociella	{méhviaszmoly}
Aphomia zelleri	{koldusmoly}
Aphrodes bicinctus	{"foltosfejű kabóca"}
Aphrophora alni	{égerfakabóca}
Apion cruentatum	{"vérpiros cickányormányos"}
Apion haematodes	{madársóska-cickányormányos}
Apion rubens	{"pirosló cickányormányos"}
Apion rubiginosum	{"sóskarágó cickányormányos"}
Aplasta ononaria	{iglicearaszoló}
Aplexa hypnorum	{"nagy balogcsiga"}
Aplocera efformata	{"hamuszín csíkosaraszoló"}
Aplocera plagiata	{"szürke csíkosaraszoló"}
Aplocera praeformata	{"hegyi csíkosaraszoló"}
Aploderus caelatus	{"rovátkás korhadékholyva"}
Aploderus caesus	{"barázdás korhadékholyva"}
Apocheima hispidaria	{"borzas tavasziaraszoló"}
Apoda limacodes	{kagylólepke}
Apodemus agrarius	{"pirók erdeiegér"}
Apodemus flavicollis	{"sárganyakú erdeiegér"}
Apodemus sylvaticus	{"közönséges erdeiegér"}
Apodemus uralensis	{"kislábú erdeiegér"}
Apoderus coryli	{mogyoró-levélpödrő}
Apoderus erythropterus	{szeder-levélpödrő}
Apodia bifractella	{peremizsmag-sarlósmoly}
Apolygus lucorum	{"fűzöld mezeipoloska"}
Apolygus spinolae	{"zöld szőlőpoloska"}
Apomyelois bistriatella nephanes	{"nyírfalakó karcsúmoly"}
Apomyelois ceratoniae	{"indiai aszalványmoly"}
Aporia crataegi	{galagonya-krétalepke}
Aporodes floralis	{"articsókaevő tűzmoly"}
Aporophyla lutulenta	{"sziki őszibagoly"}
Apotomis betuletana	{"nyírfalevélsodró tükrösmoly"}
Apotomis capreana	{"fűzrügyrágó tükrösmoly"}
Apotomis inundana	{"kormos tükrösmoly"}
Apotomis lineana	{"fűzsodró tükrösmoly"}
Apotomis sauciana	{feketeáfonya-tükrösmoly}
Apotomis semifasciana	{"fűzrügysodró tükrösmoly"}
Apotomis sororculana	{nyíres-tükrösmoly}
Apotomis turbidana	{"nyárfalevélsodró tükrösmoly"}
Apristus subaeneus	{"apró kövifutó"}
Aproaerema anthyllidella	{nyúlhere-övesmoly}
Apsis albolineata	{rozsbogár}
Apterona helicoidella	{"csigahéjas zsákhordólepke"}
Aptinus bombarda	{tüzérbogár}
Aquarius najas	{"nagy molnárpoloska"}
Aquarius paludum	{"közönséges molnárpoloska"}
Aracerus fasciculatus	{kávé-orrosbogár}
Arachnoteutes rufithorax	{"piroslábú útonálló"}
Aradus betulae	{"közönséges kéregpoloska"}
Aradus cinnamomeus	{erdeifenyő-kéregpoloska}
Aradus conspicuus	{"fűrészesvállú kéregpoloska"}
Aradus corticalis	{tölgyfa-kéregpoloska}
Aradus depressus	{"lapos kéregpoloska"}
Aradus versicolor	{"tarka kéregpoloska"}
Araeoncus humilis	{"vágottfejű pók"}
Araneus angulatus	{"vállas keresztespók"}
Araneus circe	{"hamvas keresztespók"}
Araneus diadematus	{"koronás keresztespók"}
Araneus grossus	{óriás-keresztespók}
Araneus marmoreus	{"márványos keresztespók"}
Araneus quadratus	{"négyes keresztespók"}
Araneus triguttatus	{"háromfoltos keresztespók"}
Araniella cucurbitina	{"zöld keresztespók"}
Araschnia levana	{pókhálóslepke}
Archanara algae	{"barna nádibagoly"}
Archanara dissoluta	{"kis nádibagoly"}
Archanara geminipuncta	{"fehérfoltos nádibagoly"}
Archanara neurica	{"apró nádibagoly"}
Archanara sparganii	{"nagy nádibagoly"}
Archarius crux	{"keresztes zsuzsóka"}
Archarius pyrrhoceras	{gubacszsuzsóka}
Archarius salicivorus	{fűzfazsuzsóka}
Archiearis notha	{"vörhenyes nappaliaraszoló"}
Archiearis parthenias	{"nagy nappaliaraszoló"}
Archinemapogon yildizae	{"korhadéklakó gombamoly"}
Archips crataegana	{cseresznyeilonca}
Archips oporana	{fenyő-sodrómoly}
Archips podana	{"dudvarágó sodrómoly"}
Archips rosana	{rózsailonca}
Archips xylosteana	{"kökényszövő sodrómoly"}
Arctia caja	{"közönséges medvelepke"}
Arctia villica	{"fekete medvelepke"}
Arctornis l-nigrum	{"L-betűs szövő"}
Arctorthezia cataphracta	{"hegyi lemezes pajzstetű"}
Arctosa cinerea	{"szürke farkaspókszürke farkaspók"}
Arcyptera fusca	{"szép hegyisáska"}
Arcyptera microptera	{"rövidszárnyú hegyisáska"}
Arenostola semicana	{"változó sásbagoly"}
Arethusana arethusa	{"közönséges szemeslepke"}
Argiope bruennichi	{darázspók}
Argiope lobata	{"karéjos keresztespók"}
Argogorytes mystaceus	{"nagy kabócaölő"}
Argolamprotes micella	{"ezüstpettyes sarlósmoly"}
Argynnis adippe	{"ezüstös gyöngyházlepke"}
Argynnis aglaja	{"kerekfoltú gyöngyházlepke "}
Argynnis paphia	{"nagy gyöngyházlepke"}
Argyresthia abdominalis	{borókatű-aranymoly}
Argyresthia albistria	{"kökényfúró aranymoly"}
Argyresthia arceuthina	{"borókafúró ezüstmoly"}
Argyresthia bonnetella	{"galagonyafúró aranymoly"}
Argyresthia brockeella	{"nyírrügyfúró aranymoly"}
Argyresthia conjugella	{berkenyevirág-aranymoly}
Argyresthia curvella	{"almabimbófúró aranymoly"}
Argyresthia dilectella	{borókahajtás-aranymoly}
Argyresthia glaucinella	{"tölgyfúró aranymoly"}
Argyresthia goedartella	{égerbarka-aranymoly}
Argyresthia ivella	{"almarügyfúró aranymoly"}
Argyresthia laevigatella	{vörösfenyő-ezüstmoly}
Argyresthia praecocella	{"borókamagrágó aranymoly"}
Argyresthia pruniella	{meggyvirág-aranymoly}
Argyresthia pygmaeella	{kecskefűzbarka-aranymoly}
Argyresthia retinella	{nyírfahajtás-aranymoly}
Argyresthia semifusca	{májusfa-aranymoly}
Argyresthia semitestacella	{"bükkfúró aranymoly"}
Argyresthia sorbiella	{madárbirs-aranymoly}
Argyresthia spinosella	{kökényvirág-aranymoly}
Argyresthia thuiella	{"tujafúró ezüstmoly"}
Argyroneta aquatica	{búvárpók}
Argyroploce roseomaculana	{körtikemoly}
Argyrotaenia ljungiana	{"ékes sodrómoly"}
Arianta arbustorum	{"márványozott csigamárványozott csiga"}
Arion circumscriptus	{"lantos csupaszcsiga"}
Aristotelia calastomella	{"szikespusztai sarlósmoly"}
Aristotelia decurtella	{"seprősajkú sarlósmoly"}
Aristotelia ericinella	{csarabos-sarlósmoly}
Aristotelia subdecurtella	{füzényhajtás-sarlósmoly}
Aristotelia subericinella	{"sárgacsíkos sarlósmoly"}
Arma custos	{"vörhenyes címerespoloska"}
Arocatus longiceps	{platánbodobács}
Aroga flavicomella	{"aranyfejű sarlósmoly"}
Aroga velocella	{"sóskaszövő sarlósmoly"}
Arthrolips obscura	{"sötét pontbogár"}
Arthrolips picea	{"szurkos pontbogár"}
Artianus interstitialis	{"sápadt kabóca"}
Artiora evonymaria	{kecskerágó-araszoló}
Arvicola terrestris scherman	{kószapocok}
Asaphidion austriacum	{"nyugati sárfutó"}
Asaphidion caraboides	{"nagy sárfutó"}
Asaphidion flavipes	{"közönséges sárfutó"}
Asaphidion pallipes	{"sárgalábú sárfutó"}
Ascalenia vanella	{"füstösszárnyú tündérmoly"}
Ascotis selenaria	{"holdas faaraszoló"}
Asiraca clavicornis	{"sötétbarna sarkantyúskabóca"}
Asphalia ruficollis	{"vörösnyakú pihésszövő"}
Aspidapion aeneum	{"mályvaréme cickányormányos"}
Aspidapion radiolus	{ziliz-cickányormányos}
Aspidapion validum	{"magaspajzsocskájú cickányormányos"}
Aspidiphorus lareyniei	{"simahomlokú gömböc-áltaplószú"}
Aspidiphorus orbiculatus	{"barázdáshomlokú gömböc-áltaplószú"}
Aspilapteryx limosella	{hangyabogáncs-keskenymoly}
Aspilapteryx tringipennella	{utifű-keskenymoly}
Aspitates gilvaria	{zanótaraszoló}
Aspius aspius	{balin}
Asproparthenis punctiventris	{"lisztes répabarkó"}
Assara terebrella	{"tobozlakó karcsúmoly"}
Astacus astacus	{"folyami rák"}
Astacus leptodactylus	{kecskerák}
Astata boops	{"gyakori poloskaölő"}
Astata minor	{"kis poloskaölő"}
Astenus bimaculatus	{"sziki köviholyva"}
Astenus gracilis	{"hegyi köviholyva"}
Astenus immaculatus	{"ligeti köviholyva"}
Astenus lyonessius	{"közönséges köviholyva"}
Astenus procerus	{"karcsú köviholyva"}
Astenus pulchellus	{"kecses köviholyva"}
Astenus rutilipennis	{"pirók köviholyva"}
Astenus uniformis	{"sötét köviholyva"}
Asterodiaspis variolosa	{tölgy-himlőspajzstetű}
Asteroscopus sphinx	{szfinx-őszibagoly}
Asteroscopus syriaca	{"magyar őszi-fésűsbagoly"}
Asthena albulata	{gyertyán-fehéraraszoló}
Astrapaeus ulmi	{"pirosöves mohaholyva"}
Atanygnathus terminalis	{"vékonycsápú tőzegholyva"}
Ateliotum hungaricellum	{"díszes hulladékmoly"}
Atemelia torquatella	{"pókhálós nyírmoly"}
Aterpia corticana	{sisakvirágmoly}
Atethmia ambusta	{körtebagoly}
Atethmia centrago	{"bíborsávos díszbagoly "}
Atheta aeneicollis	{"fémes penészholyva"}
Atheta basicornis	{"selyemfényű penészholyva"}
Atheta boletophila	{"taplókedvelő penészholyva"}
Atheta britanniae	{"sárgáshátú penészholyva"}
Atheta castanoptera	{"barnaszárnyú penészholyva"}
Atheta crassicornis	{"közönséges penészholyva"}
Atheta euryptera	{"termetes penészholyva"}
Atheta gagatina	{"szurkos penészholyva"}
Atheta harwoodi	{"gödörkés penészholyva"}
Atheta hybrida	{"erdei penészholyva"}
Atheta hypnorum	{"keskenyfejű penészholyva"}
Atheta intermedia	{"hosszúlábú penészholyva"}
Atheta liturata	{"taplómászó penészholyva"}
Atheta nigricornis	{"gödörkéshátú penészholyva"}
Atheta nigritula	{"gombakedvelő penészholyva"}
Atheta oblita	{"rövidcsápú penészholyva"}
Atheta pallidicornis	{"barnás penészholyva"}
Atheta ravilla	{"nyurga penészholyva"}
Atheta sodalis	{"fényesfejű penészholyva"}
Atheta triangulum	{"címeres penészholyva"}
Atheta trinotata	{"sujtásos penészholyva"}
Atheta xanthopus	{"sárgás penészholyva"}
Athetis furvula	{"homoki selymesbagoly"}
Athetis pallustris	{"kormos selymesbagoly"}
Atholus bimaculatus	{"kétfoltos sutabogár"}
Atholus corvinus	{"hollófekete sutabogár"}
Atholus duodecimstriatus	{"tizenkétbarázdás sutabogár"}
Atholus duodecimstriatus quatuordecimstriatus	{"tizennégybarázdás sutabogár"}
Atholus praetermissus	{"sziki sutabogár"}
Athous austriacus	{"osztrák pattanó"}
Athous bicolor	{"hosszúnyakú pattanó"}
Athous haemorrhoidalis	{"szurkos pattanó"}
Athous silicensis	{"szilicei pattanó"}
Athous subfuscus	{"pej pattanó"}
Athous vittatus	{"vörössávos pattanó"}
Athrips mouffetella	{lonclevél-sarlósmoly}
Athrips nigricostella	{"lucernafonó sarlósmoly"}
Athrips rancidella	{kutyabenge-sarlósmoly}
Atolmis rubricollis	{"acélszínű zuzmószövő"}
Atomaria linearis	{törpe-répabogár}
Atomaria munda	{"kétszínű spórabogár"}
Atomaria peltata	{"kisded spórabogár"}
Atomoscelis onusta	{"sziki törpepoloska"}
Atractotomus mali	{gyümölcsfapoloska}
Atralata albofascialis	{"fehércsíkos kormosmoly"}
Atrecus affinis	{"kétszínű avarholyva"}
Atremaea lonchoptera	{"magyar nádmoly"}
Atrococcus bejbienkoi	{málna-viaszospajzstetű}
Attagenus brunneus	{"barna szűcsbogár"}
Attagenus pellio	{"közönséges szűcsbogár"}
Attagenus punctatus	{"sokpettyes szűcsbogár"}
Attagenus schaefferi	{"fekete szűcsbogár"}
Attagenus unicolor	{gyapjú-szűcsbogár}
Attelabus nitens	{tölgylevélsodró}
Atylotus fulvus	{"aranyló bögöly"}
Atylotus rusticus	{"virágjáró bögöly"}
Atypus affinis	{tölgyestorzpók}
Atypus muralis	{"kövi torzpók"}
Atypus piceus	{"szurkos torzpók"}
Auchmis detersa	{sóskaborbolya-bagoly}
Augasma aeratella	{"gubacshúzó zsákosmoly"}
Augyles hispidulus	{"keskeny iszapbogár"}
Augyles pruinosus	{"tarka iszapbogár"}
Augyles sericans	{"selymes iszapbogár"}
Aulacaspis rosae	{rózsafa-pajzstetű}
Aulacobaris angusta	{"keskeny báris"}
Aulacobaris chevrolati	{chevrolat-rügylikasztó}
Aulacobaris chlorizans	{"kék káposztabáris"}
Aulacobaris coerulescens	{repcebáris}
Aulacobaris distinctus	{"kecses harangvirág-ormányos"}
Aulacobaris erythropus	{"mintás bolhaormányos"}
Aulacobaris gudenusi	{repcebáris}
Aulacobaris lepidii	{zsázsabáris}
Aulacobaris neta	{"drapp gyujtoványfű-ormányos"}
Aulacobaris picicornis	{rezedabáris}
Aulacobaris quinquepunctatus	{"ötpettyes magormányos"}
Aulacobaris villae	{földitök-báris}
Auletobius sanguisorbae	{vérfűeszelény}
Aulonium ruficorne	{fenyves-héjbogár}
Aulonium trisulcum	{"vörösbarna héjbogár"}
Aulonothroscus brevicollis	{"kerekszemű merevbogár"}
Autalia impressa	{"közönséges gödörkésholyva"}
Autalia rivularis	{"rovátkás gödörkésholyva"}
Autographa bractea	{"aranyfoltú aranybagoly"}
Autographa gamma	{"ezüstgammás aranybagoly"}
Autographa jota	{"i-betűs aranybagoly"}
Autographa pulchrina	{"v-betűs aranybagoly"}
Axinotarsus marginalis	{"feketefüggős lágybogár"}
Axinotarsus ruficollis	{"vöröstorú lágybogár"}
Axylia putris	{"gyakori gabonabagoly"}
Bacotia claustrella	{"zuzmóevő zsákhordólepke"}
Bactra furfurana	{"gyakori szittyómoly"}
Bactra lacteana	{"magyar szittyómoly"}
Bactra lancealana	{"lándzsás szittyómoly"}
Bactra robustana	{"nagy szittyómoly"}
Badister bullatus	{"kis posványfutonc"}
Badister collaris	{"barna posványfutonc"}
Badister dilatatus	{"fekete posványfutonc"}
Badister dorsiger	{"busófejű posványfutonc"}
Badister lacertosus	{"erdei posványfutonc"}
Badister meridionalis	{"közönséges posványfutonc"}
Badister peltatus	{"szárnyatlan posványfutonc"}
Badister sodalis	{"sárgavállú posványfutonc"}
Badister unipustulatus	{"nagy posványfutonc"}
Badura ischnocera	{"trágyalakó penészholyva"}
Baetis scambus	{"törékeny kérész"}
Bagoopsis globicollis	{"szürke ártériormányos"}
Bagous alismatis	{hídőr-ormányos}
Bagous collignensis	{"marti ormányos"}
Bagous frit	{fritormányos}
Balea biplicata	{"erdei orsócsiga"}
Balea perversa	{"törékeny orsócsiga"}
Ballus chalybeius	{döcögőpók}
Bangasternus orientalis	{"rövidorrú barkó"}
Barbatula barbatula	{kövicsík}
Barbitistes constrictus	{"közönséges málnaszöcske"}
Barbitistes serricauda	{"fűrészes málnaszöcske"}
Barbus carpathicus	{"kárpáti márna"}
Barbus peloponnesius	{Petényi-márna}
Baris analis	{"vörösfarú báris"}
Baris artemisiae	{ürömbáris}
Baris nesapia	{feketeüröm-báris}
Baris steppensis	{"gödröcskéstorú báris"}
Barynotus hungaricus	{"magyar busaormányos"}
Barynotus moerens	{"barázdásorrú busaormányos"}
Barynotus obscurus	{"laposszemű busaormányos"}
Barypeithes araneiformis	{"simahátú mohaormányos"}
Barypeithes interpositus	{"sarkantyús mohaormányos"}
Barypeithes interpositus siliciensis	{"aggteleki mohaormányos"}
Barypeithes liptoviensis	{"liptói mohaormányos"}
Barypeithes mollicomus	{"bolyhos mohaormányos"}
Barypeithes pellucidus	{"tetszetős mohaormányos"}
Barypeithes styriacus	{"stíriai mohaormányos"}
Barypeithes tenex	{"érzékeny mohaormányos"}
Basistriga flammatra	{"szürkéslila földibagoly"}
Bathyomphalus contortus	{pogácsacsiga}
Bathyphantes gracilis	{"törékeny vitorlapók"}
Bathyphantes nigrinus	{"feketés vitorlapók"}
Bathysolen nubilus	{"piszkos karimáspoloska"}
Batia internella	{"apró díszmoly"}
Batia lambdella	{"osztrák díszmoly"}
Batrachedra pinicolella	{"fenyőtűszövő lándzsásmoly"}
Batrachedra praeangusta	{"nyárlevélszövő lándzsásmoly"}
Bedellia ehikella	{"magyar szulákmoly"}
Bedellia somnulentella	{"közönséges szulákmoly"}
Belomicrus italicus	{"légyfogó darázs"}
Bembecia albanensis	{"albán szitkár"}
Bembecia ichneumoniformis	{fürkészszitkár}
Bembecia megillaeformis	{rekettyeszitkár}
Bembecia puella	{csüdfűszitkár}
Bembecia scopigera	{zanótszitkár}
Bembecia uroceriformis	{dárdahere-szitkár}
Bembecinus hungaricus	{"magyar kabócarontó"}
Bembidion geniculatum	{"olajzöld gyorsfutó"}
Bembidion quadrimaculatum	{"négyfoltos gyorsfutó"}
Bembidion quadripustulatum	{"négypettyes gyorsfutó"}
Bembix rostrata	{óriás-csőrösdarázs}
Bembix tarsata	{"kurtaszárnyú csőrösdarázs"}
Bena bicolorana	{tölgyfa-zöldbagoly}
Benibotarus taygetanus	{"tarnóci hajnalbogár"}
Beosus maritimus	{"homoki bodobács"}
Beosus quadripunctatus	{"pettyes homokibodobács"}
Berginus tamarisci	{"hengeres gombabogár"}
Berytinus geniculatus	{"réti szúnyogpoloska"}
Berytinus minor	{"kis szúnyogpoloska"}
Berytinus montivagus	{"hegymászó szúnyogpoloska"}
Besdolus ventralis	{Frivaldszky-álkérész}
Bessobia fungivora	{"őszi penészholyva"}
Bessobia occulta	{"téli penészholyva"}
Betarmon bisbimaculatus	{"kerekfoltos pattanó"}
Betulapion simile	{nyírfa-cickányormányos}
Biastes brevicornis	{"rövidcsápú méh"}
Bijugis bombycella	{"alkonyati zsákhordólepke"}
Bijugis pectinella	{"füstös zsákhordólepke"}
Biphyllus frater	{"egyszínű álporva"}
Biphyllus lunatus	{"kétfoltos álporva"}
Biralus satellitius	{"címeres trágyabogár"}
Bisigna procerella	{"zuzmórágó díszmoly"}
Bisnius fimetarius	{"erdei ganajholyva"}
Bisnius nigriventris	{"feketés ganajholyva"}
Bisnius nitidulus	{"kurtaszárnyú ganajholyva"}
Bisnius pseudoparcus	{"szurdoklakó ganajholyva"}
Bisnius scribae	{"hosszúlábú ganajholyva"}
Bisnius sordidus	{"busafejű ganajholyva"}
Bisnius sparsus	{"jövevény ganajholyva"}
Bisnius spermophili	{"ürgevendég ganajholyva"}
Bisnius subuliformis	{"odúlakó ganajholyva"}
Biston betularia	{"szürke pettyesaraszoló"}
Biston strataria	{"barnasávos pettyesaraszoló"}
Bithynia leachii	{"kis vízicsiga"}
Bithynia tentaculata	{"közönséges vízicsiga"}
Bitoma crenata	{"szalagos héjbogár"}
Blaps abbreviata	{"déli bűzbogár"}
Blaps halophila	{"pontusi bűzbogár"}
Blaps lethifera	{"közönséges bűzbogár"}
Blaps mortisaga	{"hosszúlábú bűzbogár"}
Blastesthia turionella	{"rügyfúró gyantamoly"}
Blastobasis huemeri	{"erdei avarevőmoly"}
Blastobasis phycidella	{"közönséges avarevőmoly"}
Blastodacna atra	{"lándzsásszárnyú almamoly"}
Blastodacna hellerella	{"galagonyarágó lándzsásmoly"}
Bledius atricapillus	{"sárgacsápú ásóholyva"}
Bledius bicornis	{"kétszarvú ásóholyva"}
Bledius cribricollis	{"pirosszárnyú ásóholyva"}
Bledius dissimilis	{"közönséges ásóholyva"}
Bledius erraticus	{"homoktúró ásóholyva"}
Bledius furcatus	{"alföldi ásóholyva"}
Bledius gallicus	{"kis ásóholyva"}
Bledius longulus	{"fényes ásóholyva"}
Bledius nanus	{"füstöscsápú ásóholyva"}
Bledius opacus	{"homoki ásóholyva"}
Bledius pallipes	{"folyóparti ásóholyva"}
Bledius procerulus	{"sziki ásóholyva"}
Bledius roubali	{"posványlakó ásóholyva"}
Bledius spectabilis	{"sókedvelő ásóholyva"}
Bledius tibialis	{"deres ásóholyva"}
Bledius tricornis	{"háromszarvú ásóholyva"}
Bledius unicornis	{"egyszarvú ásóholyva"}
Bledius verres	{"bütykös ásóholyva"}
Blemus discus	{"szalagos fürgefutonc"}
Blepharidopterus angulatus	{"atkafaló poloska"}
Blethisa multipunctata	{"gödörlés iszapfutó"}
Blicca bjoerkna	{karikakeszeg}
Bodilus circumcinctus	{"sziki trágyabogár"}
Bodilus ictericus	{"fényes trágyabogár"}
Bodilus lugens	{"borostyánsárga trágyabogár"}
Bohemannia pulverosella	{lisztes-törpemoly}
Bolbelasmus unicornis	{"szarvas álganéjtúró"}
Bolboceras armiger	{"mozgószarvú álganéjtúró"}
Bolitobius castaneus	{"pirosöves gombászholyva"}
Bolitobius cingulatus	{"termetes gombászholyva"}
Bolitochara bella	{"díszes tarkaholyva"}
Bolitochara lucida	{"fényes tarkaholyva"}
Bolitochara obliqua	{"sötét tarkaholyva"}
Bolitochara pulchra	{"ékes tarkaholyva"}
Bolitochara reyi	{"szurdoklakó tarkaholyva"}
Bolitophagus interruptus	{"láncos taplóbogár"}
Bolitophagus reticulatus	{"bordás taplóbogár"}
Boloria dia	{"kis gyöngyházlepke"}
Bolyphantes luteolus	{"nagyhomlokú vitorlapók"}
Bombina bombina × variegata	{"sárga és vöröshasú unka hibridje"}
Bombus confusus	{"bársonyos poszméh"}
Bombus hortorum	{"kerti poszméh"}
Bombus humilis	{"változékony poszméh"}
Bombus laesus	{"rozsdássárga poszméh"}
Bombus lapidarius	{"kövi poszméh"}
Bombus muscorum	{"sárga poszméh"}
Bombus paradoxus	{"ritka poszméh"}
Bombus pascuorum	{"mezei poszméh"}
Bombus pomorum	{"vörhenyes poszméh"}
Bombus pratorum	{"réti poszméh"}
Bombus soroeensis	{bogáncsposzméh}
Bombus sylvarum	{"erdei poszméh"}
Bombyx mori	{selyemlepke}
Boreococcus ingricus	{sás-viaszospajzstetű}
Borkhausenia fuscescens	{"agyagbarna díszmoly"}
Borkhausenia minutella	{"ikerpettyes díszmoly"}
Borysthenia naticina	{"kúpos kerekszájúcsiga"}
Bostrichus capucinus	{"piros csuklyásszú"}
Bothrideres contractus	{"szívnyakú humuszbogár"}
Bothynoderes affinis	{"sávos répabarkó"}
Brachida exigua	{"zömök taplóholyva"}
Brachinus bipustulatus	{"kétfoltos pöfögőfutó"}
Brachinus crepitans	{"nagy pöfögőfutrinka"}
Brachinus ejaculans	{"bordás pöfögőfutrinka"}
Brachinus elegans	{"mezei pöfögőfutrinka"}
Brachinus explodens	{"kis pöfögőfutrinka"}
Brachinus nigricornis	{"feketelábú pöfögőfutrinka"}
Brachinus plagiatus	{"pajzsfoltos pöfögőfutrinka"}
Brachinus psophia	{"pompás pöfögőfutrinka"}
Brachionycha nubeculosa	{"zömök fésűsbagoly"}
Brachmia blandella	{"fészeklakó lápimoly"}
Brachmia dimidiella	{citromkocsord-lápimoly}
Brachmia inornatella	{"nádrágó lápimoly"}
Brachmia procursella	{"hegyvidéki lápimoly"}
Brachodes appendiculata	{"homoki pusztamoly"}
Brachodes pumila	{"közönséges pusztamoly"}
Brachonyx pineti	{fenyőtűrágó-ormányos}
Brachycarenus tigrinus	{"rövidfejű üvegszárnyú poloska"}
Brachycercus europaeus	{"rövidhátú paránykérész"}
Brachycerus foveicollis	{"szögletes ragyásormányos"}
Brachycoleus pilicornis	{kutyatej-mezeipoloska}
Brachyderes incanus	{"csillámló ormányos"}
Brachygonus ruficeps	{"sárgafejű pattanó"}
Brachyleptus quadratus	{"magyar álfénybogár"}
Brachyptera braueri	{Brauer-álkérész}
Brachypterolus antirrhini	{oroszlánszáj-álfénybogár}
Brachypterolus linariae	{"feketelábú álfénybogár"}
Brachypterolus pulicarius	{gyújtoványfű-álfénybogár}
Brachypterus fulvipes	{"sárgalábú csalán-álfénybogár"}
Brachypterus glaber	{"fehérszőrű csalán-álfénybogár"}
Brachypterus urticae	{"barnaszőrű csalán-álfénybogár"}
Brachysomus dispar	{"barna gyepormányos"}
Brachysomus echinatus	{"tüskés gyepormányos"}
Brachysomus hirtus	{"sörtés gyepormányos"}
Brachysomus hispidus	{"dülledtszemű gyepormányos"}
Brachysomus setiger	{"vastagcsápú gyepormányos"}
Brachysomus slovacicus	{"szlovák gyepormányos"}
Brachysomus styriacus	{"stíriai gyepormányos"}
Brachysomus subnudus	{"meztelen gyepormányos"}
Brachysomus villosulus	{"ritkaszőrű gyepormányos"}
Brachytemnus porcatus	{"négyzetesnyakpajzsú szúormányos"}
Brachytron pratense	{"szőrös szitakötő"}
Brachyusa concolor	{"egyszínű cingárholyva"}
Bradybatus elongatulus	{"rozsdavörös juharfa-ormányos"}
Bradybatus fallax	{"pompás juharfa-ormányos"}
Bradybatus tomentosus	{"nyulánk juharfa-ormányos"}
Bradycellus caucasicus	{"szárnyatlan rétfutonc"}
Bradycellus csikii	{Csiki-rétfutonc}
Bradycellus harpalinus	{"szárnyas rétfutonc"}
Bradycellus verbasci	{"széleshátú rétfutonc"}
Brenthis daphne	{málna-gyöngyházlepke}
Brenthis hecate	{"rozsdaszínű gyöngyházlepke"}
Brevennia pulveraria	{"szár viaszospajzstetű"}
Brintesia circe	{"fehéröves szemeslepke"}
Broscus cephalotes	{busafutó}
Bruchela cana	{"déli rezedabogár"}
Bruchela conformis	{"tar rezedabogár"}
Bruchela muscula	{"egérszürke rezedabogár"}
Bruchela orientalis	{"keleti rezedabogár"}
Bruchela rufipes	{"őszes rezedabogár"}
Bruchela suturalis	{"sávos rezedabogár"}
Brumus quadripustulatus	{"négyfoltos szerecsenkata"}
Bryocoris pteridis	{harasztpoloska}
Bryoporus cernuus	{"agyagszínű gombászholyva"}
Bryoporus multipunctus	{"sokpontos gombászholyva"}
Bryotropha affinis	{"barnásfekete mohamoly"}
Bryotropha basaltinella	{"kövi mohamoly"}
Bryotropha desertella	{"homoki mohamoly"}
Bryotropha domestica	{"pettyes mohamoly"}
Bryotropha galbanella	{"északi mohamoly"}
Bryotropha senectella	{"barnásszürke mohamoly"}
Bryotropha similis	{"közönséges mohamoly"}
Bryotropha tachyptilella	{"apró mohamoly"}
Bryotropha terrella	{"rézbarna mohamoly"}
Bubo scandiaca	{hóbagoly}
Bucculatrix absinthii	{fehérüröm-bordásmoly}
Bucculatrix albedinella	{"szilaknázó bordásmoly"}
Bucculatrix artemisiella	{"ürömrágó bordásmoly"}
Bucculatrix bechsteinella	{galagonya-bordásmoly}
Bucculatrix benacicolella	{"selymes bordásmoly"}
Bucculatrix cantabricella	{"szulákrágó bordásmoly"}
Bucculatrix cidarella	{"égerrágó bordásmoly"}
Bucculatrix cristatella	{cickafark-bordásmoly}
Bucculatrix demaryella	{"mogyorós bordásmoly"}
Bucculatrix frangutella	{"mocsári bordásmoly"}
Bucculatrix gnaphaliella	{"homoki bordásmoly"}
Bucculatrix maritima	{"sziki bordásmoly"}
Bucculatrix nigricomella	{"feketefejű bordásmoly"}
Bucculatrix noltei	{feketeüröm-bordásmoly}
Bucculatrix ratisbonensis	{"homályos bordásmoly"}
Bucculatrix thoracella	{"hársaknázó bordásmoly"}
Bucculatrix ulmella	{"erdei bordásmoly"}
Bucculatrix ulmifoliae	{"szilrágó bordásmoly"}
Bupalus piniaria	{fenyőaraszoló}
Buprestis novemmaculata	{"kilencfoltos díszbogár"}
Buprestis octoguttata	{"nyolcfoltos díszbogár"}
Buprestis rustica	{lucfenyő-díszbogár}
Buzkoiana capnodactylus	{"kocsordfonó tollasmoly"}
Byctiscus betulae	{szőlőlevélsodró}
Byctiscus populi	{nyárfa-levélsodró}
Byrrhus fasciatus	{"sávos labdacsbogár"}
Byrrhus gigas	{"nagy labdacsbogár"}
Byrrhus luniger	{"holdas labdacsbogár"}
Byrrhus pilula	{"közönséges labdacsbogár"}
Byrrhus pustulatus	{"kis labdacsbogár"}
Byrsinus flavicornis	{"barna földipoloska"}
Bythinella austriaca	{"kárpáti forráscsiga"}
Bythinella pannonica	{"tornai patakcsiga"}
Bythinella thermophila	{"kácsi forráscsiga"}
Bythiospeum hungaricum	{"magyar vakcsiga"}
Byturus ochraceus	{gyömbérgyökérbogár}
Byturus tomentosus	{málnabogár}
Cabera exanthemata	{"pettyes égeraraszoló"}
Cabera pusaria	{"fehér égeraraszoló"}
Caccobius histeroides	{kakukk-trágyatúró}
Caccobius schreberi	{"lakkfényű trágyatúró"}
Cacotemnus rufipes	{"rőtlábú kopogóbogár"}
Cadra cautella	{déligyümölcs-karcsúmoly}
Cadra figulilella	{"kis gyümölcsmoly"}
Cadra furcatella	{"trópusi gyümölcsmoly"}
Caenocara affine	{"rövidszőrű pöfetegálszú"}
Caenocara bovistae	{"közönséges pöfetegálszú"}
Calambus bipustulatus	{"kétfoltos pattanó"}
Calamia tridens	{"zöld fűbagoly"}
Calamosternus granarius	{"szurokszínű trágyabogár"}
Calamotropha aureliellus	{"aranyszínű nádlevélmoly"}
Calamotropha paludella	{"barna nádlevélmoly"}
Calathus ambiguus	{"homoki tarfutó"}
Calathus cinctus	{"parlagi tarfutó"}
Calathus erratus	{"pusztai tarfutó"}
Calathus fuscipes	{"sokpontos tarfutó"}
Calathus melanocephalus	{"vörösnyakú tarfutó"}
Calidris temminckii	{Temminck-partfutó}
Caliscelis wallengreni	{"levéllábú pajzsoskabóca"}
Callilepis nocturna	{"pettyes kövipók"}
Callilepis schuszteri	{"vonalas kövipók"}
Callimorpha dominula	{"fémes medvelepke"}
Callimus angulatus	{"zöld tölgycincér"}
Calliptamus italicus	{"olasz sáska"}
Callistege mi	{lóhere-nappalibagoly}
Callisthenes reticulatus	{"smaragd bábrabló"}
Callisto denticulella	{almalevél-hólyagosmoly}
Callistus lunatus	{díszfutó}
Calliteara abietis	{fenyő-gyapjaslepke}
Calliteara fascelina	{"vesszős szövő"}
Calliteara pudibunda	{"hamvas szövő"}
Callophrys rubi	{"zöldfonákú angyallepke"}
Callopistria juventina	{"csipkésszárnyú bíborbagoly"}
Calodera aethiops	{"apró vastagcsápúholyva"}
Calodera nigrita	{"kormos vastagcsápúholyva"}
Calodera rufescens	{"vöröses vastagcsápúholyva"}
Calodera uliginosa	{"mocsári vastagcsápúholyva"}
Calodromius spilotus	{"vállfoltos kéregfutó"}
Calophasia lunula	{gyujtoványfű-apróbagoly}
Calophasia opalina	{"foltos fehérbagoly"}
Calophasia platyptera	{oroszlánszájbagoly}
Calopteryx splendens	{"sávos szitakötő"}
Calopteryx virgo	{kisasszony-szitakötő}
Caloptilia alchimiella	{"tölgygöngyölő keskenymoly"}
Caloptilia cuculipennella	{"fagyalaknázó keskenymoly"}
Caloptilia elongella	{"égerrágó keskenymoly"}
Caloptilia falconipennella	{"égergöngyölő keskenymoly"}
Caloptilia fidella	{"komlógöngyölő keskenymoly"}
Caloptilia fribergensis	{"szélesfoltú keskenymoly"}
Caloptilia hemidactylella	{"juhargöngyölő keskenymoly"}
Caloptilia honoratella	{"sárgafejű keskenymoly"}
Caloptilia robustella	{"kerekfoltú keskenymoly"}
Caloptilia roscipennella	{"diógöngyölő keskenymoly"}
Caloptilia rufipennella	{juharlevél-keskenymoly}
Caloptilia semifascia	{mezeijuhar-keskenymoly}
Caloptilia stigmatella	{"fűzgöngyölő keskenymoly"}
Calopus serraticornis	{"fekete álcincér"}
Calosirus terminatus	{petrezselyem-ormányos}
Calosoma auropunctatum	{"aranypettyes bábrabló"}
Calosoma sycophanta	{"aranyos bábrabló"}
Calvia decemguttata	{"tízcseppes füsskata"}
Calvia quatuordecimguttata	{"tizennégycseppes füsskata"}
Calvia quindecimguttata	{"tizenötcseppes füsskata"}
Calybites hauderi	{"sárgatükrű keskenymoly"}
Calybites phasianipennella	{"lóromaknázó keskenymoly"}
Calybites quadrisignella	{"bengeaknázó keskenymoly"}
Calyciphora albodactylus	{"csepeli tollasmoly"}
Calyciphora nephelodactyla	{"sötét tollasmoly"}
Calymma communimacula	{"pajzstetűfaló bagoly"}
Calyptra thalictri	{"öblösszárnyú bagoly"}
Cameraria ohridella	{vadgesztenye-sátorosmoly}
Campaea margaritaria	{"gyöngyházfényű zöldaraszoló"}
Camptogramma bilineata	{"kétvonalas sávosaraszoló"}
Camptopoeum frontale	{"sárgacsíkos méh"}
Camptopus lateralis	{"görbelábú karimáspoloska"}
Camptotelus lineolatus	{"sávos bodobács"}
Campylomma verbasci	{ökörfarkkóró-poloska}
Campylosteira verna	{"tavaszi csipkéspoloska"}
Canephora hirsuta	{"kormos zsákhordólepke"}
Canis aureus	{aranysakál}
Canis aureus moreoticus	{aranysakál}
Cantharis fusca	{"közönséges lágybogár"}
Cantharis lateralis	{"szegélyes lágybogár"}
Cantharis liburnica	{"déli lágybogár"}
Cantharis livida	{"mezei lágybogár"}
Cantharis nigricans	{"feketéllő lágybogár"}
Cantharis obscura	{"sötét lágybogár"}
Cantharis paradoxa	{"különös lágybogár"}
Cantharis pellucida	{"ligeti lágybogár"}
Cantharis pulicaria	{"szegettnyakú lágybogár"}
Cantharis rufa	{"rőt lágybogár"}
Cantharis rustica	{suszterbogár}
Canthophorus dubius	{"kékes földipoloska"}
Capnia bifrons	{"fekete álkérész"}
Capnodis tenebrionis	{kökény-tükrösdíszbogár}
Capperia celeusi	{gamandor-tollasmoly}
Capperia trichodactyla	{gyöngyajak-tollasmoly}
Capreolus capreolus	{"európai őz"}
Capricornia boisduvaliana	{"málnalevélsodró tükrösmoly"}
Capsodes gothicus	{"szőrös mezeipoloska"}
Capsus ater	{"fekete mezeipoloska"}
Capua vulgana	{"sárgásszürke sodrómoly"}
Carabus arvensis	{"sokszínű futrinka"}
Carabus arvensis austriae	{"osztrák sokszínű futrinka"}
Carabus arvensis carpathus	{"kárpáti sokszínű futrinka"}
Carabus auronitens	{aranyfutrinka}
Carabus auronitens kraussi	{"feketebordás aranyfutrinka"}
Carabus cancellatus	{"ragyás futrinka"}
Carabus cancellatus budensis	{"budai ragyás futrinka"}
Carabus cancellatus durus	{"északi ragyás futrinka"}
Carabus cancellatus maximus	{"nagy ragyás futrinka"}
Carabus cancellatus muehlfeldi	{"bihari ragyás futrinka"}
Carabus cancellatus soproniensis	{"soproni ragyás futrinka"}
Carabus cancellatus tibiscinus	{"alföldi ragyás futrinka"}
Carabus cancellatus ungensis	{"felvidéki ragyás futrinka"}
Carabus clatratus	{"szárnyas futrinka"}
Carabus clatratus auraniensis	{"balkáni szárnyas futrinka"}
Carabus convexus	{"selymes futrinka"}
Carabus convexus simplicipennis	{"kárpáti selymes futrinka"}
Carabus coriaceus	{"közönséges bőrfutrinka",bőrfutrinka}
Carabus coriaceus praeillyricus	{"illír bőrfutrinka"}
Carabus coriaceus pseudorugifer	{"nagy bőrfutrinka"}
Carabus coriaceus rugifer	{"ráncos bőrfutrinka"}
Carabus germarii	{"dunántúli kékfutrinka"}
Carabus germarii exasperatus	{"dunántúli kékfutrinka"}
Carabus glabratus	{"domború futrinka"}
Carabus granulatus	{"mezei futrinka"}
Carabus hampei ormayi	{"beregi sokbordás futrinka"}
Carabus hortensis	{"aranypettyes futrinka"}
Carabus hungaricus	{"magyar futrinka"}
Carabus intricatus	{"kék laposfutrinka"}
Carabus irregularis	{"alhavasi futrinka"}
Carabus irregularis cephalotes	{"nagyfejű alhavasi futrinka"}
Carabus linnaei	{Linné-futrinka}
Carabus linnaei transdanubialis	{"alpesi Linné-futrinka"}
Carabus marginalis	{"szegélyes futrinka","szegélyes futrinka"}
Carabus montivagus	{"balkáni futrinka"}
Carabus montivagus blandus	{"kis balkáni futrinka"}
Carabus nemoralis	{"ligeti futrinka"}
Carabus nodulosus	{"dunántúli vízifutrinka"}
Carabus obsoletus	{"pompás futrinka"}
Carabus problematicus	{"láncos futrinka"}
Carabus problematicus holdhausi	{"kis láncos futrinka"}
Carabus scabriusculus	{"érdes futrinka"}
Carabus scabriusculus lippii	{"nagy érdes futrinka"}
Carabus scheidleri	{"változó futrinka"}
Carabus scheidleri baderlei	{"mosoni változó futrinka"}
Carabus scheidleri dissimilis	{"zempléni futrinka"}
Carabus scheidleri distinguendus	{"szentendrei változó futrinka"}
Carabus scheidleri helleri	{"szlovák változó futrinka"}
Carabus scheidleri jucundus	{"pompás változó futrinka"}
Carabus scheidleri pannonicus	{"pannon változó futrinka"}
Carabus scheidleri praescheidleri	{"mecseki változó futrinka"}
Carabus scheidleri pseudopreyssleri	{"simahátú változó futrinka"}
Carabus scheidleri ronayi	{"zempléni futrinka"}
Carabus scheidleri vertesensis	{"vértesi változó futrinka"}
Carabus scheidleri zawadzkii	{"zempléni futrinka"}
Carabus ullrichii	{"rezes futrinka"}
Carabus ullrichii baranyensis	{"rezes futrinka"}
Carabus ullrichii planitiae	{"rezes futrinka"}
Carabus ullrichii sokolari	{"rezes futrinka"}
Carabus variolosus	{"kárpáti vízifutrinka"}
Carabus violaceus	{"keleti kékfutrinka"}
Carabus violaceus betuliae	{"nyírségi keleti kékfutrinka"}
Carabus violaceus pseudoviolaceus	{"hamis keleti kékfutrinka"}
Caradrina morpheus	{szulákbagoly}
Carassius carassius	{kárász}
Carassius gibelio	{ezüstkárász}
Carcharodus alceae	{mályva-busalepke}
Carcharodus floccifera	{pemetefű-busalepke}
Carcharodus orientalis	{"keleti busalepke"}
Carcina quercana	{"vastagcsápú díszmoly"}
Carcinops pumilio	{komposzt-sutabogár}
Cardiophorus anticus	{"sárgalábú szívespattanó"}
Cardiophorus asellus	{"homoki szívespattanó"}
Cardiophorus discicollis	{"korongfoltos szívespattanó"}
Cardiophorus ebeninus	{"ébenfekete szívespattanó"}
Cardiophorus gramineus	{"pirosnyakú szívespattanó"}
Cardiophorus maritimus	{"vöröslábú szívespattanó"}
Cardiophorus nigerrimus	{"szenes szívespattanó"}
Cardiophorus ruficollis	{"kékfényű szívespattanó"}
Cardiophorus vestigialis	{"sötét szívespattanó"}
Carpatolechia aenigma	{"háromsávos borzasmoly"}
Carpatolechia alburnella	{nyírfalevél-borzasmoly}
Carpatolechia decorella	{somlevél-borzasmoly}
Carpatolechia fugacella	{szilfalevél-borzasmoly}
Carpatolechia fugitivella	{juharlevél-borzasmoly}
Carpatolechia notatella	{kecskefűz-borzasmoly}
Carpatolechia proximella	{égerlevél-borzasmoly}
Carpelimus anthracinus	{"sziki iszapholyva"}
Carpelimus corticinus	{"sötét iszapholyva"}
Carpelimus elongatulus	{"erdei iszapholyva"}
Carpelimus erichsoni	{"rovátkás iszapholyva"}
Carpelimus exiguus	{"homoktútó iszapholyva"}
Carpelimus fuliginosus	{"selymes iszapholyva"}
Carpelimus ganglbaueri	{"pusztai iszapholyva"}
Carpelimus gracilis	{"kecses iszapholyva"}
Carpelimus heidenreichi	{"gyakori iszapholyva"}
Carpelimus impressus	{"barázdás iszapholyva"}
Carpelimus manchuricus	{"recés iszapholyva"}
Carpelimus nitidus	{"fényes iszapholyva"}
Carpelimus obesus	{"pohos iszapholyva"}
Carpelimus opacus	{"posványlakó iszapholyva"}
Carpelimus politus	{"selyemfényű iszapholyva"}
Carpelimus pusillus	{"apró iszapholyva"}
Carpelimus rivularis	{"közönséges iszapholyva"}
Carpelimus similis	{"partlakó iszapholyva"}
Carpelimus transversicollis	{"sókedvelő iszapholyva"}
Carphacis striatus	{"laposcsápú gombászholyva"}
Carpocoris fuscispinus	{"tüskés gyümölcspoloska"}
Carpocoris pudicus	{"közönséges gyümöncspoloska"}
Carpocoris purpureipennis	{"piros gyümölcspoloska"}
Carpophilus bipustulatus	{"kétjegyes gyümölcs-fénybogár"}
Carpophilus dimidiatus	{"vöröses gyümölcs-fénybogár"}
Carpophilus hemipterus	{"kétszínű gyümölcs-fénybogár"}
Carpophilus marginellus	{"szegélyes gyümölcs-fénybogár"}
Carpophilus pilosellus	{"selymes gyümölcs-fénybogár"}
Carpophilus quadrisignatus	{"négyjegyes gyümölcs-fénybogár"}
Carpophilus sexpustulatus	{"hatjegyes gyümölcs-fénybogár"}
Carposina berberidella	{borbolyamoly}
Carposina scirrhosella	{"közönséges csipkebogyómoly"}
Carterocephalus palaemon	{"kockás busalepke"}
Carterus angustipennis lutshniki	{"balkáni sztyeppfutó"}
Carulaspis juniperi	{boróka-pajzstetű}
Carulaspis visci	{fagyöngy-pajzstetű}
Carychium minimum	{"kétéltű csiga"}
Caryocolum alsinella	{madárhúr-sarlósmoly}
Caryocolum amaurella	{szurokszegfű-sarlósmoly}
Caryocolum blandella	{"feketesávos csillaghúrmoly"}
Caryocolum blandulella	{"apró csillaghúrmoly"}
Caryocolum cauligenella	{"szárduzzasztó sarlósmoly"}
Caryocolum fischerella	{szappangyökér-sarlósmoly}
Caryocolum huebneri	{"ékfoltos csillaghúrmoly"}
Caryocolum inflativorella	{"magyar habszegfűmoly"}
Caryocolum junctella	{"hamvas csillaghúrmoly"}
Caryocolum leucomelanella	{barátszegfű-sarlósmoly}
Caryocolum leucothoracellum	{"fehértorú sarlósmoly"}
Caryocolum marmoreum	{"márványos sarlósmoly"}
Caryocolum petryi	{fátyolvirág-sarlósmoly}
Caryocolum proximum	{"feketefoltos csillaghúrmoly"}
Caryocolum tricolorella	{"háromszínű csillaghúrmoly"}
Caryocolum vicinella	{"kiskunsági sarlósmoly"}
Caryocolum viscariella	{kakukkszegfű-sarlósmoly}
Casmerodius albus	{"nagy kócsag"}
Castor canadensis	{"kanadai hód"}
Castor fiber	{"eurázsiai hód"}
Cataclysta lemnata	{békalencsemoly}
Catapion jaffense	{"görbeorrú cickányormányos"}
Catapion meieri	{"bizonytalan cickányormányos"}
Catapion pubescens	{"széles cickányormányos"}
Catapion seniculus	{lóhereszár-cickányormányos}
Catarhoe cuculata	{"csuklyás tarkaaraszoló"}
Catarhoe rubidata	{"piros tarkaaraszoló"}
Catastia marginea	{"fekete karcsúmoly"}
Catephia alchymista	{"fekete övesbagoly"}
Catharacta skua	{"nagy halfarkas"}
Catocala electa	{fűzfa-övesbagoly}
Catocala elocata	{"közönséges övesbagoly"}
Catocala fulminea	{kökény-övesbagoly}
Catocala hymenaea	{galagonya-övesbagoly}
Catocala nupta	{"piros övesbagoly"}
Catocala nymphagoga	{"kis sárgaövesbagoly"}
Catocala promissa	{"kis tölgyfa-övesbagoly"}
Catocala puerpera	{nyárfa-övesbagoly}
Catocala sponsa	{tölgyfa-övesbagoly}
Catoplatus carthusianus	{"pimpó csipkéspoloska"}
Catops picipes	{"széles pecebogár"}
Catops tristis	{"gyászos pecebogár"}
Catoptria confusellus	{"tarka fűgyökérmoly"}
Catoptria falsella	{"hálós fűgyökérmoly"}
Catoptria fulgidella	{"villámmintás fűgyökérmoly"}
Catoptria lythargyrella	{"szalmasárga fűgyökérmoly"}
Catoptria margaritella	{"gyöngyös fűgyökérmoly"}
Catoptria myella	{"alpesi fűgyökérmoly"}
Catoptria mytilella	{"fehércsíkos fűgyökérmoly"}
Catoptria osthelderi	{"nyugati fűgyökérmoly"}
Catoptria permutatellus	{"gyöngyházcsíkos fűgyökérmoly"}
Catoptria pinella	{"ezüstös fűgyökérmoly"}
Catoptria verellus	{"kormos fűgyökérmoly"}
Cauchas fibulella	{"aranyfényű tőrösmoly"}
Cauchas leucocerella	{"aranybarna tőrösmoly"}
Cauchas rufifrontella	{"vöröshomlokú tőrösmoly"}
Cauchas rufimitrella	{"patinafényű tőrösmoly"}
Caulastrocecis furfurella	{"budai sarlósmoly"}
Cecilioides acicula	{"ragyogó tűcsiga"}
Cedestis gysseleniella	{"hamvas fenyőtűmoly"}
Cedestis subfasciella	{"szürke fenyőtűmoly"}
Celaena leucostigma	{"sötétbarna nádibagoly"}
Celastrina argiolus	{"korai bengeboglárka"}
Celes variabilis	{"változó sáska"}
Cellariopsis deubeli	{"keleti kristálycsiga"}
Celypha capreolana	{hölgymálmoly}
Celypha cespitana	{"mezei tükrösmoly"}
Celypha flavipalpana	{"öthorgú tükrösmoly"}
Celypha lacunana	{"vízparti tükrösmoly"}
Celypha rivulana	{"dudvasodró tükrösmoly"}
Celypha rufana	{"ürömfúró tükrösmoly"}
Celypha rurestrana	{hölgymál-tükrösmoly}
Celypha siderana	{"csillagos tükrösmoly"}
Celypha striana	{"pitypangfúró tükrösmoly"}
Celypha woodiana	{fagyöngy-tükrösmoly}
Centricnemus leucogrammus	{"kis kendermagbogár"}
Centrocoris variegatus	{"tüskéshátú karimáspoloska"}
Centrotus cornutus	{szarvaskabóca}
Cepaea hortensis	{"kerti csiga"}
Cepaea nemoralis	{"ligeti csiga"}
Cepaea vindobonensis	{"pannon csiga"}
Cephalophonus cephalotes	{"busafejű bársonyfutó"}
Cephimallota angusticostella	{"rozsdás hulladékmoly"}
Cepphis advenaria	{"okkersárga sávosaraszoló"}
Ceraleptus gracilicornis	{"tüskéslábú karimáspoloska"}
Ceramica pisi	{borsó-veteménybagoly}
Cerapteryx graminis	{"hegyi szegfűbagoly"}
Cerastis leucographa	{"sárgafoltos tavaszibagoly"}
Cerastis rubricosa	{"lila tavaszibagoly"}
Ceratapion armatum	{imola-cickányormányos}
Ceratapion austriacum	{"osztrák cickányormányos"}
Ceratapion basicorne	{bujdosó-cickányormányos}
Ceratapion carduorum	{bogáncs-cickányormányos}
Ceratapion cylindricolle	{vasvirág-cickányormányos}
Ceratapion gibbirostre	{"agyaras cickányormányos"}
Ceratapion onopordi	{szamárbogáncs-cickányormányos}
Ceratapion orientale	{"keleti cickányormányos"}
Ceratapion penetrans	{"lapátlábú cickányormányos"}
Ceratapion transsylvanicum	{"erdélyi cickányormányos"}
Ceratina chalybea	{"közönséges acélméh"}
Ceratina cyanea	{"gyakori acélméh"}
Cerceris arenaria	{"ormányosevő csomósdarázs"}
Cerceris circularis	{"magyar csomósdarázs"}
Cerceris quinquefasciata	{"ötsávos csomósdarázs"}
Cerceris sabulosa	{"közönséges csomósdarázs"}
Cercidia prominens	{"pajzsos keresztespók"}
Cercopis sanguinolenta	{"vérpettyes kabóca"}
Ceriagrion tenellum	{"lápi légivadász"}
Ceritaxa alluvialis	{"pirók penészholyva"}
Ceritaxa flavipes	{"sárgalábú penészholyva"}
Cerococcus cycliger	{kakukkfű-pajzstetű}
Cerocoma adamovichiana	{"balkáni torzcsápúbogár"}
Cerocoma muehlfeldi	{"hókás torzcsápúbogár"}
Cerocoma schaefferi	{"kis torzcsápúbogár"}
Cerocoma schreberi	{"nagy torzcsápúbogár"}
Ceropales maculata	{"foltos csempészdarázs"}
Cerophytum elateroides	{"európai álpattanóbogár"}
Cerura erminea	{hermelin-púposszövő}
Cerura vinula	{"nagy púposszövő"}
Cervus elaphus	{gímszarvas}
Cerylon deplanatum	{"lapított kéregbogár"}
Cerylon fagi	{"vastagcsápú kéregbogár"}
Cerylon ferrugineum	{"rozsdás kéregbogár"}
Cerylon histeroides	{"fekete kéregbogár"}
Cetonia aurata	{"aranyos virágbogár"}
Ceutorhynchus aeneicollis	{zsázsaormányos}
Ceutorhynchus alliariae	{"pirostappantyús kányazsombor-ormányos"}
Ceutorhynchus arator	{"megfoghatatlan ormányos"}
Ceutorhynchus atomus	{"szőrös rejtettormányú-bogár"}
Ceutorhynchus barbareae	{borbálafű-ormányos}
Ceutorhynchus canaliculatus	{hamukás-ormányos}
Ceutorhynchus carinatus	{tarsóka-ormányos}
Ceutorhynchus chalibaeus	{"fémes rejtettormányú-bogár"}
Ceutorhynchus chlorophanus	{"színes repcsényormányos"}
Ceutorhynchus coarctatus	{"ráspolyostorú ormányos"}
Ceutorhynchus cochleariae	{kakukktorma-ormányos}
Ceutorhynchus constrictus	{"gömböc rejtettormányú-bogár"}
Ceutorhynchus dubius	{hamukaormányos}
Ceutorhynchus erysimi	{"kis repcsényormányos"}
Ceutorhynchus fallax	{"fehérpettyes rejtettormányú-bogár"}
Ceutorhynchus fulvitarsis	{"vöröslábú rejtettormányú-bogár"}
Ceutorhynchus gallorhenanus	{"liszteshátú ormányos"}
Ceutorhynchus griseus	{"hamvasszürke rejtettormányú-bogár"}
Ceutorhynchus hirtulus	{lúdfűormányos}
Ceutorhynchus ignitus	{"kék hamukaormányos"}
Ceutorhynchus inaffectatus	{estike-ormányos}
Ceutorhynchus interjectus	{magaszsombor-ormányos}
Ceutorhynchus laetus	{"zöldorrú zsázsaormányos"}
Ceutorhynchus levantinus	{"keskeny rejtettormányú-bogár"}
Ceutorhynchus liliputanus	{liliputormányos}
Ceutorhynchus moraviensis	{"moráviai rejtettormányú-bogár"}
Ceutorhynchus nanus	{"törpe rejtettormányú-bogár"}
Ceutorhynchus napi	{"széles repceormányos"}
Ceutorhynchus nigritulus	{ikravirág-ormányos}
Ceutorhynchus niyazii	{"pirosorrú ormányos"}
Ceutorhynchus obstrictus	{repcebecő-ormányos}
Ceutorhynchus pallidactylus	{repceormányos}
Ceutorhynchus pallipes	{vadrepce-ormányos}
Ceutorhynchus parvulus	{"aprócska zsázsaormányos"}
Ceutorhynchus pectoralis	{"fehérfoltos kakukktorma-ormányos"}
Ceutorhynchus pervicax	{"zöld vízitorma-ormányos"}
Ceutorhynchus picitarsis	{"fekete káposztaormányos"}
Ceutorhynchus posthumus	{rejtőkeormányos}
Ceutorhynchus pulvinatus	{"kis zsomborormányos"}
Ceutorhynchus puncticollis	{"pontozott hamukaormányos"}
Ceutorhynchus pyrrhorhynchus	{vörösorrú-ormányos}
Ceutorhynchus querceti	{"ritka kányafűormányos"}
Ceutorhynchus rapae	{"nagy repcsényormányos"}
Ceutorhynchus rhenanus	{"szürke repcsényormányos"}
Ceutorhynchus roberti	{kányazsombor-ormányos}
Ceutorhynchus scapularis	{gomborka-ormányos}
Ceutorhynchus scrobicollis	{"gödröstorú ormányos"}
Ceutorhynchus similis	{"kis tarsókaormányos"}
Ceutorhynchus sisymbrii	{"szürke zsomborormányos"}
Ceutorhynchus sophiae	{szófia-ormányos}
Ceutorhynchus striatellus	{"sápadt rejtettormányú-bogár"}
Ceutorhynchus sulcatus	{"álomszép rejtettormányú-bogár"}
Ceutorhynchus sulcicollis	{"nagy zsomborormányos"}
Ceutorhynchus syrites	{"szíriai ormányos"}
Ceutorhynchus thlaspi	{"ritka tarsóka-ormányos"}
Ceutorhynchus turbatus	{zsázsa-rejtettormányú-bogár}
Ceutorhynchus typhae	{pásztortáska-ormányos}
Ceutorhynchus unguicularis	{"körmös rejtettormányú-bogár"}
Ceutorhynchus viridanus	{"vízparti repcsényormányos"}
Chaetabraeus globulus	{"sertés gömbsutabogár"}
Chaetida longicornis	{"hosszúcsápú penészholyva"}
Chaetococcus phragmitis	{nád-viaszospajzstetű}
Chaetococcus sulcii	{csenkesz-gömbpajzstetű}
Chaetophora spinosa	{"parti labdacsbogár"}
Chaetopteroplia segetum	{rozsszipoly}
Chaetopteryx schmidi	{"mecseki őszitegzes"}
Chaetopteryx schmidi mecsekensis	{"mecseki őszitegzes"}
Chalcionellus amoenus	{"gödrösnyakú fémsutabogár"}
Chalcionellus decemstriatus	{"fekete fémsutabogár"}
Chalcophora mariana	{"nagy fenyvesdíszbogár"}
Chalicodoma parietinum	{kőművesméh}
Chamaesphecia alysoniformis	{"ásódarázsalakú szitkár"}
Chamaesphecia annellata	{"gyűrüs szitkár"}
Chamaesphecia astatiformis	{farkasfűtej-szitkár}
Chamaesphecia bibioniformis	{"bársony szitkár"}
Chamaesphecia chalciformis	{"vörös szitkár"}
Chamaesphecia crassicornis	{tülkösszitkár}
Chamaesphecia doleriformis colpiformis	{"délvidéki szitkár"}
Chamaesphecia dumonti	{Dumont-szitkára}
Chamaesphecia euceraeformis	{kutyatejszitkár}
Chamaesphecia leucopsiformis	{"buckajáró szitkár"}
Chamaesphecia masariformis	{ökörfarkkóró-szitkár}
Chamaesphecia nigrifrons	{"középhegységi szitkár"}
Chamaesphecia tenthrediniformis	{légyszitkár}
Charagochilus gyllenhalii	{"kis cigány-mezeipoloska"}
Charanyca trigrammica	{"háromcsíkos fűbagoly"}
Charissa obscurata	{"sötét sziklaaraszoló"}
Chartoscirta cincta	{"nyakas partipoloska"}
Chartoscirta cocksii	{mocsár-partipoloska}
Cheiracanthium erraticum	{"csíkos dajkapók"}
Cheiracanthium punctorium	{"mérges dajkapók"}
Cheironitis ungaricus	{"magyarföldi ganéjtúró"}
Chelis maculosa	{"foltos medvelepke"}
Chelostoma campanularum	{"hír hengeresméh"}
Chelostoma rapunculi	{"nagy hengeresméh"}
Chersotis cuprea	{"rézfényű földibagoly"}
Chersotis margaritacea	{"gyöngyös földibagoly"}
Chersotis multangula	{"sokszögű földibagoly"}
Chersotis rectangula	{"balkáni földibagoly"}
Chesias legatella	{"szürkés zanótaraszoló"}
Chesias rufata	{"vöröses zanótaraszoló"}
Chiasmia clathrata	{"rácsos rétiaraszoló"}
Chilacis typhae	{buzogánybodobács}
Chilocorus bipustulatus	{"szalagos szerecsenkata"}
Chilocorus renipustulatus	{"vesefoltos szerecsenkata"}
Chilodes maritima	{"keskeny nádibagoly"}
Chilo phragmitella	{"csíkos nádfúrómoly"}
Chilopselaphus balneariellus podolicus	{"dunántúli kopármoly"}
Chilopselaphus fallax	{"karcsú kopármoly"}
Chilopselaphus lagopellus	{"szürke kopármoly"}
Chilothorax distinctus	{"rajzos trágyabogár"}
Chilothorax melanostictus	{"foltos trágyabogár"}
Chilothorax paykulli	{"kockás trágyabogár"}
Chilothorax pictus	{"festett trágyabogár"}
Chionaspis lepineyi	{"fehér tölgypajzstetű"}
Chionaspis salicis	{fűzfa-pajzstetű}
Chionodes distinctella	{"ürömszövő örvösmoly"}
Chionodes electella	{"fenyőrágó örvösmoly"}
Chionodes fumatella	{"mohalakó örvösmoly"}
Chionodes ignorantella	{"skandináv örvösmoly"}
Chionodes luctuella	{"havasi örvösmoly"}
Chionodes lugubrella	{szarvaskerep-örvösmoly}
Chionodes tragicella	{vörosfenyő-örvösmoly}
Chlaeniellus nigricornis	{"sötétcsápú bűzfutó"}
Chlaeniellus nitidulus	{"közönséges bűzfutó"}
Chlaeniellus tibialis	{"feketelábú bűzfutó"}
Chlaeniellus tristis	{"fekete bűzfutó"}
Chlaeniellus vestitus	{"sárgavégű bűzfutó"}
Chlaenius festivus	{"díszes bűzfutó"}
Chlaenius spoliatus	{"csupasz bűzfutó"}
Chlamydatus pulicarius	{"ugró törpepoloska"}
Chlamydatus pullus	{"sötétszürke törpepoloska"}
Chloantha hyperici	{orbáncfű-sugarasbagoly}
Chloris chloris	{zöldike}
Chlorissa cloraria	{"sárgászöld araszoló"}
Chlorissa viridata	{"üdezöld araszoló"}
Chlorita viridula	{ürömkabóca}
Chlorochroa juniperina	{"zöld borókapoloska"}
Chlorochroa pinicola	{fenyőpoloska}
Chloroclysta miata	{"zöldessárga araszoló"}
Chloroclysta siterata	{"salátazöld araszoló"}
Chloroperla tripunctata	{"folyami álkérész"}
Chlorophanus excisus	{"sárgafejű színesormányos"}
Chlorophanus graminicola	{fahéj-színesormányos}
Chlorophanus pollinosus	{"szegélyes színesormányos"}
Chlorophanus viridis	{zöld-színesormányos}
Chlorophanus viridis balcanicus	{balkáni-színesormányos}
Chlorophorus herbstii	{"foltos darázscincér"}
Chnaurococcus danzigae	{sás-gyökérpajzstetű}
Choleva cisteloides	{"hosszúlábú pecebogár"}
Chondrina arcadica	{"hamvas csiga"}
Chondrina arcadica clienta	{"hamvas csiga"}
Chondrostoma nasus	{paduc}
Chondrula tridens	{tonnacsiga}
Chonostropheus tristis	{juhar-levélsodró}
Choreutis nemorana	{"ligeti levélmoly"}
Choreutis pariana	{"őszi levélmoly"}
Choristoneura diversana	{juharlevél-sodrómoly}
Choristoneura hebenstreitella	{mogyorós-sodrómoly}
Choristoneura murinana	{jegenyefenyő-sodrómoly}
Chorosoma schillingii	{"karcsú botpoloska"}
Chorthippus albomarginatus	{"csinos rétisáska"}
Chorthippus alliaceus	{"hosszúszárnyú rétisáska"}
Chorthippus apricarius	{"szélesszárnyú tarlósáska"}
Chorthippus biguttulus	{"zengő tarlósáska"}
Chorthippus brunneus	{"közönséges tarlósáska"}
Chorthippus dichrous	{"vállas rétisáska"}
Chorthippus dorsatus	{"hátas rétisáska"}
Chorthippus mollis	{"halk tarlósáska"}
Chorthippus montanus	{"lápréti sáska"}
Chorthippus vagans	{"bolygó tarlósáska"}
Chortinaspis subterraneus	{tarackbúza-pajzstetű}
Chrysanthia nigricornis	{"feketecsápú álcincér"}
Chrysanthia viridissima	{"aranyos álcincér"}
Chrysis fulgida	{"kéksávos fémdarázs"}
Chrysis gracillima	{"kecses fémdarázs"}
Chrysis ignita	{"tűzvörös fémdarázs"}
Chrysis inaequalis	{"nyerges fémdarázs"}
Chrysis leachii	{"szivárványos fémdarázs"}
Chrysis millenaris	{"millenniumi fémdarázs"}
Chrysis pulchella	{"szép fémdarázs"}
Chrysis splendidula	{"kékfarú fémdarázs"}
Chrysis succincta	{"változó fémdarázs"}
Chrysis variegata	{"hatfogú fémdarázs"}
Chrysis viridula	{"ékes fémdarázs"}
Chrysobothris affinis	{"aranypettyes díszbogár"}
Chrysobothris chrysostigma	{"fényesjegyű díszbogár"}
Chrysobothris igniventris	{"tüzeshasú díszbogár"}
Chrysochraon dispar	{"aranyos sáska"}
Chrysoclista lathamella	{"fűzfonó lándzsásmoly"}
Chrysoclista linneella	{"pompás lándzsásmoly"}
Chrysocrambus craterella	{"rácsos fűgyökérmoly"}
Chrysocrambus linetella	{"déli fűgyökérmoly"}
Chrysodeixis chalcites	{"négyfoltos aranybagoly "}
Chrysoesthia drurella	{"labodarágó sarlósmoly"}
Chrysoesthia sexguttella	{"aranyfoltos sarlósmoly"}
Chrysops caecutiens	{"közönséges pőcsik"}
Chrysops flavipes	{"balatoni pőcsik"}
Chrysops relictus	{"kétfoltos pőcsik"}
Chrysops rufipes	{"vöröslábú pőcsik"}
Chrysops viduatus	{"egyfoltos pőcsik"}
Chrysoteuchia culmella	{"kerti fűgyökérmoly"}
Chrysura austriaca	{"osztrák fémdarázs"}
Chrysura cuprea	{"rézvörös fémdarázs"}
Chrysura dichroa	{"kétszínű fémdarázs"}
Cicada orni	{mannakabóca}
Cicadella viridis	{"méregzöld kabóca"}
Cicadetta montana	{"hegyi énekeskabóca"}
Cicadivetta tibialis	{"közönséges énekeskabóca"}
Cicindela campestris	{"mezei homokfutrinka"}
Cicindela hybrida	{"öves homokfutrinka"}
Cicindela hybrida transversalis	{"nyugati homokfutrinka"}
Cicindela soluta	{"alföldi homokfutrinka"}
Cicindela sylvicola	{"erdei homokfutrinka"}
Cicurina cicur	{"törpe tárnapók"}
Cidaria fulvata	{rózsaaraszoló}
Cidnopus pilosus	{"szőrös pattanó"}
Cidnopus ruzenae	{"fémfekete pattanó"}
Cilea silphoides	{"tarka fürgeholyva"}
Cilix glaucata	{törpeszövő}
Cimberis attelaboides	{fenyőáleszelé+J579ny}
Cimex dissimilis	{"kurtaszőrű denevérpoloska"}
Cimex lectularius	{"ágyi poloska"}
Cionellus gibbifrons	{"sokpöttyös gömbormányos"}
Cionus alauda	{"széleshomlokú gömbormányos"}
Cionus hortulanus	{"gyűrűslábú gömbormányos"}
Cionus longicollis	{"hegyi gömbormányos"}
Cionus longicollis montanus	{"hegyi gömbormányos"}
Cionus nigritarsis	{"feketelábú gömbormányos"}
Cionus olens	{"szőrös gömbormányos"}
Cionus olivieri	{"nagy ökörfarkkóró-gömbormányos"}
Cionus pulverosus	{"barna gömbormányos"}
Cionus scrophulariae	{görvélyfű-gömbormányos}
Cionus thapsus	{"kis ökörfarkkóró-gömbormányos"}
Cionus tuberculosus	{"sávosnyakú gömbormányos"}
Cionus ungulatus	{"hosszúkarmú gömbormányos"}
Cirrorhynchus arrogans	{"dölyfös gyalogormányos"}
Cirrorhynchus kelecsenyi	{kelecsényi-gyalogormányos}
Cis alter	{"sima taplószú"}
Cis bidentatus	{"kétszarvú taplószú"}
Cis boleti	{"nagy taplószú"}
Cis castaneus	{"gesztenyebarna taplószú"}
Cis comptus	{"zömök taplószú"}
Cis dentatus	{"fogas taplószú"}
Cis fagi	{"kis taplószú"}
Cis fissicollis	{"barázdásnyakú taplószú"}
Cis fissicornis	{"vágottszarvú taplószú"}
Cis glabratus	{bükk-taplószú}
Cis hispidus	{"közönséges taplószú"}
Cis jacquemartii	{"nyári taplószú"}
Cis lineatocribratus	{"vonalkás taplószú"}
Cis micans	{"fénylő taplószú"}
Cis punctulatus	{"pontocskás taplószú"}
Cis rugulosus	{"recés taplószú"}
Cis setiger	{"hosszúszőrű taplószú"}
Cis striatulus	{"sávos taplószú"}
Cixius nervosus	{égerfa-recéskabóca}
Clambus armadillo	{"ezüstszőrű combfedősbogár"}
Clambus dux	{"apró combfedősbogár"}
Clambus evae	{"homályos combfedősbogár"}
Clambus minutus	{"vörösbarna combfedősbogár"}
Clambus nigrellus	{"feketés combfedősbogár"}
Clambus pallidulus	{"halvány combfedősbogár"}
Clambus pilosellus	{"pillás combfedősbogár"}
Clambus pubescens	{"szőrös combfedősbogár"}
Clambus punctulum	{"nagyfejű combfedősbogár"}
Clanoptilus geniculatus	{"sárgaarcú bibircsbogár"}
Clanoptilus marginellus	{"sárgaarcú bibircsbogár"}
Clarias gariepinus	{"afrikai harcsa"}
Clausilia dubia	{"vésett orsócsiga"}
Clausilia dubia vindobonensis	{"vésett orsócsiga"}
Clausilia pumila	{"kis orsócsiga"}
Clemmus troglodytes	{"kerekded álböde"}
Cleoceris scoriacea	{"szürke liliombagoly"}
Cleonis pigra	{bogáncsbarkó}
Cleopomiarus graminis	{"szőrös harangvirág-ormányos"}
Cleopomiarus longirostris	{"hosszúorrú harangvirágormányos"}
Cleopomiarus medius	{"fönséges harangvirág-ormányos"}
Cleopomiarus micros	{kékcsillag-ormányos}
Cleopomiarus plantarum	{varjúköröm-ormányos}
Cleopus pulchellus	{"rövidszőrű csucsor-ormányos"}
Cleopus solani	{"hosszúszőrű csucsor-ormányos"}
Cleora cinctaria	{"körfoltos faaraszoló"}
Cleorodes lichenaria	{fazuzmó-araszoló}
Clepsis consimilana	{fagyal-sodrómoly}
Clepsis pallidana	{"aranysárga sodrómoly"}
Clepsis rolandriana	{"keleti sodrómoly"}
Clepsis rurinana	{"fakó sodrómoly"}
Clepsis senecionana	{"rozsdás sodrómoly "}
Clepsis spectrana	{"szalmaszínű sodrómoly"}
Cleptes nitidulus	{"pirosnyakú tolvajdarázs"}
Cleptes pallipes	{"sárgalábú tolvajdarázs"}
Cleptes semiauratus	{"aranyos tolvajdarázs"}
Clerus mutillarius	{"feketenyakú szúfarkas"}
Clethrionomys glareolus	{"vöröshátú erdeipocok"}
Clitostethus arcuatus	{liszteske-bödice}
Clivina collaris	{"kétszínű vakondfutó"}
Clivina fossor	{"egyszínű vakondfutó"}
Clivina ypsilon	{"sziki vakondfutó"}
Clostera anachoreta	{"tarka levélszövő"}
Clostera anastomosis	{"barna levélszövő"}
Clostera curtula	{"rövidszárnyú levélszövő"}
Clostera pigra	{"apró levélszövő"}
Closterotomus biclavatus	{"aranyszőrű mezeipoloska"}
Closterotomus fulvomaculatus	{komlópoloska}
Closterotomus norwegicus	{"zöld mezeipoloska"}
Clubiona caerulescens	{"kékes kalitpók"}
Clubiona comta	{"rácsos kalitpók"}
Clypastrea brunnea	{"barna pontbogár"}
Clypastrea orientalis	{"keleti pontbogár"}
Cnaemidophorus rhododactyla	{rózsabogyó-tollasmoly}
Cnemeplatia atropos	{"törpe gyászbogár"}
Cnephasia abrasana	{"erdőszéli sodrómoly"}
Cnephasia alticolana	{"havaslakó sodrómoly"}
Cnephasia asseclana	{aranyvessző-sodrómoly}
Cnephasia chrysantheana	{margaréta-sodrómoly}
Cnephasia communana	{"közönséges sodrómoly"}
Cnephasia ecullyana	{"déli sodrómoly"}
Cnephasia genitalana	{"homályos sodrómoly"}
Cnephasia incertana	{"márványos sodrómoly"}
Cnephasia oxyacanthana	{galagonya-sodrómoly}
Cnephasia pasiuana	{"réti sodrómoly"}
Cnephasia stephensiana	{"hegyaljai sodrómoly"}
Coccidohystrix artemisiae	{üröm-viaszospajzstetű}
Coccinella hieroglyphica	{csarabkatica}
Coccinella magnifica	{hangyászkatica}
Coccinella quinquepunctata	{"ötpettyes katica"}
Coccinella saucerottii	{"hárompettyes katica"}
Coccinella septempunctata	{"hétpettyes katica"}
Coccinella undecimpunctata	{"tizenegypettyes katica"}
Coccinella undecimpunctata tripunctata	{"tizenegypettyes katica"}
Coccura comari	{szeder-pajzstetű}
Cochlicopa lubrica	{"ragyogó csiga"}
Cochlodina cerata	{"sima orsócsiga"}
Cochlodina laminata	{"fényes orsócsiga"}
Cochlodina orthostoma	{"kis orsócsiga"}
Cochylidia heydeniana	{aranyvessző-fúrómoly}
Cochylidia implicitana	{kamilla-fúrómoly}
Cochylidia moguntiana	{mezeiüröm-fúrómoly}
Cochylidia richteriana	{ürömgyökér-fúrómoly}
Cochylidia rupicola	{sédkender-fúrómoly}
Cochylidia subroseana	{"rózsás fúrómoly"}
Cochylimorpha alternana	{"homoki sárgamoly"}
Cochylimorpha elongana	{"karcsú fúrómoly"}
Cochylimorpha hilarana	{ürömszár-fúrómoly}
Cochylimorpha jucundana	{"rozsdasávos fúrómoly"}
Cochylimorpha obliquana	{"magyar fúrómoly"}
Cochylimorpha perfusana	{"csontszínű fúrómoly"}
Cochylimorpha straminea	{"fakó sárgamoly"}
Cochylimorpha woliniana	{ürömhajtás-fúrómoly}
Cochylis atricapitana	{"mocsári fúrómoly"}
Cochylis dubitana	{"kis fúrómoly"}
Cochylis epilinana	{lentokmoly}
Cochylis flaviciliana	{"sárgarojtú fúrómoly"}
Cochylis hybridella	{keserűgyökér-fúrómoly}
Cochylis nana	{"törpe fúrómoly"}
Cochylis pallidana	{"fakó fúrómoly"}
Cochylis posterana	{aszatvirágmoly}
Cochylis roseana	{"apró pirosmoly"}
Cochylis salebrana	{"okkerbarna fúrómoly"}
Codocera ferruginea	{"nagyrágós homoktúróbogár"}
Codophila varia	{"változékony bogyómászó-poloska"}
Coeliastes lamii	{árvacsalán-ormányos}
Coeliodes rana	{cserormányos}
Coeliodes transversealbofasciatus	{"fehéröves ormányos"}
Coelioys afra	{"kis kakukkméh"}
Coelioys aurolimbata	{"csíkos kakukkméh"}
Coelioys conoidea	{"foltos kakukkméh"}
Coelioys inermis	{"rózsaméh kakukkja"}
Coelioys rufocaudata	{"pirosfarkú kakukkméh"}
Coelotes atropos	{bányászpók}
Coenagrion hastulatum	{"lándzsás légivadász"}
Coenagrion lunulatum	{"holdkék légivadász"}
Coenagrion ornatum	{"díszes légivadász"}
Coenagrion puella	{"szép légivadász"}
Coenagrion pulchellum	{"gyakori légivadász"}
Coenagrion pulchellum interruptum	{"gyakori légivadász"}
Coenagrion scitulum	{"ritka légivadász"}
Coenocalpe lapidata	{"csipkés iszalagaraszoló"}
Coenonympha arcania	{"fehérövű szénalepke"}
Coenonympha glycerion	{"közönséges szénalepke"}
Coenonympha hero	{"lápi szénalepke"}
Coenonympha pamphilus	{"kis szénalepke"}
Coenonympha tullia	{"mocsári szénalepke"}
Coleophora absinthii	{"ürömmagrágó zsákosmoly"}
Coleophora acrisella	{"dárdahererágó zsákosmoly"}
Coleophora adelogrammella	{"fűrágó zsákosmoly"}
Coleophora adjunctella	{"borókalakó zsákosmoly"}
Coleophora adspersella	{"libatoprágó zsákosmoly"}
Coleophora ahenella	{"somaknázó zsákosmoly"}
Coleophora albella	{"fehércsíkos zsákosmoly"}
Coleophora albicans	{"feketeürömlakó zsákosmoly"}
Coleophora albicostella	{"irtásréti zsákosmoly"}
Coleophora albidella	{"fehér zsákosmoly"}
Coleophora albilineella	{"délvidéki zsákosmoly"}
Coleophora albitarsella	{"árvacsalánlakó zsákosmoly"}
Coleophora alcyonipennella	{"bogáncsrágó zsákosmoly"}
Coleophora alticolella	{"szittyótermés zsákosmoly"}
Coleophora amellivora	{"őszirózsarágó zsákosmoly"}
Coleophora anatipenella	{"fehértollú zsákosmoly"}
Coleophora antennariella	{szittyólevél-zsákosmoly}
Coleophora argentula	{cickafarkmag-zsákosmoly}
Coleophora artemisicolella	{feketeüröm-zsákosmoly}
Coleophora asteris	{őszirózsa-zsákosmoly}
Coleophora astragalella	{"levantei zsákosmoly"}
Coleophora atriplicis	{labodamag-zsákosmoly}
Coleophora auricella	{"gamandorlakó zsákosmoly"}
Coleophora badiipennella	{"csíkosszegélyű zsákosmoly"}
Coleophora ballotella	{tisztesfű-zsákosmoly}
Coleophora betulella	{"nyírlakó zsákosmoly"}
Coleophora bilineatella	{"kétcsíkú zsákosmoly"}
Coleophora bilineella	{tetemtoldó-zsákosmoly}
Coleophora binderella	{égerlevél-zsákosmoly}
Coleophora binotapennella	{"kétpettyes zsákosmoly"}
Coleophora brevipalpella	{"imolarágó zsákosmoly"}
Coleophora caelebipennella	{szalmagyopár-zsákosmoly}
Coleophora caespititiella	{"mocsári zsákosmoly"}
Coleophora cartilaginella	{"keleti zsákosmoly"}
Coleophora cecidophorella	{"pusztai zsákosmoly"}
Coleophora chamaedriella	{"gamandoraknázó zsákosmoly"}
Coleophora chrysanthemi	{margaréta-zsákosmoly}
Coleophora ciconiella	{"gabonarágó zsákosmoly"}
Coleophora clypeiferella	{"pajzsoshátú zsákosmoly"}
Coleophora colutella	{pillangósvirág-zsákosmoly}
Coleophora congeriella	{"spanyol zsákosmoly"}
Coleophora conspicuella	{búzavirág-zsákosmoly}
Coleophora conyzae	{bolhafű-zsákosmoly}
Coleophora coracipennella	{kökény-zsákosmoly}
Coleophora cornutella	{"nyíraknázó zsákosmoly"}
Coleophora coronillae	{"ledneklakó zsákosmoly"}
Coleophora corsicella	{"korzikai zsákosmoly"}
Coleophora cracella	{"bükkönyaknázó zsákosmoly"}
Coleophora currucipennella	{"fakó zsákosmoly"}
Coleophora deauratella	{"aranyló zsákosmoly"}
Coleophora dentiferella	{"szürkecsíkos zsákosmoly"}
Coleophora dianthi	{szegfűtok-zsákosmoly}
Coleophora directella	{"agyagbarna zsákosmoly"}
Coleophora discordella	{bársonykerep-zsákosmoly}
Coleophora ditella	{margitvirág-zsákosmoly}
Coleophora eurasiatica	{"eurázsiai zsákosmoly"}
Coleophora flaviella	{"sárga zsákosmoly"}
Coleophora flavipennella	{"szürkésvörös zsákosmoly"}
Coleophora follicularis	{"peremizsrágó zsákosmoly"}
Coleophora frankii	{sédkender-zsákosmoly}
Coleophora fringillella	{"pontusi zsákosmoly"}
Coleophora frischella	{somkóró-zsákosmoly}
Coleophora fuscociliella	{"füstösrojtú zsákosmoly"}
Coleophora fuscocuprella	{"patinafényű zsákosmoly"}
Coleophora galatellae	{"aranyfürtlakó zsákosmoly"}
Coleophora galbulipennella	{szikárszegfű-zsákosmoly}
Coleophora gallipennella	{"csüdfűlakó zsákosmoly"}
Coleophora genistae	{"rekettyelakó zsákosmoly"}
Coleophora glaseri	{Glaser-zsákosmolya}
Coleophora glaucicolella	{"szittyólakó zsákosmoly"}
Coleophora gnaphalii	{"szalmagyopárrágó zsákosmoly"}
Coleophora granulatella	{seprőüröm-zsákosmoly}
Coleophora gryphipennella	{"rózsaaknázó zsákosmoly"}
Coleophora halophilella	{"szikréti zsákosmoly"}
Coleophora hemerobiella	{gyümölcsfalevél-zsákosmoly}
Coleophora hieronella	{"szürkés zsákosmoly"}
Coleophora hydrolapathella	{"lóromrágó zsákosmoly"}
Coleophora ibipennella	{"sárgaerű zsákosmoly"}
Coleophora inulae	{"peremizsaknázó zsákosmoly"}
Coleophora juncicolella	{"hangarágó zsákosmoly"}
Coleophora kasyi	{"bugaci zsákosmoly"}
Coleophora klimeschiella	{"buckajáró zsákosmoly"}
Coleophora kroneella	{körtelevél-zsákosmoly}
Coleophora kuehnella	{"sápadt zsákosmoly"}
Coleophora laricella	{"vörösfenyő zsákosmoly"}
Coleophora limosipennella	{szilfalevél-zsákosmoly}
Coleophora lineolea	{árvacsalán-zsákosmoly}
Coleophora linosyridella	{"őszirózsás zsákosmoly"}
Coleophora linosyris	{aranyfürt-zsákosmoly}
Coleophora lithargyrinella	{"olajsárga zsákosmoly"}
Coleophora lixella	{"fűrágó zsákosmoly"}
Coleophora longicornella	{"sziki zsákosmoly"}
Coleophora lutipennella	{"tölgyrügyrágó zsákosmoly"}
Coleophora magyarica	{"pannon zsákosmoly"}
Coleophora mayrella	{tarlóhere-zsákosmoly}
Coleophora medelichensis	{dárdahere-zsákosmoly}
Coleophora millefolii	{cickafark-zsákosmoly}
Coleophora milvipennis	{"agyagszínű zsákosmoly"}
Coleophora motacillella	{"parajaknázó zsákosmoly"}
Coleophora musculella	{"szegfűrágó zsákosmoly"}
Coleophora narbonensis	{"vértesi zsákosmoly"}
Coleophora niveiciliella	{"budai zsákosmoly"}
Coleophora niveicostella	{kakukkfű-zsákosmoly}
Coleophora niveistrigella	{fátyolvirág-zsákosmoly}
Coleophora nomgona	{"északi zsákosmoly"}
Coleophora nutantella	{"szegfűlakó zsákosmoly"}
Coleophora obtectella	{"mediterrán zsákosmoly"}
Coleophora obviella	{"ritka zsákosmoly"}
Coleophora ochrea	{"ezüstcsíkos zsákosmoly"}
Coleophora ochripennella	{peszterce-zsákosmoly}
Coleophora odorariella	{hangyabogáncs-zsákosmoly}
Coleophora onobrychiella	{"csűdfűaknázó zsákosmoly"}
Coleophora onopordiella	{szamárbogáncs-zsákosmoly}
Coleophora orbitella	{"égeraknázó zsákosmoly"}
Coleophora oriolella	{koronafürt-zsákosmoly}
Coleophora ornatipennella	{"füvönélő zsákosmoly"}
Coleophora paripennella	{"vonalkás zsákosmoly"}
Coleophora partitella	{fehérüröm-zsákosmoly}
Coleophora peisoniella	{szikiürmös-zsákosmoly}
Coleophora pennella	{"vértőrágó zsákosmoly"}
Coleophora peribenanderi	{"bogáncslakó zsákosmoly"}
Coleophora pratella	{"barna zsákosmoly"}
Coleophora preisseckeri	{erdeifenyő-zsákosmoly,"homoki zsákosmoly"}
Coleophora prunifoliae	{"kökényaknázó zsákosmoly"}
Coleophora pseudociconiella	{"sötéterű zsákosmoly"}
Coleophora pseudoditella	{"mezeiürömevő zsákosmoly"}
Coleophora pseudolinosyris	{aranyfürtös-zsákosmoly}
Coleophora pseudorepentis	{"homokháti zsákosmoly"}
Coleophora ptarmicia	{kenyérbélcickafark-zsákosmoly}
Coleophora pulmonariella	{"tüdőfűrágó zsákosmoly"}
Coleophora punctulatella	{bárányparéj-zsákosmoly}
Coleophora ramosella	{"fehércsápú zsákosmoly"}
Coleophora remizella	{"déli zsákosmoly"}
Coleophora riffelensis	{"keskenyszárnyú zsákosmoly"}
Coleophora salicorniae	{sziksófűmag-zsákosmoly}
Coleophora salinella	{sziksófű-zsákosmoly}
Coleophora saponariella	{szappanfűgyökér-zsákosmoly}
Coleophora saturatella	{"sávos zsákosmoly"}
Coleophora saxicolella	{"labodarágó zsákosmoly"}
Coleophora serpylletorum	{"kakukkfűaknázó zsákosmoly"}
Coleophora serratella	{"ligeti zsákosmoly"}
Coleophora siccifolia	{hársfalevél-zsákosmoly}
Coleophora silenella	{habszegfű-zsákosmoly}
Coleophora solitariella	{csillaghúr-zsákosmoly}
Coleophora spiraeella	{gyöngyvessző-zsákosmoly}
Coleophora squalorella	{"mocskos zsákosmoly"}
Coleophora squamosella	{"seprencelakó zsákosmoly"}
Coleophora sternipennella	{parajmag-zsákosmoly}
Coleophora stramentella	{"síksági zsákosmoly"}
Coleophora striatipennella	{"bolhafűrágó zsákosmoly"}
Coleophora striolatella	{"hegyi zsákosmoly"}
Coleophora sylvaticella	{"erdei zsákosmoly"}
Coleophora taeniipennella	{"szittyóaknázó zsákosmoly"}
Coleophora tamesis	{"lápréti zsákosmoly"}
Coleophora tanaceti	{"varádicsaknázó zsákosmoly"}
Coleophora therinella	{"aszatrágó zsákosmoly"}
Coleophora thymi	{"kakukkfűrágó zsákosmoly"}
Coleophora trifariella	{"zanótaknázó zsákosmoly"}
Coleophora trifolii	{lóhere-zsákosmoly}
Coleophora trigeminella	{cseresznyelevél-zsákosmoly}
Coleophora trochilella	{"fészkesviráglakó zsákosmoly"}
Coleophora tyrrhaenica	{"görög zsákosmoly"}
Coleophora unipunctella	{"feketepettyes zsákosmoly"}
Coleophora uralensis	{"urali zsákosmoly"}
Coleophora versurella	{labodatermés-zsákosmoly}
Coleophora vestianella	{"sirálytollú zsákosmoly"}
Coleophora vibicella	{"galajrágó zsákosmoly"}
Coleophora vibicigerella	{mezeiüröm-zsákosmoly}
Coleophora vicinella	{kecskeruta-zsákosmoly}
Coleophora virgatella	{"zsályarágó zsákosmoly"}
Coleophora vulnerariae	{nyúlhere-zsákosmoly}
Coleophora vulpecula	{baltacim-zsákosmoly}
Coleophora wockeella	{"nagy zsákosmoly"}
Coleophora zelleriella	{fűzfalevél-zsákosmoly}
Coleotechnites piceaella	{fenyőtű-borzasmoly}
Colias alfacariensis	{"déli kéneslepke"}
Colias croceus	{"sáfrányszínű kéneslepke"}
Colias erate	{"csángó kéneslepke"}
Colias hyale	{"fakó kéneslepke"}
Colias palaeno	{"tőzeg kéneslepke"}
Colletes cunicularius	{"szőrös selyemméh"}
Colletes hylaeiformis	{"rozsdáshasú selyemméh"}
Colobicus hirtus	{"szegélyes héjbogár"}
Colobochyla salicalis	{"háromsávos apróbagoly"}
Colobopterus erraticus	{"barnáshátú trágyabogár"}
Colocasia coryli	{mogyoróbagoly}
Colostygia olivata	{"olajzöld levélaraszoló"}
Colostygia pectinataria	{"zöld levélaraszoló"}
Colotois pennaria	{"tollascsápú téliaraszoló"}
Colposis mutilatus	{"zöld álormányos"}
Coluber caspius	{"haragos sikló"}
Coluber jugularis	{"haragos sikló"}
Colydium elongatum	{"ösztövér héjbogár"}
Colydium filiforme	{"karcsú héjbogár"}
Combocerus glaber	{"földi tarbogár"}
Comibaena bajularia	{"foltos zöldaraszoló"}
Coniocleonus nigrosuturatus	{"karcsú répabarkó"}
Conisania leineri	{Leiner-homokibagoly}
Conisania luteago	{"tarka homokibagoly"}
Conistra erythrocephala	{"vörösfejű télibagoly"}
Conistra ligula	{"hegyesszárnyú őszibagoly"}
Conistra rubiginea	{"vörhenyes télibagoly"}
Conistra rubiginosa	{"téli bagolylepke"}
Conistra vaccinii	{"változékony télibagoly"}
Conistra veronicae	{veronika-télibagoly}
Conobathra repandana	{"tarkamintás karcsúmoly"}
Conobathra tumidana	{"bordás karcsúmoly"}
Conocephalus dorsalis	{"sarlós kúpfejű szöcske"}
Conocephalus fuscus	{"kis kúpfejű szöcske"}
Conopalpus testaceus	{"feketecsápú komorka"}
Conostethus hungaricus	{"magyar mezeipoloska"}
Copium clavicorne	{"bunkóscsápú csipkéspoloska"}
Coprimorphus scrutator	{"vörös trágyabogár"}
Copris lunaris	{"közönséges holdszarvú-ganéjtúró"}
Copris umbilicatus	{"déli holdszarvú-ganéjtúró"}
Coproceramius cinnamopterus	{"fahéjszínű penészholyva"}
Coproceramius europaeus	{"halvány penészholyva"}
Coproceramius laevanus	{"fémeshátú penészholyva"}
Coproceramius marcidus	{"barnáshátú penészholyva"}
Coproceramius parapicipennis	{"alhavasi penészholyva"}
Coproceramius putridus	{"rozsdaszínű penészholyva"}
Coproceramius setigerus	{"apró penészholyva"}
Coprophilus piceus	{"ürgevendég peceholyva"}
Coprophilus striatulus	{"közönséges peceholyva"}
Coproporus colchicus	{"gömböc fürgeholyva"}
Coprothassa melanaria	{"trágyatúró komposztholyva"}
Coptosoma scutellatum	{bödepoloska}
Coptotriche angusticollella	{"rózsaaknázó sörtésmoly"}
Coptotriche gaunacella	{"kökényaknázó sörtésmoly"}
Coptotriche heinemanni	{"kormos sörtésmoly"}
Coptotriche marginea	{"szederaknázó sörtésmoly"}
Coptotriche szoecsi	{"magyar sörtésmoly"}
Coraebus elatus	{pimpó-díszbogár}
Coraebus florentinus	{"szalagos díszbogár"}
Coraebus rubi	{földiszeder-díszbogár}
Coraebus undatus	{"hullámos díszbogár"}
Coranus subapterus	{"alföldi rablópoloska"}
Cordalia obscura	{"szívhátú karcsúholyva"}
Cordicomus gracilis	{"karcsú fürgebogár"}
Cordicomus sellatus	{"nyerges fürgebogár"}
Cordulegaster bidentata	{hegyiszitakötő}
Cordulia aenea	{"érces szitakötő"}
Coregonus albula	{"törpe maréna"}
Coregonus lavaretus	{"nagy maréna"}
Coreus marginatus	{"közönséges karimáspoloska"}
Coriomeris denticulatus	{"fűrészesvállú karimáspoloska"}
Corixa punctata	{"nagy búvárpoloska"}
Corizus hyoscyami	{"piros karimáspoloska"}
Cornutiplusia circumflexa	{"szemfoltos ezüstbagoly "}
Corvus cornix	{"dolmányos varjú"}
Corylophus cassidoides	{"kopasz pontbogár"}
Coryssomerus capucinus	{székfű-kancsalormányos}
Corythucha ciliata	{platán-csipkéspoloska}
Coscinia cribaria	{"pettyes molyszövő"}
Coscinia striata	{"csíkos molyszövő"}
Cosmardia moritzella	{mécsvirág-sarlósmoly}
Cosmia affinis	{"kis lombbagoly"}
Cosmia diffinis	{szilfa-lombbagoly}
Cosmia pyralina	{tölgyfa-lombbagoly}
Cosmia trapezina	{trapéz-lombbagoly}
Cosmobaris scolopacea	{"pikkelyes báris"}
Cosmopterix lienigiella	{"nádaknázó tündérmoly"}
Cosmopterix orichalcea	{"csenkeszaknázó tündérmoly"}
Cosmopterix scribaiella	{"levantei tündérmoly"}
Cosmopterix zieglerella	{"komlóaknázó tündérmoly"}
Cosmorhoe ocellata	{"szemes galajaraszoló"}
Cossonus cylindricus	{"hengeres szúormányos"}
Cossonus linearis	{korhadék-szúormányos}
Cossonus parallelepipedus	{"szögletes szúormányos"}
Cossus cossus	{fűzfarontólepke}
Costaconvexa polygrammata	{"soksávú araszoló"}
Costignophos pullata	{mészkő-sziklaaraszoló}
Cottus gobio	{"botos kölönte"}
Cottus poecilopus	{"cifra kölönte"}
Coxelus pictus	{rőzse-héjbogár}
Crabro cribrarius	{"hatalmas szitár"}
Crambus ericella	{fenyéres-fűgyökérmoly}
Crambus hamella	{"uzsai fűgyökérmoly"}
Crambus lathoniellus	{"mezei fűgyökérmoly"}
Crambus pascuella	{"lápréti fűgyökérmoly"}
Crambus perlella	{"gyöngyházas fűgyökérmoly"}
Crambus pratella	{"ékes fűgyökérmoly"}
Crambus silvella	{"erdei fűgyökérmoly"}
Crambus uliginosellus	{"mocsári fűgyökérmoly"}
Craniophora ligustri	{fagyal-szigonyosbagoly}
Crassa tinctella	{"okkersárga díszmoly"}
Crassa unitella	{"aranybarna díszmoly"}
Crataraea suturalis	{"szegélyes pudvaholyva"}
Creophilus maxillosus	{"dögész holyva"}
Crepidophorus mutilatus	{"domborúhomlokú pattanó"}
Criocoris crassicornis	{"fekete törpepoloska"}
Criocoris sulcicornis	{"hamvas törpepoloska"}
Crocallis elinguaria	{"sárga sávosaraszoló"}
Crocallis tusciaria	{"barna sávosaraszoló"}
Crocidosema plebejana	{"déli tükrösmoly"}
Crocidura leucodon	{"mezei cickány"}
Crocidura suaveolens	{"keleti cickány"}
Crocothemis erythraea	{"déli szitakötő"}
Crombrugghia distans	{"rozsdabarna tollasmoly"}
Crombrugghia tristis	{"gyászos tollasmoly"}
Crossobela trinotella	{sárgaviolamoly}
Crossocerus quadrimaculatus	{"négyfoltos szitár"}
Crustulina guttata	{"gömböc törpepók"}
Cryphaeus cornutus	{"szarvas gyászbogár"}
Cryphia algae	{"sárgászöld zuzmóbagoly"}
Cryphia domestica	{"zöldesfehér zuzmóbagoly"}
Cryphia ereptricula	{"világostövű zuzmóbagoly"}
Cryphia fraudatricula	{"szürke zuzmóbagoly"}
Cryphia muralis	{"zöld zuzmóbagoly"}
Cryphia raptricula	{"palaszínű zuzmóbagoly"}
Cryphia receptricula	{"zöldesszürke zuzmóbagoly"}
Cryptarcha strigata	{"rajzos nedvbogár"}
Cryptarcha undata	{"félholdas nedvbogár"}
Crypticus quisquilius	{"fürge gyászbogár"}
Cryptobium fracticorne	{"közönséges posványholyva"}
Cryptoblabes bistriga	{"égerlápi karcsúmoly"}
Cryptocheilus egregius	{"díszes útonálló"}
Cryptocheilus versicolor	{"tarka útonálló"}
Cryptococcus fagisuga	{bükk-pajzstetű}
Cryptocochylis conjunctana	{"budai fúrómoly"}
Cryptolestes capensis	{"fokföldi szegélyeslapbogár"}
Cryptolestes corticinus	{"ráncosfejű szegélyeslapbogár"}
Cryptolestes duplicatus	{"kétvonalas szegélyeslapbogár"}
Cryptolestes ferrugineus	{jövevény-szegélyeslapbogár}
Cryptolestes pusilloides	{"apró szegélyeslapbogár"}
Cryptolestes pusillus	{"kis szegélyeslapbogár"}
Cryptolestes spartii	{"fekete szegélyeslapbogár"}
Cryptolestes turcicus	{"török szegélyeslapbogár"}
Cryptophagus cellaris	{"pince penészbogár"}
Cryptophagus lycoperdi	{pöfeteg-penészbogár}
Cryptophagus scanicus	{"közönséges penészbogár"}
Cryptophilus integer	{"halvány szőröstarbogár"}
Cryptophonus melancholicus	{"sötét lomhafutó"}
Cryptophonus tenebrosus	{"gyászos lomhafutó"}
Cryptophonus tenebrosus centralis	{"gyászos lomhafutó"}
Cryptorhynchus lapathi	{"tarka fűzormányos"}
Crypturgus pusillus	{"parányi fenyőszú"}
Ctenicera cuprea	{"rezes legyezőspattanó"}
Ctenicera pectinicornis	{"zöld legyezőspattanó"}
Ctenicera virens	{"sárgás legyezőspattanó"}
Cteniopus sulphureus	{"közönséges kénbogár"}
Cteniopus sulphuripes	{"fekete kénbogár"}
Ctenopharyngodon idella	{amur}
Ctesias serra	{"nagycsápú kéregporva"}
Cucujus cinnaberinus	{skarlátbogár}
Cucullia absinthii	{fehérüröm-csuklyásbagoly}
Cucullia argentea	{"ezüstfoltos csuklyásbagoly"}
Cucullia artemisiae	{feketeüröm-csuklyásbagoly}
Cucullia asteris	{őszirózsa-csuklyásbagoly}
Cucullia balsamitae	{homoki-csuklyásbagoly}
Cucullia chamomillae	{székfű-csuklyásbagoly}
Cucullia fraudatrix	{"keleti csuklyásbagoly"}
Cucullia lactucae	{saláta-csuklyásbagoly}
Cucullia scopariae	{seprősüröm-csuklyásbagoly}
Cucullia tanaceti	{"vonalkás csuklyásbagoly"}
Cucullia umbratica	{"közönséges csuklyásbagoly"}
Cupido alcetas	{"palakék boglárka"}
Cupido argiades	{"kóbor ékesboglárka"}
Cupido decolorata	{"fakó boglárka"}
Cupido minimus	{"fekete törpeboglárka"}
Curculio betulae	{égerzsuzsóka}
Curculio elephas	{gesztenyezsuzsóka}
Curculio glandium	{tölgymakk-zsuzsóka}
Curculio nucum	{mogyorózsuzsóka}
Curculio pellitus	{"makklikasztó zsuzsóka"}
Curculio propinquus	{estizsuzsóka}
Curculio rubidus	{nyírzsuzsóka}
Curculio venosus	{"tarajos zsuzsóka"}
Curculio villosus	{gubacslakó-zsuzsóka}
Curimopsis austriaca	{"osztrák labdacsbogár"}
Curimopsis paleata	{"polyvás labdacsbogár"}
Curimus erinaceus	{sün-labdacsbogár}
Curtimorda bisignata	{"kúpfarkú maróka"}
Cyanapion afer	{"hímjehagyott cickányormányos"}
Cyanapion alcyoneum	{"ritka cickányormányos"}
Cyanapion columbinum	{lednek-cickányormányos}
Cyanapion platalea	{"pedáns cickányormányos"}
Cybocephalus politus	{"sima pajzstetvészbogár"}
Cybocephalus pulchellus	{"csinos pajzstetvészbogár"}
Cybosia mesomella	{"csontszínű molyszövő"}
Cychramus luteus	{"sárga fénybogár"}
Cychramus variegatus	{"tarka fénybogár"}
Cychrus attenuatus	{"sárgalábú cirpelőfutó"}
Cychrus caraboides	{"fekete cirpelőfutó"}
Cyclobacanius soliman	{"török sutabogár"}
Cycloderes pilosulus	{"pikkelyeslábú ormányos"}
Cyclodinus dentatus	{"dunántúli fürgebogár"}
Cyclodinus dentatus transdanubianus	{"dunántúli fürgebogár"}
Cyclodinus humilis	{"sókedvelő fürgebogár"}
Cyclophora albiocellaria	{"fehérfoltos pettyesaraszoló"}
Cyclophora albipunctata	{"hegyi pettyesaraszoló"}
Cyclophora annularia	{"gyűrűs pettyesaraszoló"}
Cyclophora linearia	{"vonalas pettyesaraszoló"}
Cyclophora pendularia	{fűzfa-pettyesaraszoló}
Cyclophora porata	{"körös pettyesaraszoló"}
Cyclophora punctaria	{"sávos pettyesaraszoló"}
Cyclosa conica	{"csúcsos keresztespók"}
Cydalima perspectalis	{"selyemfényű pusztángmoly"}
Cydia amplana	{mogyorómoly}
Cydia conicolana	{fenyőhajtás-tükrösmoly}
Cydia coniferana	{fenyőrákmoly}
Cydia corollana	{rezgőnyár-gubacsmoly}
Cydia cosmophorana	{"gyantarágó tükrösmoly"}
Cydia duplicana	{"barna fenyőkéregmoly"}
Cydia exquisitana	{"déli magrágómoly"}
Cydia fagiglandana	{bükkmakkmoly}
Cydia illutana	{fenyőhajtás-gubacsmoly}
Cydia inquinatana	{juharmag-tükrösmoly}
Cydia leguminana	{"keleti magrágómoly"}
Cydia medicaginis	{lucerna-magrágómoly}
Cydia microgrammana	{iglice-magrágómoly}
Cydia nigricana	{borsómoly}
Cydia oxytropidis	{csajkavirágmoly}
Cydia pactolana	{fenyőkéregmoly}
Cydia pomonella	{almamoly}
Cydia pyrivora	{körtemoly}
Cydia servillana	{kecskefűz-gubacsmoly}
Cydia splendana	{tölgymakkmoly}
Cydia strobilella	{fenyőhajtásmoly}
Cydia succedana	{kerepmagmoly}
Cydnus aterrimus	{"nagy földipoloska"}
Cygnus bewickii	{"kis hattyú"}
Cylindera arenaria	{"parti homokfutrinka"}
Cylindera arenaria viennensis	{"bécsi parti homokfutrinka"}
Cylindera germanica	{"parlagi homokfutrinka"}
Cylindromorphus filum	{"nagyfejű hengerdíszbogár"}
Cylister angustatus	{"keskeny lapossutabogár"}
Cylister linearis	{fenyves-lapossutabogár}
Cylister oblongum	{"hosszúkás lapossutabogár"}
Cyllecoris histrionius	{"vörössávos mezeipoloska"}
Cyllodes ater	{"fekete fénybogár"}
Cymatia coleoptrata	{"simahátú búvárpoloska"}
Cymatophorina diluta	{"őszi pihésszövő "}
Cymindis angularis	{"zömök laposfutó"}
Cymindis axillaris	{"fényes laposfutó"}
Cymindis budensis	{"budai laposfutó"}
Cymindis cingulata	{"kárpáti laposfutó"}
Cymindis humeralis	{"vállfoltos laposfutó"}
Cymindis lineata	{"szegélyes laposfutó"}
Cymindis miliaris	{"kékes laposfutó"}
Cymindis scapularis	{"pusztai laposfutó"}
Cymolomia hartigiana	{"havasi tükrösmoly"}
Cymus claviculus	{"közönséges rétibodobács"}
Cymus glandicolor	{rétibodobács}
Cynaeda dentalis	{"gyakori ciframoly"}
Cynaeda gigantea	{"magyar ciframoly"}
Cynegetis impunctata	{pázsitfűböde}
Cypha longicornis	{"hosszúcsápú gömböcholyva"}
Cypha seminulum	{"apró gömböcholyva"}
Cypha tarsalis	{"közönséges gömböcholyva"}
Cyphea curtula	{"tömzsi kormosholyva"}
Cyphocleonus dealbatus	{"márványos barkó"}
Cyphocleonus dealbatus tigrinus	{"márványos barkó"}
Cyphon coarctatus	{"bordás rétbogár"}
Cyphon laevipennis	{"nádi rétbogár"}
Cyphon ochraceus	{"lápi rétbogár"}
Cyphon padi	{"foltos rétbogár"}
Cyphon palustris	{"mocsári rétbogár"}
Cyphon pubescens	{"szőrös rétbogár"}
Cyphon ruficeps	{"vörösfejű rétbogár"}
Cyphon variabilis	{"változékony rétbogár"}
Cyphostethus tristriatus	{"tarka borókapoloska"}
Cyprinus carpio	{ponty}
Cyrtanaspis phalerata	{"tarka árvabogár"}
Cystobranchus respirans	{"lapos halpióca"}
Cytilus sericeus	{"réti labdacsbogár"}
Dacne bipustulata	{"vállfoltos tarbogár"}
Dacne notata	{"sárgajegyű tarbogár"}
Dacne rufifrons	{"vöröshomlokú tarbogár"}
Dacrila fallax	{"selyemfényű cingárholyva"}
Dadobia immersa	{"lapos bibircsesholyva"}
Dahlica lichenella	{bükkös-szűznemzőmoly}
Dahlica nickerlii	{"hegyi csövesmoly"}
Dahlica triquetrella	{"szűznemző csövesmoly"}
Dalopius marginatus	{"szegélyes pattanó"}
Dalotia coriaria	{"penészkedvelő penészholyva"}
Daphnis nerii	{oleander-szender}
Dapsa denticollis	{"fogasnyakú álböde"}
Dascillus cervinus	{"közönséges ernyősrétbogár"}
Dasycera krueperella	{"sárgafejű díszmoly"}
Dasycera oliviella	{"fekete díszmoly"}
Dasygnypeta velata	{"deres cingárholyva"}
Dasypoda argentata	{"óriás gatyásméh"}
Dasypoda hirtipes	{"gyakori gatyásméh"}
Dasystoma salicella	{"fűzszövő tavaszimoly"}
Dasytes niger	{"szerecsen lágybogár"}
Dasytes plumbeus	{"ólmos lágybogár"}
Datomicra dadopora	{"varas penészholyva"}
Datomicra nigra	{"komposztlakó penészholyva"}
Datomicra sordidula	{"borzas penészholyva"}
Datonychus angulosus	{kenderkefű-ormányos}
Datonychus arquata	{vízipeszérce-ormányos}
Datonychus derennei	{szurokfű-ormányos}
Datonychus melanostictus	{menta-ormányos}
Datonychus transsylvanicus	{"transsylvaniai ormányos"}
Datonychus urticae	{tisztesfű-ormányos}
Daudebardia rufa	{félmeztelencsigafélmeztelencsiga}
Decantha borkhausenii	{"aranyfoltos díszmoly"}
Decticus verrucivorus	{"szemölcsrágó szöcske"}
Deilephila elpenor	{szőlőszender}
Deilephila porcellus	{"piros szender"}
Deileptenia ribeata	{"fenyőrágó faaraszoló"}
Deinopsis erosa	{"közönséges recésholyva"}
Deleaster dichrous	{"kétszínű csermelyholyva"}
Delichon urbicum	{molnárfecske}
Deliphrosoma prolongatum	{"magashegyi felemásholyva"}
Delphax crassicornis	{"foltosszárnyú nábikabóca"}
Deltote bankiana	{"ezüstcsíkos apróbagoly"}
Deltote deceptoria	{"tölgyes apróbagoly"}
Demetrias atricapillus	{"buzogányfoltos nádfutó"}
Demetrias imperialis	{"háromfoltos nádfutó"}
Demetrias monostigma	{"egyfoltos nádfutó"}
Dendrolimus pini	{fenyőpohók}
Dendrophilus punctatus	{"nagy fanedvsutabogár"}
Dendrophilus pygmaeus	{"kis fanedvsutabogár"}
Dendroxena quadrimaculata	{"négypettyes hernyórabló"}
Dendryphantes rudis	{fenyő-ugrópók}
Denisia augustella	{zebramoly}
Denisia similella	{"aranypettyes díszmoly"}
Denisia stipella	{"sárgamintás díszmoly"}
Denisia stroemella	{"kékpettyes díszmoly"}
Denops albofasciatus	{"nagyfejű szúfarkas"}
Denticollis linearis	{"dülledtszemű pattanó"}
Denticollis rubens	{"alhavasi pattanó"}
Deporaus betulae	{nyírfa-levélsodró}
Depressaria absynthiella	{fehérüröm-laposmoly}
Depressaria albipunctella	{"fehérpettyes laposmoly"}
Depressaria artemisiae	{mezeiüröm-laposmoly}
Depressaria badiella	{pasztinák-laposmoly}
Depressaria cervicella	{"vonalkás laposmoly"}
Depressaria chaerophylli	{baraboly-laposmoly}
Depressaria corticinella	{"levantei laposmoly"}
Depressaria daucella	{"köménylakó laposmoly"}
Depressaria depressana	{"fakó laposmoly"}
Depressaria dictamnella	{erősfűmoly}
Depressaria douglasella	{sárgarépamoly}
Depressaria emeritella	{"fehérfejű laposmoly"}
Depressaria heraclei	{medvetalp-laposmoly}
Depressaria marcella	{"csillogó laposmoly"}
Depressaria olerella	{cickafark-laposmoly}
Depressaria pimpinellae	{földitömjén-laposmoly}
Depressaria pulcherrimella	{vadrépa-laposmoly}
Depressaria ultimella	{csomorika-laposmoly}
Deraeocoris lutescens	{"sárgás mezeipoloska"}
Deraeocoris punctulatus	{"hamvas mezeipoloska"}
Deraeocoris ruber	{"vöröses mezeipoloska"}
Deraeocoris rutilus	{"cinóber mezeipoloska"}
Deraeocoris trifasciatus	{"nagy mezeipoloska"}
Derephysia cristata	{"tarajos csipkéspoloska"}
Derephysia foliacea	{"leveles csipkéspoloska"}
Dermestes ater	{"raktári porva"}
Dermestes bicolor	{"fészeklakó porva"}
Dermestes carnivorus	{"amerikai porva"}
Dermestes erichsoni	{"sárgahasú porva"}
Dermestes frischi	{"nyakszegélyes porva"}
Dermestes fuliginosus	{"feketeszőrű porva"}
Dermestes gyllenhali	{"ártéri porva"}
Dermestes intermedius	{"pontusi porva"}
Dermestes laniarius	{"gyászos porva"}
Dermestes lardarius	{szalonnaporva}
Dermestes maculatus	{"tüskés porva"}
Dermestes murinus	{"egérszínű porva"}
Dermestes olivieri	{"rőtszőrű porva"}
Dermestes undulatus	{"márványos porva"}
Dermestoides sanguinicollis	{"hengeres szúfarkas"}
Deroceras agreste	{"kerti meztelencsiga"}
Derodontus macularis	{"közönséges szemecskésbogár"}
Deroxena venosulella	{"csontsárga sztyeppmoly"}
Deuterogonia pudorina	{"rózsavörös díszmoly"}
Devia prospera	{"ártéri pudvaholyva"}
Dexiogyia corticina	{"kéreglakó pudvaholyva"}
Diachromus germanus	{tarkafutó}
Diachrysia chrysitis	{"zöldfényű aranybagoly"}
Diachrysia nadeja	{"szélessávú aranybagoly"}
Diaclina fagi	{"bükklakó gyászbogár"}
Diaclina testudinea	{"teknős gyászbogár"}
Diacrisia sannio	{"vörösszélű medvelepke"}
Diaea dorsata	{"címeres karolópók"}
Dialectica imperialella	{"nadálytő- hólyagosmoly"}
Dialectica soffneri	{"délvidéki hólyagosmoly"}
Dianous coerulescens	{"fémkék szemesholyva"}
Diaperis boleti	{"poszogó taplóbogár"}
Diaphora luctuosa	{"gyászos medvelepke"}
Diaphora mendica	{"felemás medvelepke"}
Diarsia brunnea	{"vörhenyes földibagoly"}
Diarsia mendica	{"díszes földibagoly"}
Diarsia rubi	{"apró földibagoly"}
Diasemia reticularis	{"betűmintás tűzmoly"}
Diaspidiotus alni	{égerfa-kagylóspajzstetű}
Diaspidiotus bavaricus	{áfonya-pajzstetű}
Diaspidiotus gigas	{nyárfa-kagylóspajzstetű}
Diaspidiotus hungaricus	{"magyar pajzstetű"}
Diaspidiotus marani	{"déli körte-pajzstetű"}
Diaspidiotus ostreaeformis	{"sárga alma-pajzstetű"}
Diaspidiotus perniciosus	{"kaliforniai pajzstetű"}
Diaspidiotus pyri	{"sárga körte-pajzstetű"}
Diaspidiotus wuenni	{tölgy-kagylóspajzstetű}
Diaspidiotus zonatus	{bükk-kagylóspajzstetű}
Diastictus vulneratus	{"kétgödrös trágyabogár"}
Diceratura ostrinana	{"bíborsávos fúrómoly"}
Dicerca aenea	{nyárfa-díszbogár}
Dicerca alni	{égerfa-díszbogár}
Dicerca berolinensis	{bükkfa-díszbogár}
Dicerca furcata	{nyírfa-díszbogár}
Dichagyris forcipula	{"barnafoltos földibagoly"}
Dichagyris nigrescens	{"feketés földibagoly"}
Dichagyris signifera	{"pompás földibagoly"}
Dicheirotrichus gustavii	{"mocsári szőrösfutó"}
Dicheirotrichus lacustris	{"tavi szőrösfutó"}
Dicheirotrichus placidus	{"szőrösszemű futó"}
Dicheirotrichus rufithorax	{"közönséges szőrösfutó"}
Dichelia histrionana	{lucfenyő-sodrómoly}
Dichomeris alacella	{"zuzmórágó sarlósmoly"}
Dichomeris barbella	{"szakállas sarlósmoly"}
Dichomeris derasella	{"rozsdás sarlósmoly"}
Dichomeris limosellus	{"lucernarágó sarlósmoly"}
Dichomeris marginella	{"fehérsávos borókamoly"}
Dichomeris rasilella	{"szibériai sarlósmoly"}
Dichomeris ustalella	{"barnásvörös sarlósmoly"}
Dichonia aeruginea	{"hamvas tölgybagoly"}
Dichonia convergens	{"őszi tölgybagoly"}
Dichrorampha acuminatana	{"réti gyökérfúrómoly"}
Dichrorampha aeratana	{"angol gyökérfúrómoly"}
Dichrorampha agilana	{"fürge gyökérfúrómoly"}
Dichrorampha alpinana	{cickafark-gyökérfúrómoly}
Dichrorampha cinerascens	{"szürkés gyökérfúrómoly"}
Dichrorampha cinerosana	{"fahéjszínű gyökérfúrómoly"}
Dichrorampha consortana	{"hegyesszárnyú gyökérfúrómoly"}
Dichrorampha distinctana	{"választójeles gyökérfúrómoly"}
Dichrorampha flavidorsana	{"sárgaszegélyű gyökérfúrómoly"}
Dichrorampha gruneriana	{pipitér-gyökérfúrómoly}
Dichrorampha heegerana	{"barna gyökérfúrómoly"}
Dichrorampha montanana	{"hegyi gyökérfúrómoly"}
Dichrorampha obscuratana	{"homályos gyökérfúrómoly"}
Dichrorampha petiverella	{"közönséges gyökérfúrómoly"}
Dichrorampha plumbana	{"sötét gyökérfúrómoly"}
Dichrorampha podoliensis	{"lengyel gyökérfúrómoly"}
Dichrorampha senectana	{"szürke gyökérfúrómoly"}
Dichrorampha sequana	{"fehérfoltos gyökérfúrómoly"}
Dichrorampha simpliciana	{feketeüröm-gyökérfúrómoly}
Dichrorampha vancouverana	{"aranyszegélyű gyökérfúrómoly"}
Dicranocephalus agilis	{"gyűrűscsápú karimáspoloska"}
Dicranocephalus albipes	{kutyatej-gyűrűspoloska}
Dicranotropis hamata	{"tarajosfejű sarkantyúskabóca"}
Dicronychus cinereus	{"szürke szívespattanó"}
Dicronychus equiseti	{zsurló-szívespattanó}
Dicronychus equisetoides	{"ólmos szívespattanó"}
Dicronychus rubripes	{tölgy-szívespattanó}
Dictyla echii	{kígyószis-csipkéspoloska}
Dictyna arundinacea	{"nádi hamvaspók"}
Dictyna civica	{"városi hamvaspók"}
Dictyna latens	{"fekete hamvaspók"}
Dictyna pusilla	{"pettyes hamvaspók"}
Dictyna szaboi	{"alföldi hamvaspók"}
Dictyna uncinata	{"horgas hamvaspók"}
Dictyna vicina	{"sávos hamvaspók"}
Dictyonota strichnocera	{seprőzanót-csipkéspoloska}
Dictyophara europaea	{"hosszúfejű zöldkabóca"}
Dictyophara pannonica	{"magyar bordásfejű-kabóca"}
Dictyoptera aurora	{"nagy hajnalbogár"}
Dicycla oo	{"sárga tölgybagoly"}
Dicyphus errans	{"karcsúnyakú mezeipoloska"}
Dicyphus globulifer	{"kis karcsúnyakú-mezeipoloska"}
Dieckmanniellus gracilis	{"karcsú füzényormányos"}
Dieckmanniellus helveticus	{"svájci füzényormányos"}
Dieckmanniellus nitidulus	{"fogascombú füzényormányos"}
Digitivalva arnicella	{"árnikarágó tarkamoly"}
Digitivalva granitella	{ökörszemlevél-tarkamoly}
Digitivalva pulicariae	{"homályos tarkamoly"}
Digitivalva reticulella	{szalmagyopár-tarkamoly}
Digitivalva valeriella	{"peremizsevő tarkamoly"}
Dignomus nitidus	{"fényes tolvajbogár"}
Dilacra luteipes	{"selymes hordalékholyva"}
Dilacra vilis	{"selyemfényű hordalékholyva"}
Diloba caeruleocephala	{"őszi kékesbagoly"}
Dimorphopterus doriae	{"alföldi bodobács"}
Dimorphopterus spinolae	{"karcsú bodobács"}
Dinaraea aequata	{"szemölcsös bibircsesholyva"}
Dinaraea angustula	{"közönséges bibircsesholyva"}
Dinaraea linearis	{"füstös bibircsesholyva"}
Dinetus pictus	{"poloskaölő darázs"}
Dinodes decipiens	{"azúr bűzfutó"}
Dinodes decipiens ambiguus	{"azúrkék bűzfutó"}
Dinothenarus fossor	{"tarka holyva"}
Dinothenarus pubescens	{"ezüsthasú holyva"}
Diodesma subterranea	{"tojásdad héjbogár"}
Diodontus minutus	{"kis levéltetűölő"}
Diodontus tristis	{"nagy levéltetűölő"}
Dioryctria abietella	{"fenyőrágó karcsúmoly"}
Dioryctria schuetzeella	{lucfenyő-karcsúmoly}
Dioryctria simplicella	{"fenyőszövő karcsúmoly"}
Dioryctria sylvestrella	{"tobozrágó karcsúmoly"}
Dioxys tridentata	{"háromfogú méh"}
Diplapion confluens	{"fejbenyomott cickányormányos"}
Diplapion detritum	{kamilla-cickányormányos}
Diplapion stolidum	{"lékeltfejű cickányormányos"}
Diplocoelus fagi	{"közönséges álporva"}
Diplodoma adspersella	{"tarka zsákhordólepke"}
Diplodoma laichartingella	{"fehérpettyes zsákhordólepke"}
Diplostyla concolor	{"egyszínú vitorlapók"}
Dipoena braccata	{"fekete törpepók"}
Dipoena melanogaster	{"feketehasú törpepók"}
Dipogon variegatus	{"réslakó útonálló"}
Dipogon vechti	{"vastagcsápú útonálló"}
Dircaea australis	{"déli komorka"}
Dirhinosia cervinella	{"sárhegyi sarlósmoly"}
Dirrhagofarsus attenuatus	{"keskeny tövisnyakúbogár"}
Discoelius zonalis	{"falakó darázs"}
Discus perspectivus	{"hegyi csiga"}
Discus rotundatus	{"tarka csiga"}
Discus ruderatus	{"barna korongcsiga"}
Dismodicus bifrons	{"kéthomlokú pók"}
Dissoleucas niveirostris	{"fehérfarú orrosbogár"}
Diurnea fagella	{"szürke tavaszimoly"}
Diurnea lipsiella	{télimoly}
Divaena haywardi	{Hayward-sárgafűbagoly}
Dixus clypeatus	{"nagyfejű futó"}
Dochmonota clancula	{"zömök hordalékholyva"}
Dochmonota rudiventris	{"ártéri hordalékholyva"}
Dociostaurus brevicollis	{"rövidnyakú sáska"}
Dociostaurus maroccanus	{"marokkói sáska"}
Dodecastichus inflatus	{"barázdásorrú gyalogormányos"}
Dodecastichus mastix	{"lécesorrú gyalogormányos"}
Dolicharthria punctalis	{"hosszúlábú tűzmoly"}
Dolichosoma femorale	{"pusztai karimásbogár"}
Dolichosoma lineare	{"ösztövér lágybogár"}
Dolichovespula adulterina	{"álszász darázs"}
Dolichovespula media	{"szögletesfejű darázs"}
Dolichovespula norwegica	{"norvég darázs"}
Dolichovespula omissa	{"álerdei darázs"}
Dolichovespula saxonica	{"szász darázs"}
Dolichovespula sylvestris	{"erdei darázs"}
Dolichurus corniculus	{"csótányölő darázs"}
Dolichus halensis	{hantfutó}
Dolomedes fimbriatus	{"szegélyes vidrapók"}
Dolomedes plantarius	{"parti vidrapók"}
Doloploca punctulana	{"lonclakó sodrómoly"}
Dolycoris baccarum	{"bogyómászó poloska"}
Domene scabricollis	{"érdeshátú mocsárholyva"}
Donacaula forficella	{"fakó nádfúrómoly"}
Donacaula mucronella	{"ritka nádfúrómoly"}
Donaspastus pannonicus	{"magyar avarmoly"}
Donus zoilus	{lucerna-gubósormányos}
Doratura homophyla	{"kurtaszárnyú kabóca"}
Dorcatoma chrysomelina	{"fénytelen taplóálszú"}
Dorcatoma dresdensis	{"drezdai taplóálszú"}
Dorcatoma flavicornis	{"kerekded taplóálszú"}
Dorcatoma robusta	{"nagy taplóálszú"}
Dorcatoma serra	{"kis taplóálszú"}
Dorcatoma setosella	{"sávosszőrű taplóálszú"}
Dorcus parallelipipedus	{"kis szarvasbogár"}
Dorytomus dorsalis	{"foltos hangormányos"}
Dorytomus edoughensis	{"tavaszi hangormányos"}
Dorytomus filirostris	{"egyenesorrú hangormányos"}
Dorytomus hirtipennis	{"szőrös hangormányos"}
Dorytomus ictor	{"szegélyes hangormányos"}
Dorytomus longimanus	{"kétalakú hangormányos"}
Dorytomus majalis	{"apró hangormányos"}
Dorytomus melanophthalmus	{"sárgaorrú hangormányos"}
Dorytomus minutus	{"parányi hangormányos"}
Dorytomus nebulosus	{fehérnyár-hangormányos}
Dorytomus nordenskioldi	{jövevény-hangormányos}
Dorytomus occalescens	{"szőrpamacsos hangormányos"}
Dorytomus puberulus	{kamasz-hangormányos}
Dorytomus rufatus	{"röt hangormányos"}
Dorytomus salicinus	{"hegyvidéki hangormányos"}
Dorytomus salicis	{fűz-hangormányos}
Dorytomus suratus	{"sárgalábú hangormányos"}
Dorytomus taeniatus	{kecskefűz-hangormányos}
Dorytomus tortrix	{"csupasz hangormányos"}
Dorytomus tremulae	{rezgőnyárfa-hangormányos}
Dorytomus villosulus	{"sárgás hangormányos"}
Doydirhynchus austriacus	{"osztrák áleszelény"}
Drapetes cinctus	{"kétcseppes merevpattanó"}
Drapetisca socialis	{"tüskésszájú vitorlapók"}
Drassodes lapidosus	{"barna kövipók"}
Drasterius bimaculatus	{"változékony pattanó"}
Dreissena polymorpha	{vándorkagyló}
Drepana curvatula	{égerfa-sarlósszövő}
Drepana falcataria	{nyárfa-sarlósszövő}
Drilus concolor	{"fekete csigabogár"}
Drobacia banatica	{"bánáti csiga"}
Dromaeolus barnabita	{"csuhás tövisnyakúbogár"}
Dromius agilis	{"fürge kéregfutó"}
Dromius angustus	{"keskeny kéregfutó"}
Dromius fenestratus	{"kétfoltos kéregfutó"}
Dromius quadraticollis	{"szélesnyakú kéregfutó"}
Dromius quadrimaculatus	{"négyfoltos kéregfutó"}
Dromius schneideri	{"szegélyes kéregfutó"}
Drusilla canaliculata	{"közönséges hangyászholyva"}
Drymonia dodonaea	{"bélyeges púposszövő"}
Drymonia obliterata	{"feketefoltos púposszövő"}
Drymonia querna	{tölgyfa-púposszövő}
Drymonia ruficornis	{cserfa-púposszövő}
Drymonia velitaris	{"hegyi púposszövő"}
Drymus brunneus	{"barna erdeibodobács"}
Drymus sylvaticus	{erdeibodobács}
Dryobotodes eremita	{"változékony tölgybagoly"}
Dryobotodes monochroma	{"szürke tölgybagoly"}
Dryocoetes autographus	{"rozsdavörös fenyőszú"}
Dryocoetes villosus	{gesztenyeszú}
Dryomys nitedula	{"erdei pele"}
Dryophilocoris flavoquadrimaculatus	{"sárgafoltos mezeipoloska"}
Dryophilus pusillus	{"kis fenyvesálszú"}
Dryophthorus corticalis	{korhadékzsuzsok}
Dryops anglicanus	{"angol fülescsápúbogár"}
Dryops auriculatus	{"közönséges fülescsápúbogár"}
Dryops ernesti	{"zömök fülescsápúbogár"}
Dryops griseus	{"szürke fülescsápúbogár"}
Dryops nitidulus	{"fényes fülescsápúbogár"}
Dryops rufipes	{"kis fülescsápúbogár"}
Dryops similaris	{"mocsári fülescsápúbogár"}
Dryops viennensis	{"bécsi fülescsápúbogár"}
Drypta dentata	{atlaszfutó}
Dufourea dentiventris	{csengettyűméh}
Dufourea vulgaris	{"fészkesek méhe"}
Duponchelia fovealis	{"pontusi tűzmoly"}
Duvalius hungaricus	{"magyar vakfutinka"}
Dynaspidiotus abietis	{"fenyőrontó pajzstetű"}
Dypterygia scabriuscula	{sóska-kormosbagoly}
Dysauxes ancilla	{"üvegpettyes álcsüngőlepke"}
Dyschiriodes aeneus	{"közönséges ásófutrinka"}
Dyschiriodes agnatus	{"csillogó ásófutrinka"}
Dyschiriodes bonellii	{"sárgacsápú ásófutrinka"}
Dyschiriodes chalceus	{"nagy ásófutrinka"}
Dyschiriodes chalybaeus	{"púposhomlokú fémes ásófutrinka"}
Dyschiriodes chalybaeus gibbifrons	{"púposhomlokú fémes ásófutrinka"}
Dyschiriodes extensus	{"nyurga ásófutrinka"}
Dyschiriodes globosus	{"apró ásófutrinka"}
Dyschiriodes intermedius	{"érces ásófutrinka"}
Dyschiriodes laeviusculus	{"iszaplakó ásófutrinka"}
Dyschiriodes nitidus	{"közönséges ásófutrinka"}
Dyschiriodes parallelus	{"vöröscsápú karcsú ásófutrinka"}
Dyschiriodes parallelus ruficornis	{"vöröscsápú karcsú ásófutrinka"}
Dyschiriodes politus	{"fényes ásófutrinka"}
Dyschiriodes pusillus	{"hengeres ásófutrinka"}
Dyschiriodes rufipes	{"vöröslábú ásófutrinka"}
Dyschiriodes salinus	{"sziki ásófutrinka"}
Dyschiriodes salinus striatopunctatus	{"sziki ásófutrinka"}
Dyschiriodes strumosus	{"termetes ásófutrinka"}
Dyschiriodes substriatus	{"kis parti ásófutrinka"}
Dyschiriodes substriatus priscus	{"kis parti ásófutrinka"}
Dyschiriodes tristis	{"ékhomlokú ásófutrinka"}
Dyschirius angustatus	{"keskeny ásófutrinka"}
Dyschirius digitatus	{"háromfogú ásófutrinka"}
Dyschirius latipennis	{"sókedvelő ásófutrinka"}
Dyschirius luticola	{"iszaplakó ásófutrinka"}
Dyschirius obscurus	{"recés ásófutrinka"}
Dysdera ninnii	{"pirosfejű fojtópók"}
Dysdera westringi	{Westring-fojtópók}
Dyseriocrania subpurpurella	{"tölgyaknázó ősmoly"}
Dysgonia algira	{"fehér övesbagoly"}
Dyspessa ulula	{fokhagymalepke}
Dysstroma truncata	{"márványos araszoló"}
Dystebenna stephensi	{"tölgyaknázó lándzsásmoly"}
Eana argentana	{"ezüstszárnyú sodrómoly"}
Eana canescana	{fenyves-sodrómoly}
Eana derivana	{barackos-sodrómoly}
Eana incanana	{"hullámos sodrómoly"}
Eana osseana	{"csontszínű sodrómoly"}
Earias clorana	{fűzfa-zöldbagoly}
Earias vernana	{nyárfa-zöldbagoly}
Eblisia minor	{"kéreglakó lapossutabogár"}
Ebrechtella tricuspidata	{"alakoskodó karolópók"}
Ebulea crocealis	{"okkerszárnyú tűzmoly "}
Ebulea testacealis	{"rozsdás tűzmoly "}
Eccopisa effractella	{körtelevél-karcsúmoly}
Echinodera valida	{"erős zártormányúbogár"}
Ecliptopera silaceata	{"barna tarkaaraszoló"}
Ecpyrrhorrhoe rubiginalis	{"rozsdavörös tűzmoly"}
Ectemnius cephalotes	{"négycsíkos szitár"}
Ectemnius continuus	{"gyakori szitár"}
Ectemnius dives	{"aranyarcú szitár"}
Ectemnius fossorius	{óriásszitár}
Ectoedemia agrimoniae	{párlófű-törpemoly}
Ectoedemia albifasciella	{"fehérsávos törpemoly"}
Ectoedemia angulifasciella	{"hajlottsávú törpemoly"}
Ectoedemia arcuatella	{eperlevél-törpemoly}
Ectoedemia argyropeza	{fehérnyár-törpemoly}
Ectoedemia atricollis	{naspolyafa-törpemoly}
Ectoedemia atrifrontella	{tölgykéreg-törpemoly}
Ectoedemia caradjai	{"balkáni törpemoly"}
Ectoedemia cerris	{"cserfarágó törpemoly"}
Ectoedemia contorta	{"szilrágó törpemoly"}
Ectoedemia decentella	{juhartermés-törpemoly}
Ectoedemia gilvipennella	{csertölgylevél-törpemoly}
Ectoedemia hannoverella	{nyárfalevél-törpemoly}
Ectoedemia heringi	{"okkerképű törpemoly"}
Ectoedemia hexapetalae	{"budai törpemoly"}
Ectoedemia intimella	{kecskefűz-törpemoly}
Ectoedemia klimeschi	{"osztrák törpemoly"}
Ectoedemia liebwerdella	{bükkfakéreg-törpemoly}
Ectoedemia liechtensteini	{cserlevél-törpemoly}
Ectoedemia longicaudella	{tölgyfakéreg-törpemoly}
Ectoedemia louisella	{"juharmagrágó törpemoly"}
Ectoedemia mahalebella	{törökmeggy-törpemoly}
Ectoedemia occultella	{nyíres-törpemoly}
Ectoedemia preisseckeri	{"nyírlevélaknázó törpemoly"}
Ectoedemia rubivora	{szederlevél-törpemoly}
Ectoedemia rufifrontella	{molyhostölgy-törpemoly}
Ectoedemia septembrella	{orbáncfű-törpemoly}
Ectoedemia sericopeza	{juharmag-törpemoly}
Ectoedemia spinosella	{"fekete törpemoly"}
Ectoedemia spiraeae	{gyöngyvessző-törpemoly}
Ectoedemia subbimaculella	{"sárgapettyes törpemoly"}
Ectoedemia turbidella	{szürkenyár-törpemoly}
Ectohomoeosoma kasyellum	{"pannon karcsúmoly"}
Ectropis crepuscularia	{"homályos faaraszoló"}
Edwardsiana rosae	{rózsakabóca}
Egira conspicillaris	{"változó szürkebagoly"}
Eidophasia messingiella	{kakukktormamoly}
Eidophasia syenitella	{"ritka tarkamoly"}
Eilema complana	{"közönséges zuzmószövő"}
Eilema griseola	{"szürke zuzmószövő"}
Eilema lurideola	{"fakó zuzmószövő"}
Eilema lutarella	{"narancsszínű zuzmószövő"}
Eilema pygmaeola	{"halvány zuzmószövő"}
Eilema pygmaeola pallifrons	{"halvány zuzmószövő"}
Eilema sororcula	{"sárga zuzmószövő"}
Eilicrinia cordiaria	{"fehér fűzaraszoló"}
Eilicrinia trinotata	{"háromjegyű araszoló"}
Elachista adscitella	{"öves fűaknázómoly"}
Elachista albidella	{gyapjúsásmoly}
Elachista alpinella	{"havasi fűaknázómoly"}
Elachista anserinella	{"sárgafoltos fűaknázómoly"}
Elachista apicipunctella	{"északi fűaknázómoly"}
Elachista argentella	{"ezüstfehér fűaknázómoly"}
Elachista atricomella	{ebírmoly}
Elachista bedellella	{"csontszínű perjemoly"}
Elachista biatomella	{deressásmoly}
Elachista bisulcella	{"szalagos fűaknázómoly"}
Elachista canapennella	{pázsitfűmoly}
Elachista chrysodesmella	{"Zeller, 1850"}
Elachista cingillella	{"fehérsávos fűaknázómoly"}
Elachista collitella	{fényperjemoly}
Elachista contaminatella	{szálkaperjemoly}
Elachista disemiella	{"kétpontú fűaknázómoly"}
Elachista dispilella	{"csontfehér fűaknázómoly"}
Elachista dispunctella	{juhcsenkeszmoly}
Elachista elegans	{"csinos fűaknázómoly"}
Elachista freyerella	{"kormos fűaknázómoly"}
Elachista gangabella	{"sárgasávos fűaknázómoly"}
Elachista gleichenella	{perjeszittyómoly}
Elachista gormella	{"skandináv fűaknázómoly"}
Elachista griseella	{"szürke fűaknázómoly"}
Elachista hedemanni	{törpe-sásaknázómoly}
Elachista heringi	{árvalányhajmoly}
Elachista herrichii	{fényperje-aknázómoly}
Elachista humilis	{sédbúzamoly}
Elachista juliensis	{szőrössásmoly}
Elachista kalki	{"fényes fűaknázómoly"}
Elachista kilmunella	{"bugaci fűaknázómoly"}
Elachista klimeschiella	{"dunántúli fűaknázómoly"}
Elachista luticomella	{"sárgafejű fűaknázómoly"}
Elachista martinii	{törpesás-aknázómoly}
Elachista nitidulella	{"alföldi fűaknázómoly"}
Elachista poae	{harmatkásamoly}
Elachista pollinariella	{aranyzabmoly}
Elachista pollutella	{"balkáni fűaknázómoly"}
Elachista pomerana	{"kerti fűaknázómoly"}
Elachista pullicomella	{zabfűmoly}
Elachista quadripunctella	{"négyfoltos fűaknázómoly"}
Elachista rudectella	{komócsinmoly}
Elachista rufocinerea	{selyemperjemoly}
Elachista scirpi	{zsiókamoly}
Elachista serricornis	{"északi erdeisásmoly"}
Elachista spumella	{"homoki fűaknázómoly"}
Elachista squamosella	{"sárgásfehér fűaknázómoly"}
Elachista stabilella	{zabmoly}
Elachista subalbidella	{kékperjemoly}
Elachista subnigrella	{"füstös fűaknázómoly"}
Elachista subocellea	{"szemes fűaknázómoly"}
Elachista szocsi	{"magyar fűaknázómoly"}
Elachista triatomea	{csenkeszmoly}
Elachista triseriatella	{"háromsávos fűaknázómoly"}
Elachista unifasciella	{"egysávos fűaknázómoly"}
Elachista utonella	{rétisás-aknázómoly}
Elaphria venustula	{"tarka törpebagoly"}
Elaphrus aureus	{"aranyos iszapfutó"}
Elaphrus cupreus	{"rezes iszapfutó"}
Elaphrus riparius	{"közönséges iszapfutó"}
Elaphrus uliginosus	{"mocsári iszapfutó"}
Elaphrus ullrichii	{"smaragdzöld iszapfutó"}
Elasmostethus interstinctus	{"pirostartka címerespoloska"}
Elasmucha ferrugata	{"rozsdás címerespoloska"}
Elasmucha grisea	{"változó címerespoloska"}
Elater ferrugineus	{fűzfapattanó}
Elatobia fuliginosella	{fenyves-korhadékmoly}
Electrophaes corylata	{mogyoróaraszoló}
Eledona agricola	{"kis taplóbogár"}
Eledonoprius armatus	{"érdes taplóbogár"}
Elegia fallax	{"füstös karcsúmoly"}
Elegia similella	{"tölgyjáró karcsúmoly"}
Eliomys quercinus	{"kerti pele"}
Ellescus bipunctatus	{kétpöttyös-fűzormányos}
Ellescus infirmus	{"tarka fűzormányos"}
Ellescus scanicus	{"sápadt nyárfa-ormányos"}
Elmis aenea	{"fémes karmosbogár"}
Elmis latreillei	{"vastagbordás karmosbogár"}
Elmis maugetii	{"bronzos karmosbogár"}
Elmis obscura	{"sötét karmosbogár"}
Elmis rioloides	{"kis karmosbogár"}
Elodes hausmanni	{"foltosnyakú rétbogár"}
Elodes johni	{"német rétbogá"}
Elodes marginata	{"szegélyes rétbogár"}
Elodes minuta	{"kis rétbogár"}
Elophila nymphaeata	{"tarka vízimoly"}
Elophila rivulalis	{"déli vízimoly"}
Ematheudes punctella	{"kúposfejű karcsúmoly"}
Ematurga atomaria	{"barna rétiaraszoló"}
Emberiza calandra	{sordély}
Emblethis ciliatus	{"alföldi gyepibodobács"}
Emblethis griseus	{"homoki gyepibodobács"}
Emblethis verbasci	{gyepibodobács}
Emelyanoviana mollicula	{"piszkoszöld kabóca"}
Emmelia trabealis	{zebrabagoly}
Emmelina argoteles	{"illír tollasmoly"}
Emmelina monodactyla	{"közönséges tollasmoly"}
Emphanes azurescens	{"ékhomlokú gyorsfutó"}
Emphanes latiplaga	{"fakóvállú gyorsfutó"}
Emphanes minimus	{"lakkfekete gyorsfutó"}
Emphanes tenellus	{"apró gyorsfutó"}
Empicoris culiciformis	{"szúnyogképű rablópoloska"}
Empicoris vagabundus	{"kóbor gyilkospoloska"}
Emus hirtus	{"bundás holyva"}
Enallagma cyathigerum	{"kéksávos légivadász"}
Enalodroma hepatica	{"bütykös humuszholyva"}
Ena montana	{"hegyi csavarcsiga"}
Enargia abluta	{"fakó nyárfabagoly"}
Enargia paleacea	{"sárga nyárfabagoly"}
Enarmonia formosana	{kéregmoly}
Encephalus complicans	{"gömbölyödő taplóholyva"}
Endecatomus reticulatus	{"recés álcsuklyásszú"}
Endomia tenuicollis	{"keskenynyakú fürgebogár"}
Endomychus coccineus	{"négypettyes álböde"}
Endophloeus marcovichianus	{"fogasszélű héjbogár"}
Endothenia gentianaeana	{mácsonya-tükrösmoly}
Endothenia lapideana	{gyűszűvirág-tükrösmoly}
Endothenia marginana	{"szegélyes tükrösmoly"}
Endothenia nigricostana	{tisztesfű-tükrösmoly}
Endothenia oblongana	{héjakút-tükrösmoly}
Endothenia quadrimaculana	{"mocsári tükrösmoly"}
Endothenia sororiana	{macskahere-tükrösmoly}
Endothenia ustulana	{"sötét tükrösmoly"}
Endotricha flammealis	{"tüzesszárnyú fényilonca"}
Endrosis sarcitrella	{kamramoly}
Enedreytes hilaris	{"kétdudoros orrosbogár"}
Enedreytes sepicola	{"hatdudoros orrosbogár"}
Enicopus hirtus	{"bundás lágybogár"}
Ennearthron cornutum	{"aranyszőrű taplószú"}
Ennearthron pruinosulum	{hárs-taplószú}
Ennomos alniaria	{égerfa-levélaraszoló}
Ennomos autumnaria	{"őszi levélaraszoló"}
Ennomos erosaria	{hársfa-levélaraszoló}
Ennomos fuscantaria	{kőris-levélaraszoló}
Ennomos quercinaria	{molyhostölgy-levélaraszoló}
Enoplognatha ovata	{"vonalas törpepók"}
Enoplops scapha	{"feketés karimáspoloska"}
Enoplopus dentipes	{"cirpelő gyászbogár"}
Entephria caesiata	{"szürke hegyiaraszoló"}
Enteucha acetosae	{mezeisóska-törpemoly}
Eosolenobia manni	{"északi csövesmoly"}
Epacromius coerulipes	{"pannon sáska"}
Epacromius tergestinus	{"pontusi sáska"}
Epagoge grotiana	{"bokorerdei sodrómoly"}
Epaphius secalis	{borostyánfutó}
Epascestria pustulalis	{"atracélrágó tűzmoly"}
Epauloecus unicolor	{"zömök tolvajbogár"}
Epeolus variegatus	{"csupasz méh"}
Epermenia aequidentellus	{angyalgyökér-íveltmoly}
Epermenia chaerophyllella	{"barabolysodró íveltmoly"}
Epermenia illigerella	{podagrafű-íveltmoly}
Epermenia insecurellus	{"ólomszínű íveltmoly"}
Epermenia petrusellus	{"buglyoskocsordfonó íveltmoly"}
Epermenia pontificella	{"tarka íveltmoly"}
Epermenia strictellus	{"pirosszárnyú íveltmoly"}
Ephemera lineata	{"vonalas kérész"}
Ephestia elutella	{készletmoly}
Ephestia kuehniella	{lisztmoly}
Ephestia unicolorella woodiella	{"szőlőrágó karcsúmoly"}
Ephestia welseriella	{"levantei karcsúmoly"}
Ephippiger ephippiger	{"nyerges szöcske"}
Ephysteris inustella	{"buckalakó sarlósmoly"}
Ephysteris promptella	{"őszi sarlósmoly"}
Epibactra sareptana	{"keleti szittyómoly"}
Epiblema cnicicolana	{bolhafű-tükrösmoly}
Epiblema foenella	{"kampósfoltú tükrösmoly"}
Epiblema grandaevana	{acsalapu-tükrösmoly}
Epiblema graphana	{cickafark-tükrösmoly}
Epiblema hepaticana	{"májfoltos tükrösmoly"}
Epiblema junctana	{"turjáni tükrösmoly"}
Epiblema mendiculana	{"hegyi tükrösmoly"}
Epiblema obscurana	{peremizsszár-tükrösmoly}
Epiblema scutulana	{"réti tükrösmoly"}
Epiblema similana	{"nyírlakó tükrösmoly"}
Epiblema sticticana	{"fehérpettyes tükrösmoly"}
Epiblema turbidana	{"acsalapurágó tükrösmoly"}
Epicallima bruandella	{"francia díszmoly"}
Epicallima formosella	{"kéreglakó díszmoly"}
Epicauta rufidorsum	{hollóbogár}
Epichnopterix kovacsi	{"magyar zsákhordólepke"}
Epichnopterix plumella	{"ólomszürke zsákhordólepke"}
Epidiaspis leperii	{"piros körte-pajzstetű"}
Epierus comptus	{"tojásdad erdeisutabogár"}
Epilecta linogrisea	{kankalin-fűbagoly}
Epimecia ustula	{ördögszem-apróbagoly}
Epinotia abbreviana	{juharlevél-tükrösmoly}
Epinotia bilunana	{"félholdas tükrösmoly"}
Epinotia brunnichana	{"nagyfoltú tükrösmoly"}
Epinotia cruciana	{szeder-tükrösmoly}
Epinotia demarniana	{"barkaszövő tükrösmoly"}
Epinotia festivana	{"barnatövű tükrösmoly"}
Epinotia granitana	{lucfenyőkéreg-tükrösmoly}
Epinotia kochiana	{"zsályaszövő tükrösmoly"}
Epinotia maculana	{"fekete tükrösmoly"}
Epinotia nanana	{fenyősövény-tükrösmoly}
Epinotia nigricana	{jegenyefenyő-tükrösmoly}
Epinotia nisella	{nyárfabarka-tükrösmoly}
Epinotia pusillana	{"apró fenyőtűmoly"}
Epinotia pygmaeana	{"fenyveslakó tükrösmoly"}
Epinotia ramella	{"ékfoltos tükrösmoly"}
Epinotia rubiginosana	{erdeifenyő-tükrösmoly}
Epinotia signatana	{májusfa-tükrösmoly}
Epinotia solandriana	{égerlevél-tükrösmoly}
Epinotia sordidana	{"égersodró tükrösmoly"}
Epinotia subocellana	{"fűzlevélsodró tükrösmoly"}
Epinotia tedella	{fenyő-tükrösmoly}
Epinotia tenerana	{nyírbarka-tükrösmoly}
Epinotia tetraquetrana	{"nyírfúró tükrösmoly"}
Epinotia thapsiana	{édeskömény-tükrösmoly}
Epinotia trigonella	{nyírfalevél-tükrösmoly}
Epione repandaria	{"narancsszínű csücskösaraszoló"}
Epione vespertaria	{"vörhenyes csücskösaraszoló"}
Epirranthis diversata	{rezgőnyár-araszoló}
Epirrhoe alternata	{galaj-tarkaraszoló}
Epirrhoe galiata	{"sávos tarkaaraszoló"}
Epirrhoe hastulata	{"nyíljegyes tarkaaraszoló"}
Epirrhoe rivata	{"ékfoltos tarkaaraszoló"}
Epirrhoe tristata	{"gyászos tarkaaraszoló"}
Epirrita autumnata	{"ezüstős ősziaraszoló"}
Epirrita christyi	{"kis ősziaraszoló"}
Epischnia prodromella	{"imolarágó karcsúmoly"}
Episcythrastis tetricella	{"tavaszi karcsúmoly"}
Episema glaucina	{"fogasjegyű liliombagoly"}
Episema tersa	{"hármasjegyű liliombagoly"}
Episernus granulatus	{"szemecskés álszú"}
Episinus truncatus	{"csonka törpepók"}
Epitheca bimaculata	{"kétfoltú szitakötő"}
Epomis dejeanii	{"zöldeskék bűzfutó"}
Eptesicus nilssoni	{"északi késeidenevér"}
Eptesicus serotinus	{"közönséges késeidenevér"}
Epuraea aestiva	{"nyári fénybogár"}
Epuraea angustula	{"párhuzamos fénybogár"}
Epuraea biguttata	{"kétcseppes fénybogár"}
Epuraea distincta	{"hullámosnyakú fénybogár"}
Epuraea fageticola	{"gesztenyebarna fénybogár"}
Epuraea fuscicollis	{"sötétnyakú fénybogár"}
Epuraea guttata	{"tízpettyes fénybogár"}
Epuraea limbata	{"szegélyes fénybogár"}
Epuraea longula	{"hosszúkás fénybogár"}
Epuraea melanocephala	{"feketefejű fénybogár"}
Epuraea melina	{"domború fénybogár"}
Epuraea neglecta	{"lapos fénybogár"}
Epuraea pallescens	{"virágjáró fénybogár"}
Epuraea pygmaea	{"szelíd fénybogár"}
Epuraea rufomarginata	{"vörösszegélyű fénybogár"}
Epuraea silacea	{bükk-fénybogár}
Epuraea terminalis	{"sötétlő fénybogár"}
Epuraea unicolor	{"széleslábú fénybogár"}
Epuraea variegata	{"feketefoltos fénybogár"}
Erannis ankeraria	{Anker-araszoló}
Erannis defoliaria	{"nagy téliaraszoló"}
Erebia aethiops	{"közönséges szerecsenlepke"}
Erebia ligea	{"fehércsíkú szerecsenlepke"}
Erebia medusa	{"tavaszi szerecsenlepke"}
Eremobia ochroleuca	{"fehérmintás fűbagoly"}
Eremocoris fenestratus	{avarbodobács}
Eremocoris podagricus	{"gyakori avarbodobács"}
Erichsonius cinerascens	{"hamuszínű ganajholyva"}
Erichsonius signaticornis	{"patakparti ganajholyva"}
Erigone dentipalpis	{"hosszúkezű pók"}
Erigonoplus globipes	{"dagadtlábú pók"}
Erinaceus concolor roumanicus	{"keleti sün"}
Eriocrania semipurpurella	{"nyírlakó ősmoly"}
Eriocrania sparrmannella	{"nyíraknázó ősmoly"}
Eriogaster rimicola	{"vörhenyes gyapjasszövő"}
Eriopeltis festucae	{tarack-teknőspajzstetű}
Eriopsela quadrana	{ördögfű-tükrösmoly}
Ernobius abietinus	{luc-tobozrágóálszú}
Ernobius abietis	{jegenyefenyő-tobozrágóálszú}
Ernobius angusticollis	{"keskenynyakú tobozrágóálszú"}
Ernobius longicornis	{"hosszúcsápú tobozrágóálszú"}
Ernobius mollis	{"közönséges tobozrágóálszú"}
Ernobius nigrinus	{"fekete tobozrágóálszú"}
Ernobius pini	{"kis tobozrágóálszú"}
Ernoporicus caucasicus	{"kaukázusi szú"}
Ernoporicus fagi	{bükkszú}
Ernoporus tilliae	{hársszú}
Ero aphana	{"nagy bütyköspók"}
Ero furcata	{"kis bütyköspók"}
Ero tuberculata	{"közönséges bütyköspókközönséges bütyköspók"}
Erpobdella nigricollis	{"feketenyakú nadály"}
Erpobdella octoculata	{"nyolcszemű nadály"}
Erpobdella vilnensis	{"sávos pióca"}
Errastunus ocellaris	{"réti kabóca"}
Erynnis tages	{cigány-busalepke}
Erythromma najas	{"fürge légivadász"}
Erythromma viridulum	{"zöld légivadász"}
Esolus angustatus	{"keskeny karmosbogár"}
Esolus parallelepipedus	{"párhuzamos karmosbogár"}
Esox lucius	{csuka}
Esymus merdarius	{"szegélyes trágyabogár"}
Esymus pusillus	{"apró trágyabogár"}
Eteobalea albiapicella	{gubóvirág-tündérmoly}
Eteobalea anonymella	{"névtelen tündérmoly"}
Eteobalea intermediella	{"tarka tündérmoly"}
Eteobalea serratella	{gyujtoványfű-tündérmoly}
Eteobalea tririvella	{"homoki tündérmoly"}
Ethelcus denticulatus	{pipacsgyötrő-ormányos}
Ethmia bipunctella	{"kétpettyes feketemoly"}
Ethmia candidella	{"őszi feketemoly"}
Ethmia dodecea	{"tízpettyes feketemoly"}
Ethmia fumidella	{"korai feketemoly"}
Ethmia haemorrhoidella	{"hullámos feketemoly"}
Ethmia iranella	{"levantei feketemoly"}
Ethmia pusiella	{"díszes feketemoly"}
Ethmia quadrillella	{"gyászos feketemoly"}
Ethmia terminella	{"hatpettyes feketemoly"}
Etiella zinckenella	{akácmoly}
Euaesthetus bipunctatus	{"ráncos tapogatósholyva"}
Euaesthetus laeviusculus	{"rozsdaszínű tapogatósholyva"}
Euaesthetus ruficapillus	{"vörhenyes tapogatósholyva"}
Euaesthetus superlatus	{"aprócska tapogatósholyva"}
Eublemma amoena	{bogáncsbagoly}
Eublemma minutata	{"fehéres törpebagoly"}
Eublemma ostrina	{szalmagyopár-bíborbagoly}
Eublemma panonica	{"magyar gyopárbagoly"}
Eublemma parva	{peremizs-törpebagoly}
Eublemma purpurina	{"bíbor gyopárbagoly"}
Eublemma rosea	{"rózsaszínű törpebagoly"}
Eubrachium pusillum	{"törpe vágottsutabogár"}
Eubria palustris	{"európai vízifillér"}
Eubrychius velutus	{úszóhínár-ormányos}
Eucarphia vinetella	{"pontusi karcsúmoly"}
Eucarta amethystina	{ametiszbagoly}
Eucarta virgo	{"mocsári bíborbagoly"}
Eucera clypeata	{lucernaméh}
Eucera crysopyga	{"rozsdás nagybajszúméh"}
Eucera iterrupta	{"délkeleti nagybajszúméh"}
Eucera tuberculata	{"gyakori nagybajszúméh"}
Euchalcia consona	{"sárgás aranybagoly "}
Euchalcia modestoides	{"zöld aranybagoly"}
Euchoeca nebulata	{"szürke ősziaraszoló"}
Euchorthippus declivus	{"rövidszárnyú rétisáska"}
Euchorthippus pulvinatus	{"karcsú rétisáska"}
Euchrognophos variegata	{"tarka sziklaaraszoló"}
Euchromius bella	{"cifra mozaikmoly"}
Euchromius ocellea	{"ezüstcsíkos mozaikmoly"}
Eucinetus haemorrhoidalis	{"közönséges álmaróka"}
Euclidia glyphica	{"közönséges nappalibagoly"}
Eucnemis capucina	{"csuklyás tövisnyakúbogár"}
Eucoeliodes mirabilis	{"csíkos kecskerágó-ormányos"}
Eucosma aemulana	{aranyvessző-tükrösmoly}
Eucosma albidulana	{zsoltína-tükrösmoly}
Eucosma aspidiscana	{aranyfürt-tükrösmoly}
Eucosma balatonana	{"dunántúli tükrösmoly"}
Eucosma campoliliana	{"feketefoltos tükrösmoly"}
Eucosma cana	{aszatvirág-tükrösmoly}
Eucosma conformana	{"fémsávos tükrösmoly"}
Eucosma conterminana	{"saláta tükrösmoly"}
Eucosma cumulana	{peremizsvirág-tükrösmoly}
Eucosma fervidana	{"tüzes tükrösmoly"}
Eucosma hohenwartiana	{bogáncsvirág-tükrösmoly}
Eucosma lacteana	{"fehér tükrösmoly"}
Eucosma lugubrana	{"hagymarágó tükrösmoly"}
Eucosma messingiana	{"parlagi tükrösmoly"}
Eucosma metzneriana	{ürömgyökér-tükrösmoly}
Eucosma obumbratana	{"nádi tükrösmoly"}
Eucosma pupillana	{"aprószemű tükrösmoly"}
Eucosma tripoliana	{"sziki tükrösmoly"}
Eucosma tundrana	{tundramoly}
Eucosma wimmerana	{ürömgubacs-tükrösmoly}
Eucosmomorpha albersana	{hóbogyómoly}
Eucrostes indigenata	{"homoki zöldaraszoló"}
Eudarcia pagenstecherella	{"fali zuzmómoly"}
Eudemis porphyrana	{porfírmoly}
Eudemis profundana	{"fehérfoltos tükrösmoly"}
Eudiplister planulus	{"pusztai sutabogár"}
Eudolus quadriguttatus	{"négycseppes trágyabogár"}
Eudonia lacustrata	{"fehér mohailonca"}
Eudonia laetella	{"cifra mohailonca"}
Eudonia mercurella	{"törpe zuzmóilonca"}
Eudonia murana	{"fali zuzmóilonca"}
Eudonia sudetica	{"karcsú mohailonca"}
Eudonia truncicolella	{"szürke mohailonca"}
Eudontomyzon danfordi	{"tiszai ingola"}
Eudontomyzon mariae	{"dunai ingola"}
Euglenes oculatus	{"nagyszemű korhóbogár"}
Euglenes pygmaeus	{"őszes korhóbogár"}
Eugnorisma depuncta	{"őszi fűbagoly"}
Eugnosta lathoniana	{"ezüstfoltos fúrómoly"}
Eugnosta magnificana	{"ezüsttükrös fúrómoly"}
Eugraphe sigma	{"tarka fűbagoly"}
Euheptaulacus porcellus	{malacka-trágyabogár}
Euheptaulacus sus	{"bordás trágyabogár"}
Euheptaulacus villosus	{"sörtés trágyabgár"}
Euhyponomeuta stannella	{varjúháj-pókhálósmoly}
Euides speciosa	{"nádi kabóca"}
Eulamprotes atrella	{orbáncfű-sarlósmoly}
Eulamprotes superbella	{"ezüstpontos sarlósmoly"}
Eulamprotes unicolorella	{"zöldesszürke sarlósmoly"}
Eulamprotes wilkella	{"ezüstsávos sarlósmoly"}
Eulecanium caraganae	{"sárga akácpajzstetű"}
Eulecanium ciliatum	{dió-teknőspajzstetű}
Eulecanium douglasi	{ribiszke-teknőspajzstetű}
Eulecanium franconicum	{csarab-teknőspajzstetű}
Eulecanium sericeum	{jegenyefenyő-teknőspajzstetű}
Eulecanium tiliae	{"vadgesztenye pajzstetű"}
Eulia ministrana	{"aranybarna sodrómoly"}
Eulithis mellinata	{"sárga tarkaaraszoló"}
Eulithis populata	{"hegyi tarkaaraszoló"}
Eulithis prunata	{kökény-tarkaaraszoló}
Eumannia lepraria	{"sávos zuzmóaraszoló"}
Eumodicogryllus bordigalensis	{"bordói tücsök"}
Euodynerus dantici	{"fűrészpajzsú kürtősdarázs"}
Euomphalia strigella	{bokorcsiga}
Euoniticellus fulvus	{"foltos trágyatúró"}
Euonthophagus amyntas	{"egyléces trágyatúró"}
Euonthophagus amyntas alces	{"egyléces trágyatúró"}
Euophrys frontalis	{"háromszöges ugrópók"}
Euorodalus paracoenosus	{"széleslábú trágyabogár"}
Eupelix cuspidata	{"lándzsafejű kabóca"}
Euphyia unangulata	{"fogassávú tarkaaraszoló"}
Eupithecia absinthiata	{üröm-törpearaszoló}
Eupithecia actaeata	{békabogyó-törpearaszoló}
Eupithecia assimilata	{komló-törpearaszoló}
Eupithecia centaureata	{búzavirág-törpearaszoló}
Eupithecia denticulata	{"sárgásszürke törpearaszoló"}
Eupithecia gueneata	{"téglavörös törpearaszoló"}
Eupithecia icterata	{"homokszínű törpearaszoló"}
Eupithecia innotata	{mezeiüröm-törpearaszoló}
Eupithecia lanceata	{fenyő-törpearaszoló}
Eupithecia linariata	{"vonalas törpearaszoló"}
Eupithecia nanata	{csarabos-törpearaszoló}
Eupithecia satyrata	{"virágrágó törpearaszoló"}
Eupithecia semigraphata	{hangyabogáncs-törpearaszoló}
Eupithecia venosata	{mécsvirág-törpearaszoló}
Eupithecia vulgata	{"közönséges törpearaszoló"}
Eupleurus subterraneus	{"barázdás trágyabogár"}
Euplexia lucipara	{hajnalfénybagoly}
Euplocamus anthracinalis	{"fésűscsápú korhadékmoly"}
Eupoecilia ambiguella	{"nyerges szőlőmoly"}
Eupoecilia angustana	{"közönséges virágfúrómoly"}
Eupoecilia sanguisorbana	{vérfű-fúrómoly}
Euproctis chrysorrhoea	{"aranyfarú szövő"}
Euproctis similis	{"sárgafarú szövő"}
Eupsilia transversa	{"holdacskás télibagoly"}
Eupteryx atropunctata	{"feketepontos kabóca"}
Eupteryx stachydearum	{tisztesfűkabóca}
Eupteryx urticae	{csalánkabóca}
Eurhodope cirrigerella	{"zörgőszárnyú karcsúmoly"}
Eurhodope rosella	{"rózsaszínű karcsúmoly"}
Eurois occulta	{"nagy áfonya-földibagoly"}
Eurrhypara hortulata	{"tarka csalánmoly "}
Eurrhypis pollinalis	{"galajszövő kormosmoly"}
Eurydema dominulus	{"tarka címerespoloska"}
Eurydema fieberi	{keresztespoloska}
Eurydema oleracea	{paréjpoloska}
Eurydema ornata	{káposztapoloska}
Eurydema ventralis	{"déli káposztapoloska"}
Eurygaster austriaca	{"osztrák poloska"}
Eurygaster dilaticollis	{"szélesnyakú poloska"}
Eurygaster maura	{mórpoloska}
Eurygaster testudinaria	{teknőspoloska}
Eurylophella karelica	{"karéliai kérész"}
Euryopis flavomaculata	{"sárgapettyes törpepók"}
Eurysa lineata	{"csíkoshátú sarkantyúskabóca"}
Eurythyrea aurata	{"aranyos díszbogár"}
Eurythyrea austriaca	{"osztrák díszbogár"}
Euryusa castanoptera	{"zömök tarkaholyva"}
Euryusa optabilis	{"pajzsoshátú tarkaholyva"}
Euryusa sinuata	{"széleshátú tarkaholyva"}
Eusomus ovulum	{cickafarkormányos}
Eusphalerum limbatum	{"barnás virágholyva"}
Eusphalerum longipenne	{"hosszúszárnyú virágholyva"}
Eusphalerum luteum	{"sárga virágholyva"}
Eusphalerum minutum	{"apró virágholyva"}
Eusphalerum primulae	{"tüskéslábú virágholyva"}
Eusphalerum rectangulum	{"fakó virágholyva"}
Eusphalerum semicoleoptratum	{"gödörkés virágholyva"}
Eusphalerum sorbi	{"kis virágholyva"}
Eusphalerum tenenbaumi	{"gödrös virágholyva"}
Euspilapteryx auroguttella	{orbáncfű-keskenymoly}
Euspilotus perrisi	{gyurgyalag-fészeksutabogár}
Eustroma reticulata	{"hálós tarkaaraszoló"}
Eustrophus dermestoides	{"porvaszerű komorka"}
Eutelia adulatrix	{"márványos bagoly"}
Euthrix potatoria	{szomjaspohók}
Euthystira brachyptera	{"smaragdzöld sáska"}
Eutrichapion ervi	{"szempillás cickányormányos"}
Eutrichapion facetum	{"sötét cickányormányos"}
Eutrichapion melancholicum	{"búskomor cickányormányos"}
Eutrichapion punctiger	{"bükkönyszomorító cickányormányos"}
Eutrichapion viciae	{bükköny-cickányormányos}
Eutrichapion vorax	{"csavartlábú cickányormányos"}
Euxoa aquilina	{"fekete fésűsbagoly"}
Euxoa hastifera pomazensis	{"fehérsávos földibagoly"}
Euxoa nigricans	{"sötét földibagoly"}
Euxoa obelisca	{"csíkos földibagoly"}
Euxoa segnilis	{"homoki földibagoly"}
Euxoa temera	{"vetési fésűsbagoly"}
Euxoa tritici	{búza-földibagoly}
Euzonitis auricoma	{"fehérszőrű élősdibogár"}
Euzonitis fulvipennis	{"homoki élősdibogár"}
Euzonitis quadrimaculata	{"barnaszőrű élősdibogár"}
Euzonitis sexmaculata	{"hatfoltos élősdibogár"}
Euzophera bigella	{"kétcsíkos karcsúmoly"}
Euzophera cinerosella	{"ürömfúró karcsúmoly"}
Euzophera fuliginosella	{"kormostövű karcsúmoly"}
Euzophera pinguis	{"kőrislakó karcsúmoly"}
Euzopherodes charlottae	{"magyar karcsúmoly"}
Euzopherodes vapidella	{"apró gyümölcsmoly"}
Evacanthus interruptus	{"pompás kabóca"}
Evagetes pectinipes	{"homoki útonálló"}
Evarcha arcuata	{"íves ugrópók"}
Evarcha falcata	{"vörhenyes ugrópók"}
Evergestis aenealis	{"patinás dudvamoly"}
Evergestis alborivulalis	{"keleti kormosmoly"}
Evergestis extimalis	{"kerti dudvamoly"}
Evergestis forficalis	{veteménymoly}
Evergestis frumentalis	{"tavaszi dudvamoly"}
Evergestis limbata	{"közönséges dudvamoly"}
Evergestis pallidata	{"szalmaszínű dudvamoly"}
Evergestis politalis	{"pompás dudvamoly"}
Exaeretia culcitella	{"feketetövű laposmoly"}
Exaeretia preisseckeri	{molyhostölgyes-laposmoly}
Exaesiopus grossipes	{"homoki sutabogár"}
Exapion compactum	{"aprócska cickányormányos"}
Exapion corniculatum	{zanót-cickányormányos}
Exapion difficile	{rekettye-cickányormányos}
Exapion elongatulum	{"lapított cickányormányos"}
Exapion fuscirostre	{seprőzanót-cickányormányos}
Exochomus nigromaculatus	{"egyszínű szerecsenkata"}
Exoteleia dodecella	{"fenyőrágó borzasmoly"}
Eysarcoris aeneus	{"fémes feketefejű-poloska"}
Eysarcoris fabricii	{"foltos feketefejű-poloska"}
Eysarcoris ventralis	{"fémes feketefejű-poloska"}
Fabiola pokornyi	{"ezüstcsíkos díszmoly"}
Fagivorina arenaria	{bükkfaaraszoló}
Fagotia daudebartii thermalis	{"melegvízi folyamcsiga"}
Fagotia esperi	{pettyescsiga}
Falagria caesa	{"barázdás karcsúholyva"}
Falagria splendens	{"fényes karcsúholyva"}
Falagria sulcatula	{"rovátkás karcsúholyva"}
Falagrioma thoracica	{"hosszúlábú karcsúholyva"}
Falcaria lacertinaria	{"csipkés sarlósszövő"}
Falco eleonorae	{Eleonóra-sólyom}
Falseuncaria degreyana	{útifű-fúrómoly}
Falseuncaria ruficiliana	{"mezei fúrómoly"}
Farsus dubius	{"zömök tövisnyakúbogár"}
Faustina faustina	{"sávos csiga"}
Felis silvestris	{vadmacska}
Filatima spurcella	{kökénymoha-sarlósmoly}
Filatima tephritidella	{"baltajegyű sarlósmoly"}
Flavohelodes flavicollis	{"sárganyakú rétbogár"}
Friedlanderia cicatricella	{"tarka nádfúrómoly"}
Frontinellina frutetorum	{"bokor vitorlapók"}
Fruticicola fruticum	{"berki csiga"}
Fulvoclysia nerminae	{"tarka sárgamoly"}
Furcula bifida	{"kis púposszövő"}
Furcula furcula	{"szürke púposszövő"}
Gabrius appendiculatus	{"barnáscombú ganajholyva"}
Gabrius astutus	{"hegyi ganajholyva"}
Gabrius breviventer	{"berki ganajholyva"}
Gabrius exspectatus	{"magashegyi ganajholyva"}
Gabrius femoralis	{"ligeti ganajholyva"}
Gabrius nigritulus	{"fekete ganajholyva"}
Gabrius osseticus	{"barnás ganajholyva"}
Gabrius piliger	{"vaskos ganajholyva"}
Gabrius splendidulus	{"kéreglakó ganajholyva"}
Gabrius suffragani	{"turjáni ganajholyva"}
Gabrius toxotes	{"mocsárjáró ganajholyva"}
Gabrius trossulus	{"lápi ganajholyva"}
Galba truncatula	{"májmételyes csiga"}
Galeatus affinis	{"átlátszószárnyú csipkéspoloska"}
Galeatus maculatus	{"foltos csipkéspoloska"}
Galleria mellonella	{"nagy viaszmoly"}
Gambusia affinis	{"szúnyogirtó fogasponty"}
Gambusia holbrooki	{"szúnyogirtó fogasponty"}
Gampsocleis glabra	{"tőrös szöcske"}
Gampsocoris culicinus	{"törékeny szúnyogpoloska"}
Gampsocoris punctipes	{"tüskés szúnyogpoloska"}
Gargara genistae	{"púpos kabóca"}
Gasterosteus aculeatus	{"tüskés pikó"}
Gasterosteus gymnurus	{"nyugati pikó"}
Gastrallus immarginatus	{"bütykösnyakú álszú"}
Gastrallus laevigatus	{"simított álszú"}
Gastrodes abietum	{"lapos fenyőbodobács"}
Gastrodes grossipes	{"lapos bodobács"}
Gastropacha populifolia	{sárgapohók}
Gastropacha quercifolia	{tölgylevélpohók}
Gauropterus fulgidus	{"fényes rovátkásholyva"}
Geina didactyla	{gyömbérgyökér-tollasmoly}
Gelechia asinella	{"fűzfaszövő sarlósmoly"}
Gelechia basipunctella	{"hamuszürke sarlósmoly"}
Gelechia muscosella	{fűzbarka-sarlósmoly}
Gelechia nigra	{"kormos sarlósmoly"}
Gelechia rhombella	{"gyümölcslevélszövő sarlósmoly"}
Gelechia rhombelliformis	{"nyárfalevélszövő sarlósmoly"}
Gelechia sabinellus	{"borókarágó sarlósmoly"}
Gelechia scotinella	{kökényvirág-sarlósmoly}
Gelechia senticetella	{"borókalakó sarlósmoly"}
Gelechia sestertiella	{"talléros sarlósmoly"}
Gelechia sororculella	{"fehérkeretes sarlósmoly"}
Gelechia turpella	{"nagy sarlósmoly"}
Geocoris ater	{"csíkoshátú bodobács"}
Geocoris erythrocephalus	{"vörösfejű bodobács"}
Geocoris grylloides	{"nagyszemű bodobács"}
Geolycosa vultuosa	{"pokoli cselőpók"}
Geometra papilionaria	{"nagy zöldaraszoló"}
Geostiba chyzeri	{"kövi humuszholyva"}
Geostiba circellaris	{"érdesszárnyú humuszholyva"}
Geostiba gyorffyi	{"apró humuszholyva"}
Geotrupes mutator	{"változékony álganéjtúró"}
Geotrupes spiniger	{"közönséges álganéjtúró"}
Geotrupes stercorarius	{"hegyi álganéjtúró"}
Gerris argentatus	{"ezüstös molnárpoloska"}
Gerris lacustris	{"tavi molnárpoloska"}
Gerris odontogaster	{víziszöcske}
Gerris thoracicus	{"rozsdáshátú molnárpoloska"}
Gesneria centuriella	{"nagy zuzmóilonca"}
Gibbaranea bituberculata strandiana	{"csúcsoshátú keresztespók"}
Gibberifera simplana	{rezgőnyárfa-tükrösmoly}
Gibbium psylloides	{"csupasz hólyagbogár"}
Gillmeria miantodactylus	{"balkáni tollasmoly"}
Gillmeria ochrodactyla	{"okkersárga tollasmoly"}
Gillmeria pallidactyla	{"fakósárga tollasmoly"}
Glaresis rufa	{"vörhenyes csorvány"}
Glaucopsyche alcon	{"szürkés hangyaboglárka"}
Glaucopsyche alexis	{"nagyszemes boglárka"}
Glaucopsyche arion	{"nagyfoltú hangyaboglárka"}
Glaucopsyche arion ligurica	{"türkiz boglárka"}
Glaucopsyche iolas	{"magyar boglárka"}
Glaucopsyche nausithous	{"sötét hangyaboglárka"}
Glaucopsyche teleius	{vérfű-hangyaboglárka}
Glischrochilus hortensis	{"négyfoltos fénybogár"}
Glischrochilus quadriguttatus	{"négycseppes fénybogár"}
Glischrochilus quadripunctatus	{"négypontos fénybogár"}
Glischrochilus quadrisignatus	{"amerikai fénybogár"}
Glis glis	{"nagy pele"}
Globiceps sphaegiformis	{"kétpúpú mezeipoloska"}
Globicornis corticalis	{"barnás kéregporva"}
Globicornis emarginata	{"sötét kéregporva"}
Globicornis nigripes	{"kis kéregporva"}
Glocianus distinctus	{"gömbded lóhereormányos"}
Glossiphonia complanata	{csigapióca}
Gluphisia crenata	{"kormos púposszövő"}
Glyphipterix bergstraesserella	{perjeszittyó-szakállasmoly}
Glyphipterix equitella	{varjúháj-szakállasmoly}
Glyphipterix forsterella	{"homoki szakállasmoly"}
Glyphipterix haworthana	{gyapjúsás-szakállasmoly}
Glyphipterix pygmaeella	{"sötét szakállasmoly"}
Glyphipterix simpliciella	{ebír-szakállasmoly}
Glyphipterix thrasonella	{"szittyófúró szakállasmoly"}
Glyptoteles leucacrinella	{"turjáni karcsúmoly"}
Gnaphosa lucifuga	{"fekete kövipók"}
Gnaptor spinimanus	{"pohos gyászbogár"}
Gnathocerus cornutus	{"szarvas lisztbogár"}
Gnathoncus communis	{"közönséges fészeksutabogár"}
Gnathoncus disjunctus suturifer	{"varratos fészeksutabogár"}
Gnathoncus nannetensis	{"nagy fészeksutabogár"}
Gnophos furvata	{"nagy sziklaaraszoló"}
Gnorimoschema antiquum	{"mediterrán sarlósmoly"}
Gnorimoschema herbichii	{"rozsdabarna sarlósmoly"}
Gnorimus nobilis	{"hegyi virágbogár"}
Gnorimus variabilis	{"nyolcpettyes virágbogár"}
Gnypeta ripicola	{"busafejű cingárholyva"}
Gnypeta rubrior	{"vöröses cingárholyva"}
Gobio gobio	{"fenékjáró küllő"}
Gomphocerippus rufus	{"erdei bunkóscsápú sáska"}
Gomphus flavipes	{"sárgás szitakötő"}
Gomphus vulgatissimus	{"feketelábú szitakötő"}
Gonatium rubellum	{"dagadtkezű pók"}
Gonatium rubens	{"vastagkezű pók"}
Gonepteryx rhamni	{citromlepke}
Goniodoma auroguttella	{"labodaszárfúró zsákosmoly"}
Gonocephalum granulatum pusillum	{"kis gyászbogár"}
Gonocephalum pygmaeum	{"homoki gyászbogár"}
Gonocerus acuteangulatus	{"aranypetés poloska"}
Gonocerus juniperi	{"boróka karimáspoloska"}
Gonodera luperus	{"gyászos alkonybogár"}
Gonospileia triquetra	{"háromszöges nappalibagoly"}
Gortyna borelii	{"nagy szikibagoly"}
Gortyna flavago	{"kénsárga nádibagoly"}
Gorytes quinquecinctus	{"ötsávos kabócaölő"}
Gossyparia spuria	{szilfa-pajzstetű}
Gracillaria loriolella	{"északi keskenymoly"}
Gracillaria syringella	{orgona-keskenymoly}
Granaria frumentum	{"sokfogú csiga"}
Graphiphora augur	{"feketekeretű földibagoly"}
Grapholita caecana	{baltacim-magrágómoly}
Grapholita compositella	{lucernahüvelymoly}
Grapholita coronillana	{koronafürt-magrágómoly}
Grapholita delineana	{"kis kendermoly"}
Grapholita difficilana	{"levantei magrágómoly"}
Grapholita discretana	{komlómagmoly}
Grapholita fissana	{bükkönymagmoly}
Grapholita funebrana	{szilvamoly}
Grapholita gemmiferana	{lednekmagmoly}
Grapholita janthinana	{galagonyabogyó-tükrösmoly}
Grapholita jungiella	{"bükkönyrágó tükrösmoly"}
Grapholita larseni	{"fényes magrágómoly"}
Grapholita lathyrana	{rekettyerügymoly}
Grapholita lobarzewskii	{almamagmoly}
Grapholita molesta	{"keleti gyümölcsmoly"}
Grapholita nebritana	{dudafürtmoly}
Grapholita orobana	{lednek-magrágómoly}
Grapholita pallifrontana	{csüdfű-magrágómoly}
Grapholita tenebrosana	{csipkebogyómoly}
Graphosoma lineatum	{"csíkos pajzsospoloska"}
Graptopeltus lynceus	{"homoki díszesbodobács"}
Graptus triguttatus	{"nagy nadálytőormányos"}
Gravitarmata margarotana	{"márványos gyantamoly"}
Griposia aprilina	{"zöld őszibagoly"}
Gronops lunatus	{holdvilág-ormányos}
Gryllotalpa gryllotalpa	{lótücsök}
Gryllus campestris	{"mezei tücsök"}
Grynocharis oblonga	{"hosszúkás korongbogár"}
Grypus brunnirostris	{"kis zsurlóormányos"}
Grypus equiseti	{zsurlóormányos}
Gymnancyla canella	{"homoki karcsúmoly"}
Gymnancyla hornigi	{"magrágó karcsúmoly"}
Gymnetron aper	{"apró veronika-ormányos"}
Gymnetron beccabungae	{deréce-ormányos}
Gymnetron furcatum	{"pici veronika-ormányos"}
Gymnetron melanarium	{"kakukk veronika-ormányos"}
Gymnetron rostellum	{"piroslábszárú veronika-ormányos"}
Gymnetron stimulosum	{"feketelábú kamilla-ormányos"}
Gymnetron veronicae	{veronika-ormányos}
Gymnetron villosulum	{"virágrontó veronika-ormányos"}
Gymnocephalus baloni	{"széles durbincs"}
Gymnocephalus cernuus	{durbincs}
Gymnocephalus schraetser	{"selymes durbincs"}
Gymnopleurus geoffroyi	{"közönséges törpegalacsinhajtó"}
Gymnopleurus mopsus	{"pontusi törpegalacsinhajtó"}
Gymnusa brevicollis	{"tőzegkedvelő tőrösholyva"}
Gynandromorphus etruscus	{"etruszk tarkafutó"}
Gynnidomorpha luridana	{"fakósárga fúrómoly"}
Gynnidomorpha minimana	{"apró fúrómoly"}
Gynnidomorpha permixtana	{"lápi fúrómoly"}
Gynnidomorpha vectisana	{"szürke fúrómoly"}
Gypsonoma aceriana	{nyárfahajtás-tükrösmoly}
Gypsonoma dealbana	{"barkarágó tükrösmoly"}
Gypsonoma minutana	{fehérnyár-tükrösmoly}
Gypsonoma nitidulana	{rezgőnyár-tükrösmoly}
Gypsonoma oppressana	{nyárfa-tükrösmoly}
Gypsonoma sociana	{tölgyfalevél-tükrösmoly}
Gyraulus albus	{"rácsos csiga"}
Gyrohypnus angustatus	{"recéshátú rovátkásholyva"}
Gyrohypnus atratus	{"hangyakedvelő rovátkásholyva"}
Gyrohypnus fracticornis	{"pontocskás rovátkásholyva"}
Gyrophaena affinis	{"villás taplóholyva"}
Gyrophaena bihamata	{"öves taplóholyva"}
Gyrophaena gentilis	{"hegyi taplóholyva"}
Gyrophaena joyi	{"füstös taplóholyva"}
Gyrophaena joyioides	{"közönséges taplóholyva"}
Gyrophaena lucidula	{"sötét taplóholyva"}
Gyrophaena manca	{"egyszínű taplóholyva"}
Gyrophaena nana	{"ligeti taplóholyva"}
Gyrophaena nitidula	{"magashegyi taplóholyva"}
Gyrophaena pulchella	{"ékes taplóholyva"}
Gyrophaena strictula	{"kormos taplóholyva"}
Habrocerus capillaricornis	{"közönséges sörtecsápúholyva"}
Habroloma geranii	{"parányi vájárdíszbogár"}
Habrosyne pyritoides	{"fehérsávos pihésszövő"}
Hada plebeja	{"hamvas kertibagoly"}
Hadena albimacula	{"fehérpettyes szegfűbagoly"}
Hadena bicruris	{"kétfoltú szegfűbagoly"}
Hadena capsincola	{"szürke szegfűbagoly"}
Hadena compta	{"foltos szegfűbagoly"}
Hadena confusa	{"apró szegfűbagoly"}
Hadena filograna	{"sötétmintás szegfűbagoly"}
Hadena irregularis	{"homoki szegfűbagoly"}
Hadena magnolii	{Magnol-szegfűbagoly}
Hadena perplexa	{"olajbarna szegfűbagoly"}
Hadena silenes	{"hegyesnyilú szegfűbagoly"}
Hadreule elongatulum	{"keskeny taplószú"}
Hadrobregmus denticollis	{"fogasnyakú kopogóbogár"}
Hadroplontus litura	{"pompás aszatormányos"}
Hadroplontus trimaculatus	{aszatormányos}
Hadula dianthi	{"sziki szegfűbagoly "}
Hadula dianthi hungarica	{"sziki szegfűbagoly "}
Hadula trifolii	{lóherebagoly}
Haematopota pluvialis	{"esőthozó pőcsik"}
Haemopis sanguisuga	{lónadály}
Haeterius ferrugineus	{"hangyász sutabogár"}
Halictus quadricinctus	{"négycsíkos karcsúméh"}
Halictus scabiosae	{ördögszemméh}
Halictus sexcinctus	{"hatsávos karcsúméh"}
Halictus tumulorum	{"zöld karcsúméh"}
Hallomenus axillaris	{"sárgavállú komorka"}
Hallomenus binotatus	{"kétfoltos komorka"}
Halticus apterus	{"rövidszárnyú ugrópoloska"}
Halticus luteicollis	{"sárganyakú ugrópoloska"}
Halticus pusillus	{"törpe ugrópoloska"}
Halticus saltator	{"vörösfejű ugrópoloska"}
Halyomorpha halys	{"ázsiai márványospoloska"}
Halyzia sedecimguttata	{"tizenhatcseppes füsskata"}
Hamearis lucina	{"nyugati kockáslepke"}
Hapalaraea pygmaea	{"szurdoklakó barázdásholyva"}
Haplochrois albanica	{"délvidéki lándzsásmoly"}
Haplochrois ochraceella	{"fényes lándzsásmoly"}
Haplodrassus signifer	{"patkós kövipók"}
Haploglossa marginalis	{"odúlakó pudvaholyva"}
Haploglossa villosula	{"fészeklakó pudvaholyva"}
Haplorhynchites caeruleus	{"hajtástörő eszelény"}
Haplorhynchites pubescens	{"szőrös eszelény"}
Haplotinea ditella	{"avarlakó hulladékmoly"}
Haplotinea insectella	{kitinmoly}
Harmonia axyridis	{harlekinkatica}
Harmonia quadripunctata	{"négypettyes katica"}
Harpactea hombergi	{"kis rablópók"}
Harpactea rubicunda	{"vörös rablópók"}
Harpactus affinis	{"piroshasú kabócaölő"}
Harpadispar diffusalis	{"villányi tűzmoly"}
Harpalus affinis	{"szőrösszárnyú lomhafutó"}
Harpalus albanicus	{"albán lomhafutó"}
Harpalus angulatus	{"pusztai piroslábú lomhafutó"}
Harpalus angulatus scytha	{"pusztai piroslábú lomhafutó"}
Harpalus anxius	{"parlagi lomhafutó"}
Harpalus atratus	{"erdei lomhafutó"}
Harpalus attenuatus	{"pusztai lomhafutó"}
Harpalus autumnalis	{"gödörkés lomhafutó"}
Harpalus caspius	{"keleti lomhafutó"}
Harpalus caspius roubali	{"keleti lomhafutó"}
Harpalus cupreus	{"keleti rezes lomhafutó"}
Harpalus cupreus fastuosus	{"keleti rezes lomhafutó"}
Harpalus dimidiatus	{"nyugati lomhafutó"}
Harpalus distinguendus	{"mezei lomhafutó"}
Harpalus flavescens	{"rozsdás lomhafutó"}
Harpalus flavicornis	{"sárgacsápú lomhafutó"}
Harpalus froelichii	{"tömzsi lomhafutó"}
Harpalus fuscicornis	{"sötétcsápú lomhafutó"}
Harpalus fuscipalpis	{"füstöscsápú lomhafutó"}
Harpalus hirtipes	{"sarkantyús lomhafutó"}
Harpalus honestus	{"azúrkék lomhafutó"}
Harpalus hospes	{"változékony lomhafutó"}
Harpalus inexpectatus	{"sztyeppi lomhafutó"}
Harpalus latus	{"szélesfejű lomhafutó"}
Harpalus luteicornis	{"fénytelen lomhafutó"}
Harpalus marginellus	{"busafejű lomhafutó"}
Harpalus modestus	{"kereknyakú lomhafutó"}
Harpalus oblitus	{"gyepi lomhafutó"}
Harpalus picipennis	{"apró lomhafutó"}
Harpalus progrediens	{"réti lomhafutó"}
Harpalus pumilus	{"törpe lomhafutó"}
Harpalus pygmaeus	{"kis lomhafutó"}
Harpalus rubripes	{"vöröslábú lomhafutó"}
Harpalus rufipalpis	{"hegyi lomhafutó"}
Harpalus saxicola	{"kéklő lomhafutó"}
Harpalus serripes	{"fekete lomhafutó"}
Harpalus servus	{"homoki lomhafutó"}
Harpalus smaragdinus	{"smaragd lomhafutó"}
Harpalus subcylindricus	{"keskeny lomhafutó"}
Harpalus tardus	{"ligeti lomhafutó"}
Harpalus xanthopus	{"európai berki lomfafutó"}
Harpalus xanthopus winkleri	{"európai berki lomfafutó"}
Harpalus zabroides	{"óriás lomhafutó"}
Harpella forficella	{"korhadékevő díszmoly"}
Harpyia milhauseri	{pergament-púposszövő}
Hebrus pusillus	{fenyérpoloska}
Hebrus ruficeps	{"kis fenyérpoloska"}
Hecatera bicolorata	{"világos kertibagoly"}
Hecatera dysodea	{parajbagoly}
Hedobia pubescens	{"szőrös álszú"}
Hedya dimidiana	{"sárgafoltú tükrösmoly"}
Hedya nubiferana	{"rügysodró tükrösmoly"}
Hedya ochroleucana	{"rózsalevélsodró tükrösmoly"}
Hedya pruniana	{szilvarügymoly}
Hedya salicella	{"fehérhátú tükrösmoly"}
Hedychridium ardens	{"apró fémdarázs"}
Hedychridium roseum	{"piroshasú fémdarázs"}
Hedychrum nobile	{"nemes fémdarázs"}
Hedychrum rutilans	{"méhfarkas fémdarázs"}
Heinemannia festivella	{aranyvessző-lándzsásmoly}
Heinemannia laspeyrella	{"sárgafejű lándzsásmoly"}
Helcystogramma albinervis	{"fehérerű lápimoly"}
Helcystogramma arulensis	{"ritka lápimoly"}
Helcystogramma lineolella	{"barnacsíkos lápimoly"}
Helcystogramma lutatella	{nádtippan-lápimoly}
Helcystogramma rufescens	{"fűsodró lápimoly"}
Helcystogramma triannulella	{"szuláksodró lápimoly"}
Helianthemapion aciculare	{napvirág-cickányormányos}
Helianthemapion velatum	{"ércszínű cickányormányos"}
Helicodonta obvoluta	{korongcsiga}
Helicoverpa armigera	{"gyapottok bagolylepke"}
Heliococcus bohemicus	{"viaszos akác-pajzstetű"}
Heliodines roesella	{parajszövőmoly}
Heliomata glarearia	{"bőrszínű araszoló"}
Heliophanus auratus	{"aranyos ugrópók"}
Heliophanus cupreus	{"rezes ugrópók"}
Heliophanus flavipes	{"fémes ugrópók"}
Heliophanus kochii	{"négypettyes ugrópók"}
Heliophanus simplex	{"egyszerű ugrópók"}
Heliothela wulfeniana	{"fényes kormosmoly"}
Heliothis maritima	{somkóróbagoly}
Heliothis maritima bulgarica	{somkóróbagoly}
Heliothis ononis	{iglicebagoly}
Heliothis peltigera	{mentabagoly}
Heliothis viriplaca	{mácsonyabagoly}
Heliozela resplendella	{"égeraknázó fényesmoly"}
Heliozela sericiella	{"tölgyaknázó fényesmoly"}
Helix lutescens	{ugarcsiga}
Helix pomatia	{"éti csiga"}
Hellinsia carphodactyla	{"kénsárga tollasmoly"}
Hellinsia didactylites	{hölgymálvirág-tollasmoly}
Hellinsia distinctus	{gyopárvirág-tollasmoly}
Hellinsia inulae	{peremizsvirág-tollasmoly}
Hellinsia lienigianus	{"ürömszövő tollasmoly"}
Hellinsia osteodactylus	{"csontszínű tollasmoly"}
Hellinsia tephradactyla	{"feketepontos tollasmoly"}
Hellula undalis	{"zegzugos tűzmoly"}
Helobdella stagnalis	{"fiahordó nadály"}
Hemianax ephippiger	{"nyerges szitakötő"}
Hemiclepsis marginata	{békapióca}
Hemicoelus costatus	{"sávosszőrű kopogóbogár"}
Hemicoelus fulvicornis	{"sárgacsápú kopogóbogár"}
Hemicoelus nitidus	{"szőrösszemű kopogóbogár"}
Hemicoelus rufipennis	{"kétszínű kopogóbogár"}
Hemicrepidius hirtus	{"borzas pattanó"}
Hemicrepidius niger	{szerecsenpattanó}
Hemipterochilus bembiciformis	{"nagy szőrösnyelvű darázs"}
Hemistola chrysoprasaria	{"kékes zöldaraszoló"}
Hemithea aestivaria	{nyír-zöldaraszoló}
Hemitrichapion pavidum	{koronafürt-cickányormányos}
Hemitrichapion reflexum	{baltacim-cickányormányos}
Henestaris halophilus	{"nyelesszemű bodobács"}
Henosepilachna argus	{földitök-böde}
Henosepilachna elaterii	{magrugó-böde}
Hepialus humuli	{"nagy gyökérrágólepke"}
Herculia incarnatalis	{"piros fényilonca"}
Herculia rubidalis	{"vörös fényilonca"}
Heriaeus hirtus	{"bozonos karolópók"}
Herminia grisealis	{"ligeti karcsúbagoly"}
Herminia tarsicrinalis	{"szőröslábú karcsúbagoly"}
Herminia tenuialis	{"magyar hullámsávos karcsúbagoly "}
Herotilapia multispinosa	{szivárványsügér}
Hesperia comma	{"vesszős busalepke"}
Hesperus rufipennis	{"pompás ganajholyva"}
Heterhelus scutellaris	{"szögletestorú bodza-álfénybogár"}
Heterhelus solani	{"kerektorú bodza-álfénybogár"}
Heterocapillus tigripes	{"tarkalábú mezeipoloska"}
Heterocerus crinitus	{"hosszúszőrű iszapbogár"}
Heterocerus fenestratus	{"foltos iszapbogár"}
Heterocerus flexuosus	{"fényes iszapbogár"}
Heterocerus fossor	{"áskáló iszapbogár"}
Heterocerus fusculus	{"sárgatérdű iszapbogár"}
Heterocerus marginatus	{"szegélyes iszapbogár"}
Heterocerus obsoletus	{"sötét iszapbogár"}
Heterocerus parallelus	{"sziki iszapbogár"}
Heterocordylus tumidicornis	{kökény-mezeipoloska}
Heterogaster affinis	{hangyabodobács}
Heterogaster urticae	{"tarka bodobács"}
Heterogenea asella	{csigalepke}
Heterogynis penella	{"füstösmoly "}
Heterotheridion nigrovariegatum	{"fehér törpepók"}
Heterothops dissimilis	{"keskenyfejű egérholyva"}
Heterothops praevius	{"barna egérholyva"}
Heterothops praevius niger	{"fekete egérholyva"}
Heterothops stiglundbergi	{"felemás egérholyva"}
Heterotoma merioptera	{"laposcsápú mezeipoloska"}
Hexarthrum exiguum	{bányafa-szúormányos}
Himacerus apterus	{"nagy tolvajpoloska"}
Himacerus mirmicoides	{csalán-tolvajpoloska}
Hipparchia fagi	{"szürkeöves szemeslepke"}
Hipparchia semele	{"barna szemeslepke"}
Hipparchia statilinus	{"homoki szemeslepke"}
Hippeutis complanatus	{lencsecsiga}
Hippodamia notata	{"alhavasi katica"}
Hippodamia septemmaculata	{"hétpontos katica"}
Hippodamia tredecimpunctata	{"mocsári katica"}
Hippodamia undecimnotata	{bogáncskatica}
Hippodamia variegata	{"tizenhárompettyes katica"}
Hippotion celerio	{"csíkos szender"}
Hirticomus hispidus	{"szőrös fürgebogár"}
Hirudo medicinalis	{"orvosi pióca"}
Hirudo verbana	{"magyar nadály"}
Hister bissexstriatus	{"nyolcsávos sutabogár"}
Hister funestus	{"tomparágójú sutabogár"}
Hister helluo	{éger-sutabogár}
Hister illigeri	{"holdfoltos sutabogár"}
Hister lugubris	{"alföldi sutabogár"}
Hister quadrimaculatus	{"közönséges sutabogár"}
Hister quadrinotatus	{"négyfoltos sutabogár"}
Hister sepulchralis	{"élesrágójú sutabogár"}
Hister unicolor	{"fekete sutabogár"}
Histopona torpida	{"karcsú zugpók"}
Hofmannophila pseudospretella	{házimoly}
Hogna radiata	{"réti cselőpók"}
Holcophora statices	{"homoki sarlósmoly"}
Holcopogon bubulcellus helveolellus	{ürülékmoly}
Holcostethus sphacelatus	{"holdas címerespoloska"}
Holcostethus strictus vernalis	{"tavaszi címerespoloska"}
Holobus apicatus	{"öves parányholyva"}
Holobus flavicornis	{"gömböc parányholyva"}
Holochelus aequinoctialis	{"tavaszeleji cserebogár"}
Holochelus aestivus	{"tavaszvégi cserebogár"}
Holochelus vernus	{"tavaszi cserebogár"}
Hololepta plana	{lemez-sutabogár}
Holoparamecus caularum	{"közönséges szénaálböde"}
Holoparamecus ragusae	{"széles szénaálböde"}
Holoscolia huebneri	{"sarlósszárnyú csíkosmoly"}
Holotrichapion aethiops	{"etióp cickányormányos"}
Holotrichapion gracilicolle	{"formás cickányormányos"}
Holotrichapion ononis	{lucerna-cickányormányos,"szakállas cickányormányos"}
Holotrichapion pisi	{lucernarügy-cickányormányos}
Homalota plana	{"lapos kérgészholyva"}
Homaloxestis briantiella	{"nagy hindumoly"}
Homoeosoma inustella	{"ázsiai karcsúmoly"}
Homoeosoma nebulella	{napraforgómoly}
Homoeosoma nimbella	{"apró karcsúmoly"}
Homoeosoma sinuella	{"agyagsárga karcsúmoly"}
Homoeusa acuminata	{"sima pajzsosholyva"}
Hoplia argentea	{"ezüstös virágcserebogár"}
Hoplia hungarica	{"homoki virágcserebogár"}
Hoplia praticola	{"réti virágcserebogár"}
Hoplodrina ambigua	{"változékony szürkebagoly"}
Hoplodrina blanda	{"sötét fűbagoly"}
Hoplodrina octogenaria	{"őzbarna fűbagoly"}
Hoplodrina respersa	{"pettyes szürkebagoly"}
Hoplomachus thunbergii	{hölgymálpoloska}
Hoplopholcus forskali	{"mintás álkaszáspók"}
Horisme corticata	{"barna iszalagaraszoló"}
Horisme tersata	{"fakó iszalagaraszoló"}
Horisme vitalbata	{"tarka iszalagaraszoló"}
Horvathiolus superbus	{"kis fehérfoltos bodobács"}
Hoshihananomia perlata	{"fehérpettyes maróka"}
Huso huso	{viza}
Hyalesthes obsoletus	{"sárgalábú recéskabóca"}
Hybomitra solstitialis	{"nyári bögöly"}
Hycleus polymorphus	{"változó hólyaghúzó"}
Hycleus tenerus	{"kis hólyaghúzó"}
Hydraecia micacea	{"rózsás fűbagoly"}
Hydraecia petasitis	{"nagy vízibagoly"}
Hydraena gracilis	{"sebesvízi laposcsiborka"}
Hydraena palustris	{"állóvízi laposcsiborka"}
Hydrelia flammeolaria	{"narancssárga araszoló"}
Hydria cervinalis	{"őzbarna araszoló"}
Hydria undulata	{"hullámvonalas araszoló"}
Hydriomena furcata	{"változékony tarkaaraszoló"}
Hydriomena impluviata	{égeraraszoló}
Hydrocyphon deflexicollis	{"hajlottnyakú rétbogár"}
Hydrometra gracilenta	{"kis vízmérőpoloska"}
Hydrometra stagnorum	{"vízmérő poloska"}
Hydroporus discretus	{"erdei kiscsíkbogár"}
Hydrosmecta delicatula	{"apró hordalékholyva"}
Hygronoma dimidiata	{"kétszínű nádiholyva"}
Hygrotus parallellogrammus	{"pettyesnyakú aprócsíkbogár"}
Hylaea fasciaria	{fenyő-zöldaraszoló}
Hylaeus annularis	{"fekete álarcosméh"}
Hylaeus gibbus	{"gyakori álarcosméh"}
Hylaeus variegatus	{"piroshasú álarcosméh"}
Hylastes angustatus	{"karcsú gyökérszú"}
Hylastes ater	{"fekete gyökérszú"}
Hylastes attenuatus	{"keskeny gyökérszú"}
Hylastes brunneus	{"barna gyökérszú"}
Hylastes cunicularius	{fenyő-gyökérszú}
Hylastes linearis	{"nyulánk gyökérszú"}
Hylastes opacus	{"sötét gyökérszú"}
Hylastinus obscurus	{vöröshereszú}
Hylecoetus dermestoides	{"penésztenyésztő fabogár"}
Hyles euphorbiae	{kutyatejszender}
Hyles gallii	{galajszender}
Hylesinus crenatus	{"nagy kőrisfarontószú"}
Hylesinus fraxini	{kőrisszú}
Hylesinus toranio	{olajfarontószú}
Hylesinus wachtli orni	{"csupasztorú kőrisszú"}
Hyles livornica	{"sávos szender"}
Hyles vespertilio	{denevérszender}
Hylis cariniceps	{"ráncos tövisnyakúbogár"}
Hylis foveicollis	{"gödrös tövisnyakúbogár"}
Hylis simonae	{"hosszúcsápú tövisnyakúbogár"}
Hylobius abietis	{"nagy fenyőormányos"}
Hylobius excavatus	{lucfenyőormányos}
Hylobius pinastri	{"kis fenyőormányos"}
Hylobius transversovittatus	{lomberdő-ormányos}
Hylurgus ligniperda	{gyökér-háncsszú}
Hymenalia rufipes	{"rőtlábú alkonybogár"}
Hypatima rhomboidella	{"levélhajtó sarlósmoly"}
Hypatopa binotella	{fenyő-avarmoly}
Hypatopa inunctella	{"barna avarevőmoly"}
Hypena crassalis	{"barnafoltú karcsúbagoly"}
Hypena obesalis	{"nagy karcsúbagoly"}
Hypena proboscidalis	{"ormányos karcsúbagoly"}
Hypena rostralis	{"közönséges karcsúbagoly"}
Hypenodes humidalis	{"felemásszárnyú karcsúbagoly "}
Hypenodes orientalis	{"pannon karcsúbagoly"}
Hypera arator	{"közönséges gubósormányos"}
Hypera meles	{lóhere-gubósormányos}
Hypera nigrirostris	{lóhere-gubósormányos}
Hypera plantaginis	{útifű-gubósormányos}
Hypera pollux	{"mocsári gubósormányos"}
Hypera postica	{lucernaormányos}
Hypera rumicis	{sóska-gubósormányos}
Hyperaspis campestris	{"mezei szerecsenkata"}
Hyperaspis concolor	{"egyszínű szerecsenkata"}
Hyperaspis erytrocephala	{"hatcseppes szerecsenkata"}
Hyperaspis inexpectata	{"kerekded szerecsenkata"}
Hyperaspis pseudopustulata	{"vállcseppes szerecsenkata"}
Hyperaspis reppensis	{"tojásdad szerecsenkata"}
Hyperaspis reppensis quadrimaculata	{"négycseppes szerecsenkata"}
Hypera suspiciosa	{lednek-gubósormányos}
Hypera venusta	{bíborhere-ormányos}
Hypera viciae	{bükköny-gubósormányos}
Hypercallia citrinalis	{"pirossávos díszmoly"}
Hyperlais dulcinalis	{"homoki tűzmoly"}
Hyphantria cunea	{"amerikai fehérmedvelepke"}
Hyphoraia aulica	{"barna medvelepke"}
Hypnogyra angularis	{"kéreglakó rovátkásholyva"}
Hypoborus ficus	{fügeszú}
Hypocacculus metallescens	{"bronzos trágyasutabogár"}
Hypocacculus rubripes	{"vöröslábú trágyasutabogár"}
Hypocacculus rufipes	{"sárgalábú trágyasutabogár"}
Hypocacculus spretulus	{"déli trágyasutabogár"}
Hypocaccus metallicus	{"fekete trágyasutabogár"}
Hypocaccus rugiceps	{"pontozott trágyasutabogár"}
Hypocaccus rugifrons	{"közönséges trágyasutabogár"}
Hypochalcia ahenella	{"óriás karcsúmoly"}
Hypochalcia decorella	{"barna karcsúmoly"}
Hypochalcia dignella	{"sárgafoltos karcsúmoly"}
Hypochalcia lignella	{"vörhenyes karcsúmoly"}
Hypoganus inunctus	{"téglavörös pattanó"}
Hypomecis punctinalis	{"pettyes tölgyaraszoló"}
Hypomecis roboraria	{"nagy tölgyaraszoló"}
Hyponephele lycaon	{"erdei ökörszemlepke"}
Hypophloeus bicolor	{"kétszínű kéregbújó"}
Hypophloeus fasciatus	{"öves kéregbújó"}
Hypophloeus fraxini	{"szélesnyakú kéregbújó"}
Hypophloeus linearis	{"karcsú kéregbújó"}
Hypophloeus longulus	{"keskenynyakú kéregbújó"}
Hypophloeus pini	{fenyő-kéregbújó}
Hypophloeus suberis	{"vöröses kéregbújó"}
Hypophloeus unicolor	{"egyszínű kéregbújó"}
Hypophloeus versipellis	{"feketésbarna kéregbújó"}
Hypophthalmichthys molitrix	{"fehér busa"}
Hypophthalmichthys nobilis	{"pettyes busa"}
Hyporatasa allotriella	{"vaksziki karcsúmoly"}
Hyppa rectilinea	{"tarka gyászbagoly"}
Hypsopygia costalis	{szénailonca}
Hypsosinga albovittata	{"fehérjegyű keresztespók"}
Hypsosinga heri	{"csipkés keresztespók"}
Hypsosinga pygmaea	{"törpe keresztespók"}
Hypsosinga sanguinea	{"vérszínű keresztespók"}
Hypsotropa unipunctella	{"sztyeppréti karcsúmoly"}
Hyptiotes paradoxus	{"furcsa pók"}
Hypulus bifasciatus	{"öves komorka"}
Hypulus quercinus	{tölgy-komorka}
Hyssia cavernosa	{"feketejegyű rétibagoly"}
Hysterophora maculosana	{"karszti fúrómoly"}
Iassus lanio	{"rozsdásfejű kabóca"}
Icaris sparganii	{békabuzogány-ormányos}
Ictalurus punctatus	{"pettyes harcsa"}
Ictiobus bubalus	{"kisszájú buffaló"}
Idaea aureolaria	{"aranyos apróaraszoló"}
Idaea aversata	{"sávos apróaraszoló"}
Idaea biselata	{"mocsári sávosaraszoló "}
Idaea degeneraria	{"barnasávos apróaraszoló"}
Idaea deversaria	{"egyszínű apróaraszoló"}
Idaea dimidiata	{"gyakori apróaraszoló"}
Idaea emarginata	{"csipkés apróaraszoló"}
Idaea fuscovenosa	{"szürkeszélű apróaraszoló"}
Idaea humiliata	{"sárgaszélű apróaraszoló"}
Idaea muricata	{"mocsári apróarszoló"}
Idaea ochrata	{"kis sárga-apróaraszoló"}
Idaea politaria	{"vonalas apróaraszoló "}
Idaea rufaria	{"vörhenyes apróaraszoló"}
Idaea rusticata	{"tarka apróaraszoló"}
Idaea seriata	{"selymes sávosaraszoló"}
Idaea sericeata	{"selymes apróaraszoló"}
Idaea serpentata	{"kis apróaraszoló"}
Idaea straminata	{"egyszínű sávosaraszoló"}
Idaea sylvestraria	{"szürke sávosaraszoló "}
Idia calvaria	{"sárgafoltú kuszabagoly"}
Idolus picipennis	{"vállfoltos pattanó"}
Iduna pallida	{"halvány geze"}
Iduna pallida elaeica	{"halvány geze"}
Ilyobates bennetti	{"alföldi vastagcsápúholyva"}
Ilyobates mech	{"ligeti vastagcsápúholyva"}
Ilyobates nigricollis	{"termetes vastagcsápúholyva"}
Ilyobates propinquus	{"hangyakedvelő vastagcsápúholyva"}
Ilyocoris cimicoides	{csíkpoloska}
Incestophantes crucifer	{"keresztes vitorlapók"}
Incurvaria koerneriella	{bükkös-virágmoly}
Incurvaria masculella	{"tölgyaknázó virágmoly"}
Incurvaria oehlmanniella	{áfonyás-virágmoly}
Incurvaria pectinea	{"nyíraknázó virágmoly"}
Incurvaria praelatella	{"szamócarágó virágmoly"}
Infurcitinea albicomella	{"fehérfejű zuzmómoly"}
Infurcitinea argentimaculella	{"ezüstös zuzmómoly"}
Infurcitinea finalis	{"magyar zuzmómoly"}
Infurcitinea roesslerella	{"szürke zuzmómoly"}
Insalebria gregella	{"dalmát karcsúmoly"}
Insalebria serraticornella	{"dalmát karcsúmoly"}
Involvulus aethiops	{szerecseneszelény}
Involvulus cupreus	{szilvaeszelény}
Ipa keyserlingi	{"hatpettyes vitorlapók"}
Ipidia binotata	{"bordás fénybogár"}
Ipimorpha retusa	{lomb-nyárfabagoly}
Ipimorpha subtusa	{"nagyfoltú nyárfabagoly"}
Ips acuminatus	{"kis betűzőszú"}
Ips cembrae	{sokfogú-szú}
Ips duplicatus	{"sorpontos betűzőszú"}
Ips sexdentatus	{"hatfogú szú"}
Ips typographus	{betűzőszú}
Isauria dilucidella	{"sziki karcsúmoly"}
Ischnodemus sabuleti	{"nádi bodobács"}
Ischnodes sanguinicollis	{"éknyakú pattanó"}
Ischnomera caerulea	{"zöldeskék álcincér"}
Ischnomera cinerascens	{"szürkés álcincér"}
Ischnomera cyanea	{"ciánkék álcincér"}
Ischnomera sanguinicollis	{"vörösnyakú álcincér"}
Ischnopoda leucopus	{"kékfényű cingárholyva"}
Ischnopoda umbratica	{"bronzfényű cingárholyva"}
Ischnopterapion aeneomicans	{"pocakos cickányormányos"}
Ischnopterapion fallens	{"alföldi cickányormányos"}
Ischnopterapion loti	{kerep-cickányormányos}
Ischnopterapion modestum	{"csodálatos cickányormányos"}
Ischnopterapion virens	{lóheregyökér-cickányormányos}
Ischnosoma longicorne	{"erdei gombászholyva"}
Ischnosoma splendidum	{"közönséges gombászholyva"}
Ischnura elegans	{"kék légivadász"}
Ischnura elegans pontica	{"kék légivadász"}
Ischnura pumilio	{"apró légivadász"}
Isidiella nickerlii	{cickafark-tündérmoly}
Isochnus angustifrons	{"törpe bolhaormányos"}
Isochnus foliorum	{rekettyefűz-bolhaormányos}
Isochnus populicola	{nyár-bolhaormányos}
Isognomostoma isognomostomos	{"háromfogú csiga"}
Isomira antennata	{"vastagcsápú alkonybogár"}
Isomira murina	{"egérszürke alkonybogár"}
Isonychia ignota	{"vastagkarmú kérész"}
Isoperla grammatica	{"patkós álkérész"}
Isophrictis anthemidella	{margitvirág-sarlósmoly}
Isophrictis striatella	{"varádicslakó sarlósmoly"}
Isophya brevipennis	{"kárpáti tarsza"}
Isophya costata	{"magyar tarsza"}
Isophya modesta	{"pusztai tarsza"}
Isophya modestior	{"illír tarsza"}
Isophya stysi	{Stys-tarsza}
Isoptena serricornis	{"homokásó álkérész"}
Isoriphis marmottani	{"kétszínű tövisnyakúbogár"}
Isoriphis melasoides	{"legyezős tövisnyakúbogár"}
Isotrias hybridana	{"cifra sodrómoly"}
Isotrias rectifasciana	{"törtsávú sodrómoly"}
Issoria lathonia	{közönséges}
Issus coleoptratus	{fa-pajzsoskabóca}
Isturgia arenacearia	{"sárga lucernaraszoló"}
Isturgia murinaria	{"egérszínű lucernaaraszoló"}
Iwaruna klimeschi	{"osztrák sarlósmoly"}
Ixapion variegatum	{fagyöngy-cickányormányos}
Jalla dumosa	{"csíkosfejű címerespoloska"}
Javesella pellucida	{"üvegszárnyú kabóca"}
Jodia croceago	{"élénksárga őszibagoly"}
Jodis lactearia	{"fehéres zöldaraszoló"}
Jordanita budensis	{"magyar fémlepke"}
Jordanita chloros	{"ércfényű fémlepke"}
Jordanita fazekasi	{Fazekas-fémlepkéje}
Jordanita globulariae	{"nagy fémlepke"}
Jordanita notata	{"aranyzöld fémlepke"}
Jordanita subsolana	{"balkáni fémlepke"}
Jucancistrocerus jucundus	{"foltos kürtősdarázs"}
Kalcapion pallipes	{szélfű-cickányormányos}
Kalcapion semivittatum	{"foltos cickányormányos"}
Kateretes mixtus	{"barnás álfénybogár"}
Kateretes pedicularius	{"sárga álfénybogár"}
Kateretes pusillus	{"vastagcsápú álfénybogár"}
Kateretes rufilabris	{szittyó-álfénybogár}
Kelisia guttula	{"pofafoltos kabóca"}
Kemtrognophos ambiguata	{"havasi sziklaaraszoló"}
Kermes quercus	{tölgy-kéregpajzstetű}
Kermes roboris	{kocsányostölgy-kéregpajzstetű}
Khorassania compositella	{ürömlevél-karcsúmoly}
Kisanthobia ariasi	{Arias-díszbogár}
Kishidaia conspicua	{"öves pók"}
Kissophagus hederae	{borostyánszú}
Kleidocerys resedae	{fabodobács}
Klimeschia transversella	{kakukkfű-legyezősmoly}
Klimeschiopsis kiningerella	{"sárgaképű sarlósmoly"}
Knipowitschia caucasica	{"kaukázusi törpegéb"}
Korscheltellus lupulinus	{"keleti gyökérrágólepke"}
Korynetes caeruleus	{"kék csontbogár"}
Kovacsia kovacsi	{"dobozi pikkelyescsiga"}
Kybos smaragdulus	{smaragdkabóca}
Labarrus lividus	{"turjáni trágyabogár"}
Labiaticola atricolor	{"hasznos tisztesfű-báris"}
Lacanobia aliena	{"sötét veteménybagoly"}
Lacanobia blenna	{libatopbagoly}
Lacanobia contigua	{"hamuszürke dudvabagoly"}
Lacanobia oleracea	{saláta-veteménybagoly}
Lacanobia splendens	{"mocsári dudvabagoly"}
Lacanobia suasa	{laboda-veteménybagoly}
Lacanobia thalassina	{borbolya-veteménybagoly}
Lacanobia w-latinum	{rekettye-veteménybagoly}
Lachnaeus crinitus	{sünbarkó}
Laciniaria plicata	{"redős orsócsiga"}
Lacon punctatus	{"pontozott pattanó"}
Lacon querceus	{"tarka pikkelyespattanó"}
Laelia coenosa	{nádiszövő}
Laemophloeus kraussi	{"kisfoltú szegélyeslapbogár"}
Laemophloeus monilis	{"nagyfoltú szegélyeslapbogár"}
Laemostenus terricola	{pincefutó}
Laena viennensis	{"bécsi avargyászbogár"}
Lagria atripes	{"feketelábú gyapjasbogár"}
Lagria hirta	{"réti gyapjasbogár"}
Lamoria anella	{törmelékmoly}
Lampides boeticus	{"trópusi vándorboglárka"}
Lamprias chlorocephalus	{"zöldfejű tetvészfutó"}
Lamprias cyanocephalus	{"kékfejű tetvészfutó"}
Lamprinodes saginatus	{"hangyakedvelő kószaholyva"}
Lamprobyrrhulus nitidus	{föveny-labdacsbogár}
Lamprohiza splendidula	{"törpe szentjánosbogár"}
Lampronia corticella	{"málnarágó virágmoly"}
Lampronia flavimitrella	{"sárgafelyű virágmoly"}
Lampronia fuscatella	{"nyírfalakó virágmoly"}
Lampronia morosa	{"rózsarágó virágmoly"}
Lampronia pubicornis	{jajrózsa-virágmoly}
Lampronia rupella	{"hegyi virágmoly"}
Lampropteryx suffumata	{füstösaraszoló}
Lamprosticta culta	{"hármasfoltú bagoly"}
Lampyris noctiluca	{"nagy szentjánosbogár"}
Langelandia anophtalma	{"vak héjbogár"}
Laodamia faecella	{"keresztsávos karcsúmoly"}
Laothoe populi	{nyárfaszender}
Larinioides cornutus	{"nádi keresztespók"}
Larinioides ixobolus	{"hídi keresztespókhídi keresztespók"}
Larinioides patagiatus	{"parti keresztespók"}
Larinus brevis	{bábakalács-púderbogár}
Larinus canescens	{"ritka púderbogár"}
Larinus jaceae	{"sávosnyakú púderbogár"}
Larinus latus	{"óriás púderbogár"}
Larinus minutus	{"apró púderbogár"}
Larinus obtusus	{imola-púderbogár}
Larinus planus	{"foltos púderbogár"}
Larinus rugulosus	{"tömzsi púderbogár"}
Larinus sturnus	{aszat-púderbogár}
Larinus turbinatus	{"kúposorrú púderbogár"}
Larus michahellis	{"sárgalábú sirály"}
Lasiacantha capucina	{"pillás csipkéspoloska"}
Lasiocampa quercus	{tölgyfapohók}
Lasiocampa trifolii	{lóherepohók}
Lasioderma obscurum	{"pusztai álszú"}
Lasioderma redtenbacheri	{imola-álszú}
Lasioderma serricorne	{dohány-álszú}
Lasioderma thoracicum	{"sárganyakú álszú"}
Lasioglossum leucozonium	{"gyakori karcsúméh"}
Lasiommata maera	{"nagyfoltú szemeslepke"}
Lasiommata megera	{"vörös szemeslepke"}
Lasiommata petropolitana	{"észeki szemeslepke"}
Lasionycta imbecilla	{"hegyi apróbagoly"}
Lasiorhynchites cavifrons	{"erdei eszelény"}
Lasiorhynchites coeruleocephalus	{"kékfejű eszelény"}
Lasiorhynchites olivaceus	{tölgyeszelény}
Lasiorhynchites praeustus	{"vörös eszelény"}
Lasiorhynchites sericeus	{kakukkeszelény}
Laspeyria flexula	{"csipkés zuzmóbagoly"}
Latheticus oryzae	{"hosszúfejű lisztbogár"}
Lathrobium brunnipes	{"nagy mocsárholyva"}
Lathrobium castaneipenne	{"feketevörös mocsárholyva"}
Lathrobium elegantulum	{"vörösfoltos mocsárholyva"}
Lathrobium elongatum	{"kétszínű mocsárholyva"}
Lathrobium fovulum	{"selymes mocsárholyva"}
Lathrobium fulvipenne	{"közönséges mocsárholyva"}
Lathrobium furcatum	{"sziki mocsárholyva"}
Lathrobium impressum	{"fényes mocsárholyva"}
Lathrobium laevipenne	{"hegyi mocsárholyva"}
Lathrobium longulum	{"apró mocsárholyva"}
Lathrobium pallidum	{"halvány mocsárholyva"}
Lathrobium rufipenne	{"tőzegkedvelő mocsárholyva"}
Lathrobium spadiceum	{"folyóparti mocsárholyva"}
Lathrobium volgense	{"változékony mocsárholyva"}
Lathronympha strigana	{orbáncfű-magrágómoly}
Lathropus sepicola	{"parányi szegélyeslapbogár"}
Lebia cruxminor	{"keresztes cserjefutó"}
Lebia humeralis	{"humeralis cserjefutó"}
Lebia marginata	{"szegélyes cserjefutó"}
Lebia scapularis	{"foltos cserjefutó"}
Lebia trimaculata	{"háromfoltos cserjefutó"}
Lecanopsis porifera	{gyökér-teknőspajzstetű}
Lecithocera nigrana	{"kis hindumoly"}
Ledra aurita	{"füles kabóca"}
Legnotus limbosus	{"fehérszélű földipoloska"}
Lehmannia marginata	{"lágy meztelencsiga"}
Leichenum pictum	{"pikkelyes gyászbogár"}
Leiestes seminiger	{"félfekete álböde"}
Leiopsammodius haruspex	{"fészeklakó trágyabogár"}
Leistus ferrugineus	{"rozsdás avarfutó"}
Leistus piceus	{"szurkos erdei avarfutó"}
Leistus piceus kaszabi	{"Kaszab erdei avarfutója"}
Leistus rufomarginatus	{"vörösszegélyű avarfutó"}
Lepidosaphes conchiformis	{"vörös kommapajzstetű"}
Lepidosaphes newsteadi	{fenyő-kagylóspajzstetű}
Lepidosaphes ulmi	{"közönséges kagylóspajzstetű"}
Lepomis gibbosus	{naphal}
Leptacinus batychrus	{"barázdásfejű rovátkoltholyva"}
Leptacinus intermedius	{"barázdáshomlokú rovátkoltholyva"}
Leptacinus pusillus	{"szurkos rovátkoltholyva"}
Lepthyphantes leprosus	{"csinos vitorlapók"}
Lepthyphantes minutus	{"apró vitorlapók"}
Leptidea morsei	{"keleti mustárlepke"}
Leptidea reali	{Lorkovic-mustárlepke}
Leptidea sinapis	{"kis mustárlepke"}
Leptinus testaceus	{egérbogár}
Leptobium gracile	{"kétfoltos pocsolyaholyva"}
Leptophius flavocinctus	{"alföldi rovátkásholyva"}
Leptophloeus alternans	{fenyő-szegélyeslapbogár}
Leptophloeus clematidis	{iszalag-szegélyeslapbogár}
Leptophloeus hypobori	{füge-szegélyeslapbogár}
Leptophloeus juniperi	{boróka-szegélyeslapbogár}
Leptophyes albovittata	{"közönséges virágszöcske"}
Leptophyes discoidalis	{"erdélyi virágszöcske"}
Leptophyes laticauda	{"szélesfarkú virágszöcske"}
Leptophyes punctatissima	{"pontozott virágszöcske"}
Leptopterna dolabrata	{"közönséges mezeipoloska"}
Leptopterna ferrugata	{"vastagcsápú mezeipoloska"}
Leptopus marmoratus	{kövipoloska}
Leptorchestes berolinensis	{"fekete hangyutánzópók"}
Leptotes pirithous	{"mediterrán márványboglárka"}
Leptusa flavicornis	{"magashegyi tarkaholyva"}
Leptusa fuliginosa	{"kormos tarkaholyva"}
Leptusa fumida	{"szurokszínű tarkaholyva"}
Leptusa pulchella	{"hegyi tarkaholyva"}
Leptusa ruficollis	{"vöröses tarkaholyva"}
Lepus europaeus	{"mezei nyúl"}
Lepyronia coleoptrata	{fűzfa-tajtékoskabóca}
Lepyrus armatus	{"tompafogú fűzormányos"}
Lepyrus capucinus	{"pettyes fűzormányos"}
Lepyrus palustris	{"kétpettyes fűzormányos"}
Lestes barbarus	{"foltosszárnyjegyű rabló"}
Lestes dryas	{"réti rabló"}
Lestes macrostigma	{"nagy foltosrabló"}
Lestes sponsa	{"lomha rabló"}
Lestes virens	{"tavi rabló"}
Lestes virens vestalis	{"tavi rabló"}
Lestes viridis	{"zöld rabló"}
Lesteva longoelytrata	{"partfutó felemásholyva"}
Lesteva punctata	{"csermelyfutó felemásholyva"}
Lestica clypeata	{"közönséges szitár"}
Lethrus apterus	{"nagyfejű csajkó"}
Leucania comma	{"feketecsíkos rétibagoly"}
Leucania obsoleta	{"pontozott rétibagoly"}
Leucaspis pusilla	{"fehér fenyő-pajzstetű"}
Leucaspius delineatus	{"kurta baing"}
Leuciscus idus	{jászkeszeg}
Leuciscus leuciscus	{nyúldomolykó}
Leucohimatium jakowlewi	{"vöröses karcsútarbogár"}
Leucohimatium langei	{"sárgás karcsútarbogár"}
Leucoma salicis	{fűzfaszövő}
Leucophyes pedestris	{"négypettyes répabarkó"}
Leucoptera aceris	{"juharaknázó fehérmoly"}
Leucoptera cytisiphagella	{"erdeilednekevő fehérmoly"}
Leucoptera genistae	{"rekettyerágó fehérmoly"}
Leucoptera heringiella	{"zanótaknázó fehérmoly"}
Leucoptera laburnella	{"aranyesőrágó fehérmoly"}
Leucoptera lotella	{"lednekaknázó fehérmoly"}
Leucoptera lustratella	{"orbáncfűrágó fehérmoly"}
Leucoptera malifoliella	{"lombosfalakó fehérmoly"}
Leucoptera onobrychidella	{"baltacimaknázó fehérmoly"}
Leucoptera sinuella	{"nyáraknázó fehérmoly"}
Leucoptera spartifoliella	{"seprőzanótevő fehérmoly"}
Leucorrhinia caudalis	{tócsaszitakötő}
Leucorrhinia pectoralis	{"lápi szitakötő"}
Leucospilapteryx omissella	{"ürömaknázó hólyagosmoly"}
Libellula depressa	{"laposhasú acsa"}
Libellula fulva	{"mocsári szitakötő"}
Libellula quadrimaculata	{"négyfoltos acsa"}
Lichenophanes varius	{"tarka csuklyásszú"}
Licinus cassideus	{"nagy pajzsosfutó"}
Licinus depressus	{"kis pajzsosfutó"}
Licinus hoffmanseggii	{"erdei pajzsosfutó"}
Ligdia adustata	{"barna levélaraszoló"}
Lignyodes enucleator	{tölgyvirágormányos}
Limacus flavus	{"pince meztelencsiga"}
Limarus maculatus	{"foltosszárnyú trágyabogár"}
Limax cinereoniger	{"óriás meztelencsiga"}
Limax maximus	{"nagy meztelencsiganagy meztelencsiga"}
Limnaecia phragmitella	{nádmoly}
Limnebius papposus	{"suta csiborka"}
Limnichus incanus	{"hamvas partibogár"}
Limnichus pygmaeus	{"közönséges partibogár"}
Limnichus sericeus	{"selymes partibogár"}
Limnius intermedius	{"pataki karmosbogár"}
Limnius volckmari	{"sebesvízi karmosbogár"}
Limnobaris dolorosa	{"szálkás báris"}
Limnobaris t-album	{"réti báris"}
Limnoporus rufoscutellatus	{"sárgahátú molnárpoloska"}
Limobius borealis	{"szőrös gólyaorr-ormányos"}
Limodromus assimilis	{"vöröslábú kisfutó"}
Limodromus krynickii	{"feketelábú kisfutó"}
Limodromus longiventris	{"termetes kisfutó"}
Limoniscus violaceus	{"kék pattanó"}
Limonius minutus	{"fekete bokorpattanó"}
Limotettix striola	{"sávosfejű kabóca"}
Lindenius albilabris	{"bronzos szitár"}
Linyphia hortensis	{"kerti vitorlapók"}
Linyphia triangularis juniperina	{"háromszöges vitorlapók"}
Liocoris tripustulatus	{"hárompettyes mezeipoloska"}
Liogluta alpestris	{"fényeshatú humuszholyva"}
Liogluta granigera	{"avarlakó humuszholyva"}
Liogluta longiuscula	{"fényesfejű humuszholyva"}
Liogluta microptera	{"penészkedvelő humuszholyva"}
Liogluta pagana	{"barna humuszholyva"}
Lionychus quadrillum	{"négyfoltos fövenyfutonc"}
Liophloeus pupillatus	{"pupillás ormányos"}
Liophloeus tessulatus	{"kockás ormányos"}
Liorhyssus hyalinus	{üvegpoloska}
Liothorax niger	{"iszaplakó trágyabogár"}
Liothorax plagiatus	{"réti trágyabogár"}
Liparthrum bartschti	{fagyöngyszú}
Liparus coronatus	{turbolyaormányos}
Liparus dirus	{szerecsenormányos}
Liparus germanus	{acsalapuormányos}
Liparus glabrirostris	{"mintás acsalapuormányos"}
Liparus transsylvanicus	{"erdélyi acsalapuormányos"}
Liris nigra	{tücsökrontó}
Lissodema cursor	{"sokfogú álormányos"}
Lissodema denticolle	{"négyfoltos álormányos"}
Litargus balteatus	{"amerikai gombabogár"}
Litargus connexus	{"szalagos gombabogár"}
Lithacodia uncula	{"lápi apróbagoly"}
Lithocharis nigriceps	{"sárgás rétiholyva"}
Lithocharis ochracea	{"vöröses rétiholyva"}
Lithoglyphus naticoides	{kavicscsiga}
Lithophane furcifera	{"villás fabagoly"}
Lithophane ornitopus	{"közönséges fabagoly"}
Lithophane semibrunnea	{"keskenyszárnyú fabagoly"}
Lithophane socia	{"változékony fabagoly"}
Lithosia quadra	{"négypettyes zuzmószövő"}
Lithostege farinata	{"ezüstfehér araszoló"}
Lithostege griseata	{"szürke araszoló"}
Lixus albomarginatus	{"fehérszegélyes dudvabarkó"}
Lixus angustatus	{"mályvafúró dudvabarkó"}
Lixus angustus	{"rövid dudvabarkó"}
Lixus bardanae	{lórum-dudvabarkó}
Lixus bituberculatus	{"kétpúpú dudvabarkó"}
Lixus brevipes	{"vaskosorrú dudvabarkó"}
Lixus cardui	{"bogáncsfúró dudvabarkó"}
Lixus cribricollis	{sóska-dudvabarkó}
Lixus cylindrus	{"cifra dudvabarkó"}
Lixus elegantulus	{"elegáns dudvabarkó"}
Lixus euphorbiae	{kutyatej-dudvabarkó}
Lixus fasciculatus	{"nyurgalábú dudvabarkó"}
Lixus filiformis	{"karcsú bogáncsfúró-dudvabarkó"}
Lixus iridis	{"bürökfúró dudvabarkó"}
Lixus lateralis	{ternye-dudvabarkó}
Lixus myagri	{"pöttyöshasú dudvabarkó"}
Lixus neglectus	{"mellőzött dudvaormányos"}
Lixus ochraceus	{retek-dudvabarkó}
Lixus paraplecticus	{"fecskefarkú dudvabarkó"}
Lixus punctirostris	{"pontozottorrú dudvabarkó"}
Lixus punctiventris	{aggófű-dudvaormányos}
Lixus rubicundus	{laboda-dudvabarkó}
Lixus scabricollis	{répalevél-barkó}
Lixus subtilis	{disznóparéj-dudvabarkó}
Lixus tibialis	{"rejtőzködő dudvaormányos"}
Lixus vilis	{gémorr-dudvabarkó}
Lobesia abscisana	{"meredeksávos tükrösmoly"}
Lobesia artemisiana	{atracél-tükrösmoly}
Lobesia bicinctana	{"kétcsíkú tükrösmoly"}
Lobesia botrana	{"tarka szőlőmoly"}
Lobesia euphorbiana	{kutyatej-tükrösmoly}
Lobesia reliquana	{"erdei tükrösmoly"}
Lobophora halterata	{"szárnyfüggelékes araszoló"}
Loborhynchapion amethystinum	{csüdfűrágó-cickányormányos}
Lobrathium multipunctum	{"sokpontos mocsárholyva"}
Locusta migratoria	{"keleti vándorsáska"}
Lomaspilis marginata	{"szegélyes nyárfaaraszoló"}
Lomechusa emarginata	{"dudoros pamacsosholyva"}
Lomechusa paradoxa	{"furcsa pamacsosholyva"}
Lomographa bimaculata	{"kétpontos kökényaraszoló"}
Lomographa temerata	{"foltos kökényaraszoló"}
Lopheros rubens	{"cinóbervörös hajnalbogár"}
Lophyridia littoralis	{"sziki homokfutrinka"}
Lophyridia littoralis nemoralis	{"foltos sziki homokfutrinka"}
Lordithon bimaculatus	{"címeres gombászholyva"}
Lordithon exoletus	{"sárgahátú gombászholyva"}
Lordithon lunulatus	{"tarka gombászholyva"}
Lordithon pulchellus	{"szalagos gombászholyva"}
Lordithon speciosus	{"alhavasi gombászholyva"}
Lordithon thoracicus	{"változékony gombászholyva"}
Lordithon trimaculatus	{"szurdoklakó gombászholyva"}
Loricera pilicornis	{"pilláscsápú futó"}
Loricula pselaphiformis	{törpepoloska}
Lota lota	{menyhal}
Loxostege aeruginalis	{"cifra tűzmoly"}
Loxostege deliblatica	{"kénszínű tűzmoly"}
Loxostege manualis	{"kékesszürke tűzmoly"}
Loxostege sticticalis	{muszkamoly}
Loxostege turbidalis	{"ürömrágó tűzmoly"}
Lozekia transsylvanica	{"erdélyi pikkelyescsiga"}
Lozotaenia forsterana	{"turjáni sodrómoly"}
Lucanus cervus	{"nagy szarvasbogár"}
Luperina testacea	{"szürkés fűbagoly"}
Luperina zollikoferi	{óriás-szürkebagoly}
Luquetia lobella	{"kökényszövő laposmoly"}
Luzulaspis jahandiezi	{nád-teknőspajzstetű}
Luzulaspis luzulae	{perjeszittyó-pajzstetű}
Lycaena dispar	{"nagy tűzlepke"}
Lycaena phlaeas	{"lángvörös tűzlepke"}
Lycaena tityrus	{"barna tűzlepke"}
Lycaena virgaureae	{"aranyos tűzlepke"}
Lycia hirtaria	{"közönséges tavasziaraszoló"}
Lycia pomonaria	{hársfa-tavasziaraszoló}
Lycia zonaria	{"öves tavasziaraszoló"}
Lycoperdina bovistae	{"fekete pöfetegálböde"}
Lycoperdina succincta	{"szalagos pöfetegálböde"}
Lycophotia porphyrea	{porfírbagoly}
Lycosa singoriensis	{"szongáriai cselőpók"}
Lyctocoris campestris	{bozót-virágpoloska}
Lyctus linearis	{"közönséges falisztbogár"}
Lyctus pubescens	{"nagy falisztbogár"}
Lydus europeus	{"vöröseshátú hólyaghúzó"}
Lydus trimaculatus	{"háromfoltos hólyaghúzó"}
Lygaeosoma sardeum	{"közönséges bodobács"}
Lygaeus equestris	{lovagbodobács}
Lygephila craccae	{bükkönybagoly}
Lygephila ludicra	{"keskenyszárnyú csüdfűbagoly"}
Lygephila lusoria	{"nagy csűdfűbagoly"}
Lygephila pastinum	{"kis csűdfűbagoly"}
Lygephila procax	{"sötét csüdfűbagoly"}
Lygephila viciae	{csűdfűbagoly}
Lygistopterus sanguineus	{vérbogár}
Lygocoris pabulinus	{"smaragd mezeipoloska"}
Lygus gemellatus	{"világoszöld mezeipoloska"}
Lygus pratensis	{"változó mezeipoloska"}
Lygus rugulipennis	{"molyhos mezeipoloska"}
Lymantor coryli	{mogyorószú}
Lymantria dispar	{"erdei gyapjaslepke"}
Lymantria monacha	{apácalepke}
Lymexylon navale	{"hajófúró bogár"}
Lymnaea stagnalis	{iszapcsiga}
Lymnastis dieneri	{"budai fürgefutonc"}
Lymnastis galilaeus	{"déli fürgefutonc"}
Lyonetia clerkella	{"kígyóaknás ezüstmoly"}
Lyonetia ledi	{tőzegeper-ezüstmoly}
Lyonetia prunifoliella	{rózsalevél-ezüstmoly}
Lyprocorrhe anceps	{"hangyász penészholyva"}
Lythria cruentaria	{"bíborcsíkos araszoló"}
Lythria purpuraria	{"bíborsávos araszoló"}
Lytta vesicatoria	{kőrisbogár}
Macaria alternata	{"közönséges szürkearaszoló"}
Macaria artesiaria	{"ibolyás szürkearaszoló"}
Macaria brunneata	{"okkermintás hegyiaraszoló"}
Macaria liturata	{"rozsdasávos szürkearaszoló"}
Macaria notata	{"foltos szürkearaszoló"}
Macaria signaria	{"fenyves szürkearszoló"}
Macaria wauaria	{"w-betűs szürkearaszoló"}
Macaroeris nidicolens	{"fészkes ugrópók"}
Macdunnoughia confusa	{"cseppfoltú aranybagoly"}
Macratria hungarica	{"magyar fürgebogár"}
Macrochilo cribrumalis	{"csontszínű karcsúbagoly"}
Macrogastra plicatula	{orsócsiga}
Macrogastra plicatula rusiostoma	{orsócsiga}
Macrogastra ventricosa	{"nagy orsócsiga"}
Macroglossum stellatarum	{"kacsafarkú szender"}
Macronychus quadrituberculatus	{"négypúpú karmosbogár"}
Macrophagus robustus	{"nyugati gencsér"}
Macroplax preyssleri	{"fekete bodobács"}
Macropsis marginata	{"változó kabóca"}
Macrosiagon tricuspidatum	{"sarkantyús darázsbogár"}
Macrosteles sexnotatus	{"feketefoltos gabonakabóca"}
Macrothylacia rubi	{málnapohók}
Macrotylus herrichi	{zsálya-mezeipoloska}
Macrotylus horvathi	{Horváth-poloska}
Magdalis barbicornis	{almafaormányos}
Magdalis cerasi	{meggyfaormányos}
Magdalis duplicata	{"halványcsíkú fenyőormányos"}
Magdalis frontalis	{"fényescsíkú fenyőormányos"}
Magdalis linearis	{"egyenes fenyőormányos"}
Magdalis memnonia	{"fekete fenyőormányos"}
Magdalis phlegmatica	{"kékes fenyőormányos"}
Magdalis rufa	{"barnásvörös fenyőormányos"}
Magdalis ruficornis	{"sárgacsápú gyümölcsfaormányos"}
Magdalis violacea	{"ibolyaszínű fenyőormányos"}
Malachius aeneus	{"bibircses bogár"}
Malachius bipustulatus	{"kétfoltos bibircsbogár"}
Malacocoris chlorizans	{almapoloska}
Malacosoma castrense	{kutyatejszövő}
Malacosoma neustria	{gyűrűsszövő}
Maladera holosericea	{"bársonyos kiscserebogár"}
Malthodes marginatus	{"törpe lágybogár"}
Malthonica campestris	{"mezei zugpók"}
Malthonica ferruginea	{"hegyi zugpók"}
Malthonica silvestris	{"erdei zugpók"}
Malvaevora timida	{mályvabáris}
Malvapion malvae	{mályva-cickányormányos}
Mamestra brassicae	{káposzta-veteménybagoly}
Manda mandibularis	{"nagy aknászholyva"}
Mangora acalypha	{"réti keresztespók"}
Maniola jurtina	{"nagy ökörszemlepke"}
Mantispa aphavexelte	{"mediterrán fogólábú-fátyolka"}
Mantispa perla	{"füstösszárnyú fogólábú-fátyolka"}
Marasmarcha lunaedactyla	{"félholdas tollasmoly"}
Margarinotus bipustulatus	{"vörösfoltos sutabogár"}
Margarinotus brunneus	{"dögjáró sutabogár"}
Margarinotus carbonarius	{"szénfekete sutabogár"}
Margarinotus ignobilis	{"rózsadombi sutabogár"}
Margarinotus marginatus	{"szegélyes sutabogár"}
Margarinotus merdarius	{"trágyabújó sutabogár"}
Margarinotus neglectus	{korhadék-sutabogár}
Margarinotus obscurus	{"ganajtúró sutabogár"}
Margarinotus punctiventer	{"pontoshasú sutabogár"}
Margarinotus purpurascens	{"vöröslő sutabogár"}
Margarinotus ruficornis	{"piroscsápú sutabogár"}
Margarinotus striola succicola	{"vonalkás sutabogár"}
Margarinotus terricola	{vakondok-sutabogár}
Margarinotus ventralis	{"kerekded sutabogár"}
Marpissa muscosa	{"mohos ugrópók"}
Marpissa radiata	{"sugaras ugrópók"}
Martes foina	{nyest}
Martes martes	{nyuszt}
Marthamea vitripennis	{"folyólakó nagyálkérész"}
Masoreus wetterhallii	{"fürge homokfutó"}
Matilella fusca	{"barnásfekete karcsúmoly"}
Matratinea rufulicaput	{"magyar fészekmoly"}
Matsucoccus matsumurae	{fenyő-kéregpajzstetű}
Mecaspis alternans	{keserűgyökér-barkó}
Mecinus caucasicus	{"kaukázusi útifűormányos"}
Mecinus circulatus	{"sávos útifűormányos"}
Mecinus collaris	{"örvös útifűormányos"}
Mecinus heydeni	{heyden-útifűormányos}
Mecinus ictericus	{"bundás útifűormányos"}
Mecinus janthinus	{"kék gyujtoványfű-ornányos"}
Mecinus labilis	{"tarka útifűormányos"}
Mecinus pascuorum	{"közönséges útifűormányos"}
Mecinus plantaginis	{"törpe útifűormányos"}
Mecinus pyraster	{"szélesnyakú útifűormányos"}
Meconema meridionale	{"déli néma szöcske"}
Meconema thalassinum	{"néma szöcske"}
Mecorhis ungarica	{"magyar eszelény"}
Mecostethus parapleurus	{"hagymazöld sáska"}
Mecyna flavalis	{"csalánszövő tűzmoly"}
Mecyna lutealis	{"citromszínű tűzmoly"}
Mecyna trinalis	{tetemtoldó-tűzmoly}
Mecynotarsus serricornis	{"kis nyakszarvúbogár"}
Mediterranea depressa	{"lapos kristálycsiga"}
Medon apicalis	{"rozsdaszínű lombholyva"}
Medon brunneus	{"barna lombholyva"}
Medon dilutus	{"fakó lombholyva"}
Medon ferrugineus	{"vöröses lombholyva"}
Medon fusculus	{"barnás lombholyva"}
Megachile centuncularis	{rózsaméh}
Megachile lagopoda	{"óriás szabóméh"}
Megachile leachella	{"kis szabóméh"}
Megachile maritima	{"iglice szabóméh"}
Megachile willoughbiella	{"piroshasú szabóméh"}
Megacraspedus binotella	{"kétpettyes kopármoly"}
Megacraspedus dolosellus	{"füvönélő kopármoly"}
Megacraspedus imparellus	{"balkáni kopármoly"}
Megacraspedus separatellus	{"apró kopármoly"}
Megalepthyphantes nebulosus	{"kóbor vitorlapók"}
Megalocoleus exsanguis	{pázsitpoloska}
Megalonotus praetextatus	{"tüskéslábú bodobács"}
Megalophanes viciella	{"útszéli zsákhordólepke"}
Meganephria bimaculosa	{"kétfoltos szilbagoly"}
Meganola albula	{"fehér pamacsosszövő"}
Meganola kolbi	{"szélesszárnyú pamacsosszövő "}
Meganola strigula	{"hamvas pamacsosszövő"}
Meganola togatulalis	{kökény-pamacsosszövő}
Megapenthes lugens	{"hegyesszárnyú pattanó"}
Megarthrus bellevoyei	{"érdes sutaholyva"}
Megarthrus denticollis	{"csipkéshátú sutaholyva"}
Megarthrus hemipterus	{"vöröses sutaholyva"}
Megarthrus proseni	{"kerekhátú sutaholyva"}
Megascolia maculata	{óriás-tőrösdarázs}
Megascolia maculata flavifrons	{óriás-tőrösdarázs}
Megasternum concinnum	{"gombaevő csiborka"}
Megatoma ruficornis	{"vöröscsápú porva"}
Megatoma undata	{"kétszalagos porva"}
Melaleucus picturatus	{"tarka báris"}
Melanapion minimum	{fűz-cickányormányos}
Melanargia galathea	{sakktáblalepke}
Melanargia russiae	{"magyar sakktáblalepke"}
Melanchra persicariae	{"fehérfoltos veteménybagoly"}
Melandrya barbata	{"vöröslábú komorka"}
Melandrya caraboides	{"fémkék komorka"}
Melandrya dubia	{"fekete komorka"}
Melanimon tibiale	{"gyászos homokbogár"}
Melanobaris atramentaria	{"fekete báris"}
Melanobaris carbonaria	{"nagy báris"}
Melanobaris dalmatina	{dalmát-báris}
Melanobaris laticollis	{"lakkfényű káposztabáris"}
Melanocoryphus albomaculatus	{"nagy fehérfoltos-bodobács"}
Melanogryllus desertus	{"fekete vagy pusztai tücsök"}
Melanophila acuminata	{"tövises fürgedíszbogár"}
Melanopsacus grenieri	{grenier-orrosbogár}
Melanotus brunnipes	{"barnalábú gyászpattanó"}
Melanotus castanipes	{"nyugati gyászpattanó"}
Melanotus crassicollis	{"vállas gyászpattanó"}
Melanotus punctolineatus	{"sávos gyászpattanó"}
Melanotus tenebrosus	{"réti gyászpattanó"}
Melanotus villosus	{"vöröslábú gyászpattanó"}
Melanthia procellata	{"hullámos tarkaarszoló"}
Melasis buprestoides	{"gyakori tövisnyakúbogár"}
Melecta albifrons	{"nagy gyászméh"}
Meles meles	{"eurázsiai borz"}
Meliboeus amethystinus	{"ametiszt tompadíszbogár"}
Meliboeus fulgidicollis	{"aranyostorú tompadíszbogár"}
Meliboeus graminis	{üröm-tompadíszbogár}
Meliboeus subulatus	{"zöldes tompadíszbogár"}
Melicius cylindrus	{"barna szúormányos"}
Meligethes acicularis	{kakukkfű-fénybogár}
Meligethes aeneus	{repce-fénybogár}
Meligethes assimilis	{"sárgaszőrű fénybogár"}
Meligethes atramentarius	{"erdei fénybogár"}
Meligethes atratus	{rózsa-fénybogár}
Meligethes bidens	{pereszlény-fénybogár}
Meligethes bidentatus	{rekettye-fénybogár}
Meligethes brachialis	{koronafürt-fénybogár}
Meligethes brevis	{szürkenapvirág-fénybogár}
Meligethes brunnicornis	{"barnacsápú fénybogár"}
Meligethes carinulatus	{kerep-fénybogár}
Meligethes coeruleovirens	{kakukktorma-fénybogár}
Meligethes coracinus	{"ólmos fénybogár"}
Meligethes corvinus	{harangvirág-fénybogár}
Meligethes czwalinai	{holdviola-fénybogár}
Meligethes denticulatus	{szeder-fénybogár}
Meligethes difficilis	{"vörösbarna fénybogár"}
Meligethes discoideus	{repcsény-fénybogár}
Meligethes distinctus	{gamandor-fénybogár}
Meligethes egenus	{"szerény fénybogár"}
Meligethes erichsoni	{patkócim-fénybogár}
Meligethes exilis	{"szikár fénybogár"}
Meligethes flavimanus	{"sárgalábú fénybogár"}
Meligethes gagathinus	{menta-fénybogár}
Meligethes haemorrhoidalis	{"hegyi fénybogár"}
Meligethes hoffmanni	{"mocsári fénybogár"}
Meligethes incanus	{macskamenta-fénybogár}
Meligethes jejunus	{kígyószisz-fénybogár}
Meligethes kraatzi	{zsombor-fénybogár}
Meligethes lepidii	{zsázsa-fénybogár}
Meligethes lugubris	{"ráncos fénybogár"}
Meligethes matronalis	{estike-fénybogár}
Meligethes maurus	{zsálya-fénybogár}
Meligethes morosus	{"sötétbarna fénybogár"}
Meligethes nanus	{pemetefű-fénybogár}
Meligethes nigrescens	{lóhere-fénybogár}
Meligethes ochropus	{"okkerlábú fénybogár"}
Meligethes ovatus	{repkény-fénybogár}
Meligethes persicus	{"perzsa fénybogár"}
Meligethes planiusculus	{"keskeny fénybogár"}
Meligethes rosenhaueri	{atracél-fénybogár}
Meligethes rotundicollis	{"kereknyakú fénybogár"}
Meligethes ruficornis	{peszterce-fénybogár}
Meligethes serripes	{"fűrészeslábú fénybogár"}
Meligethes solidus	{molyhosnapvirág-fénybogár}
Meligethes subaeneus	{"fémes fénybogár"}
Meligethes submetallicus	{levendula-fénybogár}
Meligethes subrugosus	{kékcsillag-fénybogár}
Meligethes sulcatus	{árvacsalán-fénybogár}
Meligethes symphyti	{nadálytő-fénybogár}
Meligethes tristis	{"szomorú fénybogár"}
Meligethes umbrosus	{"gyászos fénybogár"}
Meligethes variolosus	{"vöröses fénybogár"}
Meligethes villosus	{"bundás fénybogár"}
Meligethes viridescens	{"zöldes fénybogár"}
Melinopterus consputus	{"maszatos trágyabogár"}
Melinopterus prodromus	{"sárgalábú trágyabogár"}
Melinopterus pubescens	{"selymes trágyabogár"}
Melinopterus punctatosulcatus	{"pusztai trágyabogár"}
Melinopterus reyi	{"fakó trágyabogár"}
Melinopterus sphacelatus	{"szegettnyakú trágyabogár"}
Melitaea athalia	{"közönséges tarkalepke"}
Melitaea aurelia	{"recés tarkalepke"}
Melitaea britomartis	{"barnás tarkalepke"}
Melitaea cinxia	{"réti tarkalepke"}
Melitaea diamina	{"kockás tarkalepke"}
Melitaea didyma	{"tüzes tarkalepke"}
Melitaea phoebe	{"nagy tarkalepke"}
Melitaea punica	{"magyar fakó tarkalepke"}
Melitaea punica telona	{"magyar tarkalepke"}
Melitaea trivia	{"kis tarkalepke"}
Melitta leporina	{"lucerna földiméh"}
Melitturga clavicornis	{"bunkóscsápú méh"}
Meloe autumnalis	{"őszi nünüke"}
Meloe brevicollis	{"tar nünüke"}
Meloe hungarus	{"magyar nünüke"}
Meloe mediterraneus	{"déli nünüke"}
Meloe proscarabaeus	{"közönséges nünüke"}
Meloe rugosus	{"ráncos nünüke"}
Meloe scabriusculus	{"érdes nünüke"}
Meloe tuccius	{"gödörkés nünüke"}
Meloe uralensis	{"uráli nünüke"}
Meloe variegatus	{"pompás nünüke"}
Meloe violaceus	{"kék nünüke"}
Melolontha hippocastani	{"erdei cserebogár"}
Melolontha melolontha	{"májusi cserebogár"}
Melolontha pectoralis	{"hamvas cserebogár"}
Menephilus cylindricus	{"fogasnyakú gyászbogár"}
Meotica exilis	{"közönséges televényholyva"}
Meotica filiformis	{"busafejű televényholyva"}
Meotica pallens	{"ligeti televényholyva"}
Merdigera obscura	{csavarcsiga}
Mergellus albellus	{"kis bukó"}
Meridiophila fascialis	{"csíkos kormosmoly"}
Merrifieldia baliodactylus	{kakukkfű-tollasmoly}
Merrifieldia leucodactyla	{"barnacsápú tollasmoly"}
Merrifieldia malacodactylus	{"dunántúli tollasmoly"}
Merrifieldia tridactyla	{"sárgásbarna tollasmoly"}
Mesagroicus obscurus	{homályormányos}
Mesapamea secalis	{gabonabagoly}
Mesocoelopus niger	{borostyánálszú}
Mesocrambus candiellus	{"buckajáró fűgyökérmoly"}
Mesogona acetosellae	{madársóskabagoly}
Mesogona oxalina	{"sárga fűzbagoly"}
Mesoleuca albicillata	{"tarka fűzaraszoló"}
Mesoligia furuncula	{"kétszínű apróbagoly"}
Mesophleps silacella	{tetemtoldó-sarlósmoly}
Mesothes ferrugineus	{paratölgyálszú}
Mesotrichapion punctirostre	{"pontosorrú cickányormányos"}
Mesotype didymata	{"sötétfoltos araszoló"}
Mesovelia furcata	{"vízenjáró poloska"}
Metacantharis clypeata	{"tavaszi lágybogár"}
Metachrostis dardouini	{"palakék törpebagoly"}
Metaclisa azurea	{"azúrkék gyászbogár"}
Metacrambus carectellus	{"homoki fűgyökérmoly"}
Metalampra cinnamomea	{"fahéjbarna díszmoly"}
Metallina lampros	{"erdei gyorsfutó"}
Metallina properans	{"parlagi gyorsfutó"}
Metallina pygmaea	{"bronzfényű gyorsfutó"}
Metallina splendida	{"rézfényű gyorsfutó"}
Meta menardi	{"barlangi keresztespók"}
Metanomus infuscatus	{"füstös pattanó"}
Metasia ophialis	{"kígyósávos tűzmoly"}
Metatropis rufescens	{boszorkánypoloska}
Metaxmeste phrygialis	{"havasi kormosmoly"}
Metellina merianae	{"rejtett keresztespók"}
Metellina segmentata	{"gyűrűs keresztespók"}
Metendothenia atropunctana	{"pettyes tükrösmoly"}
Methocha ichneumonides	{"cicindela pusztító"}
Methorasa latreillei	{"gyöngypettyes bagoly"}
Metoecus paradoxus	{"tollascsápú darázsbogár"}
Metopoplax origani	{"szélesfejű bodobács"}
Metopsia similis	{"pajzsos sutaholyva"}
Metreletus balcanicus	{magyarkérész}
Metrioptera bicolor	{"halványzöld rétiszöcske"}
Metrioptera brachyptera	{"sötétzöld rétiszöcske"}
Metriotes lutarea	{"zöldessárga zsákosmoly"}
Metzneria aestivella	{bábakalács-sarlósmoly}
Metzneria aprilella	{"tüzesszárnyú sarlósmoly"}
Metzneria artificella	{"piroscsíkos sarlósmoly"}
Metzneria ehikeella	{"homokháti sarlósmoly"}
Metzneria intestinella	{"délvidéki sarlósmoly"}
Metzneria lappella	{bojtorjánmag-sarlósmoly}
Metzneria metzneriella	{imolamag-sarlósmoly}
Metzneria neuropterella	{"barnarácsos sarlósmoly"}
Metzneria paucipunctella	{pipitérmoly}
Metzneria santolinella	{"északi sarlósmoly"}
Metzneria subflavella	{"sárgás sarlósmoly"}
Mezira tremulae	{"nagy rücsköspoloska"}
Mezium americanum	{"amerikai hólyagbogár"}
Miarus ajugae	{"széles harangvirág-ormányos"}
Miarus banaticus	{"bánáti harangvirág-ormányos"}
Miarus monticola	{"hegyi harangvirág-ormányos"}
Miarus ursinus	{"barnaszőrű harangvirág-ormányos"}
Micilus murinus	{"egérszínű iszapbogár"}
Micrambe bimaculata	{"kétfoltos penészbogár"}
Micrelus ericae	{csarabormányos}
Microcara testacea	{"nagy rétbogár"}
Microdota aegra	{"kétszínű penészholyva"}
Microdota amicula	{"kis penészholyva"}
Microdota benickiella	{"barnásvörös penészholyva"}
Microdota ganglbaueri	{"trágyatúró penészholyva"}
Microdota indubia	{"sötét penészholyva"}
Microdota pittionii	{"satnya penészholyva"}
Microhoria nectarina	{"nagy fürgebogár"}
Microhoria unicolor	{"fekete fürgebogár"}
Microlestes corticalis	{"sziki parányfutó"}
Microlestes corticalis escorialensis	{"sziki parányfutó"}
Microlestes fissuralis	{"vörhenyes parányfutó"}
Microlestes fulvibasis	{"alföldi parányfutó"}
Microlestes maurus	{"mór parányfutó"}
Microlestes minutulus	{"nagy parányfutó"}
Microlestes plagiatus	{"foltos parányfutó"}
Microlestes schroederi	{Schröder-parányfutó}
Microlinyphia pusilla	{"kicsiny vitorlapók"}
Micrommata virescens	{hunyópók}
Micromys minutus	{törpeegér}
Micronecta griseola	{"törpe búvárpoloska"}
Micronecta minutissima	{"parányi búvárpoloska"}
Micronecta poweri	{"dalos búvárpoloska"}
Micropeplus fulvus	{"kis bordásholyva"}
Micropeplus marietti	{"közönséges bordásholyva"}
Micropeplus porcatus	{"érdes bordásholyva"}
Micropeplus tesserula	{"fényes bordásholyva","fényes bordásholyva"}
Microplontus campestris	{"réti margitvirág-ormányos"}
Microplontus edentulus	{ebszékfű-ormányos}
Microplontus millefolii	{gilisztaűző-varádicsormányos}
Microplontus molitor	{pipitérszépe-ormányos}
Microplontus rugulosus	{székfű-ormányos}
Microplontus triangulum	{"tarka cickafark-ormányos"}
Microporus nigrita	{"homoki földipoloska"}
Micropterix aruncella	{"ezüstfoltos ősmoly"}
Micropterix aureatella	{"aranyszárnyú ősmoly"}
Micropterix calthella	{"törpe ősmoly"}
Micropterix mansuetella	{"feketefejű ősmoly"}
Micropterix myrtetella	{"apró ősmoly"}
Micropterix schaefferi	{"ibolyás ősmoly"}
Micropterix tunbergella	{"vöröses ősmoly"}
Micropterus salmoides	{pisztrángsügér}
Microrhagus emyi	{"törpe tövisnyakúbogár"}
Microrhagus lepidus	{"csinos tövisnyakúbogár"}
Microrhagus pygmaeus	{"apró tövisnyakúbogár"}
Microtus agrestis	{"csalitjáró pocok"}
Microtus arvalis	{"mezei pocok"}
Microtus oeconomus	{"északi pocok"}
Microtus oeconomus mehelyi	{"északi pocok"}
Microtus subterraneus	{"földi pocok"}
Microvelia reticulata	{"törpe víztaposó"}
Micrurapteryx kollariella	{zanótaknázó-hólyagosmoly}
Millieria dolosalis	{farkasalmamoly}
Miltochrista miniata	{"piros medvelepke"}
Mimas tiliae	{hársfaszender}
Mimela aurata	{"nyugati szipoly"}
Mimumesa littoralis	{"vöröslábú darázs"}
Minetia adamczewskii	{"lengyel csíkosmoly"}
Minetia criella	{"barna csíkosmoly"}
Minetia crinitus	{"fehér csíkosmoly"}
Minetia labiosella	{"sárgás csíkosmoly"}
Minicia marginella	{"gombafejű pók"}
Miniopterus schreibersii	{"hosszúszárnyú denevér"}
Minoa murinata	{kutyatej-araszoló}
Minois dryas	{"fekete szemeslepke"}
Minucia lunaris	{"nagy foltosbagoly"}
Minyops carinatus	{"bordás ormányos"}
Miramella alpina	{"alpesi sáska"}
Mirificarma cytisella	{"zanótszövő sarlósmoly"}
Mirificarma eburnella	{"rozsdaszínű sarlósmoly"}
Mirificarma lentiginosella	{seprőzanót-sarlósmoly}
Mirificarma maculatella	{"feketepettyes sarlósmoly"}
Mirificarma mulinella	{seprőzanótvirág-sarlósmoly}
Miris striatus	{"sávos mezeipoloska"}
Mirococcopsis stipae	{árvalányhaj-pajzstetű}
Misumena vatia	{"viráglakó karolópók"}
Mniotype adusta	{"hegyi őszibagoly"}
Mocyta fungi	{"közönséges komposztholyva"}
Mocyta negligens	{"sárgás komposztholyva"}
Mocyta orbata	{"fényeshátú komposztholyva"}
Mocyta orphana	{"aprócska komposztholyva"}
Modicogryllus frontalis	{"homlokjegyes tücsök"}
Mogulones abbreviatulus	{"óvatos nadálytőormányos"}
Mogulones euphorbiae	{nefelejcs-ormányos}
Mogulones geographicus	{"térképes ormányos"}
Mogulones hungaricus	{"magyar szeplőlapú-ormányos"}
Mogulones javetii	{atracélormányos}
Mogulones raphani	{"kis nadálytőormányos"}
Mohelnaspis massiliensis	{fű-kagylóspajzstetű}
Molops elatus	{"nagy zömökfutó"}
Molops piceus	{"kis zömökfutó"}
Molops piceus austriacus	{"nyugati kis zömökfutó"}
Moma alpium	{"tarka zöldbagoly"}
Mompha bradleyi	{"angol lándzsásmoly"}
Mompha divisella	{"füzikefúró lándzsásmoly"}
Mompha epilobiella	{"agyagsárga lándzsásmoly"}
Mompha idaei	{"derécerágó lándzsásmoly"}
Mompha lacteella	{"füzikelakó lándzsásmoly"}
Mompha langiella	{"fekete lándzsásmoly"}
Mompha locupletella	{"deréceaknázó lándzsásmoly"}
Mompha miscella	{"napvirágfúró lándzsásmoly"}
Mompha ochraceella	{"okkerszínű lándzsásmoly"}
Mompha propinquella	{"füzikeaknázó lándzsásmoly"}
Mompha raschkiella	{"derécefúró lándzsásmoly"}
Mompha sturnipennella	{derécemag-lándzsásmoly}
Mompha subbistrigella	{"kétsávos lándzsásmoly"}
Mompha terminella	{varázslófű-lándzsásmoly}
Monacha cartusiana	{"tejfehér csiga"}
Monachoides incarnatus	{"vörösínyű csiga"}
Monachoides vicinus	{"pikkelyes csiga"}
Monalocoris filicis	{páfránypoloska}
Monochamus galloprovincialis	{"foltos fenyvescincér"}
Monochroa arundinetella	{"sásaknázó lápimoly"}
Monochroa conspersella	{"fehérgyűrűs lápimoly"}
Monochroa cytisella	{saspáfrány-sarlósmoly}
Monochroa divisella	{"magyar lápimoly"}
Monochroa elongella	{"keskenyszárnyú lápimoly"}
Monochroa hornigi	{keserűfű-lápimoly}
Monochroa lucidella	{"sárgafoltos lápimoly"}
Monochroa lutulentella	{"okkerbarna lápimoly"}
Monochroa niphognatha	{"bátorligeti lápimoly"}
Monochroa nomadella	{"ólomszürke lápimoly"}
Monochroa palustrellus	{"turjáni lápimoly"}
Monochroa parvulata	{"karsztlakó sarlósmoly"}
Monochroa rumicetella	{juhsóska-lápimoly}
Monochroa sepicolella	{"karszterdei sarlósmoly"}
Monochroa servella	{"fehérképű lápimoly"}
Monochroa simplicella	{"homokszínű lápimoly"}
Monochroa tenebrella	{"sóskafúró lápimoly"}
Mononychus punctumalbum	{"egykarmú ormányos"}
Monopis crocicapitella	{"sárgás ablakosmoly"}
Monopis fenestratella	{"korhadéklakó ablakosmoly"}
Monopis imella	{"szarurágó ablakosmoly"}
Monopis laevigella	{hulladékmoly}
Monopis monachella	{apácamoly}
Monopis obviella	{"közönséges ablakosmoly"}
Monopis weaverella	{"foltos hulladékmoly"}
Monosteira unicostata	{"tüskésfejű csipkéspoloska"}
Montescardia tessulatellus	{"havasi óriásmoly"}
Mordella aculeata	{"közönséges maróka"}
Mordellistena micans	{"fényes tövisesbogár"}
Mordellistena neuwaldeggiana	{"rőt tövisesbogár"}
Mordellistena pumila	{"selymes tövisesbogár"}
Mordellistena variegata	{"sávos tövisesbogár"}
Mordellochroa abdiminalis	{"vöröshasú tövisesbogár"}
Morimus asper funereus	{gyászcincér}
Morlina glabra striaria	{"átlátszó csiga"}
Mormo maura	{gyászbagoly}
Morophaga choragella	{"közönséges óriásmoly"}
Morychus aeneus	{"fémes labdacsbogár"}
Muscardinus avellanarius	{"mogyorós pele"}
Musculium lacustre	{"törékeny kagyló"}
Mus musculus	{"házi egér"}
Mus spicilegus	{"güzü egér"}
Mustela erminea	{hermelin}
Mustela eversmanii	{molnárgörény}
Mustela eversmanii hungarica	{molnárgörény}
Mustela nivalis vulgaris	{menyét}
Mycetaea subterranea	{"szőrös álböde"}
Mycetina cruciata	{"keresztes álböde"}
Mycetochara axillaris	{"fényes taplász"}
Mycetochara flavipes	{"sárgalábú taplász"}
Mycetochara humeralis	{"feketehasú taplász"}
Mycetochara linearis	{"fekete taplász"}
Mycetochara pygmaea	{"feketecombú taplász"}
Mycetochara quadrimaculata	{"négyfoltos taplász"}
Mycetoma suturale	{"sávos komorka"}
Mycetophagus ater	{"fekete gombabogár"}
Mycetophagus atomarius	{"tarka gombabogár"}
Mycetophagus decempunctatus	{"tízpettyes gombabogár"}
Mycetophagus fulvicollis	{"sárganyakú gombabogár"}
Mycetophagus multipunctatus	{"sokpettyes gombabogár"}
Mycetophagus piceus	{"hegyi gombabogár"}
Mycetophagus populi	{nyárfa-gombabogár}
Mycetophagus quadriguttatus	{"domború gombabogár"}
Mycetophagus quadripustulatus	{"négyfoltos gombabogár"}
Mycetophagus salicis	{"ligeti gombabogár"}
Mycetoporus bimaculatus	{"taplókedvelő gombászholyva"}
Mycetoporus clavicornis	{"bunkóscsápú gombászholyva"}
Mycetoporus corpulentus	{"vaskos gombászholyva"}
Mycetoporus eppelsheimianus	{"avarlakó gombászholyva"}
Mycetoporus erichsonanus	{"szenes gombászholyva"}
Mycetoporus forticornis	{"vastagcsápú gombászholyva"}
Mycetoporus gracilis	{"karcsú gombászholyva"}
Mycetoporus lepidus	{"barna gombászholyva"}
Mycetoporus longulus	{"nyurga gombászholyva"}
Mycetoporus mulsanti	{"sárgás gombászholyva"}
Mycetoporus niger	{"fekete gombászholyva"}
Mycetoporus nigricollis	{"feketehátú gombászholyva"}
Mycetoporus piceolus	{"szurkos gombászholyva"}
Mycetoporus punctipennis	{"pontozott gombászholyva"}
Mycetoporus punctus	{"hegyi gombászholyva"}
Mycetoporus rufescens	{"vöröses gombászholyva"}
Mycetota fimorum	{"érdes komposztholyva"}
Mycetota laticollis	{"sömörös komposztholyva"}
Mychothenus minutus	{törpe-álböde}
Mycterodus confusus	{"csőrös pajzsoskabóca"}
Mycterus tibialis	{"szőrös álzsizsik"}
Myelois circumvoluta	{"pettyes karcsúmoly"}
Mylabris crocata	{"pettyes hólyaghúzó"}
Mylabris pannonica	{"pannon hólyaghúzó"}
Mylabris variabilis	{"szalagos hólyaghúzó"}
Myllaena brevicornis	{"vöröses moszatholyva"}
Myllaena dubia	{"mocsári moszatholyva"}
Myllaena gracilis	{"kecses moszatholyva"}
Myllaena infuscata	{"rövidszárnyú moszatholyva"}
Myllaena intermedia	{"közönséges moszatholyva"}
Myllaena minuta	{"apró moszatholyva"}
Mylopharyngodon piceus	{"fekete amur"}
Myndus musivus	{fűzfa-recéskabóca}
Myotis alcathoe	{nimfadenevér}
Myotis bechsteinii	{"nagyfülű denevér"}
Myotis blythii oxygnathus	{"hegyesorrú denevér"}
Myotis brandtii	{Brandt-denevér}
Myotis dasycneme	{"tavi denevér"}
Myotis daubentonii	{"vízi denevér"}
Myotis emarginatus	{"csonkafülű denevér"}
Myotis myotis	{"közönséges denevér"}
Myotis mystacinus	{"bajuszos denevér"}
Myrmarachne formicaria	{"nagy hangyautánzópók"}
Myrmechixenus subterraneus	{"hangyász gyászbogár"}
Myrmechixenus vaporarium	{"korhadéklakó gyászbogár"}
Myrmecophilus acervorum	{"hangyász tücsök"}
Myrmecoris gracilis	{hangyapoloska}
Myrmecozela ochraceella	{hangyabolymoly}
Myrmeleon inconspicuus	{"homoki hangyaleső"}
Myrmeleotettix antennatus	{"homoki bunkóscsápúsáska"}
Myrmeleotettix maculatus	{"kis bunkóscsápúsáska"}
Myrmetes paykulli	{vöröshangya-sutabogár}
Myrmilla calva	{"déli álhangya"}
Myrmoecia plicata	{"dudoros hangyászholyva"}
Myrmosa atra	{"barnalábú álhangya"}
Myrmosa melanocephala	{"feketefejű álhangya"}
Myrrha octodecimguttata	{"tizennyolccseppes füsskata"}
Mythimna albipuncta	{"fehérpontos rétibagoly"}
Mythimna conigera	{"narancssárga rétibagoly"}
Mythimna ferrago	{"rozsdaszínű rétibagoly"}
Mythimna impura	{"barna rétibagoly"}
Mythimna l-album	{"l-betűs rétibagoly"}
Mythimna pallens	{"halovány rétibagoly"}
Mythimna pudorina	{"vörös rétibagoly"}
Mythimna straminea	{"szalmaszínű rétibagoly"}
Mythimna turca	{"félholdas rértibagoly"}
Mythimna unipuncta	{"vándor rétibagoly"}
Mythimna vitellina	{"sárga rétibagoly"}
Myzia oblongoguttata	{"sávos füsskata"}
Nabis brevis	{"réti tolvajpoloska"}
Nabis ferus	{"közönséges tolvajpoloska"}
Nabis flavomarginatus	{"szegélyes tolvajpoloska"}
Nabis pseudoferus	{"mezei tolvajpoloska"}
Nabis rugosus	{"foltoslábú tolvajpoloska"}
Nacerdes carniolica	{"karnióliai álcincér"}
Nacerdes melanura	{"feketevégű álcincér"}
Naenia typica	{"hálózatos sóskabagoly"}
Nagusta goedelii	{"tüskés rablópoloska"}
Nalassus convexus	{"domború gyászbogár"}
Nalassus dermestoides	{"rövidszárnyú gyászbogár"}
Nannospalax leucodon	{"nyugati földikutya"}
Nanodiscus transversus	{tamariska-ormányos}
Nanomimus anulatus	{"nagy füzényormányos","fekete füzényormányos"}
Narraga fasciolaria	{"kis barna-tarkaaraszoló"}
Narraga tessularia	{"sziki apróaraszoló"}
Narycia astrella	{"fehérfejű zsákhordólepke"}
Narycia duplicella	{"fehérsávos zsákhordólepke"}
Nascia cilialis	{"sásrágó tűzmoly"}
Neatus picipes	{fűz-gyászbogár}
Nebria brevicollis	{"rövidnyakú partfutó"}
Nebria livida	{"szegélyes partfutó"}
Nebria rufescens	{"fekete partfutó"}
Necrobia ruficollis	{"vörösnyakú hullabogár"}
Necrobia rufipes	{"vöröslábú hullabogár"}
Necrobia violacea	{"kéák hullabogár"}
Necrodes littoralis	{"nagy dögbogár"}
Nedyus quadrimaculatus	{csalánormányos}
Negastrius pulchellus	{"tarka fövenypattanó"}
Negastrius sabulicola	{"díszes fövenypattanó"}
Nehalennia speciosa	{"törpe légivadász"}
Nehemitropia lividipennis	{"orsócsápú komposztholyva"}
Neides tipularius	{"közönséges szúnyogpoloska"}
Nemapogon clematella	{"ékes gombamoly"}
Nemapogon cloacella	{"raktári gombamoly"}
Nemapogon falstriella	{"északi gombamoly"}
Nemapogon granella	{"raktári gabonamoly"}
Nemapogon gravosaellus	{"kövér gombamoly"}
Nemapogon hungarica	{"magyar gombamoly"}
Nemapogon inconditella	{"hegyi gombamoly"}
Nemapogon nigralbella	{bükkfa-gombamoly}
Nemapogon picarella	{"nagy gombamoly"}
Nemapogon variatella	{"fehérfejű gombamoly"}
Nemapogon wolffiella	{"fehérpettyes gombamoly"}
Nematodes filum	{"karcsú tövisnyakúbogár"}
Nematopogon adansoniella	{"gyűrűscsápú bajszosmoly"}
Nematopogon metaxella	{"mocsári bajszosmoly"}
Nematopogon pilella	{"hegyi bajszosmoly"}
Nematopogon robertella	{"fenyvesjáró bajszosmoly"}
Nematopogon schwarziellus	{"déli bajszosmoly"}
Nematopogon swammerdamella	{"nagy bajszosmoly"}
Nemaxera betulinella	{"homályos gombamoly"}
Nemesia pannonica budensis	{"magyar aknászpók"}
Nemobius sylvestris	{"erdei tücsök"}
Nemolecanium graniformis	{jegenyefenyőtű-pajzstetű}
Nemonyx lepturoides	{szarkaláb-áleszelény}
Nemophora cupriacella	{"rézszínű tőrösmoly"}
Nemophora degeerella	{"pompás tőrösmoly"}
Nemophora dumerilella	{"balkáni tőrösmoly"}
Nemophora fasciella	{"feketesávú tőrösmoly"}
Nemophora metallica	{"fémszínű tőrösmoly"}
Nemophora minimella	{ördögszem-tőrösmoly,ördögszem-tőrösmoly}
Nemophora mollella	{"bíborsávos tőrösmoly"}
Nemophora ochsenheimerella	{jegenyefenyő-tőrösmoly}
Nemophora pfeifferella	{"cifra tőrösmoly"}
Nemophora prodigellus	{"aranysárga tőrösmoly"}
Nemophora raddaella	{"levantei tőrösmoly"}
Nemophora violellus	{"vastagcsápú tőrösmoly"}
Nemozoma elongatum	{"karcsú szúvadász"}
Neoaliturus fenestratus	{"ablakosszárnyú kabóca"}
Neobisnius procerulus	{"kétszínű ganajholyva"}
Neobisnius villosulus	{"vöröses ganajholyva"}
Neocoenorrhinus aeneovirens	{"bronzos eszelény"}
Neocoenorrhinus germanicus	{szamócaeszelény}
Neocoenorrhinus interpunctatus	{levélér-eszelény}
Neocoenorrhinus pauxillus	{"bordafúró eszelény"}
Neoephemera maxima	{"rábai kérész"}
Neofaculta ericetella	{"hangaszövő sarlósmoly"}
Neofaculta infernella	{"áfonyaszövő sarlósmoly"}
Neofriseria singula	{"mohaszövő sarlósmoly"}
Neoglanis maculatus	{"nagy ökörfarkkóró-ormányos"}
Neoglocianus albovittatus	{"fehérsávos ormányos"}
Neoglocianus maculaalba	{máktokormányos}
Neognophina intermedia	{"változó sziklaaraszoló"}
Neognophina intermedia budensis	{"változó sziklaaraszoló"}
Neogobius fluviatilis	{"folyami géb"}
Neogobius gymnotrachelus	{"csupasztorkú géb"}
Neogobius melanostomus	{"feketeszájú géb"}
Neomargarodes festucae	{csenkesz-gyöngypajzstetű}
Neomida haemorrhoidalis	{"szarvas taplóbogá"}
Neomys anomalus	{Miller-vízicickány}
Neomys fodiens	{"közönséges vízicickány"}
Neon reticulatus	{"recés ugrópók"}
Neophilaenus campestris	{"közönséges tajtékoskabóca"}
Neophilaenus minor	{"kis tajtékoskabóca"}
Neoplinthus tigratus porcatus	{komlóormányos}
Neopristilophus insitivus	{"lapos pattanó"}
Neoscona adianta	{kökény-keresztespók}
Neosphaleroptera nubilana	{"felhős sodrómoly"}
Neottiglossa leporina	{"sárgahasú címerespoloska"}
Neottiglossa pusilla	{"feketehasú címerespoloska"}
Neottiura bimaculata	{"dudva törpepók"}
Neozephyrus quercus	{tölgyfalepke}
Nepa cinerea	{víziskorpió}
Nephopterix angustella	{kecskerágó-karcsúmoly}
Nephus anomus	{"halványfoltos törpebödice"}
Nephus bipunctatus	{"kétpettyes törpebödice"}
Nephus bisignatus	{"kétjegyes törpebödice"}
Nephus bisignatus claudiae	{"kétjegyes törpebödice"}
Nephus nigricans	{"feketés törpebödice"}
Nephus quadrimaculatus	{"négyfoltos törpebödice"}
Nephus redtenbacheri	{"korongfoltos törpebödice"}
Nephus semirufus	{"korzikai törpebödice"}
Neriene clathrata	{"rácsozott vitorlapók"}
Neriene emphana	{"címeres vitorlapók"}
Neriene montana	{"hegyi vitorlapók"}
Neriene peltata	{"sávos vitorlapók"}
Neurothaumasia ankerella	{magyarmoly}
Newsteadia floccosa	{moha-pajzstetű}
Nezara viridula	{"zöld vándorpoloska"}
Nialus varians	{"változékony trágyabogár"}
Nicrophorus antennatus	{"sárgabunkós temetőbogár"}
Nicrophorus germanicus	{"nagy temetőbogár"}
Nicrophorus humator	{"fekete temetőbogár"}
Nicrophorus interruptus	{"sárgaszőrű temetőbogár"}
Nicrophorus investigator	{"feketepillás temetőbogár"}
Nicrophorus sepultor	{"feketeszőrű temetőbogár"}
Nicrophorus vespillo	{"közönséges temetőbogár"}
Nicrophorus vespilloides	{"feketecsápú temetőbogár"}
Nicrophorus vestigator	{"szőrösnyakú temetőbogár"}
Niditinea fuscella	{"pettyes fészekmoly"}
Niditinea striolella	{szarumoly}
Nigma walckenaeri	{"zöld hamvaspók"}
Nimbus obliteratus	{"sötétjegyű trágyabogár"}
Niphonympha dealbatella	{"aranyfoltos havasmoly"}
Niptus hololeucus	{"aranysz?r? tolvajbogár"}
Nitidula bipunctata	{"kétpettyes dögészfénybogár"}
Nitidula carnaria	{"húsevő dögészfénybogár"}
Nitidula flavomaculata	{"sárgafoltos dögészfénybogár"}
Nitidula rufipes	{"vöröslábú dögészfénybogár"}
Nobius serotinus	{"őszi trágyabogár"}
Noctua comes	{"foltos sárgafűbagoly"}
Noctua fimbriata	{"szélessávú sárgafűbagoly"}
Noctua interjecta	{"zömök sárgafűbagoly"}
Noctua interposita	{"köztes sárgafűbagoly"}
Noctua janthina	{"fehérgalléros sárgafűbagoly"}
Noctua orbona	{"kis sárgafűbagoly"}
Noctua pronuba	{"nagy sárgafűbagoly"}
Nohoveus punctulatus	{"kunsági hangyafarkas"}
Nola aerugula	{"barnacsíkos pamacsosszövő"}
Nola chlamitulalis	{"pompás pamacsosszövő"}
Nola cicatricalis	{"szürke pamacsosszövő"}
Nola confusalis	{"apró pamacsosszövő"}
Nola cristatula	{"törpe pamacsosszövő "}
Nola cucullatella	{"barna pamacsosszövő"}
Nomada bifasciata	{"tarka darázsméh"}
Nomada sexfasciata	{"kaszanyűg darázsméh"}
Nomada trispinosa	{"pitypang darázsméh"}
Nomioides variegatus	{"zöld méh"}
Nomius pygmaeus	{"barna hengeresfutó"}
Nomophila noctuella	{"közönséges vándormoly"}
Nonagria typhae	{"nagy gyékénybagoly"}
Nosodendron fasciculare	{falébogár}
Notaphus dentellus	{"sárgaszalagos gyorsfutó"}
Notaphus ephippium	{"sziki gyorsfutó"}
Notaphus semipunctatus	{"címeres gyorsfutó"}
Notaphus starkii	{"sötétszalagos gyorsfutó"}
Notaphus varius	{"rajzos gyorsfutó"}
Notaris acridula	{sásormányos}
Notaris aterrima	{rejtettpajzsocskás-ormányos}
Notaris maerkeli	{maerkel-ormányos}
Notaris scirpi	{kákaormányos}
Nothodes parvulus	{"bronzos bokorpattanó"}
Nothris lemniscellus	{"fehérvállú sarlósmoly"}
Nothris verbascella	{"okkersárga sarlósmoly"}
Notiophilus biguttatus	{"kétfoltos szemesfutó"}
Notiophilus germinyi	{"recés szemesfutó"}
Notiophilus laticollis	{"sziki szemesfutó"}
Notiophilus palustris	{"mocsári szemesfutó"}
Notiophilus rufipes	{"vöröslábú szemesfutó"}
Notocelia cynosbatella	{rózsahajtás-tükrösmoly}
Notocelia incarnatana	{jajrózsa-tükrösmoly}
Notocelia roborana	{rózsarügy-tükrösmoly}
Notocelia trimaculana	{galagonya-tükrösmoly}
Notocelia uddmanniana	{"málnasodró tükrösmoly"}
Notodonta dromedarius	{"tevehátú púposszövő"}
Notodonta tritophus	{"tarajos púposszövő"}
Notodonta ziczac	{"zegzugos púposszövő"}
Notolaemus castaneus	{"gesztenyebarna szegélyeslapbogár"}
Notolaemus unifasciatus	{"szalagos szegélyeslapbogár"}
Notonecta glauca	{"tarka hanyattúszó-poloska"}
Notonecta lutea	{"sárgapajzsú hanyattúszó-poloska"}
Notonecta viridis	{"közönséges hanyattúszó-poloska"}
Notostira erratica	{"feketés mezeipoloska"}
Notothecta flavipes	{"hangyakedvelő penészholyva"}
Notoxus brachycerus	{"nagy nyakszarvúbogár"}
Notoxus cavifrons appendicinus	{"déli nyakszarvúbogár"}
Notoxus monoceros	{"sárgahasú nyakszarvúbogár"}
Notoxus trifasciatus	{közép-nyakszarvúbogár}
Noxius curtirostris	{"rövidorrú orrosbogár"}
Nuctenea umbratica	{"rés keresztespókrés keresztespók"}
Nyctalus leisleri	{"szőröskarú koraidenevér"}
Nyctalus noctula	{"rőt koraidenevér"}
Nyctegretis lineana	{"agátszínű karcsúmoly"}
Nyctegretis triangulella	{"háromszöges karcsúmoly"}
Nycteola asiatica	{nyárfa-apróbagoly}
Nycteola degenerana	{"fehérfoltos apróbagoly "}
Nycteola revayana	{"sötétszárnyó apróbagoly"}
Nycteola siculana	{fűzfa-apróbagoly}
Nyctereutes procyonoides	{nyestkutya}
Nycterosea obstipata	{vándoraraszoló}
Nymphula nitidulata	{"díszes vízimoly"}
Nysius ericae	{"kis fényesbodobács"}
Nysius senecionis	{"közönséges fényesbodobács"}
Nysius thymi	{kakukkfűbodobács}
Nysson interruptus	{"közepes tarkadarázs"}
Nysson maculosus	{"kis tarkadarázs"}
Nysson spinosus	{"nagy tarkadarázs"}
Ocalea badia	{"közönséges partfutóholyva"}
Ocalea rivularis	{"hegyi partfutóholyva"}
Ochetostethus opacus	{"törpe földipoloska"}
Ochina latrellii	{"kétszínű álszú"}
Ochlodes sylvanus	{"erdei busalepke"}
Ochodaeus chrysomeloides	{"alkonyati homoktúróbogár"}
Ochromolopis ictella	{zsellérke-íveltmoly}
Ochropacha duplaris	{égerlevél-pihésszövő}
Ochropleura plecta	{"fehérszegélyű földibagoly"}
Ochsenheimeria capella	{"keleti vaskosmoly"}
Ochsenheimeria taurella	{rozsgyökérmoly}
Ochsenheimeria urella	{"alföldi vaskosmoly"}
Ochsenheimeria vacculella	{"homoki vaskosmoly"}
Ochthebius peisonis	{"fertői zömökcsiborka"}
Ochthephilus omalinus	{"hosszúszárnyú iszapholyva"}
Ocneria rubea	{"rőt gyapjaslepke"}
Ocrasa fulvocilialis	{"aranyrojtú fényilonca"}
Ocrasa glaucinalis	{"rezes fényilonca"}
Octotemnus glabriculus	{"fényes taplószú"}
Ocydromus dalmatinus	{"dalmát gyorsfutó"}
Ocydromus decorus	{"díszes gyorsfutó"}
Ocydromus deletus	{"fénylő gyorsfutó"}
Ocydromus fasciolatus	{"szurkos gyorsfutó"}
Ocydromus femoratus	{"fövenylakó gyorsfutó"}
Ocydromus fluviatilis	{"folyóparti gyorsfutó"}
Ocydromus lunulatus	{"füstös gyorsfutó"}
Ocydromus modestus	{"sárgaöves gyorsfutó"}
Ocydromus monticola	{"kövi gyorsfutó"}
Ocydromus stephensii	{"recés gyorsfutó"}
Ocydromus subcostatus	{"rövidszárnyú gyorsfutó"}
Ocydromus subcostatus javurkovae	{"európai rövidszárnyú gyorsfutó"}
Ocydromus testaceus	{"barnáshátú gyorsfutó"}
Ocydromus tetracolus	{"keresztes gyorsfutó"}
Ocydromus tibialis	{"patakparti gyorsfutó"}
Ocypus aeneocephalus	{"bronzos holyva"}
Ocypus biharicus	{"nehézszagú holyva"}
Ocypus brunnipes	{"ártéri holyva"}
Ocypus fulvipennis	{"vörösszárnyú holyva"}
Ocypus fuscatus	{"fémeshátú holyva"}
Ocypus macrocephalus	{"busafejű holyva"}
Ocypus mus	{"egérszínű holyva"}
Ocypus nitens	{"fekete holyva"}
Ocypus olens	{"bűzös holyva"}
Ocypus ophthalmicus	{"kék holyva"}
Ocypus picipennis	{"szurtos holyva"}
Ocypus tenebricosus	{"gyászos holyva"}
Ocyusa picina	{"szurokszínű fenyérholyva"}
Odacantha melanura	{ingoványfutó}
Odezia atrata	{"fekete araszoló"}
Odice arcuinna	{"réti törpebagoly"}
Odites kollarella	{árvamoly}
Odonestis pruni	{szilvafapohók}
Odontium argenteolum	{"ezüstös gyorsfutó"}
Odontium foraminosum	{"gödörkés gyorsfutó"}
Odontium laticolle	{"szélesnyakú gyorsfutó"}
Odontium litorale	{"parti gyorsfutó"}
Odontium striatum	{"iszapjáró gyorsfutó"}
Odontium velox	{"homokjáró gyorsfutó"}
Odontopera bidentata	{"barna csipkésaraszoló"}
Odontopodisma decipiens	{"olajzöld hegyisáska"}
Odontopodisma rubripes	{"vöröslábú hegyisáska"}
Odontoscelis fuliginosa	{"szőrös pajzsospoloska"}
Odontoscelis hispidula	{"szőrös pajzsospoloska"}
Odontoscelis lineola	{"homoki pajzsospoloska"}
Odontotarsus purpureolineatus	{"tarka pajzsospoloska"}
Odynerus melanocephalus	{"sötétfejű kürtősdarázs"}
Odynerus spinipes	{"fogaslábú kürtősdarázs"}
Oecanthus pellucens	{"pirregő tücsök"}
Oeciacus hirundinis	{fecskepoloska}
Oecophora bractella	{"kis díszmoly"}
Oedaleus decorus	{"szalagos sáska"}
Oedecnemidius pictus	{"tarka tölgyormányos"}
Oedemera croceicollis	{"pirostorú álcincér"}
Oedemera femoralis	{"feketecombú álcincér"}
Oedemera femorata	{"sárgahátú álcincér"}
Oedemera flavipes	{"sárgalábú álcincér"}
Oedemera lateralis	{"feketéskék álcincér"}
Oedemera lurida	{"mezei álcincér"}
Oedemera podagrariae	{székfű-álcincér}
Oedemera pthysica	{"feketeszegélyű álcincér"}
Oedemera subrobusta	{"szélestorú álcincér"}
Oedemera virescens	{"zöldes álcincér"}
Oedipoda caerulescens	{"kékszárnyú sáska"}
Oedostethus quadripustulatus	{"négyfoltos fövenypattanó"}
Oedostethus tenuicornis	{"vékonycsápú fövenypattanó"}
Oedothorax apicatus	{"búbosfejű pók"}
Oegoconia caradjai	{"fátyolos avarmoly"}
Oegoconia deauratella	{"nagy avarmoly"}
Oegoconia uralskella	{"közönséges avarmoly"}
Oenas crassicornis	{"vastagcsápú hólyaghúzó"}
Oenopia conglobata	{"rózsás katica"}
Oenopia impustulata	{"fekete katica"}
Oenopia lyncea agnata	{"bokorerdei katica"}
Oiceoptoma thoracicum	{"vörösnyakú dögbogár"}
Oidaematophorus constanti	{"peremizsrágó tollasmoly"}
Oidaematophorus lithodactyla	{"sárgásszürke tollasmoly"}
Oinophila v-flava	{dugómoly}
Olethreutes arcuella	{"avarevő tükrösmoly"}
Olibrus aeneus	{kamilla-kalászbogár}
Olibrus affinis	{"sötétbarna kalászbogár"}
Olibrus bicolor	{pitypang-kalászbogár}
Olibrus bimaculatus	{"kétfoltos kalászbogár"}
Olibrus bisignatus	{"kétjegyes kalászbogár"}
Olibrus corticalis	{"sárgásbarna kalászbogár"}
Olibrus gerhardti	{aggófű-kalászbogár}
Olibrus liquidus	{"szurokbarna kalászbogár"}
Olibrus millefolii	{cickafark-kalászbogár}
Olibrus pygmaeus	{"törpe kalászbogár"}
Oligia latruncula	{"sötét dudvabagolyocska"}
Oligia strigilis	{"apró dudvabagolyocska"}
Oligia versicolor	{"tarka dudvabagolyocska"}
Oligolimax annularis	{"gyűrűs üvegcsiga"}
Oligomerus brunneus	{"barna álszú"}
Oligomerus ptilinoides	{"pontsoros álszú"}
Oligoneuriella keffermuellerae	{Keffermüller-denevérszárnyú-kérész}
Oligoneuriella pallida	{"sápadt denevérszárnyú-kérész"}
Oligota inflata	{"komposztlakó parányholyva"}
Oligota parva	{"tarka parányholyva"}
Oligota pumilio	{"aprócska parányholyva"}
Oligota pusillima	{"suta parányholyva"}
Olindia schumacherana	{"fehérsávos sodrómoly"}
Olisthopus rotundatus	{"nagy törekfutó"}
Olophrum assimile	{"ragyás felemásholyva"}
Olophrum puncticolle	{"alföldi felemásholyva"}
Olophrum viennense	{"ártéri felemásholyva"}
Omalisus fontisbellaquaei	{"közönséges ál-hajnalbogár"}
Omalium caesum	{"kis barázdásholyva"}
Omalium cinnamomeum	{"fahéjszínű barázdásholyva"}
Omalium excavatum	{"magashegyi barázdásholyva"}
Omalium imitator	{"barnáshátú barázdásholyva"}
Omalium littorale	{"sókedvelő barázdásholyva"}
Omalium oxyacanthae	{"erdei barázdásholyva"}
Omalium rivulare	{"gödörkés barázdásholyva"}
Omalium validum	{"termetes barázdásholyva"}
Omaloplia marginata	{"szegélyes kiscserebogár"}
Omaloplia ruricola	{"kétszínű kiscserebogár"}
Omias seminulum	{"magalakú ormányos"}
Omocestus haemorrhoidalis	{"barna tarlósáska"}
Omocestus petraeus	{"szőke tarlósáska"}
Omocestus rufipes	{"vöröshasú tarlósáska"}
Omocestus viridulus	{"hangos tarlósáska"}
Omoglymmius germari	{"fogasvállú állasbogár"}
Omonadus bifasciatus	{"kétöves fürgebogár"}
Omonadus floralis	{"virágjáró fürgebogár"}
Omonadus formicarius	{"simanyakú fürgebogár"}
Omophlus betulae	{"szőrösnyakú pejbogár"}
Omophlus lividipes	{"kis pejbogár"}
Omophlus picipes	{"déli pejbogár"}
Omophlus proteus	{"közönséges pejbogár"}
Omophlus rugosicollis	{"széles pejbogár"}
Omophron limbatum	{gömböcfutó}
Omosita colon	{"sokpettyes dögészfénybogár"}
Omosita depressa	{"lapos dögészfénybogár"}
Omosita discoidea	{"korongfoltos dögészfénybogár"}
Omphalapion dispar	{pipitér-cickányormányos}
Omphalapion laevigatum	{székfű-cickányormányos}
Omphalapion pseudodispar	{"fiatal cickányormányos"}
Omphalophana antirrhinii	{oroszlánszáj-apróbagoly}
Oncocera semirubella	{lucernamoly}
Oncochila scapularis	{"csuklyás csipkéspoloska"}
Oncorhynchus mykiss	{"szivárványos pisztráng"}
Oncotylus setulosus	{búzavirág-poloska}
Ondatra zibethicus	{pézsmapocok}
Ontholestes haroldi	{"sárgatérdű holyva"}
Ontholestes murinus	{"márványos holyva"}
Ontholestes tessellatus	{"cifra holyva"}
Onthophagus coenobita	{"rezes trágyatúró"}
Onthophagus fracticornis	{"bronzos trágyatúró"}
Onthophagus furcatus	{"villás trágyatúró"}
Onthophagus gibbulus	{"bütykös trágyatúró"}
Onthophagus grossepunctatus	{"érdes trágyatúró"}
Onthophagus illyricus	{"tülkös trágyatúró"}
Onthophagus joannae	{"szirti trágyatúró"}
Onthophagus lemur	{"szalagos trágyatúró"}
Onthophagus lucidus	{"fényes trágyatúró"}
Onthophagus nuchicornis	{"homoki trágyatúró"}
Onthophagus ovatus	{"apró trágyatúró"}
Onthophagus ruficapillus	{"füstös trágyatúró"}
Onthophagus semicornis	{"üreglakó trágyatúró"}
Onthophagus taurus	{"szarvas trágyatúró"}
Onthophagus vacca	{"zöldes trágyatúró"}
Onthophagus verticicornis	{"fekete trágyatúró"}
Onthophagus vitulus	{"ürgevendég trágyatúró"}
Onthophilus affinis	{"közönséges bordássutabogár"}
Onthophilus punctatus	{"nagy bordássutabogár"}
Onthophilus striatus	{"vonalasnyakú bordássutabogár"}
Onychogomphus forcipatus	{csermelyszitakötő}
Onyxacalles croaticus	{"horvát zártormányúbogár"}
Onyxacalles pyrenaeus	{"pireneusi zártormányúbogár"}
Oodes gracilis	{"karcsú búvárfutrinka"}
Oodes helopioides	{"széles búvárfutrinka"}
Ootypus globosus	{"parányi penészbogár"}
Opanthribus tessellatus	{"tömzsi orrosbogár"}
Opatrum riparium	{"parti gyászbogár"}
Opatrum sabulosum	{"sároshátú gyászbogár"}
Operophtera brumata	{"kis téliaraszoló"}
Operophtera fagata	{"északi téliaraszoló"}
Ophiogomphus cecilia	{"erdei szitakötő"}
Ophonus ardosiacus	{"kereknyakú bársonyfutó"}
Ophonus azureus	{"azúrkék bársonyfutó"}
Ophonus cordatus	{"szívnyakú bársonyfutó"}
Ophonus cribricollis	{"fekete bársonyfutó"}
Ophonus diffinis	{"mocsári bársonyfutó"}
Ophonus gammeli	{"kéklő bársonyfutó"}
Ophonus laticollis	{"erdei bársonyfutó"}
Ophonus melletii	{"parlagi bársonyfutó"}
Ophonus parallelus	{"karcsú bársonyfutó"}
Ophonus puncticeps	{"mezei bársonyfutó"}
Ophonus puncticollis	{"gyepi bársonyfutó"}
Ophonus rufibarbis	{"közönséges bársonyfutó"}
Ophonus rupicola	{"ligeti bársonyfutó"}
Ophonus sabulicola	{"homoki bársonyfutó"}
Ophonus sabulicola ponticus	{"dombvidéki bársonyfutó"}
Ophonus schaubergerianus	{Schauberger-bársonyfutó}
Ophonus stictus	{"sötétszőrű bársonyfutó"}
Ophonus subquadratus	{"domború bársonyfutó"}
Ophonus subsinuatus	{"kis bársonyfutó"}
Opigena polygona	{"bronzos földibagoly"}
Opilo domesticus	{"házi facsősz"}
Opilo mollis	{"közönséges facsősz"}
Opilo pallidus	{"sárga facsősz"}
Opisthograptis luteolata	{"citromsárga araszoló"}
Oporopsamma wertheimsteini	{nyúlparéj-sodrómoly}
Opostega salaciella	{"ezüstfehér aprómoly"}
Opostega spatulella	{"szalmaszínű aprómoly"}
Oprohinus consputus	{"hamvas hagymaormányos"}
Oprohinus suturalis	{hagymaormányos}
Opsibotys fuscalis	{"szürke tűzmoly"}
Orbona fragariae	{óriás-télibagoly}
Orchesia fasciata	{"szalagos szöcskebogár"}
Orchesia grandicollis	{"szélesnyakú szöcskebogár"}
Orchesia micans	{"fényes szöcskebogár"}
Orchesia minor	{"kis szöcskebogár"}
Orchesia undulata	{"hullámos szöcskebogár"}
Orchestes fagi	{bükk-bolhaormányos}
Orchestes hortorum	{"változatos bolhaormányos"}
Orchestes hungaricus	{"magyar bolhaormányos"}
Orchestes quedenfeldtii	{quenfeldt-bolhaormányos}
Orchestes quercus	{tölgy-bolhaormányos}
Orchestes rusci	{"erdei bolhaormányos"}
Orchestes subfasciatus	{"szalagos bolhaormányos"}
Orchestes testaceus	{nyír-bolhaormányos}
Orcula dolium	{hordócsiga}
Orectis proboscidata	{"szürke karcsúbagoly "}
Oreochromis niloticus	{"nílusi tilápia"}
Orgyia antiqua	{kökény-kisszövő}
Orgyia recens	{"tarka kisszövő"}
Orius majusculus	{"levéltetűfaló poloska"}
Orius minutus	{"törpe virágpoloska"}
Orius niger	{"közönséges virágpoloska"}
Ornativalva plutelliformis	{"tamariskarágó sarlósmoly"}
Ornixola caudulatella	{"farkos keskenymoly"}
Orobitis cyanea	{"ibolya gyökérbarkó"}
Orobitis nigrina	{"fekete gyökérbarkó"}
Orophia denisella	{"fehérmintás díszmoly"}
Orophia ferrugella	{"rozsdamintás díszmoly"}
Orophia sordidella	{"sárgamintás laposmoly"}
Orphilus niger	{"csupasz porva"}
Orthetrum albistylum	{"fehér pásztor"}
Orthetrum brunneum	{"pataki szitakötő"}
Orthetrum cancellatum	{"vízi pásztor"}
Orthetrum coerulescens	{"kék pásztor"}
Orthezia urticae	{csalán-pajzstetű}
Orthocephalus saltator	{"szőrös ugrópoloska"}
Orthocerus clavicornis	{"kefecsápú zuzmó-héjbogár"}
Orthocerus crassicornis	{"vastagcsápú zuzmó-héjbogár"}
Orthochaetes setiger	{"láthatatlan-pajzsocskájú ormányos"}
Orthocis alni	{"rövidszőrű taplószú"}
Orthocis coluber	{"hengeres taplószú"}
Orthocis festivus	{"díszes taplószú"}
Orthocis lucasi	{"szélesnyakú taplószú"}
Orthocis pseudolinearis	{"domború taplószú"}
Orthocis pygmaeus	{"törpe taplószú"}
Orthocis vestitus	{"pikkelyszőrös taplószú"}
Ortholepis betulae	{nyírfa-karcsúmoly}
Ortholomus punctipennis	{"szőrös fényesbodobács"}
Orthonama vittata	{"lápi galajaraszoló"}
Orthonotus rufifrons	{"vörösfejű mezeipoloska"}
Orthops basalis	{sárgarépa-poloska}
Orthops campestris	{"kis réti-mezeipoloska"}
Orthops kalmii	{"kis tarka-mezeipoloska"}
Orthosia cerasi	{"közepes tavaszi-fésűsbagoly"}
Orthosia cruda	{"kis barkabagoly"}
Orthosia gothica	{"gótikus barkabagoly"}
Orthosia gracilis	{"csinos barkabagoly"}
Orthosia incerta	{"változékony barkabagoly"}
Orthosia miniosa	{"sárgás fésűsbagoly"}
Orthosia opima	{"hegyesszárnyú fésűsbagoly"}
Orthosia populeti	{nyárfa-fésűsbagoly}
Orthostixis cribraria	{"pettyes fehéraraszoló"}
Orthotaenia undulana	{"csalánsodró tükrösmoly"}
Orthotelia sparganella	{békabuzogánymoly}
Orthotomicus laricis	{vörösfenyőszú}
Orthotomicus longicollis	{"nyurga fogasszú"}
Orthotomicus proximus	{erdeifenyő-fogasszú}
Orthotomicus robustus	{"robusztus fogasszú"}
Orthotylus flavosparsus	{"sápadt mezeipoloska"}
Orthotylus marginalis	{"fakó mezeipoloska"}
Oryctolagus cuniculus	{"üregi nyúl"}
Oryxolaemus flavifemoratus	{"rekettyeszépe cickányormányos"}
Oryzaephilus mercator	{"kalmár fogasnyakú-lapbogár"}
Oryzaephilus surinamensis	{"zsizsikfaló fogasnyakú-lapbogár"}
Osmia adunca	{"fehérhasú faliméh"}
Osmia aurulenta	{"csigalakó faliméh"}
Osmia bicolor	{"kétszínű faliméh"}
Osmia brevicornis	{"atracél faliméh"}
Osmia caerulescens	{"kék faliméh"}
Osmia cornuta	{"szarvas faliméh"}
Osmia rufa	{"kékfejű faliméh"}
Osmoderma eremita	{remetebogár}
Osmylus fulvicephalus	{"foltosszárnyú partifátyolka"}
Osphya bipunctata	{"kétalakú komorka"}
Ostoma ferruginea	{"rozsdaszínű korongbogár"}
Ostrinia nubilalis	{kukoricamoly}
Ostrinia quadripunctalis	{"turjáni kormosmoly"}
Othius laeviusculus	{"barna avarholyva"}
Othius punctulatus	{"nagy avarholyva"}
Otho sphondyloides	{"vaskos tövisnyakúbogár"}
Otiorhynchus austriacus	{"osztrák gyalogormányos"}
Otiorhynchus conspersus	{"hintett gyalogormányos"}
Otiorhynchus corruptor	{"megrontó gyalogormányos"}
Otiorhynchus crataegi	{galagonya-gyalogormányos}
Otiorhynchus fullo	{"aranypikkelyes gyalogormányos"}
Otiorhynchus gemmatus	{"ékköves gyalogormányos"}
Otiorhynchus hungaricus	{"magyar gyalogormányos"}
Otiorhynchus laevigatus	{"fényes gyalogormányos"}
Otiorhynchus ligustici	{"hamvas vincellérbogár"}
Otiorhynchus multipunctatus	{"sokpontos gyalogormányos"}
Otiorhynchus ovatus	{"apró gyalogormányos"}
Otiorhynchus porcatus	{malacka-gyalogormányos}
Otiorhynchus raucus	{"molyhos gyalogormányos"}
Otiorhynchus roubali	{Roubal-gyalogormányos}
Otiorhynchus rugosostriatus	{szamóca-gyalogormányos}
Otiorhynchus scaber	{"bordás gyalogormányos"}
Otiorhynchus similis	{álvincellérbogár}
Otiorhynchus sulcatus	{"szálkás gyalogormányos"}
Otiorhynchus tenebricosus	{"rozsdáslábú gyalogormányos"}
Otiorhynchus tristis	{"aranyszőrű gyalogormányos"}
Otiorhynchus velutinus	{"szőrös gyalogormányos"}
Otolelus pruinosus	{"deres korhóbogár"}
Otophorus haemorrhoidalis	{"csúcsfoltos trágyabogár"}
Ourapteryx sambucaria	{"fecskefarkú araszoló"}
Ovalisia dives	{nyírfa-tarkadíszbogár}
Ovalisia festiva	{boróka-tarkadíszbogár}
Ovalisia mirifica	{szilfa-tarkadíszbogár}
Ovalisia rutilans	{hársfa-tarkadíszbogár}
Ovis aries	{muflon}
Ovis aries musimon	{muflon}
Oxicesta geographica	{térképbagoly}
Oxybelus aurantiacus	{"narancsszínű légyölő"}
Oxybelus bipunctatus	{"apró légyölő"}
Oxybelus latro	{"nagy légyölő"}
Oxybelus quattuordecimnotatus	{"közönséges légyölő"}
Oxybelus uniglumis	{"fehér légyölő"}
Oxycarenus lavaterae	{mályvabodobács}
Oxylaemus cylindricus	{"hengeres humuszbogár"}
Oxyloma elegans	{"karcsú borostyánkőcsiga"}
Oxynychus chrysomeloides	{"szalagos félböde"}
Oxynychus litura	{"félholdas félböde"}
Oxynychus rufa	{"nádi félböde"}
Oxynychus scutellata	{"mocsári félböde"}
Oxyomus sylvestris	{"fogasvállú trágyabogár"}
Oxyopes heterophthalmus	{hiúzpók}
Oxypoda abdominalis	{"vöröses pudvaholyva"}
Oxypoda acuminata	{"közönséges pudvaholyva"}
Oxypoda alternans	{"tarka pudvaholyva"}
Oxypoda annularis	{"öves pudvaholyva"}
Oxypoda brevicornis	{"penészkedvelő pudvaholyva"}
Oxypoda carbonaria	{"selymes pudvaholyva"}
Oxypoda filiformis	{"karcsú pudvaholyva"}
Oxypoda flavicornis	{"rövidcsápú pudvaholyva"}
Oxypoda formosa	{"kecses pudvaholyva"}
Oxypoda haemorrhoa	{"rozsdaszínű pudvaholyva"}
Oxypoda longipes	{"hosszúlábú pudvaholyva"}
Oxypoda mutata	{"ligeti pudvaholyva"}
Oxypoda opaca	{"sötét pudvaholyva"}
Oxypoda praecox	{"berki pudvaholyva"}
Oxypoda rufa	{"dunántúli pudvaholyva"}
Oxypoda soror	{"cingár pudvaholyva"}
Oxypoda spaethi	{"alomlakó pudvaholyva"}
Oxypoda spectabilis	{"termetes pudvaholyva"}
Oxypoda togata	{"barnás pudvaholyva"}
Oxypoda vicina	{"erdei pudvaholyva"}
Oxypoda vittata	{"sujtásos pudvaholyva"}
Oxyporus maxillosus	{"feketesárga gombaholyva"}
Oxyporus rufus	{"feketevörös gombaholyva"}
Oxypselaphus obscurus	{"barnás kisfutó"}
Oxyptilus chrysodactyla	{"aranyszárnyú tollasmoly"}
Oxyptilus parvidactyla	{"törpe tollasmoly"}
Oxyptilus pilosellae	{"vörösbarna tollasmoly"}
Oxystoma cerdo	{"egérszürke cickányormányos"}
Oxystoma craccae	{kaszanyüg-cickányormányos}
Oxystoma dimidiatum	{"szürkéskék cickányormányos"}
Oxystoma ochropus	{"hatalmas cickányormányos"}
Oxystoma opeticum	{tavaszilednek-cickányormányos}
Oxystoma pomonae	{"lomblakó cickányormányos"}
Oxystoma subulatum	{"tőrösorrú cickányormányos"}
Oxytelus fulvipes	{"lápi korhóholyva"}
Oxytelus laqueatus	{"hegyi korhóholyva"}
Oxytelus migrator	{"jövevény korhóholyva"}
Oxytelus piceus	{"sárgalábú korhóholyva"}
Oxytelus sculptus	{"ráncos korhóholyva"}
Oxythyrea funesta	{"sokpettyes virágbogár"}
Ozyptila praticola	{"földi karolópók"}
Pachetra sagittigera	{"nagy fésűsbagoly"}
Pachnida nigella	{"deres bibircsesholyva"}
Pachyatheta cribrata	{"szemcsés komposztholyva"}
Pachybrachius fracticollis	{"nyakas bodobács"}
Pachycerus madidus	{"tarka répabarkó"}
Pachycnemia hippocastanaria	{gesztenyefa-araszoló}
Pachygnatha clercki	{"nagy fogaspóknagy fogaspók"}
Pachygnatha degeeri	{"kis fogaspók"}
Pachygnatha listeri	{"közönséges fogaspókközönséges fogaspók"}
Pachylister inaequalis	{"nagy sutabogár"}
Pachythelia villosella	{"nagy zsákhordólepke"}
Pachytrachis gracilis	{"karcsú szöcske"}
Pachytychius sparsutus	{rekettyeormányos}
Pactolinus major	{"szögletes sutabogár"}
Paederidus carpathicola	{"kárpáti partiholyva"}
Paederidus ruficollis	{"vöröshátú partiholyva"}
Paederus brevipennis	{"rövidszárnyú partiholyva"}
Paederus caligatus	{"sötétlábú partiholyva"}
Paederus fuscipes	{"kis partiholyva"}
Paederus limnophilus	{"iszaplakó partiholyva"}
Paederus littoralis	{"réti partiholyva"}
Paederus riparius	{"közönséges partiholyva"}
Paederus schoenherri	{"vaskos partiholyva"}
Pagodulina pagodula	{pagodacsiga}
Pagodulina pagodula altilis	{pagodacsiga}
Paidiscura pallens	{"sápadt törpepók"}
Palaeolecanium bituberculatum	{"kétpupú teknőspajzstetű"}
Palarus variegatus	{"nyaktekerő darázs"}
Palliduphantes pallidus	{"sápadt vitorlapók"}
Palmodes occitanicus	{"keleti szöcskeölő"}
Palomena prasina	{"zöld bogyómászó-poloska"}
Palomena viridissima	{"sárga bogyómászó-poloska"}
Palorus depressus	{"szögleteshomlokú gyászbogár"}
Palorus ratzeburgi	{"pontozottfejű kislisztbogár"}
Palorus subdepressus	{"ívelthomlokú gyászbogár"}
Palpita vitrealis	{"hófehér tűzmoly"}
Pammene agnotana	{"erdélyi tükrösmoly"}
Pammene albuginana	{"sötét gubacsmoly"}
Pammene amygdalana	{"mandulaszínű gubacsmoly"}
Pammene argyrana	{"feketeszegélyű gubacsmoly"}
Pammene aurana	{"aranypettyes magrágómoly"}
Pammene aurita	{"aranyló tükrösmoly"}
Pammene christophana	{"aranyfoltos magrágómoly"}
Pammene fasciana	{makkfúrómoly}
Pammene gallicana	{kocsordmagmoly}
Pammene gallicolana	{"francia gubacsmoly"}
Pammene germmana	{"kékcsíkos tükrösmoly"}
Pammene giganteana	{"tükrös gubacsmoly"}
Pammene insulana	{tölgygubacsmoly}
Pammene obscurana	{"szürkés gubacsmoly"}
Pammene ochsenheimeriana	{"pompás gubacsmoly"}
Pammene regiana	{hegyijuhar-magrágómoly}
Pammene rhediella	{galagonya-magrágómoly}
Pammene spiniana	{kökényvirág-tükrösmoly}
Pammene splendidulana	{"pompás tükrösmoly"}
Pammene suspectana	{"aprófoltos tükrösmoly"}
Pammene trauniana	{mezeijuhar-magrágómoly}
Panagaeus bipustulatus	{"kis keresztesfutrinka"}
Panagaeus cruxmajor	{"nagy keresztesfutrinka"}
Pancalia leuwenhoekella	{"feketecsápú ibolyamoly"}
Pancalia schwarzella	{"gyűrűscsápú ibolyamoly"}
Pandemis cerasana	{"kerti sodrómoly"}
Pandemis cinnamomeana	{"fahéjszínű sodrómoly"}
Pandemis corylana	{"sárga sodrómoly"}
Pandemis dumetana	{"mocsári sodrómoly"}
Pandemis heparana	{"ligeti sodrómoly"}
Panemeria tenebrata	{"apró sárgabagoly"}
Pangus scaritides	{"vastagfejű lomhafutó"}
Panolis flammea	{"lángvörös fenyőbagoly"}
Pantacordis pales	{pannimoly}
Panthea coenobita	{apácabagoly}
Panurgus calcaratus	{"szélesfejű méh"}
Paophilus hampei	{rezgőpázsit-ormányos}
Parabolitobius formosus	{"kis gombászholyva"}
Parabolitobius inclinans	{"magashegyi gombászholyva"}
Paracardiophorus musculus	{"pusztai szívespattanó"}
Parachronistis albiceps	{mogyorórügy-sarlósmoly}
Paracolax tristalis	{"sárgás karcsúbagoly"}
Paracorixa concinna	{"tarkalábú búvárpoloska"}
Paracorsia repandalis	{"szalmaszínű tűzmoly"}
Paracylindromorphus subuliformis	{"redős hengerdíszbogár"}
Paradarisa consonaria	{"szélesfoltú faaraszoló"}
Paradrina clavipalpis	{"halovány szürkebagoly"}
Paradromius linearis	{"karcsú kéregfutó"}
Paradromius longiceps	{"hosszúfejű kéregfutó"}
Parafairmairia gracilis	{gyékény-pajzstetű}
Parafomoria helianthemella	{tetemtoldó-törpemoly}
Parafoucartia squamulata	{"pikkelyes kerepormányos"}
Paragymnomerus spiricornis	{óriás-kürtősdarázs}
Parahypopta caestrum	{spárgalepke}
Paralipsa gularis	{raktármoly}
Parallelomorphus terricola	{vájárfutó}
Parameotica hydra	{"iszaplakó hordalékholyva"}
Paramesia gnomana	{"okkerszínű sodrómoly"}
Paramesus obtusifrons	{"sziki kabóca"}
Parammoecius corvinus	{"hollószínű trágyabogár"}
Paranchus albipes	{"sárgalábú kisfutó"}
Paranthrene insolita polonica	{tölgyfa-bögölyszitkár}
Paranthrene tabaniformis	{bögölyszitkár}
Paraphotistus impressus	{"szürkeszőrű pattanó"}
Paraphotistus nigricornis	{"feketecsápú pattanó"}
Parapiesma quadratum	{"sziki recéspoloska"}
Parapiesma salsolae	{ballagófű-poloska}
Parapoynx nivalis	{"fehér vízimoly"}
Parapoynx stratiotata	{"közönséges vízimoly"}
Pararge aegeria	{"erdei szemeslepke"}
Parascotia fuliginaria	{kéményseprőlepke}
Parascythris muelleri	{"kétpettyes zöldmoly"}
Parasteatoda lunata	{"holdas törpepók"}
Parasteatoda tepidariorum	{"házi törpepók"}
Parastenolechia nigrinotella	{"szőlőrágó sarlósmoly"}
Parastichtis suspecta	{"rozsdás lápibagoly"}
Parastichtis ypsillon	{ipszilon-nyárfabagoly}
Paraswammerdamia nebulella	{"galagonyafonó tarkamoly"}
Paratachys bistriatus	{"közönséges martfutó"}
Paratachys fulvicollis	{"szalagos martfutó"}
Paratachys micros	{"kis martfutó"}
Paratachys turkestanicus	{"keleti martfutó"}
Paratalanta hyalinalis	{"üvegszárnyú tűzmoly "}
Paratalanta pandalis	{"halványsárga tűzmoly "}
Parazuphium chevrolati praepannonicum	{"pannon laposfutrinka"}
Pardosa agrestis	{"pusztai farkaspók"}
Pardosa amentata	{"parti farkaspók"}
Pardosa lugubris	{"gyászos farkaspók"}
Parectopa ononidis	{iglice-magrágómoly}
Parectopa robiniella	{akáclevél-hólyagosmoly}
Parectropis similaria	{"barna zúzmóaraszoló"}
Pareulype berberata	{sóskafa-tarkaaraszoló}
Parlatoria oleae	{"ibolyaszínű pajzstetű"}
Parnopes grandior	{"pompás fémdarázs"}
Parocyusa longitarsis	{"szúnyoglábú partfutóholyva"}
Parocyusa rubicunda	{"vöröses partfutóholyva"}
Paromalus flavicornis	{"sárgabunkós kéregsutabogár"}
Paromalus parallelepipedus	{fenyves-kéregsutabogár}
Parophonus hirsutulus	{"komor bársonyfutó"}
Parophonus maculicornis	{"szurokbarna bársonyfutó"}
Parophonus mendax	{"rőt bársonyfutó"}
Parornix anglicella	{galagonya-keskenymoly}
Parornix anguliferella	{"körteráncoló keskenymoly"}
Parornix carpinella	{"gyertyánráncoló keskenymoly"}
Parornix devoniella	{"nyírráncoló keskenymoly"}
Parornix fagivora	{"bükkráncoló keskenymoly"}
Parornix finitimella	{"kökényráncoló keskenymoly"}
Parornix petiolella	{almalevél-keskenymoly}
Parornix scoticella	{berkenye-keskenymoly}
Parornix szocsi	{"magyar keskenymoly"}
Parornix tenella	{"sötétbarna keskenymoly"}
Parornix torquillella	{"almaráncoló keskenymoly"}
Parthenolecanium corni	{"közönséges teknőpajzstetű"}
Parthenolecanium fletcheri	{életfa-teknőspajzstetű}
Parthenolecanium persicae	{"hosszú teknőspajzstetű"}
Parthenolecanium pomeranicum	{tiszafa-teknőspajzstetű}
Parthenolecanium rufulum	{tölgy-teknőspajzstetű}
Patrobus atrorufus	{"szurkos ligetfutó"}
Patrobus styriacus	{"alhavasi ligetfutó"}
Pediacus depressus	{"barna lapbogár"}
Pediacus dermestoides	{"bunkóscsápú lapbogár"}
Pediasia aridella	{"sziki fűgyökérmoly"}
Pediasia contaminella	{"mocskos fűgyökérmoly"}
Pediasia fascelinella	{"sávos fűgyökérmoly"}
Pediasia jucundellus	{"homokháti fűgyökérmoly"}
Pediasia luteella	{"agyagsárga fűgyökérmoly"}
Pediasia matricella	{"alföldi fűgyökérmoly"}
Pedilophorus auratus	{"aranyzöld labdacsbogár"}
Pedilophorus obscurus	{"sötétzöld labdacsbogár"}
Pedilophorus senii	{"balkáni labdacsbogár"}
Pedinus fallax	{"földi gyászbogár"}
Pedinus fallax gracilis	{"földi gyászbogár"}
Pedinus femoralis	{"gyökérrágó gyászbogár"}
Pedinus hungaricus	{"pannóniai gyászbogár"}
Pedius inquinatus	{"karcsú gyászfutó"}
Pedius longicollis	{"csinos gyászfutó"}
Peirates hybridus	{"foltos rablópoloska"}
Pelatea klugiana	{bazsarózsamoly}
Pelecotoma fennica	{álszú-darázsbogár}
Pelecus cultratus	{garda}
Pelenomus quadrituberculatus	{"kis keserűfűormányos"}
Pellenes tripunctatus	{"keresztes ugrópók"}
Pelochares versicolor	{"fogasnyakú partibogár"}
Pelochrista arabescana	{arabeszkmoly}
Pelochrista caecimaculana	{"vakfoltú tükrösmoly"}
Pelochrista decolorana	{"fakó tükrösmoly"}
Pelochrista hepatariana	{"májszínű tükrösmoly"}
Pelochrista infidana	{mezeiüröm-tükrösmoly}
Pelochrista latericiana	{"pannon tükrösmoly"}
Pelochrista modicana	{"sárgásszürke tükrösmoly"}
Pelochrista mollitana	{"mediterrán tükrösmoly"}
Pelochrista subtiliana	{"poros tükrösmoly"}
Pelosia muscerda	{"hamvas algaszövő"}
Pelosia obtusa	{"lápi algaszövő"}
Pelurga comitata	{"nagy tarkaaraszoló"}
Pempelia albariella	{csüdfű-karcsúmoly}
Pempelia formosa	{"ékes karcsúmoly"}
Pempelia obductella	{"mentaszövő karcsúmoly"}
Pempelia palumbella	{"hamvas karcsúmoly"}
Pempeliella dilutella	{kakukkfű-karcsúmoly}
Pempeliella ornatella	{"díszes karcsúmoly"}
Pempeliella sororiella	{"zsákszövő karcsúmoly"}
Pemphredon lethifer	{"gyakori feketedarázs"}
Pemphredon lugubris	{"gyászos feketedarázs"}
Pennisetia hylaeiformis	{málnagubacsszitkár}
Pentaphyllus chrysomeloides	{"sárga korhadékbogár"}
Pentaphyllus testaceus	{"kis korhadékbogár"}
Pentaria badia	{bukfencbogár}
Pentatoma rufipes	{"vöröslábú címerespoloska"}
Penthimia nigra	{"fekete kabóca"}
Penthophera morio	{"réti gyapjaslepke"}
Pentodon idiota	{butabogár}
Perapion affine	{"kék cickányormányos"}
Perapion curtirostre	{"kurtaorrú cickányormányos"}
Perapion marchicum	{"lilás cickányormányos"}
Perapion oblongum	{"fénytelen cickányormányos"}
Perapion violaceum	{sóska-cickányormányos}
Perca fluviatilis	{sügér}
Perccottus glenii	{amurgéb}
Perforatella bidentata	{"nagyfogú csiga"}
Perforatella dibothrion	{dibothrion-csiga}
Peribatodes rhomboidaria	{"rombuszrajzolatú faaraszoló"}
Pericartiellus telephii	{varjúháj-ormányos}
Periclepsis cinctana	{"galériás sodrómoly"}
Peridea anceps	{"füstös púposszövő"}
Peridroma saucia	{"nagy földibagoly"}
Perigona nigriceps	{"feketefejű komposztfutó"}
Perigrapha i-cinctum	{"tavaszi fésűsbagoly"}
Perigrapha munda	{"kétfoltos barkabagoly"}
Perileptus areolatus	{"apró fövenyfutonc"}
Perinephela lancealis	{"hosszúszárnyú tűzmoly"}
Peritelus familiaris	{kendermagbogár}
Peritrechus gracilicornis	{"feketés bodobács"}
Perittia farinella	{"fehér fűaknázómoly"}
Perittia herrichiella	{fagyalaknázómoly}
Perittia huemeri	{"ritka fűaknázómoly"}
Perizoma alchemillata	{árvacsalán-levélaraszoló}
Perizoma blandiata	{szemvidító-levélaraszoló}
Perizoma flavofasciata	{"sárgasávos levélaraszoló"}
Perizoma minorata	{"szemvidító araszoló"}
Perla bipunctata	{"óriás álkérész"}
Perotis lugubris	{"bronzos díszbogár"}
Petasina unidentata	{"egyfogú szőrőscsiga"}
Petrophora chlorosata	{páfrányaraszoló}
Pexicopia malvella	{mályvalevél-sarlósmoly}
Pezotettix giornae	{"kis hegyisáska"}
Phacophallus parumpunctatus	{"kevéspontos rovátkoltholyva"}
Phaenops cyanea	{"kék fürgedíszbogár"}
Phaeochrotes pudens	{tölgy-orrosbogár}
Phaiogramma etruscaria	{"fehérpettyes zöldaraszoló"}
Phalacronothus biguttatus	{"kétfoltos trágyabogár"}
Phalacronothus citellorum	{"ürgevendég trágyabogár"}
Phalacronothus quadrimaculatus	{"négyfoltos trágyabogár"}
Phalacrus caricis	{sás-kalászbogár}
Phalacrus championi	{"nyugati kalászbogár"}
Phalacrus corruscus	{"recés kalászbogár"}
Phalacrus fimetarius	{"közönséges kalászbogár"}
Phalacrus grossus	{"nagy kalászbogár"}
Phalacrus substriatus	{"vonalas kalászbogár"}
Phalera bucephala	{"sárgafoltos púposszövő"}
Phalonidia affinitana	{őszirózsa-fúrómoly}
Phalonidia albipalpana	{sóvirág-fúrómoly}
Phalonidia contractana	{"pipitér-fúrómoly "}
Phalonidia curvistrigana	{"árnyéksávos fúrómoly"}
Phalonidia gilvicomana	{"sárgatövű fúrómoly"}
Phalonidia manniana	{"turjáni fúrómoly"}
Phaneroptera falcata	{"zöld repülőszöcske"}
Phaneroptera nana	{"pontozott repülőszöcske"}
Phaneta pauperana	{gyepűrózsa-tükrösmoly}
Pharmacis carna	{"pirosas gyökérrágólepke"}
Phaulernis rebeliella	{"sújtásos íveltmoly"}
Pheletes aeneoniger	{"fényes bokorpattanó"}
Pheletes quercus	{"barna bokorpattanó"}
Phenacoccus aceris	{juhar-pajzstetű}
Phenacoccus evelinae	{fű-viaszospajzstetű}
Phenacoccus hordei	{gyökér-pajzstetű}
Phenacoccus mespili	{alma-vándorpajzstetű}
Phenacoccus piceae	{lucfenyő-vándorpajzstetű}
Pheosia tremula	{nyárfa-púposszövő}
Phiaris micana	{"ezüstös tükrösmoly"}
Phiaris obsoletana	{"homályos tükrösmoly"}
Phiaris scoriana	{"galajfonó tükrösmoly"}
Phiaris stibiana	{"sárgavillás tükrösmoly"}
Phiaris umbrosana	{"árnyéklakó tükrösmoly"}
Phibalapteryx virgata	{"szürke galajaraszoló"}
Phigalia pilosaria	{"zöldes tavasziaraszoló"}
Philaenus spumarius	{"változó tajtékoskabóca"}
Philaeus chrysops	{"piros ugrópók"}
Philanthus triangulum	{"közönséges méhfarkas"}
Philedone gerningana	{"erdei sodrómoly"}
Philedonides lunana	{"pimpószövő sodrómoly"}
Philedonides rhombicana	{"rozsdasárga sodrómoly"}
Philereme transversata	{varjútövis-araszoló}
Philereme vetulata	{"levélsodró araszoló"}
Philhygra balcanicola	{"berki penészholyva"}
Philhygra elongatula	{"patakparti penészholyva"}
Philhygra hygrotopora	{"csermelyfutó penészholyva"}
Philhygra luridipennis	{"csermelyparti penészholyva"}
Philhygra malleus	{"lápi penészholyva"}
Philhygra nannion	{"ártéri penészholyva"}
Philhygra sequanica	{"partlakó penészholyva"}
Philhygra terminalis	{"iszaplakó penészholyva"}
Philhygra tmolosensis	{"folyóparti penészholyva"}
Philhygra volans	{"posványlakó penészholyva"}
Philipomyia graeca	{"balkáni bögöly"}
Philochthus biguttatus	{"kétfoltos gyorsfutó"}
Philochthus guttula	{"foltosvégű gyorsfutó"}
Philochthus inoptatus	{"kereknyakú gyorsfutó"}
Philochthus mannerheimii	{"egyszínű gyorsfutó"}
Philodromus aureolus	{"aranyos karolópók"}
Philodromus dispar	{"lomb karolópók"}
Philodromus emarginatus	{"tigris karolópók"}
Philodromus margaritatus	{"szegélyes karolópók"}
Philodromus poecilus	{"cifra karolópók"}
Philonthus addendus	{"ércfényű ganajholyva"}
Philonthus albipes	{"mezei ganajholyva"}
Philonthus atratus	{"mocsári ganajholyva"}
Philonthus cognatus	{"fémes ganajholyva"}
Philonthus concinnus	{"közönséges ganajholyva"}
Philonthus confinis	{"keskenyfejű ganajholyva"}
Philonthus coprophilus	{"szegélyesszárnyú ganajholyva"}
Philonthus corruscus	{"vörösszárnyú ganajholyva"}
Philonthus corvinus	{"szurkos ganajholyva"}
Philonthus cruentatus	{"vörösfoltos ganajholyva"}
Philonthus debilis	{"kis ganajholyva"}
Philonthus decorus	{"érces ganajholyva"}
Philonthus discoideus	{"szegélyes ganajholyva"}
Philonthus diversiceps	{"gödörkés ganajholyva"}
Philonthus ebeninus	{"szénfekete ganajholyva"}
Philonthus fumarius	{"kormos ganajholyva"}
Philonthus intermedius	{"zöldes ganajholyva"}
Philonthus laminatus	{"fémzöld ganajholyva"}
Philonthus lepidus	{"rövidszárnyú ganajholyva"}
Philonthus longicornis	{"sárgacsípőjű ganajholyva"}
Philonthus mannerheimi	{"érdeshátú ganajholyva"}
Philonthus marginatus	{"szegélyeshátú ganajholyva"}
Philonthus micans	{"selyemfényű ganajholyva"}
Philonthus nigrita	{"tőzegkedvelő ganajholyva"}
Philonthus nitidicollis	{"kétfoltos ganajholyva"}
Philonthus parvicornis	{"rövidcsápú ganajholyva"}
Philonthus politus	{"dögész ganajholyva"}
Philonthus puella	{"alhavasi ganajholyva"}
Philonthus punctus	{"sokpontos ganajholyva"}
Philonthus quisquiliarius	{"zöldfényű ganajholyva"}
Philonthus rectangulus	{"szögletesfejű ganajholyva"}
Philonthus rotundicollis	{"kerekhátú ganajholyva"}
Philonthus rubripennis	{"sárgalábú ganajholyva"}
Philonthus rufimanus	{"vöröscombú ganajholyva"}
Philonthus rufipes	{"füstös ganajholyva"}
Philonthus salinus	{"sziki ganajholyva"}
Philonthus sanguinolentus	{"foltos ganajholyva"}
Philonthus spinipes	{"töviseslábú ganajholyva"}
Philonthus splendens	{"bronzfényű ganajholyva"}
Philonthus succicola	{"érceszöld ganajholyva"}
Philonthus tenuicornis	{"bronzos ganajholyva"}
Philonthus umbratilis	{"nagyszemű ganajholyva"}
Philonthus varians	{"változékony ganajholyva"}
Philonthus ventralis	{"posványlakó ganajholyva"}
Philonthus viridipennis	{"alföldi ganajholyva"}
Philopedon plagiatum	{lomhaormányos}
Philorhizus melanocephalus	{"sötétfejű kéregfutó"}
Philorhizus notatus	{"sárgavállú kéregfutó"}
Philorhizus quadrisignatus	{"foltos kéregfutó"}
Philorhizus sigma	{"szalagos kéregfutó"}
Philothermus evanescens	{"barna kéregbogár"}
Philothermus semistriatus	{"félsávos kéregbogár"}
Phimodera humeralis	{"rücskös pajzsospoloska"}
Phlegra fasciata	{"sávos ugrópók"}
Phloeocharis subtilissima	{"közönséges penészevőholyva"}
Phloeonomus minimus	{"érdes kéregholyva"}
Phloeonomus punctipennis	{"tölgyes kéregholyva"}
Phloeonomus pusillus	{"fenyves kéregholyva"}
Phloeopora opaca	{"alföldi szúvadászholyva"}
Phloeopora teres	{"közönséges szúvadászholyva"}
Phloeopora testacea	{"fenyves szúvadászholyva"}
Phloeosinus aubei	{borókaszú}
Phloeosinus thujae	{tujaszú}
Phloeostiba plana	{"dohányszínű kéregholyva"}
Phloeostichus denticollis	{"fogastorú mezgér"}
Phlogophora meticulosa	{"zöld csipkésbagoly"}
Phlogophora scita	{"halványzöld csipkésbagoly"}
Phloiotrya tenuis	{"hengeres komorka"}
Phlyctaenia coronata	{"koronás dudvamoly"}
Phlyctaenia perlucidalis	{"lápréti tűzmoly"}
Phlyctaenia stachydalis	{"mocsári dudvamoly"}
Phlyctaenodes cruentalis	{"ázsiai tűzmoly"}
Phoenicopterus roseus	{"rózsás flamingó"}
Pholcus opilionoides	{"kis álkaszáspók"}
Pholcus phalangioides	{"nagy álkaszáspók"}
Pholidoptera aptera	{"szárnyatlan avarszöcske"}
Pholidoptera fallax	{"galléros avarzsöcske"}
Pholidoptera griseoaptera	{"szürke avarszöcske"}
Pholioxenus schatzmayri	{"nagyszemű sutabogár"}
Phosphaenus hemipterus	{"csonkaszárnyú szentjánosbogár"}
Phosphuga atrata	{"bordás csigarabló"}
Photedes captiuncula	{"hegyi törpebagoly"}
Phoxinus phoxinus	{"fürge cselle"}
Phradonoma villosulum	{"pusztai porva"}
Phragmataecia castaneae	{"nádfúró lepke"}
Phragmatobia fuliginosa	{"füstös medvelepke"}
Phragmatobia luctifera	{"füstös medvelepke"}
Phrissotrichum rugicolle	{"serteszőrös cickányormányos"}
Phtheochroa annae	{"erdei fúrómoly"}
Phtheochroa duponchelana	{medveköröm-fúrómoly}
Phtheochroa fulvicinctana	{"sárgásfehér fúrómoly"}
Phtheochroa inopiana	{"turjáni sárgamoly"}
Phtheochroa procerana	{"fehér fúrómoly"}
Phtheochroa pulvillana	{spárgaszár-fúrómoly}
Phtheochroa purana	{rózsa-fúrómoly}
Phtheochroa rugosana	{földitök-fúrómoly}
Phtheochroa schreibersiana	{májusfa-fúrómoly}
Phtheochroa sodaliana	{kutyabenge-fúrómoly}
Phthorimaea operculella	{burgonya-sarlósmoly}
Phycita meliella	{"görög karcsúmoly"}
Phycita metzneri	{"vonalkás karcsúmoly"}
Phycita roborella	{"tölgyszövő karcsúmoly"}
Phycitodes albatella	{"csenevész karcsúmoly"}
Phycitodes binaevella	{"bogáncslakó karcsúmoly"}
Phycitodes inquinatella	{"mediterrán karcsúmoly"}
Phycitodes lacteella	{"kisázsiai karcsúmoly"}
Phycitodes maritima	{aggófű-karcsúmoly}
Phycitodes saxicola	{"délvidéki karcsúmoly"}
Phyla obtusa	{"tompaszögletű gyorsfutó"}
Phyllobius arborator	{"aranyos levélormányos"}
Phyllobius argentatus	{"ezüstös levélormányos"}
Phyllobius betulinus	{"változékony levélormányos"}
Phyllobius brevis	{"rövid ürömormányos"}
Phyllobius chloropus	{"zömök levélormányos"}
Phyllobius dispar	{"hasonló ürömormányos"}
Phyllobius glaucus	{éger-ormányos}
Phyllobius maculicornis	{galagonya-levélormányos}
Phyllobius montanus	{"hegyvidéki levélormányos"}
Phyllobius oblongus	{"közönséges levélormányos"}
Phyllobius pallidus	{tölgy-levélormányos}
Phyllobius pilicornis	{"sarkantyús levélormányos"}
Phyllobius pomaceus	{csalán-levélormányos}
Phyllobius pyri	{gyümölcsfa-levélormányos}
Phyllobius seladonius	{"tömpeorrú leválormányos"}
Phyllobius thalassinus	{"zöld pázsitfűormányos"}
Phyllobius vespertinus	{"pufókfejű levélormányos"}
Phyllobius virideaeris	{"halványzöld ormányos"}
Phyllocnistis labyrinthella	{"kígyóaknás fehérnyármoly"}
Phyllocnistis saligna	{"kígyóaknás fűzmoly"}
Phyllocnistis unipunctella	{"kígyóaknás nyármoly"}
Phyllocnistis xenia	{"kígyóaknás nyárfamoly"}
Phyllodesma ilicifolium	{cserlevélpohók}
Phyllodesma tremulifolium	{nyárlevélpohók}
Phyllodrepa floralis	{"virágkedvelő barázdásholyva"}
Phyllodrepa ioptera	{"fénylő barázdásholyva"}
Phyllodrepa nigra	{"szurkos barázdásholyva"}
Phyllodrepa salicis	{"cserjelakó barázdásholyva"}
Phyllodrepoidea crenata	{"barázdált felemásholyva"}
Phylloneta impressum	{"kóró törpepók"}
Phylloneta sisyphia	{"testvér törpepók"}
Phyllonorycter abrasella	{"cseraknázó sátorosmoly"}
Phyllonorycter acaciella	{"magyar sátorosmoly"}
Phyllonorycter acerifoliella	{"juharaknázó sátorosmoly"}
Phyllonorycter agilella	{"füstös sátorosmoly"}
Phyllonorycter apparella	{nyárfalevél-sátorosmoly}
Phyllonorycter blancardella	{"almalevélaknázó sátorosmoly"}
Phyllonorycter cavella	{szőrösnyír-sátorosmoly}
Phyllonorycter cerasinella	{"száraknázó sátorosmoly"}
Phyllonorycter comparella	{fehérnyár-sátorosmoly}
Phyllonorycter connexella	{"lápi sátorosmoly"}
Phyllonorycter coryli	{mogyorólevél-sátorosmoly}
Phyllonorycter corylifoliella	{almalevél-sátorosmoly}
Phyllonorycter cydoniella	{birslevél-sátorosmoly}
Phyllonorycter delitella	{"fakó sátorosmoly"}
Phyllonorycter distentella	{tölgyfa-sátorosmoly}
Phyllonorycter dubitella	{"fűzeslakó sátorosmoly"}
Phyllonorycter emberizaepenella	{"loncaknázó sátorosmoly"}
Phyllonorycter esperella	{gyertyán-sátorosmoly}
Phyllonorycter fraxinella	{rekettyelevél-sátorosmoly}
Phyllonorycter froelichiella	{"mocsári sátorosmoly"}
Phyllonorycter harrisella	{tölgylevél-sátorosmoly}
Phyllonorycter heegeriella	{kocsányostölgy-sátorosmoly}
Phyllonorycter helianthemella	{napvirág-sátorosmoly}
Phyllonorycter hilarella	{kecskefűzlevél-sátorosmoly}
Phyllonorycter ilicifoliella	{"magyalaknázó sátorosmoly"}
Phyllonorycter insignitella	{lóhere-sátorosmoly}
Phyllonorycter kleemannella	{"láperdei sátorosmoly"}
Phyllonorycter lantanella	{"bangitarágó sátorosmoly"}
Phyllonorycter lautella	{"hegyi sátorosmoly"}
Phyllonorycter leucographella	{tűztövis-sátorosmoly}
Phyllonorycter maestingella	{bükklevél-sátorosmoly}
Phyllonorycter medicaginella	{somkóró-sátorosmoly}
Phyllonorycter mespilella	{naspolya-sátorosmoly}
Phyllonorycter messaniella	{gesztenye-sátorosmoly}
Phyllonorycter muelleriella	{tölgyfalevél-sátorosmoly}
Phyllonorycter nicellii	{"mogyoróaknázó sátorosmoly"}
Phyllonorycter nigrescentella	{"hereaknázó sátorosmoly"}
Phyllonorycter oxyacanthae	{kökénylevél-sátorosmoly}
Phyllonorycter parisiella	{"kardsávú sátorosmoly"}
Phyllonorycter pastorella	{"kormos sátorosmoly"}
Phyllonorycter platani	{platánlevél-sátorosmoly}
Phyllonorycter populifoliella	{feketenyár-sátorosmoly}
Phyllonorycter pyrifoliella	{"ékfoltos sátorosmoly"}
Phyllonorycter quercifoliella	{"közönséges sátorosmoly"}
Phyllonorycter quinqueguttella	{cinegefűz-sátorosmoly}
Phyllonorycter rajella	{enyveséger-sátorosmoly}
Phyllonorycter robiniella	{akáclevél-sátorosmoly}
Phyllonorycter roboris	{"tölgyaknázó sátorosmoly"}
Phyllonorycter sagitella	{rezgőnyár-sátorosmoly}
Phyllonorycter salicicolella	{kecskefűz-sátorosmoly}
Phyllonorycter salictella	{"fűzligeti sátorosmoly"}
Phyllonorycter schreberella	{"szilaknázó sátorosmoly"}
Phyllonorycter scitulella	{"sárgafejű sátorosmoly"}
Phyllonorycter sorbi	{berkenye-sátorosmoly}
Phyllonorycter spinicolella	{kökényes-sátorosmoly}
Phyllonorycter staintoniella	{"zanótlakó sátorosmoly"}
Phyllonorycter stettinensis	{"égerlakó sátorosmoly"}
Phyllonorycter tenerella	{"gyertyánaknázó sátorosmoly"}
Phyllonorycter tristrigella	{"háromsávos sátorosmoly"}
Phyllonorycter ulmifoliella	{nyírlevél-sátorosmoly}
Phyllopertha horticola	{"kerti szipoly"}
Phyllophila obliterata	{"apró ürömbagoly"}
Phyllophya laciniata	{"lándzsás karimáspoloska"}
Phylloporia bistrigella	{nyírlevél-virágmoly}
Phylloscopus bonelli	{Bonelli-füzike}
Phyllostroma myrtilli	{áfonya-gyapjaspajzstetű}
Phylus coryli	{mogyorópoloska}
Phymata crassipes	{"fogólábú poloska"}
Phymatopus hecta	{"aranyló gyökérrágólepke"}
Physa fontinalis	{hólyagcsiga}
Physatocheila dumetorum	{gyümölcsfa-csipkéspoloska}
Physella acuta	{"hegyes hólyagcsiga"}
Physokermes hemicryphus	{"kis lucfenyő-pajzstetű"}
Physokermes inopinatus	{"örvös pajzstetű"}
Physokermes piceae	{"nagy lucfenyő-pajzstetű"}
Phytobaenus amabilis	{"északi korhóbogár"}
Phytocoris populi	{nyárfapoloska}
Phytocoris tiliae	{hársfapoloska}
Phytocoris ulmi	{szil-fapoloska}
Phytocoris varipes	{"tarkalábú fapoloska"}
Phytoecia caerulea	{"fémzöld fűcincér"}
Phytometra viridaria	{"pirossávos apróbagoly"}
Picromerus bidens	{"tüskés címerespoloska"}
Pieris brassicae	{káposzta-fehérlepke}
Pieris bryoniae	{"hegyi fehérlepke"}
Pieris mannii	{"magyar fehérlepke"}
Pieris napi	{repce-fehérlepke}
Pieris rapae	{répa-fehérlepke}
Piesma capitatum	{"közönséges recéspoloska"}
Piesma maculatum	{"nyakas recéspoloska"}
Piesma variabile	{porcikapoloska}
Piezodorus lituratus	{"bíboros címerespoloska"}
Pilophorus confusus	{"kis ezüstös-mezeipoloska"}
Pilophorus perplexus	{"ezüstös mezeipoloska"}
Pima boisduvaliella	{"ezüstszegélyű karcsúmoly"}
Piniphila bifasciana	{"tobozrágó tükrösmoly"}
Pionosomus opacellus	{"apró bodobács"}
Pipistrellus kuhlii	{"fehérszélű törpedenevér"}
Pipistrellus nathusii	{"durvavitorlájú törpedenevér"}
Pipistrellus pipistrellus	{"közönséges törpedenevér"}
Pipistrellus pygmaeus	{"szoprán törpedenevér"}
Pirapion immune	{"immúnis cickányormányos"}
Pirapion redemptum	{"rekettyekedvelő cickányormányos"}
Pirata latitans	{"kis kalózpók"}
Pirata piraticus	{kalózpók}
Pisaura mirabilis	{csodáspók}
Piscicola geometra	{"közönséges halpióca"}
Pisidium amnicum	{"nagy borsókagyló"}
Pison atrum	{"fekete pókölő"}
Pissodes castaneus	{"fehérfoltos fenyőbogár"}
Pissodes harcyniae	{lucfenyőbogár}
Pissodes piceae	{fenyőbogár}
Pissodes pini	{"öves fenyőbogár"}
Pissodes piniphilus	{"erdei fenyőbogár"}
Pissodes scabricollis	{"kis lucfenyőbogár"}
Pissodes validirostris	{"tobozevő fenyőbogár"}
Pistius truncatus	{"csonka karolópók"}
Pityogenes bidentatus	{"kétfogú firkálószú"}
Pityogenes chalcographus	{firkálószú}
Pityokteines curvidens	{"görbefogú szú"}
Pityokteines vorontzowi	{"vastagfogú szú"}
Pityophagus ferrugineus	{fenyves-kéregfénybogár}
Pityophagus laevior	{"keskeny kéregfénybogár"}
Pityophagus quercus	{tölgy-kéregfénybogár}
Pityophthorus micrographus	{"törpe fenyőszú"}
Pityophthorus pubescens	{"barázdázott fenyőszú"}
Placobdella costata	{teknőspióca}
Placonotus testaceus	{tölgy-szegélyeslapbogár}
Placusa adscita	{"tölgyes laposholyva"}
Placusa atrata	{"kormos laposholyva"}
Placusa complanata	{"lemezszerű laposholyva"}
Placusa depressa	{"északi laposholyva"}
Placusa pumilio	{"aprócska laposholyva"}
Placusa tachyporoides	{"sárgás laposholyva"}
Plagiognathus chrysanthemi	{"fakó törpepoloska"}
Plagiognathus fulvipennis	{kígyószisz-törpepoloska}
Plagiogonus putridus	{"parányi trágyabogár","parányi trágyabogár"}
Plagodis dolabraria	{"baltaszárnyú araszoló"}
Plagodis pulveraria	{"szemcsés araszoló"}
Planchonia arabidis	{borostyán-himlőspajzstetű}
Planeustomus heydeni	{"sziki aknászholyva"}
Planeustomus palpalis	{"kis aknászholyva"}
Planococcus vovae	{boróka-viaszospajzstetű}
Planolinus uliginosus	{"erdei trágyabogár"}
Planorbarius corneus	{tányércsiga}
Planorbis carinatus	{"tarajos csiga"}
Planorbis planorbis	{"éles csiga"}
Plataphus prasinus	{"fekete gyorsfutó"}
Plataraea dubiosa	{"délvidéki penészholyva"}
Plataraea nigriceps	{"északi penészholyva"}
Plataraea nigrifrons	{"sötétfejű penészholyva"}
Plataraea sordida	{"barna penészholyva"}
Plataraea spaethi	{"pusztai penészholyva"}
Platnickina tincta	{"púpos törpepók"}
Platycerus caprea	{"nagy fémesszarvasbogár"}
Platycerus caraboides	{"kis fémesszarvasbogár"}
Platycis cosnardi	{"szegélyesnyakú hajnalbogár"}
Platycis minutus	{"kis hajnalbogár"}
Platycleis affinis	{"púposhasú rétiszöcske"}
Platycleis albopunctata	{"szürke rétiszöcske"}
Platycleis montana	{"homokpusztai szöcske"}
Platycleis veyseli	{"sávos rétiszöcske"}
Platycnemis pennipes	{"széleslábú szitakötő"}
Platydema dejeani	{"fekete taplóbogár"}
Platydema violaceum	{"fémkék taplóbogár"}
Platyderus rufus transalpinus	{"lapos rőtfutó"}
Platydracus chalcocephalus	{"rezes holyva"}
Platydracus fulvipes	{"fémkék holyva"}
Platydracus latebricola	{"érces holyva"}
Platydracus stercorarius	{"trágyatúró holyva"}
Platyedra subcinerea	{"sárgásszürke sarlósmoly"}
Platylomalus complanatus	{"lapított kéregsutabogár"}
Platymetopius undatus	{nyírfakabóca}
Platynaspis luteorubra	{"négypettyes szerecsenböde"}
Platynus livens	{"homlokfoltos kisfutó"}
Platynus scrobiculatus	{"alpesi kisfutó"}
Platyola austriaca	{"zsindelyes komposztholyva"}
Platyperigea kadenii	{"őszi selymesbagoly"}
Platyperigea terrea	{mécsvirág-selymesbagoly}
Platyplax salviae	{zsályabodobács}
Platyptilia farfarellus	{aggófű-tollasmoly}
Platyptilia gonodactyla	{"ékmintás tollasmoly"}
Platyptilia nemoralis	{"berki tollasmoly"}
Platyptilia tesseradactyla	{"barnásfehér tollasmoly"}
Platypus cylindrus	{hosszúlábúszú}
Platypus oxyurus	{"ékes hosszúlábúszú"}
Platyrhinus resinosus	{"nagy orrosbogár"}
Platyscelis hungarica	{"pusztai gyászbogár"}
Platyscelis melas	{"alföldi gyászbogár"}
Platyscelis polita	{"sima gyászbogár"}
Platysoma compressum	{"közönséges lapossutabogár"}
Platystethus alutaceus	{"mocsári korhóholyva"}
Platystethus arenarius	{"trágyatúró korhóholyva"}
Platystethus capito	{"busafejű korhóholyva"}
Platystethus cornutus	{"iszaptúró korhóholyva"}
Platystethus degener	{"iszaplakó korhóholyva"}
Platystethus nitens	{"réti korhóholyva"}
Platystethus rufospinus	{"sziki korhóholyva"}
Platystethus spinosus	{"pusztai korhóholyva"}
Platystomos albinus	{"nagybajuszú orrosbogár"}
Platytes alpinella	{moharágómoly}
Platytes cerussella	{"törpe fűgyökérmoly"}
Plea minutissima	{"vízi törpepoloska"}
Plebeius agestis	{szerecsenboglárka}
Plebeius argus	{"ezüstös plebejusboglárka"}
Plebeius argyrognomon	{"csillogó plebejusboglárka"}
Plebeius artaxerxes	{"bükki szerecsenboglárka"}
Plebeius artaxerxes issekutzi	{"bükki szerecsenboglárka"}
Plebeius eumedon	{gólyaorr-boglárka}
Plegaderus caesus	{"közönséges vágottsutabogár"}
Plegaderus dissectus	{"hosszúkás vágottsutabogár"}
Plegaderus saucius	{"fényes vágottsutabogár"}
Plegaderus vulneratus	{"fekete vágottsutabogár"}
Pleganophorus bispinosus	{"hangyász álböde"}
Plemyria rubiginata	{éger-tarkaaraszoló}
Pleurophorus caesus	{"hengeres trágyabogár"}
Pleurophorus pannonicus	{"alföldi trágyabogár"}
Pleuroptya balteata	{"szömörcerágó tűzmoly"}
Pleuroptya ruralis	{"csalánevő tűzmoly"}
Pleurota aristella	{"ezüstsávos csíkosmoly"}
Pleurota bicostella	{csarabos-csíkosmoly}
Pleurota marginella	{"barnasávos csíkosmoly"}
Pleurota pyropella	{"tüzes csíkosmoly"}
Pleurotobia magnifica	{"pompás tarkaholyva"}
Plodia interpunctella	{aszalványmoly}
Plusia festucae	{"kockás ezüstbagoly"}
Plutella porrectella	{"estikerágó tarkamoly"}
Plutella xylostella	{káposztamoly}
Pocadius adustus	{"gyapjas pöfetegfénybogár"}
Pocadius ferrugineus	{"közönséges pöfetegfénybogár"}
Podalonia hirsuta	{"szőrös homoklakó"}
Podeonius acuticornis	{"fűrészescsápú pattanó"}
Podisma pedestris	{"tarka hegyisáska"}
Podonta nigrita	{szerecsenbogár}
Podops inuncta	{"szarvas pajzsospoloska"}
Poecilia reticulata	{"szivárványos guppi"}
Poecilia sphenops	{"yukatáni fogasponty"}
Poecilimon affinis	{"közönséges pókszöcske"}
Poecilimon fussii	{Fuss-pókszöcske}
Poecilimon schmidtii	{Schmidt-pókszöcske}
Poecilocampa populi	{nyárfaszövő}
Poecilonota variolosa	{"szeplős díszbogár"}
Poecilus cupreus	{"rezes gyászfutó"}
Poecilus kekesiensis	{"hortobágyi gyászfutó"}
Poecilus lepidus	{"díszes gyászfutó"}
Poecilus puncticollis	{"sziki gyászfutó"}
Poecilus punctulatus	{"pusztai gyászfutó"}
Poecilus sericeus	{"szegélyes gyászfutó"}
Poecilus striatopunctatus	{"parti gyászfutó"}
Poecilus versicolor	{"smaragd gyászfutó"}
Pogonus luridipennis	{"pompás szikifutonc"}
Pogonus peisonis	{"karcsú szikifutonc"}
Polia bombycina	{"palaszürke nagyszegfűbagoly"}
Polia nebulosa	{"borús nagyszegfűbagoly"}
Polistes dominulus	{"déli papírosdarázs"}
Polistes nimpha	{padlásdarázs}
Polistichus connexus	{"barna sutafutó"}
Polochrum repandum	{"kékdongó áldarázs"}
Polydrusus cervinus	{"aranyporos lombormányos"}
Polydrusus confluens	{"sávos lombormányos"}
Polydrusus corruscus	{kecskefűz-lombormányos}
Polydrusus formosus	{"selymes lombormányos"}
Polydrusus fulvicornis	{"göröngyöstorú lombormányos"}
Polydrusus marginatus	{"szegélyes lombormányos"}
Polydrusus mollis	{"termetes lombormányos"}
Polydrusus picus	{"foltos lombormányos"}
Polydrusus pilosus	{"szőrfoltos lombormányos"}
Polydrusus pterygomalis	{"púposfejű lombormányos"}
Polydrusus sparsus	{éger-lombormányos}
Polydrusus tereticollis	{"szalagos lombormányos"}
Polydrusus viridicinctus	{"zöldöves lombormányos"}
Polygraphus grandiclava	{"irkáló fenyőszú"}
Polygraphus poligraphus	{"firkáló fenyőszú"}
Polygraphus subopacus	{"szemölcsös fenyőszú"}
Polymerus cognatus	{"piroscsíkos mezeipoloska"}
Polymerus unifasciatus	{galaj-mezeipoloska}
Polymixis polymita	{Kankalin-tarkabagoly}
Polymixis rufocincta	{"villányi télibagoly"}
Polymixis xanthomista	{"sárgamintás tarkabagoly"}
Polyommatus bellargus	{"égszínkék sokpöttyösboglárka"}
Polyommatus coridon	{"ezüstkék sokpöttyösboglárka"}
Polyommatus daphnis	{"csipkés sokpöttyösboglárka"}
Polyommatus dorylas	{"fénylő boglárka"}
Polyommatus icarus	{Ikarusz-sokpettyesboglárka}
Polyommatus semiargus	{"aprószemes sokpöttyösboglárka"}
Polyommatus thersites	{"ibolyaszín boglárka"}
Polyphaenis sericata	{"selyemfényű bagolylepke"}
Polyphylla fullo	{"csapó cserebogár"}
Polyploca ridens	{"zöldes pihésszövő"}
Polypogon strigilata	{"hosszúcsápú karcsúbagoly"}
Polypogon tentacularia	{"sötétaljú karcsúbagoly"}
Polysarcus denticauda	{"fogasfarkú szöcske"}
Polysticta stelleri	{Steller-pehelyréce}
Polystomophora ostiaplurima	{juhar-ágpajzstetű}
Pomatias elegans	{"nyugati ajtóscsiga"}
Pomatias rivularis	{"keleti ajtóscsiga"}
Pomatinus substriatus	{"parti fülescsápúbogár"}
Pompilus cinereus	{"hamuszínű útonálló"}
Pontia edusa	{"keleti rezedalepke"}
Poophagus robustus	{"robusztus kányafűormányos"}
Poophagus sisymbrii	{kányafűormányos}
Populicerus populi	{"kétfoltos kabóca"}
Porcinolus murinus	{"egérszínű labdacsbogár"}
Poromniusa procidua	{"berki fenyérholyva"}
Porotachys bisulcatus	{"zömök martfutó"}
Porphyrophora polonica	{"lengyel bíborpajzstetű"}
Porrittia galactodactyla	{"tejfehér tollasmoly"}
Porthmidius austriacus	{"fakó levélpattanó"}
Potamophilus acuminatus	{"nagy karmosbogár"}
Povolnya leucapennella	{"kénszínű keskenymoly"}
Praesolenobia clathrella	{"hordós csövesmoly"}
Praestochrysis megerlei	{"ötfogú fémdarázs"}
Prays fraxinella	{égerrügymoly}
Prays ruficeps	{"havasi égermoly"}
Pria dulcamarae	{ebszőlő-fénybogár}
Princidium punctulatum	{"pontozott gyorsfutó"}
Prinerigone vagans	{"tüskésfejű pók"}
Priobium carpini	{"csuklyás álszú"}
Priocnemis exaltata	{"nagykarmú útonálló"}
Priocnemis perturbator	{"tavaszi útonálló"}
Priocnemis pusilla	{"kis útonálló"}
Prionocyphon serricornis	{"fűrészescsápú rétbogár"}
Prionychus ater	{"sötét alkonybogár"}
Prionychus melanarius	{"komor alkonybogár"}
Prionyx kirbyi	{"karcsú sáskairtó"}
Pristerognatha penthinana	{mimózamoly}
Probaticus subrugosus	{"ráncos gyászbogár"}
Prochlidonia amiantana	{"fényes sárgamoly"}
Prochoreutis myllerana	{"pompás levélmoly"}
Prochoreutis sehestediana	{seprencelevélmoly}
Prochoreutis stellaris	{szittyófúrómoly}
Procraerus tibialis	{korhópattanó}
Procyon lotor	{mosómedve}
Prolita solutella	{"galajszövő sarlósmoly"}
Pronomaea korgei	{"kis csőrösholyva"}
Propylea quatuordecimpunctata	{"tizennégypettyes füsskata"}
Prostemma aeneicolle	{"feketehátú tolvajpoloska"}
Prostemma guttula	{"vöröslábú tolvajpoloska"}
Prosternon chrysocomum	{"nagy kockáspattanó"}
Prosternon tessellatum	{"kis kockáspattanó"}
Protaetia aeruginosa	{"pompás virágbogár"}
Protaetia affinis	{"smaragdzöld virágbogár"}
Protaetia cuprea	{"olajzöld virágbogár"}
Protaetia cuprea obscura	{"olajzöld virágbogár"}
Protaetia fieberi	{"rezes virágbogár"}
Protaetia lugubris	{"márványos virágbogár"}
Protaetia ungarica	{"magyar virágbogár"}
Protapion apricans	{lóheremag-cickányormányos}
Protapion assimile	{virágrontó-cickányormányos}
Protapion difforme	{"formáslábú cickányormányos"}
Protapion dissimile	{lóheremag-cickányormányos}
Protapion filirostre	{"fekete cickányormányos"}
Protapion fulvipes	{vadhere-cickányormányos}
Protapion gracilipes	{"piroscsápú cickányormányos"}
Protapion interjectum	{"téveszthető cickányormányos"}
Protapion nigritarse	{"kis cickányormányos"}
Protapion ononidis	{iglice-cickányormányos}
Protapion ruficrus	{bércihere-cickányormányos}
Protapion trifolii	{lóherevirág-cickányormányos}
Protapion varipes	{"görbelábú cickányormányos"}
Proteinus brachypterus	{"közönséges sutaholyva"}
Proteinus crenulatus	{"magashegyi sutaholyva"}
Proteinus laevigatus	{"domború sutaholyva"}
Proteinus ovalis	{"széles sutaholyva"}
Proterorhinus marmoratus	{"tarka géb"}
Protodeltote pygarga	{"fehérsávos apróbagoly"}
Protopirapion atratulum	{"dundi cickányormányos"}
Protopirapion kraatzii	{kraatz-cickányormányos}
Protoschinia scutosa	{"tüskéslábú nappalibagoly"}
Proutia betulina	{nyírfa-zsákhordólepke}
Proxenus lepigone	{"fényesszárnyú lápibagoly"}
Psacasta exanthematica	{kígyósziszpoloska}
Psallidium maxillosum	{"fekete barkó"}
Psallus ambiguus	{almafapoloska}
Psallus betuleti	{nyírfapoloska}
Psallus lepidus	{kőrisfapoloska}
Psammodius asper	{"homoktúró trágyabogár"}
Psammodius danubialis	{"dunamenti trágyabogár"}
Psammodius laevipennis	{"folyóparti trágyabogár"}
Psammoecus bipuncatatus	{"kétpettyes fogasnyakú-lapbogár"}
Psammotis pulveralis	{"rozsdasárga tűzmoly"}
Pselnophorus heterodactyla	{"kormos tollasmoly"}
Psen ater	{"nyeleshasú darázs"}
Psenulus fuscipennis	{"közönséges szederlakó"}
Psenulus pallipes	{"gyakori szederlakó"}
Pseudanodonta complanata	{"lapos tavikagyló"}
Pseudanostirus globicollis	{"gömbnyakú pattanó"}
Pseudapion fulvirostre	{zilizréme-cickányormányos}
Pseudapion rufirostre	{"vörösorrvégű cickányormányos"}
Pseudapis femoralis	{"dagadtcombú méh"}
Pseudapis unidentata	{útiméh}
Pseudargyrotoza conwagana	{"ezüstmintás sodrómoly"}
Pseudatemelia flavifrontella	{"avarlakó díszmoly"}
Pseudatemelia josephinae	{"lengyel díszmoly"}
Pseudatemelia subochreella	{"nyírlakó díszmoly"}
Pseudaulacaspis pentagona	{eper-pajzstetű}
Pseudepierus italicus	{"olasz erdeisutabogár"}
Pseudeulia asinana	{"korai sodrómoly"}
Pseudeustrotia candidula	{"fehéres apróbagoly"}
Pseudicius encarpatus	{kéregugrópók}
Pseudochermes fraxini	{kőris-pajzstetű}
Pseudochoragus piceus	{"parányi orrosbogár"}
Pseudocistela ceramboides	{"narancssárga alkonybogár"}
Pseudocleonus cinereus	{"hamvas répabarkó"}
Pseudohermenias abietana	{fenyőtű-tükrösmoly}
Pseudoips prasinana	{bükkfa-zöldbagoly}
Pseudomalus auratus	{"gyakori fémdarázs"}
Pseudomalus pusillus	{"piciny fémdarázs"}
Pseudomedon obsoletus	{"egyszínű rétiholyva"}
Pseudomyllocerus sinuatus	{szeder-ormányos}
Pseudoophonus calceatus	{"csupasz selymesfutrinka"}
Pseudoophonus griseus	{"kis selymesfutrinka"}
Pseudoophonus rufipes	{"nagy selymesfutrinka"}
Pseudopanthera macularia	{"párducfoltos araszoló"}
Pseudoperapion brevirostre	{"fényesorrú cickányormányos"}
Pseudophilotes vicrama schiffermuelleri	{"apró boglárka"}
Pseudopostega auritella	{"gólyahíraknázó aprómoly"}
Pseudopostega crepusculella	{"tarka aprómoly"}
Pseudoprotapion astragali	{csüdfű-cickányormányos}
Pseudoprotapion elegantulum	{"elegáns cickányormányos"}
Pseudoprotapion ergenense	{"aranyoszöld cickányormányos"}
Pseudoptilinus fissicollis	{hárs-álszú}
Pseudorchestes cinereus	{"fésült bolhaormányos"}
Pseudorchestes persimilis	{bolhafű-bolhaormányos}
Pseudorchestes pratensis	{imola-bolhaormányos}
Pseudosciaphila branderiana	{"homoki tükrösmoly"}
Pseudosemiris kaufmanni	{"busafejű humuszholyva"}
Pseudostenapion simum	{"nyurga cickányormányos"}
Pseudostyphlus pillumus	{kamilla-sertésormányos}
Pseudoswammerdamia combinella	{"szemfoltos tarkamoly"}
Pseudotelphusa paripunctella	{"pontozott borzasmoly"}
Pseudotelphusa scalella	{"moharágó sarlósmoly"}
Pseudotelphusa tessella	{sóskafamoly}
Pseudoterpna pruinata	{"hamvas zöldaraszoló"}
Pseudotrichia rubiginosa	{"szőrös csiga"}
Pseudotriphyllus suturalis	{"apró gombabogár"}
Pseudovadonia livida	{"barnás virágcincér"}
Psilococcus ruber	{piros-sáspajzstetű}
Psithyrus barbutellus	{"kerti álposzméh"}
Psithyrus campestris	{"mezei álposzméh"}
Psithyrus vestalis	{"földi álposzméh"}
Psoa viennensis	{venyige-csuklyásszú}
Psophus stridulus	{"kereplő sáska"}
Psoricoptera gibbosella	{"levélsodró sarlósmoly"}
Psorosa dahliella	{"sárgacsíkos karcsúmoly"}
Psyche casta	{"fényes zsákhordólepke"}
Psyche crassiorella	{"vaskos zsákhordólepke"}
Psychidea nudella	{"pőre zsákhordólepke"}
Psychoides verhuella	{zuzmóevőmoly}
Psyllobora vigintiduopunctata	{"huszonkétpettyes katica"}
Pteleobius kraatzii	{"tarka szilszú"}
Pteleobius vittatus	{szilszú}
Pterocheilus phaleratus	{"kis szőrösnyelvű darázs"}
Pterolepis germanica	{"német szöcske"}
Pterolonche albescens	{"fehér rétimoly"}
Pterolonche inspersa	{"barna rétimoly"}
Pteronemobius heydenii	{"mocsári tücsök"}
Pterophorus ischnodactyla	{szulák-tollasmoly}
Pterophorus pentadactyla	{"fehér tollasmoly"}
Pterostichus aethiops	{"felföldi gyászfutó"}
Pterostichus anthracinus biimpressus	{"fekete gyászfutó"}
Pterostichus aterrimus	{"mocsári gyászfutó"}
Pterostichus burmeisteri	{"fémes gyászfutó"}
Pterostichus calvitarsis	{"bihari gyászfutó"}
Pterostichus chameleon	{"barázdáltlábú gyászfutó"}
Pterostichus cursor	{"színjátszó gyászfutó"}
Pterostichus cylindricus	{"hengeres gyászfutó"}
Pterostichus diligens	{"réti gyászfutó"}
Pterostichus elongatus	{"nyúlánk gyászfutó"}
Pterostichus fasciatopunctatus	{"pontozott gyászfutó"}
Pterostichus gracilis	{"kecses gyászfutó"}
Pterostichus hungaricus	{"magyar gyászfutó"}
Pterostichus incommodus	{"zömök gyászfutó"}
Pterostichus leonisi	{"piroslábú gyászfutó"}
Pterostichus macer	{"lapos gyászfutó"}
Pterostichus melanarius	{"közönséges gyászfutó"}
Pterostichus melas	{"fényes gyászfutó"}
Pterostichus minor	{"kis gyászfutó"}
Pterostichus niger	{"komor gyászfutó"}
Pterostichus nigrita	{"sötét gyászfutó"}
Pterostichus oblongopunctatus	{"gödörkés gyászfutó"}
Pterostichus ovoideus	{"laposszemű gyászfutó"}
Pterostichus piceolus	{"latorcai gyászfutó"}
Pterostichus piceolus latoricaensis	{"latorcai gyászfutó"}
Pterostichus rhaeticus	{"hegyvidéki gyászfutó"}
Pterostichus strenuus	{"gombszemű gyászfutó"}
Pterostichus subsinuatus	{"alhavasi gyászfutó"}
Pterostichus taksonyis	{Taksony-gyászfutó}
Pterostichus transversalis	{"széles gyászfutó"}
Pterostichus unctulatus	{"szurokbarna gyászfutó"}
Pterostichus vernalis	{"ligeti gyászfutó"}
Pterostoma palpina	{"csőrös púposszövő"}
Pterothrixidia impurella	{"budai karcsúmoly"}
Pterothrixidia rufella	{"vörös karcsúmoly"}
Pterotmetus staphyliniformis	{"kurtaszárnyú bodobács"}
Pterotopteryx dodecadactyla	{"loncduzzasztó soktollúmoly"}
Ptilinus fuscus	{"bordás álszú"}
Ptilinus pectinicornis	{"fésűscsápú álszú"}
Ptilocephala muscella	{"tollas zsákhordólepke"}
Ptilocephala plumifera	{"alföldi zsákhordólepke"}
Ptilodon capucina	{"tevenyakú púposszövő"}
Ptilodon cucullina	{"csuklyás púposszövő"}
Ptilophora plumigera	{"tollas púposszövő"}
Ptilophorus dufouri	{"szürke darázsbogár"}
Ptinomorphus imperialis	{"díszes álszú"}
Ptinomorphus regalis	{"rajzos álszú"}
Ptinus bicinctus	{"kétöves tolvajbogár"}
Ptinus calcaratus	{"krétasz?r? tolvajbogár"}
Ptinus capellae	{"kereknyakú tolvajbogár"}
Ptinus clavipes	{"egyszín? tolvajbogár"}
Ptinus coarcticollis	{"simasz?r? tolvajbogár"}
Ptinus dubius	{feny?-tolvajbogár}
Ptinus fur	{"közönséges tolvajbogár"}
Ptinus latro	{"egyszínű tolvajbogár"}
Ptinus pilosus	{"sarkantyús tolvajbogár"}
Ptinus podolicus	{"podóliai tolvajbogár"}
Ptinus rufipes	{"vöröslábú tolvajbogár"}
Ptinus schlerethi	{"bundásnyakú tolvajbogár"}
Ptinus sexpunctatus	{"hatfoltos tolvajbogár"}
Ptinus subpilosus	{"keskenynyakú tolvajbogár"}
Ptinus tectus	{"rövidcsápú tolvajbogár"}
Ptinus variegatus	{"tarka tolvajbogár"}
Ptinus villiger	{"hosszúsz?r? tolvajbogár"}
Ptocheuusa abnormella	{peremizsvirág-sarlósmoly}
Ptocheuusa inopella	{gyopárvirág-sarlósmoly}
Ptocheuusa paupella	{bolhafű-sarlósmoly}
Ptosima undecimmaculata	{"sárgafoltos díszbogár"}
Ptycholoma lecheana	{"ezüstsávos sodrómoly"}
Ptycholomoides aeriferana	{vörösfenyő-sodrómoly}
Pulvinaria betulae	{"gyapjas pajzstetű"}
Pulvinaria floccifera	{tea-gyapjaspajzstetű}
Pulvinaria populi	{nyár-gyapjaspajzstetű}
Pulvinaria ribesiae	{ribiske-gyapjaspajzstetű}
Punctum pygmaeum	{paránycsiga}
Pungeleria capreolaria	{"hegyi szemcsésaraszoló"}
Pupilla muscorum	{bábcsiga}
Puto pilosellae	{kakukkfű-viaszospajzstetű}
Pycnomerus terebrans	{"bordás héjbogár"}
Pycnota paradoxa	{"fészeklakó trapézfejűholyva"}
Pygolampis bidentata	{"karcsú rablópoloska"}
Pyncostola bohemiella	{"cseh sarlósmoly"}
Pyralis farinalis	{lisztilonca}
Pyralis perversalis	{"pusztai fényilonca"}
Pyralis regalis	{"pompás fényilonca"}
Pyramidula pusilla	{aprócsiga}
Pyrausta aurata	{"aranyló bíbormoly"}
Pyrausta castalis	{"déli bíbormoly"}
Pyrausta cingulata	{"fehéröves kormosmoly"}
Pyrausta coracinalis	{"fekete kormosmoly"}
Pyrausta despicata	{"réti bíbormoly"}
Pyrausta falcatalis	{"hegyi bíbormoly"}
Pyrausta nigrata	{"fehérpettyes kormosmoly"}
Pyrausta obfuscata	{"félholdas kormosmoly"}
Pyrausta ostrinalis	{"ritka bíbormoly"}
Pyrausta porphyralis	{"tarka bíbormoly"}
Pyrausta purpuralis	{"közönséges bíbormoly"}
Pyrausta rectefascialis	{"lengyel kormosmoly"}
Pyrausta sanguinalis	{"vérszínű bíbormoly"}
Pyrausta virginalis	{"alföldi bíbormoly"}
Pyrgus alveus	{"hegyi busalepke"}
Pyrgus armoricanus	{"feles busalepke"}
Pyrgus carthami	{"nagy busalepke"}
Pyrgus malvae	{"kis busalepke"}
Pyrgus serratulae	{"homályos busalepke"}
Pyrochroa coccinea	{"nagy bíborbogár"}
Pyrochroa serraticornis	{közép-bíborbogár}
Pyroderces argyrogrammos	{"ezüstmintás tündérmoly"}
Pyroderces klimeschi	{"mocsári tündérmoly"}
Pyronia tithonus	{"kis ökörszemlepke"}
Pyropteron affine	{napvirágszitkár}
Pyropteron muscaeforme	{istácgyökérszitkár}
Pyropteron triannuliforme	{sóskaszitkár}
Pyropterus nigroruber	{"skarlátvörös hajnalbogár"}
Pyrrhia umbra	{"sötét tűzbagoly"}
Pyrrhocoris apterus	{"verőköltő bodobács"}
Pyrrhocoris marginatus	{"szegélyes bodobács"}
Pyrrhosoma nymphula	{"vörös légivadász"}
Pyrrhosoma nymphula interposita	{"vörös légivadász"}
Pytho depressus	{"lapos sárkánybogár"}
Quadraspidiotus labiatarum	{kakukkfű-kagylóspajzstetű}
Quasimus minutissimus	{"parányi pattanó"}
Quedius balticus	{"mocsári mohaholyva"}
Quedius boops	{"apró mohaholyva"}
Quedius brevicornis	{"odúlakó mohaholyva"}
Quedius brevis	{"hangyakedvelő mohaholyva"}
Quedius cinctus	{"szegélyes mohaholyva"}
Quedius cruentus	{"gombakedvelő mohaholyva"}
Quedius curtipennis	{"füstös mohaholyva"}
Quedius fulgidus	{"fényes mohaholyva"}
Quedius fuliginosus	{"kormos mohaholyva"}
Quedius fumatus	{"hamvas mohaholyva"}
Quedius lateralis	{"sárgaszegélyes mohaholyva"}
Quedius levicollis	{"dunántúli mohaholyva"}
Quedius limbatus	{"közönséges mohaholyva"}
Quedius longicornis	{"recéshátú mohaholyva"}
Quedius lucidulus	{"zöldfényű mohaholyva"}
Quedius maurorufus	{"ligeti mohaholyva"}
Quedius maurus	{"szénfekete mohaholyva"}
Quedius meridiocarpathicus	{"alföldi mohaholyva"}
Quedius mesomelinus	{"üregi mohaholyva"}
Quedius microps	{"vaksi mohaholyva"}
Quedius molochinus	{"sokalakú mohaholyva"}
Quedius nemoralis	{"foltosszárnyú mohaholyva"}
Quedius nigriceps	{"homoki mohaholyva"}
Quedius nitipennis	{"kecses mohaholyva"}
Quedius ochripennis	{"vörösszárnyú mohaholyva"}
Quedius ochropterus	{"hegyi mohaholyva"}
Quedius picipes	{"avarlakó mohaholyva"}
Quedius riparius	{"felemás mohaholyva"}
Quedius scintillans	{"szivárványos mohaholyva"}
Quedius scitus	{"vöröses mohaholyva"}
Quedius semiobscurus	{"sárgalábú mohaholyva"}
Quedius skoraszewskyi	{"erdei mohaholyva"}
Quedius suturalis	{"forráslakó mohaholyva"}
Quedius truncicola	{"barnás mohaholyva"}
Quedius umbrinus	{"nagyszemű mohaholyva"}
Quedius xanthopus	{"kéreglakó mohaholyva"}
Rabdocerus foveolatus	{"gödrösnyakú álormányos"}
Rabdocerus gabrieli	{"északi álormányos"}
Rabigus tenuis	{"sárgahátú ganajholyva"}
Radix auricularia	{fülcsiga}
Rana kl. esculenta	{"kecskebéka komplex"}
Ranatra linearis	{"vízi botpoloska"}
Ranunculiphilus italicus	{"olasz ormányos"}
Rattus norvegicus	{"vándor patkány"}
Rattus rattus	{"házi patkány"}
Rebelia herrichiella	{"őszi zsákhordólepke"}
Rebelia sapho	{"selyemfényű zsákhordólepke"}
Rebelia surientella	{"tollszárnyú zsákhordólepke"}
Recurvaria leucatella	{nagy-vörös-rügysodrómoly}
Recurvaria nanella	{kis-vörös-rügysodrómoly}
Reduvius personatus	{"szemetes zugpoloska"}
Reesa vespulae	{szűzporva}
Reisserita relicinella	{"barna hulladékmoly"}
Reitterelater dubius	{"halvány pattanó"}
Reptalus cuspidatus	{"közönséges recéskabóca"}
Reptalus quinquecostatus	{"csíkoslábú recéskabóca"}
Retinia resinella	{"kormos gyantamoly"}
Rhabdiopteryx acuminata	{"dombvidéki álkérész"}
Rhabdiopteryx hamulata	{Mocsáry-álkérész}
Rhabdomiris striatellus	{zebra-mezeipoloska}
Rhagades pruni	{kökény-fémlepke}
Rhagonycha elongata	{"fekete lágybogár"}
Rhagonycha fulva	{"feketevégű lágybogár"}
Rhagonycha lignosa	{"cserjés lágybogár"}
Rhamphus oxyacanthae	{galagonya-bolhaormányos}
Rhamphus pulicarius	{szil-bolhaormányos}
Rhamphus subaeneus	{"fémfényű bolhaormányos"}
Rhaphigaster nebulosa	{bencepoloska}
Rhaphitropis marchica	{"szalagos orrosbogár"}
Rheumaptera hastata	{"dárdamintás araszoló"}
Rhigognostis hufnagelii	{"keresztes tarkamoly"}
Rhigognostis incarnatella	{"hagymarágó tarkamoly"}
Rhigognostis kovacsi	{"magyar tarkamoly"}
Rhigognostis senilella	{ikravirág-tarkamoly}
Rhinocyllus conicus	{"tömpeorrú barkó"}
Rhinolophus euryale	{"kereknyergű patkósdenevér"}
Rhinolophus ferrumequinum	{"nagy patkósdenevér"}
Rhinolophus hipposideros	{"kis patkósdenevér"}
Rhinomias austriacus	{"osztrák duzzadtorrú-ormányos"}
Rhinomias forticornis	{"tövises duzzadtorrú-ormányos"}
Rhinoncus bruchoides	{"mocsári keserűfűormányos"}
Rhinoncus castor	{"réti juhsóskaormányos"}
Rhinoncus inconspectus	{"nagy keserűfűormányos"}
Rhinoncus pericarpius	{"vöröslábú lórumormányos"}
Rhinoncus perpendicularis	{"foltos keserűfűormányos"}
Rhinusa antirrhini	{"fekete gyujtoványfű-ormányos"}
Rhinusa asellus	{"szőrcsillagos ormányos"}
Rhinusa bipustulata	{"vörösfoltos görvélyfű-ormányos"}
Rhinusa collinum	{"csinos gyujtoványfű-ormányos"}
Rhinusa herbarum	{"borzos gyujtoványfű-ormányos"}
Rhinusa hispidum	{"hosszúszőrű gyujtoványfű-ormányos"}
Rhinusa linariae	{"gubacsképző ormányos"}
Rhinusa littoreum	{"bundás gyujtoványfű-ormányos"}
Rhinusa melas	{"szürke gyujtoványfű-ormányos"}
Rhinusa tetra	{"magtoklakó ormányos"}
Rhinusa thapsicola	{"aprófogú gyujtoványfű-ormányos"}
Rhizaspidiotus canariensis	{üröm-pajzstetű}
Rhizedra lutosa	{óriás-nádibagoly}
Rhizopulvinaria artemisiae	{üröm-gyapjaspajzstetű}
Rhodeus amarus	{"szivárványos ökle"}
Rhodococcus perornatus	{rózsa-teknőspajzstetű}
Rhodococcus spiraeae	{gyöngyvessző-pajzstetű}
Rhodometra sacraria	{"keresztcsíkos kecsesaraszoló"}
Rhodostrophia vibicaria	{"pirosszélű araszoló"}
Rhopalapion longirostre	{mályvamag-cickányormányos}
Rhopalocerina clavigera	{"bunkóscsápú tarkaholyva"}
Rhopalocerus rondanii	{"zömök héjbogár"}
Rhopalodontus baudueri	{"kis sertéstaplószú"}
Rhopalodontus perforatus	{"nagy sertéstaplószú"}
Rhopalus maculatus	{"mocsári üvegszárnyú-poloska"}
Rhopalus parumpunctatus	{"közönséges üvegszárnyú-poloska"}
Rhopalus subrufus	{"vörös üvegszárnyú-poloska"}
Rhophitoides canus	{"fehércsíkos méh"}
Rhopobota myrtillana	{áfonyatükrösmoly}
Rhopobota naevana	{"márványos tükrösmoly"}
Rhopobota stagnana	{"horpadtsávú tükrösmoly"}
Rhyacia simulans	{"hamvas földibagoly"}
Rhyacionia buoliana	{fenyőilonca}
Rhyacionia duplana	{"sötét gyantamoly"}
Rhyacionia piniana	{"apró gyantamoly"}
Rhyacionia pinicolana	{"piros gyantamoly"}
Rhyacionia pinivorana	{"tarka gyantamoly"}
Rhynchaenus alni	{éger-bolhaormányos}
Rhynchaenus jota	{"kormos bolhaormányos"}
Rhynchaenus lonicerae	{lonc-bolhaormányos}
Rhynchaenus pilosus	{"bronzos bolhaormányos"}
Rhynchaenus rufus	{"vörös bolhaormányos"}
Rhynchaenus sparsus	{"foltos bolhaormányos"}
Rhynchites auratus	{"aranyos eszelény"}
Rhynchites bacchus	{almaeszelény}
Rhynchites giganteus	{körteeszelény}
Rhynchites lenaeus	{"déli eszelény"}
Rhyncolus ater	{"fekete szúormányos"}
Rhynocoris annulatus	{"gyűrűslábú üvegszárnyú poloska"}
Rhynocoris iracundus	{gyilkospoloska}
Rhyparia purpurata	{"bíborszínű medvelepke"}
Rhyparioides metelkana	{Metelka-medvelepke}
Rhyparochromus pini	{avar-díszesbodobács}
Rhyparochromus vulgaris	{"közönséges díszesbodobács"}
Rhyssemus germanus	{"barázdásnyakú trágyabogár"}
Rhyzopertha dominica	{gabona-csuklyásszú}
Riolus cupreus	{"rezes karmosbogár"}
Riolus subviolaceus	{"kékes karmosbogár"}
Rivula sericealis	{"selymes apróbagoly"}
Roeslerstammia erxlebella	{"zöldes bronzmoly"}
Roeslerstammia pronubella	{"barnaöves bronzmoly"}
Romanogobio kesslerii	{"homoki küllő"}
Romanogobio uranoscopus	{"felpillantó küllő"}
Romanogobio vladykovi	{"halványfoltú küllő"}
Rophites quinquespinosus	{"tüskésfejű méh"}
Rubiconia intermedia	{"bronzos címerespoloska"}
Rugilus angustatus	{"vöröshátú cérnanyakúholyva"}
Rugilus erichsonii	{"karcsú cérnanyakúholyva"}
Rugilus mixtus	{"hegyi cérnanyakúholyva"}
Rugilus orbiculatus	{"kis cérnanyakúholyva"}
Rugilus rufipes	{"nagy cérnanyakúholyva"}
Rugilus similis	{"sárgalábú cérnanyakúholyva"}
Rugilus subtilis	{"kecses cérnanyakúholyva"}
Runcinia grammica	{"peremesfejű karolópók"}
Rupicapra rupicapra	{"alpesi zerge"}
Rushia parreyssii	{"sárgalábú komorka"}
Rusina ferruginea	{rozsdabagoly}
Ruspolia nitidula	{"nagy kúpfejű szöcske"}
Ruteria hypocrita	{avarormányos}
Ruthenica filograna	{"karcsú orsócsiga"}
Rutilus rutilus	{bodorka}
Rutilus virgo	{leánykoncér}
Rypobius praetermissus	{"vörösnyakú pontbogár"}
Sabanejewia aurata	{törpecsík}
Sabanejewia aurata balcanica	{törpecsík}
Sabanejewia aurata bulgarica	{törpecsík}
Sabra harpagula	{hársfa-sarlósszövő}
Saga pedo	{"fűrészlábú szöcske"}
Saldula opacula	{"fekete partipoloska"}
Saldula pallipes	{"közönséges partipoloska"}
Saldula pilosella	{"szőrös partipoloska"}
Saldula saltatoria	{"parti futópoloska"}
Salebriopsis albicilla	{"fűzfonó karcsúmoly"}
Salmo trutta	{"sebes pisztráng"}
Salpingus aeneus	{"fémes álormányos"}
Salpingus planirostris	{"sötétnyakú álormányos"}
Salpingus ruficollis	{"vörösnyakú álormányos"}
Salticus cingulatus	{"foltos ugrópók"}
Salticus scenicus	{színészpók}
Salticus zebraneus	{"zebra ugrópók"}
Salvelinus fontinalis	{"pataki szaibling"}
Sander lucioperca	{süllő}
Sander volgensis	{kősüllő}
Saprinus aegialius	{"varratos fémsutabogár"}
Saprinus aeneus	{"tükrös fémsutabogár"}
Saprinus cribellatus	{"szitás fémsutabogár"}
Saprinus furvus	{"sötét fémsutabogár"}
Saprinus georgicus	{"mezei fémsutabogár"}
Saprinus immundus	{"szennyes fémsutabogár"}
Saprinus lautus	{"fényes fémsutabogár"}
Saprinus maculatus	{"foltos fémsutabog"}
Saprinus pharao	{fáraó-fémsutabogár}
Saprinus planiusculus	{"gyakori fémsutabogár"}
Saprinus quadristriatus	{"ráncos fémsutabogár"}
Saprinus semipunctatus	{"déli fémsutabogár"}
Saprinus semistriatus	{"közönséges fémsutabogár"}
Saprinus subnitescens	{"simamellű fémsutabogár"}
Saprinus tenuistrius sparsutus	{"vöröscsápú fémsutabogár"}
Saprinus vermiculatus	{"pontusi fémsutabogár"}
Saprinus virescens	{"zöld fémsutabogár"}
Sapyga clavicornis	{"bunkóscsápú áldarázs"}
Sapyga quinquepunctata	{"ötfoltos áldarázs"}
Saragossa porosa	{"sziki ürömbagoly"}
Satrapes sartorii	{"pikkelykés sutabogár"}
Satyrium acaciae	{"törpe farkincáslepke"}
Satyrium pruni	{szilvafa-csücsköslepke}
Satyrium spini	{kökény-csücsköslepke}
Sauterina hofmanniella	{"lednekaknázó hólyagosmoly"}
Scaphidema metallicum	{"fémzöld taplóbogár"}
Scaphidium quadrimaculatum	{"négyfoltos sajkabogár"}
Scaphisoma agaricinum	{"fekete sajkabogár"}
Scaphisoma boleti	{"barnás sajkabogár"}
Scaphium immaculatum	{"görbelábú sajkabogár"}
Scarabaeus pius	{"jámbor galacsinhajtó"}
Scarabaeus typhon	{óriás-galacsinhajtó}
Scardia boletella	{"korhadéklakó óriásmoly"}
Scardinius erythrophthalmus	{"vörösszárnyú keszeg"}
Sceliphron destillatorium	{"gyakori lopódarázs"}
Schiffermuelleria grandis	{"ékfoltos díszmoly"}
Schiffermuelleria schaefferella	{"ólomcsíkos díszmoly"}
Schistoglossa gemina	{"iszaplakó humuszholyva"}
Schizotus pectinicornis	{"kis bíborbogár"}
Schoenobius gigantella	{"óriás nádfúrómoly"}
Schrankia costaestrigalis	{"mocsári karcsúbagoly"}
Schrankia taenialis	{"törpe karcsúbagoly"}
Schreckensteinia festaliella	{"fényes szedermoly"}
Sciaphilus asperatus	{"árnyékkereső ormányos"}
Sciocoris cursitans	{"közönséges laposfejű-poloska"}
Sciocoris microphthalmus	{"szürkés laposfejű-poloska"}
Sciota adelphella	{"csíkos karcsúmoly"}
Sciota fumella	{"hideglápi karcsúmoly"}
Sciota hostilis	{"lápi karcsúmoly"}
Sciota rhenella	{"alföldi karcsúmoly"}
Scirpophaga praelata	{kócsagmoly}
Scirtes hemisphaericus	{"közönséges ugrórétbogár"}
Scirtes orbicularis	{"kerekded ugrórétbogár"}
Sclerocona acutella	{"hegyesszárnyú tűzmoly"}
Scobicia chevrieri	{"pilláshomlokú csuklyásszú"}
Scolia hirta	{"sötétszárnyú tőrösdarázs"}
Scoliopteryx libatrix	{"vörös csipkésbagoly"}
Scolitantides orion	{"szemes boglárka"}
Scolopendra cingulata	{"öves szkolopendra"}
Scolopostethus affinis	{"tarka csalánbodobács"}
Scolopostethus pictus	{csigabodobács}
Scolytus carpini	{gyertyánkéregszú}
Scolytus ensifer	{"tőrös szil-kéregszú"}
Scolytus intricatus	{tölgy-kéreg-szú}
Scolytus laevis	{"veszélyes kéregszú"}
Scolytus mali	{"nagy gyümölcsfaszú"}
Scolytus multistriatus	{"kis szil-szijácsszú"}
Scolytus pygmaeus	{szilfa-ágszú}
Scolytus ratzeburgii	{nyírfa-kéregszú}
Scolytus rugulosus	{"kis gyümölcsfaszú"}
Scolytus scolytus	{"nagy szilfa-kéregszú"}
Scopaeus bicolor	{"kétszínű turzásholyva"}
Scopaeus debilis	{"pusztai turzásholyva"}
Scopaeus laevigatus	{"közönséges turzásholyva"}
Scopaeus minimus	{"aprócska turzásholyva"}
Scopaeus minutus	{"apró turzásholyva"}
Scopaeus pusillus	{"kis turzásholyva"}
Scopaeus ryei	{"kicsiny turzásholyva"}
Scopaeus sulcicollis	{"hegyi turzásholyva"}
Scoparia ambigualis	{"barnás mohailonca"}
Scoparia ancipitella	{"apró mohailonca"}
Scoparia basistrigalis	{"tarka mohailonca"}
Scoparia conicella	{"szürkés mohailonca"}
Scoparia ingratella	{"keleti mohailonca"}
Scoparia pyralella	{"hamvas mohailonca"}
Scoparia subfusca	{"nagy mohailonca"}
Scopula caricaria	{"mocsári sávosarszoló"}
Scopula corrivalaria	{"lápi sávosaraszoló"}
Scopula decorata	{kakukkfű-sávosaraszoló}
Scopula flaccidaria	{"alföldi sávosaraszoló"}
Scopula immorata	{"réti sávosaraszoló"}
Scopula immutata	{"fehér sávosaraszoló"}
Scopula incanata	{"hamvas sávosaraszoló"}
Scopula marginepunctata	{"pettyes sávosaraszoló"}
Scopula nemoraria	{"ligeti sávosaraszoló"}
Scopula nigropunctata	{"feketepettyes sávosaraszoló"}
Scopula ornata	{"díszes sávosaraszoló"}
Scopula rubiginata	{"piros sávosaraszoló"}
Scopula virgulata	{"vesszős sávosaraszoló"}
Scotochrosta pulla	{"sötét őszibagoly"}
Scotophaeus quadripunctatus	{"négygödrű kövipók"}
Scotopteryx bipunctaria	{lóhere-vonalkásaraszoló}
Scotopteryx chenopodiata	{libatop-vonalkásaraszoló}
Scotopteryx mucronata	{"agyagszürke vonalkásaraszoló"}
Scraptia dubia	{"nagy cérnanyakúbogár"}
Scraptia fuscula	{"kis cérnanyakúbogár"}
Scrobipalpa acuminatella	{"aszatrágó sarlósmoly"}
Scrobipalpa artemisiella	{"rozsdacsíkos sarlósmoly"}
Scrobipalpa atriplicella	{"rozsdaszárnyú sarlósmoly"}
Scrobipalpa chrysanthemella	{margaréta-sarlósmoly}
Scrobipalpa erichi	{hajnalmoly}
Scrobipalpa gallicella	{"francia sarlósmoly"}
Scrobipalpa halonella	{fehérüröm-sarlósmoly}
Scrobipalpa hungariae	{"tihanyi sarlósmoly"}
Scrobipalpa nitentella	{"széki sarlósmoly"}
Scrobipalpa obsoletella	{"libatopfúró sarlósmoly"}
Scrobipalpa ocellatella	{"répaaknázó sarlósmoly"}
Scrobipalpa pauperella	{"egyszínű sarlósmoly"}
Scrobipalpa proclivella	{"parlagi sarlósmoly"}
Scrobipalpa reiprichi	{"szlovák sarlósmoly"}
Scrobipalpa salinella	{"vaksziki sarlósmoly"}
Scrobipalpa samadensis plantaginella	{szikhagyma-sarlósmoly}
Scrobipalpula psilella	{szalmagyopár-sarlósmoly}
Scrobipalpula tussilaginis	{martilapu-sarlósmoly}
Scymnus abietis	{lucfenyő-bödice}
Scymnus apetzi	{"feketefejű bödice"}
Scymnus ater	{"fekete bödice"}
Scymnus auritus	{"sárgafejű bödice"}
Scymnus doriai	{"széleslábú bödice"}
Scymnus femoralis	{"feketecombú bödice"}
Scymnus ferrugatus	{"csúcsfoltos bödice"}
Scymnus frontalis	{"közönséges bödice"}
Scymnus haemorrhoidalis	{"sárgavégű bödice"}
Scymnus interruptus	{"vállfoltos bödice"}
Scymnus limbatus	{"gesztenyebarna bödice"}
Scymnus marginalis	{"nagyfoltos bödice"}
Scymnus mediterraneus	{"mediterrán bödice"}
Scymnus mimulus	{"pontsoros bödice"}
Scymnus nigrinus	{fenyvesbödice}
Scymnus quadriguttatus	{"négycseppes bödice"}
Scymnus rubromaculatus	{"vörösfoltos bödice"}
Scymnus sacium	{"sziki bödice"}
Scymnus silesiacus	{"sárgás bödice"}
Scymnus subvillosus	{"négyfoltos bödice"}
Scymnus suturalis	{"varratsávos bödice"}
Scythia craniumequinum	{árvalányhaj-teknőspajzstetű}
Scythia festuceti	{csenkesz-teknőspajzstetű}
Scythris aerariella	{"fémfényű zöldmoly"}
Scythris bengtssoni	{sziklagyep-zöldmoly}
Scythris bifissella	{"sávos zöldmoly"}
Scythris crassiuscula	{"barnás zöldmoly"}
Scythris cuspidella	{"sárgamintás zöldmoly"}
Scythris emichi	{"magyar zöldmoly"}
Scythris fallacella	{"bronzos zöldmoly"}
Scythris flaviventrella	{"bükkönyfonó zöldmoly"}
Scythris fuscoaenea	{"napvirágszovő zöldmoly"}
Scythris gozmanyi	{Gozmány-zöldmolya}
Scythris hungaricella	{"pannon zöldmoly"}
Scythris laminella	{"mohafonó zöldmoly"}
Scythris limbella	{"parajfonó zöldmoly"}
Scythris obscurella	{"ércfényű zöldmoly"}
Scythris palustris	{"lápi zöldmoly"}
Scythris pascuella	{"réti zöldmoly"}
Scythris paullella	{"moharágó zöldmoly"}
Scythris picaepennis	{"tűszárnyú zöldmoly"}
Scythris podoliensis	{"lengyel zöldmoly"}
Scythris productella	{szurokfű-zöldmoly}
Scythris seliniella	{"kocsordfonó zöldmoly"}
Scythris subseliniella	{"sötétszínű zöldmoly"}
Scythris tabidella	{"lisztes zöldmoly"}
Scythris tributella	{"apró zöldmoly"}
Scythris vittella	{"fehércsíkos zöldmoly"}
Scythropia crataegella	{"pókhálós gyümölcsfamoly"}
Scytodes thoracica	{csupaszpók}
Sedina buettneri	{"lápi lándzsásbagoly"}
Segestria senoculata	{darócpók}
Segmentina nitida	{gombcsiga}
Sehirus luctuosus	{"világosfarú földipoloska"}
Sehirus morio	{"szurkos földipoloska"}
Selagia argyrella	{"ezüstös karcsúmoly"}
Selagia spadicella	{"kékfényű karcsúmoly"}
Selania leplastriana	{viola-tükrösmoly}
Selatosomus aeneus	{"fényes pattanó"}
Selatosomus cruciatus	{"keresztes pattanó"}
Selatosomus gravidus	{"széles pattanó"}
Selenia dentaria	{"kétfoltos holdasaraszoló"}
Selenia lunularia	{"egyfoltos holdasaraszoló"}
Selenia tetralunaria	{"négyfoltos holdasaraszoló"}
Selenodes karelica	{varfűmoly}
Selidosema brunnearia	{"fésűs faaraszoló"}
Selimus vittatus	{"sávos törpepók"}
Semiophonus signaticornis	{"szőrös lomhafutó"}
Semioscopis avellanella	{"mogyorószövő laposmoly"}
Semioscopis oculella	{"nyírszövő laposmoly"}
Semioscopis steinkellneriana	{"levélszövő laposmoly"}
Semioscopis strigulana	{"nyárfaszövő laposmoly"}
Senta flammea	{"lándzsás lápibagoly"}
Sepedophilus binotatus	{"fanedvkedvelő pihésholyva"}
Sepedophilus bipunctatus	{"soksertéjű pihésholyva"}
Sepedophilus bipustulatus	{"taplókedvelő pihésholyva"}
Sepedophilus immaculatus	{"kormos pihésholyva"}
Sepedophilus lividus	{"recés pihésholyva"}
Sepedophilus marshami	{"közönséges pihésholyva"}
Sepedophilus obtusus	{"fényes pihésholyva"}
Sepedophilus pedicularius	{"recés pihésholyva"}
Sepedophilus testaceus	{"szurokszínű pihésholyva"}
Serica brunnea	{"homoki kiscserebogár"}
Sericoderus lateralis	{"selymes pontbogár"}
Sericus brunneus	{"gesztenyebarna pattanó"}
Serratella ignita	{"vörösbarna kérész"}
Serratella mesoleuca	{"fehérfoltú kérész"}
Serropalpus barbatus	{"farontó komorka"}
Sesia apiformis	{darázslepke}
Sesia melanocephala	{rezgőnyár-szitkár}
Setina irrorella	{"nagy algaszövő"}
Setina roscida	{"sárga molyszövő"}
Shargacucullia gozmanyi	{Gozmány-csuklyásbagoly}
Shargacucullia lanceolata	{"fakó csuklyásbagoly"}
Shargacucullia lychnitis	{kakukkfű-sárgacsuklyásbagoly}
Shargacucullia prenanthis	{tavaszigörvélyfű-csuklyásbagoly}
Shargacucullia scrophulariae	{görvélyfű-sárgacsuklyásbagoly}
Shargacucullia verbasci	{"nagy sárgacsuklyásbagoly"}
Sibinia pellucens	{habszegfűormányos}
Sibinia phalerata	{madárhúrormányos}
Sibinia primita	{"bársonyfoltos ormányos"}
Sibinia pyrrhodactyla	{pimpóormányos}
Sicista subtilis	{"csíkos szöcskeegér"}
Sicista subtilis trizona	{"csíkos szöcskeegér"}
Sideridis lampra	{földitömjén-sziklabagoly}
Sideridis reticulata	{"fehéreres kertibagoly"}
Sideridis rivularis	{"patakparti szegfűbagoly"}
Sideridis turbida	{"szürke kertibagoly"}
Siederia listerella	{"fenyveslakó csövesmoly"}
Sigara falleni	{"csíkos búvárpoloska"}
Sigara lateralis	{"közönséges búvárpoloska"}
Sigara striata	{"rovátkolt búvárpoloska"}
Sigorus porcus	{"rozsdaszínű trágyabogár"}
Silis nitidula	{"kétalakú lágybogár"}
Silpha carinata	{"karimás dögbogár"}
Silpha obscura	{"közönséges dögbogár"}
Silurus glanis	{harcsa}
Silusa rubiginosa	{"pirók borzasholyva"}
Silvanoprus fagi	{"zömök fogasnyakú-lapbogár"}
Silvanus bidentatus	{"kétfogú fogasnyakú-lapbogár"}
Silvanus unidentatus	{"rozsdás fogasnyakú-lapbogár"}
Simitidion simile	{"hímes törpepók"}
Simplicia rectalis	{tölgy-karcsúbagoly}
Simplimorpha promissa	{cserszömörce-törpemoly}
Simplocaria semistriata	{"vonalkás labdacsbogár"}
Simyra albovenosa	{"halvány lápibagoly"}
Simyra nervosa	{"homoki lándzsásbagoly"}
Sinechostictus decoratus	{"alhavasi gyorsfutó"}
Sinechostictus doderoi	{"hegyi gyorsfutó"}
Sinechostictus elongatus	{"nyúlánk gyorsfutó"}
Sinechostictus ruficornis	{"fémkék gyorsfutó"}
Sinechostictus stomoides	{"nyurga gyorsfutó"}
Singa hamata	{"horgas keresztespók"}
Singa lucina	{"ékes keresztespók"}
Singa nitidula	{"fényes keresztespók"}
Sinodendron cylindricum	{"tülkös szarvasbogár"}
Sinoxylon perforans	{"nagytüskés csuklyásszú"}
Sinoxylon sexdentatum	{"hatfogú csuklyásszú"}
Siona lineata	{"vonalas fehéraraszoló"}
Siphlonurus armatus	{"tüskés kérész"}
Siphonoperla neglecta	{"sárga álkérész"}
Sisyphus schaefferi	{"lőcslábú galacsinhajtó"}
Sitaris muralis	{"pirosvállú méhbogár"}
Sitochroa palealis	{"kénszárnyú dudvamoly"}
Sitochroa verticalis	{"világossárga dudvamoly"}
Sitona ambiguus	{"csíkos csipkézőbogár"}
Sitona callosus	{"szempillás csipkézőbogár"}
Sitona cambricus	{"elegáns csipkézöbogár"}
Sitona cinerascens	{kerep-csipkézőbogár}
Sitona cylindricollis	{"hengerestorú csipkézőbogár"}
Sitona gressorius	{"pompás csipkézőbogár"}
Sitona griseus	{"szürke csipkézőbogár"}
Sitona hispidulus	{"szőrös csipkézőbogár"}
Sitona humeralis	{lucerna-csipkézőbogár}
Sitona inops	{"laposszemű csipkézőbogár"}
Sitona languidus	{koronafürt-csipkézőbogár}
Sitona lateralis	{iglice-csipkézőbogár}
Sitona lepidus	{"sárga csipkézőbogár"}
Sitona lineatus	{"sávos csipkézőbogár"}
Sitona longulus	{"hosszú csipkézőbogár"}
Sitona macularius	{borsó-csipkézőbogár}
Sitona puncticollis	{"nagy csipkézőbogár"}
Sitona striatellus	{bükköny-csipkézőbogár}
Sitona sulcifrons	{"szegélyes csipkézőbogár"}
Sitona suturalis	{"színes csipkézőbogár"}
Sitophilus granarius	{gabonazsuzsok}
Sitophilus oryzae	{rizszsuzsok}
Sitophilus zeamais	{kukoricazsuzsok}
Sitotroga cerealella	{"mezei gabonamoly"}
Sitticus pubescens	{"bronzos ugrópókbronzos ugrópók"}
Sitticus venator	{"kis hangyutánzópók"}
Smerinthus ocellatus	{"esti pávaszem"}
Smicromyrme halensis	{"hegyi pókhangya"}
Smicromyrme rufipes	{"piroslábú pókhangya"}
Smicronyx brevicornis	{"tömzsi arankaormányos"}
Smicronyx cyaneus	{szádorgó-bolhaormányos}
Smicronyx jungermanniae	{"fiatal arankaormányos"}
Smicronyx pygmaeus	{"egykarmú arankaormányos"}
Smicronyx reichii	{"szemölcsös arankaormányos"}
Smicronyx striatipennis	{"vöröslábú arankaormányos"}
Smicronyx swertiae	{gyásztárnics-ormányos}
Solenoxyphus fuscovenosus	{bárányparéj-poloska}
Somatochlora flavomaculata	{"sárgafoltos szitakötő"}
Somatochlora metallica	{"fémzöld szitakötő"}
Sophronia ascalis	{"turjáni sarlósmoly"}
Sophronia chilonella	{"ürömrágó sarlósmoly"}
Sophronia consanguinella	{mezeiüröm-sarlósmoly}
Sophronia humerella	{"kakukkfűszövő sarlósmoly"}
Sophronia illustrella	{"tarka sarlósmoly"}
Sophronia semicostella	{"rozsdaszürke sarlósmoly"}
Sophronia sicariellus	{"ürömfonó sarlósmoly"}
Sorex alpinus	{"havasi cickány"}
Sorex fodiens	{"erdei cickány"}
Sorhagenia janiszewskae	{"lengyel tündérmoly"}
Sorhagenia lophyrella	{"középeurópai tündérmoly"}
Sorhagenia rhamniella	{"bengefúró tündérmoly"}
Soronia grisea	{"pettyegetett fénybogár"}
Soronia punctatissima	{"hullámos fénybogár"}
Spaelotis ravida	{"cirmoshernyójú bagoly"}
Sparedrus testaceus	{"szőrös álcincér"}
Spargania luctuata	{"gajános levélaraszoló"}
Sparganothis pilleriana	{szőlőilonca}
Spatalia argentina	{"ezüstfoltos púposszövő"}
Spatalistis bifasciana	{"hegyi levélmoly"}
Spathocera lobata	{"homoki karimáspoloska"}
Specodes albilabris	{"nagy piroshasúméh"}
Specodes gibbus	{"gyakori piroshasúméh"}
Spelaeodiscus triarius	{"észak-kárpáti csiga"}
Sphaeriestes castaneus	{"barna álormányos"}
Sphaeriestes stockmanni	{"fekete álormányos"}
Sphaerium corneum	{gömbkagyló}
Sphaerium rivicola	{"nagy gömbkagyló"}
Sphaerolecanium prunastri	{szilvapajzstetű}
Sphaerosoma globosum	{"csupasz gömböcálböde"}
Sphaerosoma pilosum	{"szőrös gömböcálböde"}
Sphaerotachys hoemorrhoidalis	{"vörösvégű martfutó"}
Sphenophorus abbreviatus	{"óriás zsuzsok"}
Sphenophorus piceus	{"széki zsuzsok"}
Sphenophorus striatopunctatus	{"mezei zsuzsok"}
Sphenoptera antiqua	{"fényes gyalogdíszbogár"}
Sphenoptera basalis	{"sziki gyalogdíszbogár"}
Sphenoptera parvula	{"apró gyalogdíszbogár"}
Sphenoptera substriata	{"sávos gyalogdíszbogár"}
Sphindus dubius	{"közönséges áltaplószú"}
Sphindus grandis	{"nagy áltaplószú"}
Sphingonotus caerulans	{"homokszínű sáska"}
Sphinx ligustri	{fagyalszender}
Sphinx pinastri	{fenyőszender}
Sphodrus leucophthalmus	{"fekete pincefutó"}
Sphragisticus nebulosus	{"foltos bodobács"}
Sphyradium doliolum	{"tompavégű csiga"}
Spilomena troglodytes	{"tripszölő darázs"}
Spilonota laricana	{vörösfenyő-tükrösmoly}
Spilonota ocellana	{"szemes tükrösmoly"}
Spilosoma lubricipeda	{"tejszínű medvelepke"}
Spilosoma lutea	{"sárgás medvelepke"}
Spilosoma urticae	{"hószínű medvelepke"}
Spilosoma virginica	{"virginiai medvelepke"}
Spilostethus pandurus	{pandúrpoloska}
Spilostethus saxatilis	{virágbodobács}
Spinococcus callunetti	{csarab-pajzstetű}
Spinolia insignis	{"nagyszerű fémdarázs"}
Spuleria flavicaput	{"galagonyafúró lándzsásmoly"}
Spulerina simploniella	{tölgyhajtás-keskenymoly}
Squalius cephalus	{domolykó}
Squamapion atomarium	{kakukkfű-cickányormányos}
Squamapion cineraceum	{"csupaszorrú cickányormányos"}
Squamapion elongatum	{zsálya-cickányormányos}
Squamapion flavimanum	{menta-cickányormányos}
Squamapion oblivium	{"szerény cickányormányos"}
Squamapion vicinum	{"mentakedvelő cickányormányos"}
Stagetus pilula	{"gyapjasszőrű álszú"}
Stagmatophora heydeniella	{tisztesfű-tündérmoly}
Stagnicola palustris	{"mocsári csiga"}
Stagonomus pusillus	{"kétpettyes címerespoloska"}
Stangeia siceliota	{"mediterrán tollasmoly"}
Staphylinus caesareus	{"aranysujtásos holyva"}
Staphylinus dimidiaticornis	{"aranydíszes holyva"}
Staphylinus erythropterus	{"aranypajzsú holyva"}
Staria lunata	{"barna címerespoloska"}
Stathmopoda pedella	{égertermésmoly}
Staudingeria deserticola	{"sivatagi karcsúmoly"}
Stauroderus scalaris	{"hangos hegyisáska"}
Stauropus fagi	{bükkfa-púposszövő}
Steatoda albomaculata	{"koszorús törpepókkoszorús törpepók"}
Steatoda bipunctata	{"kétpettyes faggyúpók"}
Steatoda castanea	{"közönséges faggyúpók"}
Steatoda grossa	{"nagy faggyúpóknagy faggyúpók"}
Steatoda triangulosa	{"háromszöges faggyúpók"}
Stegania dilectaria	{fehérnyár-araszoló}
Stegobium paniceum	{kenyérbogár}
Steingelia gorodetskia	{nyírfa-pajzstetű}
Stelis phaeoptera	{"fekete méh"}
Stelis punctulatissima	{"sötét méh"}
Stemonyphantes lineatus	{"vonalas vitorlapók"}
Stenagostus rhombeus	{"rombusznyakú pattanó"}
Stenagostus rufus	{"nagy pattanó"}
Stenobothrus crassipes	{"szárnyatlan rétisáska"}
Stenobothrus eurasius	{"eurázsiai rétisáska"}
Stenobothrus Fischeri	{fischer-rétisáska}
Stenobothrus lineatus	{"jajgató rétisáska"}
Stenobothrus nigromaculatus	{"sztyeppréti sáska"}
Stenobothrus stigmaticus	{"jegyes rétisáska"}
Stenocarus cardui	{pipacsgyökér-ormányos}
Stenocarus ruficornis	{mákgyökér-ormányos}
Stenocranus minutus	{orsókabóca}
Stenodema calcarata	{"sarkantyús mezeipoloska"}
Stenodema laevigata	{"karcsú mezeipoloska"}
Stenolechia gemmella	{"tölgyfúró sarlósmoly"}
Stenolechiodes pseudogemmellus	{molyhostölgyes-sarlósmoly}
Stenolophus discophorus	{"kisfoltos turzásfutó"}
Stenolophus mixtus	{"fekete turzásfutó"}
Stenolophus persicus	{"sárgahasú turzásfutó"}
Stenolophus proximus	{"déli turzásfutó"}
Stenolophus skrimshiranus	{"sárga turzásfutó"}
Stenolophus steveni	{Steven-turzásfutó}
Stenolophus teutonus	{"nagyfoltos turzásfutó"}
Stenomax aeneus incurvus	{"bronzos gyászbogár"}
Stenopelmus rufinasus	{moszatpáfrány-ormányos}
Stenopterapion intermedium	{"hosszúszőrű cickányormányos"}
Stenopterapion meliloti	{somkoró-cickányormányos}
Stenopterapion tenue	{lucernaszár-cickányormányos}
Stenoptilia annadactyla	{"vértesi tollasmoly"}
Stenoptilia bipunctidactyla	{"kétpontú tollasmoly"}
Stenoptilia coprodactylus	{tárnicsvirág-tollasmoly}
Stenoptilia gratiolae	{csikorgófű-tollasmoly}
Stenoptilia pelidnodactyla	{kőtörőfű-tollasmoly}
Stenoptilia pneumonanthes	{tárnics-tollasmoly}
Stenoptilia pterodactyla	{"fahéjbarna tollasmoly"}
Stenoptilia stigmatodactylus	{ördögszem-tollasmoly}
Stenoptilia stigmatoides	{"kárpáti tollasmoly"}
Stenoptilia zophodactylus	{imolavirág-tollasmoly}
Stenoptinea cyaneimarmorella	{"tűszárnyú zuzmómoly"}
Stenoria apicalis	{"keskenyfedős élősdibogár"}
Stenotus binotatus	{"kétfoltos mezeipoloska"}
Stenus ampliventris	{"posványlakó szemesholyva"}
Stenus argus	{"békafejű szemesholyva"}
Stenus ater	{"fekete szemesholyva"}
Stenus aterrimus	{"hangyakedvelő szemesholyva"}
Stenus bifoveolatus	{"gödörkés szemesholyva"}
Stenus biguttatus	{"kétpettyes szemesholyva"}
Stenus bimaculatus	{"kétfoltos szemesholyva"}
Stenus binotatus	{"szélestalpú szemesholyva"}
Stenus boops	{"iszaplakó szemesholyva"}
Stenus brunnipes	{"barnáslábú szemesholyva"}
Stenus canaliculatus	{"barázdáshátú szemesholyva"}
Stenus carbonarius	{"kormos szemesholyva"}
Stenus cicindeloides	{"patakjáró szemesholyva"}
Stenus circularis	{"apró szemesholyva"}
Stenus clavicornis	{"réti szemesholyva"}
Stenus comma	{"közönséges szemesholyva"}
Stenus crassus	{"partlakó szemesholyva"}
Stenus europaeus	{"kis szemesholyva"}
Stenus excubitor	{"ligeti szemesholyva"}
Stenus exspectatus	{"síkföldi szemesholyva"}
Stenus flavipalpis	{"hegyi szemesholyva"}
Stenus flavipes	{"sárgalábú szemesholyva"}
Stenus formicetorum	{"kicsiny szemesholyva"}
Stenus fornicatus	{"kétéltű szemesholyva"}
Stenus fossulatus	{"aranyszőrű szemesholyva"}
Stenus fuscicornis	{"füstös szemesholyva"}
Stenus fuscipes	{"szurkoslábú szemesholyva"}
Stenus guttula	{"cseppfoltos szemesholyva"}
Stenus horioni	{"délvidéki szemesholyva"}
Stenus humilis	{"avarlakó szemesholyva"}
Stenus impressus	{"magashegyi szemesholyva"}
Stenus incrassatus	{"vaskos szemesholyva"}
Stenus intricatus zoufali	{"ráncos szemesholyva"}
Stenus juno	{"mocsári szemesholyva"}
Stenus kiesenwetteri	{"lápi szemesholyva"}
Stenus kolbei	{"berki szemesholyva"}
Stenus latifrons	{"széleshomlokú szemesholyva"}
Stenus longipes	{"nagyfoltos szemesholyva"}
Stenus longitarsis	{"hosszúlábú szemesholyva"}
Stenus ludyi	{"érces szemesholyva"}
Stenus lustrator	{"zománcfényű szemesholyva"}
Stenus maculiger	{"dunántúli szemesholyva"}
Stenus melanarius	{"tőzegkedvelő szemesholyva"}
Stenus melanopus	{"tőzegkedvelő szemesholyva"}
Stenus morio	{"ártéri szemesholyva"}
Stenus nanus	{"aprócska szemesholyva"}
Stenus nigritulus	{"feketés szemesholyva"}
Stenus nitens	{"fénylő szemesholyva"}
Stenus obscuripalpis	{"alföldi szemesholyva"}
Stenus ochropus	{"fényes szemesholyva"}
Stenus pallipes	{"mocsárjáró szemesholyva"}
Stenus pallitarsis	{"fehérkezű szemesholyva"}
Stenus palustris	{"sötétlábú szemesholyva"}
Stenus picipennis	{"szurkoshátú szemesholyva"}
Stenus providus	{"szőröshasú szemesholyva"}
Stenus pseudoboops	{"gyíkszerű szemesholyva"}
Stenus pubescens	{"hamuszínű szemesholyva"}
Stenus pusillus	{"törpe szemesholyva"}
Stenus scrutator	{"turjáni szemesholyva"}
Stenus similis	{"selymes szemesholyva"}
Stenus solutus	{"selyemfényű szemesholyva"}
Stenus stigmula	{"foltocskás szemesholyva"}
Stenus sylvester	{"erdei szemesholyva"}
Stenus vastus	{"tömzsi szemesholyva"}
Stephanitis pyri	{"recés körtepoloska"}
Stephensia brunnichella	{pereszlénymoly}
Stereocorynes truncorum	{"sötétbarna szúormányos"}
Stereonychus fraxini	{kőris-gömbormányos}
Sterrhopterix fusca	{"barna zsákhordólepke"}
Stethophyma grossum	{tundrasáska}
Stethorus punctillum	{"atkász bödice"}
Stichoglossa semirufa	{"fanedvkedvelő pudvaholyva"}
Stictopleurus abutilon	{"pettyeslábú üvegszárnyú-poloska"}
Stictopleurus crassicornis	{"foltoslábú üvegszárnyú-poloska"}
Stigmella aceris	{"juharaknázó törpemoly"}
Stigmella alnetella	{mézgáséger-törpemoly}
Stigmella anomalella	{vadrózsalevél-törpemoly}
Stigmella assimilella	{"nyáraknázó törpemoly"}
Stigmella atricapitella	{feketefű-törpemoly}
Stigmella aurella	{"aranyszárnyú törpemoly"}
Stigmella basiguttella	{"foltosövű törpemoly"}
Stigmella benanderella	{cinegefűz-törpemoly}
Stigmella betulicola	{"nyíraknázó törpemoly"}
Stigmella carpinella	{"gyertyánrágó törpemoly"}
Stigmella catharticella	{"bengeaknázó törpemoly"}
Stigmella centifoliella	{"rózsaaknázó törpemoly"}
Stigmella confusella	{"ködössávú törpemoly"}
Stigmella continuella	{nyírfalevél-törpemoly}
Stigmella crataegella	{"rézfényű törpemoly"}
Stigmella desperatella	{vadalma-törpemoly}
Stigmella dorsiguttella	{"foltos törpemoly"}
Stigmella eberhardi	{tölgylevél-törpemoly}
Stigmella filipendulae	{legyezőfű-törpemoly}
Stigmella floslactella	{gyertyánlevél-törpemoly}
Stigmella freyella	{"szulákrágó törpemoly"}
Stigmella glutinosae	{"rozsdásfejű törpemoly"}
Stigmella hahniella	{"patinás törpemoly"}
Stigmella hemargyrella	{"bükkaknázó törpemoly"}
Stigmella hybnerella	{galagonya-törpemoly}
Stigmella incognitella	{"almaaknázó törpemoly"}
Stigmella lemniscella	{tündértörpemoly}
Stigmella lonicerarum	{"loncaknázó törpemoly"}
Stigmella luteella	{"sárgasávú törpemoly"}
Stigmella magdalenae	{"galagonyaaknázó törpemoly"}
Stigmella malella	{almalevél-törpemoly}
Stigmella mespilicola	{"aranyzöld törpemoly"}
Stigmella microtheriella	{mogyorós-törpemoly}
Stigmella minusculella	{körtelevél-törpemoly}
Stigmella naturnella	{szőrösnyír-törpemoly}
Stigmella nivenburgensis	{fehérfűz-törpemoly}
Stigmella nylandriella	{berkenyelevél-törpemoly}
Stigmella obliquella	{fűzfalevél-törpemoly}
Stigmella oxyacanthella	{"ibolyavörös törpemoly"}
Stigmella paradoxa	{"ibolyatövű törpemoly"}
Stigmella perpygmaeella	{"galagonyarágó törpemoly"}
Stigmella plagicolella	{kökénylevél-törpemoly}
Stigmella poterii	{vérfűtörpemoly}
Stigmella prunetorum	{kökényes-törpemoly}
Stigmella pyri	{vadkörte-törpemoly}
Stigmella regiella	{"királyi törpemoly"}
Stigmella rhamnella	{varjútövis-törpemoly}
Stigmella roborella	{"vörösfejű törpemoly"}
Stigmella rolandi	{jajrózsalevél-törpemoly}
Stigmella ruficapitella	{"vöröses törpemoly"}
Stigmella sakhalinella	{kocsányostölgy-törpemoly}
Stigmella salicis	{"fűzaknázó törpemoly"}
Stigmella samiatella	{gesztenyelevél-törpemoly}
Stigmella sanguisorbae	{"vérfűrágó törpemoly"}
Stigmella speciosa	{hegyijuhar-törpemoly}
Stigmella splendidissimella	{"ékes törpemoly"}
Stigmella svenssoni	{mocsártölgy-törpemoly}
Stigmella szoecsiella	{"magyar törpemoly"}
Stigmella thuringiaca	{"türingiai törpemoly"}
Stigmella tiliae	{"hársaknázó törpemoly"}
Stigmella tityrella	{bükklevél-törpemoly}
Stigmella tormentillella	{"pimpóaknázó törpemoly"}
Stigmella trimaculella	{"hárompettyes törpemoly"}
Stigmella ulmiphaga	{"szilaknázó törpemoly"}
Stigmella ulmivora	{"ezüstsávos törpemoly"}
Stigmella viscerella	{szillevél-törpemoly}
Stigmella zangherii	{"rozsdásszárnyú törpemoly"}
Stigmus solskyi	{"nagyszárnyjegyű kaparódarázs"}
Stilbum cyanurum	{"nagy smaragdfémdarázs"}
Stilbus atomarius	{margaréta-kalászbogár}
Stilbus oblongus	{"hosszúkás kalászbogár"}
Stilbus pannonicus	{"pannon kalászbogár"}
Stilbus testaceus	{fészkes-kalászbogár}
Stizoides tridentatus	{"lilaszárnyú kabócarontó"}
Stizus perrisii	{"óriás kabócarontó"}
Stomis pumicatus	{"nyurgafejű futó"}
Stomodes gyrosicollis	{"gyökérlakó ormányos"}
Stomopteryx detersella	{"karszterdei övesmoly"}
Stomopteryx hungaricella	{"pannon övesmoly"}
Stomopteryx remissella	{"ibolyafényű övesmoly"}
Stricticomus tobias	{"fűzöttnyakú fürgebogár"}
Strongylocoris leucocephalus	{"sárgafejű ugrópoloska"}
Strophedra nitidana	{tölgylevél-tükrösmoly}
Strophedra weirana	{bükklevél-tükrösmoly}
Strophomorphus porcellus	{vadmalac-ormányos}
Strophosoma capitatum	{"jegynélküli ormányos"}
Strophosoma faber	{mester-ormányos}
Strophosoma melanogrammum	{"feketejegyes ormányos"}
Stygnocoris fuligineus	{"homoki kisbodobács"}
Subcoccinella vigintiquatuorpunctata	{lucernaböde}
Subrinus sturmi	{"vöröses trágyabogár"}
Succinea putris	{"nagy borostyánkőcsiga"}
Succinella oblonga	{"kis bostyánkőcsigaro"}
Sulcacis affinis	{"apró taplószú"}
Sulcacis bidentulus	{"kétfogú taplószú"}
Sulcacis fronticornis	{"szarvashomlokú taplószú"}
Sulcopolistes sulcifer	{"ál padlásdarázs"}
Sunius fallax	{"sötétfarú lombholyva"}
Sunius melanocephalus	{"feketefarú lombholyva"}
Sus scrofa	{vaddisznó}
Swammerdamia caesiella	{"nyírlevélfonó tarkamoly"}
Swammerdamia compunctella	{"berkenyefonó tarkamoly"}
Swammerdamia pyrella	{almalevél-tarkamoly}
Symbiotes gibberosus	{"rőt álböde"}
Symmorphus murarius	{"nádlakó kürtősdarázs"}
Sympecma fusca	{"erdei rabló"}
Sympetrum danae	{"fekete szitakötő"}
Sympetrum depressiusculum	{"lassú szitakötő"}
Sympetrum flaveolum	{"útszéli szitakötő"}
Sympetrum fonscolombii	{"atkás szitakötő"}
Sympetrum meridionale	{"sárgatorú szitakötő"}
Sympetrum pedemontanum	{"szalagos szitakötő"}
Sympetrum sanguineum	{"alföldi szitakötő"}
Sympetrum striolatum	{"gyakori szitakötő"}
Sympetrum vulgatum	{"közönséges szitakötő"}
Synanthedon andrenaeformis	{bangitaszitkár}
Synanthedon conopiformis	{tölgyfaszitkár}
Synanthedon culiciformis	{szúnyogszitkár}
Synanthedon formicaeformis	{hangyaszitkár}
Synanthedon loranthi	{fakínszitkár}
Synanthedon melliniformis	{"déli szitkár"}
Synanthedon mesiaeformis	{mézgáséger-szitkár}
Synanthedon myopaeformis	{almafaszitkár}
Synanthedon spheciformis	{égerfaszitkár}
Synanthedon spuleri	{Spuler-szitkára}
Synanthedon stomoxiformis	{naspolyaszitkár}
Synanthedon tipuliformis	{ribizkeszitkár}
Synanthedon vespiformis	{darázsszitkár}
Synaphe antennalis	{"nagy fényilonca"}
Synaphe bombycalis	{"sziki fényilonca"}
Synaphe moldavica	{"moldovai fényilonca"}
Synaphe punctalis	{"hosszúlábú fényilonca"}
Synapion ebeninum	{"lakkfényű cickányormányos"}
Synaptus filiformis	{"talpas pattanó"}
Synchita humeralis	{"vállfoltos héjbogár"}
Synchita mediolanensis	{"milánói héjbogár"}
Synchita separanda	{"barna héjbogár"}
Synchita undata	{"hullámos héjbogár"}
Synchita variegata	{"tarka héjbogár"}
Syncopacma albifrontella	{"fehérfejű övesmoly"}
Syncopacma captivella	{seprőzanót-övesmoly}
Syncopacma cinctella	{"ferdesávú övesmoly"}
Syncopacma cincticulella	{rekettye-övesmoly}
Syncopacma coronillella	{koronafürt-övesmoly}
Syncopacma linella	{"magyar övesmoly"}
Syncopacma ochrofasciella	{"sárgacsíkos övesmoly"}
Syncopacma patruella	{"sárgapettyes övesmoly"}
Syncopacma sangiella	{"kereprágó övesmoly"}
Syncopacma taeniolella	{"fonáksávú övesmoly"}
Syncopacma vinella	{"angol övesmoly"}
Syndemis musculana	{"füstös sodrómoly"}
Synema globosum	{"fekete-sárga karolópók"}
Synema ornatum	{"kutyatej karolópók"}
Syngrapha ain	{"havasi ezüstbagoly"}
Syngrapha interrogationis	{áfonya-ezüstbagoly}
Synopsia sociaria	{ürömaraszoló}
Syntomus foveatus	{"bronzszínű gyökérfutó"}
Syntomus obscuroguttatus	{"négyfoltos gyökérfutó"}
Syntomus pallipes	{"sárgalábú gyökérfutó"}
Syntomus truncatellus	{"fekete gyökérfutó"}
Synuchus vivalis	{"kerekhátú levélfutó"}
Syromastus rhombeus	{"vitorlás karimáspoloska"}
Systellonotus triguttatus	{"ezüstcsíkos mezeipoloska"}
Tabanus autumnalis	{"zömök bögöly"}
Tabanus bovinus	{marhabögöly}
Tabanus bromius	{lóbögöly}
Tabanus glaucopis	{"lilaszemű bögöly"}
Tabanus quatuornotatus	{"négysávos bögöly"}
Tabanus spodopterus	{"hegyi bögöly"}
Tabanus tergestinus	{"háromsávos bögöly"}
Tabanus unifasciatus	{"egysávos bögöly"}
Tachinus bipustulatus	{"fanedvkedvelő fürgeholyva"}
Tachinus corticinus	{"apró fürgeholyva"}
Tachinus fimetarius	{"karcsú fürgeholyva"}
Tachinus humeralis	{"erdei fürgeholyva"}
Tachinus laticollis	{"széleshátú fürgeholyva"}
Tachinus lignorum	{"réti fürgeholyva"}
Tachinus pallipes	{"szegélyeshátú fürgeholyva"}
Tachinus rufipennis	{"vörösszárnyú fürgeholyva"}
Tachinus scapularis	{"foltos fürgeholyva"}
Tachinus signatus	{"közönséges fürgeholyva"}
Tachinus subterraneus	{"komposztlakó fürgeholyva"}
Tachyerges decoratus	{"díszes bolhaormányos"}
Tachyerges pseudostigma	{"egyenesorrú bolhaormányos"}
Tachyerges rufitarsis	{"keskenyöves bolhaormányos"}
Tachyerges salicis	{fűz-bolhaormányos}
Tachyerges stigma	{"fehérjegyes bolhaormányos"}
Tachymarptis melba	{"havasi sarlósfecske"}
Tachyporus abdominalis	{"mocsári kószaholyva"}
Tachyporus atriceps	{"sötétfejű kószaholyva"}
Tachyporus caucasicus	{"foltoshátú kószaholyva"}
Tachyporus chrysomelinus	{"sárgahátú kószaholyva"}
Tachyporus dispar	{"sárgáshátú kószaholyva"}
Tachyporus formosus	{"sárgás kószaholyva"}
Tachyporus hypnorum	{"közönséges kószaholyva"}
Tachyporus nitidulus	{"kis kószaholyva"}
Tachyporus obtusus	{"szalagos kószaholyva"}
Tachyporus pallidus	{"halvány kószaholyva"}
Tachyporus pusillus	{"apró kószaholyva"}
Tachyporus ruficollis	{"busafejű kószaholyva"}
Tachyporus scitulus	{"hosszúszárnyú kószaholyva"}
Tachyporus solutus	{"vöröshátú kószaholyva"}
Tachysphex nitidus	{"fekete sáskaölő"}
Tachysphex obscuripennis	{panzer-sáskaölő}
Tachys scutellaris	{"sziki martfutó"}
Tachyta nana	{"apró kéregfutrinka"}
Tachytes etruscus	{"fekete sáskarontó"}
Tachytes europaeus	{"piros sáskarontó"}
Tachyura diabrachys	{"hatsávos martfutó"}
Tachyura parvula	{"apró martfutó"}
Tachyura quadrisignata	{"nyolcsávos martfutó"}
Tachyusa coarctata	{"karcsú cingárholyva"}
Tachyusa concinna	{"kecses cingárholyva"}
Tachyusa constricta	{"szúnyogképű cingárholyva"}
Tachyusa exarata	{"nyúlánk cingárholyva"}
Tachyusa objecta	{"szürke cingárholyva"}
Taeniapion rufulum	{"vörhenyes cickányormányos"}
Taeniapion urticarium	{csalán-cickányormányos}
Taeniopteryx schoenemundi	{Schönemund-álkérész}
Taleporia politella	{"sárga csövesmoly"}
Taleporia tubulosa	{"közönséges csövesmoly"}
Talis quercella	{"pannon fűgyökérmoly"}
Tanymecus dilaticollis	{kukoricabarkó}
Tanymecus palliatus	{"hegyesfarú barkó"}
Tanysphyrus ater	{"feketelábú békalencse-ormányos"}
Tanysphyrus lemnae	{békalencse-ormányos}
Tapeinotus sellatus	{lizinka-ormányos}
Taphrorychus bicolor	{"bóbitás bükkszú"}
Taphrotopium sulcifrons	{"bizarr J622cickányormányos"}
Tapinopa longidens	{"hosszúfogú vitorlapók"}
Targionia vitis	{"fekete tölgy-pajzstetű"}
Tarsostenus univittatus	{"karcsú szúfarkas"}
Tasgius ater	{"füstös holyva"}
Tasgius melanarius	{"kormos holyva"}
Tasgius morsitans	{"vöröslábú holyva"}
Tasgius pedator	{"kékes holyva"}
Tasgius winkleri	{"sarlós holyva"}
Tatianaerhynchites aequatus	{kökényeszelény}
Taxicera sericophila	{"selymes hegyiholyva"}
Tebenna bjerkandrella	{"fészkesviráglakó levélmoly"}
Tebenna micalis	{"déli levélmoly"}
Tecmerium perplexum	{"magyar avarevőmoly"}
Tegenaria agrestis	{"réti zugpók"}
Tegenaria domestica	{"házi zugpók"}
Telechrysis tripuncta	{"hárompettyes díszmoly"}
Teleiodes flavimaculella	{"sárgafoltos borzasmoly"}
Teleiodes luculella	{"u-betűs borzasmoly"}
Teleiodes saltuum	{"fenyőlakó borzasmoly"}
Teleiodes sequax	{napvirág-borzasmoly}
Teleiodes vulgella	{"galagonya borzasmoly"}
Teleiodes wagae	{"szürke borzasmoly"}
Teleiopsis diffinis	{juhsóska-sarlósmoly}
Telestes souffia	{"vaskos csabak"}
Temnocerus longiceps	{"szélesfejű eszelény"}
Temnocerus nanus	{törpeeszelény}
Temnocerus tomentosus	{"simaszőrű eszelény"}
Temnochila coerulea	{"kék szúvadász"}
Temnostethus pusillus	{"apró virágpoloska"}
Tenebrio molitor	{"közönséges lisztbogár"}
Tenebrio obscurus	{"kéreglakó lisztbogár"}
Tenebrio opacus	{"fogastorkú lisztbogár"}
Tenebroides fuscus	{"lisztmentő bogár"}
Tenebroides mauritanicus	{"lapos szúvadász"}
Tenuiphantes alacris	{"fürge vitorlapók"}
Tenuiphantes tenuis	{"avar vitorlapók"}
Tephronia sepiaria	{zuzmóaraszoló}
Teretrius fabricii	{"éji sutabogár"}
Tetartopeus quadratus	{"turjáni mocsárholyva"}
Tetartopeus rufonitidus	{"mocsári mocsárholyva"}
Tetartopeus scutellaris	{"réti mocsárholyva"}
Tetartopeus terminatus	{"lápi mocsárholyva"}
Tethea ocularis	{"pápaszemes pihésszövő"}
Tethea or	{"bélyeges pihésszövő"}
Tetrabrachys connatus	{"pusztai földiböde"}
Tetragnatha extensa pulchra	{"közönséges állaspók"}
Tetragnatha nigrita	{"feketés állaspók"}
Tetragnatha obtusa	{"zömök állaspók"}
Tetragnatha pinicola	{"fenyves állaspók"}
Tetragnatha reimoseri	{"farkos állaspók"}
Tetragnatha shoshone	{"rejtett állaspók"}
Tetragnatha striata	{"nádi állaspók"}
Tetralonia dentata	{zsályaméh}
Tetralonia hungarica	{"magyar méh"}
Tetralonia macroglossa	{mályvaméh}
Tetralonia pollinosa	{búzavirágméh}
Tetralonia salicariae	{füzényméh}
Tetralonia tricincta	{"háromöves méh"}
Tetratoma ancora	{"horgonyos álkomorka"}
Tetratoma desmarestii	{"szőrös álkomorka"}
Tetratoma fungorum	{"vöröskék álkomorka"}
Tetrix bipunctata	{"kétpettyes tövishátú sáska"}
Tetrix depressa	{"nyomott tövishátúsáska"}
Tetrix subulata	{"közönséges tövishátúsáska"}
Tetrix tenuicornis	{"vékonycsápú tövishátúsáska"}
Tetrix undulata	{"hullámos tövishátúsáska"}
Tetrops praeustus	{"négyszemű cincér"}
Tetrops starkii	{"feketeszélű aprócincér"}
Tettigometra concolor	{hangyakabóca}
Tettigonia cantans	{"éneklő lombszöcske"}
Tettigonia caudata	{"farkos lombszöcske"}
Tettigonia viridissima	{"zöld lombszöcske"}
Teuchestes fossor	{"nagy trágyabogár"}
Thalassophilus longicornis	{"hosszúcsápú fürgefutonc"}
Thalera fimbrialis	{"csipkésszélű zöldaraszoló"}
Thalycra fervida	{gomba-fénybogár}
Thambus frivaldskyi	{frivaldszky-tövisnyakúbogár}
Thamiaraea cinnamomea	{"fahéjszínű fanedvholyva"}
Thamiaraea hospita	{"dohányszínű fanedvholyva"}
Thamiocolus signatus	{"kis tisztesfűormányos"}
Thamnurgus kaltenbachii	{"gyapjas dudvaszú"}
Thamnurgus varipes	{kutyatejszú}
Thanasimus formicarius	{"vörösnyakú szúfarkas"}
Thanasimus rufipes	{"vöröslábú szúfarkas"}
Thanatophilus rugosus	{"ripacsos dögbogár"}
Thanatophilus sinuatus	{"hegyesvállú dögbogár"}
Thanatus formicinus	{"bársonysávos karolópók"}
Thaumetopoea processionea	{"búcsújáró lepke"}
Thecla betulae	{nyírfa-csücsköslepke}
Theodoxus danubialis	{"rajzos bödöncsiga"}
Theodoxus danubialis stragulatus	{rajzoscsiga}
Theodoxus fluviatilis	{"folyami bödöncsiga"}
Theodoxus prevostianus	{"fekete bödöncsiga"}
Theodoxus transversalis	{"sávos bödöncsiga"}
Thera juniperata	{"borókás tarkaaraszoló"}
Thera obeliscata	{fenyőtű-tarkaaraszoló}
Therapis flavicaria	{"foltosszélű araszoló"}
Theresimima ampellophaga	{kormospille}
Theria rupicapraria	{"tavaszi kökényaraszoló"}
Theridion pictum	{"tarka törpepók"}
Theridion pinastri	{"fenyő törpepók"}
Theridion varians	{"változó törpepókváltozó törpepók"}
Therioplectes gigas	{óriásbögöly}
Theromyzon tessulatum	{madárpióca}
Thetidia smaragdaria	{"fűzöld araszoló"}
Thinobius brevipennis	{"rövidszárnyú fövenyholyva"}
Thinobius ciliatus	{"apró fövenyholyva"}
Thinodromus arcuatus	{"hegyi iszapholyva"}
Thinodromus dilatatus	{"hosszúlábú iszapholyva"}
Thinodromus hirticollis	{"borzas iszapholyva"}
Thinonoma atra	{"selymes cingárholyva"}
Thiodia citrana	{"citromsárga tükrösmoly"}
Thiodia lerneana	{"piros tükrösmoly"}
Thiodia torridana	{"fehéröves tükrösmoly"}
Thiodia trochilana	{dárdahere-tükrösmoly}
Thiotricha subocellea	{szurokfű-sarlósmoly}
Thisanotia chrysonuchella	{"tavaszi fűgyökérmoly"}
Tholera cespitis	{"barna pázsitbagoly"}
Tholera decimalis	{"tarka pázsitbagoly"}
Thomisus onustus	{"fehér karolópókfehér karolópók"}
Thryogenes atrirostris	{"fiatal nádormányos"}
Thryogenes festucae	{nádormányos}
Thryogenes nereis	{nádtippan-ormányos}
Thryogenes scirrhosus	{"karcsú békabuzogány-ormányos"}
Thyatira batis	{"rózsafoltos pihésszövő"}
Thymallus thymallus	{"pénzes pér"}
Thymalus limbatus	{"bronfényű korongbogár"}
Thymelicus acteon	{"csíkos busalepke"}
Thymelicus lineola	{"vonalas busalepke"}
Thymelicus sylvestris	{"barna busalepke"}
Thyreocoris scarabaeoides	{"fémes földipoloska"}
Thyreus ramosus	{"foltos méh"}
Thyris fenestrella	{"gyakori ablakosmoly"}
Tibellus oblongus	{"sovány karolópók"}
Tibicina haematodes	{óriás-énekeskabóca}
Tiliacea aurago	{aranysárgabagoly}
Tiliacea citrago	{citromsárgabagoly}
Tiliacea sulphurago	{kénsárgabagoly}
Tilloidea unifasciata	{szőlő-szúfarkas}
Tillus elongatus	{"fekete szúfarkas"}
Tillus pallidipennis	{"sápadt szúfarkas"}
Timandra comae	{"piroscsíkos csipkésaraszoló"}
Tinagma balteolella	{"ólomszürke legyezősmoly"}
Tinagma ocnerostomella	{kígyószisz-legyezősmoly}
Tinagma perdicella	{földieper-legyezősmoly}
Tinca tinca	{compó}
Tinea columbariella	{"sárga fészekmoly"}
Tinea dubiella	{gyapjúmoly}
Tinea nonimella	{"keleti ablakosmoly"}
Tinea pallescentella	{"nagy szarumoly"}
Tinea pellionella	{szűcsmoly}
Tinea semifulvella	{"fényes hulladékmoly"}
Tinea translucens	{"homályos ablakosmoly"}
Tinea trinotella	{"hárompettyes fészekmoly"}
Tineola bisselliella	{ruhamoly}
Tingis auriculata	{"nyakas csipkéspoloska"}
Tingis cardui	{bogáncs-csipkéspoloska}
Tingis geniculata	{"közönséges csipkéspoloska"}
Tingis grisea	{búzavirág-csipkéspoloska}
Tingis maculata	{"homoki csipkéspoloska"}
Tingis pilosa	{"szőrös csipkéspoloska"}
Tingis reticulata	{"szőrösszegélyű csipkéspoloska"}
Tinotus morion	{"kormos fürkészholyva"}
Tinthia brosiformis	{kígyószisz-szitkár}
Tiphia femorata	{"piroslábú bogárrontó"}
Tiphia minuta	{"apró bogárrontó"}
Tiphia morio	{"fekete bogárrontó"}
Tischeria decidua	{"sötétsárga sörtésmoly"}
Tischeria dodonaea	{"sárga sörtésmoly"}
Tischeria ekebladella	{"tölgyaknázó sörtésmoly"}
Titanio normalis	{"szulákszövő kormosmoly"}
Titanoeca quadriguttata	{"négypettyes mészpók"}
Titanoeca schineri	{"kétpettyes mészpók"}
Titanoeca tristis	{"gyászos mészpók"}
Tituboea macropus	{dárdahere-zsákhordóbogár}
Tmarus piger	{"csúcsos karolópók"}
Tomicus minor	{"kis fenyőháncsszú"}
Tomicus piniperda	{fenyő-háncsszú}
Tomoxia bucephala	{kúpbogár}
Tortricodes alternella	{"tavaszi sodrómoly"}
Tortrix viridana	{tölgyilonca}
Tournotaris bimaculata	{"fűrészeslábú gyékényormányos"}
Tournotaris granulipennis	{gyékényormányos}
Trachea atriplicis	{"nyári zöldbagoly"}
Trachonitis cristella	{"bokorrágó karcsúmoly"}
Trachycera advenella	{"gerleszínű karcsúmoly"}
Trachycera dulcella	{"kökényszövő karcsúmoly"}
Trachycera legatea	{"bengerágó karcsúmoly"}
Trachycera marmorea	{"márványos karcsúmoly"}
Trachycera suavella	{"karszterdei karcsúmoly"}
Trachyphloeus alternans	{"bordás éjiormányos"}
Trachyphloeus angustisetulus	{"szélesorrú éjiormányos"}
Trachyphloeus aristatus	{"szálkás éjiormányos"}
Trachyphloeus asperatus	{"apró éjiormányos"}
Trachyphloeus bifoveolatus	{"laposszemű éjiormányos"}
Trachyphloeus inermis	{"védtelen éjiormányos"}
Trachyphloeus parallelus	{"pompás éjiormányos"}
Trachyphloeus scabriculus	{"bunkósszőrű éjiormányos"}
Trachyphloeus spinimanus	{"töviseslábú éjiormányos"}
Trachyphloeus ventricosus	{"tarka éjiormányos"}
Trachypteris picta	{"foltos fürgedíszbogár"}
Trachypteris picta decostigma	{"foltos fürgedíszbogár"}
Trachys coruscus	{mályva-vájárdíszbogár}
Trachys fragariae	{szamóca-vájárdíszbogár}
Trachys minutus	{fűz-vájárdíszbogár}
Trachys problematicus	{tisztesfű-vájárdíszbogár}
Trachys puncticollis	{szulák-vájárdíszbogár}
Trachys puncticollis rectilineatus	{szulák-vájárdíszbogár}
Trachys scrobiculatus	{menta-vájárdíszbogár}
Trachys troglodytes	{varfű-vájárdíszbogár}
Trachyzelotes pedestris	{"vöröslábú gyászpók"}
Trapezonotus arenarius	{ugarbodobács}
Traumoecia picipes	{"gödröshátú penészholyva"}
Trechoblemus micros	{"szőrös fürgefutonc"}
Trechus austriacus	{"szárnyatlan fürgefutonc"}
Trechus obtusus	{"zömök fürgefutonc"}
Trechus pilisensis	{"pilisi fürgefutonc"}
Trechus quadristriatus	{"közönséges fürgefutonc"}
Trepanes articulatus	{"barnafoltos gyorsfutó"}
Trepanes assimilis	{"kis gyorsfutó"}
Trepanes doris	{"mocsári gyorsfutó"}
Trepanes fumigatus	{"csíkos gyorsfutó"}
Trepanes gilvipes	{"ligeti gyorsfutó"}
Trepanes octomaculatus	{"nyolcfoltos gyorsfutó"}
Trepanes schueppelii	{"ércfekete gyorsfutó"}
Triaxomasia caprimulgella	{"kis gombamoly"}
Triaxomera fulvimitrella	{"vörösfejű gombamoly"}
Triaxomera parasitella	{"tarka gombamoly"}
Tribolium castaneum	{kukorica-kislisztbogár}
Tribolium confusum	{"közönséges kislisztbogár"}
Tribolium destructor	{"készletrontó kislisztbogár"}
Tribolium madens	{"apró kislisztbogár"}
Trichia hispida	{"pelyhes csigapelyhes csiga"}
Trichia lubomirskii	{Lubomirski-csiga}
Trichia striolata	{"nagy szőrőscsiga"}
Trichia striolata danubialis	{"nagy szőrőscsiga"}
Trichiura crataegi	{galagonyaszövő}
Trichius fasciatus	{"nyugati prémesbogár"}
Trichius sexualis	{"keleti prémesbogár"}
Trichodes apiarius	{"szalagos méhészbogár"}
Trichodes favarius	{"hatfoltos méhészbogár"}
Trichonotulus scrofa	{"szőrös trágyabogár"}
Trichophaga tapetzella	{takácsmoly}
Trichophya pilicornis	{"kis pilláscsápúholyva"}
Trichopterapion holosericeum	{gyertyánmag-cickányormányos}
Trichosirocalus troglodytes	{"lándzsás útilapu-ormányos"}
Trichotichnus laevicollis	{"kárpáti simafutó"}
Trichrysis cyanea	{"háromfogú fémdarázs"}
Trifurcula beirnei	{zanótrágó-törpemoly}
Trifurcula chamaecytisi	{zanóttörpemoly}
Trifurcula cryptella	{koronafürt-törpemoly}
Trifurcula eurema	{szarvaskerep-törpemoly}
Trifurcula headleyella	{"gyíkfűaknázó törpemoly"}
Trifurcula josefklimeschi	{Klimesch-törpemolya}
Trifurcula melanoptera	{"sötét törpemoly"}
Trifurcula ortneri	{"okkerszárnyú törpemoly"}
Trifurcula pallidella	{"mocsári törpemoly"}
Trifurcula thymi	{kakukkfű-törpemoly}
Trigonogenius globosus	{gömböc-tolvajbogár}
Trigonotylus ruficornis	{"vöröscsápú mezeipoloska"}
Trinodes hirtus	{"szőrös porva"}
Triodia sylvina	{"kis gyökérrágólepke"}
Trionymus aberrans	{pázsit-pajzstetű}
Trionymus multivorus	{lucerna-pajzstetű}
Trionymus newsteadi	{bükk-viaszospajzstetű}
Triphosa dubitata	{kutyabenge-araszoló}
Triphyllus bicolor	{"kétszínű gombabogár"}
Triplax aenea	{"fémes tarbogár"}
Triplax collaris	{"sárganyakú tarbogár"}
Triplax elongata	{"hosszúkás tarbogár"}
Triplax lacordairii	{"szegélyesnyakú tarbogár"}
Triplax lepida	{"csinos tarbogár"}
Triplax melanocephala	{"feketefejű tarbogár"}
Triplax pygmaea	{"kis tarbogár"}
Triplax rufipes	{"feketehasú tarbogár"}
Triplax russica	{"orosz tarbogár"}
Triplax scutellaris	{"vörösmellű tarbogár"}
Trisateles emortualis	{"fehérsávos karcsúbagoly"}
Tritoma bipustulata	{"feketenyakú tarbogár"}
Tritomegas bicolor	{"foltos földipoloska"}
Tritomegas sexmaculatus	{"hatfoltos földipoloska"}
Trixagus carinifrons	{"bordáshomlokú merevbogár"}
Trixagus dermestoides	{"selymes merevbogár"}
Trixagus duvalii	{"sötét merevbogár"}
Trixagus elateroides	{"kis merevbogár"}
Trixagus exul	{"apró merevbogár"}
Trocheta bykowskii	{"kétéltű pióca"}
Trochosa ruricola	{"mezei farkaspók"}
Trochosa terricola	{"földi farkaspók"}
Trogoderma glabrum	{"fekete gabonaporva"}
Trogoderma granarium	{kapra-gabonaporva}
Trogoderma megatomoides	{"háromszalagos gabonaporva"}
Trogoderma versicolor	{"tarka gabonaporva"}
Trogoxylon impressum	{"gödrös falisztbogár"}
Troilus luridus	{"tőrös címerespoloska"}
Tropideres albirostris	{"fehérfoltos orrosbogár"}
Tropideres dorsalis	{"foltos orrosbogár"}
Tropidothorax leucopterus	{vadpaprika-bodobács}
Tropinota hirta	{"bundás virágbogár"}
Tropiphorus micans	{medvehagyma-ormányos}
Tropistethus holosericus	{törpebodobács}
Trox cadaverinus	{"nagy irhabogár"}
Trox eversmanni	{"homoki irhabogár"}
Trox hispidus	{"gömböc irhabogár"}
Trox niger	{"gömböc irhabogár"}
Trox perrisii	{"fészeklakó irhabogár"}
Trox sabulosus	{"közönséges irhabogár"}
Trox scaber	{"rövidsörtés irhabogár"}
Truncatellina claustralis	{"homlokfogú csiga"}
Truncatellina cylindrica	{"hengeres csiga"}
Trypetimorpha fenestrata	{"foltosszárnyú kabóca"}
Trypocopris vernalis	{"tavaszi álganéjtúró"}
Trypodendron domesticum	{"nagy bükkszú"}
Trypodendron lineatum	{"sávos fenyőszú"}
Trypodendron signatum	{"lombfarágó szú"}
Trypophloeus asperatus	{rezgőnyárszú}
Trypophloeus granulatus	{fehérnyárszú}
Trypoxylon attenuatum	{"kétfogú fazekasdarázs"}
Trypoxylon figulus	{"nagy fazekasdarázs"}
Tychius aureolus	{"vörösbarna magormányos"}
Tychius crassirostris	{levél-gubacsormányos}
Tychius cuprifer	{"rezesbarna magormányos"}
Tychius flavus	{lucerna-magormányos}
Tychius junceus	{"őszi magormányos"}
Tychius medicaginis	{lucernamag-gubacsormányos}
Tychius meliloti	{somkóró-magormányos}
Tychius picirostris	{"szurkosorrú magormányos"}
Tychius squamulatus	{somkóró-ormányos}
Tychius stephensi	{szamóca-magormányos}
Typhaea stercorea	{"egyszínű gombabogár"}
Tyria jacobaeae	{jakabfű-lepke}
Tyta luctuosa	{"fekete nappalibagoly"}
Tytthaspis sedecimpunctata	{"tizenhatpettyes katica"}
Udea ferrugalis	{"rozsdabarna tűzmoly"}
Udea fulvalis	{"hullámos tűzmoly"}
Udea inquinatalis	{"hamuszürke tűzmoly"}
Udea lutealis	{"sárgás tűzmoly"}
Udea olivalis	{"olajszínű tűzmoly"}
Udea prunalis	{"hegyi tűzmoly"}
Uleiota planata	{"hosszúcsápú fogasnyakú-lapbogár"}
Uloborus walckenaerius	{"deres pók"}
Uloma culinaris	{"nagy rágványbogár"}
Uloma rufa	{"kis rágványbogár"}
Ulopa reticulata	{kígyósziszkabóca}
Ulorhinus bilineatus	{"dülledtszemű orrosbogár"}
Umbra krameri	{"lápi póc"}
Unaspis euonymi	{kecskerágó-kagylóspajzstetű}
Unio crassus	{"tompa folyamkagyló"}
Unio pictorum	{festőkagyló}
Unio tumidus	{folyamkagyló}
Uresiphita gilvata	{"feketeöves dudvamoly"}
Urophorus rubripennis	{"vörösszárnyú gyümölcs-fénybogár"}
Ursus arctos	{"barna medve"}
Urticicola umbrosus	{"hímzett csiga"}
Utetheisa pulchella	{"vérpettyes medvelepke"}
Valeria oleagina	{"tavaszi zöldbagoly"}
Valgus hemipterus	{"suta virágbogár"}
Vallonia costata	{"bordás csiga"}
Vallonia pulchella	{"sima csiga"}
Valvata piscinalis	{kerekszájúcsiga}
Vanessa cardui	{bogáncslepke}
Variimorda villosa	{"szalagos maróka"}
Velia caprai	{"díszes vízicsúszka"}
Velleius dilatatus	{"széles mohaholyva"}
Venusia blomeri	{szilfaaraszoló}
Vertigo angustior	{"harántfogú törpecsiga"}
Vertigo antivertigo	{hasascsiga}
Vertigo moulinsiana	{"hasas törpecsiga"}
Vertigo pusilla	{"hétfogú balogcsiga"}
Vespa crabro	{lódarázs}
Vespertilio murinus	{"fehértorkú denevér"}
Vespula austriaca	{"osztrák darázs"}
Vespula germanica	{"német darázs"}
Vespula rufa	{"pirosfoltos darázs"}
Vespula vulgaris	{"közönséges darázs"}
Vestia gulo	{"sudár orsócsiga"}
Vestia turgida	{"dagadt orsócsiga"}
Vibidia duodecimguttata	{"tizenkétcseppes füsskata"}
Vimba vimba	{"szilvaorrú keszeg"}
Vincenzellus ruficollis	{"kék álormányos"}
Vitrea crystallina	{kristálycsiga}
Vitrea diaphana	{gyöngycsiga}
Vitrina pellucida	{üvegcsiga}
Vitula biviella	{fenyővirág-karcsúmoly}
Viviparus acerosus	{"folyami fiallócsiga"}
Viviparus contectus	{fiallócsiga}
Volinus sticticus	{"cirmos trágyabogár"}
Vulcaniella extremella	{zsálya-tündérmoly}
Vulcaniella pomposella	{szalmagyopár-tündérmoly}
Vulpes vulpes	{"vörös róka"}
Walckenaeria acuminata	{"toronyfejű pók"}
Walckenaeria capito	{"kettősfejű pók"}
Walckenaeria furcillata	{"villásfejű pók"}
Watsonalla binaria	{tölgyfa-sarlósszövő}
Watsonalla cultraria	{bükkfa-sarlósszövő}
Watsonarctia casta	{"pusztai medvelepke"}
Wheeleria obsoletus	{pemetefű-tollasmoly}
Whittleia undulella	{"rácsos zsákhordólepke"}
Witlesia pallida	{"mocsári mohailonca"}
Wockia asperipunctella	{fehérnyármoly}
Xanthia gilvago	{szilfa-sárgabagoly}
Xanthia icteritia	{"nyárfa sárgabagoly"}
Xanthia ocellaris	{"szemfoltos sárgabagoly"}
Xanthia togata	{"fűzfa sárgabagoly"}
Xanthochilus quadratus	{"szegélyes díszesbodobács"}
Xanthocrambus lucellus	{"zegzugos fűgyökérmoly"}
Xanthocrambus saxonellus	{"sárga fűgyökérmoly"}
Xantholinus azuganus	{"kárpáti rovátkásholyva"}
Xantholinus coiffaiti	{"pusztai rovátkásholyva"}
Xantholinus decorus	{"díszes rovátkásholyva"}
Xantholinus dvoraki	{"közönséges rovátkásholyva"}
Xantholinus kaszabi	{"zempléni rovátkásholyva"}
Xantholinus laevigatus	{"dunántúli rovátkásholyva"}
Xantholinus linearis	{"bronzfényű rovátkásholyva"}
Xantholinus longiventris	{"fényeshátú rovátkásholyva"}
Xantholinus tricolor	{"háromszínű rovátkásholyva"}
Xanthorhoe biriviata	{"kétsávos tarkaaraszoló"}
Xanthorhoe designata	{keresztesvirág-levélaraszoló}
Xanthorhoe ferrugata	{"kerti tarkaaraszoló"}
Xanthorhoe fluctuata	{"közönséges tarkaaraszoló"}
Xanthorhoe montanata	{"hegyi tarkaaraszoló"}
Xanthorhoe quadrifasiata	{"négysávos tarkaaraszoló"}
Xenostrongylus arcuatus	{"szürkésfehér fénybogár"}
Xenota myrmecobia	{"érdes penészholyva"}
Xerocnephasia rigana	{kökörcsinmoly}
Xerolenta obvia	{kórócsiga}
Xestia baja	{"rózsás földibagoly"}
Xestia castanea	{"csarabos fésűsbagoly"}
Xestia c-nigrum	{"c-betűs földibagoly"}
Xestia ditrapezium	{"kéttrapézos földibagoly"}
Xestia stigmatica	{"rombuszos földibagoly"}
Xestia triangulum	{"háromszöges földibagoly"}
Xestia xanthographa	{"rozsdabarna földibagoly"}
Xestobium plumbeum	{"fémes álszú"}
Xestobium rufovillosum	{"nagy álszú"}
Xya pfaendleri	{pfaendler-ásósáska}
Xya variegata	{"közönséges ásósáska"}
Xyleborinus saxesenii	{vadgesztenyeszú}
Xyleborus cryptographus	{nyárszú}
Xyleborus dispar	{púposszú}
Xyleborus dryographus	{"szarvas cserszú"}
Xyleborus monographus	{"szarvas tölgyszú"}
Xylena exsoleta	{"szürke nagyfabagoly"}
Xylena vetusta	{"vörhenyes nagyfabagoly"}
Xyletinus ater	{"bordásmellű szerecsenálszú"}
Xyletinus distinguendus	{"ligeti szerecsenálszú"}
Xyletinus fibyensis	{"északi szerecsenálszú"}
Xyletinus laticollis	{"szélesnyakú szerecsenálszú"}
Xyletinus longitarsis	{"hosszúlábú szerecsenálszú"}
Xyletinus moraviensis	{"pirosvégű szerecsenálszú"}
Xyletinus pectinatus	{"fésűs szerecsenálszú"}
Xyletinus pseudooblongulus	{"félkörös szerecsenálszú"}
Xyletinus subrotundatus	{"sörtésnyakú szerecsenálszú"}
Xyletinus vaederoeensis	{"svéd szerecsenálszú"}
Xylita laevigata	{"selymes komorka"}
Xylita livida	{"varratsávos komorka"}
Xylocleptes bispinus	{iszalagszú}
Xylococcus filiferus	{hárs-ágpajzstetű}
Xylocopa valga	{"gyakori fadongó"}
Xylocopa violacea	{"kék fadongó"}
Xylocoris galactinus	{"melegágyi poloska"}
Xylodromus affinis	{"pusztai felemásholyva"}
Xylodromus concinnus	{"füstös felemásholyva"}
Xylodromus depressus	{"penészkedvelő felemásholyva"}
Xylodromus testaceus	{"vöröses felemásholyva"}
Xylographus bostrichoides	{"fűrészeslábú taplószú"}
Xylopertha retusa	{"fekete csuklyásszú"}
Xylophilus testaceus	{"hengeres tövisnyakúbogár"}
Xylostiba bosnica	{"bükkös kéregholyva"}
Xylotrechus arvicola	{gazdászcincér}
Xysticus cristatus	{"tarajos karolópók"}
Xysticus kochi	{"közönséges karolópók"}
Xysticus robustus	{"vaskos karolópók"}
Xysticus striatipes	{"csíkoslábú karolópók"}
Xysticus ulmi	{"cserje karolópók"}
Xystophora carchariella	{"bükkönyrágó sarlósmoly"}
Xystophora pulveratella	{"kereprágó sarlósmoly"}
Yponomeuta cagnagella	{"pókhálós kecskerágómoly"}
Yponomeuta evonymella	{"pókhálós májusfamoly"}
Yponomeuta irrorella	{"pókhálós kökénymoly"}
Yponomeuta malinellus	{"pókhálós almamoly"}
Yponomeuta padella	{"pókhálós szilvamoly"}
Yponomeuta plumbella	{"pókhálós bengemoly"}
Yponomeuta rorrella	{"pókhálós fűzmoly"}
Yponomeuta sedella	{"húszpettyes pókhálósmoly"}
Ypsolopha alpella	{"okkersárga tarkamoly"}
Ypsolopha asperella	{"levélfonó tarkamoly"}
Ypsolopha chazariella	{"juharfonó tarkamoly"}
Ypsolopha dentella	{"fahéjszínű loncmoly"}
Ypsolopha falcella	{"sárgaráncú loncmoly"}
Ypsolopha horridella	{"kormos tarkamoly"}
Ypsolopha lucella	{"tölgyfonó tarkamoly"}
Ypsolopha mucronella	{kecskerágómoly}
Ypsolopha parenthesella	{"gyertyánfonó tarkamoly"}
Ypsolopha persicella	{őszibarack-tarkamoly}
Ypsolopha scabrella	{körtelevél-tarkamoly}
Ypsolopha sequella	{"ligeti tarkamoly"}
Ypsolopha sylvella	{"erdei tarkamoly"}
Ypsolopha ustella	{"csíkos tarkamoly"}
Ypsolopha vittella	{"füstös tarkamoly"}
Zabrus spinipes	{"zömök futrinka"}
Zabrus tenebrioides	{gabonafutrinka}
Zacladus asperatus	{"galléros gólyaorr-ormányos"}
Zacladus exiguus	{"ráspolyos gólyaorr-ormányos"}
Zacladus geranii	{gólyaorr-ormányos}
Zanclognatha lunalis	{"pelyheslábú karcsúbagoly"}
Zanclognatha tarsipennalis	{"tollaslábú karcsúbagoly"}
Zanclognatha zelleralis	{"fogassávú karcsúbagoly "}
Zebrina detrita	{zebracsiga}
Zeiraphera griseana	{"fenyőtűszövő tükrösmoly"}
Zeiraphera isertana	{"tölgysodró tükrösmoly"}
Zeiraphera rufimitrana	{fenyőrügy-tükrösmoly}
Zelotes electus	{"vörösfejű gyászpók"}
Zelotes erebeus	{"vöröslő gyászpók"}
Zelotes subterraneus	{"földi gyászpók"}
Zeuzera pyrina	{almafarontólepke}
Zicrona caerulea	{"acélkék poloska"}
Zilora obscura	{"sötét komorka"}
Zingel streber	{"német bucó"}
Zingel zingel	{"magyar bucó"}
Zonitis immaculata	{"sárga élősdobogár"}
Zonitis nana	{"déli élősdibogár"}
Zonitis praeusta	{"rőt élősdibogár"}
Zora manicata	{"egyszerű párducpók"}
Zora nemoralis	{"ligeti párducpók"}
Zora pardalis	{párducpók}
Zora silvestris	{"erdei párducpók"}
Zora spinimana	{"tüskéskezű párducpók"}
Zorochros demustoides	{"fekete fövenypattanó"}
Zorochros meridionalis	{"szemecskés fövenypattanó"}
Zorochros quadriguttatus	{"négycseppes fövenypattanó"}
Zygaena angelicae	{"vérpettyes csüngőlepke"}
Zygaena brizae	{"magyar csüngőlepke"}
Zygaena carniolica	{"fehérgyűrűs csüngőlepke"}
Zygaena cynarae	{"pusztai csüngőlepke"}
Zygaena ephialtes	{"változékony csüngőlepke"}
Zygaena filipendulae	{"acélszínű csüngőlepke"}
Zygaena lonicerae	{lonc-csüngőlepke}
Zygaena loti	{"közönséges csüngőlepke"}
Zygaena minos	{"levantei csüngőlepke"}
Zygaena osterodensis	{ördögszem-csüngőlepke}
Zygaena punctum	{"pettyes csüngőlepke"}
Zygaena purpuralis	{"bíborszínű csüngőlepke"}
Zygaena viciae	{somkóró-csüngőlepke}
Zygina flammigera	{vérkabóca}
Zygina nivea	{tölgyfakabóca}
Zyras cognatus	{"bóbitás hangyászholyva"}
Zyras collaris	{"vöröshátú hangyászholyva"}
Zyras fulgidus	{"fényes hangyászholyva"}
Zyras funestus	{"füstös hangyászholyva"}
Zyras hampei	{"fahéjszínű hangyászholyva"}
Zyras haworthi	{"termetes hangyászholyva"}
Zyras humeralis	{"érdes hangyászholyva"}
Zyras laticollis	{"barna hangyászholyva"}
Zyras limbatus	{"szegélyes hangyászholyva"}
Zyras lugens	{"fakó hangyászholyva"}
Zyras ruficollis	{"ékes hangyászholyva"}
Zyras similis	{"kis hangyászholyva"}
Acer pseudo-platanus	{"hegyi juhar"}
Achillea distans subsp. stricta	{"kárpáti cickafark"}
Achillea horanszkyi	{Horánszky-cickafark}
Achillea tuzsonii	{Tuzson-cickafark}
Aconitum moldavicum	{"kárpáti sisakvirág"}
Aconitum variegatum subsp. gracilis	{"karcsú sisakvirág"}
Aconitum vulparia	{"farkasölő sisakvirág"}
Acorellus pannonicus	{"magyar palka"}
Adonis × hybrida	{"erdélyi hérics"}
Aethusa cynapium subsp. agrestis	{ádáz}
Aethusa cynapium subsp. cynapioides	{ádáz}
Ageratina altissima	{"fehér kígyógyökér"}
Agropyron caninum	{"szálkás tarackbúza"}
Agropyron elongatum	{"magas tarackbúza"}
Agropyron elongatum cv. szarvasi-1	{energiafű}
Agropyron intermedium	{"deres tarackbúza"}
Agropyron intermedium subsp. trichophorum	{"deres tarackbúza"}
Agropyron repens	{"közönséges tarackbúza"}
Alchemilla acutiloba	{"hegyeskaréjú palástfű"}
Alchemilla gracilis	{"kecses palástfű"}
Allium montanum	{"hegyi hagyma"}
Allium paniculatum subsp. marginatum	{"bugás hagyma"}
Allium rotundum subsp. waldsteinii	{"ereszes hagyma"}
Allium sphaerocephalon subsp. amethystinum	{"bunkós hagyma"}
Aloina bifrons	{"felállótokú aloémoha"}
Althaea officinalis subsp. pseudoarmeniaca	{"ártéri ziliz"}
Alyssum saxatile	{"szirti sziklaiternye"}
Alyssum tortuosum subsp. tortuosum	{"homoki ternye"}
Amaranthus chlorostachys	{"karcsú disznóparéj"}
Amaranthus graecizans subsp. sylvestris	{"cigány disznóparéj"}
Amaranthus lividus	{"zöld disznóparéj"}
Amaranthus paniculatus	{"bíbor disznóparéj"}
Amaranthus patulus	{"terpedt disznóparéj"}
Ambrosia artemisifolia	{"ürömlevelű parlagfű"}
Amygdalus communis	{"közönséges mandula"}
Amygdalus nana	{"törpe mandula"}
Anacamptodon splachnoides	{"ernyőmohaszerű görbefogúmoha"}
Anchusa ochroleuca subsp. pustulata	{"vajszínű atracél"}
Anethum graveolens var. hortorum	{"kerti kapor"}
Anomodon rostratus	{"csőrös farkaslábmoha"}
Anthemis ruthenicus	{"homoki pipitér"}
Anthriscus cerefolium subsp. trichosperma	{"zamatos turbolya"}
Anthyllis vulneraria subsp. polyphylla	{"magyar nyúlszapuka"}
Aquilegia vulgaris subsp. nigricans	{"feketéllő harangláb"}
Arabis hirsuta subsp. gerardii	{"berki ikravirág"}
Arabis hirsuta subsp. sagittata	{"nyilas ikravirág"}
Arctium nemorosum subsp. pubens	{"erdei bojtorján"}
Arenaria procera subsp. glabra	{"hegyi homokhúr"}
Armoracia lapathifolia	{"közönséges torma"}
Artemisia campestris subsp. lednicensis	{"ezüstös üröm"}
Artemisia × gayeriana	{Gáyer-üröm}
Arum orientale subsp. bessarabicum	{"keleti kontyvirág"}
Aruncus sylvestris	{"erdei tündérfürt"}
Asperula cynanchica subsp. montana	{"török müge"}
Asplenium × alternifolium	{"hibrid fodorka"}
Asplenium × murbeckii	{"hibrid fodorka"}
Asplenium trichomanes subsp. quadrivalens	{"mészkedvelő aranyos fodorka"}
× Asplenoceterach badense	{"hibrid fodorka"}
Aster × lanceolatus	{"lándzsáslevelű őszirózsa"}
Aster × salignus	{"ligeti őszirózsa"}
Aster tradescantii	{"kisvirágú őszirózsa"}
Aster × versicolor	{"tarka őszirózsa"}
Atriplex acuminata	{"fényes laboda"}
Atropa bella-donna	{nadragulya}
Avena sterilis subsp. ludoviciana	{"magas zab"}
Azolla caroliniana	{"mexikói moszatpáfrány"}
Barbarea verna	{"tavaszi borbálafű"}
Batrachium aquatile	{"nagy víziboglárka"}
Batrachium baudotii	{"hosszúkocsányú víziboglárka"}
Batrachium circinatum	{"merev víziboglárka"}
Batrachium fluitans	{"úszó víziboglárka"}
Batrachium peltatum	{"pajzsos víziboglárka"}
Batrachium radians	{"sugaras víziboglárka"}
Batrachium rhipiphyllum	{"pajzsos víziboglárka"}
Batrachium rionii	{"kopasz víziboglárka"}
Batrachium trichophyllum	{"hínáros víziboglárka"}
Bidens cernua	{"bókoló farkasfog"}
Bidens frondosa	{"feketéllő farkasfog"}
Bidens tripartita	{"subás farkasfog"}
Biscutella laevigata subsp. kerneri	{"Kerner korongpár"}
Bolboschoenus glaucus	{"vörös zsióka"}
Bolboschoenus laticarpus	{"szélesmakkú zsióka"}
Bolboschoenus planiculmis	{"vájtmakkú zsióka"}
Brachydontium trichoides	{"szőrszerű pintycsőrűmoha"}
Brachythecium geheebii	{Geheeb-pintycsőrűmoha}
Brachythecium oxycladum	{"hegyeságú pintycsőrűmoha"}
Brassica elongata subsp. armoracioides	{"éplevelű harasztos káposzta"}
Brassica × juncea	{"szareptai mustár"}
Brassica × napus	{olajrepce}
Brassica rapa subsp. sylvestris	{"vad réparepce"}
Bromus brachystachys	{rozsnokfaj}
Bromus carinatus	{"ormós rozsnok"}
Bromus lanceolatus	{"lándzsás rozsnok"}
Bromus mollis	{"puha rozsnok"}
Bromus willdenowii	{Willdenow-rozsnok}
Broussonetia papyrifera	{"kínai papíreper"}
Brunnera macrophylla	{"nagylevelű kaukázusi-nefelejcs"}
Bryum neodamense	{"kanalaslevelű körtemoha"}
Bryum stirtonii	{"keskenyfogú körtemoha"}
Bryum versicolor	{"tarka körtemoha"}
Bryum warneum	{"ostoros körtemoha"}
Bupleurum falcatum subsp. dilatatum	{"sarlós buvákfű"}
Bupleurum pachnospermum	{"deres buvákfű"}
Buxbaumia viridis	{"zöld koboldmoha"}
Calamintha einseleana	{"lajtai pereszlény"}
Calamintha menthifolia subsp. sylvatica	{"erdei pereszlény"}
Calamintha thymifolia	{szirtipereszlény}
Calliergon giganteum	{óriásmoha}
Calliergon stramineum	{"gyökerecskés-levélcsúcsú moha"}
Caltha palustris subsp. cornuta	{"szarvacskás gólyahír"}
Caltha palustris subsp. laeta	{"hegyi gólyahír"}
Campanula patula subsp. neglecta	{"terebélyes harangvirág"}
Campanula sibirica subsp. divergentiformis	{"nagyvirágú pongyola harangvirág"}
Campylium elodes	{"mocsári aranymoha"}
Campylostelium saxicola	{"sziklai görbeszárúmoha"}
Cannabis sativa	{kender}
Cardamine occulta	{"lappangó kakukktorma"}
Cardamine pratensis subsp. dentata	{"fogas kakukktorma"}
Cardamine pratensis subsp. paludosus	{"mocsári kakukktorma"}
Cardaminopsis arenosa subsp. petrogena	{"közönséges kövifoszlár"}
Cardaminopsis halleri	{Haller-kövifoszlár}
Carduus × budaianus	{Budai-bogáncs}
Carduus × littoralis	{"tengerparti bogáncs"}
Carduus nutans subsp. macrolepis	{"bókoló bogáncs"}
Carduus × orthocephalus	{bogáncshibrid}
Carduus × solteszii	{Soltész-bogáncs}
Carex caespitosa	{"gyepes sás"}
Carex cuprina	{"berki sás"}
Carex curvata	{"görbeszárú sás"}
Carex demissa	{"nyugati sás"}
Carex gracilis	{"éles sás"}
Carex gracilis subsp. intermedia	{"éles sás"}
Carex hallerana	{"sziklai sás"}
Carex hartmannii	{"északi sás"}
Carex leporina	{nyúlsás}
Carex liparicarpos	{"fényes sás"}
Carex pairae	{"berzedt sás"}
Carex pairae subsp. leersiana	{"tölgyes sás"}
Carex transsylvanica	{"kárpáti sás"}
Carlina vulgaris subsp. intermedia	{"rövidgalléros bábakalács"}
Carlina vulgaris subsp. longifolia	{"magas bábakalács"}
Caucalis latifolia	{"nagy ördögbocskor"}
Centaurea arenaria	{"homoki imola"}
Centaurea arenaria subsp. tauscheri	{"homoki imola"}
Centaurea banatica	{"bánsági imola"}
Centaurea fritschii	{"dunántúli imola"}
Centaurea jacea subsp. subjacea	{"réti imola"}
Centaurea micranthos	{"útszéli imola"}
Centaurea rhenana	{"nyugati imola"}
Centaurea salonitana var. taurica	{"dalmát imola"}
Centaurium erythraea subsp. austriacum	{"osztrák ezerjófű"}
Centaurium erythraea subsp. turcicum	{"kis ezerjófű"}
Centaurium littorale subsp. uliginosum	{"lápi ezerjófű"}
Cephalanthera × schulzei	{"fehér és kardos madársisak hibridje"}
Cephalaria pilosa	{"erdei fejvirág"}
Cerastium arvense subsp. calcicola	{"mátrai parlagi madárhúr"}
Cerastium brachypetalum subsp. tenoreanum	{Tenore-madárhúr}
Cerastium fontanum	{"réti madárhúr"}
Cerastium fontanum subsp. macrocarpum	{"nagytokú madárhúr"}
Cerastium pumilum subsp. pallens	{"fakó madárhúr"}
Cerasus avium	{madárcseresznye}
Cerasus fruticosa	{csepleszmeggy}
Cerasus mahaleb	{sajmeggy}
Cerasus × mohacsyana	{"hibrid cseresznye"}
Cerasus vulgaris	{" meggy"}
Ceratocephalus falcata	{"sima sarlóboglárka"}
Ceratocephalus testiculatus	{"tarajos sarlóboglárka"}
Ceratoides latens	{pamacslaboda}
Ceterach javorkaeanum	{"magyar pikkelypáfrány"}
Ceterach officinarum	{"nyugati pikkelypáfrány"}
Chaenorhinum minus	{"kicsiny tátos"}
Chamaecytisus supinus subsp. aggregatus	{"gombos zanót"}
Chamaecytisus supinus subsp. pseudorochelii	{Rochel-zanót}
Chamaecytisus virescens	{"zöldellő zanót"}
Chamaespartium sagittale	{"közönséges szányasrekettye"}
Cheilanthes marantae	{cselling}
Chenopodium botryoides	{"sziki libatop"}
Chenopodium schraderanum	{"nehézszagú libatop"}
Chlorocyperus glaber	{"kopasz palka"}
Chlorocyperus glomeratus	{"csomós palka"}
Chlorocyperus longus	{"hosszú palka"}
Chrysanthemum corymbosum	{"sátoros margitvirág"}
Chrysanthemum lanceolatum	{"erdei margitvirág"}
Chrysanthemum leucanthemum	{"réti margitvirág"}
Chrysanthemum leucanthemum subsp. sylvestre	{"borzas margitvirág"}
Chrysanthemum parthenium	{"őszi margitvirág"}
Chrysanthemum serotinum	{"tiszaparti margitvirág"}
Circaea × intermedia	{"havasalji varázslófű"}
Cirsium boujarti	{"pécsvidéki aszat"}
Cirsium × candolleanum	{Candolle-aszat}
Cirsium × hybridum	{aszathibrid}
Cirsium × linkianum	{"hibrid aszat"}
Cirsium × silesiacum	{"sziléziai aszat"}
Cirsium × tataricum	{"tatár aszat"}
Cladium mariscus subsp. martii	{"déli télisás"}
Corispermum leptopterum	{"keskenyszárnyú poloskamag"}
Corispermum marschallii	{Marschall-poloskamag}
Coronilla elegans	{"nagylevelű koronafürt"}
Coronilla emerus	{"bokros koronafürt"}
Coronilla varia	{"tarka koronafürt"}
Corydalis × campylochila	{"hibrid keltike"}
Corydalis intermedia	{"bókoló keltike"}
Corydalis pumila	{"törpe keltike"}
Corydalis solida subsp. slivenensis	{"hármaslevelű ujjas keltike"}
Crataegus curvisepala	{"hosszúcsészés galagonya"}
Crataegus curvisepala subsp. calycina	{"hosszúcsészés galagonya"}
Crataegus monogyna subsp. nordica	{"északi egybibés galagonya"}
Crataegus oxyacantha	{cseregalagonya}
Cuscuta australis	{"nádfojtó aranka"}
Cuscuta australis subsp. cesatiana	{"nádfojtó aranka"}
Cuscuta australis subsp. tinei	{"nádfojtó aranka"}
Cuscuta trifolii	{"herefojtó aranka"}
Dactylorhiza × aschersoniana	{Ascherson-ujjaskosbor}
Dactylorhiza fuchsii subsp. sooiana	{Soó-ujjaskosbor}
Dactylorhiza incarnata subsp. haematodes	{"hússzínű ujjaskosbor"}
Dactylorhiza incarnata subsp. serotina	{"kisvirágú hússzínű ujjaskosbor"}
Dactylorhiza incarnata var. hyphaematodes	{"foltoslevelű hússzínű ujjaskosbor"}
Dactylorhiza incarnata var. ochrantha	{"sárgás hússzínű ujjaskosbor"}
Dactylorhiza maculata subsp. transsylvanica	{"erdélyi ujjaskosbor"}
Daphne cneorum subsp. arbusculoides	{"nyugati henye boroszlán"}
Datura wrightii	{Wright-maszlag}
Dentaria bulbifera	{"hagymás fogasír"}
Dentaria enneaphyllos	{"bókoló fogasír"}
Dentaria glandulosa	{"ikrás fogasír"}
Dentaria trifolia	{"hármaslevelű fogasír"}
Deschampsia caespitosa	{"gyepes sédbúza"}
Deschampsia caespitosa subsp. parviflora	{"egyvirágú gyepes sédbúza"}
Desmatodon cernuus	{"bókoló trágyamoha"}
Dianthus carthusianorum subsp. saxigenus	{"kisvirágú barátszegfű"}
Dianthus × hellwigii	{hibridszegfű}
Dianthus plumarius subsp. lumnitzeri	{Lumnitzer-szegfű}
Dianthus plumarius subsp. praecox	{"korai fehérszegfű"}
Dianthus plumarius subsp. regis-stephani	{"Szent István király-szegfű"}
Dianthus pontederae	{"magyar szegfű"}
Dichostylis micheliana	{iszapkáka}
Dicranella humilis	{"kis seprőcskemoha"}
Dicranum viride	{"zöld seprőmoha"}
Didymodon glaucus	{"szürke ikresmoha"}
Digitaria ciliaris	{"pillás ujjasmuhar"}
Diphasium complanatum	{"közönséges laposkorpafű"}
Diphasium issleri	{Issler-laposkorpafű}
Diphasium tristachyum	{"tölcséres laposkorpafű"}
Dipsacus × pseudosylvester	{"hibrid mácsonya"}
Dipsacus sylvestris	{"erdei mácsonya"}
Doronicum × sopianae	{"mecseki zergevirág"}
Dracocephalum moldavicum	{"moldvai sárkányfű"}
Drepanocladus exannulatus	{"gyűrűtlen sarlósmoha"}
Drepanocladus lycopodioides	{"korpafűszerű sarlósmoha"}
Drepanocladus revolvens	{"körkörös sarlósmoha"}
Drepanocladus sendtneri	{Sendtner-sarlósmoha}
Drepanocladus vernicosus	{"karcsú sarlósmoha"}
Dryopteris pseudo-mas	{"pelyvás pajzsika"}
Dryopteris × sarvelii	{"szálkás és hegyi pajzsika hibridje"}
Dryopteris × tavelii	{pajzsikahibrid}
Duchesnea indica	{"indiai díszeper"}
Echinochloa eruciformis	{"kakaslábfű faj"}
Echinochloa oryzoides	{"nagyszemű kakaslábfű"}
Echinops ruthenicus	{"kék szamárkenyér"}
Echium russicum	{"piros kígyószisz"}
Eleocharis quinqueflora	{"gyérvirágú csetkáka"}
Elodea densa	{"hévízi átokhínár"}
Elodea nuttallii	{"aprólevelű átokhínár"}
Enthostodon hungaricus	{"sóspusztai magyarmoha"}
Ephemerum cohaerens	{"csoportos paránymoha"}
Ephemerum recurvifolium	{"sarlóslevelű paránymoha"}
Epilobium adenocaulon	{"jövevény füzike"}
Epilobium tetragonum subsp. lamyi	{"Lamy füzike"}
Epipactis atrorubens subsp. borbasii	{"homoki vörösbarna nőszőfű"}
Epipactis futakii	{Futák-nőszőfű}
Epipactis × graberi	{Graber-nőszőfű}
Epipactis helleborine agg.	{"széleslevelű nőszőfű"}
Epipactis helleborine subsp. minor	{"kis széleslevelű nőszőfű"}
Epipactis helleborine subsp. orbicularis	{"rövidlevelű nőszőfű"}
Epipactis leptochila subsp. neglecta	{"keskenyajkú nőszőfű"}
Epipactis muelleri	{Müller-nőszőfű}
Epipactis nordeniorum	{Norden-nőszőfű}
Epipactis placentina	{"ciklámenlila nőszőfű"}
Epipactis purpurata var. rosea	{"ibolyás nőszőfű"}
Epipactis tallosii	{Tallós-nőszőfű}
Epipactis voethii	{Vöth-nőszőfű}
Equisetum × moorei	{Moore-zsurló}
Eragrostis megastachya	{"nagy tőtippan"}
Eragrostis mexicana	{"mexikói tőtippan"}
Eragrostis multicaulis	{"csalóka tőtippan"}
Erigeron acris	{"bóbitás küllőrojt"}
Erigeron canadensis	{betyárkóró}
Erodium neilreichii	{"homoki gémorr"}
Erophila praecox	{"korai ködvirág"}
Erophila spathulata	{"kerek ködvirág"}
Erysimum pallidiflorum	{"halványsárga repcsény"}
Euonymus europaea	{"csíkos kecskerágó"}
Euphorbia dulcis subsp. incompta	{"édes kutyatej"}
Euphorbia esula subsp. pinifolia	{sárkutyatej}
Euphorbia falcata subsp. acuminata	{"hegyes tarlókutyatej"}
Euphorbia humifusa	{"kúszó kutyatej"}
Euphorbia maculata	{"foltos kutyatej"}
Euphorbia nutans	{"bókoló kutyatej"}
Euphorbia pannonica	{"magyar kutyatej"}
Euphorbia platyphyllos subsp. literata	{"nagylevelű kutyatej"}
Euphorbia prostrata	{"lecsepült kutyatej"}
Euphorbia seguierana	{"pusztai kutyatej"}
Euphrasia kerneri	{"réti szemvidító"}
Euphrasia tatarica	{"tatár szemvidító"}
Ferula sadleriana	{"magyarföldi husáng"}
Festuca arundinacea subsp. uechtritziana	{"nádképű csenkesz"}
Festuca javorkae	{Jávorka-csenkesz}
Festuca pallens subsp. pannonica	{"pannon deres csenkesz"}
Festuca tenuifolia	{"fonalas csenkesz"}
Festuca vaginata subsp. dominii	{"szálkahegyű magyar csenkesz"}
Festuca vojtkoi	{Vojtkó-csenkesz}
Festuca × wagneri	{"rákosi csenkesz"}
Ficaria verna	{salátaboglárka}
Ficaria verna subsp. bulbifera	{"hagymás salátaboglárka"}
Ficaria verna subsp. calthifolia	{salátaboglárka}
Filago arvensis	{"gyapjas penészvirág"}
Filago minima	{"hegyi penészvirág"}
Filago vulgaris subsp. lutescens	{"sárgászöld penészvirág"}
Filipendula ulmaria subsp. denudata	{"zöldfonákú réti legyezőfű"}
Fissidens arnoldii	{Arnoldi-hasadtfogúmoha}
Fissidens exiguus	{"kis hasadtfogúmoha"}
Foeniculum vulgare	{"kerti édeskömény"}
Fraxinus angustifolia subsp. pannonica	{"magyar kőris"}
Frullania inflata	{"hólyagos kerekesféreglakta-májmoha"}
Gagea szovitsii	{"pusztai tyúktaréj"}
Gagea villosa	{"vetési tyúktaréj"}
Gaillardia pulchella	{"kétszínű kokárdavirág"}
Galeobdolon luteum subsp. montanum	{"hegyi sárgaárvacsalán"}
Galeopsis ladanum subsp. angustifolia	{"keskenylevelű kenderkefű"}
Galinsoga ciliata	{"borzas gombvirág"}
Galium album subsp. picnotrichum	{"fehér galaj"}
Galium boreale subsp. pseudorubioides	{"északi galaj"}
Galium parisiense subsp. anglicum	{"párizsi galaj"}
Galium verum subsp. wirtgenii	{"tejoltó galaj"}
Gentiana cruciata	{"Szent László-tárnics"}
Gentianella ciliata	{"prémes tárnicska"}
Gentianella livonica	{"csinos tárnicska"}
Geranium molle subsp. stripulare	{"puha gólyaorr"}
Glyceria × pedicellata	{"hibrid harmatkása"}
Glyceria plicata	{"fodros harmatkása"}
Gnaphalium luteo-album	{"halvány gyopár"}
Gnaphalium sylvaticum	{"erdei gyopár"}
Gnaphalium uliginosum	{iszapgyopár}
Grimmia sessitana	{"pirostokfedős párnácskamoha"}
Gymnadenia conopsea subsp. densiflora	{"tömöttvirágú szúnyoglábú bibircsvirág"}
Gypsophila perfoliata	{"átnőttlevelű fátyolvirág"}
Haynaldia villosa	{Haynald-fű}
Heleochloa alopecuroides	{"karcsú bajuszfű"}
Heleochloa schoenoides	{"vastag bajuszfű"}
Helictotrichon compressum	{"tömött zabfű"}
Helictotrichon praeustum	{"lapos zabfű"}
Helictotrichon pratense	{"réti zabfű"}
Helictotrichon pubescens	{"pelyhes zabfű"}
Heliopsis helianthoides	{"érdes napszemvirág"}
Helminthia echioides	{vándorvirág}
Hemerocallis lilio-asphodelus	{"sárga sásliliom"}
Heracleum sphondylium subsp. chlorathum	{"zöldes medvetalp"}
Hesperis matronalis subsp. vrabelyiana	{Vrabélyi-estike}
Hieracium × arvicola	{hibridhölgymál}
Hieracium × bifurcum	{"villás hölgymál"}
Hieracium × brachiatum	{"kétágú hölgymál"}
Hieracium bupleuroides subsp. tatrae	{"tátrai hölgymál"}
Hieracium × calodon	{hibridhölgymál}
Hieracium × densiflorum	{"tömöttvirágú hölgymál"}
Hieracium × diaphanoides	{hibridhölgymál}
Hieracium × euchaetium	{hibridhölgymál}
Hieracium × fallacinum	{hibridhölgymál}
Hieracium × fallax	{hibridhölgymál}
Hieracium × flagellare	{hibridhölgymál}
Hieracium × florentinoides	{hibridhölgymál}
Hieracium × floribundum	{"virágosindájú hölgymál"}
Hieracium × glaucinum	{"korai hölgymál"}
Hieracium × koernickeanum	{hibridhölgymál}
Hieracium lachenalii	{"közönséges hölgymál"}
Hieracium × laevicaule	{"simaszárú hölgymál"}
Hieracium × laschii	{"szürke hölgymál"}
Hieracium × latifolium	{"rövidlevelű hölgymál"}
Hieracium × laurinum	{hibridhölgymál}
Hieracium × leptophyton	{"laza hölgymál"}
Hieracium × piloselliflorum	{hölgymálhibrid}
Hieracium × polymastix	{hibridhölgymál}
Hieracium × praecurrens	{"bozontos hölgymál"}
Hieracium × ramosum	{"ágas hölgymál"}
Hieracium rothianum	{"sertés hölgymál"}
Hieracium × saxifraga	{"kőtörő hölgymál"}
Hieracium × schultesii	{"mirigyes hölgymál"}
Hieracium × sciadophorum	{hibridhölgymál}
Hieracium staticifolium	{"keskenylevelű hölgymál"}
Hieracium × sulphureum	{"sárgás hölgymál"}
Hieracium sylvaticum	{"erdei hölgymál"}
Hieracium × viridifolium	{"zöldlevelű hölgymál"}
Hieracium × wiesbaurianum	{"bérci hölgymál"}
Hieracium × zizianum	{hibridhölgymál}
Hierochloe australis	{"déli szentperje"}
Hierochloe repens	{"illatos szentperje"}
Hilpertia velenovskyi	{Velenovsky-löszmoha}
Holoschoenus romanus subsp. holoschoenus	{szürkekáka}
Hordeum hystrix	{"sziki árpa"}
Hypericum × desetangsii	{"hibrid orbáncfű"}
Hypericum maculatum subsp. obtusiusculum	{"kétes orbáncfű"}
Hypericum mutilum	{"csonka orbáncfű"}
Hypochoeris maculata	{"foltos véreslapu"}
Hypochoeris radicata	{"kacúros véreslapu"}
Impatiens balfouri	{Matild-nebáncsvirág}
Juncus alpinus	{"havasi szittyó"}
Juncus bufonius subsp. nastanthus	{varangyszittyó}
Juncus gerardii	{"sziki szittyó"}
Jungermannia subulata	{Jungermann-moha}
Jurinea mollis subsp. macrocalathia	{"nagyfészkű hangyabogáncs"}
Kickxia × confinus	{"hibrid tátika"}
Knautia × ramosissima	{"hibrid varfű"}
Kochia laniflora	{"homoki seprűfű"}
Kochia prostrata	{"heverő seprűfű"}
Kochia scoparia	{"vesszős seprűfű"}
Koeleria glauca subsp. rochelii	{Rochel-fényperje}
Koeleria grandis	{"nagy fényperje"}
Koeleria javorkae	{Jávorka-fényperje}
Lactuca quercina subsp. sagittata	{"fogaslevelű saláta"}
Lathyrus linifolius var. montanus	{"hegyi lednek"}
Lathyrus transsilvanicus	{"erdélyi lednek"}
Lavathera trimestris	{"kerti madármályva"}
Lembotropis nigricans	{"feketedő fürtös-zanót"}
Lemna aequinoctalis	{"szárnyas békalencse"}
Lemna minuta	{"szemcsés békalencse"}
Lemna turionifera	{"fias békalencse"}
Leonurus cardiaca	{"szúrós gyöngyajak"}
Lepidium crasssifolium	{"pozsgás zsázsa"}
Libanotis pyrenaica	{tömjénillat}
Libanotis pyrenaica subsp. intermedia	{tömjénillat}
Linaria × kocianovichii	{"hibrid gyújtoványfű"}
Lindernia dubia	{"rövidkocsányú iszapfű"}
Lithospermum arvense	{"mezei gyöngyköles"}
Lithospermum arvense subsp. coerulescens	{"mezei gyöngyköles"}
Lithospermum purpureo-coeruleum	{"erdei gyöngyköles"}
Lophozia ascendens	{"felálló hegyesmájmoha"}
Lotus borbasii	{Borbás-kerep}
Lotus corniculatus var. villosus	{"borzas szarvas kerep"}
Lotus tenuis	{"sziki kerep"}
Lotus uliginosus	{"lápi kerep"}
Lunaria annua subsp. pachyrrhiza	{"kerti holdviola"}
Luzula divulgata	{"csokros perjeszittyó"}
Lycopsis arvensis	{"vetési farkasszem"}
Lycopus europaeus subsp. mollis	{"vízi peszérce"}
Lycopus × intercendens	{peszércehibrid}
Lythrum × scrabrum	{"hibrid füzény"}
Majanthemum bifolium	{árnyékvirág}
Malus sylvestris subsp. dasyphylla	{"alacsony vadalma"}
Malva crispa	{"takarmány mályva"}
Mannia triandra	{"sziklai illatosmoha"}
Marrubium × paniculatum	{"korcs pemetefű"}
Matricaria maritima subsp. inodora	{"kaporlevelű ebszékfű"}
Matricaria tenuifolia	{"bánsági székfű"}
Matthiola longipetala subsp. bicornis	{"görög viola"}
Medicago nigra	{"déli lucerna"}
Medicago × varia	{"homoki lucerna"}
Meesia triquetra	{"háromélű moha"}
Melampyrum bihariense prol. romanicum	{"erdélyi csormolya"}
Melandrium album	{"fehér mécsvirág"}
Melandrium noctiflorum	{"esti mécsvirág"}
Melandrium sylvestre	{"piros mécsvirág"}
Melandrium viscosum	{"ragadós mécsvirág"}
Melilotus altissimus subsp. macrorrhizus	{"réti somkóró"}
Mentha × carinthiaca	{"karintiai menta"}
Mentha × dalmatica	{"dalmát menta"}
Mentha × dumetorum	{"berki menta"}
Mentha × gentilis	{"nemes menta"}
Mentha × verticillata	{"örvös menta"}
Mercurialis × paxii	{"hosszúnyelű szélfű"}
Micropus erectus	{"sudár topagyom"}
Minuartia fastigiata	{"nyalábos kőhúr"}
Minuartia frutescens	{"magyar kőhúr"}
Minuartia verna	{"gyepes kőhúr"}
Minuartia verna subsp. ramosissima	{"gyepes kőhúr"}
Molinia arundinacea	{"nádképű kékperje"}
Molinia arundinacea subsp. pocsii	{Pócs-kékperje}
Molinia arundinacea subsp. ujhelyii	{Ujhelyi-kékperje}
Molinia caerulea subsp. horanszkyi	{Horánszky-kékperje}
Molinia caerulea subsp. hungarica	{"magyar kékperje"}
Molinia caerulea subsp. simonii	{Simon-kékperje}
Monotropa hypopitys	{"valódi fenyőspárga"}
Monotropa hypopitys subsp. hypophegea	{"kopasz fenyőspárga"}
Montia fontana subsp. minor	{"kis forrásfű"}
Montia linearis	{"egyenes forrásfű"}
Muscari botryoides subsp. kerneri	{Kerner-epergyöngyike}
Muscari botryoides subsp. transsilvanicum	{"erdélyi epergyöngyike"}
Muscari racemosum	{"fürtös gyöngyike"}
Myosotis palustris	{"mocsári nefelejcs"}
Myosotis sicula	{"szicíliai nefelejcs"}
Myriophyllum brasiliense	{"strucctoll süllőhínár"}
Narcissus angustifolius	{"csillagos nárcisz"}
Narcissus poëticus	{"fehér nárcisz"}
Neckera pennata	{"tollas függönymoha"}
Nepeta pannonica	{"bugás macskamenta"}
Neslea paniculata	{"parlagi sömörje"}
Nicandra physalodes	{szilkesark}
Odontites vulgaris	{"vörös fogfű"}
Oenothera hoelscheri	{Hölscher-ligetszépe}
Oenothera salicifolia	{"magyar ligetszépe"}
Oenothera syrticola	{"afrikai ligetszépe"}
Ononis spinosiformis subsp. semihircina	{"bozontos tiszaháti iglice"}
Onosma arenarium	{"homoki vértő"}
Onosma arenarium subsp. tuberculatum	{"bibircses vértő"}
Ophrys holubyana	{Holub-bangó}
Ophrys × nelsonii	{Nelson-bangó}
Ophrys scolopax subsp. cornuta	{"szarvas bangó"}
Ophrys sphecodes	{pókbangó}
Orchis × angusticruris	{"majom és bíboros kosbor hibrid"}
Orchis × hybrida	{"bíboros és vitézkosbor hibridje"}
Orchis × timbalii	{Timbal-kosbor}
Orchis ustulata subsp. aestivalis	{"nyári sömörös kosbor"}
Origanum vulgare subsp. prismaticum	{szurokfű}
Orlaya grandiflora	{Orlay-turbolya}
Ornithogalum degenianum	{Degen-madártej}
Ornithogalum orthophyllum	{"pusztai madártej"}
Orobanche cernua	{"bókoló vajvirág"}
Orobanche loricata	{"üröm vajvirág"}
Orthantha lutea	{"sárga fogfű"}
Orthotrichum rogeri	{"alhavasi szőrössüvegűmoha"}
Orthotrichum scanicum	{"halványzöld szőrössüvegűmoha"}
Orthotrichum stellatum	{"csillagos szőrössüvegűmoha"}
Oxalis fontana	{"sárga madársóska"}
Padus avium	{májusfa}
Padus serotina	{"kései meggy"}
Paeonia officinalis	{"kerti bazsarózsa"}
Panicum dichotomiflorum	{"karcsú köles"}
Panicum miliaceum subsp. ruderale	{"törékeny köles"}
Papaver rhoeas	{pipacs}
Parietaria diffusa	{"ágas falgyom"}
Peltaria perennis	{"szárölelő pajzstok"}
Petrorhagia glumacea var. obcordata	{"pelyvás aszúszegfű"}
Petunia × atkinsiana	{"nagyvirágú petúnia"}
Peucedanum carvifolia	{"köménylevelű kocsord"}
Phalaroides arundinacea	{pántlikafű}
Phascum floerkeanum	{"vörösbarna rügymoha"}
Phaseolus coccineus	{"török bab"}
Phleum hubbardii	{"hagymás komócsin"}
Physcomitrium sphaericum	{"gömbös körtikemoha"}
Physocaulis nodosus	{dudatönk}
Picris hieracioides subsp. crepoides	{"nagytermésű keserűgyökér"}
Pisum sativum subsp. arvense	{"vetemény borsó"}
Plantago lanceolata subsp. dubia	{"lándzsás útifű"}
Plantago lanceolata subsp. sphaerostachya	{"lándzsás útifű"}
Plantago major	{"nagy útifű"}
Platanthera × hybrida	{"hibrid sarkvirág"}
Platanus × hybrida	{"juharlevelű platán"}
Platycladus orientalis	{"keleti életfa"}
Poa pannonica subsp. scabra	{"magyar perje"}
Poa stiriaca	{"hajszállevelű perje"}
Poa subcoerulea	{"északi perje"}
Podospermum canum	{"közönséges szikipozdor"}
Podospermum laciniatum	{"sallangos szikipozdor"}
Polycnemum heuffelii	{Heuffel-torzon}
Polygala amara subsp. brachyptera	{"osztrák keserű pacsirtafű"}
Polygonum amphibium	{vidrakeserűfű}
Polygonum bistorta	{"kígyógyökerű keserűfű"}
Polygonum hydropiper	{"borsos keserűfű"}
Polygonum lapathifolium	{"lapulevelű keserűfű"}
Polygonum lapathifolium subsp. danubiale	{"dunai lapulevelű keserűfű"}
Polygonum lapathifolium subsp. incanum	{"molyhos lapulevelű keserűfű"}
Polygonum minus	{"keskenylevelű keserűfű"}
Polygonum mite	{"szelíd keserűfű"}
Polygonum neglectum	{"vidéki porcsinkeserűfű"}
Polygonum patulum	{Bellard-porcsinkeserűfű}
Polygonum persicaria	{"baracklevelű keserűfű"}
Populus × canadensis	{"kanadai nyár"}
Populus × canescens	{"szürke nyár"}
Populus × gileadensis	{"nagylevelű nyár"}
Portulaca grandiflora	{"nagyvirágú porcsin"}
Potamogeton panormitanus	{"apró békaszőlő"}
Potamogeton × zizii	{"keskenylevelű békaszőlő"}
Potentilla argentea subsp. decumbens	{"ezüst pimpó"}
Potentilla argentea subsp. demissa	{"ezüst pimpó"}
Potentilla argentea subsp. grandiceps	{"ezüst pimpó"}
Potentilla argentea subsp. macrotoma	{"ezüst pimpó"}
Potentilla argentea subsp. tenuiloba	{"ezüst pimpó"}
Potentilla recta subsp. crassa	{"egyenes pimpó"}
Potentilla recta subsp. obscura	{"egyenes pimpó"}
Potentilla recta subsp. pilosa	{"egyenes pimpó"}
Potentilla recta subsp. tuberosa	{"egyenes pimpó"}
Potentilla wiemanniana var. javorkae	{Wiemann-pimpó}
Primula × brevistyla	{kankalinhibrid}
Primula veris subsp. inflata	{"molyhos tavaszi kankalin"}
Prunella × bicolor	{"kétszínű gyíkfű"}
Prunella × intermedia	{"hibrid gyíkfű"}
Prunella × spuria	{"hibrid gyíkfű"}
Prunus spinosa subsp. dasyphylla	{"szőröslevelű kökény"}
Prunus spinosa subsp. fruticans	{"nagytermésű kökény"}
Pterygoneurum lamellatum	{"lemezes szárnyaserűmoha"}
Puccinellia pannonica	{"magyar mézpázsit"}
Pulsatilla × mixta	{"hibrid kökörcsin"}
Pulsatilla pratensis subsp. nigricans lus. rubra	{"fekete kökörcsin"}
Pulsatilla pratensis subsp. zimmermannii	{"hegyi kökörcsin"}
Pycreus flavescens	{sárgapalka}
Pyracantha coccinea	{"közönséges tűztövis"}
Pyramidula tetragona	{"négysarkú piramismoha"}
Pyrus × austriaca	{"osztrák körte"}
Pyrus × karpatiana	{"kárpáti körte"}
Pyrus × mecsekensis	{"mecseki körte"}
Pyrus nivalis subsp. salviifolia	{"zsályalevelű körte"}
Pyrus pyraster subsp. anchras	{"molyhoslevelű vadkörte"}
Quercus farnetto	{"magyar tölgy"}
Quercus robur subsp. asterotricha	{"csillagszőrös kocsányos tölgy"}
Quercus robur subsp. pilosa	{"szőrös kocsányos tölgy"}
Ranunculus cymbalaria	{pintyő-boglárka}
Ranunculus nemorosus	{"berki boglárka"}
Reynoutria aubertii	{"kínai tatáriszalag"}
Reynoutria × bohemica	{"cseh óriáskeserűfű"}
Reynoutria sachalinensis	{"szahalini óriáskeserűfű"}
Rhamnus catharticus	{varjútövis}
Ribes rubrum subsp. rubrum	{"kerti vörös ribiszke"}
Ribes rubrum subsp. sylvestre	{"vad vörös ribiszke"}
Ribes uva-crispa subsp. grossularia	{"mirigyes egres"}
Ribes uva-crispa subsp. reclinatum	{egres}
Robinia pseudo-acacia	{"fehér akác"}
Rorippa × anceps	{"heverő kányafű"}
Rorippa × armoracioides	{"tormaképű kányafű"}
Rorippa × astylis	{"lantos kányafű"}
Rorippa × hungarica	{"magyar kányafű"}
Rorippa sylvestris subsp. sylvestris	{"erdei kányafű"}
Rosa arvensis	{"erdei rózsa"}
Rosa dumalis	{"szürke rózsa"}
Rosa gizellae	{Gizella-rózsa}
Rosa hungarica	{"magyar rózsa"}
Rosa livescens	{"nagylevelű rózsa"}
Rosa polyacantha	{"illír rózsa"}
Rosa × reversa	{"hibrid rózsa"}
Rosa scarbriuscula	{"érdeslevelű rózsa"}
Rosa spinosissima	{jajrózsa}
Rosa subcanina	{rózsafaj}
Rosa subcollina	{"dombvidéki rózsa"}
Rosa tomentosa	{"molyhos rózsa"}
Rosa zagrabiensis	{"zágrábi rózsa"}
Rubus armeniacus	{"örmény szeder"}
Rubus bellardii	{szederfaj}
Rubus candicans	{"fehéres szeder"}
Rubus fruticosus agg.	{"földi szeder"}
Rubus koehleri	{szederfaj}
Rubus macrophyllus	{"nagylevelű szeder"}
Rubus praecox	{"korai szeder"}
Rubus pyramidalis	{"piramisalakú szeder"}
Rubus rhombifolius	{"rombuszlevelű szeder"}
Rubus schleicheri	{szederfaj}
Rubus silesiacus	{"sziléziai szeder"}
Rubus sylvaticus	{"erdei szeder"}
Rubus thyrsiflorus	{szederfaj}
Rumex obtusifolius subsp. obtusifolius	{"réti lórom"}
Rumex obtusifolius subsp. subalpinus	{"réti lórom"}
Rumex obtusifolius subsp. sylvestris	{"réti lórom"}
Rumex obtusifolius subsp. transiens	{"réti lórom"}
Rumex patientia subsp. recurvatus	{"paréj lórom"}
Rynchostegiella jacquinii	{"mattzöld kiscsőrűmoha"}
Rynchostegium rotundifolium	{"kereklevelű hosszúcsőrűmoha"}
Sagina ciliata	{"pillás zöldhúr"}
Sagina micropetala	{"sziromtalan zöldhúr"}
Sagina sabuletorum	{"homoki zöldhúr"}
Salicornia prostrata subsp. simonkaiana	{Simonkai-sziksófű}
Salix eleagnos	{"parti fűz"}
Salix × multinervis	{hibridfűz}
Salix nigricans	{"feketedő fűz"}
Salix rosmarinifolia	{cinegefűz}
Salix triandra subsp. discolor	{mandulafűz}
Salsola collina	{"keskenylevelű ballagófű"}
Salsola kali subsp. ruthenica	{"homoki ballagófű"}
Salvia aethiops	{"magyar zsálya"}
Salvia verbenacea	{zsályafaj}
Sanguisorba minor subsp. muricata	{"csabaíre vérfű"}
Sarothamnus scoparius	{"közönséges seprőzanót"}
Scabiosa triandra subsp. agrestis	{"mezei ördögszem"}
Scilla drunensis subsp. buekkensis	{"bükki csillagvirág"}
Scilla paratheticum	{"balkáni csillagvirág"}
Scilla spetana	{Speta-csillagvirág}
Scolochloa festucacea	{"északi mocsáricsenkesz"}
Scorpidium scorpioides	{skorpiómoha}
Scrophularia umbrosa subsp. neesii	{"szárnyas görvélyfű"}
Sedum maximum	{"bablevelű varjúháj"}
Sedum neglectum subsp. sopianae	{"adriai varjúháj"}
Sedum reflexum	{"hegyi varjúháj"}
Sedum reflexum subsp. glaucum	{"szürkés hegyi varjúháj"}
Senecio aurantiacus	{"narancsszínű aggófű"}
Senecio erucifolius subsp. tenuifolius	{"keskenylevelű aggófű"}
Senecio fluviatilis	{"patakparti aggófű"}
Senecio integrifolius	{"mezei aggófű"}
Senecio jacobaea	{"Jakabnapi aggófű"}
Senecio nemorensis	{"berki aggófű"}
Senecio nemorensis subsp. fuchsii	{"kárpáti aggófű"}
Senecio ovirensis subsp. gaudinii	{"tömött havasalji aggófű"}
Senecio paludosus subsp. lanatus	{"mocsári aggófű"}
Senecio rivularis	{csermelyaggófű}
Seseli varium	{"változó gurgolya"}
Sesleria heuflerana	{"erdélyi nyúlfarkfű"}
Sesleria sadlerana	{"budai nyúlfarkfű"}
Sesleria uliginosa	{"lápi nyúlfarkfű"}
Sesleria varia	{"tarka nyúlfarkfű"}
Setaria × decipiens	{"csalékony muhar"}
Sieglingia decumbens	{háromfogfű}
Silene donetzica	{"donyeci habszegfű"}
Silene longiflora	{"gór habszegfű"}
Silene otites subsp. pseudo-otites	{"szikár habszegfű"}
Silene otites subsp. wolgensis	{"szikár habszegfű"}
Soldanella hungarica	{"magyar harangrojt"}
Solidago gigantea subsp. serotina	{"magas aranyvessző"}
Solidago graminifolia	{"fűlevelű aranyvessző"}
Sonchus arvensis subsp. uliginosus	{"kopasz csorbóka"}
Sorbus × acutiserratus	{"kőhányási berkenye"}
Sorbus × adamii	{Ádám-berkenye}
Sorbus × andreanszkyana	{Andreánszky-berkenye}
Sorbus aria agg.	{"lisztes berkenye"}
Sorbus × bakonyensis	{"bakonyi berkenye"}
Sorbus × balatonica	{"balatoni berkenye"}
Sorbus × barthae	{Bartha-berkenye}
Sorbus × bodajkensis	{"bodajki berkenye"}
Sorbus × borosiana	{Boros-berkenye}
Sorbus × budaiana	{Budai-berkenye}
Sorbus × buekkensis	{"bükki berkenye"}
Sorbus carpinifolia	{"gyertyánlevelű berkenye"}
Sorbus × danubialis	{"dunai berkenye"}
Sorbus × decipientiformis	{"keszthelyi berkenye"}
Sorbus × degenii	{Degen-berkenye}
Sorbus × dracofolius	{"gánti berkenye"}
Sorbus × eugenii-kelleri	{Keller-berkenye}
Sorbus × gayeriana	{Gáyer-berkenye}
Sorbus × gerecseensis	{"gerecsei berkenye"}
Sorbus × huljakii	{Hulják-berkenye}
Sorbus incisa	{"fogaslevelű berkenye"}
Sorbus × javorkae	{Jávorka-berkenye}
Sorbus × karpatii	{Kárpáti-berkenye}
Sorbus × latissima	{"nagylevelű berkenye"}
Sorbus × majeri	{Majer-berkenye}
Sorbus × pannonica	{" dunántúli berkenye"}
Sorbus × pseudobakonyensis	{"rövidkaréjú berkenye"}
Sorbus × pseudodanubialis	{"dunamenti berkenye"}
Sorbus × pseudograeca	{"kislevelű berkenye"}
Sorbus × pseudolatifolia	{"széleslevelű berkenye"}
Sorbus × pseudosemiincisa	{"kevéserű berkenye"}
Sorbus × pseudovertesensis	{"csákberényi berkenye"}
Sorbus × redliana	{Rédl-berkenye}
Sorbus × rotundifolia	{"kereklevelű berkenye"}
Sorbus × semiincisa	{"budai berkenye"}
Sorbus × semipinnata	{"hibrid madárberkenye"}
Sorbus × simonkaiana	{Simonaki-berkenye}
Sorbus × subdanubialis	{"középdunai berkenye"}
Sorbus × thaiszii	{Thaisz-berkenye}
Sorbus × tobani	{Tobán-berkenye}
Sorbus × ulmifolia	{"szillevelű berkenye"}
Sorbus × vajdae	{Vajda-berkenye}
Sorbus × vallerubusensis	{"szedresvölgyi berkenye"}
Sorbus × vertesensis	{"vértesi berkenye"}
Sorbus × veszpremensis	{"veszprémi berkenye"}
Sorbus × vrabelyiana	{Vrabélyi-berkenye}
Sorbus × zolyomii	{Zólyomi-berkenye}
Spergularia marina	{"sziki budavirág"}
Sphagnum auriculatum	{"fülecskés tőzegmoha"}
Sphagnum capillifolium	{"félgömbfejű tőzegmoha"}
Sphagnum centrale	{"sárgásbarna tőzegmoha"}
Sphagnum compactum	{"tömöttágú tőzegmoha"}
Sphagnum contortum	{"csavart tőzegmoha"}
Sphagnum cuspidatum	{"keskenylevelű tőzegmoha"}
Sphagnum fimbriatum	{"rojtos tőzegmoha"}
Sphagnum girgensohnii	{Girgensohn-tőzegmoha}
Sphagnum obtusum	{"tompalevelű tőzegmoha"}
Sphagnum palustre	{"csónakos tőzegmoha"}
Sphagnum platyphyllum	{"lágy tőzegmoha"}
Sphagnum quinquefarium	{"ötlevélsoros tőzegmoha"}
Sphagnum recurvum	{"karcsú tőzegmoha"}
Sphagnum russowii	{"óriás tőzegmoha"}
Sphagnum squarrosum	{"berzedt tőzegmoha"}
Sphagnum subnitens	{"halványbarna tőzegmoha"}
Sphagnum subsecundum	{"zászlós tőzegmoha"}
Sphagnum teres	{"láperdei tőzegmoha"}
Sphagnum warnstorfi	{Warnstorf-tőzegmoha}
Spiraea × van-houttei	{"kerti gyöngyvessző"}
Sporobolus cryptandrus	{"homoki prérifű"}
Stellaria media subsp. neglecta	{"kövér tyúkhúr"}
Stellaria media subsp. pallida	{"törpe tyúkhúr"}
Stellaria uliginosa	{"posvány csillaghúr"}
Stenactis annua	{"egynyári seprence"}
Stenactis annua subsp. strigosa	{"ligeti seprence"}
Stipa joannis subsp. puberula	{"hegyi árvalányhaj"}
Suaeda maritima subsp. salinaria	{"erdélyi sóballa"}
Suaeda prostrata subsp. prostrata	{"heverő sziki sóballa"}
Symphoricarpos rivularis	{hóbogyó}
Symphytum officinale subsp. bohemicum	{"sárgás nadálytő"}
Symphytum officinale subsp. uliginosum	{"posvány nadálytő"}
Symphytum tuberosum subsp. angustifolium	{"gumós nadálytő"}
Taeniatherum asperum	{medúzafű}
Taxiphyllum densifolium	{"sűrűleveles kaukázusimoha"}
Tetragonolobus maritimus subsp. siliquosus	{bársonykerep}
Thesium arvense	{"homoki zsellérke"}
Thlaspi coerulescens	{"havasaljai tarsóka"}
Thlaspi kovatsii subsp. schudichii	{Schudich-tarsóka}
Thymus pulegioides subsp. montanus	{"hegyi kakukkfű"}
Thymus vulgaris	{"kerti kakukkfű"}
Tilia rubra	{"vörös hárs"}
Tilia rubra ssp. caucasica	{"kaukázusi hárs"}
Torilis ucranica	{"ukrán tüskemag"}
Tortula brevissima	{törpelöszmoha}
Tragopogon dubius	{"nagy bakszakál"}
Trichophorum caespitosum	{"gyepes kisgyapjúsás"}
Trifolium pallidum	{"halvány here"}
Trifolium pratense subsp. expansum	{"réti here"}
Trifolium pratense subsp. sativum	{"réti here"}
Trifolium subterraneum var. brachycladum	{"földbentermő here"}
Trinia ramosissima	{"magyar nyúlkapor"}
Trollius europaeus subsp. demissorum	{zergeboglár}
Trollius europaeus subsp. tatrae	{"tátrai zergeboglár"}
Turritis glabra	{"kopasz toronyszál"}
Valeriana excelsa	{"bodzalevelű macskagyökér"}
Valeriana stolonifera	{"dombi macskagyökér"}
Veratrum album subsp. lobelianum	{"zöldes zászpa"}
Verbascum austriacum	{"osztrák ökörfarkkóró"}
Veronica asutriaca subsp. bihariense	{"osztrák veronika"}
Veronica austriaca subsp. dentata	{"fogaslevelű veronika"}
Veronica chamaedrys subsp. vindobonensis	{"bécsi veronika"}
Veronica hederifolia subsp. lucorum	{sövény-veronika}
Veronica hederifolia subsp. triloba	{"háromkaréjú veronika"}
Veronica longifolia	{"hosszúlevelű veronika"}
Veronica orchidea	{veronika}
Veronica pallens	{"szürke veronika"}
Veronica paniculata	{"bugás veronika"}
Veronica scardica	{"kislevelű veronika"}
Veronica spicata	{"macskafarkú veronika"}
Veronica spicata subsp. nitens	{"macskafarkú veronika"}
Vicia grandiflora	{"nagyvirágú bükköny"}
Vicia grandiflora subsp. biebersteinii	{Bieberstein-bükköny}
Vicia grandiflora subsp. sordida	{"szennyes bükköny"}
Vicia narbonensis subsp. serratifolia	{"fogaslevelű bükköny"}
Vicia villosa subsp. pseudovillosa	{"szöszös bükköny"}
Viola elatior subsp. jordanii	{Jordan-ibolya}
Viola montana	{"hegyi ibolya"}
Viola sylvestris	{"erdei ibolya"}
Viola tricolor subsp. polychroma	{"hegyi háromszínű árvácska"}
Viscaria vulgaris	{"enyves kakukkszegfű"}
Waldsteinia geoides	{Waldstein-pimpó}
Weissia rostellata	{"kiscsőrű gyöngymoha"}
Xanthium albinum subsp. riparium	{"elbai szerbtövis"}
Xanthium saccharatum	{"nagytermésű szerbtövis"}
X Orchidactyla Drudei	{"polsokaszagú kosbor és hússzínű kosbor hibridje"}
Zannichellia palustris subsp. pedicellata	{tófonal}
Zannichellia palustris subsp. polycarpa	{"soktermésű tófonal"}
Acanthaclisis occitanica	{"pusztai hangyaleső","Pusztai hangyaleső"}
Acanthocinus aedilis	{Daliáscincér,"nagy daliáscincér"}
Acanthocinus griseus	{"szürke daliáscincér","Szürke daliáscincér"}
Acasis appensata	{békabogyó-araszoló,Békabogyó-araszoló}
Accipiter brevipes	{"kis héja","Kis héja"}
Accipiter gentilis	{héja,Héja}
Accipiter gentilis buteoides	{héja,Héja}
Accipiter nisus	{karvaly,Karvaly}
Acherontia atropos	{"halálfejes lepke","Halálfejes lepke"}
Acilius canaliculatus	{"sárgacombú barázdáscsíkbogár","Sárgacombú barázdáscsíkbogár"}
Acilius sulcatus	{"gyűrűscombú barázdáscsíkbogár","Gyűrűscombú barázdáscsíkbogár"}
Acinopus ammophilus	{"nagy aknásfutó","Nagy aknásfutó"}
Acipenser gueldenstaedtii	{"vágó tok",vágótok}
Acipenser nudiventris	{"sima tok",simatok}
Acmaeops pratensis	{"alhavasi gömbtorúcincér","Alhavasi gömbtorúcincér"}
Acrocephalus agricola	{"rozsdás nádiposzáta","Rozsdás nádiposzáta"}
Acrocephalus agricola septima	{"rozsdás nádiposzáta","Rozsdás nádiposzáta"}
Acrocephalus arundinaceus	{nádirigó,Nádirigó}
Acrocephalus melanopogon	{fülemülesitke,Fülemülesitke}
Acrocephalus paludicola	{"csíkosfejű nádiposzáta","Csíkosfejű nádiposzáta"}
Acrocephalus palustris	{"énekes nádiposzáta","Énekes nádiposzáta"}
Acrocephalus schoenobaenus	{"foltos nádiposzáta","Foltos nádiposzáta"}
Acrocephalus scirpaceus	{"cserregő nádiposzáta","Cserregő nádiposzáta"}
Acrotylus longipes	{"hosszúlábú önbeásó sáska","önbeásó sáska"}
Pteris vittata	{"Csíkos szallagpáfrány"}
Adrastus lacertosus	{"ligeti cserjepattanó","sötétbarna cserjepattanó"}
Adrastus limbatus	{"hegyi cserjepattanó","ligeti cserjepattanó"}
Adrastus montanus	{"hegyi cserjepattan","sárga cserjepattanó"}
Adrastus pallens	{"közönséges cserjepattanó","sárga cserjepattanó"}
Adscita geryon	{"ritka fémlepke","Ritka fémlepke"}
Aedes cinereus	{"vöröshátú szúnyog","Vöröshátú szúnyog"}
Aedes rossicus	{"hullámtéri szúnyog","Hullámtéri szúnyog"}
Aedes vexans	{"gyötrő szúnyog","Gyötrő szúnyog"}
Aegithalos caudatus	{őszapó,Őszapó}
Aegolius funereus	{gatyáskuvik,Gatyáskuvik}
Aegomorphus clavipes	{"tarka cincér","Tarka cincér"}
Aegosoma scabricorne	{diófacincér,Diófacincér}
Aegypius monachus	{barátkeselyű,Barátkeselyű}
Agabus affinis	{"lápi gyászcsíkbogár","Lápi gyászcsíkbogár"}
Agabus biguttatus	{"pataki gyászcsíkbogár","Pataki gyászcsíkbogár"}
Agabus bipustulatus	{"gyakori gyászcsíkbogár","Gyakori gyászcsíkbogár"}
Agabus congener	{"erdei gyászcsíkbogár","Erdei gyászcsíkbogár"}
Agabus conspersus	{"vörhenyes gyászcsíkbogár","Vörhenyes gyászcsíkbogár"}
Agabus fuscipennis	{"vöröslábú gyászcsíkbogár","Vöröslábú gyászcsíkbogár"}
Agabus guttatus	{"hegyi gyászcsíkbogár","Hegyi gyászcsíkbogár"}
Agabus labiatus	{"kis gyászcsíkbogár","Kis gyászcsíkbogár"}
Agabus melanarius	{"kormos gyászcsíkbogár","Kormos gyászcsíkbogár"}
Agabus nebulosus	{"sárgás gyászcsíkbogár","Sárgás gyászcsíkbogár"}
Agabus paludosus	{"barnaszárnyú gyászcsíkbogár","Barnaszárnyú gyászcsíkbogár"}
Agabus striolatus	{"recés gyászcsíkbogár","Recés gyászcsíkbogár"}
Agabus uliginosus	{"réti gyászcsíkbogár","Réti gyászcsíkbogár"}
Agabus undulatus	{"harántsávos gyászcsíkbogár","Harántsávos gyászcsíkbogár"}
Agapanthia cardui	{"sávos bogáncscincér","Sávos bogáncscincér"}
Agapanthia cynarae	{"déli bogáncscincér","Déli bogáncscincér"}
Agapanthia dahli	{"sárgagyűrűs bogáncscincér","Sárgagyűrűs bogáncscincér"}
Agapanthia intermedia	{varfűcincér,Varfűcincér}
Agapanthia kirbyi	{"ökörfarkkóró cincér","Ökörfarkkóró cincér"}
Agapanthia maculicornis	{Harangvirág-bogáncscincér,harangvirágcincér}
Agapanthia osmanlis	{mácsonyacincér,Mácsonyacincér}
Agapanthia villosoviridescens	{"fehérgyűrűs bogáncscincér","Fehérgyűrűs bogáncscincér"}
Agapanthia violacea	{"kék bogáncscincér","Kék bogáncscincér"}
Agapanthiola leucaspis	{"Magyar bogáncscincér","magyar zsályacincér"}
Agdistis intermedia	{"magyar egytollúmoly","Magyar egytollúmoly"}
Aglais urticae	{"kis rókalepke","Kis rókalepke"}
Agrilus kubani	{kubán-karcsúdíszbogár,Kubán-karcsúdíszbogár}
Akimerus schaefferi	{szilfacincér,Szilfacincér}
Alauda arvensis	{"mezei pacsirta","Mezei pacsirta"}
Alauda arvensis cantarella	{"mezei pacsirta","Mezei pacsirta"}
Alauda arvensis dulcivox	{"mezei pacsirta","Mezei pacsirta"}
Albocosta musiva	{"szegélyes földibagoly","Szegélyes földibagoly"}
Alca torda	{alka,Alka}
Alcedo atthis	{jégmadár,Jégmadár}
Alcedo atthis ispida	{jégmadár,Jégmadár}
Aleochara verna	{"délvidéki fürkészholyva","komposztlakó fürkészholyva"}
Algedonia luctualis	{"fehérfoltos kormosmoly","Fehérfoltos kormosmoly"}
Alopochen aegyptiacus	{"nílusi lúd","Nílusi lúd"}
Alosterna tabacicolor	{juharcincér,Juharcincér}
Anastrangalia reyi	{"északi virágcincér","Északi virágcincér"}
Ammophila terminata	{mocsáry-hernyóölő,Mocsáry-hernyóölő}
Ampedus erythrogonus	{hjort-pattanó,Hjort-pattanó}
Ampedus hjorti	{hjort-pattanó,Hjort-pattanó}
Amphipoea lucens	{"Lápi fűgyökérbagoly","törékeny lápibagoly"}
Anacaena globulus	{"sötét kerekcsibor","Sötét kerekcsibor"}
Anacaena limbata	{"gyakori kerekcsibor","Gyakori kerekcsibor"}
Anacaena lutescens	{"barnás kerekcsibor","Barnás kerekcsibor"}
Anaesthetis testacea	{szedercincér,Szedercincér}
Anaglyptus mysticus	{juhardíszcincér,Juhardíszcincér}
Anarta myrtilli	{csarabbagoly,Törpeövesbagoly}
Anas acuta	{"nyílfarkú réce","Nyílfarkú réce"}
Anas clypeata	{"kanalas réce","Kanalas réce"}
Anas crecca	{"csörgő réce","Csörgő réce"}
Anasimyia transfuga	{"kampóssávú iszaplégy","Kampóssávú iszaplégy"}
Anas penelope	{"fütyülő réce","Fütyülő réce"}
Anas platyrhynchos	{"tőkés réce","Tőkés réce"}
Anas querquedula	{"böjti réce","Böjti réce"}
Anas strepera	{"kendermagos réce","Kendermagos réce"}
Anastrangalia dubia	{"feketeszélű virágcincér","Feketeszélű virágcincér"}
Anastrangalia sanguinolenta	{"kétszínű virágcincér","Kétszínű virágcincér"}
Anisarthron barbipes	{"szőrös cincér","Szőrös cincér"}
Anisorus quercus	{tölgycincér,Tölgycincér}
Asplenium ×kümmerlei	{Kümmerle-páfrány}
Anopheles claviger	{"sárga maláriaszúnyog","Sárga maláriaszúnyog"}
Anopheles hyrcanus	{"tarkaszárnyú maláriaszúnyog","Tarkaszárnyú maláriaszúnyog"}
Anopheles maculipennis	{"foltos maláriaszúnyog","Foltos maláriaszúnyog"}
Anopheles plumbeus	{"hamvas maláriaszúnyog","Hamvas maláriaszúnyog"}
Anoplodera rufipes	{"vöröslábú virágcincér","Vöröslábú virágcincér"}
Anoplodera sexguttata	{"foltos virágcincér","Foltos virágcincér"}
Anser albifrons	{"nagy lilik","Nagy lilik"}
Anser anser	{"nyári lúd","Nyári lúd"}
Anser anser rubrirostris	{"nyári lúd","Nyári lúd"}
Anser brachyrhynchus	{"rövidcsőrű lúd","Rövidcsőrű lúd"}
Anser caerulescens	{"sarki lúd","Sarki lúd"}
Anser caerulescens atlanticus	{"sarki lúd","Sarki lúd"}
Anser erythropus	{"kis lilik","Kis lilik"}
Anser fabalis	{"vetési lúd","Vetési lúd"}
Anser fabalis johanseni	{"vetési lúd","Vetési lúd"}
Anser fabalis rossicus	{"vetési lúd","Vetési lúd"}
Anser indicus	{"indiai lúd","Indiai lúd"}
Anthaxia godeti	{godet-virágdíszbogár,Godet-virágdíszbogár}
Anthaxia suzannae	{zsuzsanna-virágdíszbogár,Zsuzsanna-virágdíszbogár}
Anthonomus kirschi	{kirsch-rügylikasztó,Kirsch-rügylikasztó}
Anthropoides virgo	{"pártás daru",Pártásdaru}
Anthus campestris	{"parlagi pityer","Parlagi pityer"}
Anthus cervinus	{"rozsdástorkú pityer","Rozsdástorkú pityer"}
Anthus pratensis	{"réti pityer","Réti pityer"}
Anthus richardi	{"sarkantyús pityer","Sarkantyús pityer"}
Anthus spinoletta	{"havasi pityer","Havasi pityer"}
Anthus trivialis	{"erdei pityer","Erdei pityer"}
Apamea platinea	{Platinabagoly,platina-dudvabagoly}
Apamea sicula tallosi	{Tallós-dudvabagoly,Tallós-fűgyökérbagoly}
Apatania muliebris	{"páratlan alpesitegzes","Páratlan alpesitegzes"}
Asplenium javorkaeanum	{"Magyar pikkelypáfrány"}
Apatura ilia	{"kis színjátszólepke","Kis színjátszólepke"}
Apatura iris	{"nagy színjátszólepke","Nagy színjátszólepke"}
Apatura metis	{"magyar színjátszólepke","Magyar színjátszólepke"}
Apaustis rupicola	{"szirti törpebagoly","Szirti törpebagoly"}
Apion frumentarium	{madársóska-cickányormányos,"vörös cickányormányos"}
Apus apus	{sarlósfecske,Sarlósfecske}
Apus pallidus	{"halvány sarlósfecske","Halvány sarlósfecske"}
Aquila chrysaetos	{"szirti sas","Szirti sas"}
Aquila clanga	{"fekete sas","Fekete sas"}
Aquila heliaca	{"parlagi sas","Parlagi sas"}
Aquila nipalensis	{"pusztai sas","Pusztai sas"}
Aquila nipalensis orientalis	{"pusztai sas","Pusztai sas"}
Aquila pomarina	{"békászó sas","Békászó sas"}
Adiantum capillus-veneris	{Vénuszhajpáfrány}
Arctia festiva	{"díszes medvelepke","Díszes medvelepke"}
Ardea cinerea	{"szürke gém","Szürke gém"}
Ardea purpurea	{"vörös gém","Vörös gém"}
Ardeola bacchus	{"kínai üstökösgém","Kínai üstökösgém"}
Ardeola ralloides	{üstökösgém,Üstökösgém}
Arenaria interpres	{kőforgató,Kőforgató}
Argynnis laodice	{"keleti gyöngyházlepke","Keleti gyöngyházlepke"}
Argynnis niobe	{ibolya-gyöngyházlepke,Ibolya-gyöngyházlepke}
Argynnis pandora	{"zöldes gyöngyházlepke","Zöldes gyöngyházlepke"}
Arhopalus ferus	{"szomorú fenyőcincér","Szomorú fenyőcincér"}
Arhopalus rusticus	{"gödrösnyakú cincér","Gödrösnyakú cincér"}
Arichanna melanaria	{tőzegáfonya-araszoló,Tőzegáfonya-araszoló}
Aromia moschata	{pézsmacincér,Pézsmacincér}
Arytrura musculus	{"keleti lápibagoly","Keleti lápibagoly"}
Asemum striatum	{"komor fenyőcincér","Komor fenyőcincér"}
Asio flammeus	{"réti fülesbagoly","Réti fülesbagoly"}
Asio otus	{"erdei fülesbagoly","Erdei fülesbagoly"}
Asteroscopus syriaca decipulae	{"magyar őszi-fésűsbagoly","Magyar őszi-fésűsbagoly"}
Athene noctua	{kuvik,Kuvik}
Athous apfelbecki	{apfelbeck-pattanó,Apfelbeck-pattanó}
Athous kaszabi	{kaszab-pattanó,Kaszab-pattanó}
Aulacobaris kaufmanni	{kaufmann-báris,Kaufmann-báris}
Aulonogyrus concinnus	{"szegélyes keringőbogár","Szegélyes keringőbogár"}
Austropotamobius torrentium	{"kövi rák","Kövi rák"}
Axinopalpis gracilis	{"kecses selymescincér","Kecses selymescincér"}
Aythya affinis	{"búbos réce","Búbos réce"}
Aythya collaris	{"örvös réce","Örvös réce"}
Aythya ferina	{barátréce,Barátréce}
Aythya fuligula	{"kontyos réce","Kontyos réce"}
Aythya marila	{"hegyi réce","Hegyi réce"}
Aythya nyroca	{cigányréce,Cigányréce}
Baccha elongata	{"lombikhasú darázslégy","Lombikhasú darázslégy"}
Baetis fuscatus	{"teleszkópszemű kérész","Teleszkópszemű kérész"}
Baetis rhodani	{"tavaszi kérész","Tavaszi kérész"}
Barbus barbus	{márna,"rózsás márna"}
Barypeithes chevrolati	{chevrolat-mohaormányos,Chevrolat-mohaormányos}
Barypeithes formaneki	{formánek-mohaormányos,Formánek-mohaormányos}
Batazonellus lacerticida	{"pompás útonállódarázs","Pompás útonállódarázs"}
Berosus affinis	{"egyfoltos szemescsibor","Egyfoltos szemescsibor"}
Berosus frontifoveatus	{"tüskés szemescsibor","Tüskés szemescsibor"}
Berosus fulvus	{"feketeállú szemescsibor","Feketeállú szemescsibor"}
Berosus geminus	{"sávosnyakú szemescsibor","Sávosnyakú szemescsibor"}
Berosus luridus	{"foltosnyakú szemescsibor","Foltosnyakú szemescsibor"}
Berosus signaticollis	{"csíkosnyakú szemescsibor","Csíkosnyakú szemescsibor"}
Berosus spinosus	{"széki szemescsibor","Széki szemescsibor"}
Bidessus grossepunctatus	{"pontozott törpecsíkbogár","Pontozott törpecsíkbogár"}
Bidessus nasutus	{"tarka törpecsíkbogár","Tarka törpecsíkbogár"}
Bidessus unistriatus	{"barna törpecsíkbogár","Barna törpecsíkbogár"}
Bielzia coerulans	{"Kék meztelecsiga","kék meztelencsiga"}
Boloria euphrosyne	{árvácska-gyöngyházlepke,"Ezüstfoltos gyöngyházlepke"}
Boloria selene	{"fakó gyöngyházlepke","Fakó gyöngyházlepke"}
Bombina bombina	{"vöröshasú unka","Vöröshasú unka"}
Bombina variegata	{"sárgahasú unka","Sárgahasú unka"}
Bombus argillaceus	{"délvidéki poszméh","Délvidéki poszméh"}
Bombus fragrans	{"óriás poszméh",óriásposzméh}
Bombus ruderatus	{"ligeti poszméh","Ligeti poszméh"}
Bombus subterraneus	{"földi poszméh","rövidszőrű poszméh"}
Bombycilla garrulus	{csonttollú,Csonttollú}
Bonasa bonasia	{császármadár,Császármadár}
Bonasa bonasia rupestris	{császármadár,Császármadár}
Botaurus stellaris	{bölömbika,Bölömbika}
Brachygonus megerlei	{megerle-pattanó,Megerle-pattanó}
Brachymyia floccosa	{"sárga bundáslégy","Sárga bundáslégy"}
Brachysomus fremuthi	{fremuth-gyepormányos,Fremuth-gyepormányos}
Brachysomus frivaldszkyi	{frivaldszky-gyepormányos,Frivaldszky-gyepormányos}
Brachysomus mihoki	{"bakonyi gyepormányos",Mihók-gyepormányos}
Brachysomus zellichi	{zellich-gyepormányos,Zellich-gyepormányos}
Bradybatus creutzeri	{creutzer-ormányos,Creutzer-ormányos}
Bradybatus kellneri	{kellner-ormányos,Kellner-ormányos}
Branta bernicla	{"örvös lúd","Örvös lúd"}
Branta canadensis	{"kanadai lúd","Kanadai lúd"}
Branta leucopsis	{apácalúd,Apácalúd}
Branta ruficollis	{"vörösnyakú lúd","Vörösnyakú lúd"}
Brenthis ino	{"lápi gyöngyházlepke","Lápi gyöngyházlepke"}
Bruchela schusteri	{schuster-rezedabogár,Schuster-rezedabogár}
Brychius elevatus	{"pataki víztaposó","Pataki víztaposó"}
Bubo bubo	{uhu,Uhu}
Bubulcus ibis	{pásztorgém,Pásztorgém}
Bucephala clangula	{kerceréce,Kerceréce}
Bufo bufo	{"barna varangy","Barna varangy"}
Bufo viridis	{"zöld varangy","Zöld varangy"}
Burhinus oedicnemus	{ugartyúk,Ugartyúk}
Buteo buteo	{egerészölyv,Egerészölyv}
Buteo buteo vulpinus	{Egerészölyv,"vörösfarkú egerészölyv"}
Buteo lagopus	{"gatyás ölyv","Gatyás ölyv"}
Buteo rufinus	{"pusztai ölyv","Pusztai ölyv"}
Caenis horaria	{törpekérész,Törpekérész}
Caenis macrura	{"apró kérész","Apró kérész"}
Calamobius filum	{"hosszúcsápú szalmacincér",Szalmacincér}
Calandrella brachydactyla	{szikipacsirta,Szikipacsirta}
Calandrella brachydactyla hungarica	{szikipacsirta,Szikipacsirta}
Calcarius lapponicus	{"sarkantyús sármány","Sarkantyús sármány"}
Calidris alba	{fenyérfutó,Fenyérfutó}
Calidris alpina	{"havasi partfutó","Havasi partfutó"}
Calidris alpina schinzii	{"havasi partfutó","Havasi partfutó"}
Calidris bairdii	{baird-partfutó,Baird-partfutó}
Calidris canutus	{"sarki partfutó","Sarki partfutó"}
Calidris ferruginea	{"sarlós partfutó","Sarlós partfutó"}
Calidris fuscicollis	{bonaparte-partfutó,Bonaparte-partfutó}
Calidris maritima	{"tengeri partfutó","Tengeri partfutó"}
Calidris melanotos	{vándorpartfutó,Vándorpartfutó}
Calidris minuta	{"apró partfutó","Apró partfutó"}
Calidris pusilla	{"kis partfutó","Kis partfutó"}
Callidium aeneum	{"bronzos kéregcincér","Bronzos kéregcincér"}
Callidium coriaceum	{"bőrszárnyú fenyőcincér","Bőrszárnyú fenyőcincér"}
Callidium violaceum	{"kék korongcincér","Kék korongcincér"}
Callimoxys gracilis	{"frakkos cincér","Frakkos cincér"}
Calliptamus barbarus	{"barbár sáska",barbársáska}
Calosoma inquisitor	{"kis bábrabló","Kis bábrabló"}
Calyciphora xanthodactyla	{hangyabogáncs-tollasmoly,Hangyabogáncs-tollasmoly}
Camptorhinus simplex	{"mozdulatlan ormányosbogár","sima holttettetős-ormányos"}
Camptorhinus statua	{"bordás holttettetős-ormányos","holttettetős ormányosbogár"}
Canis lupus	{farkas,"szürke farkas"}
Caprimulgus europaeus	{lappantyú,Lappantyú}
Caprimulgus europaeus meridionalis	{lappantyú,Lappantyú}
Carabus hampei	{"beregi futrinka","sokbordás futrinka"}
Carassius auratus	{aranyhal,ezüstkárász}
Carcharodus lavatherae	{tisztesfű-busalepke,Tisztesfű-busalepke}
Cardiophorus dolini	{dolin-szívespattanó,Dolin-szívespattanó}
Cardoria scutellata	{sarlófűcincér,Sarlófűcincér}
Carduelis cannabina	{kenderike,Kenderike}
Carduelis carduelis	{tengelic,Tengelic}
Carduelis flammea	{zsezse,Zsezse}
Carduelis flammea cabaret	{"barna zsezse","Barna zsezse"}
Carduelis flavirostris	{"sárgacsőrű kenderike","Sárgacsőrű kenderike"}
Carduelis hornemanni	{"szürke zsezse","Szürke zsezse"}
Carduelis hornemanni exilipes	{"szürke zsezse","Szürke zsezse"}
Carduelis spinus	{csíz,Csíz}
Carinatodorcadion aethiops	{"fekete gyalogcincér","Fekete gyalogcincér"}
Carinatodorcadion fulvum cervae	{"barna gyalogcincér","Pusztai gyalogcincér"}
Carpodacus erythrinus	{karmazsinpirók,Karmazsinpirók}
Carpodacus roseus	{"rózsás pirók","Rózsás pirók"}
Catocala conversa	{"füstös övesbagoly","Sötét övesbagoly"}
Catocala dilecta	{"nagy övesbagoly","Nagy tölgyfa-övesbagoly"}
Catocala diversa	{"Füstös övesbagoly",kőrisfa-övesbagoly}
Catocala fraxini	{"kék övesbagoly","Kéköves bagoly"}
Catopta thrips	{sztyeplepke,Sztyeplepke}
Centroptilum luteolum	{"kardszárnyú kérész","Kardszárnyú kérész"}
Ceraclea nigronervosa	{"szürke hosszúcsápú-tegzes","Szürke hosszúcsápú-tegzes"}
Cerambyx cerdo	{"nagy hőscincér","Nagy hőscincér"}
Cerambyx miles	{"Katonás cincér","katonás hőscincér"}
Cerambyx scopolii	{"kis hőscincér","Kis hőscincér"}
Cerambyx welensii	{"molyhos hőscincér","Molyhos hőscincér"}
Cercyon analis	{"sárgalábú iszapcsiborka","Sárgalábú iszapcsiborka"}
Cercyon bifenestratus	{"sárgaszegélyű iszapcsiborka","Sárgaszegélyű iszapcsiborka"}
Cercyon convexiusculus	{"selymes iszapcsiborka","Selymes iszapcsiborka"}
Cercyon granarius	{"kis iszapcsiborka","Kis iszapcsiborka"}
Cercyon haemorrhoidalis	{"csúcsosvégű iszapcsiborka","Csúcsosvégű iszapcsiborka"}
Cercyon hungaricus	{"magyar iszapcsiborka","Magyar iszapcsiborka"}
Cercyon impressus	{"gödörkés iszapcsiborka","Gödörkés iszapcsiborka"}
Cercyon laminatus	{"jövevény iszapcsiborka","Jövevény iszapcsiborka"}
Cercyon lateralis	{"vörösbarna iszapcsiborka","Vörösbarna iszapcsiborka"}
Cercyon marinus	{"vörösvégű iszapcsiborka","Vörösvégű iszapcsiborka"}
Cercyon melanocephalus	{"háromszögfoltos iszapcsiborka","Háromszögfoltos iszapcsiborka"}
Cercyon nigriceps	{"feketefejű iszapcsiborka","Feketefejű iszapcsiborka"}
Cercyon obsoletus	{"nagy iszapcsiborka","Nagy iszapcsiborka"}
Cercyon pygmaeus	{"törpe iszapcsiborka","Törpe iszapcsiborka"}
Cercyon quisquilius	{"sárgahátú iszapcsiborka","Sárgahátú iszapcsiborka"}
Cercyon sternalis	{"apró iszapcsiborka","Apró iszapcsiborka"}
Cercyon terminatus	{"feketefoltos iszapcsiborka","Feketefoltos iszapcsiborka"}
Cercyon tristis	{"sötét iszapcsiborka","Sötét iszapcsiborka"}
Cercyon unipunctatus	{"egypontos iszapcsiborka","Egypontos iszapcsiborka"}
Cercyon ustulatus	{"kétíves iszapcsiborka","Kétíves iszapcsiborka"}
Ceriana conopsoides	{"szarvas darázslégy","Szarvas darázslégy"}
Certhia brachydactyla	{"rövidkarmú fakusz","Rövidkarmú fakusz"}
Certhia familiaris	{"hegyi fakusz","Hegyi fakusz"}
Certhia familiaris macrodactyla	{"hegyi fakusz","Hegyi fakusz"}
Cettia cetti	{"berki poszáta","Berki poszáta"}
Ceutorhynchus assimilis	{repcebecő-ormányos,repcegyökér-ormányos}
Ceutorhynchus gerhardti	{gerhardt-ormányos,Gerhardt-ormányos}
Ceutorhynchus hampei	{hampe-hamukaormányos,Hampe-hamukaormányos}
Ceutorhynchus merkli	{merkl-ormányos,Merkl-ormányos}
Ceutorhynchus pandellei	{pandelle-ormányos,Pandelle-ormányos}
Ceutorhynchus strejceki	{strejcek-ormányos,Strejcek-ormányos}
Ceutorhynchus wagneri	{wagner-ormányos,Wagner-ormányos}
Chaetarthria seminulum	{gömbcsiborka,Gömbcsiborka}
Chaetopteryx rugulosa	{"nyugati őszitegzes","Nyugati őszitegzes"}
Chamaesphecia hungarica	{"magyar szitkár","Magyar szitkár"}
Chamaesphecia palustris	{"mocsári szitkár","Mocsári szitkár"}
Charadrius alexandrinus	{"széki lile","Széki lile"}
Charadrius dubius	{"kis lile","Kis lile"}
Charadrius hiaticula	{"parti lile","Parti lile"}
Charadrius hiaticula tundrae	{"parti lile","Parti lile"}
Charadrius leschenaultii	{"sivatagi lile","Sivatagi lile"}
Charadrius leschenaultii columbinus	{"sivatagi lile","Sivatagi lile"}
Charadrius morinellus	{"havasi lile","Havasi lile"}
Charadrius vociferus	{"ékfarkú lile","Ékfarkú lile"}
Chariaspilates formosaria	{"Lápi tarkaaraszoló","pompás lápiaraszoló"}
Chazara briseis	{"tarka szemeslepke","Tarka szemeslepke"}
Cheilosia canicularis	{martilapu-bronzlégy,Martilapu-bronzlégy}
Cheilosia chloris	{acsalapu-bronzlégy,Acsalapu-bronzlégy}
Cheilosia chrysocoma	{"vörhenyes bronzlégy","Vörhenyes bronzlégy"}
Cheilosia impressa	{"feketelábú bronzlégy","Feketelábú bronzlégy"}
Cheilosia mutabilis	{bogáncs-bronzlégy,Bogáncs-bronzlégy}
Cheilosia pagana	{"füstösszárnyú bronzlégy","Füstösszárnyú bronzlégy"}
Cheilosia scutellata	{gomba-bronzlégy,Gomba-bronzlégy}
Cheilosia variabilis	{"közönséges bronzlégy","Közönséges bronzlégy"}
Chersotis fimbriola	{kökörcsinvirág-földibagoly,Kökörcsinvirág-földibagoly}
Chersotis fimbriola baloghi	{kökörcsinvirág-földibagoly,Kökörcsinvirág-földibagoly}
Chettusia gregaria	{lilebíbic,Lilebíbic}
Chettusia leucura	{"fehérfarkú lilebíbic","Fehérfarkú lilebíbic"}
Chlidonias hybridus	{fattyúszerkő,Fattyúszerkő}
Chlidonias leucopterus	{"fehérszárnyú szerkő","Fehérszárnyú szerkő"}
Chlidonias niger	{"kormos szerkő","Kormos szerkő"}
Chlorophorus figuratus	{"rajzos darázscincér","Rajzos darázscincér"}
Chlorophorus hungaricus	{"magyar darázscincér","Magyar darázscincér"}
Chlorophorus sartor	{"feketevállú darázscincér","Feketevállú darázscincér"}
Chlorophorus trifasciatus	{"vörösnyakú darázscincér","Vörösnyakú darázscincér"}
Chlorophorus varius	{"díszes darázscincér","Díszes darázscincér"}
Chondrosoma fiduciaria	{"magyar ősziaraszoló","Magyar ősziaraszoló"}
Choragus horni	{horn-orrosbogár,Horn-orrosbogár}
Choragus sheppardi	{sheppard-orrosbogár,Sheppard-orrosbogár}
Chorthippus loratus	{"hosszúcsápú rétisáska","hosszúszárnyú rétisáska"}
Chrysopa abbreviata	{"rövidszárnyú fátyolka","Rövidszárnyú fátyolka"}
Chrysopa formosa	{"csillogó fátyolka","Csillogó fátyolka"}
Chrysopa pallens	{"hétfoltos fátyolka","Hétfoltos fátyolka"}
Chrysopa perla	{"aranyszemű fátyolka","Aranyszemű fátyolka"}
Chrysopa phyllochroma	{"zöld fátyolka","Zöld fátyolka"}
Chrysoperla carnea	{"közönséges fátyolka","Közönséges fátyolka"}
Chrysotoxum arcuatum	{"közönséges öveslégy","Közönséges öveslégy"}
Chrysotropia ciliata	{"szőrösszárnyú fátyolka","Szőrösszárnyú fátyolka"}
Ciconia ciconia	{"fehér gólya","Fehér gólya"}
Ciconia nigra	{"fekete gólya","Fekete gólya"}
Cidnopus platiai	{platia-pattanó,Platia-pattanó}
Cinclus cinclus	{vízirigó,Vízirigó}
Cinclus cinclus aquaticus	{vízirigó,Vízirigó}
Cionus clairvillei	{clairville-gömbormányos,Clairville-gömbormányos}
Cionus ganglbaueri	{ganglbauer-gömbormányos,Ganglbauer-gömbormányos}
Cionus gebleri	{gebler-gömbormányos,Gebler-gömbormányos}
Cionus leonhardi	{leonhard-gömbormányos,Leonhard-gömbormányos}
Circaetus gallicus	{kígyászölyv,Kígyászölyv}
Circus aeruginosus	{"barna rétihéja","Barna rétihéja"}
Circus cyaneus	{"kékes rétihéja","Kékes rétihéja"}
Circus macrourus	{"fakó rétihéja","Fakó rétihéja"}
Circus pygargus	{"hamvas rétihéja","Hamvas rétihéja"}
Clangula hyemalis	{jegesréce,Jegesréce}
Cloeon dipterum	{"elevenszülő kérész","Elevenszülő kérész"}
Cloeon simile	{"kis kérész","Kis kérész"}
Clytus arietis	{"közönséges darázscincér","Közönséges darázscincér"}
Clytus lama	{fenyvesdarázscincér,Fenyvesdarázscincér}
Clytus rhamni	{benge-darázscincér,Benge-darázscincér}
Clytus tropicus	{tölgy-darázscincér,Tölgydíszcincér}
Coccothraustes coccothraustes	{meggyvágó,Meggyvágó}
Coelostoma orbiculare	{"kétéltű csiborka","Kétéltű csiborka"}
Coenonympha oedippus	{"ezüstsávos szénalepke","Ezüstsávos szénalepke"}
Coleophora hungariae	{"magyar zsákosmoly","Magyar zsákosmoly"}
Colias chrysotheme	{dolomit-kéneslepke,Dolomit-kéneslepke}
Columba livia	{"szirti galamb","Szirti galamb"}
Columba livia f. domestica	{"parlagi galamb","Parlagi galamb"}
Columba oenas	{"kék galamb","Kék galamb"}
Columba palumbus	{"örvös galamb","Örvös galamb"}
Colymbetes fuscus	{"gyakori recéscsíkbogár","Gyakori recéscsíkbogár"}
Colymbetes striatus	{"sárgalábú recéscsíkbogár","Sárgalábú recéscsíkbogár"}
Coniopteryx tineiformis	{"közönséges lisztesfátyolka","Közönséges lisztesfátyolka"}
Pilularia globulifera	{"Borsókás sárgolyó"}
Conwentzia pineticola	{"csonkaszárnyú lisztesfátyolka","Csonkaszárnyú lisztesfátyolka"}
Copelatus haemorrhoidalis	{"rozsdás csíkbogár","Rozsdás csíkbogár"}
Coquillettidia richiardii	{"mocsári szúnyog","Mocsári szúnyog"}
Coracias garrulus	{szalakóta,Szalakóta}
Coronella austriaca	{rézsikló,Rézsikló}
Cortodera femorata	{"vöröscsápú cserjecincér","Vöröscsápú cserjecincér"}
Cortodera flavimana	{boglárkacincér,Boglárkacincér}
Cortodera holosericea	{"selymes cserjecincér","Selymes cserjecincér"}
Cortodera humeralis	{"négyfoltos cserjecincér","Négyfoltos cserjecincér"}
Cortodera villosa	{"bozontos cserjecincér","Bozontos cserjecincér"}
Corvus corax	{holló,Holló}
Corvus corone	{"Kormos/dolmányos varjú","kormos varjú"}
Corvus corone sardonius	{"dolmányos varjú","Dolmányos varjú"}
Corvus frugilegus	{"vetési varjú","Vetési varjú"}
Corvus monedula	{csóka,Csóka}
Corvus monedula soemmeringi	{csóka,Csóka}
Corvus monedula spermologus	{csóka,Csóka}
Salvinia auriculata	{"Füles rucaöröm"}
Coturnix coturnix	{fürj,Fürj}
Creoleon plumbeus	{"rozsdás hangyaleső","Rozsdás hangyaleső"}
Crex crex	{haris,Haris}
Criorhina asilica	{"öves bundáslégy","Öves bundáslégy"}
Cryptopleurum crenatum	{"nagy trágyacsiborka","Nagy trágyacsiborka"}
Cryptopleurum minutum	{"szőrös trágyacsiborka","Szőrös trágyacsiborka"}
Cryptopleurum subtile	{"vörhenyes trágyacsiborka","Vörhenyes trágyacsiborka"}
Cucullia campanulae	{harangvirág-csuklyásbagoly,Harangvirág-csuklyásbagoly}
Cucullia dracunculi	{"lilásszürke csuklyásbagoly",Tárkonyüröm-csuklyásbagoly}
Cucullia formosa	{"díszes csuklyásbagoly","Díszes csuklyásbagoly"}
Cucullia gnaphalii	{aranyvessző-csuklyásbagoly,Gyopár-csuklyásbagoly}
Cucullia lucifuga	{"hamvas csuklyásbagoly","Hamvas csuklyásbagoly"}
Cucullia mixta lorica	{"vértesi csuklyásbagoly","Vértesi csuklyásbagoly"}
Cucullia xeranthemi	{vasvirág-csuklyásbagoly,Vasvirág-csuklyásbagoly}
Cuculus canorus	{kakukk,Kakukk}
Culex modestus	{"foltos szúnyog","Foltos szúnyog"}
Culex pipiens	{"dalos szúnyog","Dalos szúnyog"}
Culex pipiens molestus	{"házi szúnyog","Házi szúnyog"}
Culiseta annulata	{"gyűrűs szúnyog","Gyűrűs szúnyog"}
Cupido osiris	{"hegyi törpeboglárka","Hegyi törpeboglárka"}
Curimus erichsoni	{erichson-labdacsbogár,Erichson-labdacsbogár}
Cyanapion gyllenhalii	{gyllenhal-cickányormányos,Gyllenhal-cickányormányos}
Cyanapion spencii	{spence-cickányormányos,Spence-cickányormányos}
Cybister lateralimarginalis	{"nagy búvárbogár","Nagy búvárbogár"}
Cybocephalus fodori	{fodor-pajzstetvészbogár,Fodor-pajzstetvészbogár}
Cygnus columbianus	{"kis hattyú","Kis hattyú"}
Cygnus cygnus	{"énekes hattyú","Énekes hattyú"}
Cygnus olor	{"bütykös hattyú","Bütykös hattyú"}
Cymbiodyta marginella	{"szegélyes csibor","Szegélyes csibor"}
Dapsa fodori	{fodor-álböde,Fodor-álböde}
Dasypoda suripes	{"ritka gatyásméh","Ritka gatyásméh"}
Datomicra zosterae	{"mocsári  penészholyva","mocsári penészholyva"}
Datonychus paszlavszkyi	{paszlavszky-ormányos,Paszlavszky-ormányos}
Deilus fugax	{zanótcincér,Zanótcincér}
Dendrocopos leucotos	{"fehérhátú fakopáncs","Fehérhátú fakopáncs"}
Dendrocopos major	{"nagy fakopáncs","Nagy fakopáncs"}
Dendrocopos major pinetorum	{"nagy fakopáncs","Nagy fakopáncs"}
Dendrocopos medius	{középfakopáncs,"Közép fakopáncs"}
Dendrocopos minor	{"kis fakopáncs","Kis fakopáncs"}
Dendrocopos minor hortorum	{"kis fakopáncs","Kis fakopáncs"}
Dendrocopos syriacus	{"balkáni fakopáncs","Balkáni fakopáncs"}
Dendroleon pantherinus	{"párducfoltos hangyaleső","Párducfoltos hangyaleső"}
Deporaus mannerheimii	{mannerheim-levélsodró,Mannerheim-levélsodró}
Dermestes kaszabi	{kaszab-porva,Kaszab-porva}
Dermestes szekessyi	{székessy-porva,Székessy-porva}
Deronectes latus	{"nagy rücsköscsíkbogár","Nagy rücsköscsíkbogár"}
Deroplia genei	{"keskeny tölgycincér","Keskeny tölgycincér"}
Diachrysia chryson	{"nagyfoltú aranybagoly","Nagyfoltú aranybagoly"}
Diarsia dahlii	{"barnáspiros földibagoly","Barnáspiros földibagoly"}
Dichagyris candelisequa	{"szigonyos földibagoly","Szigonyos földibagoly"}
Dichochrysa flavifrons	{"sárgahátú fátyolka","Sárgahátú fátyolka"}
Dichochrysa prasina	{"alföldi fátyolka","Alföldi fátyolka"}
Dichrostigma flavipes	{"sárgalábú tevenyakú","Sárgalábú tevenyakú"}
Dicranura ulmi	{szilfa-púposszövő,Szilfa-púposszövő}
Didea alneti	{"zöldfoltos zengőlégy","Zöldfoltos zengőlégy"}
Dinoptera collaris	{"vörösnyakú virágcincér","Vörösnyakú virágcincér"}
Dioszeghyana schmidti	{"magyar tavaszi-fésűsbagoly","Magyar tavaszi-fésűsbagoly"}
Distoleon tetragrammicus	{"négyfoltos hangyaleső","Négyfoltos hangyaleső"}
Dorytomus dejeani	{dejean-hangormányos,Dejean-hangormányos}
Dorytomus schoenherri	{schönherr-hangormányos,Schönherr-hangormányos}
Drepanepteryx phalaenoides	{"sarlósszárnyú fátyolka","Sarlósszárnyú fátyolka"}
Drusus trifidus	{karsztforrástegzes,Karsztforrástegzes}
Dryocopus martius	{"fekete harkály","Fekete harkály"}
Duvalius gebhardti	{Gebhardt-vakfutinka,Gebhardt-vakfutrinka}
Dyscia conspersaria	{sziklaüröm-araszoló,Sziklaüröm-araszoló}
Dytiscus circumcinctus	{"barnahasú csíkbogár","Barnahasú csíkbogár"}
Dytiscus circumflexus	{"foltoshasú csíkbogár","Foltoshasú csíkbogár"}
Dytiscus dimidiatus	{"tompacsípős csíkbogár","Tompacsípős csíkbogár"}
Dytiscus latissimus	{óriás-csíkbogár,"Óriás csíkbogár"}
Dytiscus marginalis	{"szegélyes csíkbogár","Szegélyes csíkbogár"}
Ecdyonurus venosus	{"hegyi kérész","Hegyi kérész"}
Egretta garzetta	{"kis kócsag","Kis kócsag"}
Egretta gularis	{zátonykócsag,Zátonykócsag}
Elaphe longissima	{"erdei sikló","Erdei sikló"}
Emberiza cia	{"bajszos sármány","Bajszos sármány"}
Emberiza cirlus	{sövénysármány,Sövénysármány}
Emberiza citrinella	{citromsármány,Citromsármány}
Emberiza hortulana	{"kerti sármány","Kerti sármány"}
Emberiza leucocephalos	{fenyősármány,Fenyősármány}
Emberiza melanocephala	{"kucsmás sármány","Kucsmás sármány"}
Emberiza pusilla	{törpesármány,Törpesármány}
Emberiza schoeniclus	{"nádi sármány","Nádi sármány"}
Emberiza schoeniclus intermedia	{"nádi sármány","Nádi sármány"}
Emberiza schoeniclus stresemanni	{"nádi sármány","Nádi sármány"}
Emberiza schoeniclus tschusii	{"nádi sármány","Nádi sármány"}
Emberiza schoeniclus ukrainae	{"nádi sármány","Nádi sármány"}
Emys orbicularis	{"mocsári teknős","Mocsári teknős"}
Endromis versicolora	{tarkaszövő,Tarkaszövő}
Ennomos quercaria	{molyhostölgy-levélaraszoló,Molyhostölgy-levélaraszoló}
Enochrus affinis	{"apró fakócsibor","Apró fakócsibor"}
Enochrus ater	{"kormos fakócsibor","Kormos fakócsibor"}
Enochrus bicolor	{"alföldi fakócsibor","Alföldi fakócsibor"}
Enochrus coarctatus	{"kis fakócsibor","Kis fakócsibor"}
Enochrus fuscipennis	{"barnalábú fakócsibor","Barnalábú fakócsibor"}
Enochrus halophilus	{"sósvízi fakócsibor","Sósvízi fakócsibor"}
Enochrus hamifer	{"széki fakócsibor","Széki fakócsibor"}
Enochrus melanocephalus	{"feketefejű fakócsibor","Feketefejű fakócsibor"}
Enochrus ochropterus	{"mocsári fakócsibor","Mocsári fakócsibor"}
Enochrus quadripunctatus	{"négypettyes fakócsibor","Négypettyes fakócsibor"}
Enochrus testaceus	{"barna fakócsibor","Barna fakócsibor"}
Entephria cyanata	{"bükki hegyiaraszoló","Bükki hegyiaraszoló"}
Epeorus assimilis	{"lebegő kérész","Lebegő kérész"}
Ephemera danica	{dánkérész,Dánkérész}
Ephemera vulgata	{"tarka kérész","Tarka kérész"}
Ephoron virgo	{dunavirág,Dunavirág}
Episyrphus balteatus	{"ékfoltos zengőlégy","Ékfoltos zengőlégy"}
Epuraea marseuli	{marseul-fénybogár,Marseul-fénybogár}
Eremophila alpestris	{"havasi fülespacsirta","Havasi fülespacsirta"}
Eremophila alpestris flava	{"havasi fülespacsirta","Havasi fülespacsirta"}
Eretes sticticus	{"szegélyesnyakú csíkbogár","Szegélyesnyakú csíkbogár"}
Ergates faber	{ácscincér,Ácscincér}
Eriogaster catax	{"sárga gyapjasszövő","Sárga gyapjasszövő"}
Eriogaster lanestris	{"tavaszi gyapjasszövő","Tavaszi gyapjasszövő"}
Eristalinus aeneus	{"bronzos herelégy","Bronzos herelégy"}
Eristalis arbustorum	{"fehérarcú herelégy","Fehérarcú herelégy"}
Eristalis interrupta	{"barna herelégy","Barna herelégy"}
Eristalis tenax	{"közönséges herelégy","Közönséges herelégy"}
Erithacus rubecula	{vörösbegy,Vörösbegy}
Ernobius kiesenwetteri	{kiesenwetter-tobozrágóálszú,Kiesenwetter-tobozrágóálszú}
Euchalcia variabilis	{sisakvirág-aranybagoly,Sisakvirág-aranybagoly}
Eumerus strigatus	{"kis nárciszlégy","Kis nárciszlégy"}
Euphydryas aurinia	{"lápi tarkalepke","Lápi tarkalepke"}
Euphydryas maturna	{"díszes tarkalepke","Díszes tarkalepke"}
Euphyia scripturata	{"vonalkás hegyiaraszoló","Vonalkás hegyiaraszoló"}
Eupithecia graphata	{hangyabogáncs-törpearaszoló,Hangyabogáncs-törpearaszoló}
Euplagia quadripunctaria	{"csíkos medvelepke","Csíkos medvelepke"}
Euroleon nostras	{"pöttyös hangyaleső","Pöttyös hangyaleső"}
Eurythyrea quercus	{tölgy-díszbogár,tölgyfa-díszbogár}
Eutrichapion gribodoi	{gribodo-cickányormányos,Gribodo-cickányormányos}
Euxoa birivia	{"ezüstös földibagoly","Ezüstös földibagoly"}
Euxoa decora	{"selymes földibagoly","Selymes földibagoly"}
Euxoa distinguenda	{"keleti földibagoly","Keleti földibagoly"}
Euxoa vitta	{dolomit-földibagoly,"Vonalkás földibagoly"}
Evodinus clathratus	{"rácsos mintáscincér","Rácsos mintáscincér"}
Exapion formaneki	{formánek-cickányormányos,Formánek-cickányormányos}
Exocentrus adspersus	{nyírfarőzsecincér,Nyírfarőzsecincér}
Exocentrus lusitanus	{hársrőzsecincér,Hársrőzsecincér}
Exocentrus punctipennis	{szilrőzsecincér,Szilrőzsecincér}
Exocentrus stierlini	{fűzrőzsecincér,Fűzrőzsecincér}
Falco cherrug	{kerecsensólyom,Kerecsensólyom}
Falco cherrug cyanopus	{kerecsensólyom,Kerecsensólyom}
Falco columbarius	{"kis sólyom","Kis sólyom"}
Falco columbarius aesalon	{"kis sólyom","Kis sólyom"}
Falco naumanni	{"fehérkarmú vércse","Fehérkarmú vércse"}
Falco peregrinus	{vándorsólyom,Vándorsólyom}
Falco peregrinus calidus	{vándorsólyom,Vándorsólyom}
Falco subbuteo	{kabasólyom,Kabasólyom}
Falco tinnunculus	{"vörös vércse","Vörös vércse"}
Falco vespertinus	{"kék vércse","Kék vércse"}
Ferdinandea cuprea	{"rézszínű zengőlégy","Rézszínű zengőlégy"}
Ficedula albicollis	{"örvös légykapó","Örvös légykapó"}
Ficedula hypoleuca	{"kormos légykapó","Kormos légykapó"}
Ficedula parva	{"kis légykapó","Kis légykapó"}
Fratercula arctica	{lunda,Lunda}
Fratercula arctica grabae	{lunda,Lunda}
Fringilla coelebs	{"erdei pinty","Erdei pinty"}
Fringilla montifringilla	{fenyőpinty,Fenyőpinty}
Fulica atra	{szárcsa,Szárcsa}
Furcula bicuspis	{apáca-púposszövő,Apáca-púposszövő}
Gagitodes sagittata	{"nyílfoltos tarkaaraszoló","Nyílfoltos tarkaaraszoló"}
Galerida cristata	{"búbos pacsirta",Búbospacsirta}
Galerida cristata tenuirostris	{búbospacsirta,Búbospacsirta}
Gallinago gallinago	{sárszalonka,Sárszalonka}
Gallinago media	{"nagy sárszalonka","Nagy sárszalonka"}
Gallinula chloropus	{vízityúk,Vízityúk}
Garrulus glandarius	{szajkó,Szajkó}
Garrulus glandarius albipectus	{szajkó,Szajkó}
Gasterocercus depressirostris	{"laposorrú ormányos","laposorrú ormányosbogár"}
Gaurotes virginea	{"kis fémescincér","Kis fémescincér"}
Gavia arctica	{"sarki búvár","Sarki búvár"}
Gavia immer	{"jeges búvár","Jeges búvár"}
Gavia stellata	{"északi búvár","Északi búvár"}
Gelochelidon nilotica	{kacagócsér,Kacagócsér}
Georissus crenulatus	{"gyakori fövenybogár","Gyakori fövenybogár"}
Georissus laesicollis	{"bordás fövenybogár","Bordás fövenybogár"}
Glaphyra kiesenwetteri	{"mandula légycincér","Mandula légycincér"}
Glaphyra schmidti	{"fűz légycincér","Fűz légycincér"}
Glaphyra umbellatarum	{"apró légycincér","Apró légycincér"}
Glareola nordmanni	{"feketeszárnyú székicsér","Feketeszárnyú székicsér"}
Glareola pratincola	{székicsér,Székicsér}
Glaucidium passerinum	{törpekuvik,Törpekuvik}
Glocianus lethierryi	{lethierry-ormányos,Lethierry-ormányos}
Glocianus moelleri	{möller-ormányos,Möller-ormányos}
Glyphipterix loricatella	{"budai szakállasmoly","Budai szakállasmoly"}
Gnathoncus buyssoni	{buysson-fészeksutabogár,Buysson-fészeksutabogár}
Gnathoncus schmidti	{schmidt-fészeksutabogár,Schmidt-fészeksutabogár}
Gortyna borelii lunata	{"nagy szikibagoly","Nagy szikibagoly"}
Gracilia minuta	{törpecincér,Törpecincér}
Grammoptera abdominalis	{"fekete galagonyacincér","Fekete galagonyacincér"}
Grammoptera ruficornis	{galagonyacincér,Galagonyacincér}
Grammoptera ustulata	{"aranyszőrű galagonyacincér","Aranyszőrű galagonyacincér"}
Graphoderus austriacus	{"kis tavicsíkbogár","Kis tavicsíkbogár"}
Graphoderus bilineatus	{"széles tavicsíkbogár","Széles tavicsíkbogár"}
Graphoderus cinereus	{"gyakori tavicsíkbogár","Gyakori tavicsíkbogár"}
Graphoderus zonatus	{"tompakarmú tavicsíkbogár","Tompakarmú tavicsíkbogár"}
Graptodytes bilineatus	{"gyakori csíkbogárka","Gyakori csíkbogárka"}
Graptodytes granularis	{"zömök csíkbogárka","Zömök csíkbogárka"}
Graptodytes pictus	{"gyűrűs csíkbogárka","Gyűrűs csíkbogárka"}
Graptus kaufmanni	{kaufmann-nadálytőormányos,Kaufmann-nadálytőormányos}
Grus grus	{daru,Daru}
Gyps fulvus	{"fakó keselyű","Fakó keselyű"}
Gyrinus colymbus	{"recés keringőbogár","Recés keringőbogár"}
Gyrinus distinctus	{"keskeny keringőbogár","Keskeny keringőbogár"}
Gyrinus marinus	{"érces keringőbogár","Érces keringőbogár"}
Gyrinus minutus	{"törpe keringőbogár","Törpe keringőbogár"}
Gyrinus paykulli	{"karcsú keringőbogár","Karcsú keringőbogár"}
Gyrinus substriatus	{"gyakori keringőbogár","Gyakori keringőbogár"}
Gyrinus suffriani	{"nádi keringőbogár","Nádi keringőbogár"}
Gyrophaena fasciata	{"öves taplóholyva","termetes taplóholyva"}
Haematopus ostralegus	{csigaforgató,Csigaforgató}
Haematopus ostralegus longipes	{csigaforgató,Csigaforgató}
Haliaeetus albicilla	{rétisas,Rétisas}
Haliplus apicalis	{"tengerparti víztaposó","Tengerparti víztaposó"}
Haliplus flavicollis	{"sárga víztaposó","Sárga víztaposó"}
Haliplus fluviatilis	{"kecses víztaposó","Kecses víztaposó"}
Haliplus fulvicollis	{"lápi víztaposó","Lápi víztaposó"}
Haliplus fulvus	{"nagy víztaposó","Nagy víztaposó"}
Haliplus furcatus	{"kockás víztaposó","Kockás víztaposó"}
Haliplus heydeni	{heyden-víztaposó,Heyden-víztaposó}
Haliplus immaculatus	{"csíkos víztaposó","Csíkos víztaposó"}
Haliplus laminatus	{"szürkés víztaposó","Szürkés víztaposó"}
Haliplus lineatocollis	{"sávosnyakú víztaposó","Sávosnyakú  víztaposó"}
Haliplus maculatus	{"foltos víztaposó","Foltos víztaposó"}
Haliplus obliquus	{"tavi víztaposó","Tavi víztaposó"}
Haliplus ruficollis	{"vörhenyes víztaposó","Vörhenyes víztaposó"}
Haliplus variegatus	{"tarka víztaposó","Tarka víztaposó"}
Helochares lividus	{"sárga laposcsibor","Sárga laposcsibor"}
Helochares obscurus	{"sötét laposcsibor","Sötét laposcsibor"}
Helophilus pendulus	{"közönséges iszaplégy","Közönséges iszaplégy"}
Helophilus trivittatus	{"nagy iszaplégy","Nagy iszaplégy"}
Helophorus aequalis	{"termetes vésettcsibor","Termetes vésettcsibor"}
Helophorus aquaticus	{"nagy vésettcsibor","Nagy vésettcsibor"}
Helophorus arvernicus	{"pataki vésettcsibor","Pataki vésettcsibor"}
Helophorus asperatus	{"érdes vésettcsibor","Érdes vésettcsibor"}
Helophorus brevipalpis	{"tarka vésettcsibor","Tarka vésettcsibor"}
Helophorus croaticus	{"színes vésettcsibor","Színes vésettcsibor"}
Helophorus discrepans	{"hegyi vésettcsibor","Hegyi vésettcsibor"}
Helophorus dorsalis	{"erdei vésettcsibor","Erdei vésettcsibor"}
Helophorus flavipes	{"lápi vésettcsibor","Lápi vésettcsibor"}
Helophorus granularis	{"szemcsés vésettcsibor","Szemcsés vésettcsibor"}
Helophorus griseus	{"fénylő vésettcsibor","Fénylő vésettcsibor"}
Helophorus liguricus	{"óriás vésettcsibor","Óriás vésettcsibor"}
Helophorus longitarsis	{"ragyogó vésettcsibor","Ragyogó vésettcsibor"}
Helophorus micans	{"sziki vésettcsibor","Sziki vésettcsibor"}
Helophorus minutus	{"kis vésettcsibor","Kis vésettcsibor"}
Helophorus montenegrinus	{"sötét vésettcsibor","Sötét vésettcsibor"}
Helophorus nanus	{"barázdásfejű vésettcsibor","Barázdásfejű vésettcsibor"}
Helophorus nubilus	{"réti vésettcsibor","Réti vésettcsibor"}
Helophorus obscurus	{"nyomottvállú vésettcsibor","Nyomottvállú vésettcsibor"}
Helophorus paraminutus	{"karcsú vésettcsibor","Karcsú vésettcsibor"}
Helophorus porculus	{"vetési vésettcsibor","Vetési vésettcsibor"}
Helophorus redtenbacheri	{redtenbacher-vésettcsibor,Redtenbacher-vésettcsibor}
Helophorus rufipes	{"torz vésettcsibor","Torz vésettcsibor"}
Helophorus strigifrons	{"szűkhomlokú vésettcsibor","Szűkhomlokú vésettcsibor"}
Helophorus villosus	{"sárgás vésettcsibor","Sárgás vésettcsibor"}
Hemaris fuciformis	{dongószender,Pöszörszender}
Hemaris tityus	{Dongószender,pöszörszender}
Hemerobius humulinus	{"közönséges barnafátyolka","Közönséges barnafátyolka"}
Hemerobius nitidulus	{fenyő-barnafátyolka,Fenyő-barnafátyolka}
Hemitrichapion waltoni	{walton-cickányormányos,Walton-cickányormányos}
Heptagenia flava	{"sárga kérész","Sárga kérész"}
Heptagenia longicauda	{"halvány kérész","Halvány kérész"}
Heptagenia sulphurea	{patakvirág,Patakvirág}
Herophila tristis	{"selymes alkonycincér","Selymes alkonycincér"}
Herpes porcellus	{"bütyköshátú ormányos","bütykös ormányosbogár"}
Heteropterus morpheus	{"tükrös busalepke","Tükrös busalepke"}
Hieraaetus fasciatus	{héjasas,Héjasas}
Hieraaetus pennatus	{törpesas,Törpesas}
Himantopus himantopus	{gólyatöcs,Gólyatöcs}
Hippolais icterina	{"kerti geze","Kerti geze"}
Hirundo daurica	{"vörhenyes fecske","Vörhenyes fecske"}
Hirundo rustica	{"füsti fecske","Füsti fecske"}
Hoplopterus spinosus	{"tüskés bíbic","Tüskés bíbic"}
Hucho hucho	{"dunai galóca",galóca}
Hydaticus aruspex	{"csíkos mocsáricsíkbogár","Csíkos mocsáricsíkbogár"}
Hydaticus continentalis	{"sávos mocsáricsíkbogár","Sávos mocsáricsíkbogár"}
Hydaticus grammicus	{"déli mocsáricsíkbogár","Déli  mocsáricsíkbogár"}
Hydaticus seminiger	{"fekete mocsáricsíkbogár","Fekete mocsáricsíkbogár"}
Hydaticus transversalis	{"harántsávos mocsáricsíkbogár","Harántsávos  mocsáricsíkbogár"}
Hydrelia sylvata	{"havasi lápiaraszoló","Havasi lápiaraszoló"}
Hydrobius fuscipes	{"változékony csibor","Változékony csibor"}
Hydrochara caraboides	{"kis csibor","Kis csibor"}
Hydrochara dichroma	{"keleti csibor","Keleti csibor"}
Hydrochara flavipes	{"sárgalábú csibor","Sárgalábú csibor"}
Hydrochus angustatus	{"bronzos nyurgacsibor","Bronzos nyurgacsibor"}
Hydrochus brevis	{"zömök nyurgacsibor","Zömök nyurgacsibor"}
Hydrochus crenatus	{"nagy nyurgacsibor","Nagy nyurgacsibor"}
Hydrochus elongatus	{"bordás nyurgacsibor","Bordás nyurgacsibor"}
Hydrochus flavipennis	{"barna nyurgacsibor","Barna nyurgacsibor"}
Hydrochus ignicollis	{"karcsú nyurgacsibor","Karcsú nyurgacsibor"}
Hydrochus megaphallus	{turul-nyurgacsibor,Turul-nyurgacsibor}
Hydroglyphus geminus	{"gyakori paránycsíkbogár","Gyakori paránycsíkbogár"}
Hydrophilus aterrimus	{"fekete óriáscsibor","Fekete óriáscsibor"}
Hydrophilus piceus	{"közönséges óriáscsibor","Közönséges óriáscsibor"}
Hydroporus angustatus	{"karcsú kiscsíkbogár","Karcsú kiscsíkbogár"}
Hydroporus discretus ponticus	{"pontusi kiscsíkbogár","Pontusi kiscsíkbogár"}
Hydroporus dobrogeanus	{forrás-kiscsíkbogár,Forrás-kiscsíkbogár}
Hydroporus erythrocephalus	{"vörösfejű kiscsíkbogár","Vörösfejű kiscsíkbogár"}
Hydroporus ferrugineus	{"rozsdás kiscsíkbogár","Rozsdás kiscsíkbogár"}
Hydroporus foveolatus	{"alpesi kiscsíkbogár","Alpesi kiscsíkbogár"}
Hydroporus fuscipennis	{"barnaszárnyú kiscsíkbogár","Barnaszárnyú kiscsíkbogár"}
Hydroporus hebaueri	{hebauer-kiscsíkbogár,Hebauer-kiscsíkbogár}
Hydroporus incognitus	{"vöröshasú kiscsíkbogár","Vöröshasú kiscsíkbogár"}
Hydroporus longicornis	{"fényes kiscsíkbogár","Fényes kiscsíkbogár"}
Hydroporus marginatus	{"szegélyes kiscsíkbogár","Szegélyes kiscsíkbogár"}
Hydroporus melanarius	{"zömök kiscsíkbogár","Zömök kiscsíkbogár"}
Hydroporus memnonius	{"érces kiscsíkbogár","Érces kiscsíkbogár"}
Hydroporus neglectus	{"apró kiscsíkbogár","Apró kiscsíkbogár"}
Hydroporus nigrita	{"fekete kiscsíkbogár","Fekete kiscsíkbogár"}
Hydroporus notatus	{"nagyfejű kiscsíkbogár","Nagyfejű kiscsíkbogár"}
Hydroporus palustris	{"mocsári kiscsíkbogár","Mocsári kiscsíkbogár"}
Hydroporus planus	{"gyakori kiscsíkbogár","Gyakori kiscsíkbogár"}
Hydroporus rufifrons	{"termetes kiscsíkbogár","Termetes kiscsíkbogár"}
Hydroporus scalesianus	{"pöttöm kiscsíkbogár","Pöttöm kiscsíkbogár"}
Hydroporus striola	{"tarka kiscsíkbogár","Tarka kiscsíkbogár"}
Hydroporus tristis	{"gyászos kiscsíkbogár","Gyászos kiscsíkbogár"}
Hydroporus umbrosus	{"barnás kiscsíkbogár","Barnás kiscsíkbogár"}
Hydrovatus cuspidatus	{"hegysszárnyú csíkbogár","Hegysszárnyú csíkbogár"}
Hygrobia hermanni	{"európai pocsolyaúszó","Európai pocsolyaúszó"}
Hygrotus confluens	{"sárga aprócsíkbogár","Sárga aprócsíkbogár"}
Hygrotus decoratus	{"keresztes aprócsíkbogár","Keresztes aprócsíkbogár"}
Hygrotus enneagrammus	{"csíkos aprócsíkbogár","Csíkos aprócsíkbogár"}
Hygrotus impressopunctatus	{"barázdás aprócsíkbogár","Barázdás aprócsíkbogár"}
Hygrotus inaequalis	{"gyakori aprócsíkbogár","Gyakori aprócsíkbogár"}
Hygrotus pallidulus	{"barnahasú aprócsíkbogár","Barnahasú aprócsíkbogár"}
Hygrotus versicolor	{"tarka aprócsíkbogár","Tarka aprócsíkbogár"}
Hyla arborea	{"zöld levelibéka","Zöld levelibéka"}
Hylotrupes bajulus	{"házi cincér","Házi cincér"}
Hymenalia morio	{"magyar alkonybogár","magyarföldi alkonybogár"}
Hyphydrus anatolicus	{"déli gömbcsíkbogár","Déli gömbcsíkbogár"}
Hyphydrus ovatus	{"gyakori gömbcsíkbogár","Gyakori gömbcsíkbogár"}
Hyponephele lupinus	{"homoki ökörszemlepke","Homoki ökörszemlepke"}
Hypothenemus hampei	{hampe-szú,Hampe-szú}
Hypsugo savii	{"alpesi denevér","alpesi törpedenevér"}
Ilybius ater	{"nagy orsócsíkbogár","Nagy orsócsíkbogár"}
Ilybius chalconatus	{"rezes gyászcsíkbogár","Rezes gyászcsíkbogár"}
Ilybius crassus	{"zömök orsócsíkbogár","Zömök orsócsíkbogár"}
Ilybius erichsoni	{erichson-gyászcsíkbogár,Erichson-gyászcsíkbogár}
Ilybius fenestratus	{"vörhenyes orsócsíkbogár","Vörhenyes orsócsíkbogár"}
Ilybius fuliginosus	{"szegélyes orsócsíkbogár","Szegélyes orsócsíkbogár"}
Ilybius guttiger	{"kis orsócsíkbogár","Kis orsócsíkbogár"}
Ilybius neglectus	{"fényes gyászcsíkbogár","Fényes gyászcsíkbogár"}
Ilybius quadriguttatus	{"gyakori orsócsíkbogár","Gyakori orsócsíkbogár"}
Ilybius similis	{"sötét orsócsíkbogár","Sötét orsócsíkbogár"}
Ilybius subaeneus	{"fényes orsócsíkbogár","Fényes orsócsíkbogár"}
Ilybius subtilis	{"hegyesvállú gyászcsíkbogár","Hegyesvállú gyászcsíkbogár"}
Inachis io	{"nappali pávaszem","Nappali pávaszem"}
Inocellia crassicornis	{"közönséges tevenyakú","Közönséges tevenyakú"}
Iphiclides podalirius	{kardoslepke,Kardoslepke}
Ips mannsfeldi	{mannsfeld-szú,Mannsfeld-szú}
Isogenus nubecula	{"füstös álkérész","Füstös álkérész"}
Isophya kraussii	{krauss-tarsza,Krauss-tarsza}
Isotomus speciosus	{nyírfadarázscincér,Nyírfadarázscincér}
Ixobrychus minutus	{törpegém,Törpegém}
Jordanita graeca	{"görög fémlepke","Görög fémlepke"}
Judolia sexmaculata	{"hatsávos cincér","Hatsávos cincér"}
Jynx torquilla	{nyaktekercs,Nyaktekercs}
Laccobius bipunctatus	{"kétfoltos pocsolyacsibor","Kétfoltos pocsolyacsibor"}
Laccobius obscuratus	{"sötét pocsolyacsibor","Sötét pocsolyacsibor"}
Laccobius simulatrix	{"nagy pocsolyacsibor","Nagy pocsolyacsibor"}
Laccobius sinuatus	{"feketefejű pocsolyacsibor","Feketefejű pocsolyacsibor"}
Laccobius striatulus	{"termetes pocsolyacsibor","Termetes pocsolyacsibor"}
Laccobius syriacus	{"keleti pocsolyacsibor","Keleti pocsolyacsibor"}
Laccophilus hyalinus	{"cirpelő bukóbogár","Cirpelő bukóbogár"}
Laccophilus minutus	{"néma bukóbogár","Néma bukóbogár"}
Laccophilus poecilus	{"tarka bukóbogár","Tarka bukóbogár"}
Laccornis kocae	{"kis zömökcsíkbogár","Kis zömökcsíkbogár"}
Laccornis oblongus	{"nagy zömökcsíkbogár","Nagy zömökcsíkbogár"}
Lacerta agilis	{"fürge gyík","Fürge gyík"}
Lacerta viridis	{"zöld gyík","Zöld gyík"}
Laena reitteri	{reitter-avargyászbogár,Reitter-avargyászbogár}
Lamellocossus terebra	{nyárfarontólepke,Nyárfarontólepke}
Lamia textor	{takácscincér,Takácscincér}
Lamprotes c-aureum	{"c-betűs aranybagoly","C-betűs aranybagoly"}
Lanius collurio	{"tövisszúró gébics","Tövisszúró gébics"}
Lanius excubitor	{"nagy őrgébics","Nagy őrgébics"}
Lanius excubitor homeyeri	{"nagy őrgébics","Nagy őrgébics"}
Lanius minor	{"kis őrgébics","Kis őrgébics"}
Lanius senator	{"vörösfejű gébics","Vörösfejű gébics"}
Larentia clavaria	{"nagy mályvaaraszoló","Nagy mályvaaraszoló"}
Larinus beckeri	{becker-púderbogár,Becker-púderbogár}
Larus argentatus	{ezüstsirály,Ezüstsirály}
Larus canus	{viharsirály,Viharsirály}
Larus canus heinei	{viharsirály,Viharsirály}
Larus delawarensis	{"gyűrűscsőrű sirály","Gyűrűscsőrű sirály"}
Larus fuscus	{heringsirály,Heringsirály}
Larus fuscus heuglini	{heringsirály,Heringsirály}
Larus genei	{"vékonycsőrű sirály","Vékonycsőrű sirály"}
Larus glaucoides	{"sarki sirály","Sarki sirály"}
Larus hyperboreus	{"jeges sirály","Jeges sirály"}
Larus ichthyaetus	{halászsirály,Halászsirály}
Abies concolor	{"Kolorádói jegenyefenyő"}
Larus marinus	{"dolmányos sirály","Dolmányos sirály"}
Larus melanocephalus	{szerecsensirály,Szerecsensirály}
Larus minutus	{"kis sirály","Kis sirály"}
Larus pipixcan	{prérisirály,Prérisirály}
Larus ridibundus	{dankasirály,Dankasirály}
Larus sabini	{fecskesirály,Fecskesirály}
Leioderes kollari	{"vörösessárga juharcincér","Vörösessárga juharcincér"}
Leiopus nebulosus	{gesztcincér,Gesztcincér}
Leiopus punctulatus	{"feketemintás gesztcincér","Feketemintás gesztcincér"}
Leistus terminatus	{"vöröslő cirpelőfutó","vöröslő szívnyakúfutó"}
Lemonia dumi	{"sávos pohók","Sávos pohók"}
Lemonia taraxaci	{pitypangszövő,Pitypangszövő}
Leptacinus sulcifrons	{"barázdáshomlokú rovátkoltholyva","recés rovátkoltholyva"}
Leptidea morsei major	{"keleti mustárlepke","Keleti mustárlepke"}
Leptophyes boscii	{bosc-virágszöcske,Bosc-virágszöcske}
Leptura aethiops	{szerecsencincér,Szerecsencincér}
Leptura annularis	{"szalagos karcsúcincér",Völgycincér}
Leptura aurulenta	{"sárgaszőrű szalagoscincér","Sárgaszőrű szalagoscincér"}
Leptura quadrifasciata	{"feketeszőrű szalagoscincér","Feketeszőrű szalagoscincér"}
Lepturobosca virens	{"alhavasi virágcincér","Alhavasi virágcincér"}
Leucodonta bicoloria	{"aranyfoltos púposszövő","Aranyfoltos púposszövő"}
Libelloides macaronius	{"keleti rablópille","Keleti rablópille"}
Libythea celtis	{csőröslepke,Csőröslepke}
Lignyoptera fumidaria	{"füstös ősziaraszoló","Füstös ősziaraszoló"}
Abies pinsapo	{"Andalúziai jegenyefenyő"}
Limenitis camilla	{"kis lonclepke",Lonclepke}
Limenitis populi	{"nagy nyárfalepke","Nagy nyárfalepke"}
Limenitis reducta	{"kék lonclepke","Kék lonclepke"}
Limicola falcinellus	{sárjáró,Sárjáró}
Limnephilus elegans	{"elegáns mocsáritegzes","Elegáns mocsáritegzes"}
Limnius muelleri	{müller-karmosbogár,Müller-karmosbogár}
Limnius perrisi	{perris-karmosbogár,Perris-karmosbogár}
Limnodromus scolopaceus	{"hosszúcsőrű cankógoda","Hosszúcsőrű cankógoda"}
Limnoxenus niger	{"fekete csibor","Fekete csibor"}
Limosa lapponica	{"kis goda","Kis goda"}
Limosa limosa	{"nagy goda","Nagy goda"}
Lioderina linearis	{mandulacincér,Mandulacincér}
Lixus apfelbecki	{apfelbeck-dudvabarkó,Apfelbeck-dudvabarkó}
Locustella fluviatilis	{"berki tücsökmadár","Berki tücsökmadár"}
Locustella luscinioides	{"nádi tücsökmadár","Nádi tücsökmadár"}
Locustella naevia	{"réti tücsökmadár","Réti tücsökmadár"}
Lopinga achine	{"sápadt szemeslepke","Sápadt szemeslepke"}
Loxia curvirostra	{keresztcsőrű,Keresztcsőrű}
Loxia leucoptera	{"szalagos keresztcsőrű","Szalagos keresztcsőrű"}
Loxia leucoptera bifasciata	{"szalagos keresztcsőrű","Szalagos keresztcsőrű"}
Lullula arborea	{"erdei pacsirta","Erdei pacsirta"}
Luscinia luscinia	{"nagy fülemüle","Nagy fülemüle"}
Luscinia megarhynchos	{fülemüle,Fülemüle}
Luscinia svecica	{kékbegy,Kékbegy}
Luscinia svecica cyanecula	{kékbegy,Kékbegy}
Lutra lutra	{"közönséges vidra",vidra}
Lycaena alciphron	{"ibolyás tűzlepke","Ibolyás tűzlepke"}
Lycaena dispar rutila	{"nagy tűzlepke","Nagy tűzlepke"}
Lycaena helle	{"lápi tűzlepke","Lápi tűzlepke"}
Lycaena hippothoe	{"havasi tűzlepke","Havasi tűzlepke"}
Lycaena thersamon	{"kis tűzlepke","Kis tűzlepke"}
Picea pungens	{Ezüstfenyő,"Szúrós fenyő"}
Lymnocryptes minimus	{"kis sárszalonka","Kis sárszalonka"}
Lynx lynx	{hiúz,"közönséges hiúz"}
Macronemurus bilineatus	{"kétsávos hangyaleső","Kétsávos hangyaleső"}
Picea omorika	{"Szerb luc"}
Mantispa styriaca	{"Fogolábú fátyolka","kétszínű fogólábú-fátyolka"}
Mantis religiosa	{imádkozósáska,"imádkozó sáska / ájtatos manó"}
Marmaronetta angustirostris	{"márványos réce","Márványos réce"}
Marumba quercus	{tölgyfaszender,Tölgyfaszender}
Mecinus pirazzolii	{pirazzoli-ormányos,Pirazzoli-ormányos}
Megalomus tortricoides	{"nyári törpefátyolka","Nyári törpefátyolka"}
Megarthrus depressus	{"kerekhátú sutaholyva","szögleteshátú sutaholyva"}
Picea orientalis	{"Keleti luc"}
Megistopus flavicornis	{"kétfoltos hangyaleső","Kétfoltos hangyaleső"}
Melampophylax nepos	{"kárpáti forrástegzes","Kárpáti forrástegzes"}
Melanitta fusca	{"füstös réce","Füstös réce"}
Melanitta nigra	{"fekete réce","Fekete réce"}
Melanocorypha calandra	{kalandrapacsirta,Kalandrapacsirta}
Meligethes buyssoni	{buysson-fénybogár,Buysson-fénybogár}
Meligethes jelineki	{jelínek-fénybogár,Jelínek-fénybogár}
Meligethes kunzei	{kunze-fénybogár,Kunze-fénybogár}
Meligethes pedicularius	{kenderkefű-fénybogár,"perzsa fénybogár"}
Meloe cicatricosus	{óriás-nünüke,óriásnünüke}
Meloe decorus	{"csinos nünüke","díszes nünüke"}
Meloe rufiventris	{"fekete nünüke","vöröshasú nünüke"}
Menesia bipunctata	{"kétpettyes kutyabengecincér","Kétpettyes kutyabengecincér"}
Mergus merganser	{"nagy bukó","Nagy bukó"}
Mergus serrator	{"örvös bukó","Örvös bukó"}
Merodon equestris	{"nagy nárciszlégy","Nagy nárciszlégy"}
Merops apiaster	{gyurgyalag,Gyurgyalag}
Mesosa curculionoides	{"szemfoltos cincér","Szemfoltos cincér"}
Mesosa nebulosa	{"ködfoltos cincér","Ködfoltos cincér"}
Mesotrosta signalis	{"fehérjegyű törpebagoly","Fehérjegyű törpebagoly"}
Cedrus atlantica	{Atlasz-cédrus}
Metrioptera roeselii	{roesel-szöcske,Roesel-szöcske}
Micromus angulatus	{"v-foltos barnafátyolka","V-foltos barnafátyolka"}
Micromus paganus	{"sárga törpefátyolka","Sárga törpefátyolka"}
Micromus variegatus	{"foltosszárnyú törpefátyolka","Foltosszárnyú törpefátyolka"}
Microon sahlbergi	{sahlberg-tócshúrormányos,Sahlberg-tócshúrormányos}
Microrhagus palmi	{palm-tövisnyakúbo,Palm-tövisnyakúbo}
Milvus migrans	{"barna kánya","Barna kánya"}
Milvus milvus	{"vörös kánya","Vörös kánya"}
Misgurnus fossilis	{"réti csík",réticsík}
Mogulones aubei	{aube-ormányos,Aube-ormányos}
Mogulones diecki	{dieck-ormányos,Dieck-ormányos}
Molorchus minor	{"kis légycincér","Kis légycincér"}
Cedrus deodara	{"Himalájai cédrus"}
Monochamus galloprovincialis pistor	{"foltos fenyvescincér","Foltos fenyvescincér"}
Monochamus rosenmuelleri	{"keleti fenyvescincér","Keleti fenyvescincér"}
Monochamus saltuarius	{"bársonyos fenyvescincér","Bársonyos fenyvescincér"}
Monochamus sartor	{"nagy fenyvescincér","Nagy fenyvescincér"}
Monochamus sutor	{"kis fenyvescincér","Kis fenyvescincér"}
Monticola saxatilis	{kövirigó,Kövirigó}
Montifringilla nivalis	{havasipinty,Havasipinty}
Motacilla alba	{barázdabillegető,Barázdabillegető}
Motacilla cinerea	{"hegyi billegető","Hegyi billegető"}
Motacilla citreola	{citrombillegető,Citrombillegető}
Motacilla citreola werae	{citrombillegető,Citrombillegető}
Motacilla flava	{"sárga billegető","Sárga billegető"}
Motacilla flava feldegg	{"kucsmás billegető","Kucsmás billegető"}
Motacilla flava thunbergi	{"sárga billegető","Sárga billegető"}
Musaria affinis	{"feketefejű cincér","Feketefejű cincér"}
Musaria argus	{"árgusszemű cincér","Árgusszemű cincér"}
Muscicapa striata	{"szürke légykapó","Szürke légykapó"}
Myathropa florea	{"halálfejes odulégy","Halálfejes odulégy"}
Mycetochara roubali	{roubal-taplász,Roubal-taplász}
Cedrus libani	{"Libanoni cédrus"}
Mycetoporus brucki	{"avarlakó gombászholyva","hegyvidéki gombászholyva"}
Myotis nattereri	{"horgasszőrű denevér","horgaszőrű denevér"}
Myrmecaelurus trigrammus	{"sárga hangyafarkas","Sárga hangyafarkas"}
Myrmeleon bore	{"északi hangyaleső","Északi hangyaleső"}
Myrmeleon formicarius	{"erdei hangyaleső","Közönséges hangyaleső"}
Myrmoecia perezi	{perez-hangyászholyva,Perez-hangyászholyva}
Pinus mugo	{Törpefenyő}
Nanomimus circumscriptus	{"háromszöges füzényormányos","nagy füzényormányos"}
Nanomimus hemisphaericus	{"háromszöges füzényormányos","szőrszálas füzényormányos"}
Nanophyes brevis	{"gömbölyded füzényormányos","szőrszálas füzényormányos"}
Nanophyes globiformis	{"gömbölyded füzényormányos","pöttyös tócsahúr-ormányos"}
Nanophyes globulus	{"pöttyös tócsahúr-ormányos","simacombú füzényormányos"}
Nathrius brevipennis	{kosárcincér,Kosárcincér}
Natrix natrix	{vízisikló,Vízisikló}
Natrix tessellata	{"kockás sikló","Kockás sikló"}
Nebrioporus canaliculatus	{"sárgás patakcsíkbogár","Sárgás patakcsíkbogár"}
Nebrioporus depressus	{"karcsú patakcsíkbogár","Karcsú patakcsíkbogár"}
Nebrioporus elegans	{"díszes patakcsíkbogár","Díszes patakcsíkbogár"}
Necydalis major	{"nagy fürkészcincér","Nagy fürkészcincér"}
Necydalis ulmi	{"aranyszőrű fürkészcincér","Aranyszőrű fürkészcincér"}
Pinus strobus	{Simafenyő}
Nemoura cinerea	{"közönséges álkérész","Közönséges álkérész"}
Neoclytus acuminatus	{"amerikai darázscincér","Amerikai darázscincér"}
Neodorcadion bilineatum	{"kétsávos földicincér","Kétsávos földicincér"}
Neogobius kessleri	{kessler-géb,Kessler-géb}
Neogobius syrman	{szirman-géb,Szirman-géb}
Neophron percnopterus	{dögkeselyű,Dögkeselyű}
Nephus horioni	{horion-törpebödice,Horion-törpebödice}
Nephus ulbrichi	{ulbrich-törpebödice,Ulbrich-törpebödice}
Neptis rivularis	{"nagy fehérsávoslepke","Nagy fehérsávoslepke"}
Neptis sappho	{"kis fehérsávoslepke","Kis fehérsávoslepke"}
Netta rufina	{"üstökös réce",Üstökösréce}
Neuroleon nemausiensis	{"kis hangyaleső","Kis hangyaleső"}
Pinus wallichiana	{"Himalájai selyemfenyő"}
Nineta flava	{"sárga fátyolka","Sárga fátyolka"}
Nivellia sanguinosa	{"vérvörös virágcincér","Vérvörös virágcincér"}
Noterus clavicornis	{"szélescsápú merülőbogár","Szélescsápú merülőbogár"}
Noterus crassicornis	{"keskenycsápú merülőbogár","Keskenycsápú merülőbogár"}
Nucifraga caryocatactes	{fenyőszajkó,Fenyőszajkó}
Nucifraga caryocatactes macrorhynchos	{fenyőszajkó,Fenyőszajkó}
Nudaria mundana	{"csupasz medvelepke","Csupasz medvelepke"}
Numenius arquata	{"nagy póling","Nagy póling"}
Numenius arquata orientalis	{"nagy póling","Nagy póling"}
Numenius phaeopus	{"kis póling","Kis póling"}
Numenius tenuirostris	{"vékonycsőrű póling","Vékonycsőrű póling"}
Nyctalus lasiopterus	{"óriás koraidenevér",óriás-koraidenevér}
Nycticorax nycticorax	{bakcsó,Bakcsó}
Nymphalis antiopa	{gyászlepke,Gyászlepke}
Nymphalis polychloros	{"nagy rókalepke","Nagy rókalepke"}
Nymphalis vaualbum	{"l-betűs rókalepke","L-betűs rókalepke"}
Nymphalis xanthomelas	{"vörös rókalepke","Vörös rókalepke"}
Oberea erythrocephala	{"pirosfejű kutyatejcincér","Pirosfejű kutyatejcincér"}
Oberea euphorbiae	{"magyar kutyatejcincér","Magyar kutyatejcincér"}
Oberea linearis	{mogyorócincér,Mogyorócincér}
Oberea moravica	{"közepes kutyatejcincér","Közepes kutyatejcincér"}
Oberea oculata	{"vörösnyakú fűzcincér","Vörösnyakú fűzcincér"}
Oberea pedemontana	{varjútöviscincér,Varjútöviscincér}
Oberea pupillata	{lonccincér,Lonccincér}
Obrium brunneum	{"törpe hengercincér","Törpe hengercincér"}
Obrium cantharinum	{"nyárfa hengercincér","Nyárfa hengercincér"}
Ochlerotatus annulipes	{"balatoni szúnyog","Balatoni szúnyog"}
Ochlerotatus cantans	{"erdei szúnyog","Erdei szúnyog"}
Ochlerotatus caspius	{"aranyló szúnyog","Aranyló szúnyog"}
Ochlerotatus cataphylla	{"gyakori tavasziszúnyog","Gyakori tavasziszúnyog"}
Ochlerotatus dorsalis	{"sziki szúnyog","Sziki szúnyog"}
Ochlerotatus flavescens	{"sárga szúnyog","Sárga szúnyog"}
Ochlerotatus geniculatus	{"díszes szúnyog","Díszes szúnyog"}
Ochlerotatus hungaricus	{"magyar szúnyog","Magyar szúnyog"}
Ochlerotatus sticticus	{"oldalfoltos szúnyog","Oldalfoltos szúnyog"}
Ocnogyna parasita	{"csonkaszárnyú medvelepke","Csonkaszárnyú medvelepke"}
Pseudotsuga menziesii	{Duglászfenyő}
Odontognophos dumetata	{"csücskös sziklaaraszoló","Csücskös sziklaaraszoló"}
Odontopodisma schmidtii	{schmidt-hegyisáska,Schmidt-hegyisáska}
Odontosia carmelita	{barátka-púposszövő,Barátka-púposszövő}
Oenanthe deserti	{"sivatagi hantmadár","Sivatagi hantmadár"}
Oenanthe hispanica	{"déli hantmadár","Déli hantmadár"}
Oenanthe hispanica melanoleuca	{"déli hantmadár","Déli hantmadár"}
Oenanthe isabellina	{"pusztai hantmadár","Pusztai hantmadár"}
Oenanthe oenanthe	{hantmadár,Hantmadár}
Oenanthe pleschanka	{apácahantmadár,Apácahantmadár}
Oligomerus retowskii	{retowski-álszú,Retowski-álszú}
Oligoneuriella rhenana	{"Denevérszárnyú kérész, Rajnai denevérszárnyú kérész","rajnai denevérszárnyú-kérész"}
Oligotricha striata	{"lomha lápipozdorján","Lomha lápipozdorján"}
Omphalapion hookerorum	{hooker-cickányormányos,Hooker-cickányormányos}
Onyxacalles luigionii	{luigioni-zártormányúbogár,Luigioni-zártormányúbogár}
Oplosia cinerea	{"foltos hárscincér","Foltos hárscincér"}
Opsilia coerulescens	{kígyósziszcincér,Kígyósziszcincér}
Opsilia molybdaena	{"fémkék dudvacincér","Fémkék dudvacincér"}
Opsilia uncinata	{szeplőlapucincér,Szeplőlapucincér}
Orectochilus villosus	{"szőrös keringőbogár","Szőrös keringőbogár"}
Oria musculosa	{"szalmasárga búzabagoly","Szalmasárga búzabagoly"}
Oriolus oriolus	{sárgarigó,Sárgarigó}
Oryctes nasicornis	{"orrszarvú bogár",orrszarvúbogár}
Ostrinia palustralis	{"mocsári tűzmoly","Mocsári tűzmoly"}
Otiorhynchus frescati	{frescat-gyalogormányos,Frescat-gyalogormányos}
Otiorhynchus kollari	{kollar-gyalogormányos,Kollar-gyalogormányos}
Otiorhynchus reichei	{reiche-gyalogormányos,Reiche-gyalogormányos}
Otiorhynchus winkleri	{winkler-gyalogormányos,Winkler-gyalogormányos}
Otis tarda	{túzok,Túzok}
Otus scops	{füleskuvik,Füleskuvik}
Oxymirus cursor	{futócincér,Futócincér}
Oxytripia orbiculosa	{"nagyfoltú bagoly","Nagyfoltú bagoly"}
Oxyura jamaicensis	{"halcsontfarkú réce","Halcsontfarkú réce"}
Oxyura leucocephala	{"kékcsőrű réce","Kékcsőrű réce"}
Pachyta lamed	{"fakó cserjecincér","Fakó cserjecincér"}
Pachyta quadrimaculata	{bajnócacincér,Bajnócacincér}
Pachytodes cerambyciformis	{"változékony virágcincér","Változékony virágcincér"}
Pachytodes erraticus	{"rajzos virágcincér","Rajzos virágcincér"}
Paidia rica	{májmoha-medvelepke,Májmoha-medvelepke}
Palingenia longicauda	{tiszavirág,Tiszavirág}
Palmitia massilialis	{"cifra fényilonca","Cifra fényilonca"}
Palpares libelluloides	{"balkáni hangyaleső","Balkáni hangyaleső"}
Pammene querceti	{"magyar tölgymakkmoly","Magyar tölgymakkmoly"}
Panchrysia deaurata	{Aranybagoly,"pompás aranybagoly"}
Pandion haliaetus	{halászsas,Halászsas}
Panurus biarmicus	{barkóscinege,Barkóscinege}
Panurus biarmicus russicus	{barkóscinege,Barkóscinege}
Papilio machaon	{"fecskefarkú lepke","Fecskefarkú lepke"}
Paraboarmia viertlii	{"magyar faaraszoló","Magyar faaraszoló"}
Paracaloptenus caloptenoides	{ál-olaszsáska,álolaszsáska}
Paracorymbia fulva	{"vörhenyes virágcincér","Vörhenyes virágcincér"}
Paracorymbia maculicornis	{"tarkacsápú virágcincér","Tarkacsápú virágcincér"}
Parainocellia braueri	{"déli kurta-tevenyakú","Déli kurta-tevenyakú"}
Paraleptophlebia submarginata	{"vékonyerű kérész","Vékonyerű kérész"}
Parasemia plantaginis	{útifű-medvelepke,Útifű-medvelepke}
Parexarnis fugax	{"pusztai földibagoly","Pusztai földibagoly"}
Parhelophilus versicolor	{"tarka iszaplégy","Tarka iszaplégy"}
Parnassius apollo	{"nagy apollólepke","Nagy apollólepke"}
Parnassius mnemosyne	{"kis apollólepke","Kis apollólepke"}
Parus ater	{fenyvescinege,Fenyvescinege}
Parus cristatus	{"búbos cinege","Búbos cinege"}
Parus major	{széncinege,Széncinege}
Parus montanus	{"kormosfejű cinege","Kormosfejű cinege"}
Parus montanus borealis	{"kormosfejű cinege","Kormosfejű cinege"}
Parus palustris	{barátcinege,Barátcinege}
Parus palustris stagnatilis	{barátcinege,Barátcinege}
Passer domesticus	{"házi veréb","Házi veréb"}
Passer montanus	{"mezei veréb","Mezei veréb"}
Pedestredorcadion decipiens	{"homoki gyalogcincér","Homoki gyalogcincér"}
Pedestredorcadion pedestre	{"kétsávos gyalogcincér","Kétsávos gyalogcincér"}
Pedestredorcadion scopolii	{"nyolcsávos gyalogcincér","Nyolcsávos gyalogcincér"}
Pedostrangalia revestita	{"kétszínű karcsúcincér","Kétszínű karcsúcincér"}
Pelecanus crispus	{"borzas gödény","Borzas gödény"}
Pelecanus onocrotalus	{"rózsás gödény","Rózsás gödény"}
Pelecanus rufescens	{"vörhenyes gödény","Vörhenyes gödény"}
Pelobates fuscus	{"barna ásóbéka","Barna ásóbéka"}
Peltodytes caesus	{"zömök víztaposó","Zömök víztaposó"}
Perapion lemoroi	{lemoro-cickányormányos,Lemoro-cickányormányos}
Perconia strigillaria	{"fehérszárnyú aranyaraszoló","Fehérszárnyú aranyaraszoló"}
Perdix perdix	{fogoly,Fogoly}
Peribatodes umbraria	{fagyal-faaraszoló,Fagyal-faaraszoló}
Pericallia matronula	{óriás-medvelepke,Óriás-medvelepke}
Periphanes delphinii	{szarkalábbagoly,Szarkalábbagoly}
Perla burmeisteriana	{"burmeister álkérész","Burmeister álkérész"}
Perla marginata	{"hatalmas álkérész","Hatalmas álkérész"}
Pernis apivorus	{darázsölyv,Darázsölyv}
Phaenops formaneki	{formanek-fürgedíszbogár,Formanek-fürgedíszbogár}
Phaeostigma major	{"óriás tevenyakú","Óriás tevenyakú"}
Phaeostigma notata	{"barna tevenyakú","Barna tevenyakú"}
Phalacrocorax carbo	{kárókatona,Kárókatona}
Phalacrocorax carbo sinensis	{kárókatona,Kárókatona}
Phalacrocorax pygmeus	{"kis kárókatona","Kis kárókatona"}
Phalaropus fulicarius	{"laposcsőrű víztaposó","Laposcsőrű víztaposó"}
Phalaropus lobatus	{"vékonycsőrű víztaposó","Vékonycsőrű víztaposó"}
Parus caeruleus	{"kék cinege","Kék cinege",kékcinke}
Pharmacis fusconebulosa	{"északi gyökérrágólepke","Északi gyökérrágólepke"}
Phasianus colchicus	{fácán,Fácán}
Phasianus colchicus torquatus	{fácán,Fácán}
Pheosia gnoma	{nyírfa-púposszövő,Nyírfa-púposszövő}
Philomachus pugnax	{pajzsoscankó,Pajzsoscankó}
Philonthus carbonarius	{"bronzos ganajholyva","réti ganajholyva"}
Phloeopora corticalis	{"közönséges szúvadászholyva","tölgyes szúvadászholyva"}
Phoenicopterus minor	{"kis flamingó","Kis flamingó"}
Phoenicopterus ruber	{"rózsás flamingó","Rózsás flamingó"}
Phoenicurus ochruros	{"házi rozsdafarkú","Házi rozsdafarkú"}
Phoenicurus ochruros gibraltariensis	{"házi rozsdafarkú","Házi rozsdafarkú"}
Phoenicurus phoenicurus	{"kerti rozsdafarkú","Kerti rozsdafarkú"}
Pholidoptera littoralis	{"bujkáló avarszöcske","bújkáló avarszöcske"}
Pholidoptera transsylvanica	{"erdélyi avarszöcske","erdélyi kurtaszárnyú szöcske"}
Photedes captiuncula delattini	{"hegyi törpebagoly","Hegyi törpebagoly"}
Phragmatiphila nexa	{"Erdei nádibagoly","lángszínű nádibagoly"}
Phyllometra culminaria	{csüngőaraszoló,Csüngőaraszoló}
Phylloscopus collybita	{csilpcsalpfüzike,Csilpcsalpfüzike}
Phylloscopus collybita abietinus	{csilpcsalpfüzike,Csilpcsalpfüzike}
Phylloscopus collybita tristis	{csilpcsalpfüzike,Csilpcsalpfüzike}
Phylloscopus fuscatus	{"barna füzike","Barna füzike"}
Phylloscopus inornatus	{vándorfüzike,Vándorfüzike}
Phylloscopus proregulus	{királyfüzike,Királyfüzike}
Phylloscopus schwarzi	{"vastagcsőrű füzike","Vastagcsőrű füzike"}
Phylloscopus sibilatrix	{"sisegő füzike","Sisegő füzike"}
Phylloscopus trochilus	{fitiszfüzike,Fitiszfüzike}
Ballota nigra	{"fekete peszterce",Peszterce}
Phylloscopus trochilus acredula	{fitiszfüzike,Fitiszfüzike}
Phymatodes testaceus	{"változékony korongcincér","Változékony korongcincér"}
Phytoecia cylindrica	{medvelapucincér,Medvelapucincér}
Phytoecia icterica	{murokcincér,Murokcincér}
Phytoecia nigricornis	{ürömcincér,Ürömcincér}
Phytoecia pustulata	{"parányi fűcincér","Parányi fűcincér"}
Phytoecia virgula	{"pirospontos fűcincér","Pirospontos fűcincér"}
Pica pica	{szarka,Szarka}
Picus canus	{"hamvas küllő","Hamvas küllő"}
Picus viridis	{"zöld küllő","Zöld küllő"}
Pidonia lurida	{"hegyi cserjecincér","Hegyi cserjecincér"}
Pieris bryoniae marani	{"hegyi fehérlepke","Hegyi fehérlepke"}
Pieris ergane	{"sziklai fehérlepke","Sziklai fehérlepke"}
Pilemia hirsutula	{macskaherecincér,Macskaherecincér}
Pilemia tigrina	{atracélcincér,Atracélcincér}
Pinicola enucleator	{"nagy pirók","Nagy pirók"}
Plagionotus arcuatus	{"bársonyos darázscincér","Bársonyos darázscincér"}
Plagionotus detritus	{"sárgafarú darázscincér","Sárgafarú darázscincér"}
Plagionotus floralis	{lucernacincér,Lucernacincér}
Platalea leucorodia	{kanalasgém,Kanalasgém}
Platambus maculatus	{"tarka csíkbogár","Tarka csíkbogár"}
Platyphylax frauenfeldi	{"drávai tegzes","Drávai tegzes"}
Plebeius idas	{"északi boglárka","Északi boglárka"}
Plebeius sephirus	{"fóti boglárka","Fóti boglárka"}
Plecotus auritus	{"barna hosszúfülű-denevér","barna hosszúfülűdenevér"}
Plecotus austriacus	{"szürke hosszúfülű-denevér","szürke hosszúfülűdenevér"}
Plectrocnemia minima	{"balkáni pálcástegzes","Balkáni pálcástegzes"}
Plectrophenax nivalis	{hósármány,Hósármány}
Plectrophenax nivalis vlasowae	{hósármány,Hósármány}
Plegadis falcinellus	{batla,Batla}
Pluvialis apricaria	{aranylile,Aranylile}
Pluvialis fulva	{"ázsiai pettyeslile","Ázsiai pettyeslile"}
Pluvialis squatarola	{ezüstlile,Ezüstlile}
Podarcis muralis	{"fali gyík","Fali gyík"}
Podarcis taurica	{"homoki gyík","Homoki gyík"}
Podiceps auritus	{"füles vöcsök","Füles vöcsök"}
Podiceps cristatus	{"búbos vöcsök","Búbos vöcsök"}
Podiceps grisegena	{"vörösnyakú vöcsök","Vörösnyakú vöcsök"}
Podiceps nigricollis	{"feketenyakú vöcsök","Feketenyakú vöcsök"}
Poecilimon intermedius	{"keleti pókszöcske","sztyeppei pókszöcske"}
Poecilium alni	{"apró háncscincér","Apró háncscincér"}
Poecilium fasciatum	{szőlőcincér,Szőlőcincér}
Poecilium glabratum	{borókaháncscincér,Borókaháncscincér}
Poecilium puncticolle	{"vörösbarna háncscincér","Vörösbarna háncscincér"}
Poecilium pusillum	{"vállfoltos háncscincér","Vállfoltos háncscincér"}
Poecilium rufipes	{"kék háncscincér","Kék háncscincér"}
Pogonocherus decoratus	{"díszes ecsetcincér","Díszes ecsetcincér"}
Pogonocherus fasciculatus	{"árva ecsetcincér","Árva ecsetcincér"}
Pogonocherus hispidulus	{"négytövises ecsetcincér","Négytövises ecsetcincér"}
Pogonocherus hispidus	{"kéttövises ecsetcincér","Kéttövises ecsetcincér"}
Pogonocherus ovatus	{pettyesvégűecsetcincér,Pettyesvégűecsetcincér}
Polychrysia moneta	{"szélesszárnyú aranybagoly","Szélesszárnyú aranybagoly"}
Polygonia c-album	{"c-betűs lepke","C-betűs lepke"}
Polymixis rufocincta isolata	{"villányi télibagoly","Villányi télibagoly"}
Polyommatus admetus	{"Barnabundás boglárka","bundás boglárka"}
Polyommatus amandus	{"csillogó boglárka","Csillogó boglárka"}
Polyommatus damon	{"csíkos boglárka","Csíkos boglárka"}
Polypogon gryphalis	{"láperdei karcsúbagoly","Láperdei karcsúbagoly"}
Poophagus hopffgarteni	{hopffgarten-kányafűormányos,Hopffgarten-kányafűormányos}
Porhydrus lineatus	{"csíkos selymescsíkbogár","Csíkos selymescsíkbogár"}
Porhydrus obliquesignatus	{"foltos selymescsíkbogár","Foltos selymescsíkbogár"}
Porphyrio porphyrio	{"kék fú","Kék fú"}
Porphyrio porphyrio seistanicus	{"kék fú","Kék fú"}
Porzana parva	{"kis vízicsibe","Kis vízicsibe"}
Porzana porzana	{"pettyes vízicsibe","Pettyes vízicsibe"}
Porzana pusilla	{törpevízicsibe,Törpevízicsibe}
Porzana pusilla intermedia	{törpevízicsibe,Törpevízicsibe}
Potamanthus luteus	{folyamvirág,Folyamvirág}
Prionus coriarius	{"hegedülő csercincér","Hegedülő csercincér"}
Pronocera angusta	{"keskeny luccincér","Keskeny luccincér"}
Proserpinus proserpina	{törpeszender,Törpeszender}
Prostomis mandibularis	{"európai fogasállúbogár",fogasállúbogár}
Protapion schoenherri	{schönherr-cickányormányos,Schönherr-cickányormányos}
Protolampra sobrina	{csarabos-földibagoly,Csarabos-földibagoly}
Prunella collaris	{"havasi szürkebegy","Havasi szürkebegy"}
Prunella modularis	{"erdei szürkebegy","Erdei szürkebegy"}
Juniperus sabina	{"Nehézszagú pikkelyboróka"}
Pseudomyllocerus magnanoi	{magnano-ormányos,Magnano-ormányos}
Pseudopodisma fieberi	{fiber-hegyisáska,Fiber-hegyisáska}
Pseudopodisma nagyi	{nagy-hegyisáska,Nagy-hegyisáska}
Pseudorchestes ermischi	{ermisch-bolhaormányos,Ermisch-bolhaormányos}
Pseudorchestes horioni	{horioni-bolhaormányos,Horioni-bolhaormányos}
Pseudorchestes kostali	{kostál-bolhaormányos,Kostál-bolhaormányos}
Pseudorchestes smreczynskii	{smreczynski-bolhaormányos,Smreczynski-bolhaormányos}
Pseudovadonia livida pecta	{"barnás virágcincér","Barnás virágcincér"}
Pterocles exustus	{"barnahasú pusztaityúk","Barnahasú pusztaityúk"}
Purpuricenus budensis	{bíborcincér,Bíborcincér}
Purpuricenus globulicollis	{"kerekpajzsú vércincér","Kerekpajzsú vércincér"}
Purpuricenus kaehleri	{"hosszúcsápú vércincér",Vércincér}
Pyrrhia purpurina	{ezerjófűbagoly,Ezerjófűbagoly}
Pyrrhidium sanguineum	{"tűzpiros facincér","Tűzpiros facincér"}
Pyrrhocorax graculus	{"havasi csóka","Havasi csóka"}
Pyrrhocorax pyrrhocorax	{"havasi varjú","Havasi varjú"}
Pyrrhocorax pyrrhocorax erythrorhamphus	{"havasi varjú","Havasi varjú"}
Pyrrhula pyrrhula	{süvöltő,Süvöltő}
Rallus aquaticus	{guvat,Guvat}
Rana arvalis	{"mocsári béka","Mocsári béka"}
Rana arvalis wolterstorffi	{"hosszúlábú mocsári béka","Hosszúlábú mocsári béka"}
Rana dalmatina	{"erdei béka","Erdei béka"}
Rana lessonae	{"kis tavibéka","Kis tavibéka"}
Rana temporaria	{"gyepi béka","Gyepi béka"}
Raphidia ophiopsis	{"tevenyakú fátyolka","Tevenyakú fátyolka"}
Raphidia ophiopsis mediterranea	{"tevenyakú fátyolka","Tevenyakú fátyolka"}
Recurvirostra avosetta	{gulipán,Gulipán}
Regulus ignicapillus	{"tüzesfejű királyka","Tüzesfejű királyka"}
Regulus regulus	{"sárgafejű királyka","Sárgafejű királyka"}
Reitterelater bouyoni	{bouyon-pattanó,Bouyon-pattanó}
Remiz pendulinus	{függőcinege,Függőcinege}
Rhacopus sahlbergi	{"sahlberg- tövisnyakúbogár","Sahlberg- tövisnyakúbogár"}
Rhagium bifasciatum	{"kétcsíkos tövisescincér","Kétcsíkos tövisescincér"}
Rhagium inquisitor	{"fenyves tövisescincér","Fenyves tövisescincér"}
Rhagium mordax	{csertövisescincér,Csertövisescincér}
Rhagium sycophanta	{tölgyes-tövisescincér,Tölgyes-tövisescincér}
Rhamnusium bicolor	{"kétszínű nyárfacincér","Kétszínű nyárfacincér"}
Rhantus bistriatus	{"sávosnyakú particsíkbogár","Sávosnyakú particsíkbogár"}
Rhantus consputus	{"lapos particsíkbogár","Lapos particsíkbogár"}
Rhantus frontalis	{"sárgamellű particsíkbogár","Sárgamellű particsíkbogár"}
Rhantus grapii	{"fekete particsíkbogár","Fekete particsíkbogár"}
Rhantus latitans	{"sárgahasú particsíkbogár","Sárgahasú particsíkbogár"}
Rhantus suturalis	{"gyakori particsíkbogár","Gyakori particsíkbogár"}
Rhantus suturellus	{"északi particsíkbogár","Északi particsíkbogár"}
Juniperus chinensis	{"Kínai pikkelyboróka"}
Rhinomias viertli	{viertl-duzzadtorrú-ormányos,Viertl-duzzadtorrú-ormányos}
Rhinusa smreczynskii	{smreczynski-gyujtoványfű-ormányos,Smreczynski-gyujtoványfű-ormányos}
Rhithrogena semicolorata	{pehelykérész,Pehelykérész}
Rhyacophila hirticornis	{"márványos örvénytegzes","Márványos örvénytegzes"}
Rhysodes sulcatus	{"kerekvállú állasbogár","Kerekvállú állasbogár"}
Rhyssa persuasoria	{"óriás fenyőfürkész",óriás-fenyőfürkész}
Rileyiana fovea	{zörgőbagoly,Zörgőbagoly}
Riparia riparia	{partifecske,Partifecske}
Rissa tridactyla	{csüllő,Csüllő}
Juniperus virginiana	{"Virginiai pikkelyboróka"}
Ropalopus clavipes	{"feketelábú facincér","Feketelábú facincér"}
Ropalopus femoratus	{"vékonycsápú facincér","Vékonycsápú facincér"}
Ropalopus insubricus	{"kékeszöld facincér","Kék-zöld facincér"}
Ropalopus macropus	{"kis facincér","Kis facincér"}
Ropalopus ungaricus	{"magyar facincér","Magyar facincér"}
Ropalopus varini	{"vöröscombú facincér","Vöröscombú facincér"}
Rosalia alpina	{"havasi cincér","Havasi cincér"}
Rutilus frisii	{"fekete-tengeri koncér","gyöngyös koncér"}
Rutpela maculata	{"tarkacsápú karcsúcincér","Tarkacsápú karcsúcincér"}
Salamandra salamandra	{"foltos szalamandra","Foltos szalamandra"}
Saperda carcharias	{"nagy nyárfacincér","Nagy nyárfacincér"}
Saperda octopunctata	{"nyolcpontos cincér","Nyolcpontos nyárfacincér"}
Saperda perforata	{"díszes nyárfacincér","Díszes nyárfacincér"}
Saperda populnea	{"kis nyárfacincér","Kis nyárfacincér"}
Saperda punctata	{"pettyes szilcincér","Pettyes szilcincér"}
Saperda scalaris	{létracincér,Létracincér}
Saperda similis	{kecskefűzcincér,Kecskefűzcincér}
Saphanus piceus	{"szurok cincér","Szurok cincér"}
Saragossa porosa kenderesiensis	{"sziki ürömbagoly","Sziki ürömbagoly"}
Saturnia pavonia	{"kis pávaszem","Kis pávaszem"}
Saturnia pyri	{"nagy pávaszem","Nagy pávaszem"}
Saturnia spini	{"közepes pávaszem","Közepes pávaszem"}
Satyrium ilicis	{tölgyfa-csücsköslepke,Tölgyfa-csücsköslepke}
Satyrium w-album	{szilfa-csücsköslepke,Szilfa-csücsköslepke}
Saxicola rubetra	{"rozsdás csuk","Rozsdás csuk"}
Scarodytes halensis	{zebracsíkbogár,Zebracsíkbogár}
Schinia cardui	{keserűgyökér-nappalibagoly,Keserűgyökér-nappalibagoly}
Schinia cognata	{nyúlparéj-nappalibagoly,Nyúlparéj-nappalibagoly}
Schistostege decussata	{"hálós rétiaraszoló","Hálós rétiaraszoló"}
Sciurus vulgaris	{mókus,"vörös mókus"}
Scolopax rusticola	{"erdei szalonka","Erdei szalonka"}
Scolytus kirschii	{kirsch-kéregszú,Kirsch-kéregszú}
Scolytus koenigi	{kőnig-kéregszú,Kőnig-kéregszú}
Thuja occidentalis	{"Nyugati tuja"}
Semanotus russicus	{borókacincér,Borókacincér}
Semanotus undatus	{"hullámos fenyőcincér","Hullámos fenyőcincér"}
Semidalis aleyrodiformis	{"lisztes fátyolka","Lisztes fátyolka"}
Serinus serinus	{csicsörke,Csicsörke}
Sialis lutaria	{"vízi recésfátyolka","Vízi recésfátyolka"}
Sideridis implexa	{"ázsiai szegfűbagoly","Ázsiai szegfűbagoly"}
Siphlonurus lacustris	{"tavi kérész","Tavi kérész"}
Sitona waterhousei	{waterhouse-csipkézőbogár,Waterhouse-csipkézőbogár}
Sitta europaea	{csuszka,Csuszka}
Sitta europaea caesia	{csuszka,Csuszka}
Smicronyx smreczynskii	{smreczynski-arankaormányos,Smreczynski-arankaormányos}
Somateria mollissima	{pehelyréce,Pehelyréce}
Somateria spectabilis	{"cifra pehelyréce","Cifra pehelyréce"}
Sorex minutus	{"törpe cickány",törpecickány}
Thuja plicata	{"Óriás tuja"}
Spercheus emarginatus	{"cirpelő dajkacsibor","Cirpelő dajkacsibor"}
Spermophilus citellus	{"közönséges ürge",ürge}
Sphaeridium bipustulatum	{"szalagos ganajcsibor","Szalagos ganajcsibor"}
Sphaeridium lunatum	{"holdfoltú ganajcsibor","Holdfoltú ganajcsibor"}
Sphaeridium marginatum	{"fekete ganajcsibor","Fekete ganajcsibor"}
Sphaeridium scarabaeoides	{"négyfoltos ganajcsibor","Négyfoltos ganajcsibor"}
Sphaeridium substriatum	{"pontsoros ganajcsibor","Pontsoros ganajcsibor"}
Sphaerophoria scripta	{"tarka darázslégy","Tarka darázslégy"}
Sphegina clunipes	{"hangyaalakú zengőlégy","Hangyaalakú zengőlégy"}
Sphenoptera petriceki	{petricek-gyalogdíszbogár,Petricek-gyalogdíszbogár}
Sphex rufocinctus	{"szöcskeölő darázs","Szöcskeölő darázs"}
Spialia orbifer	{törpebusalepke,Törpebusalepke}
Spondylis buprestoides	{"erdei félcincér","Erdei félcincér"}
Spudaea ruticilla	{tölgyfa-őszibagoly,Tölgyfa-őszibagoly}
Squamapion hoffmanni	{hoffmann-cickányormányos,Hoffmann-cickányormányos}
Staurophora celsia	{buckabagoly,Buckabagoly}
Stenhomalus bicolor	{"kétszínű hengercincér","Kétszínű hengercincér"}
Stenocorus meridianus	{fűzcincér,Fűzcincér}
Stenopterus flavicornis	{"sárgacsápú keskenyfedőscincér","Sárgacsápú keskenyfedőscincér"}
Carex distans	{"réti sás","Réti sás"}
Stenopterus rufus	{keskenyfedőscincér,Keskenyfedőscincér}
Stenostola dubia	{"fémes hársfacincér","Fémes hársfacincér"}
Stenostola ferrea	{hársfacincér,Hársfacincér}
Stenurella bifasciata	{"kétöves karcsúcincér","Kétöves karcsúcincér"}
Stenurella melanura	{"feketevégű karcsúcincér","Feketevégű karcsúcincér"}
Stenurella nigra	{"fekete karcsúcincér","Fekete karcsúcincér"}
Stenurella septempunctata	{"hétpettyes karcsúcincér","Hétpettyes karcsúcincér"}
Stercorarius longicaudus	{"nyílfarkú halfarkas","Nyílfarkú halfarkas"}
Stercorarius parasiticus	{"ékfarkú halfarkas","Ékfarkú halfarkas"}
Stercorarius pomarinus	{"szélesfarkú halfarkas","Szélesfarkú halfarkas"}
Sterna albifrons	{"kis csér","Kis csér"}
Sterna caspia	{lócsér,Lócsér}
Sterna hirundo	{"küszvágó csér","Küszvágó csér"}
Sterna paradisaea	{"sarki csér","Sarki csér"}
Sterna sandvicensis	{"kenti csér","Kenti csér"}
Stictoleptura erythroptera	{"bordó virágcincér","Bordó virágcincér"}
Stictoleptura rubra	{"vörös virágcincér","Vörös virágcincér"}
Stictoleptura scutellata	{"hegyi virágcincér","Hegyi virágcincér"}
Strangalia attenuata	{nyurgacincér,Nyurgacincér}
Streptopelia decaocto	{"balkáni gerle","Balkáni gerle"}
Streptopelia orientalis	{"keleti gerle","Keleti gerle"}
Streptopelia orientalis meena	{"keleti gerle","Keleti gerle"}
Streptopelia turtur	{vadgerle,Vadgerle}
Strix aluco	{macskabagoly,Macskabagoly}
Strix uralensis	{"uráli bagoly","Uráli bagoly"}
Strix uralensis macroura	{"uráli bagoly","Uráli bagoly"}
Stromatium unicolor	{"sárgás galléroscincér","Sárgás galléroscincér"}
Sturnus roseus	{pásztormadár,Pásztormadár}
Sturnus vulgaris	{seregély,Seregély}
Suphrodytes dorsalis	{"kerekvállú csíkbogár","Kerekvállú csíkbogár"}
Surnia ulula	{karvalybagoly,Karvalybagoly}
Sylvia atricapilla	{barátposzáta,Barátposzáta}
Sylvia borin	{"kerti poszáta","Kerti poszáta"}
Sylvia cantillans	{"bajszos poszáta","Bajszos poszáta"}
Sylvia cantillans albistriata	{"bajszos poszáta","Bajszos poszáta"}
Sylvia communis	{"mezei poszáta","Mezei poszáta"}
Sylvia curruca	{"kis poszáta","Kis poszáta"}
Sylvia melanocephala	{"kucsmás poszáta","Kucsmás poszáta"}
Sylvia nisoria	{karvalyposzáta,Karvalyposzáta}
Sympherobius elegans	{"tarka barnafátyolka","Tarka barnafátyolka"}
Sympherobius pygmaeus	{törpefátyolka,Törpefátyolka}
Syritta pipiens	{"zizegő comboslégy","Zizegő comboslégy"}
Syrphus ribesii	{"közönséges zengőlégy","Közönséges zengőlégy"}
Syrrhaptes paradoxus	{talpastyúk,Talpastyúk}
Tachybaptus ruficollis	{"kis vöcsök","Kis vöcsök"}
Tadorna ferruginea	{"vörös ásólúd","Vörös ásólúd"}
Tadorna tadorna	{"bütykös ásólúd","Bütykös ásólúd"}
Taeniopteryx nebulosa	{"felhős álkérész","Felhős álkérész"}
Talpa europaea	{"közönséges vakond",vakond}
Tetrao tetrix	{nyírfajd,Nyírfajd}
Tetrao urogallus	{siketfajd,Siketfajd}
Tetrao urogallus major	{siketfajd,Siketfajd}
Tetrax tetrax	{reznek,Reznek}
Tetropium castaneum	{"romboló fenyőcincér","Romboló fenyőcincér"}
Tetropium fuscum	{"tompafényű fenyőcincér","Tompafényű fenyőcincér"}
Tetropium gabrieli	{"simafejű fenyőcincér","Simafejű fenyőcincér"}
Theophilea subcylindricollis	{"hengeres szalmacincér","Hengeres szalmacincér"}
Tichodroma muraria	{hajnalmadár,Hajnalmadár}
Trachyphloeus frivaldszkyi	{frivaldszky-éjiormányos,Frivaldszky-éjiormányos}
Tragosoma depsarium	{kecskecincér,Kecskecincér}
Berberis thunbergii	{"Japán borbolya"}
Trichoferus holosericeus	{"szürkés éjcincér","Szürkés éjcincér"}
Trichoferus pallidus	{"Sápadt éjcincér","sápadt éjicincér"}
Tringa erythropus	{"füstös cankó","Füstös cankó"}
Tringa flavipes	{"sárgalábú cankó","Sárgalábú cankó"}
Tringa glareola	{"réti cankó","Réti cankó"}
Tringa nebularia	{"szürke cankó","Szürke cankó"}
Tringa ochropus	{"erdei cankó","Erdei cankó"}
Tringa stagnatilis	{"tavi cankó","Tavi cankó"}
Galium spurium	{"Gyanus galaj","vetési galaj"}
Tringa totanus	{"piroslábú cankó","Piroslábú cankó"}
Triodia amasina	{"balkáni gyökérrágólepke","Balkáni gyökérrágólepke"}
Triturus alpestris	{"alpesi gőte","Alpesi gőte"}
Triturus carnifex	{"alpesi tarajosgőte","Alpesi tarajosgőte"}
Triturus cristatus	{"közönséges tarajosgőte","Tarajos gőte"}
Triturus dobrogicus	{"dunai tarajosgőte","Dunai tarajosgőte"}
Triturus vulgaris	{"pettyes gőte","Pettyes gőte"}
Troglodytes troglodytes	{ökörszem,Ökörszem}
Tryngites subruficollis	{cankópartfutó,Cankópartfutó}
Trypophloeus rybinskii	{rybinski-szú,Rybinski-szú}
Turdus iliacus	{szőlőrigó,Szőlőrigó}
Turdus merula	{"fekete rigó","Fekete rigó"}
Turdus naumanni	{naumann-rigó,Naumann-rigó}
Turdus philomelos	{"énekes rigó","Énekes rigó"}
Turdus pilaris	{fenyőrigó,Fenyőrigó}
Turdus torquatus	{"örvös rigó","Örvös rigó"}
Turdus torquatus alpestris	{"örvös rigó","Örvös rigó"}
Turdus viscivorus	{léprigó,Léprigó}
Tyto alba	{gyöngybagoly,Gyöngybagoly}
Tyto alba guttata	{gyöngybagoly,Gyöngybagoly}
Upupa epops	{búbosbanka,Búbosbanka}
Vadonia steveni	{"alföldi virágcincér","Alföldi virágcincér"}
Vadonia unipunctata	{"kétpettyes virágcincér","Kétpettyes virágcincér"}
Vanellus vanellus	{bíbic,Bíbic}
Vanessa atalanta	{atalantalepke,Atalantalepke}
Venustoraphidia nigricollis	{"feketelábú tevenyakú","Feketelábú tevenyakú"}
Mahonia repens	{"Kúszó mahónia"}
Vipera berus	{"keresztes vipera","Keresztes vipera"}
Vipera ursinii rakosiensis	{"rákosi vipera","Rákosi vipera"}
Volucella bombylans	{"bundás pihelégy","Bundás pihelégy"}
Volucella inanis	{"háromöves pihelégy","Háromöves pihelégy"}
Volucella pellucens	{"üvegpotrohú légy","Üvegpotrohú légy"}
Volucella zonaria	{"kétöves pihelégy","Kétöves pihelégy"}
Wesmaelius nervosus	{"hegyi törpefátyolka","Hegyi törpefátyolka"}
Wesmaelius quadrifasciatus	{"négyerű barnafátyolka","Négyerű barnafátyolka"}
Wesmaelius subnebulosus	{"pettyes barnafátyolka","Pettyes barnafátyolka"}
Xanthogramma pedissequum	{"nyári darázslégy","Nyári darázslégy"}
Xanthostigma xanthostigma	{"sárgajegyű tevenyakú","Sárgajegyű tevenyakú"}
Xenus cinereus	{terekcankó,Terekcankó}
Xerasia meschniggi	{"téli zuzmóbogár",tölgy-zuzmóbogár}
Xestia sexstrigata	{"hatcsíkú földibagoly","Szürkésvörös földibagoly"}
Xylota segnis	{"közönséges comboslégy","Közönséges comboslégy"}
Xylotrechus antilope	{"fürge darázscincér","Fürge darázscincér"}
Xylotrechus pantherinus	{Párduccincér,"párducfoltos darázscincér"}
Xylotrechus rusticus	{"egérszínű darázscincér","Egérszínű darázscincér"}
Zerynthia polyxena	{farkasalmalepke,Farkasalmalepke}
Zootoca vivipara	{"elevenszülő gyík","Elevenszülő gyík"}
Zootoca vivipara pannonica	{"elevenszülő gyík","Elevenszülő gyík"}
Zygaena fausta	{"nyugati csüngőlepke","Nyugati csüngőlepke"}
Zygaena laeta	{"vörös csüngőlepke","Vörös csüngőlepke"}
Abies alba	{"közönséges jegenyefenyő","Közönséges jegenyefenyő"}
Abutilon theophrasti	{"sárga selyemmályva",Selyemmályva}
Acer acuminatilobum	{"mátrai ősjuhar","Mátrai ősjuhar"}
Acer campestre	{"mezei juhar","Mezei juhar"}
Acer platanoides	{"korai juhar","Korai juhar"}
Acer saccharinum	{"ezüst juhar","Ezüst juhar"}
Acer tataricum	{"tatár juhar",Tatárjuhar}
Achillea asplenifolia	{"sziki cickafark","Sziki cickafark"}
Achillea collina	{"mezei cickafark","Mezei cickafark"}
Achillea crithmifolia	{"hegyközi cickafark","Hegyközi cickafark"}
Achillea distans	{"nagy cickafark","Nagy cickafark"}
Achillea millefolium	{"közönséges cickafark","Közönséges cickafark"}
Achillea nobilis subsp. neilreichii	{"nemes cickafark","Nemes cickafark"}
Achillea ochroleuca	{"homoki cickafark","Homoki cickafark"}
Achillea pannonica	{"magyar cickafark","Magyar cickafark"}
Achillea setacea	{"pusztai cickafark","Pusztai cickafark"}
Acinos arvensis	{"parlagi pereszlény","Parlagi pereszlény"}
Aconitum anthora	{"méregölő sisakvirág","Méregölő sisakvirág"}
Actaea spicata	{békabogyó,Békabogyó}
Adenophora liliifolia	{csengettyűvirág,Csengettyűvirág}
Adonis aestivalis	{"nyári hérics","Nyári hérics"}
Adonis flammea	{"lángszínű hérics","Lángszínű hérics"}
Adonis vernalis	{"tavaszi hérics","Tavaszi hérics"}
Adoxa moschatellina	{pézsmaboglár,Pézsmaboglár}
Aegilops cylindrica	{kecskebúza,Kecskebúza}
Aegopodium podagraria	{podagrafű,Podagrafű}
Aesculus hippocastanum	{"fehér vadgesztenye","Fehér vadgesztenye"}
Aethusa cynapium	{ádáz,Ádáz}
Agrimonia eupatoria	{"közönséges párlófű","Közönséges párlófű"}
Agrimonia procera	{"szagos párlófű","Szagos párlófű"}
Agropyron pectinatum	{"taréjos búzafű","Taréjos búzafű"}
Agrostis canina	{ebtippan,Ebtippan}
Agrostis capillaris	{cérnatippan,Cérnatippan}
Agrostis stolonifera	{"fehér tippan","Fehér tippan"}
Agrostis vinealis	{fenyértippan,Fenyértippan}
Ailanthus altissima	{bálványfa,"Mirigyes bálványfa"}
Aira caryophyllea	{"pusztai lengefű","Pusztai lengefű"}
Aira elegantissima	{"csinos lengefű","Csinos lengefű"}
Ajuga chamaepitys	{"kalinca ínfű",Kalincaínfű}
Ajuga chamaepitys subsp. ciliata	{"pillás kalinca ínfű","Pillás kalincaínfű"}
Ajuga genevensis	{"közönséges ínfű","Közönséges ínfű"}
Ajuga laxmannii	{"szennyes ínfű","Szennyes ínfű"}
Ajuga reptans	{"indás ínfű","Indás ínfű"}
Alcea biennis	{"halvány ziliz","Halvány ziliz"}
Alcea rosea	{mályvarózsa,Mályvarózsa}
Alchemilla crinita	{"csipkéslevelű palástfű","Csipkéslevelű palástfű"}
Alchemilla filicaulis	{"vékonyszárú palástfű","Vékonyszárú palástfű"}
Alchemilla glabra	{"havasi palástfű","Havasi palástfű"}
Alchemilla hungarica	{"magyar palástfű","Magyar palástfű"}
Alchemilla subcrenata	{"hullámoslevelű palástfű","Hullámoslevelű palástfű"}
Alchemilla xanthochlora	{"réti palástfű","Réti palástfű"}
Aldrovanda vesiculosa	{aldrovanda,Aldrovanda}
Alisma gramineum	{"Fűlevelű hídőr","úszó hídőr"}
Alisma lanceolatum	{"lándzsás hídőr","Lándzsás hídőr"}
Alisma plantago-aquatica	{"vízi hídőr","Vízi hídőr"}
Alkanna tinctoria	{báránypirosító,Báránypirosító}
Alliaria petiolata	{kányazsombor,Kányazsombor}
Allium angulosum	{gyíkhagyma,Gyíkhagyma}
Allium atropurpureum	{"bíborfekete hagyma","Bíborfekete hagyma"}
Allium atroviolaceum	{"sötét hagyma","Sötét hagyma"}
Allium carinatum	{"szarvas hagyma","Szarvas hagyma"}
Allium flavum	{"sárga hagyma","Sárga hagyma"}
Allium oleraceum	{"érdes hagyma","Érdes hagyma"}
Allium scorodoprasum	{kígyóhagyma,Kígyóhagyma}
Allium sphaerocephalon	{"bunkós hagyma","Bunkós hagyma"}
Allium suaveolens	{"illatos hagyma","Illatos hagyma"}
Allium ursinum	{medvehagyma,Medvehagyma}
Allium vineale	{"bajuszos hagyma","Bajuszos hagyma"}
Alnus glutinosa	{"Enyves éger","mézgás éger"}
Alnus incana	{"hamvas éger","Hamvas éger"}
Alnus viridis	{"havasi éger","Havasi éger"}
Alopecurus aequalis	{"mocsári ecsetpázsit","Mocsári ecsetpázsit"}
Alopecurus geniculatus	{"gombos ecsetpázsit","Gombos ecsetpázsit"}
Alopecurus myosuroides	{"parlagi ecsetpázsit","Parlagi ecsetpázsit"}
Alopecurus pratensis	{"réti ecsetpázsit","Réti ecsetpázsit"}
Althaea cannabina	{kenderziliz,"Kender ziliz"}
Althaea hirsuta	{"borzas ziliz","Borzas ziliz"}
Althaea officinalis	{"orvosi ziliz","Orvosi ziliz"}
Alyssum alyssoides	{"közönséges ternye","Közönséges ternye"}
Alyssum desertorum	{"pusztai ternye","Pusztai ternye"}
Alyssum montanum	{"hegyi ternye","Hegyi ternye"}
Armoracia macrocarpa	{"debreceni torma","Debreceni torma"}
Alyssum montanum subsp. brymii	{"nagyvirágú hegyi ternye","Nagyvirágú hegyi ternye"}
Alyssum montanum subsp. gmelinii	{"pusztai hegyi ternye","Pusztai hegyi ternye"}
Alyssum tortuosum	{"homoki ternye","Homoki ternye"}
Alyssum tortuosum subsp. heterophyllum	{"széleslevelű homoki ternye","Széleslevelű homoki ternye"}
Amaranthus albus	{"fehér disznóparéj","Fehér disznóparéj"}
Amaranthus blitoides	{"labodás disznóparéj","Labodás disznóparéj"}
Amaranthus bouchonii	{Bouchon-disznóparéj,"Piros amaránt"}
Amaranthus crispus	{"bodros disznóparéj","Bodros disznóparéj"}
Amaranthus cruentus	{"Bíbor amaránt","bíbor disznóparéj"}
Amaranthus deflexus	{"vöröslő disznóparéj","Vöröslő disznóparéj"}
Amaranthus lividus subsp. ascendens	{"Henye zöld disznóparéj","zöld disznóparéj"}
Amaranthus retroflexus	{"szőrös disznóparéj","Szőrös disznóparéj"}
Amelanchier ovalis	{"közönséges fanyarka","Közönséges fanyarka"}
Ammannia verticillata	{iszapfüzény,Iszapfüzény}
Anagallis arvensis	{"mezei tikszem","Mezei tikszem"}
Anagallis foemina	{"kék tikszem","Kék tikszem"}
Anchusa azurea	{"olasz atracél","Olasz atracél"}
Anchusa barrelieri	{"kék atracél","Kék atracél"}
Anchusa officinalis	{"orvosi atracél","Orvosi atracél"}
Andromeda polifolia	{tőzegrozmaring,Tőzegrozmaring}
Androsace elongata	{"cingár gombafű","Cingár gombafű"}
Androsace maxima	{"nagy gombafű","Nagy gombafű"}
Anemone nemorosa	{"berki szellőrózsa","Berki szellőrózsa"}
Anemone ranunculoides	{"bogláros szellőrózsa","Bogláros szellőrózsa"}
Anemone sylvestris	{"erdei szellőrózsa","Erdei szellőrózsa"}
Anemone trifolia	{"hármaslevelű szellőrózsa","Hármaslevelű szellőrózsa"}
Angelica archangelica	{"orvosi angyalgyökér","Orvosi angyalgyökér"}
Angelica palustris	{"réti angyalgyökér","Réti angyalgyökér"}
Angelica sylvestris	{"erdei angyalgyökér","Erdei angyalgyökér"}
Antennaria dioica	{macskatalp,Macskatalp}
Anthemis arvensis	{"parlagi pipitér","Parlagi pipitér"}
Anthemis austriaca	{"szöszös pipitér","Szöszös pipitér"}
Anthemis cotula	{"nehézszagú pipitér","Nehézszagú pipitér"}
Anthemis tinctoria	{"festő pipitér","Festő pipitér"}
Anthericum liliago	{"fürtös homokliliom","Fürtös homokliliom"}
Anthericum ramosum	{"ágas homokliliom","Ágas homokliliom"}
Anthoxanthum aristatum	{"parlagi borjúpázsit","Szálkás borjúpázsit"}
Anthoxanthum odoratum	{Borjúpázsit,"illatos borjúpázsit"}
Anthriscus caucalis	{"borzas turbolya","Borzas turbolya"}
Anthriscus nitida	{"havasi turbolya","Havasi turbolya"}
Anthriscus sylvestris	{"erdei turbolya","Erdei turbolya"}
Anthyllis vulneraria	{Nyúlszapuka,"réti nyúlszapuka"}
Anthyllis vulneraria subsp. alpestris	{"havasi nyúlszapuka","Havasi nyúlszapuka"}
Anthyllis vulneraria subsp. carpatica	{"kárpáti nyúlszapuka","Kárpáti nyúlszapuka"}
Antirrhinum majus	{"kerti oroszlánszáj","Kerti oroszlánszáj"}
Apera interrupta	{"rongyos széltippan","Rongyos széltippan"}
Apera spica-venti	{"nagy széltippan","Nagy széltippan"}
Aphanes arvensis	{"nagy ugarpalástfű","Nagy ugarpalástfű"}
Aphanes microcarpa	{"kis ugarpalástfű","Kis ugarpalástfű"}
Apium graveolens	{"kerti zeller","Kerti zeller"}
Apium repens	{"kúszó zeller","Kúszó zeller"}
Aquilegia vulgaris	{"közönséges harangláb","Közönséges harangláb"}
Arabidopsis thaliana	{lúdfű,Lúdfű}
Arabis alpina	{"havasi ikravirág","Havasi ikravirág"}
Arabis auriculata	{"egyenes ikravirág","Egyenes ikravirág"}
Arabis hirsuta	{"borzas ikravirág","Borzas ikravirág"}
Arabis turrita	{"tornyos ikravirág","Tornyos ikravirág"}
Arctium lappa	{"közönséges bojtorján","Közönséges bojtorján"}
Arctium minus	{"kis bojtorján","Kis bojtorján"}
Arctium nemorosum	{"berki bojtorján","Berki bojtorján"}
Arctium tomentosum	{"pókhálós bojtorján","Pókhálós bojtorján"}
Aremonia agrimonioides	{"bükkös kispárlófű",Kispárlófű}
Arenaria leptoclados	{"puha homokhúr","Puha homokhúr"}
Arenaria serpyllifolia	{"kakukk homokhúr",Kakukkhomokhúr}
Aristolochia clematitis	{farkasalma,Platán}
Armeria elongata	{"magas istác","Magas istác"}
Arrhenatherum elatius	{franciaperje,Franciaperje}
Artemisia abrotanum	{istenfa,Istenfa}
Artemisia absinthium	{"fehér üröm","Fehér üröm"}
Artemisia alba	{"sziklai üröm","Sziklai üröm"}
Artemisia alba subsp. canescens	{"szürke sziklai üröm","Szürke sziklai üröm"}
Artemisia alba subsp. saxatilis	{"sötétzöld sziklai üröm","Sötétzöld sziklai üröm"}
Artemisia annua	{"egynyári üröm","Egynyári üröm"}
Artemisia austriaca	{"selymes üröm","Selymes üröm"}
Artemisia campestris	{"mezei üröm","Mezei üröm"}
Artemisia pontica	{bárányüröm,Bárányüröm}
Artemisia santonicum	{"sziki üröm","Sziki üröm"}
Artemisia santonicum subsp. patens	{"kopasz sziki üröm","Kopasz sziki üröm"}
Artemisia scoparia	{Seprőüröm,seprűüröm}
Artemisia vulgaris	{"fekete üröm","Fekete üröm"}
Arum maculatum	{"foltos kontyvirág","Foltos kontyvirág"}
Arundo donax	{olasznád,Olasznád}
Asarum europaeum	{Kapotnyak,"kereklevelű kapotnyak"}
Asarum europaeum subsp. caucasicum	{"kaukázusi kapotnyak","Szívlevelű kapotnyak"}
Asclepias syriaca	{selyemkóró,Selyemkóró}
Asparagus officinalis	{"közönséges spárga","Közönséges spárga"}
Asperugo procumbens	{magiszák,Magiszák}
Asperula arvensis	{"vetési müge","Vetési müge"}
Asperula cynanchica	{"ebfojtó müge","Ebfojtó müge"}
Asperula orientalis	{"keleti müge","Keleti müge"}
Asperula taurina subsp. leucanthera	{"olasz müge","Olasz müge"}
Asperula tinctoria	{"festő müge","Festő müge"}
Asplenium adiantum-nigrum	{"fekete fodorka","Fekete fodorka"}
Asplenium fontanum	{forrásfodorka,Forrásfodorka}
Asplenium lepidum	{"mirigyes fodorka","Mirigyes fodorka"}
Asplenium ruta-muraria	{"kövi fodorka","Kövi fodorka"}
Asplenium septentrionale	{"északi fodorka","Északi fodorka"}
Asplenium trichomanes	{"aranyos fodorka","Aranyos fodorka"}
Asplenium viride	{"zöld fodorka","Zöld fodorka"}
Aster amellus	{csillagőszirózsa,Csillagőszirózsa}
Aster bellidiastrum	{százszorszép-őszirózsa,"Százszorszép őszirózsa"}
Aster linosyris	{aranyfürt,Aranyfürt}
Aster novae-angliae	{"mirigyes őszirózsa","Mirigyes őszirózsa"}
Aster novi-belgii	{"sötétlila őszirózsa","Sötétlila őszirózsa"}
Aster oleifolius	{"gyapjas őszirózsa","Gyapjas őszirózsa"}
Aster sedifolius subsp. canus	{"réti őszirózsa","Réti őszirózsa"}
Aster tripolium subsp. pannonicus	{"sziki őszirózsa","Sziki őszirózsa"}
Astragalus asper	{"érdes csüdfű","Érdes csüdfű"}
Astragalus austriacus	{"kisvirágú csüdfű","Kisvirágú csüdfű"}
Astragalus cicer	{"hólyagos csüdfű","Hólyagos csüdfű"}
Astragalus contortuplicatus	{"tekert csüdfű","Tekert csüdfű"}
Astragalus dasyanthus	{"gyapjas csüdfű","Gyapjas csüdfű"}
Astragalus exscapus	{"szártalan csüdfű","Szártalan csüdfű"}
Astragalus glycyphyllos	{"édeslevelű csüdfű","Édeslevelű csüdfű"}
Astragalus onobrychis	{"zászlós csüdfű","Zászlós csüdfű"}
Astragalus sulcatus	{"barázdás csüdfű","Barázdás csüdfű"}
Astragalus varius	{"homoki csüdfű","Homoki csüdfű"}
Astragalus vesicarius subsp. albidus	{"fehéres csüdfű","Fehéres csüdfű"}
Asyneuma canescens	{harangcsillag,Harangcsillag}
Asyneuma canescens subsp. salicifolium	{"fűzlevelű harangcsillag","Fűzlevelű harangcsillag"}
Athyrium filix-femina	{"erdei hölgypáfrány",Hölgypáfrány}
Atriplex hortensis	{"kerti laboda","Kerti laboda"}
Atriplex littoralis	{"parti laboda","Parti laboda"}
Atriplex oblongifolia	{"hosszúlevelű laboda","Hosszúlevelű laboda"}
Atriplex patula	{"terebélyes laboda","Terebélyes laboda"}
Atriplex prostrata	{"dárdás laboda","Dárdás laboda"}
Atriplex rosea	{"fehér laboda","Fehér laboda"}
Atriplex tatarica	{"tatár laboda","Tatár laboda"}
Avena barbata	{"Szakálas zab","szakállas zab"}
Avena fatua	{"héla zab","Héla zab"}
Avena nuda	{"kopasz zab","Kopasz zab"}
Avena sativa	{abrakzab,Abrakzab}
Avena strigosa	{"érdes zab","Érdes zab"}
Azolla filiculoides	{"nagylevelű moszatpáfrány","Nagylevelű moszatpáfrány"}
Barbarea stricta	{"merev borbálafű","Merev borbálafű"}
Barbarea vulgaris	{"közönséges borbálafű","Közönséges borbálafű"}
Bassia sedoides	{"hamvas seprőparéj",Seprűparéj}
Beckmannia eruciformis	{hernyópázsit,Hernyópázsit}
Bellis perennis	{százszorszép,Százszorszép}
Berberis julianae	{júlia-borbolya,Júlia-borbolya}
Berberis vulgaris	{sóskaborbolya,Sóskaborbolya}
Berteroa incana	{"fehér hamuka",Hamuka}
Berula erecta	{"keskenylevelű békakorsó","Keskenylevelű békakorsó"}
Beta trigyna	{"Hárombibés répa","termesztett répa"}
Betonica officinalis	{bakfű,Bakfű}
Betula pendula	{"bibircses nyír","Közönséges nyír"}
Bifora radians	{poloskagyom,Poloskagyom}
Biscutella laevigata	{korongpár,Korongpár}
Biscutella laevigata subsp. austriaca	{"osztrák korongpár","Osztrák korongpár"}
Biscutella laevigata subsp. hungarica	{"magyar korongpár","Magyar korongpár"}
Biscutella laevigata subsp. illyrica	{"illír korongpár","Illír korongpár"}
Bolboschoenus maritimus	{"sziki zsióka",Zsióka}
Borago officinalis	{borágó,Borágó}
Bothriochloa ischaemum	{fenyérfű,Fenyérfű}
Botrychium lunaria	{"kis holdruta","Kis holdruta"}
Botrychium matricariifolium	{"ágas holdruta","Ágas holdruta"}
Botrychium multifidum	{"sokcimpájú holdruta","Sokcimpájú holdruta"}
Botrychium virginianum subsp. europaeum	{"virginiai holdruta","Virginiai holdruta"}
Brachypodium pinnatum	{"tollas szálkaperje","Tollas szálkaperje"}
Brachypodium pinnatum subsp. rupestre	{"sziklai szálkaperje","Sziklai szálkaperje"}
Brachypodium sylvaticum	{"erdei szálkaperje","Erdei szálkaperje"}
Brassica elongata	{"harasztos káposzta",Káposzta}
Brassica nigra	{"francia mustár","Szareptai mustár"}
Brassica oleracea	{káposzta,Káposzta}
Briza media	{"Közepes rezgőpázsit",rezgőpázsit}
Bromus arvensis	{"mezei rozsnok","Mezei rozsnok"}
Bromus benekenii	{"erdei rozsnok","Erdei rozsnok"}
Bromus commutatus	{"bókoló rozsnok","Bókoló rozsnok"}
Bromus erectus	{"sudár rozsnok","Sudár rozsnok"}
Bromus inermis	{"árva rozsnok","Árva rozsnok"}
Bromus japonicus	{"parlagi rozsnok","Parlagi rozsnok"}
Bromus lepidus	{"szikár rozsnok","Szikár rozsnok"}
Bromus madritensis	{"Madridi rozsnok","spanyol rozsnok"}
Bromus pannonicus	{"magyar rozsnok","Magyar rozsnok"}
Bromus racemosus	{"fürtös rozsnok","Fürtös rozsnok"}
Bromus ramosus	{"ágas rozsnok","Ágas rozsnok"}
Bromus rigidus	{"óriás rozsnok","Óriás rozsnok"}
Bromus secalinus	{gabonarozsnok,Gabonarozsnok}
Bromus squarrosus	{"berzedt rozsnok","Berzedt rozsnok"}
Bromus sterilis	{"meddő rozsnok","Meddő rozsnok"}
Paeonia lactiflora	{"Fehér bazsarózsa"}
Bromus tectorum	{"fedél rozsnok",Fedélrozsnok}
Bryonia alba	{"fekete földitök","Fekete földitök"}
Bryonia dioica	{"piros földitök","Piros földitök"}
Bulbocodium versicolor	{egyhajúvirág,Egyhajúvirág}
Bunias erucago	{"Csípős szümcső",szümcsőfaj}
Bunias orientalis	{"keleti szümcső","Keleti szümcső"}
Buphthalmum salicifolium	{"fűzlevelű ökörszem",Ökörszem}
Bupleurum affine	{"vöröslő buvákfű","Vöröslő buvákfű"}
Bupleurum falcatum	{"sarlós buvákfű","Sarlós buvákfű"}
Bupleurum longifolium	{"hosszúlevelű buvákfű","Hosszúlevelű buvákfű"}
Bupleurum praealtum	{"tejelő buvákfű","Tejelő buvákfű"}
Bupleurum rotundifolium	{"kereklevelű buvákfű","Kereklevelű buvákfű"}
Bupleurum tenuissimum	{"sziki buvákfű","Sziki buvákfű"}
Butomus umbellatus	{virágkáka,Virágkáka}
Calamagrostis arundinacea	{"erdei nádtippan","Erdei nádtippan"}
Calamagrostis canescens	{"dárdás nádtippan","Dárdás nádtippan"}
Calamagrostis epigeios	{Siskanád,"siska nádtippan"}
Calamagrostis pseudophragmites	{"parti nádtippan","Parti nádtippan"}
Calamagrostis stricta	{"lápi nádtippan","Lápi nádtippan"}
Calamagrostis varia	{"tarka nádtippan","Tarka nádtippan"}
Calamagrostis villosa	{"pillás nádtippan","Szöszös nádtippan"}
Calendula officinalis	{"orvosi körömvirág","Orvosi körömvirág"}
Calepina irregularis	{matyó,Matyó}
Calla palustris	{sárkánygyökér,Sárkánygyökér}
Callitriche cophocarpa	{"szárnyatlan mocsárhúr","Változékony mocsárhúr"}
Callitriche palustris	{mocsárhúr,"Tavaszi mocsárhúr"}
Calluna vulgaris	{Csarab,"közönséges csarab"}
Caltha palustris	{"mocsári gólyahír","Mocsári gólyahír"}
Calystegia sepium	{sövényszulák,Sövényszulák}
Camelina alyssum	{"duzzadt gomborka","Duzzadt gomborka"}
Camelina microcarpa	{"kis gomborka","Kis gomborka"}
Camelina microcarpa subsp. pilosa	{"szőrös kis gomborka","Szőrös kis gomborka"}
Camelina rumelica	{"fehérvirágú gomborka","Fehérvirágú gomborka"}
Camelina sativa	{sárgarepce,Sárgarepce}
Campanula bononiensis	{"olasz harangvirág","Olasz harangvirág"}
Campanula cervicaria	{"halvány harangvirág","Halvány harangvirág"}
Campanula glomerata	{"csomós harangvirág","Csomós harangvirág"}
Campanula glomerata subsp. elliptica	{"Kétes hazai előfordulású","kopasz csomós harangvirág"}
Campanula glomerata subsp. farinosa	{"lisztes csomós harangvirág","Lisztes csomós harangvirág"}
Campanula latifolia	{"széleslevelű harangvirág","Széleslevelű harangvirág"}
Campanula macrostachya	{"hosszúfüzérű harangvirág","Hosszúfüzérű harangvirág"}
Campanula moravica	{"moráviai harangvirág","Moráviai harangvirág"}
Campanula patula	{"terebélyes harangvirág","Terebélyes harangvirág"}
Campanula persicifolia	{"baracklevelű harangvirág","Baracklevelű harangvirág"}
Campanula rapunculoides	{kányaharangvirág,Kányaharangvirág}
Campanula rapunculus	{raponcharangvirág,Raponcharangvirúg}
Campanula rotundifolia	{"kereklevelű harangvirág","Kereklevelű harangvirág"}
Campanula sibirica	{"pongyola harangvirág","Pongyola harangvirág"}
Campanula trachelium	{"csalánlevelű harangvirág","Csalánlevelű harangvirág"}
Campanula xylocarpa	{"kereklevelű harangvirágfaj","Kereklevelű harangvirágfaj"}
Camphorosma annua	{bárányparéj,Bárányparéj}
Campsis radicans	{"Amerikai trombitafolyondár","kapaszkodó trombitafolyondár"}
Cannabis sativa subsp. spontanea	{vadkender,Vadkender}
Capsella bursa-pastoris	{"közönséges pásztortáska","Közönséges pásztortáska"}
Capsella rubella	{"pirosló pásztortáska","Pirosló pásztortáska"}
Cardamine amara	{"keserű kakukktorma","Keserű kakukktorma"}
Cardamine flexuosa	{"erdei kakukktorma","Ligeti kakukktorma"}
Cardamine hirsuta	{"borzas kakukktorma","Borzas kakukktorma"}
Cardamine impatiens	{"virágrúgó kakukktorma","Virágrúgó kakukktorma"}
Cardamine parviflora	{"kisvirágú kakukktorma","Kisvirágú kakukktorma"}
Cardamine pratensis	{"réti kakukktorma","Réti kakukktorma"}
Cardamine pratensis subsp. matthioli	{"ágas kakukktorma","Ágas réti kakukktorma"}
Cardamine trifolia	{"hármaslevelű kakukktorma","Hármaslevelű kakukktorma"}
Cardaminopsis arenosa	{"Közönséges dercevirág","közönséges kövifoszlár"}
Cardaminopsis arenosa subsp. borbasii	{Borbás-kövifoszlár,"Borbás közönséges dercevirága"}
Cardaminopsis petraea	{"Sziklai dercevirág","sziklai kövifoszlár"}
Cardaria draba	{"közönséges útszéli-zsázsa","Útszéli zsázsa"}
Carduus acanthoides	{"útszéli bogáncs","Útszéli bogáncs"}
Carduus collinus	{"magyar bogáncs","Magyar bogáncs"}
Carduus crispus	{"fodros bogáncs","Fodros bogáncs"}
Carduus glaucus	{"szürke bogáncs","Szürke bogáncs"}
Carduus hamulosus	{"horgas bogáncs","Horgas bogáncs"}
Carex acutiformis	{"mocsári sás","Mocsári sás"}
Carex alba	{"fehér sás","Fehér sás"}
Carex appropinquata	{"rostostövű sás","Rostostövű sás"}
Carex bohemica	{palkasás,Palkasás}
Carex brevicollis	{"mérges sás","Mérges sás"}
Carex brizoides	{"rezgő sás","Rezgő sás"}
Carex buekii	{"bánsági sás","Bánsági sás"}
Carex buxbaumii	{"bunkós északi sás",Buxbaum-sás}
Carex canescens	{"szürkés sás","Szürkés sás"}
Carex caryophyllea	{"tavaszi sás","Tavaszi sás"}
Carex davalliana	{"lápi sás","Lápi sás"}
Carex diandra	{"hengeres sás","Hengeres sás"}
Carex digitata	{"ujjas sás","Ujjas sás"}
Carex disticha	{"kétsoros sás","Kétsoros sás"}
Carex divisa	{"csátés sás","Csátés sás"}
Carex divulsa	{"zöldes sás","Zöldes sás"}
Carex echinata	{"töviskés sás","Töviskés sás"}
Carex elata	{zsombéksás,Zsombéksás}
Carex elongata	{"nyúlánk sás","Nyúlánk sás"}
Carex ericetorum	{fenyérsás,Fenyérsás}
Carex flacca	{"deres sás","Deres sás"}
Carex flava	{"sárga sás","Sárga sás"}
Carex fritschii	{"dunántúli sás","Dunántúli sás"}
Carex hirta	{"borzas sás","Borzas sás"}
Carex hordeistichos	{árpasás,Árpasás}
Carex hostiana	{"barna sás","Barna sás"}
Carex humilis	{"lappangó sás","Lappangó sás"}
Carex lasiocarpa	{"gyapjasmagvú sás","Gyapjasmagvú sás"}
Carex lepidocarpa	{"pikkelyes sás","Pikkelyes sás"}
Carex limosa	{"Iszapos sás",semlyéksás}
Carex melanostachya	{"bókoló sás","Bókoló sás"}
Carex michelii	{"sárgás sás","Sárgás sás"}
Carex montana	{"hegyi sás","Hegyi sás"}
Carex nigra	{"fekete sás","Fekete sás"}
Carex pallescens	{"sápadt sás","Sápadt sás"}
Carex panicea	{muharsás,Muharsás}
Carex paniculata	{"bugás sás","Bugás sás"}
Carex pendula	{"lecsüngő sás","Lecsüngő sás"}
Carex pilosa	{"bükkös sás","Bükkös sás"}
Carex pilulifera	{"eperjes sás","Eperjes sás"}
Carex praecox	{"korai sás","Korai sás"}
Carex pseudocyperus	{"villás sás","Villás sás"}
Carex remota	{"ritkás sás","Ritkás sás"}
Carex repens	{"kúszó sás","Kúszó sás"}
Carex riparia	{"parti sás","Parti sás"}
Carex secalina	{csutaksás,Csutaksás}
Carex serotina	{iszapsás,Iszapsás}
Carex spicata	{"sulymos sás","Sulymos sás"}
Carex stenophylla	{"keskenylevelű sás","Keskenylevelű sás"}
Carex strigosa	{"borostás sás","Borostás sás"}
Carex supina	{"gindár sás","Gindár sás"}
Carex sylvatica	{"erdei sás","Erdei sás"}
Carex tomentosa	{"molyhos sás","Molyhos sás"}
Carex umbrosa	{"árnyéki sás","Árnyéki sás"}
Carex vesicaria	{"hólyagos sás","Hólyagos sás"}
Carex vulpina	{"róka sás",Rókasás}
Carlina acaulis	{"szártalan bábakalács","Szártalan bábakalács"}
Carlina vulgaris	{"közönséges bábakalács","Közönséges bábakalács"}
Carpesium abrotanoides	{"fürtös gyűrűvirág","Fürtös gyűrűvirág"}
Carpesium cernuum	{"bókoló gyűrűvirág","Bókoló gyűrűvirág"}
Carpinus betulus	{"közönséges gyertyán","Közönséges gyertyán"}
Carpinus orientalis	{"keleti gyertyán","Keleti gyertyán"}
Carthamus lanatus	{vadpórsáfrány,Vadpórsáfrány}
Carum carvi	{kömény,Kömény}
Castanea sativa	{szelídgesztenye,Szelídgesztenye}
Catabrosa aquatica	{Forrásperje,"vízi forrásperje"}
Catalpa bignonioides	{"Széleslevelű szivarfa","szívlevelű szivarfa"}
Caucalis platycarpos	{"tüskés ördögbocskor","Tüskés ördögbocskor"}
Caucalis platycarpos subsp. muricata	{"Rücskös tüskés ördögbocskor","tüskés ördögbocskor"}
Celtis australis	{"déli ostorfa","Déli ostorfa"}
Celtis occidentalis	{"nyugati ostorfa","Nyugati ostorfa"}
Cenchrus incertus	{átoktüske,Átoktüske}
Centaurea arenaria subsp. borysthenica	{"homoki imola","Homoki imola"}
Centaurea calcitrapa	{"úti imola","Úti imola"}
Centaurea cyanus	{"kék búzavirág","Kék búzavirág"}
Centaurea diffusa	{"terpedt imola","Terpedt imola"}
Centaurea indurata	{"borzas imola","Borzas imola"}
Centaurea jacea	{"réti imola","Réti imola"}
Centaurea macroptilon	{"tollas imola","Tollas imola"}
Centaurea macroptilon subsp. oxylepis	{"egyfészkű imola","Egyfészkű tollas imola"}
Centaurea mollis	{"szirti imola","Szirti imola"}
Centaurea nigrescens	{"feketés imola","Feketés imola"}
Centaurea nigrescens subsp. vochinensis	{"kereklevelű imola","Kereklevelű imola"}
Centaurea pannonica	{"magyar imola","Magyar imola"}
Centaurea pseudophrygia	{"paróka imola","Parókás imola"}
Centaurea scabiosa	{"vastövű imola","Vastövű imola"}
Centaurea solstitialis	{"sáfrányos imola","Sáfrányos imola"}
Centaurea spinulosa	{"töviskés imola","Töviskés imola"}
Centaurea stenolepis	{"pókhálós imola","Pókhálós imola"}
Centaurea triumfetti	{"tarka imola","Tarka imola"}
Centaurea triumfetti subsp. aligera	{"széleslevelű imola","Széleslevelű tarka imola"}
Centaurea triumfetti subsp. stricta	{"keskenylevelű imola","Keskenylevelű tarka imola"}
Centaurium erythraea	{"kis ezerjófű",Százforintosfű}
Centaurium pulchellum	{"csinos ezerjófű","Csinos ezerjófű"}
Centunculus minimus	{"apró centike",Centike}
Cephalanthera damasonium	{"fehér madársisak","Fehér madársisak"}
Cephalanthera longifolia	{"kardos madársisak","Kardos madársisak"}
Cephalanthera rubra	{"piros madársisak","Piros madársisak"}
Cephalaria transsylvanica	{"mezei fejvirág","Mezei fejvirág"}
Cerastium arvense	{"parlagi madárhúr","Parlagi madárhúr"}
Cerastium brachypetalum	{"Kisszirmú madárhúr","ugari madárhúr"}
Cerastium brachypetalum subsp. tauricum	{"mirigyes kisszirmú madárhúr","Mirigyes kisszirmú madárhúr"}
Cerastium dubium	{"sziki madárhúr","Sziki madárhúr"}
Cerastium glomeratum	{"gomolyos madárhúr","Gomolyos madárhúr"}
Cerastium pumilum	{"törpe madárhúr","Törpe madárhúr"}
Cerastium semidecandrum	{"béka madárhúr",Békamadárhúr}
Cerastium subtetrandrum	{"négyporzós madárhúr","Négyporzós madárhúr"}
Cerastium sylvaticum	{"erdei madárhúr","Erdei madárhúr"}
Cerastium tomentosum	{"molyhos madárhúr","Molyhos madárhúr"}
Ceratophyllum demersum	{"érdes tócsagaz","Érdes tócsagaz"}
Ceratophyllum submersum	{"sima tócsagaz","Sima tócsagaz"}
Cercis siliquastrum	{júdásfa,"Közönséges júdásfa"}
Cerinthe minor	{"kis szeplőlapu",Szeplőlapu}
Chaerophyllum aromaticum	{"fűszeres baraboly","Fűszeres baraboly"}
Chaerophyllum aureum	{"aranyos baraboly","Aranyos baraboly"}
Chaerophyllum bulbosum	{csemegebaraboly,Csemegebaraboly}
Chaerophyllum temulum	{"bóditó baraboly","Bódító baraboly"}
Chamaecyparis lawsoniana	{"oregoni hamisciprus","Oregoni hamisciprus"}
Chamaecytisus austriacus	{"buglyos zanót","Buglyos zanót"}
Chamaecytisus hirsutus subsp. leucotrichus	{"bozontos zanót","Fehérszőrű borzas zanót"}
Chamaecytisus ratisbonensis	{"selymes zanót","Selymes zanót"}
Chamaecytisus supinus	{"gombos zanót","Gombos zanót"}
Chamaecytisus supinus subsp. pannonicus	{"pannon gombos zanót","Pannon gombos zanót"}
Chelidonium majus	{"vérehulló fecskefű","Vérehulló fecskefű"}
Chenopodium album	{"fehér libatop","Fehér libatop"}
Chenopodium ambrosioides	{Mirha-libatop,"mirrha libatop"}
Chenopodium aristatum	{"szálkás libatop","Szálkás libatop"}
Chenopodium bonus-henricus	{parajlibatop,Parajlibatop}
Chenopodium botrys	{"mirigyes libatop",Rubianka-libatop}
Chenopodium ficifolium	{"fügelevelű libatop","Fügelevelű libatop"}
Chenopodium foliosum	{vesszősparaj,Vesszősparéj}
Chenopodium giganteum	{"óriás libatop","Óriás libatop"}
Chenopodium glaucum	{"fakó libatop","Fakó libatop"}
Chenopodium hybridum	{pokolvarlibatop,Pokolvarlibatop}
Chenopodium murale	{"kőfali libatop","Kőfali libatop"}
Chenopodium opulifolium	{"bangitalevelű libatop","Bangitalevelű libatop"}
Chenopodium polyspermum	{"hegyeslevelű libatop","Hegyeslevelű libatop"}
Chenopodium pumilio	{"alacsony libatop","Alacsony libatop"}
Chenopodium rubrum	{"vörös libatop","Vörös libatop"}
Chenopodium strictum	{"csíkos libatop","Csíkós libatop"}
Chenopodium suecicum	{"zöld libatop","Zöld libatop"}
Chenopodium urbicum	{"faluszéli libatop","Faluszéli libatop"}
Chenopodium vulvaria	{"Büdös libatop","rubianka libatop"}
Chondrilla juncea	{nyúlparéj,Nyúlparéj}
Chorispora tenella	{"alacsony cikkesbecő","Alacsony cikkesbecő"}
Chrysanthemum segetum	{"Vetési aranyvirág","vetési margitvirág"}
Chrysopogon gryllus	{élesmosófű,Élesmosófű}
Chrysosplenium alternifolium	{"aranyos veselke","Aranyos veselke"}
Cichorium intybus	{"mezei katáng","Mezei katáng"}
Circaea alpina	{"havasi varázslófű","Havasi varázslófű"}
Circaea lutetiana	{"erdei varázslófű","Erdei varázslófű"}
Cirsium arvense	{"mezei aszat","Mezei aszat"}
Cirsium brachycephalum	{"kisfészkű aszat","Kisfészkű aszat"}
Cirsium canum	{"szürke aszat","Szürke aszat"}
Cirsium eriophorum	{"gyapjas aszat","Gyapjas aszat"}
Cirsium erisithales	{"enyves aszat","Enyves aszat"}
Cirsium furiens	{"öldöklő aszat","Öldöklő aszat"}
Cirsium oleraceum	{"halovány aszat","Halovány aszat"}
Cirsium palustre	{"mocsári aszat","Mocsári aszat"}
Cirsium pannonicum	{"magyar aszat","Magyar aszat"}
Cirsium rivulare	{csermelyaszat,Csermelyaszat}
Cirsium vulgare	{"közönséges aszat","Közönséges aszat"}
Cladium mariscus	{télisás,Télisás}
Cleistogenes serotina	{késeiperje,Késeiperje}
Clematis alpina	{"havasi iszalag","Havasi iszalag"}
Clematis flammula	{"illatos iszalag","Illatos iszalag"}
Clematis integrifolia	{"réti iszalag","Réti iszalag"}
Clematis recta	{"egyenes iszalag","Egyenes iszalag"}
Clematis vitalba	{"erdei iszalag","Erdei iszalag"}
Clematis viticella	{"olasz iszalag","Olasz iszalag"}
Clinopodium vulgare	{borsfű,Borsfű}
Cnidium dubium	{Gyíkvirág,"inas gyíkvirág"}
Colchicum arenarium	{"homoki kikerics","Homoki kikerics"}
Colchicum autumnale	{"őszi kikerics","Őszi kikerics"}
Colchicum hungaricum	{"magyar kikerics","Magyar kikerics"}
Colutea arborescens	{"pukkantó dudafürt","Pukkantó dudafürt"}
Commelina communis	{kommelina,Kommelína}
Conium maculatum	{bürök,Bürök}
Conringia orientalis	{"keleti nyilasfű","Keleti nyilasfű"}
Consolida orientalis	{"keleti szarkaláb","Keleti szarkaláb"}
Consolida regalis	{"mezei szarkaláb","Mezei szarkaláb"}
Consolida regalis subsp. paniculata	{"bugás mezei szarkaláb","Bugás mezei szarkaláb"}
Convallaria majalis	{gyöngyvirág,Gyöngyvirág}
Convolvulus arvensis	{"apró szulák","Apró szulák"}
Convolvulus cantabrica	{"borzas szulák","Borzas szulák"}
Coriandrum sativum	{koriander,Koriander}
Corispermum canescens	{"szürke poloskamag","Szürke poloskamag"}
Corispermum nitidum	{"fényes poloskamag","Fényes poloskamag"}
Cornus alba	{"fehér som","Fehér som"}
Cornus mas	{"húsos som","Húsos som"}
Cornus sanguinea	{"veresgyűrű som","Veresgyűrű som"}
Cornus sanguinea subsp. hungarica	{"magyar veresgyűrű som","Magyar veresgyűrű som"}
Coronilla coronata	{"sárga koronafürt","Sárga koronafürt"}
Coronilla vaginalis	{"terpedt koronafürt","Terpedt koronafürt"}
Coronopus didymus	{"sárga varjúláb","Sárga varjúláb"}
Coronopus squamatus	{"terpedt varjúláb",Varjúláb}
Corydalis cava	{"odvas keltike","Odvas keltike"}
Corydalis lutea	{"kerti sárgakeltike","Sárga keltike"}
Corydalis solida	{"ujjas keltike","Ujjas keltike"}
Corylus avellana	{"közönséges mogyoró","Közönséges mogyoró"}
Corylus colurna	{"török mogyoró","Török mogyoró"}
Corynephorus canescens	{ezüstperje,Ezüstperje}
Cotinus coggygria	{cserszömörce,Cserszömörce}
Crataegus monogyna	{"egybibés galagonya","Egybibés galagonya"}
Crataegus nigra	{"fekete galagonya","Fekete galagonya"}
Crepis biennis	{"réti zörgőfű","Réti zörgőfű"}
Crepis capillaris	{"vékony zörgőfű","Vékony zörgőfű"}
Crepis nicaeensis	{"nizzai zörgőfű","Nizzai zörgőfű"}
Crepis paludosa	{"mocsári zörgőfű","Mocsári zörgőfű"}
Crepis pannonica	{"magyar zörgőfű","Magyar zörgőfű"}
Crepis polymorpha	{"pitypanglevelű zörgőfű","Pitypanglevelű zörgőfű"}
Crepis praemorsa	{"fürtös zörgőfű","Fürtös zörgőfű"}
Crepis pulchra	{"szép zörgőfű","Szép zörgőfű"}
Crepis rhoeadifolia	{"pipacslevelű zörgőfű","Pipacslevelű zörgőfű"}
Crepis setosa	{"Serteszőrös zörgőfű","serteszőrű zörgőfű"}
Crepis tectorum	{"hamvas zörgőfű","Hamvas zörgőfű"}
Crocus albiflorus	{"fehér sáfrány","Fehér sáfrány"}
Paeonia suffruticosa	{"Fás bazsarózsa"}
Crocus heuffelianus	{"kárpáti sáfrány","Kárpáti sáfrány"}
Crocus reticulatus	{"tarka sáfrány","Tarka sáfrány"}
Crocus tommasinianus	{"illír sáfrány","Illír sáfrány"}
Crocus vittatus	{"halvány sáfrány","Halvány sáfrány"}
Cruciata glabra	{"tavaszi keresztfű","Tavaszi keresztfű"}
Cruciata laevipes	{"mezei keresztfű","Mezei keresztfű"}
Cruciata pedemontana	{"apró keresztfű","Apró keresztfű"}
Crupina vulgaris	{magvasodró,Magvasodró}
Crypsis aculeata	{Bajuszpázsit,"hegyes bajuszpázsit"}
Cucubalus baccifer	{"közönséges szegfűbogyó",Szegfűbogyó}
Cucurbita pepo	{"közönséges tök","Közönséges tök"}
Cuscuta campestris	{"nagy aranka","Nagy aranka"}
Cuscuta epilinum	{"lenfojtó aranka","Lenfojtó aranka"}
Cuscuta epithymum subsp. kotschyi	{"kakukkfű aranka",Kakukkfű-aranka}
Cuscuta europaea	{"közönséges aranka","Közönséges aranka"}
Cuscuta lupuliformis	{"komlóképű aranka","Komlóképű aranka"}
Cyclamen purpurascens	{Ciklámen,"erdei ciklámen"}
Cydonia oblonga	{Birsalma,"közönséges birs"}
Cymbalaria muralis	{"kőfali pintyő",Pintyő}
Cynodon dactylon	{csillagpázsit,Csillagpázsit}
Cynoglossum hungaricum	{"magyar ebnyelvfű","Magyar ebnyelvfű"}
Cynoglossum officinale	{"közönséges ebnyelvfű","Közönséges ebnyelvfű"}
Cynosurus cristatus	{"taréjos cincor","Taréjos cincor"}
Cynosurus echinatus	{"tüskés cincor","Tüskés cincor"}
Cyperus difformis	{rizspalka,Rizspalka}
Cyperus esculentus	{"Ehető palka",mandulapalka}
Cyperus fuscus	{"barna palka","Barna palka"}
Cystopteris fragilis	{hólyagpáfrány,Hólyagpáfrány}
Cytisus procumbens	{"elfekvő sziklai-zanót","Sziklai zanót"}
Dactylis glomerata	{"csomós ebír","Csomós ebír"}
Dactylis polygama	{"erdei ebír","Erdei ebír"}
Dactylorhiza fuchsii	{"erdei ujjaskosbor","Erdei ujjaskosbor"}
Dactylorhiza incarnata	{"hússzínű ujjaskosbor","Hússzínű ujjaskosbor"}
Dactylorhiza lapponica	{"lappföldi ujjaskosbor","Lappföldi ujjaskosbor"}
Dactylorhiza maculata	{"foltos ujjaskosbor","Foltos ujjaskosbor"}
Dactylorhiza majalis	{"széleslevelű ujjaskosbor","Széleslevelű ujjaskosbor"}
Dactylorhiza ochroleuca	{"halvány ujjaskosbor","Széleslevelű ujjaskosbor"}
Dactylorhiza sambucina	{"bodzaszagú ujjaskosbor","Bodzaszagú ujjaskosbor"}
Danthonia alpina	{fogtekercs,Fogtekercs}
Daphne cneorum	{"henye boroszlán","Henye boroszlán"}
Daphne laureola	{babérboroszlán,Babérboroszlán}
Daphne mezereum	{farkasboroszlán,Farkasboroszlán}
Datura stramonium	{"csattanó maszlag","Csattanó maszlag"}
Daucus carota	{vadmurok,Vadmurok}
Deschampsia flexuosa	{"erdei sédbúza","Erdei sédbúza"}
Descurainia sophia	{"parlagi sebforrasztófű",Sebforrasztófű}
Dianthus arenarius subsp. borussicus	{"balti szegfű","Balti szegfű"}
Dianthus armeria	{"szeplős szegfű","Szeplős szegfű"}
Dianthus armeria subsp. armeriastrum	{"déli szegfű","Déli szeplős szegfű"}
Dianthus barbatus	{"Szakállas szegfű","török szegfű"}
Dianthus carthusianorum	{"barát szegfű",Barátszegfű}
Dianthus carthusianorum subsp. carthusianorum	{barátszegfű,"Kisvirágú barátszegfű"}
Dianthus carthusianorum subsp. latifolius	{"sokvirágú barát szegfű","Sokvirágú barátszegfű"}
Dianthus collinus	{"dunai szegfű","Dunai szegfű"}
Dianthus collinus subsp. glabriusculus	{"kopasz dunai szegfű","Kopasz dunai szegfű"}
Dianthus deltoides	{"réti szegfű","Réti szegfű"}
Dianthus diutinus	{"tartós szegfű","Tartós szegfű"}
Dianthus giganteiformis	{"nagy szegfű","Nagy szegfű"}
Dianthus serotinus	{"kései szegfű","Kései szegfű"}
Dianthus superbus	{"buglyos szegfű","Buglyos szegfű"}
Digitalis ferruginea	{"rozsdás gyűszűvirág","Rozsdás gyűszűvirág"}
Digitalis grandiflora	{"sárga gyűszűvirág","Sárga gyűszűvirág"}
Digitalis lanata	{"gyapjas gyűszűvirág","Gyapjas gyűszűvirág"}
Digitalis purpurea	{"piros gyűszűvirág","Piros gyűszűvirág"}
Digitaria ischaemum	{"apró ujjasmuhar","Apró ujjasmuhar"}
Digitaria sanguinalis	{"pirók ujjasmuhar",Pirók-ujjasmuhar}
Diplotaxis erucoides	{"fehér kányazsázsa","Fehér kányazsázsa"}
Diplotaxis muralis	{"fali kányazsázsa","Fali kányazsázsa"}
Diplotaxis tenuifolia	{"szabdalt kányazsázsa","Szabdalt kányazsázsa"}
Dipsacus laciniatus	{héjakútmácsonya,Héjakútmácsonya}
Doronicum austriacum	{"osztrák zergevirág","Osztrák zergevirág"}
Doronicum hungaricum	{"magyar zergevirág","Magyar zergevirág"}
Doronicum orientale	{"keleti zergevirág","Keleti zergevirág"}
Dorycnium germanicum	{"selymes dárdahere","Selymes dárdahere"}
Dorycnium herbaceum	{"zöld dárdahere","Zöld dárdahere"}
Draba lasiocarpa	{"kövér daravirág","Kövér daravirág"}
Draba muralis	{"kövi daravirág","Kövi daravirág"}
Draba nemorosa	{"ligeti daravirág","Ligeti daravirág"}
Dracocephalum austriacum	{"osztrák sárkányfű","Osztrák sárkányfű"}
Dracocephalum ruyschiana	{"északi sárkányfű","Északi sárkányfű"}
Drosera anglica	{"hosszúlevelű harmatfű","Hosszúlevelű harmatfű"}
Drosera rotundifolia	{"kereklevelű harmatfű","Kereklevelű harmatfű"}
Dryopteris carthusiana	{"szálkás pajzsika","Szálkás pajzsika"}
Dryopteris cristata	{"tarajos pajzsika","Tarajos pajzsika"}
Dryopteris dilatata	{"széles pajzsika","Széles pajzsika"}
Dryopteris expansa	{"hegyi pajzsika","Hegyi pajzsika"}
Dryopteris filix-mas	{"erdei pajzsika","Erdei pajzsika"}
Ecballium elaterium	{Magrúgó,"uborkás magrógú"}
Echinochloa crus-galli	{"közönséges kakaslábfű","Közönséges kakaslábfű"}
Echinochloa occidentalis	{"tömött kakaslábfű","Tömött kakaslábfű"}
Echinochloa phyllopogon	{"szakállas kakaslábfű","Szakállas kakaslábfű"}
Echinocystis lobata	{süntök,Süntök}
Echinops sphaerocephalus	{"fehér szamárkenyér","Fehér szamárkenyér"}
Echium italicum	{"magas kígyószisz","Magas kígyószisz"}
Echium vulgare	{"terjőke kígyószisz","Terjőke kígyószisz"}
Elaeagnus angustifolia	{"keskenylevelű ezüstfa","Keskenylevelű ezüstfa"}
Elatine alsinastrum	{pocsolyalátonya,Pocsolyalátonya}
Elatine hexandra	{"cseplesz látonya","Cseplesz látonya"}
Elatine hungarica	{"magyar látonya","Magyar látonya"}
Elatine hydropiper	{"csigásmagvú látonya","Csigásmagvú látonya"}
Elatine triandra	{"háromporzós látonya","Háromporzós látonya"}
Eleocharis acicularis	{"apró csetkáka","Apró csetkáka"}
Eleocharis austriaca	{"osztrák csetkáka","Osztrák csetkáka"}
Eleocharis carniolica	{"sűrű csetkáka","Sűrű csetkáka"}
Eleocharis mamillata	{"szemcsés csetkáka","Szemcsés csetkáka"}
Eleocharis ovata	{"tojásdad csetkáka","Tojásdad csetkáka"}
Eleocharis palustris	{"mocsári csetkáka","Mocsári csetkáka"}
Eleocharis uniglumis	{"egypelyvás csetkáka","Egypelyvás csetkáka"}
Eleusine indica	{aszályfű,Aszályfű}
Ephedra distachya	{csikófark,"Közönséges csikófark"}
Epilobium angustifolium	{"erdei deréce","Erdei deréce"}
Epilobium collinum	{"dombi füzike","Dombi füzike"}
Epilobium dodonaei	{"vízparti deréce","Vízparti deréce"}
Epilobium hirsutum	{"borzas füzike","Borzas füzike"}
Epilobium lanceolatum	{"lándzsás füzike","Lándzsás füzike"}
Epilobium montanum	{"erdei füzike","Erdei füzike"}
Epilobium obscurum	{"sötétzöld füzike","Sötétzöld füzike"}
Epilobium palustre	{"mocsári füzike","Mocsári füzike"}
Epilobium parviflorum	{"kisvirágú füzike","Kisvirágú füzike"}
Epilobium roseum	{"rózsás füzike","Rózsás füzike"}
Epilobium tetragonum	{"négyélű füzike","Négyélű füzikesy"}
Epipactis albensis	{"elbai nőszőfű","Elbai nőszőfű"}
Epipactis bugacensis	{"bugaci nőszőfű","Bugaci nőszőfű"}
Epipactis gracilis	{"karcsú nőszőfű","Karcsú nőszőfű"}
Epipactis helleborine	{"széleslevelű nőszőfű","Széleslevelű nőszőfű"}
Epipactis microphylla	{"kislevelű nőszőfű","Kislevelű nőszőfű"}
Epipactis palustris	{"mocsári nőszőfű","Mocsári nőszőfű"}
Epipactis pontica	{"pontuszi nőszőfű","Pontuszi nőszőfű"}
Linum austriacum	{"hegyi len","Hegyi len"}
Equisetum arvense	{"mezei zsurló","Mezei zsurló"}
Equisetum fluviatile	{iszapzsurló,Iszapzsurló}
Equisetum hyemale	{"téli zsurló","Téli zsurló"}
Equisetum palustre	{"mocsári zsurló","Mocsári zsurló"}
Equisetum ramosissimum	{"hosszú zsurló","Hosszú zsurló"}
Equisetum sylvaticum	{"erdei zsurló","Erdei zsurló"}
Equisetum telmateia	{"óriás zsurló","Óriás zsurló"}
Equisetum variegatum	{"tarka zsurló","Tarka zsurló"}
Eragrostis minor	{"kis tőtippan","Kis tőtippan"}
Eragrostis parviflora	{"kisvirágú tőtippan","Kisvirágú tőtippan"}
Eragrostis pilosa	{"szőrös tőtippan","Szőrös tőtippan"}
Erigeron acris subsp. angulosus	{"kopaszodó küllőrojt","Kopaszodó küllőrojt"}
Erigeron acris subsp. macrophyllus	{"kárpáti küllőrojt","Kárpáti küllőrojt"}
Eriophorum angustifolium	{"keskenylevelű gyapjúsás","Keskenylevelű gyapjúsás"}
Eriophorum gracile	{"vékony gyapjúsás","Vékony gyapjúsás"}
Eriophorum latifolium	{"széleslevelű gyapjúsás","Széleslevelű gyapjúsás"}
Eriophorum vaginatum	{"hüvelyes gyapjúsás","Hüvelyes gyapjúsás"}
Erodium ciconium	{"gerelyes gémorr","Gerelyes gémorr"}
Erodium cicutarium	{bürök-gémorr,Bürökgémorr}
Erophila verna	{"tavaszi ködvirág","Tavaszi ködvirág"}
Eruca sativa	{Borsmustár,"kerti borsmustár"}
Erucastrum gallicum	{"francia nyurgaszáj","Francia nyurgaszál"}
Erucastrum nasturtiifolium	{"ártéri nyurgaszáj","Ártéri nyurgaszál"}
Eryngium campestre	{"mezei iringó","Mezei iringó"}
Eryngium planum	{"kék iringó","Kék iringó"}
Erysimum cheiranthoides	{"violás repcsény","Violás repcsény"}
Erysimum crepidifolium	{"sziklai repcsény","Sziklai repcsény"}
Erysimum diffusum	{"szürke repcsény","Szürke repcsény"}
Erysimum hieracifolium	{"vízparti repcsény","Vízparti repcsény"}
Erysimum odoratum	{"magyar repcsény","Magyar repcsény"}
Erysimum odoratum subsp. buekkense	{"bükki magyar repcsény","Bükki magyar repcsény"}
Erysimum repandum	{"fürtös repcsény","Fürtös repcsény"}
Euclidium syriacum	{"madárorrú táskazár",Táskazár}
Euonymus verrucosus	{"bibircses kecskerágó","Bibircses kecskerágó"}
Eupatorium cannabinum	{sédkender,Sédkender}
Euphorbia amygdaloides	{"erdei kutyatej","Erdei kutyatej"}
Euphorbia angulata	{"szögletes kutyatej","Szögletes kutyatej"}
Euphorbia cyparissias	{farkaskutyatej,Farkaskutyatej}
Euphorbia dulcis	{"édes kutyatej","Édes kutyatej"}
Euphorbia epithymoides	{"színeváltó kutyatej","Színeváltó kutyatej"}
Euphorbia esula	{sárkutyatej,Sárkutyatej}
Euphorbia exigua	{"apró kutyatej","Apró kutyatej"}
Euphorbia falcata	{tarlókutyatej,Tarlókutyatej}
Euphorbia helioscopia	{"napraforgó kutyatej",Napraforgó-kutyatej}
Euphorbia lathyris	{"kerti kutyatej","Nagy sárfű"}
Euphorbia lucida	{"fényes kutyatej","Fényes kutyatej"}
Euphorbia myrsinites	{"Délszaki kutyatej","szürke kutyatej"}
Euphorbia palustris	{"mocsári kutyatej","Mocsári kutyatej"}
Euphorbia peplus	{"vézna kutyatej","Vézna kutyatej"}
Euphorbia platyphyllos	{"nagylevelű kutyatej","Nagylevelű kutyatej"}
Euphorbia salicifolia	{"fűzlevelű kutyatej","Fűzlevelű kutyatej"}
Euphorbia segetalis	{"vetési kutyatej","Vetési kutyatej"}
Euphorbia stricta	{"merev kutyatej","Merev kutyatej"}
Euphorbia taurinensis	{"görög kutyatej","Görög kutyatej"}
Euphorbia verrucosa	{"bibircses kutyatej","Bibircses kutyatej"}
Euphorbia villosa	{"bozontos kutyatej","Bozontos kutyatej"}
Euphorbia virgata	{"vesszős kutyatej","Vesszős kutyatej"}
Euphrasia rostkoviana	{"orvosi szemvidító","Orvosi szemvidító"}
Euphrasia stricta	{"közönséges szemvidító","Közönséges szemvidító"}
Fagopyrum esculentum	{pohánka,Pohánka}
Fagus sylvatica	{bükk,"Közönséges bükk"}
Fagus sylvatica subsp. moesiaca	{"keleti bükk","Mőziai bükk"}
Falcaria vulgaris	{sarlófű,Sarlófű}
Fallopia convolvulus	{szulákkeserűfű,Szulákkeserűfű}
Fallopia dumetorum	{sövénykeserűfű,Sövénykeserűfű}
Festuca altissima	{"erdei csenkesz","Erdei csenkesz"}
Festuca amethystina	{"lila csenkesz","Lila csenkesz"}
Festuca arundinacea	{"nádképű csenkesz","Nádképű csenkesz"}
Festuca dalmatica	{"dalmát csenkesz","Dalmát csenkesz"}
Festuca drymeia	{"hegyi csenkesz","Hegyi csenkesz"}
Festuca gigantea	{"óriás csenkesz","Óriás csenkesz"}
Festuca heterophylla	{"felemáslevelű csenkesz","Felemáslevelű csenkesz"}
Festuca nigrescens	{"feketés csenkesz","Zsombékoló csenkesz"}
Festuca ovina	{juhcsenkesz,Juhcsenkesz}
Festuca pallens	{"deres csenkesz","Deres csenkesz"}
Festuca pratensis	{"réti csenkesz","Réti csenkesz"}
Festuca pseudodalmatica	{"sziklai csenkesz","Sziklai csenkesz"}
Festuca pseudovina	{"sziki csenkesz","Sziki csenkesz"}
Festuca rubra	{"veres csenkesz","Veres csenkesz"}
Festuca rupicola	{"pusztai csenkesz","Pusztai csenkesz"}
Festuca vaginata	{"Homoki csenkesz","magyar csenkesz"}
Festuca valesiaca	{"vékony csenkesz","Vékony csenkesz"}
Ficus carica	{füge,"Közönséges füge"}
Filago vulgaris	{"Közönséges penészvirág","német penészvirág"}
Filipendula ulmaria	{"réti legyezőfű","Réti legyezőfű"}
Filipendula vulgaris	{"koloncos legyezőfű","Koloncos legyezőfű"}
Fragaria moschata	{"fahéjillatú szamóca","Fahéjillatú szamóca"}
Fragaria vesca	{"erdei szamóca","Erdei szamóca"}
Fragaria viridis	{"csattogó szamóca","Csattogó szamóca"}
Frangula alnus	{kutyabenge,Kutyabenge}
Fraxinus excelsior	{"magas kőris","Magas kőris"}
Fraxinus ornus	{"virágos kőris","Virágos kőris"}
Fumana procumbens	{"heverő naprózsa",Naprózsa}
Fumaria officinalis	{"Csőrös füstike","orvosi füstike"}
Fumaria parviflora	{"kisvirágú füstike","Szürke füstike"}
Fumaria rostellata	{"csőrös füstike",Füstike}
Fumaria schleicheri	{"közönséges füstike","Orvosi füstike"}
Fumaria vaillantii	{"Közönséges füstike","szürke füstike"}
Gagea bohemica	{"cseh tyúktaréj","Cseh tyúktaréj"}
Gagea lutea	{"sárga tyúktaréj","Sárga tyúktaréj"}
Gagea minima	{"apró tyúktaréj","Apró tyúktaréj"}
Gagea pratensis	{"mezei tyúktaréj","Mezei tyúktaréj"}
Gagea pusilla	{"kis tyúktaréj","Kis tyúktaréj"}
Gagea spathacea	{"fiókás tyúktaréj","Fiókás tyúktaréj"}
Gaillardia aristata	{"sárga kokárdavirág","Sárga kokárdavirág"}
Galanthus nivalis	{hóvirág,Hóvirág}
Galega officinalis	{kecskeruta,Kecskeruta}
Galeobdolon argentatum	{"ezüstös sárgaárvacsalán","Hegyi sárgaárvacsalán"}
Galeobdolon luteum	{"erdei sárgaárvacsalán",Sárgaárvacsalán}
Galeopsis bifida	{"vágási kenderkefű","Vágási kenderkefű"}
Galeopsis ladanum	{"piros kenderkefű","Piros kenderkefű"}
Galeopsis pubescens	{"pelyhes kenderkefű","Pelyhes kenderkefű"}
Galeopsis segetum	{"vetési kenderkefű","Vetési kenderkefű"}
Galeopsis speciosa	{"szőrös kenderkefű","Szőrös kenderkefű"}
Galeopsis tetrahit	{"tarka kenderkefű","Tarka kenderkefű"}
Galinsoga parviflora	{"kicsiny gombvirág","Kicsiny gombvirág"}
Galium abaujense	{"abaúji galaj","Abaúji galaj"}
Galium album	{"Fehér galaj","felálló galaj"}
Galium aparine	{"ragadós galaj","Ragadós galaj"}
Galium boreale	{"északi galaj","Északi galaj"}
Galium divaricatum	{"berzedt galaj","Berzedt galaj"}
Galium elongatum	{"megnyúlt galaj","Megnyúlt galaj"}
Galium glaucum	{"szürke galaj","Szürke galaj"}
Galium glaucum subsp. hirsutum	{"szőrös szürke galaj","Szőrös szürke galaj"}
Galium lucidum	{"fényes galaj","Fényes galaj"}
Galium mollugo	{"közönséges galaj","Közönséges galaj"}
Galium odoratum	{"szagos müge","Szagos müge"}
Galium palustre	{"mocsári galaj","Mocsári galaj"}
Galium parisiense	{"párizsi galaj","Párizsi galaj"}
Galium pumilum	{"törpe galaj","Törpe galaj"}
Galium rivale	{"kapaszkodó galaj","Kapaszkodó galaj"}
Galium rotundifolium	{"kereklevelű galaj","Kereklevelű galaj"}
Galium rubioides	{"réti galaj","Réti galaj"}
Galium schultesii	{"fénytelen galaj","Fénytelen galaj"}
Galium spurium subsp. infestum	{"vetési galaj","Vetési gyanus galaj"}
Galium sylvaticum	{"erdei galaj","Erdei galaj"}
Galium tenuissimum	{"vékony galaj","Vékony galaj"}
Galium tricornutum	{"háromszarvú galaj","Háromszarvú galaj"}
Galium uliginosum	{"lápi galaj","Lápi galaj"}
Galium verum	{"tejoltó galaj","Tejoltó galaj"}
Gaudinia fragilis	{füzérzab,Füzérzab}
Genista germanica	{"sváb rekettye","Sváb rekettye"}
Genista ovata subsp. nervata	{"szőrös rekettye","Szőrös rekettye"}
Genista pilosa	{"selymes rekettye","Selymes rekettye"}
Genista tinctoria	{"festő rekettye","Festő rekettye"}
Genista tinctoria subsp. elatior	{"magas festő rekettye","Magas festő rekettye"}
Gentiana asclepiadea	{fecsketárnics,Fecsketárnics}
Gentiana pneumonanthe	{kornistárnics,Kornistárnics}
Geranium bohemicum	{"cseh gólyaorr","Cseh gólyaorr"}
Geranium columbinum	{"galambláb gólyaorr",Galambláb-gólyaorr}
Geranium dissectum	{"sallangos gólyaorr","Sallangos gólyaorr"}
Geranium divaricatum	{"berzedt gólyaorr","Berzedt gólyaorr"}
Geranium lucidum	{"csillogó gólyaorr","Csillogó gólyaorr"}
Geranium molle	{"Puha gályaorr","puha gólyaorr"}
Geranium palustre	{"mocsári gólyaorr","Mocsári gólyaorr"}
Geranium phaeum	{"fodros gólyaorr","Fodros gólyaorr"}
Geranium pratense	{"mezei gólyaorr","Mezei gólyaorr"}
Geranium pusillum	{"apró gólyaorr","Apró gólyaorr"}
Geranium pyrenaicum	{"pireneusi gólyaorr","Pireneusi gólyaorr"}
Geranium robertianum	{"nehézszagú gólyaorr","Nehézszagú gólyaorr"}
Geranium rotundifolium	{"kereklevelű gólyaorr","Kereklevelű gólyaorr"}
Geranium sanguineum	{"piros gólyaorr","Piros gólyaorr"}
Geranium sibiricum	{"szibériai gólyaorr","Szibériai gólyaorr"}
Geranium sylvaticum	{"erdei gólyaorr","Erdei gólyaorr"}
Geum aleppicum	{"hegyi gyömbérgyökér","Hegyi gyömbérgyökér"}
Geum urbanum	{"erdei gyömbérgyökér","Erdei gyömbérgyökér"}
Gladiolus imbricatus	{"réti kardvirág","Réti kardvirág"}
Gladiolus palustris	{"mocsári kardvirág","Mocsári kardvirág"}
Glaucium corniculatum	{"vörös szarumák","Vörös szarumák"}
Glaucium flavum	{"sárga szarumák","Sárga szarumák"}
Glaux maritima	{Bagolyfű,"tengerparti bagolyfű"}
Glechoma hederacea	{"kerek repkény","Kerek repkény"}
Glechoma hirsuta	{"borzas repkény","Borzas repkény"}
Gleditsia triacanthos	{"tövises lepényfa","Tövises lepényfa"}
Globularia punctata	{"magas gubóvirág","Magas gubóvirág"}
Glyceria declinata	{"kékeszöld harmatkása","Kékeszöld harmatkása"}
Glyceria fluitans	{"réti harmatkása","Réti harmatkása"}
Glyceria maxima	{"vízi harmatkása","Vízi harmatkása"}
Glyceria nemoralis	{"berki harmatkása","Berki harmatkása"}
Glycine soja	{Bab,"kerti szójabab"}
Glycyrrhiza echinata	{"keserű édesgyökér","Keserű édesgyökér"}
Glycyrrhiza glabra	{"igazi édesgyökér","Igazi édesgyökér"}
Gratiola officinalis	{Csikorgófű,"orvosi csikorgófű"}
Gymnadenia conopsea	{"szúnyoglábú bibircsvirág","Szúnyoglábú bibircsvirág"}
Gymnadenia odoratissima	{"illatos bibircsvirág","Jószagú bibircsvirág"}
Gypsophila fastigiata subsp. arenaria	{"homoki fátyolvirág","Homoki fátyolvirág"}
Gypsophila muralis	{"mezei fátyolvirág","Mezei fátyolvirág"}
Gypsophila paniculata	{"buglyos fátyolvirág","Buglyos fátyolvirág"}
Hedera helix	{borostyán,"Közönséges borostyán"}
Helianthemum canum	{"szürke napvirág","Szürke napvirág"}
Helianthemum nummularium	{"molyhos napvirág","Molyhos napvirág"}
Helianthemum ovatum	{"közönséges napvirág","Molyhos napvirág"}
Helianthus annuus	{"közönséges napraforgó","Közönséges napraforgó"}
Helianthus rigidus	{"érdes napraforgó","Érdes napraforgó"}
Helianthus tuberosus	{csicsóka,Csicsóka}
Helichrysum arenarium	{"homoki szalmagyopár","Homoki szalmagyopár"}
Heliotropium europaeum	{"európai kunkor","Európai kunkor"}
Heliotropium supinum	{"henye kunkor","Henye kunkor"}
Helleborus dumetorum	{"kisvirágú hunyor","Kisvirágú hunyor"}
Helleborus odorus	{"illatos hunyor","Illatos hunyor"}
Helleborus purpurascens	{"pirosló hunyor","Pirosló hunyor"}
Helleborus viridis	{"Zöld hunyor","zöldvirágú hunyor"}
Hemerocallis fulva	{"lángszínű sásliliom","Lángszínű sásliliom"}
Heracleum mantegazzianum	{"kaukázusi medvetalp","Kaukázusi medvetalp"}
Heracleum sphondylium	{"közönséges medvetalp","Közönséges medvetalp"}
Heracleum sphondylium subsp. flavescens	{"Sárgás közönséges medvetalp","sárgás medvetalp"}
Heracleum sphondylium subsp. trachycarpum	{"Szőröstermésű közönséges medvetalp","szőröstermésű medvetalp"}
Herminium monorchis	{minka,Minka}
Herniaria glabra	{"kopasz porcika","Kopasz porcika"}
Herniaria hirsuta	{"borzas porcika","Borzas porcika"}
Herniaria incana	{"szürke porcika","Szürke porcika"}
Hesperis matronalis subsp. candida	{"fehér hölgyestike","Fehér hölgyestike"}
Hesperis matronalis subsp. spontanea	{"vad hölgyestike","Vad hölgyestike"}
Hesperis sylvestris	{"erdei estike","Erdei estike"}
Hesperis tristis	{"szomorú estike","Szomorú estike"}
Hibiscus trionum	{varjúmák,Varjúmák}
Hieracium aurantiacum	{"rezes hölgymál","Rezes hölgymál"}
Hieracium auriculoides	{"füles hölgymál","Füles hölgymál"}
Hieracium bauhinii	{"magas hölgymál","Magas hölgymál"}
Hieracium bifidum	{"villás hölgymál","Villás hölgymál"}
Hieracium caesium	{"deres hölgymál","Deres hölgymál"}
Hieracium caespitosum	{"réti hölgymál","Réti hölgymál"}
Hieracium cymosum	{"csomós hölgymál","Csomós hölgymál"}
Hieracium echioides	{"szúrós hölgymál","Szúrós hölgymál"}
Hieracium hoppeanum	{"zömök hölgymál","Zömök hölgymál"}
Hieracium lactucella	{"füles hölgymál","Füles hölgymál"}
Hieracium laevigatum	{"élesfogú hölgymál","Élesfogú hölgymál"}
Hieracium maculatum	{"foltos hölgymál","Foltos hölgymál"}
Hieracium pallidum	{"halványszürke hölgymál","Halványszürke hölgymál"}
Hieracium pilosella	{"ezüstös hölgymál","Ezüstös hölgymál"}
Hieracium piloselloides	{"firenzei hölgymál","Firenzei hölgymál"}
Hieracium racemosum	{"fürtös hölgymál","Fürtös hölgymál"}
Hieracium sabaudum	{"olasz hölgymál","Olasz hölgymál"}
Hieracium umbellatum	{"ernyős hölgymál","Ernyős hölgymál"}
Himantoglossum adriaticum	{"adriai sallangvirág","Adriai sallangvirág"}
Hippocrepis comosa	{patkócím,Patkófű}
Hippophaë rhamnoides subsp. carpatica	{"európai homoktövis","Kárpáti homoktövis"}
Hippuris vulgaris	{"közönséges vízilófark",Vízi-lófark}
Holcus lanatus	{"Pelyhes elyemperje","pelyhes selyemperje"}
Holcus mollis	{"lágy selyemperje","Lágy selyemperje"}
Holosteum umbellatum	{olocsán,Olocsán}
Holosteum umbellatum subsp. glutinosum	{"ragadós olocsán","Ragadós olocsán"}
Hordelymus europaeus	{"erdei hajperje",Hajperje}
Hordeum marinum	{"tengerparti árpa","Tengerparti árpa"}
Hordeum murinum	{egérárpa,Egérárpa}
Hordeum murinum subsp. leporinum	{"Déli egérárpa",egérárpa}
Hornungia petraea	{"apró szirtőr",Szirtőr}
Humulus lupulus	{"felfutó komló","Felfutó komló"}
Humulus scandens	{"japán komló","Japán komló"}
Hydrocharis morsus-ranae	{békatutaj,Békatutaj}
Hyoscyamus niger	{beléndek,Beléndek}
Hypericum barbatum	{"szakállas orbáncfű","Szakállas orbáncfű"}
Hypericum elegans	{"karcsú orbáncfű","Karcsú orbáncfű"}
Hypericum hirsutum	{"borzas orbáncfű","Borzas orbáncfű"}
Hypericum humifusum	{"heverő orbáncfű","Heverő orbáncfű"}
Hypericum maculatum	{"pettyes orbáncfű","Pettyes orbáncfű"}
Hypericum montanum	{"hegyi orbáncfű","Hegyi orbáncfű"}
Hypericum perforatum	{"közönséges orbáncfű","Közönséges orbáncfű"}
Hypericum tetrapterum	{"mocsári orbáncfű","Mocsári orbáncfű"}
Hyssopus officinalis	{Izsóp,"kerti izsóp"}
Ilex aquifolium	{"közönséges magyal","Közönséges magyal"}
Impatiens glandulifera	{"bíbor nebáncsvirág","Bíbor nebáncsvirág"}
Impatiens noli-tangere	{"erdei nebáncsvirág","Erdei nebáncsvirág"}
Impatiens parviflora	{"kisvirágú nebáncsvirág","Kisvirágú nebáncsvirág"}
Inula britannica	{"réti peremizs","Réti peremizs"}
Inula conyza	{"erdei peremizs","Erdei peremizs"}
Inula ensifolia	{"kardos peremizs","Kardos peremizs"}
Inula germanica	{"hengeresfészkű peremizs","Hengeresfészkű peremizs"}
Inula helenium	{örménygyökér,Örménygyökér}
Inula hirta	{"borzas peremizs","Borzas peremizs"}
Inula oculus-christi	{"selymes peremizs","Selymes peremizs"}
Inula salicina	{"fűzlevelű peremizs","Fűzlevelű peremizs"}
Inula salicina subsp. aspera	{"keménylevelű peremizs","Keménylevelű peremizs"}
Inula salicina subsp. denticulata	{"fogazottlevelű peremizs","Fogazottlevelű peremizs"}
Inula spiraeifolia	{"baranyai peremizs","Baranyai peremizs"}
Ipomoea purpurea	{"bíboros hajnalka","Bíboros hajnalka"}
Iris aphylla subsp. hungarica	{"magyar nőszirom","Magyar nőszirom"}
Iris arenaria	{"homoki nőszirom","Homoki nőszirom"}
Iris germanica	{"kék nőszirom","Kék nőszirom"}
Iris graminea	{"pázsitos nőszirom","Pázsitos nőszirom"}
Iris graminea subsp. pseudocyperus	{"palka-képű nőszirom","Sáslevelű nőszirom"}
Iris pseudacorus	{"sárga nőszirom","Sárga nőszirom"}
Iris sibirica	{"szibériai nőszirom","Szibériai nőszirom"}
Iris variegata	{"tarka nőszirom","Tarka nőszirom"}
Isatis tinctoria	{"festő csülleng","Festő csülleng"}
Isatis tinctoria subsp. praecox	{"magyar csülleng","Magyar csülleng"}
Isopyrum thalictroides	{galambvirág,Galambvirág}
Jasione montana	{"hegyi kékcsillag",Kékcsillag}
Jovibarba sobolifera	{"gömbös kövirózsa","Gömbös kövirózsa"}
Juglans nigra	{"fekete dió","Fekete dió"}
Juglans regia	{"közönséges dió","Közönséges dió"}
Juncus articulatus	{fülemüleszittyó,Fülemüleszittyó}
Juncus atratus	{"fekete szittyó","Fekete szittyó"}
Juncus bufonius	{varangyszittyó,Varangyszittyó}
Juncus bulbosus	{"fonalas szittyó","Fonalas szittyó"}
Juncus capitatus	{"fejecses szittyó","Fejecses szittyó"}
Juncus compressus	{"réti szittyó","Réti szittyó"}
Juncus conglomeratus	{"csomós szittyó","Csomós szittyó"}
Juncus effusus	{békaszittyó,Békaszittyó}
Juncus inflexus	{"deres szittyó","Deres szittyó"}
Juncus maritimus	{"tengeri szittyó","Tengeri szittyó"}
Juncus sphaerocarpus	{"gömböstermésű szittyó","Gömbtermésű szittyó"}
Juncus subnodulosus	{"nagy szittyó","Nagy szittyó"}
Juncus tenageia	{"iszap szittyó",Iszapszittyó}
Juncus tenuis	{"vékony szittyó","Vékony szittyó"}
Juniperus communis	{"közönséges boróka","Közönséges tűboróka"}
Jurinea mollis	{"kisfészkű hangyabogáncs","Kisfészkű hangyabogáncs"}
Jurinea mollis subsp. dolomitica	{dolomit-hangyabogáncs,Dolomit-hangyabogáncs}
Kickxia elatine	{"cseplesz tátika","Cseplesz tátika"}
Kickxia spuria	{"kétszínű tátika","Kétszínű tátika"}
Knautia arvensis	{"mezei varfű","Mezei varfű"}
Knautia arvensis subsp. pannonica	{"Magyar mezei varfű","magyar varfű"}
Knautia arvensis subsp. rosea	{"rószás mezei varfű","Rózsás mezei varfű"}
Knautia dipsacifolia	{"erdei varfű","Erdei varfű"}
Knautia drymeia	{"magyar varfű","Magyar varfű"}
Knautia kitaibelii subsp. tomentella	{Kitaibell-varfű,Kitaibel-varfű}
Koeleria cristata	{"karcsú fényperje","Karcsú fényperje"}
Koeleria glauca	{"deres fényperje","Deres fényperje"}
Koeleria majoriflora	{"nagyvirágú fényperje","Nagyvirágú fényperje"}
Koelreuteria paniculata	{"bugás csörgőfa","Bugás csörgőfa"}
Laburnum anagyroides	{aranyeső,"Közönséges aranyeső"}
Lactuca perennis	{"kék saláta","Kék saláta"}
Lactuca quercina	{"cserlevelű saláta","Cserlevelű saláta"}
Lactuca saligna	{"szálaslevelű saláta","Szálaslevelű saláta"}
Lactuca serriola	{"keszeg saláta","Keszeg saláta"}
Lactuca viminea	{"gatyás saláta","Gatyás saláta"}
Lamium album	{"fehér árvacsalán","Fehér árvacsalán"}
Lamium amplexicaule	{"bársonyos árvacsalán","Bársonyos árvacsalán"}
Lamium maculatum	{"foltos árvacsalán","Foltos árvacsalán"}
Lamium purpureum	{"piros árvacsalán","Piros árvacsalán"}
Lappula deflexa	{koldustetűfaj,"Kónya koldustetű"}
Lappula heteracantha	{"sziklai koldustetű","Sziklai koldustetű"}
Lappula patula	{"berzedt koldustetű","Berzedt koldustetű"}
Lappula squarrosa	{"bojtorjános koldustetű","Bojtorjános koldustetű"}
Lapsana communis	{"bojtorján saláta",Bojtorjánsaláta}
Larix decidua	{"európai vörösfenyő","Európai vörösfenyő"}
Laserpitium latifolium	{"széleslevelű bordamag","Széleslevelű bordamag"}
Laserpitium prutenicum	{"rutén bordamag","Rutén bordamag"}
Laser trilobum	{sujtár,Sujtár}
Lathraea squamaria	{vicsorgó,Vicsorgó}
Lathyrus aphaca	{"levéltelen lednek","Levéltelep lednek"}
Lathyrus hirsutus	{"borzas lednek","Borzas lednek"}
Lathyrus latifolius	{"nagyvirágú lednek","Nagyvirágú lednek"}
Lathyrus niger	{"fekete lednek","Fekete lednek"}
Lathyrus nissolia	{"kacstalan lednek","Kacstalan lednek"}
Lathyrus pallescens	{"sápadt lednek","Sápadt lednek"}
Lathyrus palustris	{"mocsári lednek","Mocsári lednek"}
Lathyrus pannonicus	{"magyar lednek","Magyar lednek"}
Lathyrus pannonicus subsp. collinus	{"koloncos lednek","Koloncos magyar lednek"}
Lathyrus pisiformis	{"borsóképű lednek","Borsóképű lednek"}
Lathyrus pratensis	{"réti lednek","Réti lednek"}
Lathyrus sativus	{"szegletes lednek","Szegletes lednek"}
Lathyrus sphaericus	{"téglaszínű lednek","Téglaszínű lednek"}
Lathyrus sylvestris	{"erdei lednek","Erdei lednek"}
Lathyrus tuberosus	{"gumós lednek","Gumós lednek"}
Lathyrus venetus	{"tarka lednek","Tarka lednek"}
Lathyrus vernus	{"tavaszi lednek","Tavaszi lednek"}
Lavandula angustifolia	{"közönséges levendula",Levendula}
Lavatera thuringiaca	{"parlagi madármályva","Parlagi madármályva"}
Leersia oryzoides	{Rizsfű,"vad rizsfű"}
Legousia speculum-veneris	{Tükörvirág,"vetési tükörvirág"}
Lemna gibba	{"púpos békalencse","Púpos békalencse"}
Lemna minor	{"apró békalencse","Apró békalencse"}
Lemna trisulca	{"keresztes békalencse","Keresztes békalencse"}
Lens culinaris	{"főzelék lencse",Főzeléklencse}
Leontodon autumnalis	{"őszi oroszlánfog","Őszi oroszlánfog"}
Leontodon hispidus	{"közönséges oroszlánfog","Közönséges oroszlánfog"}
Leontodon incanus	{"szőke oroszlánfog","Szőke oroszlánfog"}
Leontodon saxatilis	{Békapitypang,"sziklai oroszlánfog"}
Leonurus cardiaca subsp. villosus	{"Szőrös szúrós gyöngyajak","szúrós gyöngyajak"}
Leonurus marrubiastrum	{"pemete gyöngyajak",Pemetegyöngyajak}
Lepidium campestre	{"mezei zsázsa","Mezei zsázsa"}
Lepidium densiflorum	{"kisvirágú zsázsa","Kisvirágú zsázsa"}
Lepidium graminifolium	{"keskenylevelű zsázsa","Keskenylevelű zsázsa"}
Lepidium perfoliatum	{"felemás zsázsa","Felemás zsázsa"}
Lepidium ruderale	{"büdös zsázsa","Büdös zsázsa"}
Lepidium virginicum	{"amerikai zsázsa","Amerikai zsázsa"}
Leucojum aestivum	{"nyári tőzike","Nyári tőzike"}
Leucojum vernum	{"tavaszi tőzike","Tavaszi tőzike"}
Ligularia sibirica	{"szibériai hamuvirág","Szibériai hamuvirág"}
Ligustrum vulgare	{"Közönséges fagyal","vesszős fagyal"}
Lilium bulbiferum	{"tüzes liliom",Tűzliliom}
Lilium martagon	{turbánliliom,Turbánliliom}
Lilium martagon subsp. alpinum	{"havasi turbánliliom","Szórtlevelű turbánliliom"}
Limonium gmelini subsp. hungaricum	{"magyar sóvirág","Magyar sóvirág"}
Limosella aquatica	{iszaprojt,Iszaprojt}
Linaria angustissima	{"keskenylevelű gyújtoványfű","Keskenylevelű gyújtoványfű"}
Linaria arvensis	{"mezei gyújtoványfű","Mezei gyújtoványfű"}
Linaria genistifolia	{"rekettyelevelű gyújtoványfű","Rekettyelevelű gyújtoványfű"}
Linaria vulgaris	{"közönséges gyújtoványfű","Közönséges gyújtoványfű"}
Lindernia procumbens	{"heverő iszapfű",Iszapfű}
Linum catharticum	{békalen,Békalen}
Linum flavum	{"sárga len","Sárga len"}
Linum hirsutum	{"borzas len","Borzas len"}
Linum hirsutum subsp. glabrescens	{"Homoki borzas len","homoki len"}
Linum perenne	{"évelő len","Évelő len"}
Linum tenuifolium	{"árlevelű len","Árlevelű len"}
Linum trigynum	{"francia len","Francia len"}
Linum usitatissimum	{"házi len","Házi len"}
Listera ovata	{békakonty,Békakonty}
Lithospermum officinale	{"kőmagvú gyöngyköles","Kőmagvú gyöngyköles"}
Lolium multiflorum	{olaszperje,Olaszperje}
Lolium perenne	{angolperje,Angolperje}
Lolium remotum	{lenvadóc,Lenvadóc}
Lolium temulentum	{"szédítő vadóc","Szédítő vadóc"}
Lonicera caprifolium	{"jerikói lonc","Jerikói lonc"}
Lonicera nigra	{"fekete lonc","Fekete lonc"}
Lonicera xylosteum	{ükörkelonc,Ükörkelonc}
Loranthus europaeus	{sárgafagyöngy,"Sárgafagyöngy (fakín)"}
Lotus angustissimus	{"karcsú kerep","Karcsú kerep"}
Lotus corniculatus	{"szarvas kerep","Szarvas kerep"}
Lunaria annua	{"kerti holdviola","Kerti holdviola"}
Lunaria rediviva	{"erdei holdviola","Erdei holdviola"}
Lupinus albus	{"fehér csillagfürt","Fehér csillagfürt"}
Lupinus angustifolius	{"keskenylevelű csillagfürt","Keskenylevelű csillagfürt"}
Lupinus luteus	{"sárga csillagfürt","Sárga csillagfürt"}
Lupinus polyphyllus	{"erdei csillagfürt","Erdei csillagfürt"}
Luzula campestris	{"mezei perjeszittyó","Mezei perjeszittyó"}
Luzula forsteri	{"délvidéki perjeszittyó","Délvidéki perjeszittyó"}
Luzula luzuloides	{"fehér perjeszittyó","Fehér perjeszittyó"}
Luzula multiflora	{"sokvirágú perjeszittyó","Sokvirágú perjeszittyó"}
Luzula pallescens	{"halvány perjeszittyó","Halvány perjeszittyó"}
Luzula pilosa	{"pillás perjeszittyó","Pillás perjeszittyó"}
Lychnis coronaria	{"bársonyos kakukkszegfű","Bársonyos kakukkszegfű"}
Lychnis flos-cuculi	{"réti kakukkszegfű","Réti kakukkszegfű"}
Lycium barbarum	{"közönséges ördögcérna","Közönséges ördögcérna"}
Lycium chinense	{"kínai ördögcérna","Kínai ördögcérna"}
Lycopersicon esculentum	{Paradicsom,"termesztett paradicsom"}
Lycopodium annotinum	{"kígyózó korpafű","Kígyózó korpafű"}
Lycopodium clavatum	{"kapcsos korpafű","Kapcsos korpafű"}
Lycopus europaeus	{"vízi peszérce","Vízi peszérce"}
Lycopus exaltatus	{"magas peszérce","Magas peszérce"}
Lysimachia nemorum	{"berki lizinka","Berki lizinka"}
Lysimachia nummularia	{"pénzlevelű lizinka","Pénzlevelű lizinka"}
Lysimachia punctata	{"pettyegetett lizinka","Pettyegetett lizinka"}
Lysimachia thyrsiflora	{"fürtös lizinka","Fürtös lizinka"}
Lysimachia vulgaris	{"közönséges lizinka","Közönséges lizinka"}
Lythrum hyssopifolia	{"alacsony füzény","Alacsony füzény"}
Lythrum linifolium	{"lenlevelű füzény","Lenlevelű füzény"}
Lythrum salicaria	{"réti füzény","Réti füzény"}
Lythrum thesioides	{Zsellérkefüzény,"zsellérkeképű füzény"}
Lythrum tribracteatum	{"apró füzény","Apró füzény"}
Lythrum virgatum	{"vesszős füzény","Vesszős füzény"}
Maclura pomifera	{narancseperfa,Oszázsnarancs}
Mahonia aquifolium	{"Magyallevelű mahónia",mahónia}
Malaxis monophyllos	{"egylevelű lágyvirág","Egylevelű lágyvirág"}
Malcolmia africana	{Afrikairepcsény,"afrikai szegecsfű"}
Malus domestica	{"nemes alma","Nemes alma"}
Malus sylvestris	{"közönséges vadalma",Vadalma}
Malva alcea	{"érdes mályva","Érdes mályva"}
Malva moschata	{mályvafaj,"Pézsma mályva"}
Malva neglecta	{"papsajt mályva",Papsajtmályva}
Malva pusilla	{"apró mályva","Apró mályva"}
Malva sylvestris	{"erdei mályva","Erdei mályva"}
Marrubium peregrinum	{"fehér pemetefű","Fehér pemetefű"}
Marrubium vulgare	{"orvosi pemetefű","Orvosi pemetefű"}
Matricaria chamomilla	{"orvosi székfű","Orvosi székfű"}
Matricaria discoidea	{"sugártalan székfű","Sugártalan székfű"}
Matthiola incana	{"kerti viola","Kerti viola"}
Medicago arabica	{"arab lucena","Arab lucerna"}
Medicago falcata	{"sárkerep lucerna",Sárkereplucerna}
Medicago lupulina	{"komlós lucerna","Komlós lucerna"}
Medicago minima	{"apró lucerna","Apró lucerna"}
Medicago orbicularis	{"korongos lucerna","Korongos lucerna"}
Medicago prostrata	{"cseplesz lucerna","Cseplesz lucerna"}
Medicago rigidula	{"keménytövisű lucerna","Keménytövisű lucerna"}
Medicago sativa	{"takarmány lucerna",Takarmánylucerna}
Melampyrum arvense	{"mezei csormolya","Mezei csormolya"}
Melampyrum barbatum	{"szakállas csormolya","Szakállas csormolya"}
Melampyrum cristatum	{"taréjos csormolya","Taréjos csormolya"}
Melampyrum nemorosum	{"kéküstökű csormolya","Kéküstökű csormolya"}
Melampyrum pratense	{"réti csormolya","Réti csormolya"}
Melica altissima	{"magas gyöngyperje","Magas gyöngyperje"}
Melica ciliata	{"prémes gyöngyperje","Prémes gyöngyperje"}
Melica nutans	{"bókoló gyöngyperje","Bókoló gyöngyperje"}
Melica picta	{"tarka gyöngyperje","Tarka gyöngyperje"}
Melica transsilvanica	{"erdélyi gyöngyperje","Erdélyi gyöngyperje"}
Melica uniflora	{"egyvirágú gyöhgyperje","Egyvirágú gyöngyperje"}
Melilotus albus	{"fehér somkóró","Fehér somkóró"}
Melilotus dentatus	{"fogas somkóró","Fogas somkóró"}
Melilotus officinalis	{"orvosi somkóró","Orvosi somkóró"}
Melissa officinalis	{citromfű,Citromfű}
Melittis carpatica	{"nagyvirágú méhfű","Nagyvirágú méhfű"}
Melittis melissophyllum	{"déli méhfű","Déli méhfű"}
Mentha aquatica	{"vízi menta","Vízi menta"}
Mentha arvensis	{"mezei menta","Mezei menta"}
Mentha longifolia	{lómenta,Lómenta}
Mentha pulegium	{csombormenta,Csombormenta}
Mercurialis annua	{"egynyári szélfű","Egynyári szélfű"}
Mercurialis ovata	{"pusztai szélfű","Pusztai szélfű"}
Mercurialis perennis	{"erdei szélfű","Erdei szélfű"}
Mespilus germanica	{naspolya,Naspolya}
Milium effusum	{kásafű,Kásafű}
Minuartia glomerata	{"gomolyos kőhúr","Gomolyos kőhúr"}
Minuartia setacea	{"sziklai kőhúr","Sziklai kőhúr"}
Minuartia viscosa	{"enyves kőhúr","Enyves kőhúr"}
Misopates orontium	{"vetési oroszlánszáj","Vetési oroszlánszáj"}
Moehringia muscosa	{"mohos csitri","Mohos csitri"}
Moehringia trinervia	{"erdei csitri","Erdei csitri"}
Moenchia mantica	{Rigószegfű,"sudár rigószegfű"}
Molinia caerulea	{"nyugati kékperje","Nyugati kékperje"}
Morus alba	{"fehér eperfa","Fehér eperfa"}
Morus nigra	{"fekete eperfa","Fekete eperfa"}
Muscari botryoides	{epergyöngyike,Epergyöngyike}
Muscari comosum	{"üstökös gyöngyike","Üstökös gyöngyike"}
Muscari tenuiflorum	{"karcsú gyöngyike","Karcsú gyöngyike"}
Myagrum perfoliatum	{"galléros légyfogó",Légyfogó}
Mycelis muralis	{kakicsvirág,Kakicsvirág}
Myosotis arvensis	{"parlagi nefelejcs","Parlagi nefelejcs"}
Myosotis caespitosa	{"gyepes nefelejcs","Gyepes nefelejcs"}
Myosotis discolor	{"tarka nefelejcs","Tarka nefelejcs"}
Myosotis nemorosa	{"ligeti nefelejcs","Ligeti nefelejcs"}
Myosotis ramosissima	{"borzas nefelejcs","Borzas nefelejcs"}
Myosotis sparsiflora	{"lazavirágú nefelejcs","Lazavirágú nefelejcs"}
Myosotis stenophylla	{"sziklai nefelejcs","Sziklai nefelejcs"}
Myosotis stricta	{"apró nefelejcs","Apró nefelejcs"}
Myosotis sylvatica	{"erdei nefelejcs","Erdei nefelejcs"}
Myosoton aquaticum	{"közönséges vízicsillaghúr",Vízicsillaghúr}
Myosurus minimus	{egérfarkfű,Egérfarkfű}
Myriophyllum spicatum	{"füzéres süllőhínár","Füzéres süllőhínár"}
Myriophyllum verticillatum	{"gyűrűs süllőhínár","Gyűrűs süllőhínár"}
Najas marina	{"nagy tüskéshínár","Nagy tüskéshínár"}
Najas minor	{"kis tüskéshínár","Kis tüskéshínár"}
Narcissus pseudonarcissus	{"Csupros nárcisz","sárga nárcisz"}
Nardus stricta	{szőrfű,Szőrfű}
Nasturtium officinale	{vízitorma,Vízitorma}
Nepeta cataria	{"illatos macskamenta","Illatos macskamenta"}
Nicotiana rustica	{kapadohány,Kapadohány}
Nicotiana tabacum	{"virginiai dohány","Virginiai dohány"}
Nigella arvensis	{kandilla,Kandilla}
Nigella damascena	{borzaskata,Borzaskata}
Nonea lutea	{"sárga apácavirág","Sárga apácavirág"}
Nonea pulla	{Apácavirág,"közönséges apácavirág"}
Nuphar lutea	{vízitök,Vízitök}
Nymphaea alba	{"fehér tündérrózsa","Fehér tündérrózsa"}
Odontites vernus	{"korai fogfű","Korai fogfű"}
Oenanthe aquatica	{mételykóró,Mételykóró}
Oenanthe banatica	{"bánsági borgyökér","Bánsági borgyökér"}
Oenanthe fistulosa	{"Bördös borgyökér","csöves borgyökér"}
Oenanthe silaifolia	{"gyűrűs borgyökér","Gyűrűs borgyökér"}
Oenothera biennis	{"parlagi ligetszépe","Parlagi ligetszépe"}
Oenothera erythrosepala	{"vöröslő ligetszépe","Vöröslő ligetszépe"}
Oenothera rubricaulis	{"vörösszárú ligetszépe","Vörösszárú ligetszépe"}
Oenothera suaveolens	{"illatos ligetszépe","Illatos ligetszépe"}
Omphalodes scorpioides	{"erdei békaszem","Erdei békaszem"}
Omphalodes verna	{"tavaszi békaszem","Tavaszi békaszem"}
Onobrychis arenaria	{"homoki baltacim","Homoki baltacím"}
Onobrychis viciifolia	{"takarmány baltacim",Takarmánybaltacím}
Ononis arvensis	{"mezei iglice","Mezei iglice"}
Ononis pusilla	{"sárga iglice","Sárga iglice"}
Ononis spinosa	{"tövises iglice","Tövises iglice"}
Ononis spinosa subsp. austriaca	{"osztrák tövises iglice","Osztrák tövises iglice"}
Ononis spinosiformis	{"tiszaháti iglice","Tiszaháti iglice"}
Onopordum acanthium	{szamárbogáncs,Szamárbogáncs}
Onosma tornense	{"tornai vértő","Tornai vértő"}
Onosma visianii	{"borzas vértő","Borzas vértő"}
Ophrys apifera	{méhbangó,Méhbangó}
Ophrys fuciflora	{poszméhbangó,Poszméhbangó}
Ophrys insectifera	{légybangó,Légybangó}
Orchis laxiflora subsp. elegans	{"pompás kosbor","Pompás kosbor"}
Orchis laxiflora subsp. palustris	{"mocsári kosbor","Mocsári kosbor"}
Orchis mascula subsp. signifera	{"füles kosbor","Füles kosbor"}
Orchis militaris	{vitézkosbor,"Vitéz kosbor"}
Orchis pallens	{"sápadt kosbor","Sápadt kosbor"}
Orchis purpurea	{"bíboros kosbor","Bíboros kosbor"}
Orchis simia	{majomkosbor,Majomkosbor}
Origanum vulgare	{szurokfű,Szurokfű}
Ornithogalum boucheanum	{"kónya madártej","Kónya madártej"}
Ornithogalum comosum	{"üstökös madártej","Üstökös madártej"}
Ornithogalum umbellatum	{"ernyős madártej","Ernyős madártej"}
Orobanche alba	{"fehér vajvirág","Fehér vajvirág"}
Orobanche alsatica	{"Elszászi vajvirág","elzászi vajvirág"}
Orobanche arenaria	{"homoki vajvirág","Homoki vajvirág"}
Orobanche caryophyllacea	{"galajfojtó vajvirág","Galajfojtó vajvirág"}
Orobanche cernua subsp. cumana	{"napraforgó vajvirág",Napraforgó-vajvirág}
Orobanche coerulescens subsp. occidentalis	{"kékes vajvirág","Kékes vajvirág"}
Orobanche elatior	{"nagy vajvirág","Nagy vajvirág"}
Orobanche gracilis	{Martilapu-vajvirág,"vérveres vajvirág"}
Orobanche lutea	{"sárga vajvirág","Sárga vajvirág"}
Orobanche minor	{"lóhere vajvirág",Lóhere-vajvirág}
Orobanche picridis	{"keserűgyökér vajvirág",Keserűgyökér-vajvirág}
Orobanche purpurea	{"bíboros vajvirág","Bíboros vajvirág"}
Orobanche ramosa	{"dohányfojtó vajvirág","Dohányfojtó vajvirág"}
Sambucus nigra	{"fekete bodza","Fekete bodza"}
Orobanche reticulata	{"recés vajvirág","Recés vajvirág"}
Orobanche teucrii	{"gamandor vajvirág",Gamandor-vajvirág}
Ostrya carpinifolia	{"Déli komlógyertyán",komlógyertyán}
Oxalis acetosella	{"erdei madársóska","Erdei madársóska"}
Oxalis corniculata	{"szürke madársóska","Szürke madársóska"}
Oxybaphus nyctagineus	{"kisvirágú csodatölcsér","Kisvirágú csodatölcsér"}
Oxytropis pilosa subsp. hungarica	{csajkavirág,Csajkavirág}
Paeonia officinalis subsp. banatica	{"bánáti bazsarózsa","Bánáti bazsarózsa"}
Paeonia tenuifolia	{"keleti bazsarózsa","Keleti bazsarózsa"}
Panicum capillare	{"hajszálágú köles","Hajszálágú köles"}
Panicum miliaceum	{"termesztett köles","Termesztett köles"}
Panicum philadelphicum	{"Amerikai köles","korcs köles"}
Papaver argemone	{ördögmák,Ördögmák}
Papaver dubium	{"bujdosó mák","Bujdosó mák"}
Papaver hybridum	{"korcs mák","Korcs mák"}
Papaver somniferum	{mák,Mák}
Parietaria officinalis	{Falgyom,"közönséges falgyom"}
Paris quadrifolia	{farkasszőlő,Farkasszőlő}
Parthenocissus inserta	{"közönséges vadszőlő","Közönséges vadszőlő"}
Parthenocissus quinquefolia	{"tapadó vadszőlő","Tapadó vadszőlő"}
Parthenocissus tricuspidata	{"japán vadszőlő","Japán vadszőlő"}
Pastinaca sativa subsp. pratensis	{Pasztinák,"vad pasztinák"}
Pedicularis palustris	{"posvány kakastaréj",Posványkakastaréj}
Peplis portula	{tócsahúr,Tócsahúr}
Persica vulgaris	{őszibarack,Őszibarack}
Petasites albus	{"fehér acsalapu","Fehér acsalapu"}
Petasites hybridus	{"vörös acsalapu","Vörös acsalapu"}
Petrorhagia prolifera	{Aszúszegfű,"homoki aszúszegfű"}
Petrorhagia saxifraga	{"kőtörő aszúszegfű",Kőtörőszegfű}
Peucedanum alsaticum	{"buglyos kocsord","Buglyos kocsord"}
Peucedanum arenarium	{"homoki kocsord","Homoki kocsord"}
Peucedanum cervaria	{"szarvas kocsord","Szarvas kocsord"}
Peucedanum officinale	{"sziki kocsord","Sziki kocsord"}
Peucedanum oreoselinum	{citromkocsord,Citromkocsord}
Peucedanum palustre	{"mocsári kocsord","Mocsári kocsord"}
Peucedanum verticillare	{"magasszárú kocsord","Magasszárú kocsord"}
Phacelia congesta	{"tömött mézontófű","Tömött mézontófű"}
Phacelia tanacetifolia	{"közönséges mézontófű","Közönséges mézontófű"}
Phalaris canariensis	{kanáriköles,Kanáriköles}
Phaseolus vulgaris	{"Török bab",veteménybab}
Philadelphus coronarius	{jezsámen,"Pompás jezsámen"}
Phleum paniculatum	{"érdes komócsin","Érdes komócsin"}
Phleum phleoides	{"sima komócsin","Sima komócsin"}
Phleum pratense	{"mezei komócsin","Mezei komócsin"}
Pholiurus pannonicus	{kígyófark,Kígyófark}
Phragmites australis	{nád,Nád}
Physalis alkekengi	{zsidócseresznye,Zsidócseresznye}
Phyteuma orbiculare	{"gombos varjúköröm","Gombos varjúköröm"}
Phyteuma spicatum	{"erdei varjúköröm","Erdei varjúköröm"}
Phytolacca americana	{"amerikai alkörmös","Amerikai alkörmös"}
Picea abies	{"Közönséges luc","közönséges lucfenyő"}
Picris hieracioides	{Keserűgyökér,"közönséges keserűgyökér"}
Picris hieracioides subsp. spinulosa	{"kisfészkű keserűgyökér","Kisfészkű keserűgyökér"}
Pimpinella major	{"nagy földitömjén","Nagy földitömjén"}
Pimpinella saxifraga	{"hasznos földitömjén","Hasznos földitömjén"}
Pimpinella saxifraga subsp. nigra	{"fekete hasznos földitömjén","Fekete hasznos földitömjén"}
Pinguicula alpina	{"havasi hízóka","Havasi hízóka"}
Pinguicula vulgaris	{"lápi hízóka","Lápi hízóka"}
Pinus nigra	{feketefenyő,Feketefenyő}
Pinus sylvestris	{erdeifenyő,"Közönséges erdeifenyő"}
Piptatherum miliaceum	{kásafűfaj,"Sokvirágú bajuszoskásafű"}
Piptatherum virescens	{"bajuszos kásafű",Bajuszoskásafű}
Pisum elatius	{"magas borsó","Magas borsó"}
Pisum sativum	{"vetemény borsó",Veteményborsó}
Plantago altissima	{"magas útifű","Magas útifű"}
Plantago arenaria	{"homoki útifű","Homoki útifű"}
Plantago argentea	{"ezüstös útifű","Ezüstös útifű"}
Plantago lanceolata	{"lándzsás útifű","Lándzsás útifű"}
Plantago major subsp. intermedia	{"vízparti nagy útifű","Vízparti nagy útifű"}
Plantago major subsp. winteri	{"sósréti nagy útifű","Sósréti nagy útifű"}
Plantago maritima	{"sziki útifű","Sziki útifű"}
Plantago maxima	{"óriás útifű","Óriás útifű"}
Plantago media	{"réti útifű","Réti útifű"}
Plantago schwarzenbergiana	{"erdélyi útifű","Erdélyi útifű"}
Plantago tenuiflora	{"vékony útifű","Vékony útifű"}
Platanthera bifolia	{"kétlevelű sarkvirág","Kétlevelű sarkvirág"}
Platanthera chlorantha	{"zöldes sarkvirág","Zöldes sarkvirág"}
Pleurospermum austriacum	{Borzamag,"osztrák borzamag"}
Poa angustifolia	{"karcsú perje","Karcsú perje"}
Poa annua	{"egynyári perje","Egynyári perje"}
Poa badensis	{"sziklai perje","Sziklai perje"}
Poa bulbosa	{"gumós perje","Gumós perje"}
Poa compressa	{"laposszárú perje","Laposszárú perje"}
Poa nemoralis	{"ligeti perje","Ligeti perje"}
Poa palustris	{"mocsári perje","Mocsári perje"}
Poa pratensis	{"réti perje","Réti perje"}
Poa supina	{"henye perje","Henye perje"}
Poa trivialis	{"sovány perje","Sovány perje"}
Polycnemum arvense	{"közönséges torzon","Közönséges torzon"}
Polycnemum majus	{"nagy torzon","Nagy torzon"}
Polycnemum verrucosum	{"bibircses torzon","Bibircses torzon"}
Polygala amara	{"keserű pacsirtafű","Keserű pacsirtafű"}
Polygala amarella	{"kisvirágú pacsirtafű","Kisvirágú pacsirtafű"}
Polygala amarella subsp. austriaca	{"osztrák kisvirágú pacsirtafű","Osztrák kisvirágú pacsirtafű"}
Polygala comosa	{"üstökös pacsirtafű","Üstökös pacsirtafű"}
Polygala major	{"nagy pacsirtafű","Nagy pacsirtafű"}
Polygala nicaeensis subsp. carniolica	{"krajnai pacsirtafű","Krajnai pacsirtafű"}
Polygala vulgaris	{"hegyi pacsirtafű","Hegyi pacsirtafű"}
Polygala vulgaris subsp. oxyptera	{"Hegyesszárnyú hegyi pacsirtafű","hegyi pacsirtafű"}
Polygonatum latifolium	{"széleslevelű salamonpecsét","Széleslevelű salamonpecsét"}
Polygonatum multiflorum	{"fürtös salamonpecsét","Fürtös salamonpecsét"}
Polygonatum odoratum	{"soktérdű salamonpecsét","Soktérdű salamonpecsét"}
Polygonatum verticillatum	{"pávafarkú salamonpecsét","Pávafarkú salamonpecsét"}
Polygonum arenarium	{"Homoki keserűfű","homoki porcsinkeserűfű"}
Polygonum arenastrum	{"taposott porcsinkeserűfű","Útszéli keserűfű"}
Polygonum aviculare	{Madárkeserűfű,madár-porcsinkeserűfű}
Polygonum graminifolium	{"Pázsitlevelű keserűfű","pázsitlevelű porcsinkeserűfű"}
Polypodium interjectum	{"Hegyesszárnyú édesgyökerű-páfrány","hegyesszárú édesgyökerű-páfrány"}
Polypodium vulgare	{"közönséges édesgyökerű-páfrány","Közönséges édesgyökerű-páfrány"}
Polystichum aculeatum	{"karéjos vesepáfrány","Karéjos vesepáfrány"}
Polystichum braunii	{"szőrös vesepáfrány","Szőrös vesepáfrány"}
Polystichum lonchitis	{"dárdás vesepáfrány","Dárdás vesepáfrány"}
Polystichum setiferum	{"díszes vesepáfrány","Díszes vesepáfrány"}
Populus alba	{"fehér nyár","Fehér nyár"}
Populus deltoides	{"Amerikai fekete nyár","virginiai nyár"}
Populus nigra	{"fekete nyár","Fekete nyár"}
Populus simonii	{"kínai nyár","Kínai nyár"}
Populus tremula	{"rezgő nyár","Rezgő nyár"}
Portulaca oleracea	{"kövér porcsin","Kövér porcsin"}
Potamogeton acutifolius	{"hegyeslevelű békaszőlő","Hegyeslevelű békaszőlő"}
Potamogeton berchtoldii	{"Berchtold békaszőlő",Berchtold-békaszőlő}
Potamogeton coloratus	{"színes békaszőlő","Színes békaszőlő"}
Potamogeton crispus	{"bodros békaszőlő","Bodros békaszőlő"}
Potamogeton filiformis	{"fonalas békaszőlő","Fonalas békaszőlő"}
Potamogeton gramineus	{"felemás békaszőlő","Felemáslevelű békaszőlő"}
Potamogeton lucens	{"üveglevelű békaszőlő","Üveglevelű békaszőlő"}
Potamogeton natans	{"úszó békaszőlő","Úszó békaszőlő"}
Potamogeton nodosus	{"imbolygó békaszőlő","Imbolygó békaszőlő"}
Potamogeton obtusifolius	{"tompalevelű békaszőlő","Tompalevelű békaszőlő"}
Potamogeton pectinatus	{"fésűs békaszőlő","Fésűs békaszőlő"}
Potamogeton pectinatus subsp. balatonicus	{"balatoni békaszőlő","Balatoni békaszőlő"}
Potamogeton perfoliatus	{"hínáros békaszőlő","Hínáros békaszőlő"}
Potamogeton trichoides	{"sertelevelű békaszőlő","Sertelevelű békaszőlő"}
Potentilla alba	{"fehér pimpó","Fehér pimpó"}
Potentilla anserina	{"liba pimpó",Libapimpó}
Potentilla arenaria	{"homoki pimpó","Homoki pimpó"}
Potentilla argentea	{"ezüst pimpó","Ezüst pimpó"}
Potentilla collina	{"terpedt pimpó","Terpedt pimpó"}
Potentilla erecta	{vérontófű,Vérontófű}
Potentilla heptaphylla	{"vörösszárú pimpó","Vörösszárú pimpó"}
Potentilla impolita	{"Molyhos pimpó","zöld-ezüst pimpó"}
Potentilla inclinata	{"szürke pimpó","Szürke pimpó"}
Potentilla leucopolitana	{"elzászi pimpó","Elzászi pimpó"}
Potentilla micrantha	{"kisvirágú pimpó","Kisvirágú pimpó"}
Potentilla neumanniana	{"tavaszi pimpó",Tavaszi-pimpó}
Potentilla patula	{"kiterült pimpó","Kiterült pimpó"}
Potentilla pedata	{"borzas pimpó","Borzas pimpó"}
Potentilla pusilla	{"mirigyes pimpó","Mirigyes pimpó"}
Potentilla recta	{"egyenes pimpó","Egyenes pimpó"}
Potentilla recta subsp. laciniosa	{"egyenes pimpó","Szabdaltlevelű egyenes pimpó"}
Potentilla recta subsp. leucotricha	{"Aranysárga egyenes pimpó","egyenes pimpó"}
Potentilla reptans	{"indás pimpó","Indás pimpó"}
Potentilla rupestris	{"kövi pimpó","Kövi pimpó"}
Potentilla supina	{"henye pimpó","Henye pimpó"}
Potentilla thyrsiflora	{"sátorozó pimpó","Sátorozó pimpó"}
Prenanthes purpurea	{nyúlsaláta,Nyúlsaláta}
Primula auricula subsp. hungarica	{"cifra kankalin","Magyar cifra kankalin"}
Primula farinosa subsp. alpigena	{"lisztes kankalin","Lisztes kankalin"}
Primula veris	{"tavaszi kankalin","Tavaszi kankalin"}
Primula vulgaris	{"szártalan kankalin","Szártalan kankalin"}
Prunella grandiflora	{"nagyvirágú gyíkfű","Nagyvirágú gyíkfű"}
Prunella laciniata	{"fehér gyíkfű","Fehér gyíkfű"}
Prunella vulgaris	{"közönséges gyíkfű","Közönséges gyíkfű"}
Prunus cerasifera	{cseresznyeszilva,Cseresznyeszilva}
Prunus domestica	{"házi szilva","Kerti szilva"}
Prunus domestica subsp. insititia	{kökényszilva,Kökényszilva}
Prunus spinosa	{kökény,Kökény}
Ptelea trifoliata	{Alásfa,"hármaslevelű alásfa"}
Pteridium aquilinum	{saspáfrány,Saspáfrány}
Puccinellia distans	{"közönséges mézpázsit","Közönséges mézpázsit"}
Puccinellia limosa	{"sziki mézpázsit","Sziki mézpázsit"}
Pulicaria dysenterica	{"réti bolhafű","Réti bolhafű"}
Pulicaria vulgaris	{"parlagi bolhafű","Parlagi bolhafű"}
Pulmonaria angustifolia	{"keskenylevelű tüdőfű","Keskenylevelű tüdőfű"}
Pulmonaria mollis	{"bársonyos tüdőfű","Bársonyos tüdőfű"}
Pulmonaria obscura	{"zöldlevelű tüdőfű","Zöldlevelű tüdőfű"}
Pulmonaria officinalis	{"orvosi tüdőfű","Orvosi tüdőfű"}
Pulsatilla grandis	{leánykökörcsin,Leánykökörcsin}
Pulsatilla patens	{Ibolyakökörcsin,"tátogó kökörcsin"}
Pulsatilla pratensis subsp. hungarica	{"magyar kökörcsin","Magyar kökörcsin"}
Pulsatilla pratensis subsp. nigricans	{"fekete kökörcsin","Fekete kökörcsin"}
Pyrola minor	{"kis körtike","Kis körtike"}
Pyrola rotundifolia	{"kereklevelű körtike","Kereklevelű körtike"}
Pyrus communis	{"nemes körte","Nemes körte"}
Pyrus nivalis	{"vastaggallyú körte","Vastaggallyú körte"}
Pyrus pyraster	{vadkörte,Vadkörte}
Quercus cerris	{Cser,csertölgy}
Quercus dalechampii	{"dárdáskaréjú kocsánytalan tölgy","Dárdáskaréjú tölgy"}
Quercus petraea	{"kocsánytalan tölgy","Kocsánytalan tölgy"}
Quercus polycarpa	{"erdélyi kocsánytalan tölgy","Erdélyi tölgy"}
Quercus pubescens	{"molyhos tölgy","Molyhos tölgy"}
Quercus robur	{"kocsányos tölgy","Kocsányos tölgy"}
Quercus robur subsp. slavonica	{"szlavón kocsányos tölgy","Szlavón tölgy"}
Quercus rubra	{"vörös tölgy","Vörös tölgy"}
Quercus virgiliana	{"olasz tölgy","Olasz tölgy"}
Ranunculus acris	{"réti boglárka","Réti boglárka"}
Ranunculus arvensis	{"vetési boglárka","Vetési boglárka"}
Ranunculus auricomus	{"változó boglárka","Változó boglárka"}
Ranunculus bulbosus	{"hagymás boglárka","Hagymás boglárka"}
Ranunculus cassubicus	{boglárkafaj,"Pajzsos boglárka"}
Ranunculus fallax	{boglárkafaj,"Bonyolult boglárka"}
Ranunculus flammula	{békaboglárka,Békaboglárka}
Ranunculus illyricus	{"selymes boglárka","Selymes boglárka"}
Ranunculus lanuginosus	{"gyapjas boglárka","Gyapjas boglárka"}
Ranunculus lateriflorus	{"sziki boglárka","Sziki boglárka"}
Ranunculus lingua	{"nádi boglárka","Nádi boglárka"}
Ranunculus parviflorus	{"kisvirágú boglárka","Kisvirágú boglárka"}
Ranunculus pedatus	{"villás boglárka","Villás boglárka"}
Ranunculus polyanthemos	{"sokvirágú boglárka","Sokvirágú boglárka"}
Ranunculus polyphyllus	{"buglyos boglárka","Buglyos boglárka"}
Ranunculus repens	{"kúszó boglárka","Kúszó boglárka"}
Ranunculus sardous	{buborcsboglárka,"Buborcs boglárka"}
Ranunculus sceleratus	{torzsikaboglárka,"Torzsika boglárka"}
Raphanus raphanistrum	{repcsényretek,Repcsényretek}
Raphanus sativus	{"kerti retek","Kerti retek"}
Rapistrum perenne	{rekenyő,Rekenyő}
Rapistrum rugosum	{"ráncos rekenyő","Ráncos rekenyő"}
Reseda inodora	{"szagtalan rezeda","Szagtalan rezeda"}
Reseda lutea	{"vad rezeda",Vadrezeda}
Reseda luteola	{"sárga rezeda","Sárga rezeda"}
Reseda phyteuma	{"terpedt rezeda","Terpedt rezeda"}
Rhamnus saxatilis	{"sziklai benge","Sziklai benge"}
Rhinanthus alectorolophus	{"borzas kakascímer","Borzas kakascímer"}
Rhinanthus borbasii	{"pusztai kakascímer","Pusztai kakascímer"}
Rhinanthus minor	{"csörgő kakascímer","Csörgő kakascímer"}
Rhinanthus rumelicus	{"mirigyes kakascímer","Mirigyes kakascímer"}
Rhinanthus serotinus	{"nagyvirágú kakascímer","Nagyvirágú kakascímer"}
Rhinanthus wagneri	{"balkáni kakascímer","Balkáni kakascímer"}
Ribes alpinum	{"havasi ribiszke","Havasi ribiszke"}
Ribes aureum	{"arany ribiszke","Arany ribiszke"}
Ribes nigrum	{"fekete ribiszke","Fekete ribiszke"}
Ribes petraeum	{"bérci ribiszke","Bérci ribiszke"}
Ribes rubrum	{"Kerti ribiszke","vörös ribiszke"}
Ribes uva-crispa	{egres,Köszméte}
Ricinus communis	{ricinus,Ricinus}
Robinia hispida	{"piros akác","Rózsás akác"}
Rorippa amphibia	{"vízi kányafű","Vízi kányafű"}
Rorippa austriaca	{"osztrák kányafű","Osztrák kányafű"}
Rorippa palustris	{"mocsári kányafű","Mocsári kányafű"}
Rorippa sylvestris	{"erdei kányafű","Erdei kányafű"}
Rorippa sylvestris subsp. kerneri	{"A. Kern.-kányafű","Kerner erdei kányafű"}
Rosa agrestis	{"mezei rózsa","Mezei rózsa"}
Rosa albiflora	{"fehérvirágú rózsa","Fehérvirágú rózsa"}
Rosa caesia	{"keménylevelű rózsa","Keménylevelű rózsa"}
Rosa canina	{"gyepű rózsa",Gyepűrózsa}
Rosa corymbifera	{"berki rózsa","Berki rózsa"}
Rosa deseglisei	{Déséglise-rózsa,rózsafaj}
Rosa elliptica	{"elliptikus rózsa","Elliptikus rózsa"}
Rosa gallica	{"parlagi rózsa","Parlagi rózsa"}
Rosa glauca	{"Oszlopos rózsa","piroslevelű rózsa"}
Rosa inodora	{"illattalan rózsa","Illattalan rózsa"}
Rosa kmetiana	{Kmeti'-rózsa,Kmet'-rózsa}
Rosa micrantha	{"kisvirágú rózsa","Kisvirágú rózsa"}
Rosa obtusifolia	{"molyhosodó rózsa","Molyhosodó rózsa"}
Rosa pendulina	{"havasalji rózsa","Havasalji rózsa"}
Rosa rubiginosa	{"rozsdás rózsa","Rozsdás rózsa"}
Rosa rugosa	{"japán rózsa","Japán rózsa"}
Rosa sherardii	{"Molyhos rózsa",Sherard-rózsa}
Rosa stylosa	{"Erdei rózsa","oszlopos rózsa"}
Rosa szaboi	{"dunántúli rózsa","Dunántúli rózsa"}
Rosa zalana	{"zalai rózsa","Zalai rózsa"}
Rubia tinctorum	{"festő buzér","Festő buzér"}
Rubus bifrons	{"rózsás szeder","Rózsás szeder"}
Rubus caesius	{"hamvas szeder","Hamvas szeder"}
Rubus canescens	{"molyhos szeder","Molyhos szeder"}
Rubus discolor	{"kétszínű szeder","Útszéli szeder"}
Rubus divaricatus	{"fényes szeder","Fényes szeder"}
Rubus hirtus	{"borzas szeder","Borzas szeder"}
Rubus idaeus	{"erdei málna",Málna}
Rubus nessensis	{"felegyenesedő szeder","Felegyenesedő szeder"}
Rubus plicatus	{"ráncos szeder","Ráncos szeder"}
Rubus radula	{"ráspolyos szeder","Ráspolyos szeder"}
Rubus rhamnifolius	{"bengelevelű szeder","Bengelevelű szeder"}
Rubus rudis	{"durva szeder","Durva szeder"}
Rubus saxatilis	{"kövi szeder","Kövi szeder"}
Rubus scaber	{"érdes szeder","Érdes szeder"}
Rubus serpens	{"kúszó szeder","Kúszó szeder"}
Rubus sulcatus	{"barázdás szeder","Barázdás szeder"}
Rubus vestitus	{"dússzőrű szeder","Dússzőrű szeder"}
Rudbeckia hirta	{"borzas kúpvirág","Borzas kúpvirág"}
Rudbeckia laciniata	{"magas kúpvirág","Magas kúpvirág"}
Rumex acetosa	{"mezei sóska","Mezei sóska"}
Rumex acetosella	{juhsóska,Juhsóska}
Rumex acetosella subsp. tenuifolius	{"ágas juhsóska","Ágas juhsóska"}
Rumex aquaticus	{"vízi lórom","Vízi lórom"}
Rumex confertus	{"tömött lórom","Tömött lórom"}
Rumex conglomeratus	{"murvás lórom","Murvás lórom"}
Rumex crispus	{"fodros lórom","Fodros lórom"}
Rumex dentatus subsp. halacsyi	{"töviskés lórom","Töviskés lórom"}
Rumex hydrolapathum	{"tavi lórom","Tavi lórom"}
Rumex kerneri	{"A. Kern.-lórom",Kerner-lórom}
Rumex maritimus	{"tengeri lórom","Tengeri lórom"}
Rumex obtusifolius	{"réti lórom","Réti lórom"}
Rumex palustris	{"mocsári lórom","Mocsári lórom"}
Rumex patientia	{"paréj lórom",Paréjlórom}
Rumex patientia subsp. orientalis	{"keleti paréj lórom","Keleti paréjlórom"}
Rumex pseudonatronatus	{"sziki lórom","Sziki lórom"}
Rumex pulcher	{"csinos lórom","Csinos lórom"}
Rumex sanguineus	{"erdei lórom","Erdei lórom"}
Rumex scutatus	{"francia sóska","Római sóska"}
Rumex stenophyllus	{"keskenylevelű lórom","Keskenylevelű lórom"}
Rumex thyrsiflorus	{"füles sóska","Füles sóska"}
Ruscus aculeatus	{"szúrós csodabogyó","Szúrós csodabogyó"}
Ruscus hypoglossum	{"lónyelvű csodabogyó","Lónyelvű csodabogyó"}
Sagina nodosa	{"csomós zöldhúr","Csomós zöldhúr"}
Sagina procumbens	{"heverő zöldhúr","Heverő zöldhúr"}
Sagina saginoides	{"havasi zöldhúr","Havasi zöldhúr"}
Sagina subulata	{"szálkás zöldhúr","Szálkás zöldhúr"}
Sagittaria sagittifolia	{nyílfű,Nyílfű}
Salicornia prostrata	{sziksófű,Sziksófű}
Salix alba	{"fehér fűz","Fehér fűz"}
Salix alba subsp. vitellina	{"fehér fűz","Sárgaüstökű fehér fűz"}
Salix aurita	{"füles fűz","Füles fűz"}
Salix caprea	{kecskefűz,Kecskefűz}
Salix cinerea	{"Hamvas fűz",rekettyefűz}
Salix fragilis	{csöregefűz,Csőregefűz}
Salix pentandra	{babérfűz,Babérfűz}
Salix purpurea	{csigolyafűz,Csigolyafűz}
Salix triandra	{mandulafűz,"Mandulalevelű fűz"}
Salix viminalis	{"Kosárfonó fűz",kosárkötőfűz}
Salsola soda	{"sziki ballagófű","Sziki ballagófű"}
Salvia austriaca	{"osztrák zsálya","Osztrák zsálya"}
Salvia glutinosa	{"enyves zsálya","Enyves zsálya"}
Salvia nemorosa	{"ligeti zsálya","Ligeti zsálya"}
Salvia nutans	{"kónya zsálya","Kónya zsálya"}
Salvia officinalis	{"kerti zsálya","Kerti zsálya"}
Salvia pratensis	{"mezei zsálya","Mezei zsálya"}
Salvia sclarea	{"muskotály zsálya",Muskotályzsálya}
Salvia verticillata	{lózsálya,Lózsálya}
Sambucus ebulus	{"földi bodza","Földi bodza"}
Sambucus racemosa	{"fürtös bodza","Fürtös bodza"}
Sanguisorba minor	{Csabaíre,"csabaíre vérfű"}
Sanguisorba officinalis	{"őszi vérfű","Őszi vérfű"}
Sanicula europaea	{gombernyő,Gombernyő}
Saponaria officinalis	{szappanfű,Szappanfű}
Satureja hortensis	{Csombor,csombord}
Saxifraga adscendens	{"hegyi kőtörőfű","Hegyi kőtörőfű"}
Saxifraga bulbifera	{"gumós kőtörőfű","Gumós kőtörőfű"}
Saxifraga granulata	{"bibircses kőtörőfű","Bibircses kőtörőfű"}
Saxifraga paniculata	{"fürtös kőtörőfű","Fürtös kőtörőfű"}
Saxifraga tridactylites	{"apró kőtörőfű","Apró kőtörőfű"}
Scabiosa canescens	{"szürkés ördögszem","Szürkés ördögszem"}
Scabiosa columbaria	{"Galambszín ördögszem","galambszínű ördögszem"}
Senecio viscosus	{"enyves aggófű","Enyves aggófű"}
Scabiosa columbaria subsp. pseudobanatica	{"bánáti ördögszem","Bánáti ördögszem"}
Scabiosa ochroleuca	{"vajszínű ördögszem","Vajszínű ördögszem"}
Scandix pecten-veneris	{berzenke,Berzenke}
Schoenoplectus americanus subsp. triangularis	{"vékony káka","Vékony káka"}
Schoenoplectus lacustris	{"tavi káka","Tavi káka"}
Schoenoplectus litoralis	{"tengermelléki káka","Tengermelléki káka"}
Schoenoplectus mucronatus	{"szúrós káka","Szúrós káka"}
Schoenoplectus setaceus	{kasikakáka,Kasikakáka}
Schoenoplectus supinus	{"henye káka","Henye káka"}
Schoenoplectus tabernaemontani	{"kötő káka",Kötőkáka}
Schoenoplectus triqueter	{"háromélű káka","Háromélű káka"}
Schoenus ferrugineus	{"rozsdás csáté","Rozsdás csáté"}
Schoenus nigricans	{"kormos csáté","Kormos csáté"}
Scilla drunensis	{"nyugati csillagvirág","Nyugati csillagvirág"}
Scilla kladnii	{"erdélyi csillagvirág","Erdélyi csillagvirág"}
Scilla siberica	{"bókoló csillagvirág","Bókoló csillagvirág"}
Scilla vindobonensis	{"ligeti csillagvirág","Ligeti csillagvirág"}
Scirpus radicans	{"gyökerező erdeikáka","Gyökerező erdeikáka"}
Scirpus sylvaticus	{"közönséges erdeikáka","Közönséges erdeikáka"}
Scleranthus annuus	{"egynyári szikárka","Egynyári szikárka"}
Scleranthus perennis	{"évelő szikárka","Évelő szikárka"}
Scleranthus polycarpos	{"füzéres szikárka","Füzéres szikárka"}
Scleranthus verticillatus	{"dombi szikárka","Dombi szikárka"}
Sclerochloa dura	{kőperje,Kőperje}
Scorzonera austriaca	{"osztrák pozdor","Osztrák pozdor"}
Scorzonera hispanica	{"spanyol pozdor","Spanyol pozdor"}
Scorzonera humilis	{"alacsony pozdor","Alacsony pozdor"}
Scorzonera parviflora	{"kisvirágú pozdor","Kisvirágú pozdor"}
Scorzonera purpurea	{"piros pozdor","Piros pozdor"}
Scrophularia nodosa	{"göcsös görvélyfű","Göcsös görvélyfű"}
Scrophularia scopolii	{"bársonyos görvélyfű","Bársonyos görvélyfű"}
Scrophularia umbrosa	{"szárnyas görvélyfű","Szárnyas görvélyfű"}
Scrophularia vernalis	{"tavaszi görvélyfű","Tavaszi görvélyfű"}
Scutellaria altissima	{"magas csukóka","Magas csukóka"}
Scutellaria columnae	{"bozontos csukóka","Bozontos csukóka"}
Scutellaria galericulata	{"vízmelléki csukóka","Vízmelléki csukóka"}
Scutellaria hastifolia	{"dárdás csukóka","Dárdás csukóka"}
Secale sylvestre	{"vad rozs",Vadrozs}
Sedum acre	{"borsos varjúháj","Borsos varjúháj"}
Sedum album	{"fehér varjúháj","Fehér varjúháj"}
Sedum caespitosum	{"sziki varjúháj","Sziki varjúháj"}
Sedum hispanicum	{"deres varjúháj","Deres varjúháj"}
Sedum sartorianum subsp. hillebrandtii	{"homoki varjúháj","Homoki varjúháj"}
Sedum sexangulare	{"hatsoros varjúháj","Hatsoros varjúháj"}
Sedum spurium	{"kaukázusi varjúháj","Kaukázusi varjúháj"}
Selaginella helvetica	{"hegyi csipkeharaszt","Hegyi csipkeharaszt"}
Selinum carvifolia	{"közönséges nyúlkömény",Nyúlkömény}
Senecio aquaticus	{"vízi aggófű","Vízi aggófű"}
Senecio doria	{"kövér aggófű","Kövér aggófű"}
Senecio erraticus subsp. barbareifolius	{"réti aggófű","Réti aggófű"}
Senecio erucifolius	{"keskenylevelű aggófű","Keskenylevelű aggófű"}
Senecio inaequidens	{"vesszős aggófű","Vesszős aggófű"}
Senecio paludosus	{"mocsári aggófű","Mocsári aggófű"}
Senecio rupestris	{"szirti aggófű","Szirti aggófű"}
Senecio sylvaticus	{"erdei aggófű","Erdei aggófű"}
Senecio umbrosus	{"nagy aggófű","Nagy aggófű"}
Senecio vernalis	{"tavaszi aggófű","Tavaszi aggófű"}
Senecio vulgaris	{"közönséges aggófű","Közönséges aggófű"}
Serratula lycopifolia	{"fénylő zsoltina","Fénylő zsoltina"}
Serratula radiata	{"sugaras zsoltina","Sugaras zsoltina"}
Serratula tinctoria	{"festő zsoltina","Festő zsoltina"}
Seseli annuum	{"homoki gurgolya","Homoki gurgolya"}
Seseli hippomarathrum	{"szilkés gurgolya","Szilkés gurgolya"}
Seseli leucospermum	{"magyar gurgolya","Magyar gurgolya"}
Seseli osseum	{"szürke gurgolya","Szürke gurgolya"}
Sesleria hungarica	{"magyar nyúlfarkfű","Magyar nyúlfarkfű"}
Setaria italica	{"olasz muhar","Olasz muhar"}
Setaria pumila	{"fakó muhar","Fakó muhar"}
Setaria verticillata	{"ragadós muhar","Ragadós muhar"}
Setaria viridis	{"Zöldes muhar","zöld muhar"}
Sherardia arvensis	{Csillagfű,"vetési csillagfű"}
Sicyos angulatus	{Gyepűtök,"szögletes gyepűtök"}
Sideritis montana	{sármányvirág,Sármányvirág}
Silaum silaus	{"sárga kígyókapor","Sárga kígyókapor"}
Silene armeria	{"kerti habszegfű","Kerti habszegfű"}
Silene borysthenica	{"kisvirágú habszegfű","Kisvirágú habszegfű"}
Silene conica	{"homoki habszegfű","Homoki habszegfű"}
Silene dichotoma	{"villás habszegfű","Villás habszegfű"}
Silene flavescens	{"Sárga habszegfű","sárgás habszegfű"}
Silene gallica	{"francia habszegfű","Francia habszegfű"}
Silene multiflora	{"sokvirágú habszegfű","Sokvirágú habszegfű"}
Silene nemoralis	{"berki habszegfű","Ligeti habszegfű"}
Silene nutans	{"kónya habszegfű","Kónya habszegfű"}
Silene otites	{"szikár habszegfű","Szikár habszegfű"}
Silene viridiflora	{"zöldesvirágú habszegfű","Zöldesvirágú habszegfű"}
Silene vulgaris	{"hólyagos habszegfű","Hólyagos habszegfű"}
Silphium perfoliatum	{csészekóró,Csészekóró}
Sinapis alba	{"fehér mustár","Fehér mustár"}
Sinapis alba subsp. dissecta	{"fehér mustár","Szabdaltlevelű mustár"}
Sinapis arvensis	{vadrepce,Vadrepce}
Sisymbrium altissimum	{"magyar zsombor","Magyar zsombor"}
Sisymbrium loeselii	{"parlagi zsombor","Parlagi zsombor"}
Sisymbrium officinale	{"szapora zsombor","Szapora zsombor"}
Sisymbrium orientale	{"hamvas zsombor","Hamvas zsombor"}
Sisymbrium polymorphum	{"karcsú zsombor","Karcsú zsombor"}
Sisymbrium strictissimum	{"magas zsombor","Magas zsombor"}
Sium latifolium	{"széleslevelű békakorsó","Széleslevelű békakorsó"}
Sium sisaroideum	{"keleti békaborsó","Keleti békakorsó"}
Smyrnium perfoliatum	{őzsaláta,Őzsaláta}
Solanum alatum	{"vörös csucsor","Vörös csucsor"}
Solanum dulcamara	{"keserű csucsor","Keserű csucsor"}
Solanum luteum	{"sárga csucsor","Sárga csucsor"}
Solanum nigrum	{"fekete csucsor","Fekete csucsor"}
Solanum nigrum subsp. schultesii	{"mirigyes fekete csucsor","Mirigyes fekete csucsor"}
Solanum rostratum	{tányértüske,Tányértüske}
Solidago canadensis	{"kanadai aranyvessző","Kanadai aranyvessző"}
Solidago virgaurea	{"közönséges aranyvessző","Közönséges aranyvessző"}
Sonchus arvensis	{"mezei csorbóka","Mezei csorbóka"}
Sonchus asper	{"szúrós csorboka","Szúrós csorbóka"}
Sonchus oleraceus	{"szelíd csorbóka","Szelíd csorbóka"}
Sonchus palustris	{"mocsári csorbóka","Mocsári csorbóka"}
Sorbus aria	{"lisztes berkenye","Lisztes berkenye"}
Sorbus aucuparia	{madárberkenye,Madárberkenye}
Sorbus austriaca subsp. hazslinszkyana	{Hazslinszky-berkenye,Hazsylinszky-berkenye}
Sorbus graeca	{"déli berkenye","Déli berkenye"}
Sorbus torminalis	{barkócaberkenye,Barkócafa}
Sorghum bicolor	{"tarka cirok","Termesztett cirok"}
Sorghum halepense	{fenyércirok,Fenyércirok}
Sorghum sudanense	{szudánifű,Szudánifű}
Sparganium emersum	{"egyszerű békabuzogány","Egyszerű békabuzogány"}
Sparganium erectum	{"ágas békabuzogány","Ágas békabuzogány"}
Sparganium erectum subsp. microcarpum	{"kistermésű ágas békabuzogány","Kistermésű békabuzogány"}
Sparganium erectum subsp. neglectum	{"ágas békabuzogány","Henye békabuzogány"}
Sparganium minimum	{"lápi békabuzogány","Lápi békabuzogány"}
Spergula arvensis	{"mezei csibehúr","Mezei csibehúr"}
Spergula pentandra	{"homoki csibehúr","Homoki csibehúr"}
Spergularia maritima	{"szárnyasmagvú budavirág","Szárnyasmagvú budavirág"}
Spergularia rubra	{"piros budavirág","Piros budavirág"}
Spiraea crenata	{"csipkés gyöngyvessző","Csipkés gyöngyvessző"}
Spiraea media	{"Sziklai gyöngyvessző","szirti gyöngyvessző"}
Spiraea salicifolia	{"fűzlevelű gyöngyvessző","Fűzlevelű gyöngyvessző"}
Spirodela polyrhiza	{"bojtos békalencse","Bojtos békalencse"}
Stachys alpina	{"havasi tisztesfű","Havasi tisztesfű"}
Stachys annua	{tarlóvirág,Tarlóvirág}
Stachys byzantina	{nyúlfüle,Nyúlfüle}
Stachys germanica	{"fehér tisztesfű","Fehér tisztesfű"}
Stachys palustris	{"mocsári tisztesfű","Mocsári tisztesfű"}
Stachys recta	{"Hasznos tarlóvirág","hasznos tisztesfű"}
Stachys sylvatica	{"erdei tisztesfű","Erdei tisztesfű"}
Staphylea pinnata	{hólyagfa,"Mogyorós hólyagfa"}
Stellaria graminea	{"pázsitos csillaghúr","Pázsitos csillaghúr"}
Stellaria holostea	{"olocsán csillaghúr",Olocsáncsillaghúr}
Stellaria media	{"közönséges tyúkhúr",Tyúkhúr}
Stellaria nemorum	{"erdei csillaghúr","Erdei csillaghúr"}
Stellaria palustris	{"mocsári csillaghúr","Mocsári csillaghúr"}
Stipa borysthenica	{"homoki árvalányhaj","Homoki árvalányhaj"}
Stipa bromoides	{"szálkás árvalányhaj","Szálkás árvalányhaj"}
Stipa capillata	{"kunkorgó árvalányhaj","Kunkorgó árvalányhaj"}
Stipa dasyphylla	{"bozontos árvalányhaj","Bozontos árvalányhaj"}
Stipa eriocaulis	{"délvidéki árvalányhaj","Délvidéki árvalányhaj"}
Stipa pulcherrima	{"csinos árvalányhaj","Csinos árvalányhaj"}
Stipa tirsa	{"hosszúlevelű árvalányhaj","Hosszúlevelű árvalányhaj"}
Stratiotes aloides	{kolokán,Kolokán}
Suaeda pannonica	{"magyar sóballa","Magyar sóballa"}
Succisa pratensis	{ördögharaptafű,Ördögharaptafű}
Succisella inflexa	{Csonkaír,"déli csonkaír"}
Symphytum officinale	{"fekete nadálytő","Fekete nadálytő"}
Syrenia cana	{Homokviola,"sárga homokviola"}
Syringa vulgaris	{"közönséges orgona","Májusi orgona"}
Tagetes patula	{"törpe büdöske","Törpe büdöske"}
Tamarix gallica	{"francia tamariska","Francia tamariska"}
Tamarix ramosissima	{"szürke tamariska","Szürke tamariska"}
Tamarix tetrandra	{"keleti tamariska","Keleti tamariska"}
Tanacetum vulgare	{"gilisztaűző varádics","Gilisztaűző varádics"}
Taraxacum bessarabicum	{"sziki pitypang","Sziki pitypang"}
Taraxacum laevigatum	{"szarvacskás pitypang","Szarvacskás pitypang"}
Taraxacum officinale	{"pongyola pitypang","Pongyola pitypang"}
Taraxacum palustre	{"lápi pitypang","Lápi pitypang"}
Taxus baccata	{tiszafa,Tiszafa}
Teesdalia nudicaulis	{"nyugati rejtőke",Rejtőke}
Teucrium botrys	{"fürtös gamandor","Fürtös gamandor"}
Teucrium chamaedrys	{"sarlós gamandor","Sarlós gamandor"}
Teucrium montanum	{"hegyi gamandor","Hegyi gamandor"}
Teucrium montanum subsp. subvillosum	{"bozontos hegyi gamandor","Bozontos hegyi gamandor"}
Teucrium scordium	{"vízi gamandor","Vízi gamandor"}
Teucrium scorodonia	{fenyérgamandor,Fenyérgamandor}
Thalictrum aquilegiifolium	{"erdei borkóró","Erdei borkóró"}
Thalictrum flavum	{"sárga borkóró","Sárga borkóró"}
Thalictrum foetidum	{"sziklai borkóró","Sziklai borkóró"}
Thalictrum lucidum	{"fényes borkóró","Fényes borkóró"}
Thalictrum minus	{"közönséges borkóró","Közönséges borkóró"}
Thalictrum minus subsp. pseudominus	{"kékes borkóró","Pannon borkóró"}
Thalictrum simplex	{"egyszerű borkóró","Egyszerű borkóró"}
Thalictrum simplex subsp. galioides	{"galajlevelű borkóró","Galajlevelű borkóró"}
Consolida ajacis	{"Kerti szarkaláb"}
Thesium bavarum	{"hegyi zsellérke","Hegyi zsellérke"}
Thesium dollineri	{"vetési zsellérke","Vetési zsellérke"}
Thesium dollineri subsp. simplex	{"egyenes vetési zsellérke","Egyenes vetési zsellérke"}
Thesium linophyllon	{"lenlevelű zsellérke","Lenlevelű zsellérke"}
Thladiantha dubia	{"bozontos kabakpityóka",Kabakpityóka}
Thlaspi alliaceum	{"hagymaszagú tarsóka","Hagymaszagú tarsóka"}
Thlaspi arvense	{"mezei tarsóka","Mezei tarsóka"}
Thlaspi goesingense	{"osztrák tarsóka","Osztrák tarsóka"}
Thlaspi montanum	{"hegyi tarsóka","Hegyi tarsóka"}
Thlaspi perfoliatum	{"galléros tarsóka","Galléros tarsóka"}
Thuja orientalis	{"keleti tuja","Keleti tuja"}
Thymelaea passerina	{Cicó,"egynyári cicó"}
Thymus glabrescens	{"közönséges kakukkfű","Közönséges kakukkfű"}
Thymus glabrescens subsp. decipiens	{"Kisded közönséges kakukkfű","közönséges kakukkfű"}
Thymus pannonicus	{"magas kakukkfű","Magas kakukkfű"}
Thymus praecox	{"korai kakukkfű","Korai kakukkfű"}
Thymus pulegioides	{"hegyi kakukkfű","Hegyi kakukkfű"}
Thymus serpyllum	{"keskenylevelű kakukkfű","Keskenylevelű kakukkfű"}
Tilia cordata	{"kislevelű hárs","Kislevelű hárs"}
Tilia platyphyllos	{"nagylevelű hárs","Nagylevelű hárs"}
Tilia platyphyllos subsp. cordifolia	{"bársonyos hárs","Bársonyos nagylevelű hárs"}
Tilia platyphyllos subsp. pseudorubra	{"kopasz hárs","Kopasz nagylevelű hárs"}
Tilia tomentosa	{"ezüst hárs","Ezüst hárs"}
Tofieldia calyculata	{"Hegyi pázsitliliom",pázsitliliom}
Tordylium maximum	{"borzas szarvasgyökér",Szarvasgyökér}
Torilis arvensis	{"vetési tüskemag","Vetési tüskemag"}
Torilis japonica	{"bojtorjános tüskemag","Bojtorjános tüskemag"}
Tragopogon dubius subsp. major	{"sokfészkű bakszakál","Sokfészkű nagy bakszakáll"}
Tragopogon orientalis	{"közönséges bakszakál","Közönséges bakszakáll"}
Tribulus terrestris	{királydinnye,Királydinnye}
Tribulus terrestris subsp. orientalis	{"keleti királydinnye","Keleti királydinnye"}
Trichophorum alpinum	{"havasi kisgyapjúsás","Havasi kisgyapjúsás"}
Trifolium alpestre	{"bérci here","Bérci here"}
Trifolium angulatum	{"sziki here","Sziki here"}
Trifolium arvense	{"Halvány here",tarlóhere}
Trifolium aureum	{"zörgő here","Zörgő here"}
Trifolium campestre	{"mezei here","Mezei here"}
Trifolium diffusum	{"buglyos here","Buglyos here"}
Trifolium dubium	{"apró here","Apró here"}
Trifolium fragiferum	{"eper here",Eperhere}
Trifolium fragiferum subsp. bonannii	{"eper here","Nagyfejű eperhere"}
Trifolium hybridum	{"korcs here","Korcs here"}
Trifolium hybridum subsp. elegans	{"csinos korcs here","Csinos korcs here"}
Trifolium incarnatum	{bíborhere,"Bíbor here"}
Trifolium medium	{"erdei here","Erdei here"}
Trifolium medium subsp. banaticum	{"bánáti erdei here","Bánáti erdei here"}
Trifolium medium subsp. sarosiense	{"sárosi erdei here","Sárosi erdei here"}
Trifolium micranthum	{cérnahere,Cérnahere}
Trifolium montanum	{"hegyi here","Hegyi here"}
Trifolium ochroleucon	{"vajszínű here","Vajszínű here"}
Trifolium ornithopodioides	{"egyvirágú here","Egyvirágú here"}
Trifolium pannonicum	{"magyar here","Magyar here"}
Trifolium patens	{"terpedő here","Terpedő here"}
Trifolium pratense	{"réti here","Réti here"}
Trifolium repens	{"fehér here","Fehér here"}
Trifolium resupinatum	{"fonák here","Fonák here"}
Trifolium retusum	{"pusztai here","Pusztai here"}
Trifolium rubens	{"pirosló here","Pirosló here"}
Trifolium striatum	{"sávos here","Törékeny tarlóhere"}
Trifolium strictum	{"sudár here","Sudár here"}
Trifolium vesiculosum	{"hólyagos here","Hólyagos here"}
Triglochin maritimum	{"Tengerpari kígyófű","tengerparti kígyófű"}
Triglochin palustre	{"mocsári kígyófű","Mocsári kígyófű"}
Trigonella caerulea	{kékhere,"Kék here"}
Trigonella foenum-graecum	{görögszéna,Görögszéna}
Delphinium ×cultorum	{"Kerti szarkaláb"}
Trigonella gladiata	{"bakszarvú lepkeszeg","Bakszarvú lepkeszeg"}
Trigonella monspeliaca	{"Francia lepkeszeg",lucerna}
Trigonella procumbens	{"sziki lepkeszeg","Sziki lepkeszeg"}
Trinia glauca	{"szürke nyúlkapor","Szürke nyúlkapor"}
Trisetum flavescens	{aranyzab,Aranyzab}
Tulipa sylvestris	{"erdei tulipán","Erdei tulipán"}
Tussilago farfara	{martilapu,Martilapu}
Typha angustifolia	{"keskenylevelű gyékény","Keskenylevelű gyékény"}
Typha latifolia	{"bodnározó gyékény","Bodnározó gyékény"}
Typha laxmannii	{rizsgyékény,Rizsgyékény}
Typha minima	{"apró gyékény","Apró gyékény"}
Typha shuttleworthii	{"ezüstös gyékény","Ezüstös gyékény"}
Ulmus glabra	{"hegyi szil","Hegyi szil"}
Ulmus laevis	{vénicfa,Vénic-szil}
Ulmus minor	{"mezei szil","Mezei szil"}
Urtica dioica	{"nagy csalán","Nagy csalán"}
Urtica pilulifera	{"római csalán",Szedercsalán}
Urtica urens	{"apró csalán","Kis csalán"}
Utricularia australis	{"pongyola rence","Pongyola rence"}
Utricularia bremii	{"lápi rence","Lápi rence"}
Utricularia minor	{"kis rence","Kis rence"}
Utricularia vulgaris	{"közönséges rence","Közönséges rence"}
Vaccaria hispanica	{Tinóöröm,"vetési tinóöröm"}
Vaccaria hispanica subsp. grandiflora	{"nagyvirágú tinóöröm","Nagyvirágú tinóöröm"}
Vaccinium myrtillus	{"fekete áfonya","Fekete áfonya"}
Vaccinium oxycoccos	{tőzegáfonya,Tőzegáfonya}
Vaccinium vitis-idaea	{"vörös áfonya","Vörös áfonya"}
Valeriana dioica	{"kétlaki macskagyökér","Kétlaki macskagyökér"}
Valeriana officinalis	{"orvosi macskagyökér","Orvosi macskagyökér"}
Valeriana simplicifolia	{"éplevelű macskagyökér","Éplevelű macskagyökér"}
Valeriana tripteris subsp. austriaca	{"hármaslevelű macskagyökér","Hármaslevelű macskagyökér"}
Valerianella carinata	{"hasábos galambbegy","Hasábos galambbegy"}
Valerianella coronata	{"koronás galambbegy","Koronás galambbegy"}
Valerianella dentata	{"fogas galambbegy","Fogas galambbegy"}
Valerianella locusta	{saláta-galambbegy,"Saláta galambbegy"}
Valerianella pumila	{"hártyás galambbegy","Hártyás galambbegy"}
Valerianella rimosa	{"füles galambbegy","Füles galambbegy"}
Vallisneria spiralis	{"közönséges csavarhínár",Vallisznéria}
Ventenata dubia	{vékonyzab,Vékonyzab}
Veratrum album	{"fehér zászpa","Fehér zászpa"}
Veratrum nigrum	{"fekete zászpa","Fekete zászpa"}
Verbascum blattaria	{"molyűző ökörfarkkóró","Molyűző ökörfarkkóró"}
Verbascum densiflorum	{"dúsvirágú ökörfarkkóró","Dúsvirágú ökörfarkkóró"}
Verbascum lychnitis	{"csilláros ökörfarkkóró","Csilláros ökörfarkkóró"}
Verbascum nigrum	{"fekete ökörfarkkóró","Fekete ökörfarkkóró"}
Verbascum phlomoides	{"szöszös ökörfarkkóró","Szöszös ökörfarkkóró"}
Verbascum phoeniceum	{"lila ökörfarkkóró","Lila ökörfarkkóró"}
Verbascum pulverulentum	{"pamutos ökörfarkkóró","Pamutos ökörfarkkóró"}
Verbascum speciosum	{"pompás ökörfarkkóró","Pompás ökörfarkkóró"}
Verbascum thapsus	{"molyhos ökörfarkkóró","Molyhos ökörfarkkóró"}
Verbena officinalis	{"közönséges vasfű","Közönséges vasfű"}
Verbena supina	{"henye vasfű","Henye vasfű"}
Veronica acinifolia	{"csomborlevelű veronika","Csomborlevelű veronika"}
Veronica agrestis	{"északi veronika","Északi veronika"}
Veronica anagallis-aquatica	{"Pólé vízi veronika","vízi veronika"}
Veronica anagalloides	{"iszaplakó veronika","Iszaplakó veronika"}
Veronica arvensis	{"mezei veronika","Mezei veronika"}
Veronica austriaca	{Jacquin-veronika,"Osztrák veronika"}
Veronica beccabunga	{"deréce veronika",Deréceveronika}
Veronica catenata	{"üstökös veronika","Üstökös veronika"}
Veronica chamaedrys	{"ösztörűs veronika","Ösztörűs veronika"}
Veronica dillenii	{"sovány veronika","Sovány veronika"}
Veronica filiformis	{"gyepes veronika","Gyepes veronika"}
Veronica hederifolia	{"borostyánlevelű veronika","Borostyánlevelű veronika"}
Veronica montana	{"hegyi veronika","Hegyi veronika"}
Veronica officinalis	{"orvosi veronika","Orvosi veronika"}
Veronica opaca	{"fénytelen veronika","Fénytelen veronika"}
Veronica peregrina	{"vándor veronika","Vándor veronika"}
Veronica persica	{"perzsa veronika","Perzsa veronika"}
Veronica polita	{"fényes veronika","Fényes veronika"}
Veronica praecox	{"korai veronika","Korai veronika"}
Veronica prostrata	{"lecsepült veronika","Lecsepült veronika"}
Veronica scutellata	{"Pajzsos veronika","pajzsos veronila"}
Veronica serpyllifolia	{"kakukk veronika",Kakukkveronika}
Veronica teucrium	{"gamandor veronika",Gamandorveronika}
Veronica triphyllos	{"ujjaslevelű veronika","Ujjaslevelű veronika"}
Veronica verna	{"tavaszi veronika","Tavaszi veronika"}
Viburnum lantana	{ostorménbangita,"Ostormén bangita"}
Viburnum opulus	{kányabangita,Kányabangita}
Vicia angustifolia	{"Keskenylevelű bükköny","vetési bükköny"}
Vicia angustifolia subsp. segetalis	{"vetési bükköny","Vetési keskenylevelű bükköny"}
Vicia articulata	{"Dél-európai bükköny","egyvirágú bükköny"}
Vicia biennis	{"kunsági bükköny","Kunsági bükköny"}
Vicia cassubica	{"vitéz bükköny","Vitéz bükköny"}
Vicia cracca	{"kaszanyűg bükköny",Kaszanyűgbükköny}
Vicia dumetorum	{cserebükköny,Cserebükköny}
Vicia ervilia	{bükkönyfaj,"Lencsealakú bükköny"}
Vicia faba	{lóbab,Lóbab}
Vicia hirsuta	{"borzas bükköny","Borzas bükköny"}
Vicia lathyroides	{"pici bükköny","Pici bükköny"}
Vicia lutea	{"sárga bükköny","Sárga bükköny"}
Vicia oroboides	{"zalai bükköny","Zalai bükköny"}
Vicia pannonica	{"pannon bükköny","Pannon bükköny"}
Vicia pannonica subsp. striata	{"csíkos bükköny","Csíkos bükköny"}
Vicia peregrina	{"idegen bükköny","Idegen bükköny"}
Vicia pisiformis	{"borsóka bükköny",Borsókabükköny}
Vicia sativa	{"takarmány bükköny",Takarmánybükköny}
Vicia sativa subsp. cordata	{"Apróhüvelyű takarmánybükköny",takarmánybükköny}
Vicia sepium	{"gyepű bükköny",Gyepűbükköny}
Vicia sparsiflora	{"pilisi bükköny","Pilisi bükköny"}
Vicia sylvatica	{"ligeti bükköny","Ligeti bükköny"}
Vicia tenuifolia	{"keskenylevelű bükköny","Keskenylevelű bükköny"}
Vicia tenuifolia subsp. stenophylla	{"dalmát bükköny","Szálas keskenylevelű bükköny"}
Vicia tenuissima	{"többmagvú bükköny","Vékonyszárú bükköny"}
Vicia tetrasperma	{"négymagvú bükköny","Négymagvú bükköny"}
Vicia villosa	{"szöszös bükköny","Szöszös bükköny"}
Vinca herbacea	{"pusztai meténg","Pusztai meténg"}
Vinca major	{"nagy meténg","Nagy meténg"}
Vinca minor	{"Kis télizöld","télizöld meténg"}
Vincetoxicum hirundinaria	{"közönséges méreggyilok","Közönséges méreggyilok"}
Vincetoxicum pannonicum	{"magyar méreggyilok","Magyar méreggyilok"}
Viola alba	{"fehér ibolya","Fehér ibolya"}
Viola ambigua	{"csuklyás ibolya","Csuklyás ibolya"}
Viola arvensis	{"mezei árvácska","Mezei árvácska"}
Viola biflora	{"sárga ibolya","Sárga ibolya"}
Viola canina	{"sovány ibolya","Sovány ibolya"}
Viola collina	{"dombi ibolya","Dombi ibolya"}
Viola elatior	{"nyúlánk ibolya","Nyúlánk ibolya"}
Viola hirta	{"borzas ibolya","Borzas ibolya"}
Viola kitaibeliana	{"törpe árvácska","Törpe árvácska"}
Viola mirabilis	{"csodás ibolya","Csodás ibolya"}
Viola odorata	{"illatos ibolya","Illatos ibolya"}
Viola pumila	{"réti ibolya","Réti ibolya"}
Viola riviniana	{"nagyvirágú ibolya","Nagyvirágú ibolya"}
Viola rupestris	{"homoki ibolya","Homoki ibolya"}
Viola sororia	{"amerikai ibolya","Csíkos ibolya"}
Viola stagnina	{"lápi ibolya","Lápi ibolya"}
Viola suavis	{"kék ibolya","Keleti ibolya"}
Viola tricolor	{"háromszínű árvácska","Háromszínű árvácska"}
Viola tricolor subsp. subalpina	{"bükkös háromszínű árvácska","Kövi vadárvácska"}
Viscum album	{"fehér fagyöngy","Fehér fagyöngy"}
Viscum album subsp. abietis	{"jegenyefenyő fagyöngy","Jegenyefenyő fehér fagyöngy"}
Viscum album subsp. austriacum	{"Erdeifenyő fehér fagyöngy","fenyő fagyöngy"}
Vitis rupestris	{"sziklai szőlő","Sziklai szőlő"}
Vitis sylvestris	{"ligeti szőlő","Ligeti szőlő"}
Vitis vinifera	{"Bortermő szőlő","kerti szőlő"}
Vitis vulpina	{"parti szőlő","Parti szőlő"}
Vulpia bromoides	{"déli egércsenkesz","Déli egércsenkesz"}
Vulpia myuros	{"vékony egércsenkesz","Vékony egércsenkesz"}
Wolffia arrhiza	{vizidara,Vízidara}
Woodsia alpina	{"havasi szirtipáfrány","Havasi szirtipáfrány"}
Xanthium italicum	{"olasz szerbtövis","Olasz szerbtövis"}
Xanthium spinosum	{"szúrós szerbtövis","Szúrós szerbtövis"}
Xanthium strumarium	{bojtorjánszerbtövis,Bojtorjánszerbtövis}
Xeranthemum annuum	{"ékes vasvirág","Ékes vasvirág"}
Xeranthemum cylindraceum	{"hengeres vasvirág","Hengeres vasvirág"}
Yucca filamentosa	{"kerti pálmaliliom","Kerti pálmaliliom"}
Zannichellia palustris	{tófonal,Tófonal}
Abies nordmanniana	{"Kaukázusi jegenyefenyő"}
Abies cephalonica	{"Görög jegenyefenyő"}
Aconitum lycoctonum subsp. moldavicum	{"Kárpáti sisakvirág"}
Aconitum lycoctonum subsp. vulparia	{"Farkasölő sisakvirág"}
Trox hispidus niger	{"gömböc irhabogár"}
Vicia cracca subsp. kitaibeliana	{"Kitaibel kaszanyűg bükköny"}
Vicia villosa subsp. varia	{"Kopaszodó szöszös bükköny"}
Viola canina subsp. montana	{"Hegyi ibolya"}
Arachis hypogaea	{Földimogyoró}
Nothoscordum borbonicum	{"Magas szagtalanhagyma"}
Carex depressa	{"Erdélyi sás"}
Acipenser gueldenstaedti colchicus	{vágótok}
Aegithalos caudatus europaeus	{Őszapó}
Allium scorodoprasum subsp. waldsteinii	{"Ereszes hagyma"}
Allium senescens subsp. montanum	{"Hegyi hagyma"}
Allium ursinum subsp. ucrainicum	{"Ukrán medvehagyma"}
Amaranthus chlorostachys subsp. powellii	{Bouchon-disznóparéj}
Anisoplia zwickii lata	{"széles szipoly"}
Anogcodes dispar azurea	{"azúrkék álcincér"}
Anthicus dentatus transdanubialis	{"dunántúli fürgebogár"}
Aricia artaxerxes issekutzi	{"Bükki szerecsenboglárka"}
Arrhenatherum elatius subsp. bulbosum	{"Gumós franciaperje"}
Aster sedifolius subsp. sedifolius	{"Pettyegetett őszirózsa"}
Barbus peloponnesius petenyi	{Petényi-márna}
Biscutella laevigata subsp. angustifolia	{"Keskenylevelű korongpár"}
Brassica elongata subsp. elongata	{"Harasztos káposzta"}
Brassica napus subsp. napus	{Olajrepce}
Brassica napus subsp. rapifera	{Karórépa}
Brassica rapa subsp. campestris	{Tarlórépa}
Buglossoides arvensis subsp. arvensis	{"Mezei gyöngyköles"}
Bupleurum commutatum subsp. glaucocarpum	{"Deres buvákfű"}
Calliptamus barbarus parvus	{"barbár sáska"}
Cannabis sativa subsp. sativa	{"Termesztett kender"}
Carassius auratus gibelio	{ezüstkárász}
Cardamine pratensis subsp. paludosa	{"Fogas réti kakukktorma"}
Carduus collinus subsp. glabrescens	{"Kopaszodó magyar bogáncs"}
Carlina biebersteinii subsp. brevibracteata	{"Rövidgalléros bábakalács"}
Centaurea scabiosa subsp. fritschii	{"Dunántúli imola"}
Centaurea scabiosa subsp. tematinensis	{"Kopasz vastövű imola"}
Centaurea stoebe subsp. micranthos	{"Útszéli imola"}
Centaurium litorale subsp. uliginosum	{"Lápi ezerjófű"}
Cerastium arvense subsp. molle	{"Pillás parlagi madárhúr"}
Cerastium pumilum subsp. glutinosum	{"Halovány törpe madárhúr"}
Chalcalburnus chalcoides mento	{"állas küsz"}
Charissa intermedia budensis	{"Változó sziklaaraszoló"}
Cirsium eriophorum subsp. degenii	{Degen-aszat}
Cobitis taenia elongatoides	{vágócsík}
Coluber jugularis caspius	{"Haragos sikló"}
Corvus corone cornix	{"Dolmányos varjú"}
Cygnus columbianus bewickii	{"Kis hattyú"}
Daphne cneorum subsp. arbuscoloides	{"Bokros henye boroszlán"}
Daucus carota subsp. sativus	{Sárgarépa}
Dianthus giganteiformis subsp. pontederae	{"Magyar szegfű"}
Dianthus praecox subsp. lumnitzeri	{Lumnitzer-szegfű}
Dianthus praecox subsp. praecox	{"Korai fehér szegfű"}
Dianthus praecox subsp. regis-stephani	{"Szent István-szegfű"}
Digitaria sanguinalis subsp. pectiniformis	{"Pillás ujjasmuhar"}
Dorcadion pedestre kaszabi	{"Sziki kétsávos gyalogcincér"}
Echinops ritro subsp. ruthenicus	{"Kék szamárkenyér"}
Epacromius coerulipes pannonicus	{"pannon sáska"}
Epacromius tergestinus ponticus	{"pontusi sáska"}
Ephedra distachya subsp. monostachya	{"Kaukázusi csikófark"}
Erigeron annuus subsp. strigosus	{"Ligeti seprence"}
Erinaceus europaeus roumanicus	{"keleti sün"}
Erophila verna subsp. praecox	{"Korai ködvirág"}
Erysimum wittmannii subsp. pallidiflorum	{"Halványsárga repcsény"}
Euphrasia picta subsp. kerneri	{"Réti szemvidító"}
Foeniculum vulgare subsp. sativum	{Édeskömény}
Fraxinus angustifolia subsp. danubialis	{"Magyar kőris"}
Gambusia affinis holbrooki	{"szúnyogirtó fogasponty"}
Gentianella amarella subsp. livonica	{"Csinos tárnicska"}
Gladiolus communis subsp. byzantinus	{"Bizánci kardvirág"}
Heliopsis helianthoides subsp. scabra	{"Szőrös napszem"}
Heracleum sphondylium subsp. transsilvanicum	{"Erdélyi közönséges medvetalp"}
Hesperis matronalis subsp. nivea	{Vrabélyi-hölgyestike}
Hippolais pallida elaeica	{"Halvány geze"}
Hippophaë rhamnoides subsp. rhamnoides	{"Közönséges homoktövis"}
Hypericum maculatum subsp. desentangsiiforme	{"Kihagyni, kisbetűs"}
Iris variegata subsp. leucographa	{"Fehérerű nőszirom"}
Jovibarba globifera subsp. hirta	{"Sárga kövirózsa"}
Knautia dipsacifolia subsp. turocensis	{"Túróci varfű"}
Lathyrus laevigatus subsp. transsylvanicus	{"Erdélyi lednek"}
Leontodon hispidus subsp. danubialis	{"Dunai oroszlánfog"}
Leptura livida pecta	{"Barnás virágcincér"}
Leuciscus souffia agassizi	{"vaskos csabak"}
Linum flavum subsp. hungaricum	{"Magyar sárga len"}
Lunaria annua subsp. pachyrhiza	{"Répás holdviola"}
Saxicola torquata rubicola	{Cigánycsuk,"cigány csaláncsúcs","európai cigánycsuk"}
Malva sylvestris subsp. mauritiana	{"Mór erdei mályva"}
Melanophila picta decastigma	{"foltos fürgedíszbogár"}
Melitaea telona kovacsi	{"Magyar tarkalepke"}
Minuartia hirsuta subsp. frutescens	{"Magyar kőhúr"}
Molinia caerulea subsp. arundinacea	{"Nádképű kékperje"}
Montia fontana subsp. chondrosperma	{Forrásfű}
Mus musculus spicilegus	{"güzü egér"}
Mustela eversmanni hungarica	{molnárgörény}
Myrmeleon formicarius nigrolabris	{"Közönséges hangyaleső"}
Nacerdes dispar austriaca	{"azúrkék álcincér"}
Netocia cuprea obscura	{"olajzöld virágbogár"}
Odontopodisma schmidtii rubripes	{"vöröslábú hegyisáska"}
Orobanche ramosa subsp. nana	{"Apró vajvirág"}
Papaver dubium subsp. lecoqii	{"Fehér bujdosó mák"}
Papaver rhoeas subsp. strigosum	{Pipacs}
Pedestredorcadion pedestre kaszabi	{"Sziki kétsávos gyalogcincér"}
Persicaria lapathifolia subsp. brittingeri	{"Dunai lapulevelű keserűfű"}
Persicaria lapathifolia subsp. pallida	{"Molyhos lapulevelű keserűfű"}
Phoenicopterus ruber roseus	{"Rózsás flamingó"}
Picris hieracioides subsp. villarsii	{"Nagytermésű keserűgyökér"}
Plantago lanceolata subsp. eriophora	{"Molyhos lándzsás útifű"}
Polygala comosa subsp. podolica	{"Lengyel üstökös pacsirtafű"}
Portulaca oleracea subsp. sativa	{"Termesztett porcsin"}
Potentilla impolita subsp. wallrothii	{Wallroth-pimpó}
Potentilla thyrsiflora subsp. loczyana	{Lóczy-pimpó}
Primula veris subsp. canescens	{"Molyhos tavaszi kankalin"}
Prunus cerasus subsp. acida	{Cigánymeggy}
Pulsatilla pratensis subsp. zimmermanii	{"Hegyi kökörcsin"}
Ranunculus ficaria subsp. bulbilifer	{"Közönséges salátaboglárka"}
Ranunculus ficaria subsp. calthifolius	{"Gólyahírlevelű salátaboglárka"}
Ranunculus polyanthemos subsp. nemorosus	{"Berki boglárka"}
Rhodeus sericeus amarus	{"szivárványos ökle"}
Rhyparioides flavidus metelkanus	{Metelka-medvelepke}
Rosa jundzillii subsp. trachyphylla	{"Nagylevelű rózsa"}
Rosa villosa subsp. sancti-andreae	{"Szentendrei rózsa"}
Rutilus frisii meidingeri	{"gyöngyös koncér"}
Rutilus pigus virgo	{leánykoncér}
Sagina apetala subsp. erecta	{"Sziromtalan pillás zöldhúr"}
Salix repens subsp. rosmarinifolia	{"Cinege fűz"}
Sanguisorba minor subsp. polygama	{"Szárnyalt csabaíre"}
Scleranthus perennis subsp. dichotomus	{"Villás szikárka"}
Sedum rupestre subsp. glaucum	{"Kövi varjúháj"}
Sedum telephium subsp. maximum	{"Bablevelű varjúháj"}
Silene latifolia subsp. alba	{"Fehér mécsvirág"}
Silene otites subsp. hungarica	{"Magyar szikár habszegfű"}
Solanum nigrum subsp. humile	{"Alacsony fekete csucsor"}
Spergula arvensis subsp. maxima	{"Nagymagvú mezei csibehúr"}
Stipa crassiculmis subsp. euroanatolica	{"Vastagszárú árvalányhaj"}
Suaeda maritima subsp. salsa	{"Ágas sziki sóballa"}
Synharmonia lyncea agnata	{"bokorerdei katica"}
Tephroseris longifolia subsp. gaudinii	{"Tömött havasalji aggófű"}
Thalictrum minus subsp. olympicum	{"Hegyi borkóró"}
Tilia platyphyllos subsp. caucasica	{"Kaukázusi nagylevelű hárs"}
Tilia platyphyllos subsp. rubra	{"Vörös nagylevelű hárs"}
Torilis japonica subsp. ucranica	{"Ukrán tüskemag"}
Trachypteris picta decastigma	{"foltos fürgedíszbogár"}
Trifolium incarnatum subsp. molinerii	{"Fehéres bíbor here"}
Triturus cristatus carnifex	{"Alpesi tarajosgőte"}
Triturus cristatus dobrogicus	{"Dunai tarajosgőte"}
Trollius europaeus subsp. transsylvanicus	{"Erdélyi zergeboglár"}
Valeriana officinalis subsp. sambucifolia	{"Bodzalevelű macskagyökér"}
Verbascum lychnitis subsp. kanitzianum	{"Balkáni csilláros ökörfarkkóró"}
Rhacocleis germanica	{"német szöcske"}
Barbus meridionalis petenyi	{Petényi-márna}
Caspialosa kessleri pontica	{"dunai nagyhering"}
Falco rusticolus	{"északi sólyom"}
Charadrius dubius curonicus	{"Kis lile"}
Larus cachinnans michahellis	{"Sárgalábú sirály"}
Larus audounii	{korallsirály}
Apus pallidus illyricus	{"Halvány sarlósfecske"}
Acrocephalus arundinaceus arundindinaceus	{Nádirigó}
Helianthemum nummularium subsp. ovatum	{"Közönséges napvirág"}
Hieracium latifolium	{"Rövidlevelű hölgymál"}
Crepis mollis subsp. hieracioides	{"Bársonyos zörgőfű"}
Minuartia rubra	{"Nyalábos kőhúr"}
Minuartia glaucina	{"Tavaszi kőhúr"}
Arenaria procera	{"Hegyi homokhúr"}
Chenopodium carnosulum	{"Aprólevelű libatop"}
Quercus frainetto	{"Magyar tölgy"}
Verbascum chaixii subsp. austriacum	{"Osztrák ökörfarkkóró"}
Erysimum cheiri	{Sárgaviola}
Panicum ruderale	{Gyomköles}
Paspalum dilatatum	{"Széles csomósmuhar"}
Elaphomyces virgatosporus	{"csíkosspórájú álszarvasgomba"}
Tulostoma volvatum	{"Bocskoros nyelespöfeteg"}
Diphasiastrum tristachyum	{"Tölcséres laposkorpafű"}
Diphasiastrum issleri	{Issler-laposkorpafű}
Helleborus foetidus	{"Büdös hunyor"}
Helleborus niger	{"Fekete hunyor"}
Nigella sativa	{"Szőrös kandilla"}
Anemone japonica	{"Japán szellőrózsa"}
Clematis ×jackmanii	{"Kerti iszalag"}
Ceratocephala testiculata	{"Tarajos sarlóboglárka"}
Ceratocephala falcata	{Sarlóboglárka}
Ranunculus aquatilis	{"Nagy víziboglárka"}
Ranunculus radians	{"Sugaras víziboglárka"}
Ranunculus baudotii	{"Hosszúkocsányú víziboglárka"}
Ranunculus peltatus	{"Pajzsos víziboglárka"}
Ranunculus trichophyllus	{"Hínáros víziboglárka"}
Ranunculus rionii	{"Kopasztermésű víziboglárka"}
Ranunculus circinatus	{"Merev víziboglárka"}
Ranunculus penicillatus	{"Szőröstermésű víziboglárka"}
Ranunculus ficaria	{Salátaboglárka}
Adonis annua	{"Őszi hérics"}
Pulsatilla hungarica	{"Magyar kökörcsin"}
Adonis ×hybrida	{"Erdélyi hérics"}
Nymphaea rubra	{"Vörös tündérrózsa"}
Nelumbo nucifera	{"Indiai lótusz"}
Platanus occidentalis	{"Nyugati platán"}
Platanus orientalis	{"Keleti platán"}
Spiraea ×vanhouttei	{"Közönséges gyöngyvessző"}
Spiraea ×bumalda	{"Pompás gyöngyvessző"}
Spiraea alba	{"Lándzsáslevelű gyöngyvessző"}
Spiraea japonica	{"Japán gyöngyvessző"}
Spiraea chamaedryfolia	{"Hegyi gyöngyvessző"}
Sorbaria sorbifolia	{"Északi tollasgyöngyvessző"}
Physocarpus opulifolius	{Hólyagvessző}
Cotoneaster horizontalis	{"Kerti madárbirs"}
Chaenomeles japonica	{"Téglapiros japánbirs"}
Chaenomeles speciosa	{"Skarlátvörös japánbirs"}
Pyrus salviifolia	{"Zsályalevelű körte"}
Pyrus ×austriaca	{"Osztrák körte"}
Pyrus amygdaliformis	{"Mandulalevelű körte"}
Pyrus syriaca	{"Szír körte"}
Pyrus ×mecsekensis	{"Mecseki körte"}
Pyrus ×karpatiana	{"Kárpáti körte"}
Pyrus ×hazslinszkyana	{"Hazslinszky körte"}
Pyrus ×pannonica	{"Pannon körte"}
Pyrus ×praenorica	{"Alpokalji körte"}
Pyrus ×transdanubica	{"Dunántúli körte"}
Pyrus ×pomazensis	{"Pomázi körte"}
Malus pumila	{"Alacsony vadalma"}
Sorbus ulmiifolia	{Szillevelű-berkenye}
Sorbus thaiszii	{Thaisz-berkenye}
Sorbus hungarica	{Bornmüller-berkenye}
Sorbus bükkensis	{"Bükki berkenye"}
Sorbus zólyomii	{Zólyomi-berkenye}
Sorbus huljákii	{Hulják-berkenye}
Sorbus soói	{Soó-berkenye}
Sorbus vajdae	{Vajda-berkenye}
Sorbus jávorkae	{Jávorka-berkenye}
Sorbus gerecsensis	{"Gerecsei berkenye"}
Sorbus degenii	{Degen-berkenye}
Sorbus vértesensis	{"Vértesi berkenye"}
Sorbus pseudovértesensis	{"Ál-vértesi berkenye"}
Sorbus pseudobakonyesis	{"Rövidkaréjú berkenye"}
Sorbus kárpátii	{Kárpáti-berkenye}
Sorbus simonkaiana	{Simonkai-berkenye}
Sorbus eugenii-kelleri	{Keller-berkenye}
Sorbus adami	{Ádám-berkenye}
Sorbus rédliana	{"Rédl- berkenye"}
Sorbus barthae	{Bartha-berkenye}
Sorbus andreánszkyana	{Andreánszky-berkenye}
Sorbus gáyeriana	{Gáyer-berkenye}
Sorbus ×rotundifolia	{"Kereklevelű berkenye"}
Sorbus ×kitaibeliana	{Kitaibel-berkenye}
Crataegus laevigata	{Cseregalagonya}
Crataegus ×degenii	{Degen-galagonya}
Crataegus ×media	{"Hibrid galagonya"}
Rubus lentiginosus	{"Gyertyánlevelű szeder"}
Rubus silvaticus	{"Berki szeder"}
Rubus montanus	{"Fehéres szeder"}
Rubus histrix	{"Sün szeder"}
Fragaria ×ananassa	{"Kerti szamóca"}
Potentilla wiemanniana	{Wiemann-pimpó}
Potentilla norvegica	{"Norvég pimpó"}
Alchemilla vulgaris	{"Hegyeskaréjú palástfű"}
Rosa pimpinellifolia	{Jajrózsa}
Rosa jundzillii	{"Vöröslevelű rózsa"}
Rosa caryophyllacea	{Szegfűrózsa}
Rosa zagrebiensis	{"Zágrábi rózsa"}
Rosa ×dumalis	{"Ligeti rózsa"}
Rosa multiflora	{"Futó rózsa"}
Rosa ×alba	{"Fehér rózsa"}
Rosa ×centifolia	{"Százlevelű rózsa"}
Rosa ×damascena	{"Damaszkuszi rózsa"}
Rosa foetida	{"Sárga rózsa"}
Rosa blanda	{"Kőrislevelű rózsa"}
Rosa turbinata	{"Cövekes rózsa"}
Rosa majalis	{"Fahéj rózsa"}
Rosa ×scabriuscula	{"Érdeslevelű rózsa"}
Prunus padus	{Májusfa}
Prunus mahaleb	{Sajmeggy}
Prunus fruticosus	{Csepleszmeggy}
Prunus avium	{Vadcseresznye}
Prunus dulcis	{"Közönséges mandula"}
Prunus armeniaca	{Kajszi}
Prunus persica	{Barack}
Armeniaca vulgaris	{Kajszi}
Sedum neglectum	{"Adriai varjúháj"}
Sedum spectabile	{"Pompás varjúháj"}
Sedum ellacombianum	{Hokkaido-varjúháj}
Sedum sarmentosum	{"Indás varjúháj"}
Saxifraga stolonifera	{"Indás kőtörőfű"}
Ribes spicatum	{"Veres ribiszke"}
Ribes sanguineum	{"Vérpiros ribiszke"}
Hydrangea macrophylla	{"Kerti hortenzia"}
Deutzia scabra	{"Érdeslevelű gyöngyvirágcserje"}
Gymnocladus dioicus	{"Kanadai vasfa"}
Genista sagittalis	{"Szárnyas rekettye"}
Cytisus scoparius	{Seprőzanót}
Chamaecytisus hirsutus	{"Borzas zanót"}
Ononis natrix	{"Kígyó iglice"}
Medicago ×varia	{"Homoki lucerna"}
Medicago polymorpha	{"Déli lucerna"}
Melilotus altissimus	{"Réti somkóró"}
Melilotus indicus	{"Indiai somkóró"}
Trifolium lappaceum	{"Sávos here"}
Trifolium alexandrinum	{"Töviskés here"}
Lotus siliquosus	{Bársonykerep}
Lotus glaber	{"Sziki kerep"}
Lotus pedunculatus	{"Lápi kerep"}
Wisteria sinensis	{"Kínai lilaakác"}
Robinia viscosa	{"Enyves akác"}
Halimodendron halodendron	{Szikfa}
Caragana arborescens	{"Sárga borsófa"}
Astragalus galegiformis	{"Kaukázusi csüdfű"}
Ornithopus sativus	{Szerradella}
Securigera varia	{"Tarka koronafürt"}
Cicer arietinum	{Bagolyborsó}
Lathyrus odoratus	{"Szagos lednek"}
Lathyrus cicera	{"Csicseri lednek"}
Vigna unguiculata	{Boroszlánfélék}
Cytisus albus	{"Fehér zanót"}
Vicia serratifolia	{"Fogaslevelű bükköny"}
Anthyllis macrocephala	{"Magyar nyúlszapuka"}
Elaeagnus umbellata	{"Pirostermésű ezüstfa"}
Elaeagnus commutata	{"Amerikai ezüstfa"}
Elodea nuttalii	{"Aprólevelű átokhínár"}
Potamogeton ×zizii	{"Keskenylevelű békaszőlő"}
Potamogeton pusillus	{"Apró békaszőlő"}
Potamogeton compressus	{"Bodros békaszőlő"}
Hemerocallis lilioasphodelus	{Sárgaliliom}
Allium schoenoprasum	{Metélőhagyma}
Allium cepa	{Vöröshagyma}
Allium sativum	{Fokhagyma}
Allium ampeloprasum	{"Francia hagyma"}
Allium ramosum	{"Mandulaillatú hagyma"}
Allium lusitanicum	{"Hegyi hagyma"}
Prospero autumnale	{"Balkáni csillagvirág"}
Scilla amoena	{"Kedves csillagvirág"}
Chionodoxa luciliae	{"Fehérszemű hófény"}
Puschkinia scilloides	{"Csillagos puskin-csillagvirág"}
Ornithogalum nutans	{"Bókoló madártej"}
Ornithogalum ×degenianum	{Degen-madártej}
Hyacinthus orientalis	{"Kerti jácint"}
Muscari armeniacum	{"Örmény gyöngyike"}
Muscari neglectum	{"Fürtös gyöngyike"}
Ornithogalum kochii	{"Pusztai madártej"}
Gagea arvensis	{"Vetési tyúktaréj"}
Nothoscordum bivalve	{"Alacsony szagtalanhagyma"}
Ipheion uniflorum	{"Egyvirágú csillaghagyma"}
Fritillaria imperialis	{Császárkorona}
Tulipa ×gesnerana	{"Pompás tulipán"}
Crocus sativus	{"Jóféle sáfrány"}
Sisyrinchium bermudiana	{"Pázsitos sásbokor"}
Juncus gerardi	{"Sziki szittyó"}
Himantoglossum hircinum	{"Mediterrán sallangvirág"}
Corallorrhiza trifida	{Korallgyökér}
Scirpoides holoschoenus	{Szürkekáka}
Trichophorum cespitosum	{"Apró kisgyapjúsás"}
Cyperus glomeratus	{"Csomós palka"}
Cyperus longus	{"Hosszú palka"}
Cyperus glaber	{"Kopasz palka"}
Cyperus michelianus	{Iszapkáka}
Cyperus pannonicus	{Magyarpalka}
Cyperus flavescens	{Sárgapalka}
Carex otrubae	{"Berki sás"}
Carex pairaei	{"Berzedt sás"}
Carex ovalis	{Nyúlsás}
Carex acuta	{"Éles sás"}
Carex liparocarpos	{"Fényes sás"}
Carex halleriana	{"Sziklai sás"}
Carex curta	{"Szürkés sás"}
Juncellus serotinus	{"Őszi palka"}
Tradescantia virginiana	{Tradeszkancia}
Campanula medium	{Bögrevirág}
Campanula carpatica	{"Kárpáti harangvirág"}
Lobelia erinus	{Lobélia}
Protoclepsis tessulatum	{madárpióca}
Clepsine elegans	{csigapióca}
Clepsine marginata	{békapióca}
Haementeria costata	{teknőspióca}
Glossiphonia bioculata	{"fiahordó nadály"}
Glossiphonia heteroclita	{"kis csigapióca"}
Piscicola respirans	{"lapos halpióca"}
Piscicola lippa	{"közönséges halpióca"}
Aulastoma gulo	{lónadály}
Herpobdella atomaria	{"nyolcszemű nadály"}
Erpobdella monostriata	{"sávos pióca"}
Blanchardia bykowskii	{"kétéltű pióca"}
Trebacosa europaea	{"európai ál-kalózpók"}
Ephemerella ignita	{"Vörösbarna kérész"}
Baetis bioculatus	{"Teleszkópszemű kérész"}
Caenis lactella	{Törpekérész}
Caenis halterata	{"Apró kérész"}
Cloeon rufulum	{"Elevenszülő kérész"}
Cloeon inscriptum	{"Elevenszülő kérész"}
Cloeon praetextum	{"Kis kérész"}
Epeorus sylvicola	{"Lebegő kérész"}
Ephemerella gibba	{"Vörösbarna kérész"}
Heptagenia flavipennis	{"Halvány kérész"}
Metreletus hungaricus	{"Magyar kérész"}
Perla abdominalis	{"Burmeister álkérész"}
Isophya pienensis	{"pienini tarsza"}
Modicogryllus truncatus	{"déli homlokjegyestücsök"}
Poecilimon brunneri	{Brunner-pókszöcske}
Myrmecophyla acervorum	{"hangyász tücsök"}
Pteronemobius concolor	{"mocsári tücsök"}
Tartarogryllus burdigalensis	{"bordói tücsök"}
Gryllotalpa vulgaris	{lótücsök}
Acheta frontalis	{"homlokjegyes tücsök"}
Conocephalus discolor	{"kis kúpfejű szöcske"}
Homorocoryphus nitidulus	{"nagy kúpfejű szöcske"}
Pachytrachys gracilis	{"karcsú szöcske"}
Platycleis grisea	{"szürke rétiszöcske"}
Platycleis vittata	{"sávos rétiszöcske"}
Poecilimon fussi	{Fuss-pókszöcske}
Bicolorana bicolor	{"halványzöld rétiszöcske"}
Roeseliana roeselii	{Roesel-szöcske}
Phaneroptera quadripunctata	{"pontozott repülőszöcske"}
Pholidoptera cinerea	{"szürke avarszöcske"}
Montana montana	{"homokpusztai szöcske"}
Tesselana vittata	{"sávos rétiszöcske"}
Saga serrata	{"fűrészlábú szöcske"}
Tetrix nutans	{"vékonycsápú tövishátúsáska"}
Tridactylus pfaendleri	{Pfaendler-ásósáska}
Tridactylus variegatus	{"közönséges ásósáska"}
Acrida hungarica	{"sisakos sáska"}
Chorthippus longicornis	{"hosszúcsápú rétisáska"}
Glyptobothrus apricarius	{"szélesszárnyú tarlósáska"}
Glyptobothrus biguttulus	{"zengő tarlósáska"}
Glyptobothrus brunneus	{"közönséges tarlósáska"}
Glyptobothrus mollis	{"halk tarlósáska"}
Glyptobothrus vagans	{"bolygó tarlósáska"}
Chrysochraon brachyptera	{"smaragdzöld sáska"}
Gomphocerus rufus	{"erdei bunkóscsápú sáska"}
Mecosthetus grossus	{tundrasáska}
Omocestus ventralis	{"vöröshasú tarlósáska"}
Parapleurus alliaceus	{"hagymazöld sáska"}
Psophos stridulus	{"kereplő sáska"}
Stenobothrus fischeri	{Fischer-rétisáska}
Pararcyptera microptera	{"rövidszárnyú hegyisáska"}
Chorthippus elegans	{"csinos rétisáska"}
Chorthippus parallelus	{"hosszúcsápú rétisáska"}
Dirschius haemorrhoidalis	{"barna tarlósáska"}
Dirschius petraeus	{"szőke tarlósáska"}
Aelia klugi	{"csíkos szipolypoloska"}
Aethus flavicornis	{"barna földipoloska"}
Apolygus spinolai	{"zöld szőlőpoloska"}
Charagochilus gyllenhali	{"kis cigány-mezeipoloska"}
Chartoscirta cocksi	{mocsár-partipoloska}
Chlorocroa juniperina	{"zöld borókapoloska"}
Chlorocroa pinicola	{fenyőpoloska}
Chorosoma schillingi	{"karcsú botpoloska"}
Eurydema dominulum	{"tarka címerespoloska"}
Eurydema oleraceum	{paréjpoloska}
Eurydema ornatum	{káposztapoloska}
Eurydema ventrale	{"déli káposztapoloska"}
Eusarcoris aeneus	{"fémes feketefejű-poloska"}
Heterotoma meriopterum	{"laposcsápú mezeipoloska"}
Holcostethus vernalis	{"tavaszi címerespoloska"}
Hydrometra gracilentum	{"kis vízmérőpoloska"}
Microporus nigritus	{"homoki földipoloska"}
Nabicula flavomarginata	{"szegélyes tolvajpoloska"}
Orthops kalmi	{"kis tarka-mezeipoloska"}
Piesma quadratum	{"sziki recéspoloska"}
Piesma salsolae	{ballagófű-poloska}
Pirates hybridus	{"foltos rablópoloska"}
Raglius vulgaris	{"közönséges díszesbodobács"}
Stenodema laevigatum	{"karcsú mezeipoloska"}
Syromastes rhombeus	{"vitorlás karimáspoloska"}
Tropistethus holosericeus	{törpebodobács}
Zicrona coerulea	{"acélkék poloska"}
Aptus mirmicoides	{csalán-tolvajpoloska}
Dictyonota tricornis	{virág-csipkéspoloska}
Tingis similis	{aszat-csipkéspoloska}
Chloropulvinaria floccifera	{tea-gyapjaspajzstetű}
Poaspis jahandiezi	{nád-teknőspajzstetű}
Rhodococcus spireae	{gyöngyvessző-pajzstetű}
Nuculaspis abietis	{"fenyőrontó pajzstetű"}
Quadraspidiotus gigas	{nyárfa-kagylóspajzstetű}
Quadraspidiotus marani	{"déli körte-pajzstetű"}
Quadraspidiotus ostreaeformis	{"sárga alma-pajzstetű"}
Quadraspidiotus perniciosus	{"kaliforniai pajzstetű"}
Quadraspidiotus pyri	{"sárga körte-pajzstetű"}
Quadraspidiotus zonatus	{bükk-kagylóspajzstetű}
Rhizococcus insignis	{pázsit-tüskéspajzstetű}
Matsucoccus pini	{fenyő-kéregpajzstetű}
Allococcus vovae	{boróka-viaszospajzstetű}
Chaetococcus sulci	{csenkesz-gömbpajzstetű}
Gyrinus strigulosus	{"Recés keringőbogár"}
Gyrinus bicolor	{"Karcsú keringőbogár"}
Gyrinus pygolampis	{"Gyakori keringőbogár"}
Haliplus transversus	{Heyden-víztaposó}
Haliplus affinis	{"Csíkos víztaposó"}
Haliplus thoracicus	{"Sávosnyakú  víztaposó"}
Peltodytes curculinus	{"Zömök víztaposó"}
Noterus semipunctatus	{"Szélescsápú merülőbogár"}
Noterus sparsus	{"Szélescsápú merülőbogár"}
Noterus capricornis	{"Keskenycsápú merülőbogár"}
Hygrobia tarda	{"Európai pocsolyaúszó"}
Hygrotus parallelogrammus	{"Pettyesnyakú aprócsíkbogár"}
Agabus chalconatus	{"Rezes gyászcsíkbogár"}
Agabus erichsoni	{Erichson-gyászcsíkbogár}
Agabus nitidus	{"Pataki gyászcsíkbogár"}
Agabus nigricollis	{"Pataki gyászcsíkbogár"}
Agabus femoralis	{"Kis gyászcsíkbogár"}
Agabus abbreviatus	{"Harántsávos gyászcsíkbogár"}
Bidessus parvulus	{"Barna törpecsíkbogár"}
Copelatus ruficollis	{"Rozsdás csíkbogár"}
Copelatus agilis	{"Rozsdás csíkbogár"}
Cybister virens	{"Nagy búvárbogár"}
Cybister roeselii	{"Nagy búvárbogár"}
Graptodytes minimus	{"Zömök csíkbogárka"}
Hydaticus laevipennis	{"Csíkos mocsáricsíkbogár"}
Hydaticus modestus	{"Csíkos mocsáricsíkbogár"}
Hydaticus stagnalis	{"Sávos mocsáricsíkbogár"}
Hydroglyphus pusillus	{"Gyakori paránycsíkbogár"}
Hydroporus neuter	{"Erdei kiscsíkbogár"}
Hydroporus incertus	{"Érces kiscsíkbogár"}
Hydroporus ater	{"Gyakori kiscsíkbogár"}
Hygrotus picipes	{"Barázdás aprócsíkbogár"}
Hygrotus reticulatus	{"Tarka aprócsíkbogár"}
Ilybius ungularis	{"Nagy orsócsíkbogár"}
Ilybius foetidus	{"Szegélyes orsócsíkbogár"}
Ilybius obscurus	{"Gyakori orsócsíkbogár"}
Laccophilus interruptus	{"Cirpelő bukóbogár"}
Laccophilus obscurus	{"Néma bukóbogár"}
Laccophilus variegatus	{"Tarka bukóbogár"}
Laccophilus obsoletus	{"Tarka bukóbogár"}
Rhantus roridus	{"Sárgamellű particsíkbogár"}
Rhantus notatus	{"Sárgamellű particsíkbogár"}
Rhantus pulverosus	{"Gyakori particsíkbogár"}
Rhantus punctatus	{"Gyakori particsíkbogár"}
Suphrodytes fimbriatus	{"Kerekvállú csíkbogár"}
Calomera littoralis	{"sziki homokfutrinka"}
Cicindela arenaria	{"parti homokfutrinka"}
Scarites terricola	{vájárfutó}
Hydrochus carinatus	{"Nagy nyurgacsibor"}
Helophorus viridicollis	{"Lápi vésettcsibor"}
Megasternum concinnuum	{"Gombaevő csiborka"}
Berosus aericeps	{"Csíkosnyakú szemescsibor"}
Cercyon atricapillus	{"Feketefejű iszapcsiborka"}
Cercyon atriceps	{"Feketefejű iszapcsiborka"}
Cercyon lugubre	{"Nagy iszapcsiborka","Törpe iszapcsiborka"}
Enochrus minutus	{"Apró fakócsibor"}
Enochrus maritimus	{"Alföldi fakócsibor"}
Enochrus frontalis	{"Mocsári fakócsibor"}
Enochrus nigricans	{"Mocsári fakócsibor"}
Helochares griseus	{"Sötét laposcsibor"}
Helochares erythrocephalus	{"Sötét laposcsibor"}
Laccobius alutaceus	{"Kétfoltos pocsolyacsibor"}
Laccobius nigriceps	{"Termetes pocsolyacsibor"}
Megasternum obscurum	{"Gombaevő csiborka"}
Bacanius soliman	{"török sutabogár"}
Cylistus angustatus	{"keskeny lapossutabogár"}
Cylistus linearis	{fenyves-lapossutabogár}
Cylistus oblongus	{"hosszúkás lapossutabogár"}
Gnathoncus nanus	{"közönséges fészeksutabogár"}
Gnathoncus suturifer	{"varratos fészeksutabogár"}
Hetaerius ferrugineus	{"hangyász sutabogár"}
Myrmetes piceus	{vöröshangya-sutabogár}
Platysoma frontale	{"kéreglakó lapossutabogár"}
Gnathoncus rotundatus	{"közönséges fészeksutabogár"}
Hister uncinatus	{"holdfoltos sutabogár"}
Margarinotus stercorarius	{"ganajtúró sutabogár"}
Saprinus incognitus	{"varratos fémsutabogár"}
Teretrius picipes	{"éji sutabogár"}
Bisnius parcus	{"jövevény ganajholyva"}
Bolitobius formosus	{"kis gombászholyva"}
Bolitobius inclinans	{"magashegyi gombászholyva"}
Dropephylla ioptera	{"fénylő barázdásholyva"}
Geostiba gyoerffyi	{"apró humuszholyva"}
Hapalarea pygmaea	{"szurdoklakó barázdásholyva"}
Heterothops niger	{"fekete egérholyva"}
Ilyobates propinguus	{"hangyakedvelő vastagcsápúholyva"}
Megarthrus prosseni	{"kerekhátú sutaholyva"}
Mycetoporus eppelsheimiana	{"avarlakó gombászholyva"}
Ontholestes tesselatus	{"cifra holyva"}
Paederus limophilus	{"iszaplakó partiholyva"}
Platydracus fulfipes	{"fémkék holyva"}
Staphylinus erytropterus	{"aranypajzsú holyva"}
Acrolocha striata	{"apró barázdásholyva"}
Acrotona parva	{"mezei komposztholyva"}
Acrotona consanguinea	{"hegyi komposztholyva"}
Agaricochara laevicollis	{"hosszúcsápú taplóholyva"}
Aleochara nitida	{"kétfoltos fürkészholyva"}
Aleochara mycetophaga	{"gombakedvelő fürkészholyva"}
Aleochara ripicola	{"partlakó fürkészholyva"}
Aleochara crassicornis	{"partlakó fürkészholyva"}
Aleochara crassiuscula	{"kétszínű fürkészholyva"}
Aleochara succicola	{"közönséges fürkészholyva"}
Aloconota aegyptiaca	{"hegyi humuszholyva"}
Amischa soror	{"suta trapézfejűholyva"}
Amischa arata	{"suta trapézfejűholyva"}
Anomognathus inconspicuus	{"lapos kérgészholyva"}
Anotylus affinis	{"tüskésfarú korhóholyva"}
Anotylus mutator	{"erdei korhóholyva"}
Anotylus striatus	{"rovátkáshátú korhóholyva"}
Astenus longelytratus	{"közönséges köviholyva"}
Astenus brevelytratus	{"közönséges köviholyva"}
Astenus filiformis	{"karcsú köviholyva"}
Atheta pertyi	{"barnaszárnyú penészholyva"}
Atheta repanda	{"közönséges penészholyva"}
Atheta vaga	{"gödörkéshátú penészholyva"}
Atheta angusticollis	{"nyurga penészholyva"}
Bisnius rigidicornis	{"erdei ganajholyva"}
Bisnius denigrator	{"kurtaszárnyú ganajholyva"}
Bisnius varipennis	{"hosszúlábú ganajholyva"}
Bisnius pachycephalus	{"busafejű ganajholyva"}
Bisnius fuscus	{"odúlakó ganajholyva"}
Bledius dama	{"kétszarvú ásóholyva"}
Bledius fracticornis	{"kis ásóholyva"}
Bledius orientalis	{"posványlakó ásóholyva"}
Bolitochara lunulata	{"ékes tarkaholyva"}
Bryoporus merdarius	{"agyagszínű gombászholyva"}
Carpelimus bilineatus	{"rovátkás iszapholyva"}
Carpelimus despectus	{"sókedvelő iszapholyva"}
Carphacis angularis	{"laposcsápú gombászholyva"}
Ceritaxa wasserburgeri	{"sárgalábú penészholyva"}
Coproceramius lividus	{"halvány penészholyva"}
Cypha pulicaria	{"apró gömböcholyva"}
Deinopsis fuscata	{"közönséges recésholyva"}
Dinaraea hungarica	{"közönséges bibircsesholyva"}
Eusphalerum adustum	{"barnás virágholyva"}
Eusphalerum imhoffii	{"hosszúszárnyú virágholyva"}
Eusphalerum ophthalmicum	{"sárga virágholyva"}
Eusphalerum abdominale	{"gödörkés virágholyva"}
Eusphalerum testaceum	{"kis virágholyva"}
Eusphalerum florale	{"gödrös virágholyva"}
Falagria sulcata	{"barázdás karcsúholyva"}
Gabrius subnigritulus	{"barnáscombú ganajholyva"}
Gabrius astutoides	{"hegyi ganajholyva"}
Gabrius pennatus	{"berki ganajholyva"}
Gabrius vernalis	{"barnás ganajholyva"}
Gyrohypnus scoticus	{"recéshátú rovátkásholyva"}
Gyrophaena laevipennis	{"termetes taplóholyva"}
Gyrophaena convexicollis	{"füstös taplóholyva"}
Gyrophaena angustata	{"egyszínű taplóholyva"}
Haploglossa puncticollis	{"fészeklakó pudvaholyva"}
Haploglossa pulla	{"fészeklakó pudvaholyva"}
Hypnogyra glabra	{"kéreglakó rovátkásholyva"}
Ilyobates subopacus	{"alföldi vastagcsápúholyva"}
Lathrobium punctulatum	{"közönséges mocsárholyva"}
Platystethus luzei	{"réti korhóholyva"}
Lathrobium filiforme	{"fényes mocsárholyva"}
Lathrobium boreale	{"változékony mocsárholyva"}
Leptacinus aristaeus	{"barázdásfejű rovátkoltholyva"}
Leptacinus linearis	{"szurkos rovátkoltholyva"}
Leptacinus ops	{"recés rovátkoltholyva"}
Leptacinus othioides	{"recés rovátkoltholyva"}
Leptobium biguttulum	{"kétfoltos pocsolyaholyva"}
Leptophius relucens	{"alföldi rovátkásholyva"}
Leptusa vavrai	{"kormos tarkaholyva"}
Leptusa angusta	{"hegyi tarkaholyva"}
Lesteva villosa	{"csermelyfutó felemásholyva"}
Liogluta nitidula	{"fényeshatú humuszholyva"}
Liogluta crassicornis	{"avarlakó humuszholyva"}
Liogluta oblongiuscula	{"penészkedvelő humuszholyva"}
Lithocharis tricolor	{"vöröses rétiholyva"}
Lordithon trinotatus	{"címeres gombászholyva"}
Megarthrus affinis	{"érdes sutaholyva"}
Megarthrus sinuatocollis	{"szögleteshátú sutaholyva"}
Meotica capitalis	{"busafejű televényholyva"}
Meotica apicalis	{"busafejű televényholyva"}
Meotica hanseni	{"ligeti televényholyva"}
Metopsia clypeata	{"pajzsos sutaholyva"}
Microdota mortuorum	{"trágyatúró penészholyva"}
Mycetoporus ruficornis	{"taplókedvelő gombászholyva"}
Mycetoporus laevicollis	{"hegyvidéki gombászholyva"}
Mycetoporus baudueri	{"szenes gombászholyva"}
Mycetoporus brunneus	{"barna gombászholyva"}
Mycetoporus splendens	{"feketehátú gombászholyva"}
Nehemitropia sordida	{"orsócsápú komposztholyva"}
Neobisnius elongatus	{"vöröses ganajholyva"}
Ocypus similis	{"fekete holyva"}
Ocypus penetrans	{"szurtos holyva"}
Oligota atomaria	{"suta parányholyva"}
Omalium cursor	{"gödörkés barázdásholyva"}
Oxypoda lividipennis	{"közönséges pudvaholyva"}
Oxypoda umbrata	{"penészkedvelő pudvaholyva"}
Oxypoda sericea	{"selymes pudvaholyva"}
Oxypoda amoena	{"rövidcsápú pudvaholyva"}
Oxypoda riparia	{"ligeti pudvaholyva"}
Oxypoda rugulosa	{"ligeti pudvaholyva"}
Oxypoda humidula	{"erdei pudvaholyva"}
Paederus baudii	{"vaskos partiholyva"}
Parocyusa femoralis	{"szúnyoglábú partfutóholyva"}
Philhygra balcanica	{"berki penészholyva"}
Philhygra parca	{"ártéri penészholyva"}
Philhygra dentifera	{"folyóparti penészholyva"}
Philhygra halophila	{"posványlakó penészholyva"}
Philonthus varius	{"réti ganajholyva"}
Philonthus fuscipennis	{"fémes ganajholyva"}
Philonthus ochropus	{"közönséges ganajholyva"}
Philonthus bimaculatus	{"kétfoltos ganajholyva"}
Spatulonthus agilis	{"rövidcsápú ganajholyva"}
Philonthus fulvipes	{"sárgalábú ganajholyva"}
Philonthus fumigatus	{"füstös ganajholyva"}
Philonthus niger	{"bronzfényű ganajholyva"}
Philonthus chalceus	{"érceszöld ganajholyva"}
Phloeopora angustiformis	{"tölgyes szúvadászholyva"}
Phloeostiba flavipes	{"dohányszínű kéregholyva"}
Phyllodrepoidea creatoris	{"barázdált felemásholyva"}
Plataraea brunnea	{"északi penészholyva"}
Plataraea melanocephala	{"sötétfejű penészholyva"}
Plataraea interurbana	{"barna penészholyva"}
Pronomaea rostrata	{"kis csőrösholyva"}
Proteinus macropterus	{"domború sutaholyva"}
Quedius rufitarsis	{"fényes mohaholyva"}
Quedius assimilis	{"fényes mohaholyva"}
Quedius tristis	{"dunántúli mohaholyva"}
Quedius limbatoides	{"közönséges mohaholyva"}
Quedius fuscipennis	{"üregi mohaholyva"}
Quedius ruficollis	{"homoki mohaholyva"}
Quedius attenuatus	{"kecses mohaholyva"}
Quedius analis	{"vöröses mohaholyva"}
Quedius humeralis	{"forráslakó mohaholyva"}
Quedius ventralis	{"barnás mohaholyva"}
Rugilus scutellatus	{"vöröshátú cérnanyakúholyva"}
Rugilus fragilis	{"vöröshátú cérnanyakúholyva"}
Scopaeus scitulus	{"pusztai turzásholyva"}
Scopaeus cognatus	{"hegyi turzásholyva"}
Stenus angustatus	{"iszaplakó szemesholyva"}
Stenus bipunctatus	{"közönséges szemesholyva"}
Stenus cautus	{"kis szemesholyva"}
Stenus rossicus	{"ligeti szemesholyva"}
Stenus umbricus	{"délvidéki szemesholyva"}
Stenus coarcticollis	{"érces szemesholyva"}
Stenus erichsoni	{"fényes szemesholyva"}
Stenus rogeri	{"szőröshasú szemesholyva"}
Stenus exiguus	{"törpe szemesholyva"}
Stenus humiliodes	{"tömzsi szemesholyva"}
Sunius austriacus	{"sötétfarú lombholyva"}
Tachinus rufipes	{"közönséges fürgeholyva"}
Tachyporus scutellaris	{"halvány kószaholyva"}
Tachyporus macropterus	{"apró kószaholyva","hosszúszárnyú kószaholyva"}
Tasgius compressus	{"vöröslábú holyva"}
Tetartopeus fennicus	{"mocsári mocsárholyva"}
Tetartopeus confusus	{"mocsári mocsárholyva"}
Traumoecia excavata	{"gödröshátú penészholyva"}
Xantholinus sublinearis	{"pusztai rovátkásholyva"}
Xantholinus roubali	{"közönséges rovátkásholyva"}
Xantholinus balaton	{"közönséges rovátkásholyva"}
Xantholinus varhegyanus	{"közönséges rovátkásholyva"}
Xantholinus lichtneckerti	{"közönséges rovátkásholyva"}
Xantholinus magyaricus	{"közönséges rovátkásholyva"}
Xantholinus pseudobalaton	{"közönséges rovátkásholyva"}
Xantholinus clairei	{"dunántúli rovátkásholyva"}
Xylodromus heterocerus	{"vöröses felemásholyva"}
Aleochara lateralis	{"partlakó fürkészholyva"}
Oromus corvinus	{"hollószínű trágyabogár"}
Phalacronothus paracoenosus	{"széleslábú trágyabogár"}
Phalacronothus pusillus	{"apró trágyabogár"}
Phalacronothus quadriguttatus	{"négycseppes trágyabogár"}
Cetonischema aeruginosa	{"pompás virágbogár"}
Codocera ferrugineum	{"nagyrágós homoktúróbogár"}
Epicometis hirta	{"bundás virágbogár"}
Eupotosia affinis	{"smaragdzöld virágbogár"}
Liocola lugubris	{"márványos virágbogár"}
Miltotrogus aequinoctialis	{"tavaszeleji cserebogár"}
Miltotrogus vernus	{"tavaszi cserebogár"}
Netocia fieberi	{"rezes virágbogár"}
Netocia ungarica	{"magyar virágbogár"}
Odonteus armiger	{"mozgószarvú álganéjtúró"}
Omaloplia nigromarginata	{"szegélyes kiscserebogár"}
Rhizotrogus aestivus	{"tavaszvégi cserebogár"}
Trox eversmannii	{"homoki irhabogár"}
Geotrupes stercorosus	{"erdei álganéjtúró"}
Aphodius immundus	{"viaszsárga trágyabogár"}
Aphodius depressus	{"lapos trágyabogár"}
Aphodius luridus	{"tarka trágyabogár"}
Aphodius rufipes	{"vörösbarna trágyabogár"}
Aphodius nemoralis	{"hegyi trágyabogár"}
Aphodius ater	{"kormos trágyabogár"}
Aphodius convexus	{"domború trágyabogár"}
Aphodius rufus	{"sárgásvörös trágyabogár"}
Aphodius sordidus	{"homoki trágyabogár"}
Aphodius brevis	{"zömök trágyabogár"}
Aphodius aestivalis	{"vöröshasú trágyabogár"}
Aphodius satellitius	{"címeres trágyabogár"}
Aphodius circumcinctus	{"sziki trágyabogár"}
Aphodius ictericus	{"fényes trágyabogár"}
Aphodius nitidulus	{"fényes trágyabogár"}
Aphodius lugens	{"borostyánsárga trágyabogár"}
Aphodius granarius	{"szurokszínű trágyabogár"}
Aphodius distinctus	{"rajzos trágyabogár"}
Aphodius melanostictus	{"foltos trágyabogár"}
Aphodius paykulli	{"kockás trágyabogár"}
Aphodius pictus	{"festett trágyabogár"}
Aphodius erraticus	{"barnáshátú trágyabogár"}
Aphodius scrutator	{"vörös trágyabogár"}
Aphodius merdarius	{"szegélyes trágyabogár"}
Aphodius subterraneus	{"barázdás trágyabogár"}
Aphodius lividus	{"turjáni trágyabogár"}
Aphodius maculatus	{"foltosszárnyú trágyabogár"}
Aphodius niger	{"iszaplakó trágyabogár"}
Aphodius plagiatus	{"réti trágyabogár"}
Aphodius consputus	{"maszatos trágyabogár"}
Aphodius prodromus	{"sárgalábú trágyabogár"}
Aphodius pubescens	{"selymes trágyabogár"}
Aphodius punctatosulcatus	{"pusztai trágyabogár"}
Aphodius reyi	{"fakó trágyabogár"}
Aphodius sphacelatus	{"szegettnyakú trágyabogár"}
Aphodius varians	{"változékony trágyabogár"}
Aphodius venyigei	{"változékony trágyabogár"}
Aphodius obliteratus	{"sötétjegyű trágyabogár"}
Aphodius serotinus	{"őszi trágyabogár"}
Aphodius corvinus	{"hollószínű trágyabogár"}
Aphodius haemorrhoidalis	{"csúcsfoltos trágyabogár"}
Aphodius biguttatus	{"kétfoltos trágyabogár"}
Aphodius citellorum	{"ürgevendég trágyabogár"}
Aphodius paracoenosus	{"széleslábú trágyabogár"}
Aphodius pusillus	{"apró trágyabogár"}
Aphodius quadriguttatus	{"négycseppes trágyabogár"}
Aphodius quadrimaculatus	{"négyfoltos trágyabogár"}
Aphodius putridus	{"parányi trágyabogár"}
Aphodius uliginosus	{"erdei trágyabogár"}
Aphodius porcus	{"rozsdaszínű trágyabogár"}
Aphodius sturmi	{"vöröses trágyabogár"}
Aphodius fossor	{"nagy trágyabogár"}
Aphodius scrofa	{"szőrös trágyabogár"}
Aphodius sticticus	{"cirmos trágyabogár"}
Potosia aeruginosa	{"pompás virágbogár"}
Gnorimus octopunctatus	{"nyolcpettyes virágbogár"}
Gymnopleurus cantharus	{"közönséges törpegalacsinhajtó"}
Hoplia farinosa	{"ezüstös virágcserebogár"}
Hoplia subnuda	{"homoki virágcserebogár"}
Potosia ungarica	{"magyar virágbogár"}
Platycerus cribratus	{"nagy fémesszarvasbogár"}
Psammodius sulcicollis	{"homoktúró trágyabogár"}
Scarabaeus affinis	{óriás-galacsinhajtó}
Geotrupes vernalis	{"tavaszi álganéjtúró"}
Cyphon phragmiteticola	{"nádi rétbogár"}
Helodes hausmanni	{"foltosnyakú rétbogár"}
Helodes johni	{"német rétbogá"}
Helodes marginata	{"szegélyes rétbogár"}
Helodes minuta	{"kis rétbogár"}
Helodes flavicollis	{"sárganyakú rétbogár"}
Aurigena lugubris	{"bronzos díszbogár"}
Chrysobothrys affinis	{"aranypettyes díszbogár"}
Chrysobothrys chrysostigma	{"fényesjegyű díszbogár"}
Chrysobothrys igniventris	{"tüzeshasú díszbogár"}
Nalanda fulgidicollis	{"aranyostorú tompadíszbogár"}
Palmar festiva	{boróka-tarkadíszbogár}
Lamprodila decipiens	{nyírfa-tarkadíszbogár}
Lampra mirifica	{szilfa-tarkadíszbogár}
Lampra rutilans	{hársfa-tarkadíszbogár}
Ptosima flavoguttata	{"sárgafoltos díszbogár"}
Trachys scrobiculata	{menta-vájárdíszbogár}
Anthaxia millefolii	{cickafark-virágdíszbogár}
Agrilus sexguttatus	{"hatpettyes karcsúdíszbogár"}
Agrilus aurichalceus	{földiszeder-karcsúdíszbogár}
Agrilus acutangulus	{cinegefűz-karcsúdíszbogár}
Anthaxia aurulenta	{szil-virágdíszbogár}
Anthaxia balatonica	{"isztriai virágdíszbogár"}
Coraebus sinuatus	{pimpó-díszbogár}
Coraebus fasciatus	{"szalagos díszbogár"}
Dicerca acuminata	{nyírfa-díszbogár}
Habroloma nana	{"parányi vájárdíszbogár"}
Byrrhus lineatus	{"holdas labdacsbogár"}
Cytilus auricomus	{"réti labdacsbogár"}
Latelmis volckmari	{"sebesvízi karmosbogár"}
Helichus substriatus	{"parti fülescsápúbogár"}
Heterocerus hispidulus	{"keskeny iszapbogár"}
Heterocerus pruinosus	{"tarka iszapbogár"}
Heterocerus sericans	{"selymes iszapbogár"}
Actenicerus sjaelandicus	{"márványos pattanó"}
Adelocera punctata	{"pontozott pattanó"}
Adelocera quercea	{"tarka pikkelyespattanó"}
Adrastus rachifer	{"közönséges cserjepattanó"}
Ampedus cinnabarinus	{"cinóbervörös pattanó"}
Dicronychus equisetioides	{"ólmos szívespattanó"}
Isorhipis marmottani	{"kétszínű tövisnyakúbogár"}
Isorhipis melasoides	{"legyezős tövisnyakúbogár"}
Rhacopus attenuatus	{"keskeny tövisnyakúbogár"}
Thambus frivaldszkyi	{Frivaldszky-tövisnyakúbogár}
Trixagus aurociliatus	{"sötét merevbogár"}
Agriotes incognitus	{"szürke pattanó"}
Brachylacon murinus	{"egérszínű pattanó"}
Ampedus elongatulus	{"hosszúkás pattanó"}
Betarmon ferrugineus	{"kerekfoltos pattanó"}
Drapetes biguttatus	{"kétcseppes merevpattanó"}
Ludius ferrugineus	{fűzfapattanó}
Pseudathous hirtus	{"borzas pattanó"}
Pseudathous niger	{szerecsenpattanó}
Hypoganus cinctus	{"téglavörös pattanó"}
Kibunea minuta	{"fekete bokorpattanó"}
Melanotus niger	{"sávos gyászpattanó"}
Melanotus rufipes	{"vöröslábú gyászpattanó"}
Dirhagus emyi	{"törpe tövisnyakúbogár"}
Dirhagus lepidus	{"csinos tövisnyakúbogár"}
Dirhagus palmi	{Palm-tövisnyakúbo}
Dirhagus pygmaeus	{"apró tövisnyakúbogár"}
Neopristilophus depressus	{"lapos pattanó"}
Cidnopus parvulus	{"bronzos bokorpattanó"}
Selatosomus impressus	{"szürkeszőrű pattanó"}
Selatosomus nigricornis	{"feketecsápú pattanó"}
Anchastus acuticornis	{"fűrészescsápú pattanó"}
Selatosomus latus	{"széles pattanó"}
Zorochros minimus	{"fekete fövenypattanó"}
Metacantharis haemorrhoidalis	{"tavaszi lágybogár"}
Platycis minuta	{"kis hajnalbogár"}
Cantharis oralis	{"szegélyes lágybogár"}
Cantharis Csikii	{"déli lágybogár"}
Cantharis hungarica	{"mezei lágybogár"}
Cantharis cinctithorax	{"különös lágybogár"}
Phausis splendidula	{"törpe szentjánosbogár"}
Aplatopterus rubens	{"cinóbervörös hajnalbogár"}
Methacantharis clypeata	{"tavaszi lágybogár"}
Attagenus dalmatinus	{"barna szűcsbogár"}
Attagenus piceus	{gyapjú-szűcsbogár}
Dermestes atomarius	{"ártéri porva"}
Dermestes vulpinus	{"tüskés porva"}
Megatoma pici	{"vöröscsápú porva"}
Hendecatomus reticulatus	{"recés álcsuklyásszú"}
Caenocara affinis	{"rövidszőrű pöfetegálszú"}
Ochina latreillei	{"kétszínű álszú"}
Ptinus subpillosus	{"keskenynyakú tolvajbogár"}
Anobium striatum	{"kis kopogóbogár"}
Ptinus nitidus	{"fényes tolvajbogár"}
Tipnus unicolor	{"zömök tolvajbogár"}
Oligomerus reyi	{"pontsoros álszú"}
Ptinus hungaricus	{"sarkantyús tolvajbogár"}
Dolichomorphus femoralis	{"pusztai karimás lágybogár"}
Malachius geniculatus	{"sárgaarcú bibircsbogár"}
Pseudoclerops mutillarius	{"feketenyakú szúfarkas"}
Psilotrhrix femoralis	{"pusztai karimás lágybogár"}
Enicopus pilosus	{"bundás lágybogár"}
Korynetes meridionalis	{"kék csontbogár"}
Trogossita coerulea	{"kék szúvadász"}
Tenebroides maroccanus	{"lisztmentő bogár"}
Aspidiphorus orbicularis	{"barázdáshomlokú gömböc-áltaplószú"}
Ceratomegilla notata	{"alhavasi katica"}
Ceratomegilla undecimnotata	{bogáncskatica}
Clypastraea brunnea	{"barna pontbogár"}
Clypastraea orientalis	{"keleti pontbogár"}
Coccidula rufa	{"nádi félböde"}
Coccidula scutellata	{"mocsári félböde"}
Coccinula quatuordecimpustulata	{"feketesárga katóka"}
Coccinula sinuatomarginata	{"oldalsávos katóka"}
Corylophus cassidioides	{"kopasz pontbogár"}
Epuraea placida	{"szelíd fénybogár"}
Hyperaspis erythrocephala	{"hatcseppes szerecsenkata"}
Hyperaspis quadrimaculata	{"négycseppes szerecsenkata"}
Propylea quatuordecimguttata	{"tizennégypettyes füsskata"}
Psammoecus bipunctatus	{"kétpettyes fogasnyakú-lapbogár"}
Philothermus semistriatum	{"félsávos kéregbogár"}
Rhyzobius chrysomeloides	{"szalagos félböde"}
Rhyzobius litura	{"félholdas félböde"}
Sospita vigintiguttata	{"húszcseppes füsskata"}
Triplax lacordairei	{"szegélyesnyakú tarbogár"}
Carcophilus hemipterus	{"kétszínű gyümölcsfénybogár"}
Airaphilus geminus	{"hosszúkás fogasnyakú-lapbogár"}
Brachypterolus villiger	{oroszlánszáj-álfénybogár}
Exochomus quadripustulatus	{"négyfoltos szerecsenkata"}
Byturus aestivus	{gyömbérgyökérbogár}
Semiadalia notata	{"alhavasi katica"}
Semiadalia undecimnotata	{bogáncskatica}
Sacium brunneum	{"barna pontbogár"}
Sacium orientale	{"keleti pontbogár"}
Coccinella distincta	{hangyászkatica}
Epuraea adumbrata	{"sötétlő fénybogár"}
Epuraea depressa	{"nyári fénybogár"}
Epuraea castanea	{"gesztenyebarna fénybogár"}
Epuraea bickhardti	{Marseul-fénybogár}
Epuraea deleta	{bükk-fénybogár}
Epilachna argus	{földitök-böde}
Adonia variegata	{"tizenhárompettyes katica"}
Haplolophus neglectus	{"nyugati gencsér"}
Meligethes obscurus	{gamandor-fénybogár}
Meligethes erythropus	{kerep-fénybogár}
Meligethes viduatus	{kenderkefű-fénybogár}
Meligethes flavipes	{peszterce-fénybogár}
Mycetaea hirta	{"szőrös álböde"}
Mychophilus minutus	{törpe-álböde}
Paramysia oblongoguttata	{"sávos füsskata"}
Synharmonia conglobata	{"rózsás katica"}
Synharmonia impustulata	{"fekete katica"}
Phalacrus brisouti	{"közönséges kalászbogár"}
Pocadius lanuginosus	{"gyapjas pöfetegfénybogár"}
Pseudophilothermus evanescens	{"barna kéregbogár"}
Pseudophilothermus semistriatum	{"félsávos kéregbogár"}
Thea vigintiduopunctata	{"huszonkétpettyes katica"}
Rhypobius ruficollis	{"vörösnyakú pontbogár"}
Lithophilus connatus	{"pusztai földiböde"}
Satorystia meschniggi	{tölgy-zuzmóbogár}
Cis jacquemarti	{"nyári taplószú"}
Xylographus bostrychoides	{"fűrészeslábú taplószú"}
Cis nitidus	{"sima taplószú"}
Cis pubescens	{"sávos taplószú"}
Orthocis reflexicollis	{"szélesnyakú taplószú"}
Orthocis oblongus	{"törpe taplószú"}
Orthocis perrisi	{"domború taplószú"}
Cicones undatus	{"hullámos héjbogár"}
Cicones variegatus	{"tarka héjbogár"}
Endophloeus markovichianus	{"fogasszélű héjbogár"}
Langelandia anophthalma	{"vak héjbogár"}
Cicones pictus	{"hullámos héjbogár"}
Tetratoma desmaresti	{"szőrös álkomorka"}
Phloiotrya vaudoueri	{"hengeres komorka"}
Xylita buprestoides	{"selymes komorka"}
Zilora eugeniae	{"sötét komorka"}
Zilora sericea	{"sötét komorka"}
Evaniocera dufouri	{"szürke darázsbogár"}
Cyttaroecus paradoxus	{"tollascsápú darázsbogár"}
Corticeus bicolor	{"kétszínű kéregbújó"}
Corticeus fasciatus	{"öves kéregbújó"}
Corticeus fraxini	{"szélesnyakú kéregbújó"}
Corticeus linearis	{"karcsú kéregbújó"}
Corticeus longulus	{"keskenynyakú kéregbújó"}
Corticeus pini	{fenyő-kéregbújó}
Corticeus rufulus	{"vöröses kéregbújó"}
Corticeus unicolor	{"egyszínű kéregbújó"}
Corticeus versipellis	{"feketésbarna kéregbújó"}
Enoplopus velikensis	{"cirpelő gyászbogár"}
Gonocephalum pusillum	{"kis gyászbogár"}
Oodescelis polita	{"sima gyászbogár"}
Oodescelis melas	{"alföldi gyászbogár"}
Stenomax aeneus	{"bronzos gyászbogár"}
Cteniopus flavus	{"közönséges kénbogár"}
Isomira budensis	{"vastagcsápú alkonybogár"}
Laena ormayi	{Reitter-avargyászbogár}
Lagria tenuicollis	{"feketelábú gyapjasbogár"}
Cylinronotus convexus	{"domború gyászbogár"}
Cylindronotus dermestoides	{"rövidszárnyú gyászbogár"}
Oplocephala haemorrhoidalis	{"szarvas taplóbogá"}
Omophlus rufitarsis	{"szőrösnyakú pejbogár"}
Stenomax lanipes	{"bronzos gyászbogár"}
Cylindronotus aeneus	{"bronzos gyászbogár"}
Uloma perroudi	{"kis rágványbogár"}
Anogcodes ferruginea	{"rozsdás álcincér"}
Anogcodes ustulata	{"fogascombú álcincér"}
Nacerdes ferruginea	{"rozsdás álcincér"}
Nacerdes fulvicollis	{"fekete álcincér"}
Nacerdes ruficollis	{"sárganyakú álcincér"}
Nacerdes rufiventris	{"vöröshasú álcincér"}
Nacerdes ustulata	{"fogascombú álcincér"}
Asclera caerulea	{"zöldeskék álcincér"}
Xanthochroa carniolica	{"karnióliai álcincér"}
Nacerda melanura	{"feketevégű álcincér"}
Oncomera femorata	{"feketecombú álcincér"}
Oedemera laticollis	{"szélestorú álcincér"}
Oedemera subulata	{"feketeszegélyű álcincér"}
Apalus necydalaeus	{"pusztai élősdibogár"}
Mylabris tenera	{"kis hólyaghúzó"}
Mylabris polymorpha	{"változó hólyaghúzó"}
Meloe punctatoradiatus	{óriás-nünüke}
Meloe coriarius	{"fekete nünüke"}
Colposis mutilata	{"zöld álormányos"}
Rabocerus foveolatus	{"gödrösnyakú álormányos"}
Rabocerus gabrieli	{"északi álormányos"}
Lissodema quadripustulatum	{"négyfoltos álormányos"}
Rhinosimus aeneus	{"fémes álormányos"}
Rhinosimus planirostris	{"sötétnyakú álormányos"}
Rhinosimus ruficollis	{"vörösnyakú álormányos"}
Salpingus castaneus	{"barna álormányos"}
Salpingus stockmanni	{"fekete álormányos"}
Sphaeriestes ater	{"fekete álormányos"}
Anthicus schmidti	{Schmidt-fürgebogár}
Notoxus appendicinus	{"déli nyakszarvúbogár"}
Formicomus pedestris	{"hangyaszerű fürgebogár"}
Anthicus gracilis	{"karcsú fürgebogár"}
Anthicus sellatus	{"nyerges fürgebogár"}
Anthicus humilis	{"sókedvelő fürgebogár"}
Anthicus hispidus	{"szőrös fürgebogár"}
Anthicus nectarinus	{"nagy fürgebogár"}
Anthicus unicolor	{"fekete fürgebogár"}
Notoxus szalavszkyi	{"nagy nyakszarvúbogár"}
Notoxus cornutus	{közép-nyakszarvúbogár}
Anthicus bifasciatus	{"kétöves fürgebogár"}
Anthicus kolenatii	{"kétöves fürgebogár"}
Anthicus floralis	{"virágjáró fürgebogár"}
Anthicus formicarius	{"simanyakú fürgebogár"}
Anthicus tobias	{"fűzöttnyakú fürgebogár"}
Aderus nigrinus	{"fekete korhóbogár"}
Aderus oculatus	{"nagyszemű korhóbogár"}
Aderus pygmaeus	{"őszes korhóbogár"}
Aderus lokvenci	{"őszes korhóbogár"}
Aderus pruinosus	{"deres korhóbogár"}
Diabrotica virgifera	{"amerikai kukoricabogár"}
Anaerea carcharias	{"Nagy nyárfacincér"}
Anaerea similis	{Kecskefűzcincér}
Callimellum angulatum	{"Zöld tölgycincér"}
Chlorophorus herbsti	{"Foltos darázscincér"}
Compsidia populnea	{"Kis nyárfacincér"}
Morinus funereus	{Gyászcincér}
Phytoecia coerulea	{"Fémzöld fűcincér"}
Tetrops praeusta	{"Négyszemű cincér"}
Tetrops starki	{"Feketeszélű aprócincér"}
Acanthoderes clavipes	{"Tarka cincér"}
Agapanthia pannonica	{"Sávos bogáncscincér"}
Agapanthia leucaspis	{"Magyar bogáncscincér"}
Leptura dubia	{Feketeszélű}
Leptura inexpectata	{"Északi virágcincér"}
Leptura sanguinolenta	{"Kétszínű virágcincér"}
Stenocorus qercus	{Tölgycincér}
Leptura rufipes	{"Vöröslábú virágcincér"}
Leptura sexguttata	{"Foltos virágcincér"}
Callidostola aenea	{"Bronzos kéregcincér"}
Palaeocallidium coriaceum	{"Bőrszárnyú fenyőcincér"}
Pilema angulatum	{"Zöld tölgycincér"}
Phytoecia scutellata	{Sarlófűcincér}
Dorcadion aethiops	{"Fekete gyalogcincér"}
Cerambyx velutinus	{"Molyhos hőscincér"}
Stenidea genei	{"Keskeny tölgycincér"}
Acmaeops collaris	{"Vörösnyakú virágcincér"}
Carilia virginea	{"Kis fémescincér"}
Molorchus kiesenwetteri	{"Mandula légycincér"}
Glaphyra salicicola	{"Fűz légycincér"}
Molorchus umbellatarum	{"Apró légycincér"}
Grammoptera variegata	{"Fekete galagonyacincér"}
Dorcatypus tristis	{"Selymes alkonycincér"}
Leioderus kollari	{"Vörösessárga juharcincér"}
Strangalia aethiops	{Szerecsencincér}
Strangalia arcuata	{Völgycincér}
Leptura arcuata	{Völgycincér}
Leptura mimica	{Völgycincér}
Strangalia aurulenta	{"Sárgaszőrű szalagoscincér"}
Strangalia quadrifasciata	{"Feketeszőrű szalagoscincér"}
Leptura virens	{"Alhavasi virágcincér"}
Monochamus urussovi	{"Keleti fenyvescincér"}
Phytoecia nigripes	{"Feketefejű cincér"}
Phytoecia argus	{"Árgusszemű cincér"}
Oplosia fennica	{"Foltos hárscincér"}
Phytoecia coerulescens	{Kígyósziszcincér}
Phytoecia molybdaena	{"Fémkék dudvacincér"}
Phytoecia uncinata	{Szeplőlapucincér}
Toxotus cursor	{Futócincér}
Judolia cerambyciformis	{"Változékony virágcincér"}
Judolia erratica	{"Rajzos virágcincér"}
Leptura fulva	{"Vörhenyes virágcincér"}
Leptura maculicornis	{"Tarkacsápú virágcincér"}
Dorcadion pedestre	{"Kétsávos gyalogcincér"}
Dorcadion scopolii	{"Nyolcsávos gyalogcincér"}
Strangalia revestita	{"Kétszínű karcsúcincér"}
Echinocerus floralis	{Lucernacincér}
Phymatodes alni	{"Apró háncscincér"}
Phymatodes fasciatus	{Szőlőcincér}
Phymatodes glabratus	{Borókaháncscincér}
Phymatodes puncticollis	{"Vörösbarna háncscincér"}
Phymatodes pusillus	{"Vállfoltos háncscincér"}
Phymatodes rufipes	{"Kék háncscincér"}
Leptura livida	{"Barnás virágcincér"}
Ropalopus spinicornis	{"Vöröscombú facincér"}
Strangalia maculata	{"Tarkacsápú karcsúcincér"}
Obrium bicolor	{"Kétszínű hengercincér"}
Obriopsis bicolor	{"Kétszínű hengercincér"}
Strangalia bifasciata	{"Kétöves karcsúcincér"}
Strangalia melanura	{"Feketevégű karcsúcincér"}
Strangalia nigra	{"Fekete karcsúcincér"}
Strangalia septempunctata	{"Hétpettyes karcsúcincér"}
Leptura erythroptera	{"Bordó virágcincér"}
Leptura rubra	{"Vörös virágcincér"}
Leptura scutellata	{"Hegyi virágcincér"}
Strangalina attenuata	{Nyurgacincér}
Stromatium fulvum	{"Sárgás galléroscincér"}
Trichoferus cinereus	{"Szürkés éjcincér"}
Hesperophanes pallidus	{"Sápadt éjcincér"}
Leptura steveni	{"Alföldi virágcincér"}
Leptura unipunctata	{"Kétpettyes virágcincér"}
Rusticoclytus pantherinus	{Párduccincér}
Rusticoclytus rusticus	{"Egérszínű darázscincér"}
Antipus macropus	{Dárdahere-zsákhordóbogár}
Macroplea mutica	{"balatoni hínárbogár"}
Acanthoscelides pallidipennis	{gyalogakáczsizsik}
Anthonomus chevrolati	{Chevrolat-rügylikasztó}
Apsis albolineatus	{rozsbogár}
Araecerus fasciculatus	{kávé-orrosbogár}
Araeocerodes grenieri	{Grenier-orrosbogár}
Bagopsis globicollis	{"szürke ártériormányos"}
Ceutorhynchus chalybaeus	{"fémes rejtettormányú-bogár"}
Cirrorhynchus kelecsenyii	{Kelecsényi-gyalogormányos}
Cleopomiarus distinctus	{"kecses harangvirág-ormányos"}
Datonychus arquatus	{vízipeszérce-ormányos}
Donus maculatus	{"nagy ökörfarkkóró-ormányos"}
Echinodera validus	{"erős zártormányúbogár"}
Ernoporus tiliae	{hársszú}
Eutrichapion punctigerum	{"bükkönyszomorító cickányormányos"}
Holotrichapion aestimatum	{lucerna-cickányormányos}
Leperisinus orni	{"csupasztorú kőrisszú"}
Leperisinus varius	{kőrisszú}
Liparthrum bartschi	{fagyöngyszú}
Liparus transsilvanicus	{"erdélyi acsalapuormányos"}
Mecinus heydenii	{Heyden-útifűormányos}
Mecorhis ungaricus	{"magyar eszelény"}
Mesotrichapion amethystinum	{csüdfűrágó-cickányormányos}
Nanophyes marmoratus	{"simacombú füzényormányos"}
Neliocarus faber	{mester-ormányos}
Neoplinthus tigratus	{komlóormányos}
Notaris acridulus	{sásormányos}
Notaris aterrimus	{rejtettpajzsocskás-ormányos}
Notaris maerkli	{Maerkel-ormányos}
Omphalapion sorbi	{székfű-cickányormányos}
Orobitis cyaneus	{"ibolya gyökérbarkó"}
Orobitis nigrinus	{"fekete gyökérbarkó"}
Pachycerus cordiger	{"tarka répabarkó"}
Phaeochrotes cinctus	{tölgy-orrosbogár}
Philopedon plagiatus	{lomhaormányos}
Platypus cylindricus	{hosszúlábúszú}
Protopirapion kraatzi	{Kraatz-cickányormányos}
Rhaphitropis marchicus	{"szalagos orrosbogár"}
Rhinusa bipustulatum	{"vörösfoltos görvélyfű-ormányos"}
Rhinusa netum	{"drapp gyujtoványfű-ormányos"}
Rhinusa tetrum	{"magtoklakó ormányos"}
Rhynchaenus erythropus	{"mintás bolhaormányos"}
Rhynchaenus hungaricus	{"magyar bolhaormányos"}
Rhynchaenus quedenfeldtii	{Quenfeldt-bolhaormányos}
Rhynchaenus quercus	{tölgy-bolhaormányos}
Teretriorhynchites aethiops	{szerecseneszelény}
Teretriorhynchites caeruleus	{"hajtástörő eszelény"}
Tournotaris bimaculatus	{"fűrészeslábú gyékényormányos"}
Trypodendron domesticus	{"nagy bükkszú"}
Trypodendron lineatus	{"sávos fenyőszú"}
Trypodendron signatus	{"lombfarágó szú"}
Taphrotopium brunnipes	{penészvirág-cickányormányos}
Perapion sedi	{varjúháj-cickányormányos}
Nanophyes quadrivirgata	{"apró tamariska-ormányos"}
Amalus haemorrhous	{"barna keserűfű-ormányos"}
Anthonomus varians	{fenyőbarkarontó-ormányos}
Furcipus rectirostris	{alma-bimbólikasztó}
Brachytarsus fasciatus	{alma-bimbólikasztó}
Brachytarsus nebulosus	{"ködfoltos pajzstetvész"}
Apion miniatum	{"vörös cickányormányos"}
Apion sanguineum	{"sóskarágó cickányormányos"}
Myorhinus albolineatus	{rozsbogár}
Curculio crux	{"keresztes zsuzsóka"}
Curculio pyrrhoceras	{gubacszsuzsóka}
Curculio salicivorus	{fűzfazsuzsóka}
Baris angusta	{"keskeny báris"}
Baris chlorizans	{"kék káposztabáris"}
Baris coerulescens	{repcebáris}
Baris gudenusi	{repcebáris}
Baris kaufmanni	{Kaufmann-báris}
Baris lepidii	{zsázsabáris}
Baris picicornis	{rezedabáris}
Baris villae	{földitök-báris}
Echinocnemus globicollis	{"szürke ártériormányos"}
Trichapion simile	{nyírfa-cickányormányos}
Chromoderus affinis	{"sávos répabarkó"}
Urodon canus	{"déli rezedabogár"}
Urodon conformis	{"tar rezedabogár"}
Urodon musculus	{"egérszürke rezedabogár"}
Urodon orientalis	{"keleti rezedabogár"}
Urodon rufipes	{"őszes rezedabogár"}
Urodon schusteri	{Schuster-rezedabogár}
Urodon suturalis	{"sávos rezedabogár"}
Sirocalus terminatus	{petrezselyem-ormányos}
Ctenochirus leucogrammus	{"kis kendermagbogár"}
Ceratapion alliariae	{bujdosó-cickányormányos}
Acanephodus onopordi	{szamárbogáncs-cickányormányos}
Acanephodus orientale	{"keleti cickányormányos"}
Ceutorhynchus pleurostigma	{repcegyökér-ormányos}
Ceutorhynchus vindobonensis	{"pirosorrú ormányos"}
Ceutorhynchus quadridens	{repceormányos}
Ceutorhynchus contractus	{vadrepce-ormányos}
Ceutorhynchus angustus	{"sápadt rejtettormányú-bogár"}
Ceutorhynchus floralis	{pásztortáska-ormányos}
Chlorophanus gibbosus	{"szegélyes színesormányos"}
Deporaus tristis	{juhar-levélsodró}
Otiorrhynchus arrogans	{"dölyfös gyalogormányos"}
Otiorrhynchus kelecsenyii	{Kelecsényi-gyalogormányos}
Cleonus piger	{bogáncsbarkó}
Miarus distinctus	{"kecses harangvirág-ormányos"}
Miarus graminis	{"szőrös harangvirág-ormányos"}
Miarus longirostris	{"hosszúorrú harangvirágormányos"}
Miarus medius	{"fönséges harangvirág-ormányos"}
Miarus micros	{kékcsillag-ormányos}
Miarus plantarum	{varjúköröm-ormányos}
Coeliodes dryados	{cserormányos}
Coeliodes erythroleucos	{"fehéröves ormányos"}
Stephanocleonus nigrosuturatus	{"karcsú répabarkó"}
Baris scolopacea	{"pikkelyes báris"}
Hadroplomtus magnini	{szurokfű-ormányos}
Nanophyes gracilis	{"karcsú füzényormányos"}
Nanophyes helveticus	{"svájci füzényormányos"}
Nanophyes nitidulus	{"fogascombú füzényormányos"}
Otiorrhynchus inflatus	{"barázdásorrú gyalogormányos"}
Otiorrhynchus mastix	{"lécesorrú gyalogormányos"}
Hypera tessellata	{"nagy ökörfarkkóró-ormányos"}
Hypera zoilus	{lucerna-gubósormányos}
Dorytomus affinis	{"tavaszi hangormányos"}
Dorytomus validirostris	{"szegélyes hangormányos"}
Dorytomus flavipes	{"sárgalábú hangormányos"}
Diodyrhynchus austriacus	{"osztrák áleszelény"}
Acalles validus	{"erős zártormányúbogár"}
Elleschus bipunctatus	{kétpöttyös-fűzormányos}
Elleschus infirmus	{"tarka fűzormányos"}
Elleschus scanicus	{"sápadt nyárfa-ormányos"}
Enedreutes hilaris	{"kétdudoros orrosbogár"}
Enedreutes sepicola	{"hatdudoros orrosbogár"}
Coeliodes zonatus	{"csíkos kecskerágó-ormányos"}
Gasterocerus depressirostris	{"laposorrú ormányosbogár"}
Glocianus marginatus	{"gömbded lóhereormányos"}
Alophus kaufmanni	{Kaufmann-nadálytőormányos}
Alophus triguttatus	{"nagy nadálytőormányos"}
Involvulus pubescens	{"szőrös eszelény"}
Hemitrichapion curtisi	{Walton-cickányormányos}
Hexarthrum culinaris	{bányafa-szúormányos}
Hylesinus oleiperda	{olajfarontószú}
Hylobitelus abietis	{"nagy fenyőormányos"}
Hylobius piceus	{lucfenyőormányos}
Hylobitelus pinastri	{"kis fenyőormányos"}
Hylobitelus transversovittatus	{lomberdő-ormányos}
Phytonomus arator	{"közönséges gubósormányos"}
Phytonomus meles	{lóhere-gubósormányos}
Phytonomus nigrirostris	{lóhere-gubósormányos}
Phytonomus plantaginis	{útifű-gubósormányos}
Phytonomus adspersus	{"mocsári gubósormányos"}
Phytonomus variabilis	{lucernaormányos}
Phytonomus rumicis	{sóska-gubósormányos}
Phytonomus pedestris	{lednek-gubósormányos}
Phytonomus trilineatus	{bíborhere-ormányos}
Phytonomus viciae	{bükköny-gubósormányos}
Rhynchites cupreus	{szilvaeszelény}
Ischnopterapion sicardi	{"csodálatos cickányormányos"}
Rhynchaenus populi	{nyár-bolhaormányos}
Baris atricolor	{"hasznos tisztesfű-báris"}
Leperisinus fraxini	{kőrisszú}
Leucosomus pedestris	{"négypettyes répabarkó"}
Lixus algirus	{"mályvafúró dudvabarkó"}
Lixus sanguineus	{"rövid dudvabarkó"}
Lixus difficilis	{"vaskosorrú dudvabarkó"}
Lixus elongatus	{"karcsú bogáncsfúró-dudvabarkó"}
Lixus flavescens	{laboda-dudvabarkó}
Baris timida	{mályvabáris}
Gymnetron caucasicum	{"kaukázusi útifűormányos"}
Gymnetron labile	{"tarka útifűormányos"}
Gymnetron pascuorum	{"közönséges útifűormányos"}
Gymnetron pirazzolii	{Pirazzoli-ormányos}
Gymnetron plantaginis	{"törpe útifűormányos"}
Rhyncites hungaricus	{"magyar eszelény"}
Baris dalmatina	{dalmát-báris}
Baris laticollis	{"lakkfényű káposztabáris"}
Phloeophagus cylindrus	{"barna szúormányos"}
Nanophyes sahlbergi	{Sahlberg-tócshúrormányos}
Ceutorrhynchus campestris	{"réti margitvirág-ormányos"}
Ceutorrhynchus edentulus	{ebszékfű-ormányos}
Ceutorrhynchus millefolii	{gilisztaűző-varádicsormányos}
Ceutorrhynchus molitor	{pipitérszépe-ormányos}
Ceutorrhynchus rugulosus	{székfű-ormányos}
Ceutorrhynchus triangulum	{"tarka cickafark-ormányos"}
Hadroplontus abbreviatulus	{"óvatos nadálytőormányos"}
Hadroplontus aubei	{Aube-ormányos}
Hadroplontus curvistriatus	{Dieck-ormányos}
Hadroplontus euphorbiae	{nefelejcs-ormányos}
Hadroplontus hungaricus	{"magyar szeplőlapú-ormányos"}
Hadroplontus javetii	{atracélormányos}
Hadroplontus symphyti	{"kis nadálytőormányos"}
Nanophyes annulatus	{"fekete füzényormányos"}
Nanophyes circumscriptus	{"nagy füzényormányos"}
Nanophyes hemisphaericus	{"háromszöges füzényormányos"}
Cidnorrhinus quadrimaculatus	{csalánormányos}
Coenorrhinus aeneovirens	{"bronzos eszelény"}
Coenorrhinus germanicus	{szamócaeszelény}
Coenorrhinus interpunctatus	{levélér-eszelény}
Coenorrhinus pauxillus	{"bordafúró eszelény"}
Glocianus albovittatus	{"fehérsávos ormányos"}
Glocianus maculaalba	{máktokormányos}
Neoplinthus porcatus	{komlóormányos}
Tropideres curtirostris	{"rövidorrú orrosbogár"}
Phyllobius pictus	{"tarka tölgyormányos"}
Mylacus seminulum	{"magalakú ormányos"}
Omphalapion hookeri	{Hooker-cickányormányos}
Acalles croaticus	{"horvát zártormányúbogár"}
Acalles luigionii	{Luigioni-zártormányúbogár}
Acalles pyrenaeus	{"pireneusi zártormányúbogár"}
Paramesus tessellatus	{"tömzsi orrosbogár"}
Ceutorrhynchus conspersus	{"hamvas hagymaormányos"}
Ceutorrhynchus suturalis	{hagymaormányos}
Rhynchaenus fagi	{bükk-bolhaormányos}
Rhynchaenus signifer	{"változatos bolhaormányos"}
Rhynchaenus rusci	{"erdei bolhaormányos"}
Rhynchaenus subfasciatus	{"szalagos bolhaormányos"}
Rhynchaenus testaceus	{nyír-bolhaormányos}
Comasinus setiger	{"láthatatlan-pajzsocskájú ormányos"}
Kalkapion flavofemoratum	{"rekettyeszépe cickányormányos"}
Otiorrhynchus fuscipes	{"rozsdáslábú gyalogormányos"}
Otiorhynchus olivieri	{"rozsdáslábú gyalogormányos"}
Oxystoma pseudocerdo	{"szürkéskék cickányormányos"}
Foucartia squamulata	{"pikkelyes kerepormányos"}
Phytobius quadrituberculatus	{"kis keserűfűormányos"}
Nanophyes maculipes	{varjúháj-ormányos}
Phyllobius viridicollis	{"zömök levélormányos"}
Phyllobius calcaratus	{éger-ormányos}
Phyllobius incanus	{tölgy-levélormányos}
Phyllobius urticae	{csalán-levélormányos}
Phyllobius roboretanus	{"kardlábú levélormányos"}
Phyllobius parvulus	{"kardlábú levélormányos"}
Phyllobius scutellaris	{"zöld pázsitfűormányos"}
Pissodes notatus	{"fehérfoltos fenyőbogár"}
Anthribus albinus	{"nagybajuszú orrosbogár"}
Polydrosus sericeus	{"selymes lombormányos"}
Polydrosus ruficornis	{"göröngyöstorú lombormányos"}
Polydrosus undatus	{"szalagos lombormányos"}
Protapion flavipes	{vadhere-cickányormányos}
Protapion aestivum	{lóherevirág-cickányormányos}
Pirapion striatum	{"dundi cickányormányos"}
Pirapion kraatzi	{Kraatz-cickányormányos}
Psalidium maxillosum	{"fekete barkó"}
Pseudomyllocerus cinerascens	{Magnano-ormányos}
Phyllobius sinuatus	{szeder-ormányos}
Pseudotyphlus pilumnus	{kamilla-sertésormányos}
Austroceutorhynchus italicus	{"olasz ormányos"}
Tropideres marchicus	{"szalagos orrosbogár"}
Gymnetron antirrhini	{"fekete gyujtoványfű-ormányos"}
Gymnetron asellus	{"szőrcsillagos ormányos"}
Gymnetron bipustulatum	{"vörösfoltos görvélyfű-ormányos"}
Gymnetron collinum	{"csinos gyujtoványfű-ormányos"}
Gymnetron herbarum	{"borzos gyujtoványfű-ormányos"}
Gymnetron hispidum	{"hosszúszőrű gyujtoványfű-ormányos"}
Gymnetron linariae	{"gubacsképző ormányos"}
Gymnetron littoreum	{"bundás gyujtoványfű-ormányos"}
Gymnetron melas	{"szürke gyujtoványfű-ormányos"}
Gymnetron netum	{"drapp gyujtoványfű-ormányos"}
Gymnetron smreczynskii	{Smreczynski-gyujtoványfű-ormányos}
Gymnetron tetrum	{"magtoklakó ormányos"}
Gymnetron thapsicola	{"aprófogú gyujtoványfű-ormányos"}
Acalles hypocrita	{avarormányos}
Sitona ononidis	{iglice-csipkézőbogár}
Sitona flavescens	{"sárga csipkézőbogár"}
Sitona crinitus	{borsó-csipkézőbogár}
Sitona tibialis	{bükköny-csipkézőbogár}
Calandra granaria	{gabonazsuzsok}
Calandra oryzae	{rizszsuzsok}
Smicronyx coecus	{"egykarmú arankaormányos"}
Squamapion millum	{"csupaszorrú cickányormányos"}
Stenocarus fuliginosus	{mákgyökér-ormányos}
Tanysphyrus makolskii	{"feketelábú békalencse-ormányos"}
Tapinotus sellatus	{lizinka-ormányos}
Pselaphorhynchites longiceps	{"szélesfejű eszelény"}
Pselaphorhynchites nanus	{törpeeszelény}
Pselaphorhynchites tomentosus	{"simaszőrű eszelény"}
Involvulus caeruleus	{"hajtástörő eszelény"}
Notaris bimaculatus	{"fűrészeslábú gyékényormányos"}
Notaris granulipennis	{gyékényormányos}
Trachyphloeus olivieri	{"apró éjiormányos"}
Ceutorrynchidius troglodytes	{"lándzsás útilapu-ormányos"}
Xyloterus domesticus	{"nagy bükkszú"}
Xyloterus lineatus	{"sávos fenyőszú"}
Xyloterus signatus	{"lombfarágó szú"}
Zacladus affinis	{gólyaorr-ormányos}
Sialis flavilatera	{"Vízi recésfátyolka"}
Chrysopa septempunctata	{"Hétfoltos fátyolka"}
Chrysopa chrysops	{"Aranyszemű fátyolka"}
Chrysoperla kolthoffi	{"Közönséges fátyolka"}
Chrysopa vulgaris	{"Közönséges fátyolka"}
Chrysopa pillichi	{"Közönséges fátyolka"}
Chrysopa microcephala	{"Közönséges fátyolka"}
Chrysopa lamproptera	{"Közönséges fátyolka"}
Micromus aphidovorus	{"V-foltos barnafátyolka"}
Sympherobius striatellus	{"Tarka barnafátyolka"}
Sympherobius laetus	{Törpefátyolka}
Hemerobius betulinus	{"Hegyi törpefátyolka"}
Subboriomyia fusca	{"Pettyes barnafátyolka"}
Semidalis curtisiana	{"Lisztes fátyolka"}
Myrmeleon europeus	{"Pöttyös hangyaleső"}
Myrmeleon innotatus	{"Közönséges hangyaleső"}
Ascalaphus pupillatus	{"Keleti rablópille"}
Ascalaphus hungaricus	{"Keleti rablópille"}
Ascalaphus kolyvanensis	{"Keleti rablópille"}
Subilla sulfuricosta	{"Sárgalábú tevenyakú"}
Raphidia maculicaput	{"Sárgalábú tevenyakú"}
Raphidia dichroma	{"Sárgalábú tevenyakú"}
Raphidia durmitorica	{"Sárgalábú tevenyakú"}
Raphidia monotona	{"Sárgalábú tevenyakú"}
Raphidia affinis	{"Sárgalábú tevenyakú"}
Raphidia notata	{"Barna tevenyakú"}
Raphidia laticeps	{"Barna tevenyakú"}
Lesna navasi	{"Barna tevenyakú"}
Lesna lestica	{"Barna tevenyakú"}
Lesna stigmata	{"Barna tevenyakú"}
Lesna laticaput	{"Barna tevenyakú"}
Navasana perumbrata	{"Barna tevenyakú"}
Protichneumon pisarius	{"nagy szenderfürkész"}
Chrysis iris	{"ragyogó fémdarázs"}
Ammophila apicalis	{Mocsáry-hernyóölő}
Sphex maxillosus	{"Szöcskeölő darázs"}
Bombus silvarum	{"erdei poszméh"}
Aristotelia decoratella	{"díszes sarlósmoly"}
Anania luctualis	{"Fehérfoltos kormosmoly"}
Reskovitsia alborivulalis	{"Keleti kormosmoly"}
Pieris manni	{"Magyar fehérlepke"}
Iolana iolas	{"Magyar boglárka"}
Maculinea xerophila	{"Karszti hangyaboglárka"}
Maculinea rebeli	{"Karszti hangyaboglárka"}
Cycnia luctuosa	{"Gyászos medvelepke"}
Bittacus italicus	{"hosszúlábú csőrösrovar"}
Philipomia graeca	{"balkáni bögöly"}
Atylotus rufipes	{"aranyló bögöly"}
Chrysops maritimus	{"közönséges pőcsik"}
Chrysops perspicillaris	{"balatoni pőcsik"}
Chrysops pictus	{"egyfoltos pőcsik"}
Philipomia ferruginea	{"balkáni bögöly"}
Tabanus anthophilus	{lóbögöly}
Tabanus cognatus	{"lilaszemű bögöly"}
Abramis bjoerkna	{karikakeszeg}
Chalcalburnus chalcoides	{"kaszpi küsz"}
Gobio albipinnatus	{"halványfoltú küllő"}
Gobio kessleri	{"homoki küllő"}
Gobio uranoscopus	{"felpillantó küllő"}
Leuciscus cephalus	{domolykó}
Leuciscus souffia	{"vaskos csabak"}
Rhodeus sericeus	{"szivárványos ökle"}
Ictalurus melas	{"fekete törpeharcsa"}
Ictalurus nebulosus	{törpeharcsa}
Noemacheilus barbatulus	{kövicsík}
Orthrias barbatulus	{kövicsík}
Barbus petenyi	{Petényi-márna}
Romanogobio albipinatus	{"halványfoltú küllő"}
Acerina cernua	{durbincs}
Acerina schraetzer	{"selymes durbincs"}
Gymnocephalus schraetzer	{"selymes durbincs"}
Aristichthys nobilis	{"pettyes busa"}
Gobius fluviatilis	{"folyami géb"}
Gobius kessleri	{Kessler-géb}
Gobius syrman	{Szirman-géb}
Salmo gairdneri	{"szivárványos pisztráng"}
Cobitis aurata	{törpecsík}
Sabanejewia balcanica	{törpecsík}
Sabanejewia bulgarica	{törpecsík}
Stizostedion lucioperca	{süllő}
Stizostedion volgense	{kősüllő}
Stizostedion volgensis	{kősüllő}
Aspro streber	{"német bucó"}
Aspro zingel	{"magyar bucó"}
Graptemys pseudogeographica	{"közönséges tarajosteknős"}
Lacerta muralis	{"Fali gyík"}
Lacerta taurica	{"Homoki gyík"}
Elanus caeruleus	{kuhi}
Clamator glandarius	{"pettyes kakukk"}
Monticola solitarius	{"kék kövirigó"}
Tarsiger cyanurus	{kékfarkú}
Cisticola juncidis	{szuharbújó}
Emberiza rustica	{"erdei sármány"}
Sorex araneus	{"erdei cickány"}
Miniopterus schreibersi	{"hosszúszárnyú denevér"}
Myotis oxygnathus	{"hegyesorrú denevér"}
Myotis blythii	{"hegyesorrú denevér"}
Lepus capensis	{"mezei nyúl"}
Arvicola terrestris	{kószapocok}
Apodemus microps	{"kislábú erdeiegér"}
Microtus ratticeps	{"északi pocok"}
Pitymys subterraneus	{"földi pocok"}
Spalax leucodon	{"nyugati földikutya"}
Myoxus glis	{"nagy pele"}
Mustela eversmanni	{molnárgörény}
Mustela nivalis	{menyét}
Putorius eversmanni	{molnárgörény}
Putorius putorius	{"közönséges görény"}
Felis lynx	{"közönséges hiúz"}
Cervus dama	{dámszarvas}
Ovis orientalis	{muflon}
Lythrum ×scabrum	{"Hibrid füzény"}
Rotala macranda	{Hínárfüzény}
Epilobium ciliatum	{"Jövevény füzike"}
Oenothera villosa	{"Magyar ligetszépe"}
Oenothera oakesiana	{"Afrikai ligetszépe"}
Circaea ×intermedia	{"Havasalji varázslófű"}
Lopezia racemosa	{"Változékony hajnalpír"}
Clarkia elegans	{Klárkia}
Godetia amoena	{Tündérkürt}
Fumana ericoides	{"Délszaki naprózsa"}
Viola cyanea	{"Kék ibolya"}
Viola reichenbachiana	{"Erdei ibolya"}
Viola palustris	{"Posvány ibolya"}
Viola jooi	{Joó-ibolya}
Viola ×wittrockiana	{"Kerti árvácska"}
Lagenaria siceraria	{Lopótök}
Cucurbita maxima	{Sütőtök}
Cucurbita ficifolia	{Laskatök}
Citrullus lanatus	{Görögdinnye}
Citrullus colocynthis	{Sártök}
Cucumis melo	{Sárgadinnye}
Cucumis sativus	{Uborka}
Hypericum calycinum	{"Örökzöld orbáncfű"}
Toxicodendron radicans	{"Mérges szömörce"}
Ruta graveolens	{"Kerti ruta"}
Poncirus trifoliata	{Vadcitrom}
Citrus limon	{Citrom}
Monotropa hypopithys	{"Szőrös fenyőspárga"}
Monotropa hypophegea	{"Kopasz fenyőspárga"}
Acer pseudoplatanus	{"Hegyi juhar"}
Ageratum houstonianum	{"Kék bojtocska"}
Grindelia squarrosa	{Gyantásgyom}
Solidago shortii	{"Királyi aranyvessző"}
Aster ×versicolor	{"Tarka őszirózsa"}
Aster parviflorus	{"Kisvirágú őszirózsa"}
Aster ericoides	{Hangaőszirózsa}
Aster dumosus	{"Törpe őszirózsa"}
Callistephus chinensis	{"Kerti őszirózsa"}
Stenactis philadelphica	{"Lilás seprence"}
Bombycilaena erecta	{Topagyom}
Filago lutescens	{"Német penészvirág"}
Logfia arvensis	{"Gyapjas penészvirág"}
Logfia minima	{"Hegyi penészvirág"}
Omalotheca sylvatica	{"Erdei gyopár"}
Filaginella uliginosa	{Iszapgyopár}
Pseudognaphalium luteoalbum	{"Halvány gyopár"}
Helichrysum bracteatum	{Szalmavirág}
Ambrosia coronopifolia	{"Évelő parlagfű"}
Zinnia elegans	{Rézvirág}
Helenium autumnale	{Napfény}
Bidens tripartitus	{"Subás farkasfog"}
Bidens cernuus	{"Bókoló farkasfog"}
Bidens frondosus	{"Feketés farkasfog"}
Galinsoga quadriradiata	{"Borzas gombvirág"}
Tagetes erecta	{"Nagy büdöske"}
Cosmos bipinnatus	{"Kerti pillangóvirág"}
Cosmos sulphureus	{"Sárga pillangóvirág"}
Anthemis ruthenica	{"Homoki pipitér"}
Anthemis triumfettii	{"Déli pipitér"}
Achillea stricta	{"Kárpáti cickafark"}
Tripleurospermum inodorum	{Ebszékfű}
Tripleurospermum tenuifolium	{"Bánsági székfű"}
Leucanthemum vulgare	{"Réti margitvirág"}
Leucanthemum ircutianum	{Szőrös}
Leucanthemum maximum	{"Óriás margitvirág"}
Tanacetum corymbosum	{"Sátoros margitvirág"}
Tanacetum parthenium	{"Őszi margitvirág"}
Chrysanthemum coronarium	{"Koronás aranyvirág"}
Chrysanthemum balsamita	{"Boldogasszony tenyere"}
Artemisia dracunculus	{Tárkony}
Erechtites hieracifolia	{Keresztlapu}
Doronicum ×sopianae	{"Mecseki zergevirág"}
Tephroseris integrifolia	{"Mezei aggófű"}
Tephroseris palustris	{"Északi aggófű"}
Senecio sarracenicus	{"Patakparti aggófű"}
Senecio ovatus	{"Lilásszárú aggófű"}
Calendula arvensis	{"Mezei körömvirág"}
Arctium pubens	{"Erdei bojtorján"}
Silybum marianum	{Máriatövis}
Carduus nutans	{"Bókoló bogáncs"}
Cnicus benedictus	{"Áldott bárcs"}
Cynara cardunculus	{"Bogáncsos articsóka"}
Centaurea salonitana	{"Dalmát imola"}
Centaurea orientalis	{"Keleti imola"}
Centaurea sadlerana	{Sadler-imola}
Centaurea stoebe	{"Nyugati imola"}
Centaurea rocheliana	{"Bánsági imola"}
Carthamus tinctorius	{"Kerti pórsáfrány"}
Shinnersia rivuralis	{"Vízi tölgylevél"}
Cichorium endivia	{Salátakatáng}
Hypochaeris maculata	{"Foltos véreslapu"}
Hypochaeris radicata	{"Kacúros véreslapu"}
Picris echioides	{Vándorvirág}
Scorzonera cana	{"Közönséges szikipozdor"}
Scorzonera laciniata	{"Sallangos szikipozdor"}
Taraxacum obliquilobum	{"Ferde pitypang"}
Lactuca sativa	{"Kerti saláta"}
Lactuca virosa	{"Mérges saláta"}
Hieracium ×schultesii	{"Mirigyes hölgymál"}
Hieracium bifurcum	{"Villás hölgymál"}
Hieracium laschii	{"Szürke hölgymál"}
Hieracium brachiatum	{"Kétágú hölgymál"}
Hieracium flagellare	{"Ostoros hölgymál"}
Hieracium floribundum	{"Virágosindáju hölgymál"}
Hieracium ×leptophyton	{"Laza hölgymál"}
Hieracium ×densiflorum	{"Tömöttfészkű hölgymál"}
Hieracium ×fallax	{"Csalóka hölgymál"}
Tolpis staticifolia	{"Keskenylevelű hölgymál"}
Hieracium virgicaule	{"Vesszős hölgymál"}
Hieracium saxifragum	{"Kőtörő hölgymál"}
Hieracium glaucinum	{"Korai hölgymál"}
Hieracium murorum	{"Erdei hölgymál"}
Hieracium praecurrens	{"Bozontos hölgymál"}
Hieracium diaphanoides	{"Áttetsző hölgymál"}
Hieracium lachenali	{"Közönséges hölgymál"}
Hieracium degenianum	{Dégen-hölgymál}
Hieracium laevicaule	{"Simaszárú hölgymál"}
Hieracium ramosum	{"Ágas hölgymál"}
Senecio germanicus	{"Berki aggófű"}
Senecio hercynicus	{"Berki aggófű"}
Cyclachaena xanthiifolia	{Íva}
Cichorium pumilum	{"Törpe katáng"}
Aesculus parviflora	{"Cserjés vadgesztenye"}
Aesculus ×carnea	{"Húspiros vadgesztenye"}
Xanthoceras sorbifolia	{Sárgaszarvfa}
Euonymus europaeus	{"Csíkos kecskerágó"}
Euonymus japonicus	{"Japán kecskerágó"}
Euonymus radicans	{"Kúszó kecskerágó"}
Thesium ramosum	{"Homoki zsellérke"}
Rhamnus cathartica	{"Varjútövis benge"}
Rhamnus ×gayeri	{Gáyer-benge}
Paliurus spina-christi	{Krisztustövis}
Lychnis viscaria	{Szurokszegfű}
Silene noctiflora	{"Esti mécsvirág"}
Silene viscosa	{"Ragadós mécsvirág"}
Silene pendula	{"Bókoló habszegfű"}
Silene coelirosa	{"Lenvirágú habszegfű"}
Gypsophila altissima	{"Óriás fátyolvirág"}
Gypsophila acutifolia	{"Kaukázusi fátyolvirág"}
Stellaria neglecta	{"Terpedt tyúkhúr"}
Stellaria pallida	{"Sápadt tyúkhúr"}
Stellaria alsine	{Posványcsillaghúr}
Cerastium tenoreanum	{"Mirigytelen kisszirmú madárhúr"}
Cerastium lucorum	{"Nagymagvú madárhúr"}
Cerastium holosteoides	{"Kismagvú madárhúr"}
Spergularia salina	{"Sziki budavirág"}
Polycarpon tetraphyllum	{"Négylevelű csészepörc"}
Cornus stolonifera	{"Tarackos som"}
Mirabilis jalapa	{"Nagy csodatölcsér"}
Chenopodium schraderianum	{"Nehézszagú libatop"}
Chenopodium chenopodioides	{"Sziki libatop"}
Chenopodium capitatum	{Eperparéj}
Chenopodium multifidum	{"Fésűslevelű libatop"}
Chenopodium hircinum	{"Heringszagú libatop"}
Atriplex sagittata	{"Fényes laboda"}
Spinacia oleracea	{Spenót}
Beta vulgaris	{Répa}
Bassia scoparia	{"Vesszős seprűfű"}
Bassia prostrata	{"Heverő seprűfű"}
Bassia laniflora	{"Homoki seprőfű"}
Suaeda prostrata	{"Heverő sóballa"}
Suaeda salinaria	{"Erdélyi sóballa"}
Tetragonia tetragonoides	{"Újzélandi paraj"}
Amaranthus hypochondriacus	{"Terpedt disznóparéj"}
Amaranthus graecizans	{"Cigány disznóparéj"}
Amaranthus viridis	{"Vékony disznóparéj"}
Amaranthus spinosus	{"Tövises disznóparéj"}
Amaranthus caudatus	{"Bókoló amaránt"}
Hedera colchica	{"Kaukázusi borostyán"}
Hedera hibernica	{"Ír borostyán"}
Myrrhoides nodosa	{Dudatönk}
Anthriscus cerefolium	{"Zamatos turbolya"}
Turgenia latifolia	{"Nagy ördögbocskor"}
Trinia kitaibelii	{"Nagy nyúlkapor"}
Petroselinum crispum	{Petrezselyem}
Ammi majus	{Ammi}
Bunium montanum	{Gumóskömény}
Pimpinella anisum	{Ánizs}
Seseli pallasii	{"Változó gurgolya"}
Seseli libanotis	{Tömjénillat}
Anethum graveolens	{Kapor}
Ferula sadlerana	{"Magyarföldi husáng"}
Ferulago sylvatica	{"Erdei mézgabürök"}
Peucedanum carvifolium	{"Köménylevelű kocsord"}
Peucedanum rochelianum	{"Rochel- kocsord"}
Heracleum ×chloranthum	{"Zöldes közönséges medvetalp"}
Crucianella latifolia	{"Széleslevelű szálkanyak"}
Asperula rumelica	{"Török müge"}
Galium humifusum	{"Heverő galaj"}
Ceratostigma plumbaginoides	{"Tarackoló kékgyökér"}
Symphoricarpos albus	{"Közönséges hóbogyó"}
Lonicera tatarica	{"Tatár lonc"}
Lonicera periclymenum	{"Búbos lonc"}
Weigela florida	{Rózsalonc}
Persicaria amphibia	{Vidrakeserűfű}
Persicaria lapathifolia	{"Lapulevelű keserűfű"}
Persicaria maculosa	{"Baracklevelű keserűfű"}
Persicaria hydropiper	{"Borsos keserűfű"}
Persicaria mitis	{"Szelíd keserűfű"}
Persicaria minor	{"Keskenylevelű keserűfű"}
Polygonum bellardii	{Kitaibel-keserűfű}
Polygonum rurivagum	{"Nyugati keserűfű"}
Polygonum orientale	{Pulykavirág}
Fallopia aubertii	{"Kínai iszalag"}
Fallopia ×bohemica	{"Hibrid japánkeserűfű"}
Fagopyrum tataricum	{"Tatár pohánka"}
Centranthus ruber	{"Piros sarkantyúvirág"}
Dipsacus fullonum	{"Erdei mácsonya"}
Dipsacus sativus	{Takácsmácsonya}
Dipsacus ×pseudosilvestris	{"Hibrid mácsonya"}
Virga pilosa	{"Erdei fejvirág"}
Knautia ×speciosa	{"Hibrid varfű"}
Knautia macedonica	{"Macedón varfű"}
Scabiosa triandra	{"Mezei ördögszem"}
Scabiosa caucasica	{"Kaukázusi ördögszem"}
Tilia ×vulgaris	{"Közönséges hárs"}
Tilia ×juranyiana	{Jurányi-hárs}
Broussonetia papirifera	{"Kínai papíreperfa"}
Lavatera trimestris	{"Kerti madármályva"}
Malva verticillata	{"Takarmány mályva"}
Hibiscus syriacus	{Májvacserje}
Malope trifida	{Tölcsérmályva}
Kitaibela vitiifolia	{Kitaibel-mályva}
Sphaeralcea miniata	{Gömbmályva}
Parietaria judaica	{"Ágas falgyom"}
Impatiens walleriana	{"Törpe nebáncsvirág"}
Impatiens balsamina	{Fájvirág}
Quercus palustris	{Mocsártölgy}
Geranium macrorrhizum	{"Illatos gólyaorr"}
Erodium hoefftianum	{"Homoki gémorr"}
Erodium moschatum	{"Pézsma gémorr"}
Peganum harmala	{Törökpirosító}
Zygophyllum fabago	{"Cserjés járomfű"}
Tropaeolum majus	{"Nagy sarkantyúka"}
Chamaesyce humifusa	{"Kúszó kutyatej"}
Chamaesyce nutans	{"Bókoló kutyatej"}
Chamaesyce maculata	{"Foltos kutyatej"}
Euphorbia seguieriana	{"Pusztai kutyatej"}
Euphorbia nicaeensis	{"Magyar kutyatej"}
Euphorbia marginata	{"Tarka kutyatej"}
Populus ×canescens	{"Szürke nyár"}
Populus ×euramericana	{"Nemes nyár"}
Populus ×interamericana	{"Interamerikai nyár"}
Salix elaegnos	{"Parti fűz"}
Salix matsudana	{"Mandzsu fűz"}
Salix babylonica	{"Kínai szomorúfűz"}
Salix ×rubens	{"Berki fűz"}
Buxus sempervirens	{"Örökzöld puszpáng"}
Fraxinus americana	{"Fehér kőris"}
Jasminum fruticans	{"Cserjés jázmin"}
Jasminum nudiflorum	{"Téli jázmin"}
Ligustrum ovalifolium	{"Széleslevelű fagyal"}
Periploca graeca	{"Görög fatekercs"}
Amsonia tabernaemontana	{"Széleslevelű csillagmeténg"}
Cynoglossum creticum	{"Csíkoltszírmú ebnyelvfű"}
Cynoglossum columnae	{"Bársonyos ebnyelvfű"}
Symphytum tuberosum	{"Gumós nadálytő"}
Symphytum asperum	{"Érdeslevelű nadálytő"}
Brunerra macrophylla	{Kaukázusi-nefelejcs}
Anchusa arvensis	{Farkasszem}
Myosotis scorpioides	{"Mocsári nefelejcs"}
Buglossoides purpurocaerulea	{"Erdei gyöngyköles"}
Echium plantagineum	{"Útifűlevelű kígyószisz"}
Symphytum tanaicense	{"Posvány fekete nadálytő"}
Verbena bonariensis	{"Óriás vasfű"}
Verbena rigida	{"Lila vasfű"}
Verbena ×hybrida	{"Kerti vasfű"}
Vitex agnus-castus	{Barátcserje}
Lantana camara	{Sétányrózsa}
Lavandula latifolia	{"Széleslevelű levendula"}
Rosmarinus officinalis	{Rozmaring}
Marrubium ×paniculatum	{"Korcs pemetefű"}
Nepeta nuda	{"Bugás macskamenta"}
Nepeta mussinii	{"Kaukázusi macskamenta"}
Dracocephalum moldavica	{"Moldvai sárkányfű"}
Physostegia virginiana	{Füzérajak}
Prunella ×intermedia	{"Hibrid gyíkfű"}
Galeopsis angustifolia	{"Keskenylevelű piros kenderkefű"}
Galeobdolon montanum	{"Erdei sárgaárvacsalán"}
Salvia aethiopis	{"Magyar zsálya"}
Salvia verbenaca	{"Kisvirágú zsálya"}
Salvia ×sylvestris	{"Erdei zsálya"}
Perovskia atriplicifolia	{"Kék sudárzsálya"}
Monarda didyma	{"Vörös méhbalzsam"}
Monarda fistulosa	{"Csöves méhbalzsam"}
Calamintha menthifolia	{"Erdei pereszlény"}
Calamintha nepeta	{"Mirigyes pereszlény"}
Majorana hortensis	{Majoránna}
Mentha ×niliaca	{"Ligeti menta"}
Mentha ×dumetorum	{"Berki menta"}
Mentha ×dalmatica	{"Dalmát menta"}
Mentha ×carinthiaca	{"Karintiai menta"}
Mentha ×gentilis	{"Nemes menta"}
Mentha ×verticillata	{"Örvös menta"}
Mentha spicata	{"Zöld menta"}
Mentha ×piperita	{"Borsos menta"}
Ocimum basalicum	{Bazsalikom}
Callitriche hamulata	{"Horgas mocsárhúr"}
Callitriche stagnalis	{"Posványlakó mocsárhúr"}
Nicandra physaloides	{Szilkesark}
Atropa belladonna	{Nadragulya}
Physalis pubescens	{"Szőrös földicseresznye"}
Physalis peruviana	{"Ehető zsidócseresznye"}
Capsicum annuum	{Paprika}
Solanum melongeana	{Tojásgyümölcs}
Solanum tuberosum	{Burgonya}
Solanum laciniatum	{"Orvosi csucsor"}
Datura innoxia	{"Indián maszlag"}
Nicotiana alata	{Díszdohány}
Petunia violacea	{"Ibolyás petúnia"}
Cuscuta scandens	{"Nádfojtó aranka"}
Cuscuta suaveolens	{"Fürtös aranka"}
Gilia tricolor	{"Tarka gomolyvirág"}
Gilia capitata	{"Fejecskés gomolyvirág"}
Collomia grandiflora	{"Nagyvirágú gallérvirág"}
Phlox drummondii	{"Egynyári lángvirág"}
Phlox paniculata	{"Bugás lángvirág"}
Polemonium coeruleum	{"Kék csatavirág"}
Verbascum ×rubiginosum	{"Ragyás ökörfarkkóró"}
Cymbalaria pallida	{"Halványkék pintyő"}
Cymbalaria hepaticifolia	{"Korzikai pintyő"}
Kickxia ×confinis	{"Hibrid tátika"}
Linaria ×kocianowichii	{"Hibrid gyújtoványfű"}
Linaria purpurea	{"Pirosló gyújtoványfű"}
Asarina procumbens	{"Kereklevelű oroszlánszáj"}
Microrrhinum minus	{Tátos}
Mimulus guttatus	{"Sárga bohócvirág"}
Veronica scardiaca	{"Kislevelű veronika"}
Pseudolysimachion spicatum	{"Macskafarkú veronika"}
Pseudolysimachion orchideum	{"Kosborképű veronika"}
Veronica triloba	{"Háromhasábú veronika"}
Veronica sublobata	{"Hosszúkocsányú veronika"}
Veronica cymbalaria	{Pintyőveronika}
Digitalis lutea	{"Vajsárga gyűszűvirág"}
Euphrasia pectinata	{"Tatár szemvidító"}
Odontites luteus	{"Sárga fogfű"}
Odontites rubra	{"Vörös fogfű"}
Orobanche artemisii-campestris	{Üröm-vajvirág}
Orobanche pančići	{"Vérveres vajvirág"}
Utricularia gibba	{"Törpe rence"}
Plantago holosteum	{"Árlevelű útifű"}
Papaver setigerum	{"Borzas mák"}
Macleya cordata	{"Magas mákkóró"}
Eschscholzia californica	{Kakukkmák}
Corydalis ×campylochila	{"Törpe keltike"}
Corydalis fabacea	{"Bókoló keltike"}
Corydalis ochroleuca	{"Vajsárga keltike"}
Brassica rapa	{"Francia mustár"}
Brassica napus	{Réparepce}
Hirschfeldia incana	{"Szürke nyurgaszál"}
Lepidium sativum	{"Kerti zsázsa"}
Lepidium crassifolium	{"Pozsgás zsázsa"}
Iberis umbellata	{"Ernyős tatárvirág"}
Neslia paniculata	{Sömörje}
Peltaria alliacea	{Pajzstok}
Lobularia maritima	{"Tengerpari fülesternye"}
Armoracia rusticana	{"Közönséges torma"}
Cardamine bulbifera	{"Hagymás fogas-ír"}
Cardamine enneaphyllos	{"Bókoló fogas-ír"}
Arabis caucasica	{"Kaukázusi ikravirág"}
Arabis procurrens	{"Indás ikravirág"}
Arabis glabra	{Toronyszál}
Rorippa ×anceps	{"Heverő kányafű"}
Rorippa ×astyla	{"Lantos kányafű"}
Rorippa ×armoracioides	{"Tormaképű kányafű"}
Rorippa ×hungarica	{"Magyar kányafű"}
Matthiola longipetala	{"Görög viola"}
Sisymbrium austriacum	{"Osztrák zsombor"}
Brassica campestris	{"Vad réparepce"}
Thlaspi schudichii	{Schudich-tarsóka}
Reseda odorata	{"Kerti rezeda"}
Reseda alba	{"Fehér rezeda"}
Maianthemum bifolium	{Árnyékvirág}
Yucca recurvifolia	{"Ágas jukka pálmaliliom"}
Galanthus imperati	{"Termetes hóvirág"}
Narcissus poeticus	{"Csillagos nárcisz"}
Narcissus ×incomparabilis	{"Pompás nárcisz"}
Narcissus ×biflorus	{"Kétvirágú nárcisz"}
Bromus hordeaceus	{"Puha rozsnok"}
Festuca filiformis	{"Fonalas csenkesz"}
Festuca stricta	{"Merev csenkesz"}
Glyceria arundinacea	{"Nádképű harmatkása"}
Glyceria notata	{"Fodros harmatkása"}
Poa humilis	{"Északi perje"}
Briza maxima	{"Nagy rezgőpázsit"}
Elymus caninus	{"Szálkás tarackbúza"}
Elymus repens	{"Közönséges tarackbúza"}
Elymus hispidus	{"Deres tarackbúza"}
Dasypyrum villosum	{Haynald-fű}
Aegilotriticum sancti-andreae	{"Hibrid kecskebúza"}
Triticum aestivum	{"Közönséges búza"}
Secale cereale	{"Közönséges rozs"}
Hordeum geniculatum	{"Sziki árpa"}
Hordeum secalinum	{"Évelő árpa"}
Hordeum jubatum	{Díszárpa}
Hordeum bulbosum	{"Gumós árpa"}
Hordeum distichon	{"Kétsoros árpa"}
Hordeum vulgare	{"Négysoros árpa"}
Leymus arenarius	{"Homoki hajperje"}
Taeniatherum caput-medusae	{Medúzafű}
Deschampsia cespitosa	{"Gyepes sédbúza"}
Avena sterilis	{"Magas zab"}
Avenula pubescens	{"Pelyhes zabfű"}
Avenula pratensis	{"Réti zabfű"}
Avenula praeusta	{"Lapos zabfű"}
Avenula adsurgens	{"Felálló zabfű"}
Avenula planiculmis	{"Laposhüvelyű zabfű"}
Danthonia decumbens	{Háromfogfű}
Koeleria mollis	{"Magas fényperje"}
Agrostis gigantea	{"Óriás tippan"}
Polypogon monspeliensis	{"Közönséges kefefű"}
Phleum subulatum	{"Egyéves komócsin"}
Hierochloë repens	{"Illatos szentperje"}
Hierochloë australis	{"Déli szentperje"}
Phalaris aquatica	{"Gumós kanáriköles"}
Phalaris arundinacea	{Pántlikafű}
Eragrostis cilianensis	{"Nagy tőtippan"}
Crypsis alopecuroides	{"Karcsú bajuszfű"}
Crypsis schoenoides	{"Vastag bajuszfű"}
Setaria gussonei	{"Csalékony muhar"}
Sorghum dochna	{Seprűcirok}
Coix lacryma-jobi	{"Jób könnye"}
Zea mays	{Kukorica}
Phyllostachys viridiglaucescens	{Botnád}
Eriochloa villosa	{"Ázsiai gyapjúfű"}
Lophochloa cristata	{"Közönséges tarajosperje"}
Phoenix dactylifera	{Datolyapálma}
Arum orientale	{"Keleti kontyvirág"}
Anthyllis vulneraria subsp. vulneraria	{"Réti nyúlszapuka"}
Centaurea nigrescens subsp. nigrescens	{"Tévesen szerepel"}
Cuscuta epithymum subsp. epithymum	{"Herefojtó aranka"}
Erigeron acris subsp. acris	{"Bóbitás küllőrojt"}
Erigeron annuus subsp. annuus	{"Egynyári seprence"}
Euphorbia seguieriana subsp. seguieriana	{"Alacsony pusztai kutyatej"}
Heliopsis helianthoides subsp. helianthoides	{Napszem}
Leonurus cardiaca subsp. cardiaca	{"Szúrós gyöngyajak"}
Paeonia officinalis subsp. officinalis	{"Kerti bazsarózsa"}
Plantago major subsp. major	{"Nagy útifű"}
Sagina apetala subsp. apetala	{"Pillás zöldhúr"}
Trifolium arvense subsp. arvense	{Tarlóhere}
Saxicola rubicola	{cigánycsuk,"európai cigánycsuk","cigány csaláncsúcs"}
Anguis colchica	{"kékpettyes lábatlangyík",lábatlangyík}
Arion lusitanicus	{"spanyol csupaszcsiga","spanyol meztelencsiga"}
Barbus meridionalis	{"mediterrán márna","Petényi-márna (magyar márna)"}
Carabus cancellatus adeptus	{"kis ragyás futrinka","ragyás futrinka"}
Carabus convexus kiskunensis	{"kiskunsági selymes futrinka","selymes futrinka"}
Carabus violaceus rakosiensis	{"keleti kékfutrinka","rákosi keleti kékfutrinka"}
Cicindela soluta pannonica	{"alföldi homokfutrinka","pannon alföldi homokfutrinka"}
Cobitis elongatoides	{"vágó csík",vágócsík}
Cordulegaster heros	{"kétcsíkos hegyiszitakötő","kétcsíkos hegyiszitakötő (ritka hegyiszitakötő)"}
Cricetus cricetus	{hörcsög,"közönséges hörcsög"}
Dama dama	{dámszarvas,dámvad}
Eresus kollari	{bikapók,"skarlát bikapók"}
Eresus moravicus	{bikapók,"sárgafejű bikapók"}
Erinaceus concolor	{"keleti sün","kis-ázsiai sün"}
Erinaceus europaeus	{"európai sün","nyugati sün"}
Fagotia daudebartii acicularis	{"dunai csúcsos szurokcsiga (folyamcsiga alfaj)",folyamcsiga}
Leptoglossus occidentalis	{"keleti levéllábú-poloska","nyugati levéllábú poloska"}
Mustela putorius	{"házi görény","közönséges görény"}
Orconectes limosus	{"cifra rák",jelzőrák}
Pseudorasbora parva	{"kínai razbóra",razbóra}
Pyrois cinnamomea	{"ritka fahéj bagoly","ritka fahéjbagoly"}
Stictocephala bisonia	{"amerikai bivalykabóca",bivalykabóca}
Trachemys scripta elegans	{"pirosfülű ékszerteknős","vörösfülű ékszerteknős"}
Xiphophorus helleri	{"mexikói kardfarkúhal","mexikói kardfarkú hal (szifó)"}
Amblystegium saxatile	{"sziklai karcsútokúmoha","sziklai karcsútorkúmoha"}
Asterella saccata	{"szákos zacskósmoha","zsákos zacskósmoha"}
Aster laevis	{"simalevelű őszirózsa","sima őszirózsa"}
Aster sedifolius	{"pettyegetett őszirózsa","réti őszirózsa (pettyegetett őszirózsa)"}
Centaurea sadleriana	{"budai imola",Sadler-imola}
Cephalozia lacinulata	{"lapos májmoha","lapos májmohácska"}
Chamaecytisus albus	{"fehér törpezanót","fehér zanót"}
Chamaecytisus heuffelii	{Heuffel-törpezanót,Heuffel-zanót}
Comarum palustre	{tőzegeper,"tőzegeper (tőzegpimpó)"}
Corallorhiza trifida	{"erdei korallgyökér",korallgyökér}
Epipactis latina	{"lazioi nőszőfű","római nőszőfű"}
Erechtites hieraciifolia	{"amerikai keresztlapu",keresztlapu}
Fissidens algarvicus	{"algarvei hasadtfogúmoha","Algarvei hasadtfogúmoha"}
Grimmia plagiopodia	{"hasastokú őszmoha","hasastokú párnácskamoha"}
Grimmia teretinervis	{"vastagerű őszmoha","vastagerű párnácskamoha"}
Heracleum sosnowskyi	{sosnowsky-medvetalp,Sosnowsky-medvetalp}
Iva xanthiifolia	{"parlagi rézgyom",rézgyom}
Jovibarba hirta	{"sárga kövirózsa",sárga-kövirózsa}
Koeleria pyramidata	{"magas fényperje","nyugati fényperje (magas fényperje)"}
Leucobryum glaucum	{"fehérlő vánkosmoha","fénylő vánkosmoha"}
Orobanche bartlingii	{Bartling-vajvirág,"Bartling-vajvirág (Bartling-szádor)"}
Orobanche flava	{"martilapu vajvirág","martilapu-vajvirág (martilapu-szádor)"}
Orobanche nana	{"apró vajvirág","apró vajvirág (apró szádor)"}
Orobanche pancicii	{"varfű vajvirág","varfű-vajvirág (vajfűszádor)"}
Phyllitis scolopendrium	{gímpáfrány,"gímpáfrány (gímnyelvű fodorka)"}
Reynoutria japonica	{japánkeserűfű,"japán óriáskeserűfű"}
Riccia frostii	{Frost-májmoha,Frost-úszómájmoha}
Riccia huebeneriana	{Hübener-májmoha,Hübener-úszómájmoha}
Rosa sancti-andreae	{"gyapjas rózsa (incl. szentendrei rózsa)","szentendrei rózsa"}
Scilla autumnalis	{"őszi csillagvirág","őszi csillagvirágok"}
Senecio ovirensis	{"havasalji aggófű","hosszúlevelű aggóvirág"}
Silaum peucedanoides	{"zöldes gurgolya (zöldes kígyókapor)","zöldes kigyókapor"}
Sphagnum magellanicum	{magellán-tőzegmoha,Magellán-tőzegmoha}
Stipa joannis	{"hegyi árvalányhaj","pusztai árvalányhaj"}
Telekia speciosa	{Teleki-virág,"Teleki-virág (pompás Teleki-virág)"}
Thlaspi jankae	{Janka-tarsóka,"Janka-tarsóka (incl. magyar tarsóka)"}
Ulmus pumila	{"szibériai (turkesztáni) szil","turkesztáni szil"}
Vaccinium microcarpum	{"kistermésű áfonya",tőzegáfonya}
Ablepharus kitaibelii fitzingeri	{pannongyík,Pannongyík,"pannon gyík (magyar gyík)"}
Asplenium scolopendrium	{Gímpáfrány,"gímpáfrány (gímnyelvű fodorka)"}
Anguis fragilis	{"lábatlan gyík",lábatlangyík,Lábatlangyík}
Anisus vorticulus	{"apró fillércsiga","Apró fillércsiga","apró fillércsiga (kis lemezcsiga)"}
Asplenium ceterach	{"nyugati pikkelypáfrány","Nyugati pikkelypáfrány"}
Archiearis puella	{"kis nappaliaraszoló","kis nappaliaraszoló (kis tavasziaraszoló)","Kis tavasziaraszoló"}
Notholaena marantae	{Cselling,"cselling (déli cselling)"}
Barbastella barbastellus	{"nyugati piszedenevér","nyugati piszedenevér (piszedenevér)","pisze denevér"}
Curruca communis	{"mezei poszáta"}
Carinatodorcadion fulvum	{"barna gyalogcincér","Barna gyalogcincér","pusztai gyalogcincér"}
Dryopteris affinis	{"pelyvás pajzsika","Pelyvás pajzsika"}
Colias myrmidone	{Narancslepke,"narancsszínű kéneslepke","narancsszínű kéneslepke (narancslepke)"}
Azolla mexicana	{"mexikói moszatpáfrány","Mexikói moszatpáfrány"}
Curruca curruca	{"kis poszáta"}
Diachrysia zosimi	{"Nemes aranybagoly",vérfű-aranybagoly,"vérfű-aranybagoly (nemes aranybagoly)"}
Eremodrina gilva	{"Ezüstös apróbagoly",platina-vacakbagoly,"platina-vacakbagoly (ezüstös apróbagoly)"}
Curruca nisoria	{karvalyposzáta}
Larus cachinnans	{"sárgalábú sirály","Sárgalábú sirály","sztyeppi sirály"}
Aquilegia nigricans	{"feketéllő harangláb","Feketés harangláb"}
Arnica montana	{Árnika,"árnika (hegyi árnika)","hegyi árnika"}
Notodonta torva	{"Kormos púposszövő","ritka púposszövő","ritka púposszövő (kormos púposszövő)"}
Phalera bucephaloides	{"magyar púposszövő","magyar púposszövő (sárgaholdas púposszövő)","Sárgaholdas púposszövő"}
Rana ridibunda	{"kacagó béka","kacagó béka (tavibéka)","Tavi béka"}
Spialia sertorius	{"lápi busalepke","lápi busalepke (nyugati törpebusalepke)","Nyugati törpebusalepke"}
Ovis musimon	{muflon}
Trachemys scripta	{ékszerteknős,Ékszerteknős,"sárgafülű ékszerteknős"}
Vipera ursinii	{"parlagi vipera","Parlagi vipera","rákosi vipera"}
Acer negundo	{"kőrislevelű juhar","zöld juhar","Zöld juhar"}
Achillea ptarmica	{"kenyérbél cickafark",kenyérbélcickafark,Kenyérbélcickafark}
Acorus calamus	{kálmos,Kálmos,"kálmos (orvosi kálmos)"}
Aethionema saxatile	{"kövi sulyoktáska",Sulyoktáska,"sulyoktáska (kövi sulyoktáska)"}
Agrostemma githago	{konkoly,Konkoly,"konkoly (vetési konkoly)"}
Alchemilla glaucescens	{"hegyi palástfű","szürkészöld palástfű","Szürkezöld palástfű"}
Alchemilla monticola	{"hegyi palástfű","közönséges palástfű","Közönséges palástfű"}
Allium moschatum	{"pézsma hagyma",pézsmahagyma,Pézsmahagyma}
Allium victorialis	{"győzedelmes hagyma","győzedelmes hagyma (havasi hagyma)","Havasi hagyma"}
Amorpha fruticosa	{"cserjés gyalogakác","Cserjés gyalogakác",gyalogakác}
Anacamptis pyramidalis	{vitézvirág,Vitézvirág,"vitézvirág (tornyos sisakoskosbor, tornyos vitézvirág)"}
Anogramma leptophylla	{"tavaszi kérészpáfrány (télipáfrány)",télipáfrány,Télipáfrány}
Asphodelus albus	{"genyőte (fehér genyőte, királyné gyertyája)","királyné gyertyája","Királyné gyertyája"}
Astrantia major	{"nagy völgycsillag",völgycsillag,Völgycsillag}
Betula pubescens	{"molyhos nyír","molyhos nyír (szőrös nyír)","Szőrös nyír"}
Blackstonia acuminata	{gyíkpohár,Gyíkpohár,"kései gyíkpohár"}
Blechnum spicant	{bordapáfrány,Bordapáfrány,"erdei bordapáfrány"}
Blysmus compressus	{"kétsoros káka",Kétsoroskáka,"lapos kétsoroskáka"}
Calamagrostis purpurea	{"északi nádtippan","pirosló nádtippan","Pirosló nádtippan"}
Dendrocopos sp.	{fakopáncs}
Caldesia parnassifolia	{"lápi széleslevelű-hídőr",Szíveslevelű-hídőr,"szíveslevelű-hídőr (lápi szíveslevelű-hídőr)"}
Carex rostrata	{"csőrös sás","Csőrös sás","csőrős sás"}
Dryobates minor	{"kis fakopáncs"}
Chaerophyllum hirsutum	{"szőrös baraboly","Szőrös baraboly","szőrős baraboly"}
Chamaecytisus ciliatus	{"pillás törpezanót","pillás zanót","Pillás zanót"}
Chimaphila umbellata	{"csinos ernyőskörtike",ernyőskörtike,"Közönséges ernyőskörtike"}
Cicuta virosa	{"gyilkos csomorika","Gyilkos csomorika","gyílkos csomorika"}
Cimicifuga europaea	{"büdös poloskavész",poloskavész,Poloskavész}
Coeloglossum viride	{zöldike,Zöldike,"zöldike (zöldike ujjaskosbor)"}
Conringia austriaca	{"keleti nyilasfű","nyugati nyilasfű","Osztrák nyilasfű"}
Cotoneaster integerrimus	{"Piros madárbirs","szirti madárbirs","szirti madárbirs (piros madárbirs)"}
Cotoneaster matrensis	{"fekete madárbirs (incl. pannon madárbirs)","pannon madárbirs","Pannon madárbirs"}
Cotoneaster niger	{"fekete madárbirs","Fekete madárbirs","fekete madárbirs (incl. pannon madárbirs)"}
Cotoneaster tomentosus	{"molyhos madárbirs","molyhos madárbirs (nagylevelű madárbirs)","Nagylevelű madárbirs"}
Crambe tataria	{tátorján,Tátorján,"tátorján (buglyos tátorján)"}
Cypripedium calceolus	{"boldogasszony papucsa",Rigópohár,"rigópohár (erdei papucskosbor, Boldogasszony papucsa)"}
Dictamnus albus	{"kőrislevelű nagyezerjófű",nagyezerjófű,"Nagy ezerjófű"}
Elodea canadensis	{átokhínár,Átokhínár,"kanadai átokhínár"}
Epipactis atrorubens	{"vörösbarna nőszőfű","Vörösbarna nőszőfű","vörösbarna nőszőfű (incl. Borbás nőszőfű)"}
Epipactis leptochila	{"csőrös nőszőfű","Kesenyajkú nőszőfű","keskenyajkú nőszőfű"}
Epipactis purpurata	{"bíboribolya nőszőfű","Bíboribolya nőszőfű","ibolyás nőszőfű"}
Epipogium aphyllum	{bajuszvirág,Bajuszvirág,"levéltelen bajuszvirág"}
Eranthis hyemalis	{téltemető,Téltemető,"téltemető (kikeletnyitó téltemető)"}
Erythronium dens-canis	{kakasmandikó,Kakasmandikó,"kakasmandikó (európai kakasmandikó)"}
Ereunetes alpina	{"havasi partfutó"}
Fraxinus pennsylvanica	{"amerikai kőris","Amerikai kőris","vörös kőrisfa"}
Fritillaria meleagris	{kockásliliom,Kockásliliom,"mocsári kockásliliom (kotuliliom)"}
Galium austriacum	{"balatoni galaj","Balatoni galaj","osztrák galaj"}
Gentianella austriaca	{"Hegyi tárnicska","osztrák tárnicska","osztrák tárnicska (hegyi tárnicska)"}
Geum rivale	{"patakparti gyömbérgyökér","Patakparti gyömbérgyökér","patakparti gyömbérgyökér (bókoló gyömbérgyökér)"}
Globularia cordifolia	{"szíveslevelű gubóvirág","Szíveslevelű gubóvirág","szívlevelű gubóvirág"}
Goodyera repens	{avarvirág,Avarvirág,"avarvirág (kúszó avarvirág)"}
Groenlandia densa	{"sűrűlevelű békaszőlő",sűrűlevelű-békaszőlő,"Sűrűlevelű békaszőlő"}
Radiola linoides	{"apró csepplen",csepplen,Csepplen}
Gymnocarpium dryopteris	{"közönséges tölgyespáfrány","Közönséges tölgyespáfrány","közönséges tölgyespáfrány (közönséges hármasleveű-páfrány)"}
Gymnocarpium robertianum	{"mirigyes tölgyespáfrány","Mirigyes tölgyespáfrány","mirigyes tölgyespáfrány (mirigyes hármaslevelűpáfrány)"}
Hammarbya paludosa	{tőzegorchidea,Tőzegorchidea,"tőzegorchidea (fiókás tőzegorchidea)"}
Ereunetes ferrugineus	{"sarlós partfutó"}
Hepatica nobilis	{májvirág,Májvirág,"nemes májvirág"}
Hesperis matronalis	{hölgyestike,Hölgyestike,"hölgyestike (kiv. Vrabély-estike)"}
Hieracium kossuthianum	{"dunai hölgymál","Dunai hölgymál",Kossuth-hölgymál}
Himantoglossum caprinum	{"bíboros/Janka sallangvirág","bíbor sallangvirág","Bíbor sallangvirág"}
Hottonia palustris	{békaliliom,Békaliliom,"mocsári békaliliom"}
Huperzia selago	{"györgyfű (részegkorpafű)","részeg korpafű",Részegkorpafű}
Hydrocotyle vulgaris	{Gázló,"gázló (lápi gázló)","lápi gázló"}
Ereunetes temminckii	{Temminck-partfutó}
Iris pumila	{"apró nőszirom","Apró nőszirom","törpe nőszirom"}
Iris spuria	{"fátyolos nőszirom (korcs nőszirom)","korcs nőszirom","Korcs nőszirom"}
Lamium orvala	{"pofók árvacsalán","pufók árvacsalán","Pufók árvacsalán"}
Limodorum abortivum	{gérbics,Gérbics,"gérbics (ibolyás gérbics)"}
Ficedula sp.	{légykapó}
Linum dolomiticum	{dolomitlen,Dolomitlen,"pilisi len (dolomitlen)"}
Liparis loeselii	{hagymaburok,Hagymaburok,"hagymaburok (lápi hagymaburok)"}
Ludwigia palustris	{tóalma,Tóalma,"tóalma (közönséges tóalma)"}
Marsilea quadrifolia	{mételyfű,Mételyfű,"mételyfű (négylevelű mételyfű)"}
Matteuccia struthiopteris	{struccpáfrány,Struccpáfrány,"struccpáfrárny (európai struccpáfrárny)"}
Menyanthes trifoliata	{"hármaslevelű vidrafű",vidrafű,Vidrafű}
Moneses uniflora	{"egyvirágú kiskörtike","egyvirágú körtike",Egyvirágú-körtike}
Myricaria germanica	{csermelyciprus,"német csermelyciprus","Német csermelyciprus"}
Neottia nidus-avis	{Madáfészek,madárfészek,"madárfészek (madárfészek-békakonty)"}
Nepeta parviflora	{"borzas macskamenta","kisvirágú macskamenta","Kisvirágú macskamenta"}
Nymphoides peltata	{tündérfátyol,Tündérfátyol,"tündérfátyol (vízi tündérfátyol)"}
Ophioglossum vulgatum	{kígyónyelv,Kígyónyelv,"kígyónyelv (közönséges kígyónyelv)"}
Orchis coriophora	{"poloskaszagú kosbor","Poloskaszagú kosbor","poloskaszagú kosbor (poloskaszagú sisakoskosbor)"}
Orchis morio	{"agár kosbor",Agárkosbor,"agár kosbor (agár sisakoskosbor)"}
Orchis tridentata	{"tarka kosbor","Tarka kosbor","tarka kosbor (tarka pettyeskosbor)"}
Orchis ustulata	{"sömörös kosbor","Sömörös kosbor","sömörös kosbor (sömörös pettyeskosbor)"}
Oreopteris limbosperma	{hegyipáfrány,Hegyipáfrány,"tölcséres hegyipáfrány"}
Ornithogalum pyramidale	{"nyúlánk madártej","Nyúlánk madártej","nyúlánk sárma"}
Ornithogalum refractum	{"csilláros madártej","Csilláros madártej","csilláros sárma"}
Ornithogalum sphaerocarpum	{"gömböstermésű sárma","gömbtermésű madártej","Gömbtermésű madártej"}
Orobanche caesia	{"deres vajvirág","Deres vajvirág","deres vajvirág (deres szádor)"}
Orobanche hederae	{"borostyán vajvirág",Borostyán-vajvirág,"borostyán-vajvirág (borostyán-szádor)"}
Orthilia secunda	{"bókoló gyöngyvirágoskörtike","gyöngyvirágos körtike",Gyöngyvirágos-körtike}
Osmunda regalis	{"királyharaszt (óriás királyharaszt)",királypáfrány,Királypáfrány}
Oxalis dillenii	{dillenius-madársóska,Dillenius-madársóska,"parlagi madársóska"}
Parnassia palustris	{fehérmájvirág,Fehérmájvirág,"fehérmájvirág (mocsári tőzegboglár)"}
Paronychia cephalotes	{Ezüstaszott,"ezüstaszott (keskenylevelű ezüstvirág)",ezüstvirág}
Paulownia tomentosa	{császárfa,Császárfa,"illatos császárfa"}
Phegopteris connectilis	{buglyospáfrány,Buglyospáfrány,"bükkös buglyospáfrány"}
Phlomis tuberosa	{macskahere,Macskahere,"macskahere (gumós macskahere)"}
Physospermum cornubiense	{dudamag,Dudamag,"harangláblevelű dudamag"}
Phytolacca esculenta	{"kínai alkörmös","Kínai alkörmös","kínai karmazsinbogyó"}
Poa remota	{"hegyi perje","Hegyi perje","magyar perje"}
Grus virgo	{"pártás daru"}
Primula elatior	{"sudár kankalin (sugárkankalin)","sugár kankalin",Sugárkankalin}
Puccinellia peisonis	{"fertő-tavi mézpázsit","fertőtavi mézpázsit","Fertő-tavi mézpázsit"}
Pyrola chlorantha	{"zöldes körtike","zöldvirágú körtike","Zöldvirágú körtike"}
Pyrola media	{"közepes körtike","középső körtike","Középső körtike"}
Pyrus magyarica	{"magyar körte","Magyar körte","magyar vadkörte"}
Ranunculus psilostachys	{"csőrös boglárka","Csőrös boglárka","csőrős boglárka"}
Ranunculus strigulosus	{"merevszőrű boglárka","Merevszőrű boglárka","merevszőrű boglárka (Steven-boglárka)"}
Rhus hirta	{ecetfa,ecetszömörce,"Torzsás szömörce"}
Rhynchospora alba	{"fehér tőzegkáka",tőzegkáka,Tőzegkáka}
Salvinia natans	{rucaöröm,"rucaöröm (vízi rucaöröm)","Vízi rucaöröm"}
Hydrocoloeus minutus	{"kis sirály"}
Samolus valerandi	{árokvirág,Árokvirág,"sziki árokvirág"}
Scopolia carniolica	{farkasbogyó,Farkasbogyó,"krajnai farkasbogyó"}
Sempervivum marmoreum	{"mátrai kövirózsa","mátrai kövirózsa (rózsás kövirózsa)","Rózsás kövirózsa"}
Sempervivum tectorum	{"fali kövirózsa","Fali kövirózsa","házi kövirózsa (fali kövirózsa)"}
Sorbus domestica	{"házi berkenye","házi berkenye (kerti berkenye)","Kerti berkenye"}
Spiranthes aestivalis	{"nyári füzértekercs","Nyári füzértekercs","nyári fűzértekercs"}
Spiranthes spiralis	{"őszi füzértekercs","Őszi füzértekercs","őszi fűzértekercs"}
Sternbergia colchiciflora	{vetővirág,Vetővirág,"vetővirág (apró vetővirág)"}
Tamus communis	{pirítógyökér,Pirítógyökér,"pirítógyökér (felfutó pirítógyökér)"}
Taraxacum serotinum	{"kései pittypang","kései pitypang","Kései pitypang"}
Thelypteris palustris	{tőzegpáfrány,Tőzegpáfrány,"tőzegpáfrány (mocsári tőzegpáfrány)"}
Tragopogon floccosus	{"homoki bakszakál","homoki bakszakáll","Homoki bakszakáll"}
Tragus racemosus	{"bugás tövisperje",tövisperje,Tövisperje}
Trapa natans	{sulyom,Sulyom,"sulyom (csemegesulyom)"}
Traunsteinera globosa	{gömböskosbor,Gömböskosbor,"gömböskosbor (karcsú gömböskosbor)"}
Trollius europaeus	{"Közönséges zergeboglár",zergeboglár,"zergeboglár (közönséges zergeboglár)"}
Urtica kioviensis	{"kúszó csalán","Kúszó csalán","kúszó csalán (lápi csalán)"}
Aconitum variegatum	{"karcsú sisakvirág","Karcsú sisakvirág"}
Woodsia ilvensis	{"északi szirtipáfrány","Északi szirtipáfrány","hegyi szirtipáfrány (északi szirtipárfány)"}
Dorcadion fulvum cervae	{"pusztai gyalogcincér","Pusztai gyalogcincér"}
Lacerta vivipara pannonica	{"elevenszülő gyík","Elevenszülő gyík"}
Maculinea alcon xerophila	{"Karszti hangyaboglárka","szürkés hangyaboglárka"}
Maculinea arion ligurica	{"nagyfoltú hangyaboglárka alfaj","Türkiz boglárka"}
Egretta alba	{"nagy kócsag","Nagy kócsag"}
Mergus albellus	{"kis bukó","Kis bukó"}
Falco biarmicus	{"Feldegg sólyom",Feldegg-sólyom}
Apus melba	{"havasi sarlósfecske","Havasi sarlósfecske"}
Hippolais pallida	{"halvány geze","Halvány geze"}
Onosma arenaria	{"homoki vértő","Homoki vértő"}
Elaphomyces persooni	{"kékbelű álszarvasgomba","Kékbelű álszarvasgomba"}
Elaphomyces mutabilis	{"bundás álszarvasgomba","Bundás álszarvasgomba"}
Elaphomyces maculatus	{"foltos álszarvasgomba","Foltos álszarvasgomba"}
Elaphomyces leveillei	{"patinás álszarvasgomba","Patinás álszarvasgomba"}
Elaphomyces anthracinus	{"köldökös álszarvasgomba","Köldökös álszarvasgomba"}
Gomphus clavatus	{disznófülgomba,Disznófülgomba}
Hericium erinaceum	{"közönséges süngomba","Közönséges süngomba"}
Polyporus tuberaster	{olaszgomba,Olaszgomba}
Sarcodon scabrosus	{"korpás gereben","Korpás gereben"}
Scutiger pescaprae	{"barnahátú zsemlegomba","Barnahátú zsemlegomba"}
Polyporus umbellatus	{tüskegomba,Tüskegomba}
Endoptychum agaricoides	{"lemezes pöfeteg","Lemezes pöfeteg"}
Gomphidius roseus	{"rózsaszínű nyálkásgomba","Rózsaszínű nyálkásgomba"}
Gyrodon lividus	{égertinóru,Égertinóru}
Hygrocybe calyptriformis	{"rózsaszínű nedűgomba","Rózsaszínű nedűgomba"}
Hypsizygus ulmarius	{laskapereszke,Laskapereszke}
Lactarius helvus	{daróc-tejelőgomba,Daróc-tejelőgomba}
Leccinum variicolor	{"tarkahúsú érdestinóru","Tarkahúsú érdestinóru"}
Leucopaxillus macrocephalus	{"gyökeres álpereszke","Gyökeres álpereszke"}
Phylloporus pelletieri	{"lemezes tinóru","Lemezes tinóru"}
Rhodotus palmatus	{"tönkös kacskagomba","Tönkös kacskagomba"}
Russula claroflava	{"krómsárga galambgomba","Krómsárga galambgomba"}
Squamanita schreieri	{"sárga pikkelyesgalóca","Sárga pikkelyesgalóca"}
Strobilomyces strobilaceus	{"pikkelyes tinóru","Pikkelyes tinóru"}
Volvariella bombycina	{"óriás bocskorosgomba","Óriás bocskorosgomba"}
Pseudoboletus parasiticus	{"élősdi tinóru","Élősdi tinóru"}
Geastrum hungaricum	{"honi csillaggomba","Honi csillaggomba"}
Diphasiastrum complanatum	{"közönséges laposkorpafű","Közönséges laposkorpafű"}
Ranunculus fluitans	{"úszó víziboglárka","Úszó víziboglárka"}
Aruncus dioicus	{"erdei tündérfürt",Tündérfürt}
Sorbus danubialis	{"dunai berkenye","Dunai berkenye"}
Sorbus pseudodanubialis	{"Áldunai berkenye","dunamenti berkenye"}
Sorbus pannonica	{"dunántúli berkenye","Dunántúli berkenye"}
Sorbus budaiana	{"Budai berkenye",Budai-berkenye}
Sorbus semiincisa	{"budai berkenye","Budai berkenye"}
Sorbus borosiana	{"Boros- berkenye",Boros-berkenye}
Sorbus pseudolatifolia	{"Sárgáslevelű berkenye","széleslevelű berkenye"}
Sorbus pseudosemiincisa	{"kevéserű berkenye","Kevéserű berkenye"}
Sorbus bakonyensis	{"bakonyi berkenye","Bakonyi berkenye"}
Sorbus balatonica	{"balatoni berkenye","Balatoni berkenye"}
Sorbus decipientiformis	{"keszthelyi berkenye","Keszthelyi berkenye"}
Sorbus latissima	{"nagylevelű berkenye","Nagylevelű berkenye"}
Sorbus subdanubialis	{"Duna-vidéki berkenye","közép-dunai berkenye (Duna-vidéki berkenye)"}
Potentilla palustris	{Tőzegeper,"tőzegeper (tőzegpimpó)"}
Alchemilla micans	{"kecses palástfű","Kecses palástfű"}
Rosa villosa	{"gyapjas rózsa (incl. szentendrei rózsa)",Sherard-rózsa}
Prunus serotina	{"kései meggy","Kései meggy"}
Prunus tenella	{"törpe mandula",Törpemandula}
Trifolium subterraneum	{"földbentermő here","Földbentermő here"}
Robinia pseudoacacia	{"fehér akác","Fehér akác"}
Hippocrepis emerus	{"bokros koronafürt","Bokros koronafürt"}
Securigera elegans	{"Nagylevelű koronafürt","nagylevelű tarkakoronafürt"}
Lathyrus linifolius	{"hegyi lednek","Hegyi lednek"}
Egeria densa	{"argentin átokhínár","Sűrűlevelű átokhínár"}
Allium paniculatum	{"Bugás hagyma","bugás hagyma (vöröses hagyma)"}
Allium marginatum	{"bugás hagyma (vöröses hagyma)","Vöröses hagyma"}
Prospero elisae	{"Őszi csillagvirág","őszi csillagvirágok"}
Juncus alpinoarticulatus	{"havasi szittyó","Havasi szittyó"}
Ophrys sphegodes	{pókbangó,Pókbangó}
Carex cespitosa	{"gyepes sás","Gyepes sás"}
Carex hartmanii	{"északi sás","Északi sás"}
Eriocheir sinensis	{"kínai gyapjasollósrák","Kínai gyapjasollósrák"}
Pacifastacus leniusculus	{Jelzőrák,"jelzőrák, vagy amerikai folyamirák"}
Procambarus clarkii	{"kaliforniai vörösrák","Kaliforniai vörösrák"}
Procambarus falax	{"virginiai márványrák","Virginiai márványrák"}
Polymitarcis virgo	{dunavirág,Dunavirág}
Carabus linnei	{"Kárpáti futrinka",Linné-futrinka}
Chlidonias hybrida	{fattyúszerkő}
Megopis scabricornis	{diófacincér,Diófacincér}
Dorcadion fulvum	{"barna gyalogcincér","Barna gyalogcincér"}
Morimus funereus	{gyászcincér,Gyászcincér}
Dorcadion decipiens	{"homoki gyalogcincér","Homoki gyalogcincér"}
Chroicocephalus genei	{"vékonycsőrű sirály"}
Chroicocephalus ridibundus	{dankasirály}
Ichneumon dispar	{"gyapjaslepke fürkész",gyapjaslepke-fürkész}
Dasypoda mixta	{"ritka gatyásméh","Ritka gatyásméh"}
Chamaesphecia colpiformis	{"délvidéki szitkár","Délvidéki szitkár"}
Synansphecia affinis	{napvirágszitkár,Napvirágszitkár}
Maculinea alcon	{"szürkés hangyaboglárka","Szürkés hangyaboglárka"}
Maculinea arion	{"nagyfoltú hangyaboglárka","Nagyfoltú hangyaboglárka"}
Maculinea teleius	{Vérfűboglárka,vérfű-hangyaboglárka}
Maculinea nausithous	{"sötét hangyaboglárka (zanótboglárka)",Zanótboglárka}
Aricia eumedon	{gólyaorr-boglárka,Gólyaorrboglárka}
Rheumaptera undulata	{"hullámvonalas araszoló","Hullámvonalas araszoló"}
Charissa ambiguata	{"havasi sziklaaraszoló","Havasi sziklaaraszoló"}
Charissa pullata	{mészkő-sziklaaraszoló,Mészkő-sziklaaraszoló}
Charissa variegata	{"tarka sziklaaraszoló","Tarka sziklaaraszoló"}
Coscinia cribraria	{"pettyes molyszövő","Pettyes molyszövő"}
Hypenodes pannonica	{"pannon karcsúbagoly","pannon karcsúbagoly (lápi karcsúbagoly)"}
Eublemma pannonica	{"magyar gyopárbagoly","Magyar gyopárbagoly"}
Euxoa hastifera	{"fehérsávos földibagoly","Fehérsávos földibagoly"}
Bittacus hageni	{"Hagen csőrösrovar",Hagen-csőrösrovar}
Rutilus pigus	{leánykoncér,pigó}
Cobitis taenia	{"vágó csík",vágócsík}
Acipenser gueldenstaedti	{"vágó tok",vágótok}
Rana esculenta	{kecskebéka,Kecskebéka}
Lacerta vivipara	{"elevenszülő gyík","Elevenszülő gyík"}
Actitis hypoleucos	{billegetőcankó,Billegetőcankó}
Stercorarius skua	{"nagy halfarkas","Nagy halfarkas"}
Nyctea scandiaca	{hóbagoly,Hóbagoly}
Delichon urbica	{molnárfecske,Molnárfecske}
Carduelis chloris	{zöldike,Zöldike}
Miliaria calandra	{sordély,Sordély}
Myocastor coypus	{nutria,Nutria}
Pipistrellus savii	{"alpesi denevér","alpesi törpedenevér"}
Citellus citellus	{"közönséges ürge",ürge}
Rhus typhina	{ecetfa,Szömörce}
Solidago gigantea	{"magas aranyvessző","Magas aranyvessző"}
Aster ×salignus	{"fűzlevelű őszirózsa","Fűzlevelű őszirózsa"}
Aster lanceolatus	{"Lándzsáslevelű őszirózsa","lándzsás őszirózsa"}
Conyza canadensis	{betyárkóró,Betyárkóró}
Ambrosia artemisiifolia	{Parlagfű,"ürömlevelű parlagfű"}
Helianthus decapetalus	{"sokvirágú napraforgó","Sokvirágú napraforgó"}
Leucanthemella serotina	{"tiszaparti késeimargitvirág (tisza-parti margitvirág)","Tiszaparti margitvirág"}
Tephroseris aurantiaca	{"Narancsszínű aggófű","narancsszínű aggóvirág"}
Tephroseris longifolia	{"Havasalji aggófű","hosszúlevelű aggóvirág"}
Tephroseris crispa	{Csermelyaggófű,csermelyaggóvirág}
Jurinea glycacantha	{"nagyfészkű hangyabogáncs","Nagyfészkű hangyabogáncs"}
Cirsium boujartii	{Boujart-aszat,"pécsvidéki aszat"}
Gymnocoronis spilanthoides	{"mexikói vízibojt",Vízibojt}
Hieracium bupleuroides	{"tátrai hölgymál","Tátrai hölgymál"}
Silene dioica	{"piros mécsvirág","Piros mécsvirág"}
Silene bupleuroides	{"Gór habszegfű","termetes habszegfű (gór habszegfű)"}
Krascheninnikovia ceratoides	{pamacslaboda,Pamacslaboda}
Amaranthus powellii	{"karcsú disznóparéj","Karcsú disznóparéj"}
Seseli peucedanoides	{"zöldes gurgolya (zöldes kígyókapor)","Zöldes kígyókapor"}
Heracleum persicum	{"perzsa medvetalp","Perzsa medvetalp"}
Persicaria bistorta	{"kígyógyökerű keserűfű","Kígyógyökerű keserűfű"}
Fallopia japonica	{"ártéri japánkeserűfű","Ártéri japánkeserűfű"}
Fallopia sachalinensis	{"óriás japánkeserűfű","Óriás japánkeserűfű"}
Ulmus procera	{"érdeslevelű szil","Érdeslevelű szil"}
Oxalis stricta	{"Felálló madársáska","felálló madársóska"}
Salix myrsinifolia	{"Feketedő fűz","feketéllő fűz"}
Gentianopsis ciliata	{"kései prémestárnics","Prémes tárnicska"}
Buddleja davidii	{"Illatos nyáriorgona",nyáriorgona}
Anchusa ochroleuca	{"vajszínű atracél","Vajszínű atracél"}
Echium maculatum	{"piros kígyószisz","Piros kígyószisz"}
Micromeria thymifolia	{"illír szirtiperesztény","Szirti pereszlény"}
Pseudolysimachion spurium	{"bugás fürtösveronika","Bugás veronika"}
Pseudolysimachion longifolium	{"hosszúlevelű fürtösveronika","Hosszúlevelű veronika"}
Pseudolysimachion incanum	{"szürke fürtösveronika","Szürke veronika"}
Melampyrum bihariense	{"erdélyi csormolya","Erdélyi csormolya"}
Thlaspi caerulescens	{"havasalji tarsóka","Havasalji tarsóka"}
Aurinia saxatilis	{"Sziklai ternye","sziklaiternye (szirti sziklaiternye)"}
Corvus corone corone	{"kormos varjú"}
Cardamine glanduligera	{"ikrás fogasír","Ikrás fogas-ír"}
Cardamine waldsteinii	{"hármaslevelű fogasír","Hármaslevelű fogas-ír"}
Festuca wagneri	{"rákosi csenkesz","Rákosi csenkesz"}
Poa scabra	{"hegyi perje","Magyar perje"}
Sesleria heufleriana	{"erdélyi nyúlfarkfű","Erdélyi nyúlfarkfű"}
Sesleria sadleriana	{"budai nyúlfarkfű","Budai nyúlfarkfű"}
Sesleria albicans	{"tarka nyúlfarkfű","Tarka nyúlfarkfű"}
Sesleria caerulea	{"lápi nyúlfarkfű","Lápi nyúlfarkfű"}
Avenula compressa	{"tömött zabfű","Tömött zabfű"}
Stipa pennata	{"Hegyi árvalányhaj","pusztai árvalányhaj"}
Corvus splendens	{"házi varjú"}
Fallopia x bohemica	{"cseh óriáskeserűfű"}
Bison bison	{"amerikai bölény"}
Callosciurus erythraeus	{"csinos tarkamókus"}
Crocidura sp.	{cickány}
Erinaceus roumanicus	{"keleti sün"}
Herpestes javanicus	{"jávai mongúz"}
Muntiacus reevesii	{"indiai muntjákszarvas"}
Mustela eversmannii	{molnárgörény}
Nasua nasua	{"vörösorrú koáti (ormányosmedve)"}
Nasua nasua	{"ormányos medve"}
Neomys sp.	{vízicickány}
Sciurus carolinensis	{"keleti szürkemókus"}
Sciurus niger	{"vörös rókamókus"}
Sorex sp.	{cickány}
Tamias sibiricus	{"szibériai csíkosmókus, vagy burunduk"}
Barbastella sp.	{piszedenevér}
Chiroptera	{denevér}
Eptesicus nilssonii	{"északi késeidenevér"}
Eptesicus sp.	{késeidenevér-fajok}
Miniopterus sp.	{denevér-fajok}
Myotis sp.	{denevér-fajok}
Nyctalus sp.	{koraidenevér-fajok}
Pipistrellus sp.	{törpedenevér-fajok}
Plecotus sp.	{denevér-fajok}
Rhinolophus sp.	{denevér-fajok}
Vespertilio sp.	{denevér-fajok}
Actebia fugax	{"pusztai földibagoly"}
Aglais io	{"nappali pávaszem"}
Amphimelania holandri	{"szávai vízicsiga"}
Amphipyra cinnamomea	{"ritka fahéjbagoly"}
Anaciaeschna isosceles	{"lápi acsa"}
Anodonta woodiana	{"amuri kagyló"}
Apatura sp.	{színjátszólepke-fajok}
Argynnis paphia f. valesiana	{"nagy gyöngyházlepke"}
Aricia agestis	{szerecsenboglárka}
Aricia artaxerxes	{"bükki szerecsenboglárka"}
Arion rufus	{"barna csupaszcsiga"}
Arion vulgaris	{"spanyol meztelencsiga"}
Asteroscopus syriacus	{"magyar őszi-fésűsbagoly (Kovács-bundásbagoly)"}
Astiotes dilecta	{"nagy övesbagoly"}
Callimorpha quadripunctaria	{"csíkos medvelepke"}
Callimorpha quadripunctata	{"csíkos medvelepke"}
Calomera littoralis nemoralis	{"sziki homokfutrinka"}
Camptogramma scripturata	{"vonalkás hegyiaraszoló"}
Camptorrhinus simplex	{"sima holttettetős-ormányos"}
Camptorrhinus statua	{"bordás holttettetős-ormányos"}
Carabus clathratus	{"szárnyas futrinka"}
Carabus convexus convexus	{"selymes futrinka"}
Carabus scabriusculus scabriusculus	{"érdes futrinka"}
Carabus sp.	{futrinka-fajok}
Carabus ullrichi	{"rezes futrinka"}
Carabus zawadszkii	{"zempléni futrinka"}
Caradrina gilva	{"platina-vacakbagoly (ezüstös apróbagoly)"}
Chariaspilates formosarius	{"pompás lápiaraszoló (lápi tarkaaraszoló)"}
Charissa intermedia	{"változó sziklaaraszoló"}
Chilostoma banaticum	{"bánáti csiga"}
Chlaenius decipiens	{"azúr bűzfutó"}
Chlaenius sulcicollis	{"szomorú bűzfutó"}
Chondrosoma fiduciarium	{"magyar ősziaraszoló"}
Cicindela transversalis	{"nyugati homokfutrinka"}
Corbicula fluminea	{"ázsiai kagyló"}
Cornu aspersum	{"pettyes éticsiga"}
Cucullia mixta	{"vértesi csuklyásbagoly"}
Diabrotica virgifera virgifera	{"amerikai kukoricabogár"}
Dichagyris musiva	{"szegélyes földibagoly"}
Dioszeghyana schmidtii	{"magyar tavaszi-fésűsbagoly (magyar barkabagoly)"}
Discoloxia blomeri	{szilfaaraszoló}
Dorcadion fulvum fulvum	{"barna gyalogcincér"}
Ennomos quercarius	{molyhostölgy-levélaraszoló}
Epatolmis luctifera	{"füstös medvelepke"}
Ephemerella mesoleuca	{"fehérfoltú kérész"}
Ephesia diversa	{kőrisfa-övesbagoly}
Eresus cinnaberinus	{bikapók}
Eudia pavonia	{"kis pávaszem"}
Eudia spini	{"közepes pávaszem"}
Euphya scripturata	{"vonalkás hegyiaraszoló"}
Everes alcetas	{"palakék boglárka"}
Everes decolorata	{"fakó boglárka"}
Fabula zollikoferi	{óriás-szürkebagoly}
Fagotia acicularis	{folyamcsiga}
Fagotia daudebartii	{folyamcsiga}
Formica execta	{"fészeképítő hangyafajok védett fészkei"}
Formica polyctenarufa	{"fészeképítő hangyafajok védett fészkei"}
Formica pratensis	{"fészeképítő hangyafajok védett fészkei"}
Formica pressilabris	{"fészeképítő hangyafajok védett fészkei"}
Formica sp.	{"fészeképítő hangyafajok védett fészkei"}
Formica truncorum	{"fészeképítő hangyafajok védett fészkei"}
Gerris najas	{"nagy molnárpoloska"}
Helix lucorum	{"fehérsávos éticsiga"}
Homarus americanus	{"amerikai homár"}
Hygromia kovacsi	{"dobozi pikkelyescsiga"}
Hygromia transsylvanica	{"erdélyi pikkelyescsiga"}
Hyles galii	{galajszender}
Hypantria cunea	{"amerikai fehér medvelepke v. amerikai szövőlepke"}
Hyponephele lupina	{"homoki ökörszemlepke"}
Inocellia braueri	{"déli kurta-tevenyakú"}
Isognomostoma isognomostoma	{"háromfogú csiga"}
Isophya camptoxypha	{"kárpáti tarsza"}
Jolana iolas	{"magyar boglárka"}
Lamellocossus terebrus	{nyárfarontólepke}
Lamprodila festiva	{boróka-tarkadíszbogár}
Lamprodila mirifica	{szilfa-tarkadíszbogár}
Lamprodila rutilans	{hársfa-tarkadíszbogár}
Lycaena dispar hungarica	{"nagy tűzlepke"}
Lycosa vultuosa	{"pokoli cselőpók"}
Macrosiagon bimaculata	{"sarkantyús fészekbogár"}
Mantispa aphavexelle	{"mediterrán fogólábú-fátyolka"}
Melitaea ornata	{"magyar tarkalepke"}
Myrmecaelurus punctulatus	{"kunsági hangyafarkas"}
Nemesia pannonica	{"magyar aknászpók"}
Netocia hungarica	{"magyar virágbogár"}
Nymphalis atalanta	{atalantalepke}
Nymphalis c-album	{"c-betűs lepke"}
Nymphalis io	{"nappali pávaszem"}
Nymphalis urticae	{"kis rókalepke"}
Nymphalis vau-album	{"l-betűs rókalepke"}
Odontognophos dumetatus	{"csücskös sziklaaraszoló"}
Orconectes virilis	{"északi cifrarák"}
Oryctes nasicornis holdhausi	{orrszarvúbogár}
Oxychilus depressus	{"lapos kristálycsiga"}
Oxychilus orientalis	{"keleti kristálycsiga"}
Oxytrypia orbiculosa	{"nagyfoltú bagoly"}
Paladilhia hungarica	{"magyar vakcsiga"}
Perforatella vicinus	{"pikkelyes csiga"}
Perizoma sagittata	{"nyílfoltos tarkaaraszoló"}
Pharmacis fusconebulosus	{"északi gyökérrágólepke"}
Phenacolimax annularis	{"gyűrűs üvegcsiga"}
Phengaris alcon	{"szürkés hangyaboglárka"}
Phengaris arion	{"nagyfoltú hangyaboglárka"}
Phengaris nausithous	{"sötét hangyaboglárka (zanótboglárka)"}
Phengaris teleius	{vérfű-hangyaboglárka}
Phyllodesma ilicifolia	{cserlevélpohók}
Phyllomorpha laciniata	{"lándzsás karimáspoloska"}
Plebejus idas	{"északi boglárka"}
Plebejus sephirus	{"fóti boglárka"}
Pomatias rivulare	{"keleti ajtóscsiga"}
Potosia fieberi	{"rezes virágbogár"}
Procambarus falax virginalis	{"virginiai márványrák"}
Procambarus falax forma virginalis	{"virginiai márványrák"}
Procambarus fallax	{"virginiai márványrák"}
Procambarus sp.	{"virginiai márványrák"}
Potosia affinis	{"smaragdzöld virágbogár"}
Pseudophilotes schiffermuelleri	{"apró boglárka"}
Psilothrix femoralis	{"pusztai karimásbogár"}
Ptilophorus dufourii	{"szürke darázsbogár"}
Reskovitsia alborivularis	{"keleti kormosmoly"}
Rhyparioides metelkanus	{Metelka-medvelepke}
Sadleriana pannonica	{"tornai patakcsiga"}
Saragossa implexa	{"ázsiai szegfűbagoly"}
Scoliantides orion	{"szemes boglárka"}
Shargacucullia thapsiphaga	{"fakó csuklyásbagoly"}
Sphyracephala europaea	{"európai nyelesszemű-légy"}
Stilbium cyanurum	{"nagy smaragdfémdarázs"}
Stylurus flavipes	{"sárgás szitakötő"}
Theodoxus danubialis danubialis	{"rajzos bödöncsiga"}
Trichia unidentata	{"egyfogú szőrőscsiga"}
Valvata naticina	{"kúpos kerekszájúcsiga"}
Vespa velutina nigrithorax	{"ázsiai lódarázs"}
Agaricus bohusii	{"csoportos csiperke"}
Amanita caesarea	{császárgalóca}
Amanita lepiotoides	{"húsbarna galóca"}
Amanita vittadinii	{őzlábgalóca}
Battarrea phalloides	{álszömörcsög}
Boletus dupainii	{"kárminvörös tinóru"}
Cantharellus melanoxeros	{"sötétedőhúsú rókagomba"}
Cetraria aculeata	{"tüskés vértecs"}
Cetraria islandica	{"izlandi zuzmó"}
Cladonia arbuscula	{"erdei rénzuzmó"}
Cladonia magyarica	{"magyar tölcsérzuzmó"}
Cladonia mitis	{"szelíd rénzuzmó"}
Cladonia rangiferina	{"valódi rénzuzmó"}
Cortinarius (Phl.) paracephalixus	{nyárfa-pókhálósgomba}
Cortinarius (Phl.) praestans	{"óriás pókhálósgomba"}
Disciotis venosa	{"ráncos tárcsagomba"}
Entoloma porphyrophaeum	{"lilásbarna döggomba"}
Flammulina ononidis	{"iglice-fülőke, mezei télifülőke"}
Ganoderma cupreolaccatum	{"rézvörös lakkos tapló"}
Grifola frondosa	{"ágas tapló"}
Hapalopilus croceus	{"sáfrányszínű likacsgomba"}
Hericium cirrhatum	{"tüskés sörénygomba"}
Hygrocybe punicea	{"vérvörös nedűgomba"}
Hygrophorus marzuolus	{"tavaszi csigagomba"}
Hygrophorus poetarum	{"izabellvöröses csigagomba"}
Leucopaxillus compactus	{"háromszínű álpereszke"}
Leucopaxillus lepistoides	{tejpereszke}
Lobaria pulmonaria	{tüdőzuzmó}
Lycoperdon mammiforme	{"cafatos pöfeteg"}
Peltigera leucophlebia	{"változó ebzuzmó"}
Phellodon niger	{"fekete gereben, fekete szagosgereben"}
Pholiota sqarrosoides	{"fakópikkelyes tőkegomba"}
Pluteus umbrosus	{"pelyhes csengettyűgomba"}
Polyporus rhizophilus	{"gyepi likacsosgomba"}
Porphyrellus porphyrosporus	{"sötét tinóru"}
Sarcodon joeides	{"lilahúsú gereben"}
Solorina saccata	{"pettyegetett tárcsalapony"}
Tulostoma volvulatum	{"bocskoros nyelespöfeteg"}
Umbilicaria deusta	{"korpás csigalapony"}
Umbilicaria hirsuta	{"bozontos csigalapony"}
Umbilicaria polyphylla	{"soklombú csigalapony"}
Usnea florida	{"virágos szakállzuzmó"}
Xanthoparmelia pokornyi	{Pokorny-bodrány}
Xanthoparmelia pseudohungarica	{"magyar bodrány"}
Xanthoparmelia pulvinaris	{"magyar bodrány"}
Xanthoparmelia ryssolea	{"homoki bodrány"}
Xanthoparmelia subdiffluens	{"terülékeny bodrány"}
Alosa immaculata	{"dunai nagyhering"}
Carassius gibelio auratus	{ezüstkárász}
Eudontomyzon vladykovi	{Vladykov-ingola}
Gobio kesslerii	{"homoki küllő"}
Romanogobio albipinnatus	{"halványfoltú küllő"}
Dolichophis caspius	{"haragos sikló"}
Lacerta agilis argus	{"fürge gyík"}
Lacerta sp.	{gyík-fajok}
Natrix sp.	{sikló-fajok}
Podarcis sp.	{gyík-fajok}
Reptilia	{hüllők}
Trachemys scripta scripta	{"sárgafülű ékszerteknős"}
Vipera sp.	{vipera-fajok}
Zamenis longissimus	{"erdei sikló"}
Amphibia	{kétéltűek}
Anura	{békák}
Bombina sp.	{unka-fajok}
Bufo sp.	{varangy-fajok}
Lissotriton sp.	{gőte-fajok}
Lissotriton vulgaris	{"pettyes gőte"}
Lithobates catesbeianus	{"amerikai ökörbéka"}
Mesotriton alpestris	{"alpesi gőte"}
Pelophylax kl. esculentus	{kecskebéka}
Pelophylax lessonae	{"kis tavibéka"}
Pelophylax ridibundus	{"kacagó béka (tavibéka)"}
Pelophylax sp.	{béka-fajok}
Rana catesbeianus	{"amerikai ökörbéka"}
Rana sp.	{béka-fajok}
Triturus montandoni	{"kárpáti gőte"}
Triturus sp.	{gőte-fajok}
Alopochen aegyptiaca	{"nílusi lúd"}
Lyrurus tetrix	{nyírfajd}
Alopochen aegyptiacus 	{"nílusi lúd"}
Aquila fasciata	{héjasas}
Aquila pennata	{törpesas}
Ardea alba	{"nagy kócsag"}
Bubo scandiacus	{hóbagoly}
Cecropis daurica	{"vörhenyes fecske"}
Cercotrichas galactotes	{tüskebujkáló}
Hydroprogne caspia	{lócsér}
Ichthyaetus ichthyaetus	{halászsirály}
Ichthyaetus melanocephalus	{szerecsensirály}
Larus audouinii	{korallsirály}
Larus fuscus graellsii	{heringsirály}
Leiopicus medius	{középfakopáncs}
Lophophanes cristatus	{"búbos cinege"}
Microcarbo pygmeus	{"kis kárókatona"}
Pastor roseus	{pásztormadár}
Periparus ater	{fenyvescinege}
Phalaropus sp.	{víztaposó}
Pluvialis dominicus	{"amerikai pettyeslile"}
Podiceps ruficollis	{"kis vöcsök"}
Poecile montanus	{"kormosfejű cinege"}
Poecile palustris	{barátcinege}
Regulus ignicapilla	{"tüzesfejű királyka"}
Sternula albifrons	{"kis csér"}
Tetrastes bonasia	{császármadár}
Threskiornis aethiopicus	{"szent íbisz"}
Tringa hypoleucos	{billegetőcankó}
Vanellus gregarius	{lilebíbic}
Vanellus leucurus	{"fehérfarkú lilebíbic"}
Vanellus spinosus	{"tüskés bíbic"}
Xema sabini	{fecskesirály}
Aconitum sp.	{sisakvirág-faj}
Adonis volgensis	{"volgamenti hérics"}
Adonis x hybrida	{"volgamenti hérics"}
Alchemilla sp.	{palástfű}
Allium paniculatum ssp. marginatum	{"bugás hagyma"}
Alternanthera philoxeroides	{aligátorfű}
Anacamptis coriophora	{"poloskaszagú kosbor (poloskaszagú sisakoskosbor)"}
Anacamptis palustris ssp. elegans	{"pompás kosbor (pompás sisakoskosbor)"}
Anacamptis palustris ssp. palustris	{"mocsári kosbor (mocsári sisakoskosbor)"}
Anthriscus nitidus	{"havasi turbolya"}
Apamea syriaca	{Tallós-dudvabagoly}
Asperula taurina	{"olasz müge"}
Asplenium javorkeanum	{"magyar pikkelypáfrány"}
Aster sedifolius ssp. canus	{"réti őszirózsa (pettyegetett őszirózsa)"}
Aster sedifolius ssp. sedifolius	{"réti őszirózsa (pettyegetett őszirózsa)"}
Aster x versicolor	{"tarka őszirózsa"}
Astragalus vesicarius ssp. albidus	{"fehéres csüdfű"}
Avena sterilis ssp. ludoviciana	{"magas zab"}
Baccharis halimifolia	{"borfa,  tengerparti seprűcserje"}
Botrychium virginianum	{"virginiai holdruta"}
Brachydontium trichodes	{"szőrszerű pintycsőrfogúmoha"}
Brachytecium geheebii	{Geheeb-pintycsőrűmoha}
Brachytecium oxycladum	{"hegyeságú pintycsőrűmoha"}
Bulbocodium vernum	{egyhajúvirág}
Cabomba caroliniana	{"karolinai tündérhínár"}
Calamagrostis phragmitoides	{"pirosló nádtippan"}
Campylium vernicosus	{"karcsú pásztorbotmoha"}
Carduus crassifolius ssp. glaucus	{"szürke bogáncs"}
Carex depressa ssp. transsilvanica	{"erdélyi sás"}
Centaurea arenaria ssp. tauscheri	{"homoki imola alfaj"}
Centaurea montana ssp. mollis	{"szirti imola"}
Centaurea scabiosa ssp. sadleriana	{"budai imola"}
Centaurea triumfettii	{"tarka imola"}
Centaurea triumfettii ssp. aligera	{"tarka imola"}
Cephalanthera alba	{"fehér madársisak"}
Cephalanthera sp.	{madársisak-fajok}
Cerastium arvense ssp. matrense	{"mátrai madárhúr"}
Cerastium arvense ssp. molle	{"mátrai madárhúr"}
Ceratophyllum tanaiticum	{"donvidéki tócsagaz"}
Ceterach javorkeanum	{"magyar pikkelypáfrány"}
Cetraria aculeata  	{"tüskés vértecs"}
Chamaecytisus hirsutus ssp. ciliatus	{"pillás törpezanót"}
Chamaecytisus rochelii	{Rochel-törpezanót}
Chamaenerion dodonaei	{"vízparti deréce"}
Chlorocrepis staticifolia	{"keskenylevelű hölgymál"}
Cladonia spp. (subgenus Cladinia)	{"tölcsérzuzmó fajok"}
Cotoneaster sp.	{madárbirs-fajok}
Crataegus x degenii	{"fekete galagonya hibrid"}
Cyperus esculentus var. leptostachyus	{mandulapalka}
Dactylorhiza incarnata ssp. haematodes	{"hússzínű ujjaskosbor"}
Dactylorhiza incarnata ssp. incarnata	{"hússzínű ujjaskosbor"}
Dactylorhiza incarnata ssp. ochroleuca	{"halvány ujjaskosbor"}
Dactylorhiza incarnata ssp. serotina	{"hússzínű ujjaskosbor"}
Jurinea sp.	{hangyabogáncs-fajok}
Dactylorhiza maculata ssp. transylvanica	{"foltos ujjaskosbor"}
Dactylorhiza viridis	{"zöldike (zöldike ujjaskosbor)"}
Daphne sp.	{boroszlán}
Dianthus arenarius	{"balti szegfű"}
Dianthus arenarius ssp. borussicus	{"balti szegfű"}
Dianthus giganteiformis ssp. pontederae	{"nagy szegfű"}
Dianthus plumarius ssp. lumnitzeri	{"Lumnitzer-szegfű (incl. István király-szegfű)"}
Dianthus plumarius ssp. praecox	{"korai szegfű"}
Drepanocladus cossonii	{"körkörös sarlósmoha"}
Eichhornia crassipes	{vízijácint}
Elymus elongatus	{"magas tarackbúza"}
Epipactis atrorubens ssp. borbasii	{"vörösbarna nőszőfű (incl. Borbás nőszőfű)"}
Epipactis exilis	{"karcsú nőszőfű"}
Epipactis mecsekensis	{"mecseki nőszőfű"}
Epipactis moravica	{"morva nőszőfű"}
Epipactis neglecta	{"keskenyajkú nőszőfű"}
Epipactis peitzii	{Peitz-nőszőfű}
Epipactis pseudopurpurata	{nőszőfű-faj}
Epipactis sp.	{nőszőfű-fajok}
Erigeron annuus	{"egynyári seprence"}
Erysimum witmannii ssp. pallidiflorum	{"halványsárga repcsény"}
Erysimum wittmannii	{"halványsárga repcsény"}
Fallopia sp.	{japánkeserűfű}
Festuca pallens ssp. pannonica	{"deres csenkesz"}
Gentianella amarella	{"csinos tárnicska"}
Gunnera manicata	{"brazil óriáslapu"}
Gunnera tinctoria	{"óriás rebarbara"}
Gymnadenia densiflora	{"sűrűvirágú bibircsvirág"}
Gymnocarpium sp.	{tölgyespáfrány-fajok}
Gypsophila arenaria	{"homoki fátyolvirág"}
Gypsophila fastigiata	{"homoki fátyolvirág"}
Gypsophila fastigiata ssp. arenaria	{"homoki fátyolvirág alfaj"}
Hamatocaulis vernicosus	{"karcsú pásztorbotmoha"}
Helianthus ×laetiflorus	{"kései napraforgó"}
Helianthus pauciflorus	{"merevlevelű napraforgó"}
Helianthus sp.	{napraforgó}
Hesperis matronalis ssp. vrabelyiana	{Vrabély-estike}
Hesperis vrabelyiana	{Vrabély-estike}
Himantoglossum jankae	{"bíboros/Janka sallangvirág"}
Hippophae rhamnoides	{"homoktövis (európai homoktövis)"}
Hippophaë rhamnoides	{"homoktövis (európai homoktövis)"}
Humulus japonicus	{"japán komló"}
Hydrocotyle ranunculoides	{"hévízi gázló"}
Iris aphylla ssp. hungarica	{"magyar nőszirom"}
Iris humilis ssp. arenaria	{"homoki nőszirom"}
Jovibarba globifera	{sárga-kövirózsa}
Jovibarba globifera ssp. globifera	{sárga-kövirózsa}
Jovibarba globifera ssp. hirta	{sárga-kövirózsa}
Jovibarba sp.	{kövirózsa-fajok}
Knautia arvensis ssp. kitaibelii	{Kitaibel-varfű}
Knautia kitaibelii ssp. tomentella	{Kitaibel-varfű}
Knautia maxima	{"erdei varfű"}
Lagarosiphon major	{"fodros átokhínár"}
Lathyrus lacteus	{"koloncos lednek"}
Lathyrus pannonicus ssp. collinus	{"koloncos lednek"}
Lathyrus transsylvanicus	{"erdélyi lednek"}
Lilium bulbiferum ssp. bulbiferum	{"tüzes liliom"}
Linaria biebersteinii	{Bieberstein-gyújtoványfű}
Linum hirsutum ssp. glabrescens	{"borzas len"}
Linum hirsutum ssp. hirsutum	{"borzas len"}
Ludwigia grandiflora	{"nagyvirágú tóalma"}
Ludwigia peploides	{"sárgavirágú tóalma"}
Lycopodium complanatum	{"közönséges laposkorpafű"}
Lycopodium issleri	{Issler-laposkorpafű}
Lycopodium spp.	{korpafű}
Lysichiton americanus	{"sárga lápbuzogány"}
Microstegium vimineum	{"japán gázlófű"}
Minuartia hirsuta	{"magyar kőhúr"}
Monochoria korsakowii	{"kék rizsjácint"}
Montia fontana ssp. chondrosperma	{"kis forrásfű"}
Myosotis laxa ssp. caespitosa	{"gyepes nefelejcs"}
Myriophyllum aquaticum	{"közönséges süllőhínár"}
Myriophyllum heterophyllum	{"felemáslevelű süllőhínár"}
Narcissus poeticus ssp. radiiflorus	{"csillagos nárcisz"}
Narcissus radiiflorus	{"csillagos nárcisz"}
Neotinea tridentata	{"tarka kosbor (tarka pettyeskosbor)"}
Neotinea ustulata	{"sömörös kosbor (sömörös pettyeskosbor)"}
Neotinea ustulata ssp. aestivalis	{"sömörös kosbor (sömörös pettyeskosbor)"}
Neotinea ustulata ssp. ustulata	{"sömörös kosbor (sömörös pettyeskosbor)"}
Neotinea x dietrichiana	{"hibrid pettyeskosbor (tarka és sömörös kosbor hibrid)"}
Neottia ovata	{békakonty}
Onosma arenaria ssp. tuberculata	{"homoki vértő"}
Onosma sp.	{vértő-fajok}
Ophrys bertolonii	{Bertoloni-bangó}
Ophrys fuciflora ssp. fuciflora	{poszméhbangó}
Ophrys fuciflora ssp. holubyana	{Holuby-bangó}
Ophrys holoserica	{poszméhbangó}
Ophrys oestrifera	{"szarvas bangó"}
Ophrys scolopax	{"szarvas bangó"}
Ophrys scolopax ssp. cornuta	{"szarvas bangó"}
Ophrys sp.	{bangó-fajok}
Opuntia fragilis	{"törékeny fügekaktusz"}
Orchidaceae	{orchidea-faj}
Orchis laxiflora	{"mocsári kosbor (mocsári sisakoskosbor)"}
Orchis laxiflora ssp. elegans	{"pompás kosbor (pompás sisakoskosbor)"}
Orchis laxiflora ssp. palustris	{"mocsári kosbor (mocsári sisakoskosbor)"}
Orchis mascula	{"füles kosbor"}
Orchis mascula ssp. signifera	{"füles kosbor"}
Orchis palustris	{"mocsári kosbor (mocsári sisakoskosbor)"}
Orchis sp.	{kosbor-fajok}
Ornithogalum brevistylum	{"nyúlánk sárma"}
Ornithogalum pannonicum	{"üstökös sárma"}
Orobanche artemisiae-campestris	{"üröm-vajvirág (ürömszádor)"}
Orobanche coerulescens	{"kékes vajvirág (kékes szádor)"}
Oxytropis pilosa	{"borzas csajkavirág"}
Oxytropis pilosa ssp. hungarica	{"borzas csajkavirág"}
Paeonia officinalis ssp. banatica	{"bánáti bazsarózsa"}
Panicum miliaceum ssp. ruderale	{"törékeny köles"}
Panicum sp.	{köles}
Parthenium hysterophorus	{"keserű hamisüröm"}
Paulownia sp.	{császárfa}
Pennisetum setaceum	{tollborzfű}
Persicaria perfoliata	{"ördögfarok keserűfű"}
Petrosimonia triandra	{szikárszik}
Phascum floekeanum	{"vörösbarna rügymoha"}
Pistia stratiotes	{"úszó kagylótutaj"}
Platanthera sp.	{sarkvirág-fajok}
Poa pannonica ssp. glabra	{"hegyi perje"}
Polystichum sp.	{vesepáfrány-fajok}
Potentilla indica	{indiaiszamóca}
Primula auricula	{"cifra kankalin (medvefül kankalin)"}
Primula farinosa	{"lisztes kankalin"}
Primula nutans	{"bókoló kankalin"}
Primula x brevistyla	{"szártalan/tavaszi kankalin hibrid"}
Prospero paratheticum	{"őszi csillagvirágok"}
Prunella x dissecta	{"nagyvirágú/fehér gyíkfű hibrid"}
Pseudolysimachion spurium ssp. foliosum	{"bugás fürtösveronika alfaj"}
Puccinellia festuciformis ssp. intermedia	{"fertőtavi mézpázsit"}
Pueraria lobata	{"kudzu nyílgyökér"}
Pueraria montana	{"kudzu nyílgyökér"}
Pueraria montana var. lobata	{"kudzu nyílgyökér"}
Pulsatilla flavescens	{"magyar kökörcsin"}
Pulsatilla montana	{"hegyi kökörcsin"}
Pulsatilla nigricans	{"fekete kökörcsin"}
Pulsatilla pratensis ssp. hungarica	{"magyar kökörcsin"}
Pulsatilla pratensis ssp. nigricans	{"fekete kökörcsin"}
Pulsatilla sp.	{kökörcsin-fajok}
Pulsatilla zimmermannii	{"hegyi kökörcsin"}
Pyrola sp.	{körtike-fajok}
Reynoutria sp.	{japánkeserűfű}
Reynoutria x bohemica	{"cseh óriáskeserűfű"}
Rhynchostegiella jacquinii	{"mattzöld kiscsőrűmoha"}
Rhynchostegium rotundifolium	{"kereklevelű hosszúcsőrűmoha"}
Salix elaeagnos	{"parti fűz"}
Scilla autumnalis agg.	{"őszi csillagvirágok"}
Scilla bifolia agg.	{"tavaszi csillagvirág-fajok"}
Sedum acre ssp. neglectum	{"adriai varjúháj"}
Sedum hillebrandtii	{"homoki varjúháj"}
Sedum urvillei ssp. hillebrandtii	{"homoki varjúháj"}
Sempervivum marmoreum auct. hung.	{"mátrai kövirózsa (rózsás kövirózsa)"}
Sempervivum matricum	{"mátrai kövirózsa (rózsás kövirózsa)"}
Sempervivum sp.	{kövirózsa-fajok}
Sesleria heufleriana ssp. hungarica	{"magyar nyúlfarkfű"}
Sesleria sp.	{nyúlfarkfű-fajok}
Sium sisarum	{"keleti békakorsó"}
Solidago sp.	{aranyvessző}
Sorbus × kitaibeliana	{"dunai/barkócaberkenye hibrid"}
Sorbus acutiserratus	{"kőhányási berkenye"}
Sorbus andreanszkyana	{Andreánszky-berkenye}
Sorbus austriaca	{"Hazslinszky-berkenye (osztrák berkenye alakköre)"}
Sorbus barabitsii	{Barabits-berkenye}
Sorbus bodajkensis	{"bodajki berkenye"}
Sorbus buekkensis	{"bükki berkenye"}
Sorbus cretica	{"déli berkenye"}
Sorbus dracofolius	{"gánti berkenye"}
Sorbus gayerana	{Gáyer-berkenye}
Sorbus gerecseensis	{"gerecsei berkenye"}
Sorbus hazslinszkyana	{"Hazslinszky-berkenye (osztrák berkenye alakköre)"}
Sorbus huljakii	{Hulják-berkenye}
Sorbus javorkae	{Jávorka-berkenye}
Sorbus karpatii	{kárpáti-berkenye}
Sorbus majeri	{Májer-berkenye}
Sorbus pseudobakonyensis	{"rövidkaréjú berkenye"}
Sorbus pseudovertesensis	{"csákberényi berkenye"}
Sorbus redliana	{Rédl-berkenye}
Sorbus sooi	{Soó-berkenye}
Sorbus tobani	{Tobán-berkenye}
Sorbus ulmifolia	{"szillevelű berkenye"}
Sorbus vallerubusensis	{"szedresvölgyi berkenye"}
Sorbus vertesensis	{"vértesi berkenye"}
Sorbus veszpremensis	{"veszprémi berkenye"}
Sorbus x buekkensis	{"bükki berkenye"}
Sorbus zolyomii	{Zólyomi-berkenye}
Sparganium natans	{"lápi békabuzogány"}
Sphagnum russowi	{óriás-tőzegmoha}
Sphagnum sp.	{"tőzegmoha fajok"}
Sphagnum spp.	{"tőzegmoha fajok"}
Stipa stenophylla	{"hosszúlevelű árvalányhaj"}
Thalictrum minus ssp. pseudominus	{"kékes borkóró"}
Thalictrum pseudominus	{"kékes borkóró"}
Thlaspi hungaricum	{"Janka-tarsóka (incl. magyar tarsóka)"}
Thlaspi jankae agg.	{"Janka-tarsóka (incl. magyar tarsóka)"}
Thlaspi kovatsii	{Schudich-tarsóka}
Thlaspi kovatsii ssp. schudichii	{Schudich-tarsóka}
Torilis ucrainica	{"keleti tüskemag"}
Valeriana officinalis ssp. sambucifolia	{"bodzalevelű macskagyökér"}
Valeriana tripteris	{"hármaslevelű macskagyökér"}
Vicia narbonensis	{"fogaslevelű bükköny"}
Vitis riparia	{"parti szőlő"}
Weisia rostellata	{"kiscsőrű gyöngymoha"}
Xanthium albinum ssp. riparium	{"elbai szerbtövis"}
Xanthium sp.	{szerbtövis}
Sialis nigripes	{"fekete vízifátyolka"}
Fallopia x hybrida	{"japánkeserűfű hibrid"}
Phytolacca acinosa	{"kínai alkörmös"}
Anacamptis morio	{"agár kosbor (agár sisakoskosbor)"}
Carabus ulrichii	{"rezes futrinka"}
Metcalfa pruinosa	{"amerikai lepkekabóca"}
Eresus sandaliatus	{bikapók}
Eresus hermani	{bikapók}
Eresus sp.	{"bikapók faj"}
Ludwigia repens	{"pirosfonákú tóalma"}
Phyllostachys bambusoides	{bambusz}
Hygrophila polysperma	{"indiai vízicsillag"}
Gaillardia sp.	{kokárdavirág}
Shinnersia rivularis	{"mexikói tölgy"}
Cherax quadricarinatus	{"ausztrál vörösollós rák"}
Xiphophorus sp.	{platti}
Xiphophorus maculatus	{"széleshátú fogasponty"}
Proterorhinus semilunaris	{"tarka géb "}
Opuntia phaeacantha	{"coloradói fügekaktusz"}
Opuntia sp.	{fügekaktusz}
Ablepharus kitaibelii	{"pannon gyík (magyar gyík)"}
Phytolacca sp.	{"alkörmös faj"}
Linaria cannabina	{kenderike}
Opuntia humifusa	{ördögnyelv-fügekaktusz}
Graptemys kohni	{"mississippi tarajosteknős"}
Limnobium laevigatum	{"amasonasi békatutaj"}
Acridotheres tristis	{pásztormejnó}
Polygonum perfoliatum	{ördögfarok-keserûfû}
Salvinia molesta	{"átellenes rucaöröm"}
Ehrharta calycina	{ }
Pelophylax esculentus	{kecskebéka}
Dendrocygna bicolor	{"sujtásos fütyülőlúd"}
Anas discors	{"kékszárnyú réce"}
Testudo hermanni	{"görög teknős"}
Leuciscus aspius	{balin}
Dianthus plumarius ssp. regis-stephani	{"Lumnitzer-szegfű (incl. István király-szegfű)"}
Carassius auratus auratus	{ezüstkárász}
Maculinea alcon alcon	{"szürkés hangyaboglárka"}
\.


--
-- Name: ssp_magyarnevek ssp_magyarnevek_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_magyarnevek
    ADD CONSTRAINT ssp_magyarnevek_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_magyarnevek ssp_magyarnevek_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_magyarnevek
    ADD CONSTRAINT ssp_magyarnevek_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

