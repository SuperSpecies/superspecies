\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_odonata_hun DROP CONSTRAINT ssp_odonata_hun_species_name_fkey;
--ALTER TABLE ONLY public.ssp_odonata_hun DROP CONSTRAINT ssp_odonata_hun_pkey;
--DROP TABLE public.ssp_odonata_hun;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_odonata_hun; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_odonata_hun (
    species_name character varying(64) NOT NULL,
    status character varying(12) NOT NULL
);


ALTER TABLE public.ssp_odonata_hun OWNER TO ssp_admin;

--
-- Data for Name: ssp_odonata_hun; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_odonata_hun (species_name, status) FROM stdin;
Aeshna affinis	elfogadott
Aeshna cyanea	elfogadott
Aeshna isoceles	szinonim
Aeshna mixta	elfogadott
Anaciaeschna isosceles	szinonim
Anax ephippiger	elfogadott
Anax imperator	elfogadott
Anax parthenope	elfogadott
Brachytron pratense	elfogadott
Calopteryx splendens	elfogadott
Calopteryx virgo	elfogadott
Chalcolestes parvidens	elfogadott
Chalcolestes viridis	elfogadott
Chalcolestes viridis/parvidens	elfogadott
Coenagrion ornatum	elfogadott
Coenagrion puella	elfogadott
Coenagrion pulchellum	elfogadott
Coenagrion pulchellum interruptum	szinonim
Coenagrion scitulum	elfogadott
Cordulegaster bidentata	elfogadott
Cordulegaster heros	elfogadott
Cordulia aenea	elfogadott
Crocothemis erythraea	elfogadott
Enallagma cyathigerum	elfogadott
Epitheca bimaculata	elfogadott
Erythromma najas	elfogadott
Erythromma viridulum	elfogadott
Erythromma lindenii	elfogadott
Gomphus flavipes	elfogadott
Gomphus vulgatissimus	elfogadott
Hemianax ephippiger	szinonim
Ischnura elegans	elfogadott
Ischnura elegans pontica	szinonim
Ischnura pumilio	elfogadott
Lestes barbarus	elfogadott
Lestes dryas	elfogadott
Lestes macrostigma	elfogadott
Lestes sponsa	elfogadott
Lestes virens	elfogadott
Lestes virens vestalis	szinonim
Lestes viridis	szinonim
Leucorrhinia caudalis	elfogadott
Leucorrhinia pectoralis	elfogadott
Libellula depressa	elfogadott
Libellula fulva	elfogadott
Libellula quadrimaculata	elfogadott
Onychogomphus forcipatus	elfogadott
Ophiogomphus cecilia	elfogadott
Orthetrum albistylum	elfogadott
Orthetrum brunneum	elfogadott
Orthetrum cancellatum	elfogadott
Orthetrum coerulescens	elfogadott
Platycnemis pennipes	elfogadott
Pyrrhosoma nymphula	elfogadott
Pyrrhosoma nymphula interposita	szinonim
Somatochlora flavomaculata	elfogadott
Somatochlora metallica	elfogadott
Stylurus flavipes	szinonim
Sympecma fusca	elfogadott
Sympetrum danae	elfogadott
Sympetrum depressiusculum	elfogadott
Sympetrum flaveolum	elfogadott
Sympetrum fonscolombii	elfogadott
Sympetrum meridionale	elfogadott
Sympetrum sanguineum	elfogadott
Sympetrum striolatum	elfogadott
Sympetrum vulgatum	elfogadott
\.


--
-- Name: ssp_odonata_hun ssp_odonata_hun_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_odonata_hun
    ADD CONSTRAINT ssp_odonata_hun_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_odonata_hun ssp_odonata_hun_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_odonata_hun
    ADD CONSTRAINT ssp_odonata_hun_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

