\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_auctor DROP CONSTRAINT ssp_auctor_species_name_fkey;
--ALTER TABLE ONLY public.ssp_auctor DROP CONSTRAINT ssp_auctor_pkey;
--DROP TABLE public.ssp_auctor;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_auctor; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_auctor (
    species_name character varying(64) NOT NULL,
    auctor character varying(64) NOT NULL
);


ALTER TABLE public.ssp_auctor OWNER TO ssp_admin;

--
-- Data for Name: ssp_auctor; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_auctor (species_name, auctor) FROM stdin;
Acanthopsyche siederi	Szöcs, 1961
Acanthosoma haemorrhoidale	(Linnaeus, 1758)
Acartauchenius scurrilis	(O.P.-Cambridge, 1872)
Acentrotypus brunnipes	(Boheman, 1839)
Acetropis carinata	(Herrich-Schäffer, 1842)
Acetropis longirostris	(Puton, 1875)
Parasteatoda lunata	(Clerck, 1757)
Cryptachaea riparia	(Blackwall, 1834)
Parasteatoda simulans	(Thorell, 1875)
Parasteatoda tepidariorum	(C.L. Koch, 1841)
Achenium ephippium	Erichson, 1840
Achenium humile	(Nicolai, 1822)
Acherontia atropos	(Linnaeus, 1758)
Acheta domesticus	Linne, 1758
Achroia grisella	(Fabricius, 1794)
Achtheres percarum	Nordmann, 1832
Achlya flavicornis	(Linnaeus, 1758)
Achyra nudalis	(Hübner, 1796)
Acidota crenata	(Fabricius, 1792)
Acidota cruentata	Mannerheim, 1830
Acilius canaliculatus	(Nicolai, 1822)
Acasis appensata	(Eversmann, 1842)
Acasis viretata	(Hübner, 1799)
Accipiter brevipes	(Severtzov, 1850)
Accipiter gentilis	(Linnaeus, 1758)
Accipiter gentilis buteoides	(Linnaeus, 1758)
Accipiter nisus	(Linnaeus, 1758)
Acentria ephemerella	(Denis & Schiffermüller, 1775)
Adalia decempunctata	(Linnaeus, 1758)
Adarrus multinotatus	(Boheman, 1847)
Adela croesella	(Scopoli, 1763)
Adela cuprella	(Denis & Schiffermüller, 1775)
Adela mazzolella	(Hübner, 1801)
Adela paludicolella	(Zeller, 1850)
Adela reaumurella	(Linnaeus, 1758)
Adela violella	(Denis & Schiffermüller, 1775)
Lacon punctatus	(Herbst, 1779)
Lacon querceus	(Herbst, 1784)
Adelphocoris lineolatus	(Goeze, 1778)
Adelphocoris quadripunctatus	(Fabricius, 1794)
Adelphocoris reichelii	(Fieber, 1836)
Adelphocoris seticornis	(Fabricius, 1775)
Adelphocoris ticinensis	(Meyer-Dür, 1843)
Adelphocoris vandalicus	(Rossi, 1790)
Aderus populneus	(Creutzer, 1796)
Adexius scrobipennis	Gyllenhal, 1834
Adicella balcanica	Botosaneanu & Novák, 1965
Adicella filicornis	(Pictet, 1834)
Adicella reducta	(McLachlan, 1865)
Adicella syriaca	Ulmer, 1907
Adomerus biguttatus	(Linnaeus, 1758)
Adosomus roridus	(Pallas, 1781)
Adoxophyes orana	(Fischer v. Röslerstamm, 1834)
Adrastus axillaris	Erichson, 1841
Adrastus kryshtali	Dolin, 1988
Adrastus lacertosus	Erichson, 1841
Adrastus limbatus	(Fabricius, 1776)
Adrastus montanus	(Scopoli, 1763)
Adrastus pallens	(Fabricius, 1792)
Adscita geryon	(Hübner, 1813)
Adscita statices	(Linnaeus, 1758)
Aedes cinereus	Meigen, 1818
Aedes rossicus	Dolb., Gor. & Mitr., 1930
Aedes vexans	(Meigen, 1830)
Aedia funesta	(Esper, 1786)
Aedia leucomelas	(Linnaeus, 1758)
Aegithalos caudatus	(Linnaeus, 1758)
Aegle kaekeritziana	(Hübner, 1799)
Aegolius funereus	(Linnaeus, 1758)
Aegomorphus clavipes	(Schrank, 1781)
Aegopinella minor	(Stabile, 1864)
Aegopinella nitens	(Michaud, 1831)
Aegopinella pura	(Alder, 1830)
Aegopinella ressmanni	(Westerlund, 1883)
Aegopis verticillus	(Lamarck, 1822)
Aegosoma scabricorne	(Scopoli, 1763)
Aegypius monachus	(Linnaeus, 1766)
Aeletes atomarius	(Aubé, 1842)
Aeletes hopffgarteni	(Reitter, 1878)
Aelia acuminata	(Linnaeus, 1758)
Aelia klugii	Hahn, 1833
Aelia rostrata	Boheman, 1852
Aellopus atratus	(Goeze, 1778)
Aelurillus m-nigrum	(Kulczynski, 1891)
Aelurillus v-insignitus	(Clerck, 1757)
Aesalus scarabaeoides	(Panzer, 1794)
Aeshna affinis	Van Der Linden, 1820
Aeshna cyanea	(Müller, 1764)
Aeshna grandis	(Linné, 1758)
Aeshna juncea	(Linné, 1758)
Acalles ptinoides	(Marsham, 1802)
Kyklioacalles roboris	Curtis, 1834
Acallocrates denticollis	(Germar, 1824)
Acalypta carinata	(Panzer, 1806)
Acalypta gracilis	(Fieber, 1844)
Acalypta marginata	(Wolff, 1804)
Acalypta musci	(Schrank, 1781)
Acalypta nigrina	(Fallén, 1807)
Acalypta parvula	(Fallén, 1807)
Acalypta platycheila	(Fieber, 1844)
Acalyptris loranthella	(Klimesch, 1937)
Acalyptus carpini	(Fabricius, 1792)
Acalyptus sericeus	Gyllenhal, 1836
Acanthaclisis occitanica	(Villers, 1789)
Acanthinula aculeata	(O.F. Müller 1774)
Acanthobodilus immundus	(Creutzer, 1799)
Acanthocinus aedilis	(Linnaeus, 1758)
Acanthocinus griseus	(Fabricius, 1792)
Abacoproeces saltuum	(L. Koch, 1872)
Abax carinatus	(Duftschmid, 1812)
Abax ovalis	(Duftschmid, 1812)
Abax parallelepipedus	(Piller et Mitterpacher, 1783)
Abax parallelus	(Duftschmid, 1812)
Abax schueppeli	(Palliardi, 1825)
Abax schueppeli rendschmidtii	(Germar, 1839)
Abdera affinis	(Paykull, 1799)
Abdera quadrifasciata	Curtis, 1829
Abdera triguttata	(Gyllenhal, 1810)
Abemus chloropterus	(Panzer, 1796)
Ablattaria laevigata	(Fabricius, 1775)
Ablepharus kitaibelii fitzingeri	Bibron & Bory, 1833
Abraeus granulum	Erichson, 1839
Abraeus perpusillus	(Marsham, 1802)
Abraeus roubali	Olexa, 1958
Abramis ballerus	(Linnaeus, 1758)
Blicca bjoerkna	(Linnaeus, 1758)
Abramis brama	(Linnaeus, 1758)
Abramis sapa	(Pallas, 1814)
Abraxas grossulariata	(Linnaeus, 1758)
Abrostola agnorista	Dufay, 1956
Abrostola asclepiadis	([Denis & Schiffermüller], 1775)
Abrostola tripartita	(Hufnagel, 1766)
Abrostola triplasia	(Linnaeus, 1758)
Absidia pilosa	(Paykull, 1798)
Absidia rufotestacea	(Letzner, 1845)
Acalles camelus	(Fabricius, 1792)
Acalles commutatus	Dieckmann, 1982
Acalles echinatus	(Germar, 1824)
Acalles papei	A. Solari & F. Solari, 1905
Acalles papei balcanicus	A. Solari & F. Solari, 1905
Acalles parvulus	Boheman, 1837
Acanthococcus aceris	Signoret,1875
Acanthococcus desertus	Matesova,1957
Acanthococcus devoniensis	(Green,1896)
Acanthococcus erinaceus	(Kiritchenko,1940)
Acanthococcus greeni	(Newstead,1898)
Acanthococcus munroi	Boratynski,1962
Acanthococcus roboris	(Goux,1931)
Acanthococcus uvaeursi	(Linnaeus,1761)
Acanthocyclops robustus	(Sars, 1863)
Acanthocyclops vernalis	(Fischer, 1853)
Acanthodelphax denticauda	(Boheman, 1847)
Acanthodelphax spinosus	(Fieber, 1866)
Acanthodiaptomus denticornis	(Wierzejski, 1887)
Acantholeberis curvirostris	(O.F. Müller, 1776)
Dichomeris alacella	(Zeller, 1839)
Acanthopsyche atra	(Linnaeus, 1767)
Acanthopsyche ecksteini	(Lederer, 1855)
Aeshna mixta	Latreille, 1805
Aeshna viridis	Eversmann, 1836
Acilius sulcatus	(Linnaeus, 1758)
Acinopus ammophilus	Dejean, 1829
Acinopus picipes	(Olivier, 1795)
Acipenser baeri	Brandt, 1869
Acipenser gueldenstaedtii	Brandt & Ratzeburg, 1833
Acipenser nudiventris	Lovetzky, 1828
Acipenser ruthenus	Linnaeus, 1758
Acipenser stellatus	Pallas, 1771
Aclerda subterranea	Signoret,1874
Acleris bergmanniana	(Linnaeus, 1758)
Acleris cristana	(Denis & Schiffermüller, 1775)
Acleris emargana	(Fabricius, 1775)
Acleris ferrugana	(Denis & Schiffermüller, 1775)
Acleris fimbriana	(Thunberg, 1791)
Acleris forsskaleana	(Linnaeus, 1758)
Acleris hastiana	(Linnaeus, 1775)
Acleris hippophaeana	(Heyden, 1865)
Acleris holmiana	(Linnaeus, 1758)
Acleris kochiella	(Goeze, 1783)
Acleris lipsiana	(Denis & Schiffermüller, 1775)
Acleris literana	(Linnaeus, 1758)
Acleris logiana	(Clerck, 1759)
Acleris lorquiniana	(Duponchel, 1835)
Acleris notana	(Donovan, 1806)
Acleris permutana	(Duponchel, 1836)
Acleris quercinana	(Zeller, 1849)
Acleris rhombana	(Denis & Schiffermüller, 1775)
Acleris roscidana	(Hübner, 1799)
Acleris rufana	(Denis & Schiffermüller, 1775)
Acleris scabrana	(Denis & Schiffermüller, 1775)
Acleris schalleriana	(Linnaeus, 1761)
Acleris shepherdana	(Stephens, 1852)
Acleris sparsana	(Denis & Schiffermüller, 1775)
Acleris umbrana	(Hübner, 1799)
Acleris variegana	(Denis & Schiffermüller, 1775)
Aclypea undata	(O.F. Müller, 1776)
Acmaeodera degener	(Scopoli, 1763)
Acmaeoderella flavofasciata	(Piller et Mitterpacher, 1783)
Acmaeoderella mimonti	(Boieldieu, 1865)
Acmaeops pratensis	(Laicharting, 1784)
Acompsia cinerella	(Clerck, 1759)
Acompsia tripunctella	(Denis & Schiffermüller, 1775)
Acompus pallipes	(Herrich-Schäffer, 1834)
Acompus rufipes	(Wolff, 1804)
Acontia lucida	(Hufnagel, 1766)
Aconurella prolixa	(Herrich-Schäffer, 1838)
Aconurella quadrum	(Herrich-Schäffer, 1838)
Acosmetia caliginosa	(Hübner, 1813)
Acrida ungarica	(Herbst, 1786)
Acritus homoeopathicus	Wollaston, 1857
Acritus minutus	(Herbst, 1792)
Acritus nigricornis	(Hoffmann, 1803)
Acrobasis consociella	(Hübner, 1813)
Acrobasis glaucella	Staudinger, 1859
Acrobasis obtusella	(Hübner, 1796)
Acrobasis sodalella	Zeller, 1848
Acrocephalus agricola	(Jerdon, 1845)
Acrocephalus agricola septima	(Jerdon, 1845)
Acrocephalus arundinaceus	(Linnaeus, 1758)
Acrocephalus melanopogon	(Temminck, 1823)
Acrocephalus paludicola	(Vieillot, 1817)
Acrocephalus palustris	(Bechstein, 1798)
Acrocephalus schoenobaenus	(Linnaeus, 1758)
Acrocephalus scirpaceus	(Hermann, 1804)
Acrocercops brongniardella	(Fabricius, 1798)
Acrochordum evae	Loksa, 1960
Acrolepia autumnitella	Curtis, 1838
Acrolepiopsis tauricella	(Staudinger, 1870)
Acrolepiopsis assectella	(Zeller, 1839)
Acrolocha minuta	(Olivier, 1795)
Acrolocha pliginskii	Bernhauer, 1912
Acroloxus lacustris	(Linnaeus, 1758)
Acronicta aceris	(Linnaeus, 1758)
Acronicta alni	(Linnaeus, 1767)
Acronicta auricoma	([Denis & Schiffermüller], 1775)
Acronicta cuspis	(Hübner, 1813)
Acronicta euphorbiae	([Denis & Schiffermüller], 1775)
Acronicta leporina	(Linnaeus, 1758)
Acronicta megacephala	([Denis & Schiffermüller], 1775)
Acronicta psi	(Linnaeus, 1758)
Acronicta rumicis	(Linnaeus, 1758)
Acronicta strigosa	([Denis & Schiffermüller], 1775)
Acronicta tridens	([Denis & Schiffermüller], 1775)
Acroperus elongatus	(Sars, 1862)
Acroperus harpae	(Baird, 1835)
Acrossus depressus	(Kugelann, 1792)
Acrossus luridus	(Fabricius, 1775)
Acrossus rufipes	(Linnaeus, 1758)
Acrotona aterrima	(Gravenhorst, 1802)
Acrotona benicki	(Allen, 1940)
Acrotona convergens	(A.Strand, 1958)
Acrotona exigua	(Erichson, 1837)
Acrotona muscorum	(Ch.Brisout de Barneville, 1860)
Acrotona nigerrima	(Aubé, 1850)
Acrotona obfuscata	(Gravenhorst, 1802)
Acrotona parens	(Mulsant et Rey, 1852)
Acrotona parvula	(Mannerheim, 1830)
Acrotona piceorufa	(Mulsant et Rey, 1873)
Acrotona pygmaea	(Gravenhorst, 1802)
Acrotona troglodytes	(Motschulsky, 1858)
Acrotrichis arnoldi	Rosskothen, 1935
Acrotrichis atomaria	(De Geer, 1774)
Acrotrichis brevipennis	(Erichson, 1845)
Acrotrichis dispar	(Matthews, 1865)
Acrotrichis fascicularis	(Herbst, 1793)
Acrotrichis grandicollis	(Mannerheim, 1844)
Acrotrichis intermedia	(Gillmeister, 1845)
Acrotrichis montandoni	(Allibert, 1844)
Acrotrichis norvegica	Strand, 1941
Acrotrichis sericans	(Heer, 1841)
Acrotrichis sitkaensis	(Motschulsky, 1845)
Acrotrichis thoracica	(Waltl, 1838)
Acrotylus insubricus	(Scopoli, 1786)
Acrotylus longipes	(Charpentier, 1845)
Acrulia inflata	(Gyllenhal, 1813)
Actebia praecox	(Linnaeus, 1758)
Actenia brunnealis	(Treitschke, 1829)
Actenia honestalis	(Treitschke, 1829)
Actenicerus siaelandicus	(O. F. Müller, 1764)
Actidium boudieri	(Allibert, 1844)
Actinotia polyodon	(Clerck, 1759)
Actinotia radiosa	(Esper, 1804)
Actites hypoleucos	(Linnaeus, 1758)
Aculepeira armida	(Audouin, 1826)
Aculepeira ceropegia	(Walckenaer, 1802)
Acupalpus brunnipes	(Sturm, 1825)
Acupalpus dubius	Schilsky, 1888
Acupalpus elegans	Dejean, 1829
Acupalpus exiguus	Dejean, 1829
Acupalpus flavicollis	(Sturm, 1825)
Acupalpus interstitialis	Reitter, 1884
Acupalpus luteatus	(Duftschmid, 1812)
Acupalpus maculatus	(Schaum, 1860)
Acupalpus meridianus	(Linnaeus, 1761)
Acupalpus notatus	Mulsant et Rey, 1861
Acupalpus parvulus	(Sturm, 1825)
Acupalpus suturalis	Dejean, 1829
Acylophorus glaberrimus	(Herbst, 1784)
Adaina microdactyla	(Hübner, 1813)
Adalia bipunctata	(Linnaeus, 1758)
Adalia conglomerata	(Linnaeus, 1758)
Aethalura punctulata	(Denis & Schiffermüller, 1775)
Aethes beatricella	(Walsingham, 1898)
Aethes bilbaensis	(Rössler, 1877)
Aethes cnicana	(Westwood, 1854)
Aethes dilucidana	(Stephens, 1852)
Aethes flagellana	(Duponchel, 1836)
Aethes francillana	(Fabricius, 1794)
Aethes hartmanniana	(Clerck, 1758)
Aethes kindermanniana	(Treitschke, 1830)
Aethes margaritana	(Haworth, 1811)
Aethes margarotana	(Duponchel, 1836)
Aethes moribundana	(Staudinger, 1859)
Aethes nefandana	(Kennel, 1899)
Aethes piercei	(Obraztsov, 1952)
Aethes rubigana	(Treitschke, 1830)
Aethes rutilana	(Hübner, 1817)
Aethes sanguinana	(Treitschke, 1830)
Aethes smeathmanniana	(Fabricius, 1781)
Aethes tesserana	(Denis & Schiffermüller, 1775)
Aethes tornella	(Walsingham, 1898)
Aethes triangulana	(Treitschke, 1835)
Aethes vicinana	(Mann, 1859)
Aethes williana	(Brahm, 1791)
Byrsinus flavicornis	(Fabricius, 1794)
Agabus affinis	(Paykull, 1798)
Agabus biguttatus	(Olivier, 1795)
Agabus bipustulatus	(Linnaeus, 1767)
Agabus congener	(Thunberg, 1794)
Agabus conspersus	(Marsham, 1802)
Agabus fuscipennis	(Paykull, 1798)
Agabus guttatus	(Paykull, 1798)
Agabus labiatus	(Brahm, 1790)
Agabus melanarius	Aubé, 1836
Agabus nebulosus	(Forster, 1771)
Agabus paludosus	(Fabricius, 1801)
Agabus striolatus	(Gyllenhal, 1808)
Agabus uliginosus	(Linnaeus, 1761)
Agabus undulatus	(Schrank, 1776)
Agalenatea redii	(Scopoli, 1763)
Agallia brachyptera	(Boheman, 1847)
Agallia consobrina	Curtis, 1833
Agalmatium flavescens	(Olivier, 1791)
Agapanthia cardui	(Linnaeus, 1767)
Agapanthia cynarae	(Germar, 1817)
Agapanthia dahli	(Richter, 1820)
Agapanthia intermedia	Ganglbauer, 1884
Agapanthia kirbyi	(Gyllenhal, 1817)
Agapanthia maculicornis	(Gyllenhal, 1817)
Agapanthia osmanlis	Reiche, 1858
Agapanthia villosoviridescens	(De Geer, 1775)
Agapanthia violacea	(Fabricius, 1775)
Agapanthiola leucaspis	(Steven, 1817)
Agapeta hamana	(Linnaeus, 1758)
Agapeta largana	(Rebel, 1906)
Agapeta zoegana	(Linnaeus, 1767)
Agapetus delicatulus	McLachlan, 1884
Agapetus fuscipes	Curtis, 1834
Agapetus laniger	(Pictet, 1834)
Agapetus ochripes	Curtis, 1834
Agardhiella lamellata	(Clessin, 1887)
Agardhiella parreyssii	(L. Pfeiffer, 1848)
Agaricochara latissima	(Stephens, 1832)
Agaricophagus cephalotes	W.L.E. Schmidt, 1841
Agaricophagus reitteri	Ganglbauer, 1899
Agathidium arcticum	C.G. Thomson, 1862
Agathidium atrum	(Paykull, 1798)
Agathidium badium	Erichson, 1845
Agathidium banaticum	Reitter, 1885
Agathidium bohemicum	Reitter, 1885
Agathidium confusum	Brisout De Barneville, 1863
Agathidium discoideum	Erichson, 1845
Agathidium haemorrhoum	Erichson, 1845
Agathidium laevigatum	Erichson, 1845
Agathidium leonhardianum	Roubal, 1915
Agathidium mandibulare	Sturm, 1807
Agathidium marginatum	Sturm, 1807
Agathidium nigrinum	Sturm, 1807
Agathidium nigripenne	(Fabricius, 1792)
Agathidium nudum	Hampe, 1870
Agathidium pisanum	Brisout De Barneville, 1872
Agathidium plagiatum	(Gyllenhal, 1810)
Agathidium pseudopallidum	Hlisnikovsky, 1964
Agathidium seminulum	(Linnaeus, 1758)
Agathidium varians	Beck, 1817
Agdistis adactyla	(Hübner, 1819)
Agdistis heydeni	(Zeller, 1852)
Agdistis intermedia	Caradja, 1920
Agdistis tamaricis	(Zeller, 1847)
Allagelena gracilens	(C.L. Koch, 1841)
Agelena labyrinthica	(Clerck, 1757)
Agenioideus apicalis	(Vander Linden, 1827)
Agenioideus cinctellus	(Spinola, 1808)
Agenioideus nubecula	(Costa, 1874)
Agenioideus sericeus	(Vander Linden, 1827)
Agenioideus usurarius	(Tournier, 1889)
Aglais urticae	(Linnaeus, 1758)
Aglenus brunneus	(Gyllenhal, 1813)
Aglia tau	(Linnaeus, 1758)
Aglossa caprealis	(Hübner, 1809)
Aglossa pinguinalis	(Linnaeus, 1758)
Aglossa signicostalis	Staudinger, 1871
Agnathus decoratus	Germar, 1818
Agnetina elegantula	(Klapálek, 1905)
Agnocoris reclairei	E. Wagner, 1949
Agnocoris rubicundus	(Fallén, 1829)
Agoliinus nemoralis	(Erichson, 1848)
Agonopterix adspersella	(Kollar, 1832)
Agonopterix alstromeriana	(Clerck, 1759)
Agonopterix angelicella	(Hübner, 1813)
Agonopterix arenella	(Denis & Schiffermüller, 1775)
Agonopterix assimilella	(Treitschke, 1832)
Agonopterix astrantiae	(Heinemann, 1870)
Agonopterix atomella	(Denis & Schiffermüller, 1775)
Agonopterix capreolella	(Zeller, 1839)
Agonopterix carduella	(Hübner, 1817)
Agonopterix ciliella	(Stainton, 1849)
Agonopterix cnicella	(Treitschke, 1835)
Agonopterix curvipunctosa	(Haworth, 1811)
Agonopterix doronicella	(Wocke, 1849)
Agonopterix furvella	(Treitschke, 1832)
Agonopterix heracliana	(Linnaeus, 1758)
Agonopterix hippomarathri	(Nickerl, 1864)
Agonopterix kaekeritziana	(Linnaeus, 1767)
Agonopterix laterella	(Denis & Schiffermüller, 1775)
Agonopterix liturosa	(Haworth, 1811)
Agonopterix nanatella	(Stainton, 1849)
Agonopterix nervosa	(Haworth, 1811)
Agonopterix ocellana	(Fabricius, 1775)
Agonopterix oinochroa	(Turati, 1879)
Agonopterix pallorella	(Zeller, 1839)
Agonopterix parilella	(Treitschke, 1835)
Agonopterix petasitis	(Standfuss, 1851)
Agonopterix propinquella	(Treitschke, 1835)
Agonopterix purpurea	(Haworth, 1811)
Agonopterix putridella	(Denis & Schiffermüller, 1775)
Agonopterix rotundella	(Douglas, 1846)
Agonopterix selini	(Heinemann, 1870)
Agonopterix senecionis	(Nickerl, 1864)
Agonopterix subpropinquella	(Stainton, 1849)
Agonopterix thapsiella	(Zeller, 1847)
Agonopterix yeatiana	(Fabricius, 1781)
Agonum afrum	(Duftschmid, 1812)
Agonum angustatum	Dejean, 1828
Agonum atratum	(Duftschmid, 1812)
Agonum duftschmidi	Schmidt, 1994
Agonum gracilipes	(Duftschmid, 1812)
Agonum hypocrita	(Apfelbeck, 1904)
Agonum impressum	(Panzer, 1797)
Agonum longicorne	Chaudoir, 1846
Agonum lugens	(Duftschmid, 1812)
Agonum marginatum	(Linnaeus, 1758)
Agonum muelleri	(Herbst, 1784)
Agonum nigrum	Dejean, 1828
Agonum permoestum	Puel, 1938
Agonum sexpunctatum	(Linnaeus, 1758)
Agonum versutum	Sturm, 1824
Agonum viduum	(Panzer, 1797)
Agonum viridicupreum	(Goeze, 1777)
Agramma atricapillum	(Spinola, 1837)
Agramma confusum	(Puton, 1879)
Agramma fallax	(Horváth, 1906)
Agramma laetum	(Fallén, 1807)
Agramma minutum	Horváth, 1874
Agramma ruficorne	(Germar, 1833)
Agraylea multipunctata	Curtis, 1834
Agraylea sexmaculata	Curtis, 1834
Agrilinus ater	(De Geer, 1774)
Agrilinus convexus	(Erichson, 1848)
Agrilinus rufus	(Moll, 1782)
Agrilinus sordidus	(Fabricius, 1775)
Agrilus albogularis	Gory, 1841
Agrilus angustulus	(Illiger, 1803)
Agrilus ater	(Linnaeus, 1767)
Agrilus auricollis	Kiesenwetter, 1857
Agrilus betuleti	(Ratzeburg, 1837)
Agrilus biguttatus	(Fabricius, 1777)
Agrilus convexicollis	L. Redtenbacher, 1849
Agrilus convexifrons	Kiesenwetter, 1857
Agrilus croaticus	Abeille de Perrin, 1897
Agrilus cuprescens	Ménétries, 1832
Agrilus cyanescens	Ratzeburg, 1837
Agrilus delphinensis	Abeille de Perrin, 1897
Agrilus derasofasciatus	Lacordaire, 1835
Agrilus graminis	Laporte de Castelnau et Gory, 1837
Agrilus guerini	Lacordaire, 1835
Agrilus hastulifer	Ratzeburg, 1837
Agrilus hyperici	(Creutzer, 1799)
Agrilus integerrimus	(Ratzeburg, 1839)
Agrilus kubani	Bily, 1991
Agrilus laticornis	(Illiger, 1803)
Agrilus lineola	L. Redtenbacher, 1849
Agrilus litura	Kiesenwetter, 1857
Agrilus macroderus	Abeille de Perrin, 1897
Agrilus obscuricollis	Kiesenwetter, 1857
Agrilus olivicolor	Kiesenwetter, 1857
Agrilus populneus	(Schaefer, 1946)
Agrilus pratensis	(Ratzeburg, 1837)
Agrilus roscidus	Kiesenwetter, 1857
Agrilus salicis	Frivaldszky, 1877
Agrilus sericans	Kiesenwetter, 1857
Agrilus sinuatus	(Olivier, 1790)
Agrilus subauratus	Gebler, 1833
Agrilus sulcicollis	Lacordaire, 1835
Agrilus viridis	(Linnaeus, 1758)
Agrilus viscivorus	Bily, 1991
Agriopis aurantiaria	(Hübner, 1799)
Agriopis bajaria	(Denis & Schiffermüller, 1775)
Agriopis leucophaearia	(Denis & Schiffermüller, 1775)
Agriopis marginaria	(Fabricius, 1776)
Agriotes acuminatus	(Stephens, 1830)
Agriotes brevis	Candeze, 1863
Agriotes lineatus	(Linnaeus, 1767)
Agriotes medvedevi	Dolin, 1960
Agriotes modestus	Kiesenwetter, 1858
Agriotes obscurus	(Linnaeus, 1758)
Agriotes pallidulus	(Illiger, 1807)
Agriotes pilosellus	(Schönherr, 1817)
Agriotes proximus	Schwarz, 1891
Agriotes rufipalpis	(Brullé, 1832)
Agriotes sputator	(Linnaeus, 1758)
Agriotes ustulatus	(Schaller, 1783)
Agriphila brioniellus	(Zerny, 1914)
Agriphila deliella	(Hübner, 1813)
Agriphila geniculea	(Haworth, 1811)
Agriphila hungaricus	(A. Schmidt, 1909)
Agriphila inquinatella	(Denis & Schiffermüller, 1775)
Agriphila latistria	(Haworth,1811)
Agriphila poliellus	(Treitschke, 1832)
Agriphila selasella	(Hübner, 1813)
Agriphila straminella	(Denis & Schiffermüller, 1775)
Agriphila tolli	(Bleszynski, 1952)
Agriphila tristella	(Denis & Schiffermüller, 1775)
Agrius convolvuli	(Linnaeus, 1758)
Agrochola circellaris	(Hufnagel, 1766)
Agrochola helvola	(Linnaeus, 1758)
Agrochola humilis	([Denis & Schiffermüller], 1775)
Agrochola laevis	(Hübner, 1803)
Agrochola litura	(Linnaeus, 1758)
Agrochola lota	(Clerck, 1759)
Agrochola lychnidis	([Denis & Schiffermüller], 1775)
Agrochola macilenta	(Hübner, 1809)
Agrochola nitida	([Denis & Schiffermüller], 1775)
Agroeca brunnea	(Blackwall, 1833)
Agroeca cuprea	Menge, 1873
Agroeca lusatica	(L. Koch, 1875)
Agroeca proxima	(O.P.-Cambridge, 1871)
Agrotera nemoralis	(Scopoli, 1763)
Agrotis bigramma	(Esper, 1790)
Agrotis cinerea	([Denis & Schiffermüller], 1775)
Agrotis clavis	(Hufnagel, 1766)
Agrotis exclamationis	(Linnaeus, 1758)
Agrotis ipsilon	(Hufnagel, 1766)
Agrotis puta	(Hübner, 1803)
Agrotis segetum	([Denis & Schiffermüller], 1775)
Agrotis vestigialis	(Hufnagel, 1766)
Agrypnia pagetana	Curtis, 1834
Agrypnia varia	(Fabricius, 1793)
Agrypnus murinus	(Linnaeus, 1758)
Aguriahana stellulata	(Burmeister, 1841)
Agyrtes bicolor	Laporte De Castelnau, 1840
Agyrtes castaneus	(Fabricius, 1792)
Ahasverus advena	(Waltl, 1834)
Aiolopus strepens	(Latreille, 1804)
Aiolopus thalassinus	(Fabricius, 1781)
Airaphilus elongatus	(Gyllenhal, 1813)
Aizobius sedi	(Germar, 1818)
Akimerus schaefferi	(Laicharting, 1784)
Alabonia staintoniella	(Zeller, 1850)
Alaobia scapularis	(C.R.Sahlberg, 1831)
Alastor biegelebeni	Gordani Soika, 1942
Alastorynerus ludendorffi	(Dusmet, 1917)
Alastorynerus microdynerus	(Dalla Torre, 1889)
Alauda arvensis	Linnaeus, 1758
Alauda arvensis cantarella	Linnaeus, 1758
Alauda arvensis dulcivox	Linnaeus, 1758
Albocosta musiva	(Hübner, 1803)
Alboglossiphonia heteroclita	(Linnaeus, 1758)
Alboglossiphonia hyalina	(O.F. Müller, 1774)
Alburnoides bipunctatus	(Bloch, 1782)
Alburnus alburnus	(Linnaeus, 1758)
Alca torda	Linnaeus, 1758
Alcedo atthis	Linnaeus, 1758
Alcedo atthis ispida	Linnaeus, 1758
Alces alces	(Linnaeus, 1758)
Alcis bastelbergeri	(Hirschke, 1908)
Alcis jubata	(Thunberg, 1788)
Alcis repandata	(Linnaeus, 1758)
Alebra albostriella	(Fallén, 1826)
Alebra neglecta	Wagner, 1940
Alebra wahlbergi	(Boheman, 1845)
Aleimma loeflingiana	(Linnaeus, 1758)
Aleochara bellonata	Krása, 1922
Aleochara bilineata	(Gyllenhal, 1810)
Aleochara binotata	(Kraatz, 1856)
Aleochara bipustulata	(Linnaeus, 1761)
Aleochara breiti	(Ganglbauer, 1897)
Aleochara brevipennis	Gravenhorst, 1806
Aleochara clavicornis	L.Redtenbacher, 1849
Aleochara crassa	Baudi, 1848
Aleochara cuniculorum	(Kraatz, 1858)
Aleochara curtula	(Goeze, 1777)
Aleochara egregia	Apfelbeck, 1906
Aleochara erythroptera	Gravenhorst, 1806
Aleochara fumata	(Gravenhorst, 1802)
Aleochara funebris	(Wollaston, 1864)
Aleochara haematoptera	Kraatz, 1858
Aleochara haemoptera	(Kraatz, 1856)
Aleochara inconspicua	(Aubé, 1850)
Aleochara intricata	Mannerheim, 1830
Aleochara laevigata	(Gyllenhal, 1810)
Aleochara lanuginosa	(Gravenhorst, 1802)
Aleochara lata	Gravenhorst, 1802
Aleochara laticornis	Kraatz, 1856
Aleochara lygaea	(Kraatz, 1862)
Aleochara milleri	Kraatz, 1862
Aleochara moerens	(Gyllenhal, 1827)
Aleochara moesta	Gravenhorst, 1802
Aleochara peusi	(Wagner, 1949)
Aleochara puberula	Klug, 1832
Aleochara ruficornis	Gravenhorst, 1802
Aleochara sanguinea	(Linnaeus, 1758)
Aleochara sparsa	(Heer, 1839)
Aleochara spissicornis	Erichson, 1839
Aleochara tristis	Gravenhorst, 1806
Aleochara vagepunctata	(Kraatz, 1856)
Aleochara verna	(Say, 1836)
Aleochara villosa	(Mannerheim, 1830)
Aleuropteryx juniperi	Ohm, 1968
Aleuropteryx loewii	Klapálek, 1894
Aleuropteryx umbrata	Zelený, 1964
Alevonota aurantiaca	(Fauvel, 1895)
Alevonota egregia	(Rye, 1875)
Alevonota gracilenta	(Erichson, 1839)
Alevonota rufotestacea	(Kraatz, 1856)
Algedonia luctualis	(Hübner, 1793)
Alianta incana	(Erichson, 1837)
Allajulus dicentrus	(Latzel, 1884)
Allajulus groedensis	(Attems, 1899)
Allandrus undulatus	(Panzer, 1794)
Allecula atterima	Küster, 1849
Allecula morio	(Fabricius, 1787)
Planococcus vovae	(Nasonov, 1908)
Allodynerus delphinalis	(Giraud, 1866)
Allodynerus floricola	(Saussure, 1853)
Allodynerus rossii	(Lepeletier, 1841)
Alloeonotus fulvipes	(Scopoli, 1763)
Alloeorhynchus flavipes	Fieber, 1836
Alloeotomus germanicus	E. Wagner, 1939
Alloeotomus gothicus	(Fallén, 1807)
Allomalia quadrivirgata	(Costa, 1863)
Allomengea vidua	(L. Koch, 1879)
Allonyx quadrimaculatus	(Schaller, 1783)
Allophyes oxyacanthae	(Linnaeus, 1758)
Allotrichia pallicornis	(Eaton, 1873)
Alnetoidia alneti	(Dahlbom, 1850)
Alocentron curvirostre	(Gyllenhal, 1833)
Alocoderus hydrochaeris	(Fabricius, 1798)
Aloconota cambrica	(Wollaston, 1855)
Aloconota currax	(Kraatz, 1856)
Aloconota gregaria	(Erichson, 1839)
Aloconota insecta	(Thomson, 1856)
Aloconota languida	(Erichson, 1837)
Aloconota longicollis	(Mulsant et Rey, 1852)
Aloconota subgrandis	(A.Strand, 1954)
Aloconota sulcifrons	(Stephens, 1832)
Alona affinis	(Leydig, 1860)
Alona costata	Sars, 1862
Alona elegans	Kurz, 1874
Alona guttata	Sars, 1862
Alona intermedia	Sars, 1862
Alona karelica	Stenroos, 1897
Alona protzi	Hartwig, 1900
Alona quadrangularis	(O.F. Müller, 1785)
Alona rectangula	Sars, 1862
Alona rustica	Scott, 1895
Alonella excisa	(Fischer, 1854)
Alonella exigua	(Lilljeborg, 1853)
Alonella nana	(Baird, 1843)
Alopecosa accentuata	(Latreille, 1817)
Alopecosa aculeata	(Clerck, 1757)
Alopecosa cuneata	(Clerck, 1757)
Alopecosa cursor	(Hahn, 1831)
Alopecosa fabrilis	(Clerck, 1757)
Alopecosa inquilina	(Clerck, 1757)
Alopecosa mariae orientalis	(Dahl, 1908)
Alopecosa psammophila	Buchar, 2001
Alopecosa pulverulenta	(Clerck, 1757)
Alopecosa reimoseri	(Kolosváry, 1834)
Alopecosa schmidti	(Hahn, 1835)
Alopecosa solitaria	(Herman, 1879)
Alopecosa sulzeri	(Pavesi, 1873)
Alopecosa trabalis	(Clerck, 1757)
Alopia livida	(Menke, 1830)
Alopochen aegyptiacus	(Linnaeus, 1766)
Alosa kessleri	(Grimm 1887)
Alosa pontica	Eichwald, 1838
Alosimus syriacus	(Linnaeus, 1758)
Amara infima	(Duftschmid, 1812)
Alosimus syriacus austriacus	(Linnaeus, 1758)
Alosterna tabacicolor	(De Geer, 1775)
Alphitobius diaperinus	(Panzer, 1797)
Alphitophagus bifasciatus	(Say, 1823)
Alsophila aceraria	(Denis & Schiffermüller, 1775)
Alsophila aescularia	(Denis & Schiffermüller, 1775)
Altella hungarica	Loksa, 1981
Altella orientalis	Balogh, 1935
Altenia scriptella	(Hübner, 1796) comb.n.
Alucita cymatodactyla	Zeller, 1852
Alucita desmodactyla	Zeller, 1847
Alucita grammodactyla	Zeller, 1841
Alucita hexadactyla	Linnaeus, 1758
Alucita huebneri	Wallengren, 1859
Alydus calcaratus	(Linnaeus, 1758)
Allygidius abbreviatus	(Lethierry, 1878)
Allygidius atomarius	(Fabricius, 1794)
Allygidius commutatus	(Fieber, 1872)
Allygidius furcatus	(Ferrari, 1882)
Allygidius mayri	(Kirschbaum, 1868)
Allygus maculatus	Ribaut, 1948
Allygus mixtus	(Fabricius, 1794)
Allygus modestus	Scott, 1876
Alysson pertheesi	Gorski, 1852
Alysson ratzeburgi	Dahlbom, 1843
Alysson spinosus	(Panzer, 1801)
Alysson tricolor	Lepeletier & Serville, 1825
Amalorrhynchus melanarius	(Stephens, 1831)
Amalus scortillum	(Herbst, 1795)
Amara aenea	(De Geer, 1774)
Amara anthobia	A. villa et J. B. Villa, 1833
Amara apricaria	(Paykull, 1790)
Amara aulica	(Panzer, 1797)
Amara bifrons	(Gyllenhal, 1810)
Amara brunnea	(Gyllenhal, 1810)
Amara chaudoiri	Schaum, 1858
Amara chaudoiri incognita	Schaum, 1858
Amara communis	(Panzer, 1797)
Amara concinna	Zimmermann, 1832
Amara consularis	(Duftschmid, 1812)
Amara convexior	Stephens, 1828
Amara convexiuscula	(Marsham, 1802)
Amara crenata	Dejean, 1828
Amara cursitans	Zimmermann, 1832
Amara curta	Dejean, 1828
Amara equestris	(Duftschmid, 1812)
Amara eurynota	(Panzer, 1797)
Amara famelica	Zimmermann, 1832
Amara familiaris	(Duftschmid, 1812)
Amara fulva	(O. F. Müller, 1776)
Amara fulvipes	(Audinet-Serville, 1821)
Amara gebleri	Dejean, 1831
Amara ingenua	(Duftschmid, 1812)
Amara littorea	Thomson, 1857
Amara lucida	(Duftschmid, 1812)
Amara lunicollis	Schiödte, 1837
Amara majuscula	Chaudoir, 1850
Amara montivaga	Sturm, 1825
Amara municipalis	(Duftschmid, 1812)
Amara nitida	Sturm, 1825
Amara ovata	(Fabricius, 1792)
Amara plebeja	(Gyllenhal, 1810)
Amara praetermissa	Sahlberg, 1827
Amara proxima	Putzeys, 1866
Amara pseudostrenua	Kult, 1946
Amara sabulosa	(Audinet-Serville, 1821)
Amara saginata	Ménétriés, 1847
Amara saphyrea	Dejean, 1828
Amara similata	(Gyllenhal, 1810)
Amara sollicita	Pantel, 1888
Amara tibialis	(Paykull, 1798)
Amara tricuspidata	Dejean, 1831
Amarochara bonnairei	(Fauvel, 1865)
Amarochara forticornis	(Lacordaire, 1835)
Amarochara umbrosa	(Erichson, 1837)
Amata phegea	(Linnaeus, 1758)
Amaurobius erberi	(Keyserling, 1863)
Amaurobius fenestralis	(Stroem, 1768)
Amaurobius ferox	(Walckenaer, 1830)
Amaurobius jugorum	L. Koch, 1868
Amaurobius pallidus	L. Koch, 1868
Amauronyx maerkelii	(Aubé, 1844)
Amaurophanes stigmosalis	(Herrich-Schäffer, 1848)
Amblyptilia acanthadactyla	(Hübner, 1813)
Amblyptilia punctidactyla	(Haworth, 1811)
Amblystomus metallescens	(Dejean, 1829)
Amblystomus niger	Herr, 1838
Amblytylus albidus	(Hahn, 1834)
Amblytylus brevicollis	Fieber, 1858
Amblytylus concolor	Jakovlev, 1877
Amblytylus glaucicollis	Kerzhner, 1977
Amblytylus longiceps	(Flor, 1860)
Amblytylus nasutus	(Kirschbaum, 1856)
Amegilla albigena	(Lepeletier, 1841)
Amegilla garrula	(Rossi, 1790)
Amegilla magnilabris	(Fedtschenko, 1875)
Amegilla quadrifasciata	(Villers, 1789)
Amegilla salviae	(Morawitz, 1856)
Ameiurus melas	(Rafinesque, 1820)
Ameiurus nebulosus	(Lesueur, 1819)
Ameiurus nebulosus pannonicus	(Lesueur, 1819)
Ametropus fragilis	Albarda, 1878
Amidobia talpa	(Heer, 1841)
Amidorus thermicola	(Sturm, 1800)
Amischa analis	(Gravenhorst, 1802)
Amischa bifoveolata	(Mannerheim, 1830)
Amischa decipiens	(Sharp, 1869)
Amischa filum	(Mulsant et Rey, 1870)
Amischa forcipata	(Mulsant et Rey, 1873)
Amischa nigrofusca	(Stephens, 1832)
Ammobates melectoides	Smith, 1852
Ammobates punctatus	(Fabricius, 1804)
Ammobates similis	Mocsáry, 1895
Ammobates vinctus	Gerstaecker, 1869
Ammobatoides abdominalis	(Eversmann, 1852)
Ammoconia caecimacula	([Denis & Schiffermüller], 1775)
Ammoecius brevis	(Erichson, 1848)
Ammophila campestris	Latreille, 1809
Ammophila heydeni	Dahlbom, 1845
Ammophila hungarica	Mocsáry, 1883
Ammophila sabulosa	(Linnaeus, 1758)
Ammophila terminata	Smith, 1856
Ammophila terminata mocsaryi	Smith, 1856
Ammoplanus handlirschi	Gussakovkskij, 1931
Ammoplanus hofferi	Snoflak, 1943
Ammoplanus wesmaeli	Giraud, 1869
Ampedus balteatus	(Linnaeus, 1758)
Ampedus cardinalis	(Schiödte, 1865)
Ampedus cinnaberinus	(Eschscholtz, 1829)
Ampedus elegantulus	(Schönherr, 1817)
Ampedus erythrogonus	(P. W. J. Müller, 1821)
Ampedus forticornis	(Schwarz, 1900)
Ampedus glycereus	(Herbst, 1784)
Ampedus hjorti	(Rye, 1905)
Ampedus nigerrimus	(Lacordaire, 1835)
Ampedus nigroflavus	(Goeze, 1777)
Ampedus pomonae	(Stephens, 1830)
Ampedus pomorum	(Herbst, 1784)
Ampedus praeustus	(Fabricius, 1792)
Ampedus quadrisignatus	(Gyllenhal, 1817)
Ampedus rufipennis	(Stephens, 1830)
Ampedus sanguineus	(Linnaeus, 1758)
Ampedus sanguinolentus	(Schrank, 1776)
Ampedus sinuatus	(Germar, 1844)
Amphiareus obscuriceps	Poppius, 1909
Amphichroum canaliculatum	(Erichson, 1840)
Amphicyllis globiformis	(C.R. Sahlberg, 1833)
Amphicyllis globus	(Fabricius, 1792)
Amphimallon assimile	(Herbst, 1790)
Amphimallon burmeisteri	(Brenske, 1886)
Amphimallon solstitiale	(Linnaeus, 1758)
Amphimelania holandrii	(C. Pfeiffer, 1828)
Amphinemura borealis	(Morton, 1894)
Amphinemura standfussi	(Ris, 1902)
Amphinemura sulcicollis	(Stephens, 1836)
Amphipoea fucosa	(Freyer, 1830)
Amphipoea lucens	(Freyer, 1845)
Amphipoea oculea	(Linnaeus, 1761)
Amphipyra berbera	(Rungs, 1949)
Amphipyra berbera svenssoni	Fletcher, 1968
Amphipyra livida	([Denis & Schiffermüller], 1775)
Amphipyra perflua	(Fabricius, 1787)
Amphipyra pyramidea	(Linnaeus, 1758)
Amphipyra tetra	(Fabricius, 1787)
Amphipyra tragopoginis	(Clerck, 1759)
Amphisbatis incongruella	(Stainton, 1849)
Amphotis marginata	(Fabricius, 1781)
Ampulex fasciata	Jurine, 1807
Anabolia brevipennis	(Curtis, 1834)
Anabolia furcata	Brauer, 1857
Anacaena globulus	(Paykull, 1798)
Anacaena limbata	(Fabricius, 1792)
Anacaena lutescens	(Stephens, 1829)
Anacampsis blattariella	(Hübner, 1796)
Anacampsis obscurella	(Denis & Schiffermüller, 1775)
Anacampsis populella	(Clerck, 1759)
Anacampsis scintillella	(Fischer v. Röslerstamm, 1841)
Anacampsis timidella	(Wocke, 1887)
Anaceratagallia laevis	(Ribaut, 1935)
Anaceratagallia ribauti	(Ossiannilsson, 1938)
Anaceratagallia venosa	(Fourcroy, 1758)
Aeshna isoceles	(Müller, 1767)
Saperda carcharias	(Linnaeus, 1758)
Saperda similis	Laicharting, 1784
Anaesthetis testacea	(Fabricius, 1781)
Anaglyptus mysticus	(Linnaeus, 1758)
Anakelisia fasciata	(Kirschbaum, 1868)
Anakelisia perspicillata	(Boheman, 1845)
Anania funebris	(Ström, 1768)
Anania verbascalis	(Denis & Schiffermüller, 1775)
Anaplectoides prasina	([Denis & Schiffermüller], 1775)
Anapus longicornis	Jakovlev, 1881
Anarsia lineatella	Zeller, 1839
Anarsia spartiella	(Schrank, 1802)
Anarta myrtilli	(Linnaeus, 1761)
Anas acuta	Linnaeus, 1758
Anas carolinensis	Gmelin, 1789
Anas clypeata	Linnaeus, 1758
Anas crecca	Linnaeus, 1758
Anas penelope	Linnaeus, 1758
Anas platyrhynchos	Linnaeus, 1758
Anas querquedula	Linnaeus, 1758
Anas strepera	Linnaeus, 1758
Anasimyia contracta	Claussen & Torp, 1980
Anasimyia interpuncta	(Harris, 1776)
Anasimyia lineata	(Fabricius, 1787)
Anasimyia transfuga	(Linnaeus, 1758)
Anasphaltis renigerellus	(Zeller, 1839)
Anaspis brunnipes	Mulsant, 1856
Anaspis costai	Emery, 1876
Anaspis excellens	Schilsky, 1908
Anaspis flava	(Linnaeus, 1758)
Anaspis frontalis	(Linnaeus, 1758)
Anaspis kiesenwetteri	Emery, 1876
Anaspis lurida	Stephens, 1832
Anaspis melanostoma	Costa, 1854
Anaspis nigripes	Dh.Brisout, 1866
Anaspis palpalis	Gerhardt, 1876
Anaspis pulicaria	Costa, 1854
Anaspis ruficollis	Fabricius, 1792
Anaspis rufilabris	(Gyllenhall, 1827)
Anaspis subtilis	Hampe, 1870
Anaspis thoracica	(Linnaeus, 1758)
Anaspis varians	Mulsant, 1856
Anaspis viennensis	Schilsky, 1895
Anastrangalia dubia	(Scopoli, 1763)
Anastrangalia reyi	(Heyden, 1889)
Anastrangalia sanguinolenta	(Linnaeus, 1761)
Anatis ocellata	(Linnaeus, 1758)
Anaulacaspis laevigata	(Eppelsheim, 1883)
Anaulacaspis nigra	(Gravenhorst, 1802)
Anax imperator	Leach, 1815
Anax parthenope	(Sélys-Longchammps, 1839)
Anchinia cristalis	(Scopoli, 1763)
Anchinia daphnella	(Denis & Schiffermüller, 1775)
Anchinia laureolella	Herrich-Schäffer, 1854
Anchistropus emarginatus	Sars, 1862
Anchomenus dorsalis	(Pontoppidan, 1763)
Ancistrocerus acutus	(Fabricius, 1793)
Ancistrocerus antilope	(Panzer, 1798)
Ancistrocerus claripennis	Thomson, 1874
Ancistrocerus dusmetiolus	(Strand, 1914)
Ancistrocerus gazella	(Panzer, 1798)
Ancistrocerus ichneumonideus	(Ratzeburg, 1844)
Ancistrocerus nigricornis	(Curtis, 1826)
Ancistrocerus oviventris	(Wesmael, 1836)
Ancistrocerus parietinus	(Linnaeus, 1761)
Ancistrocerus parietum	(Linnaeus, 1758)
Ancistrocerus scoticus	(Curtis, 1826)
Ancistrocerus trifasciatus	(Müller, 1776)
Ancistronycha abdominalis	(Fabricius, 1789)
Ancistronycha erichsonii	Bach, 1854
Ancistronycha violacea	(Paykull, 1798)
Ancylis achatana	(Denis & Schiffermüller, 1775)
Ancylis apicella	(Denis & Schiffermüller, 1775)
Ancylis badiana	(Denis & Schiffermüller, 1775)
Ancylis comptana	(Frölich, 1828)
Ancylis diminutana	(Haworth, 1811)
Ancylis geminana	(Donovan, 1806)
Ancylis laetana	(Fabricius, 1775)
Ancylis mitterbacheriana	(Denis & Schiffermüller, 1775)
Ancylis myrtillana	(Treitschke, 1830)
Ancylis obtusana	(Haworth, 1811)
Ancylis paludana	Barrett, 1886
Ancylis selenana	(Guenée, 1845)
Ancylis subarcuana	(Douglas, 1847)
Ancylis tineana	(Hübner, 1799)
Ancylis uncella	(Denis & Schiffermüller, 1775)
Ancylis unculana	(Haworth, 1811)
Ancylis unguicella	(Linnaeus, 1758)
Ancylis upupana	(Treitschke, 1835)
Ancylolomia disparalis	Hübner, 1825
Ancylolomia palpella	(Denis & Schiffermüller, 1775)
Ancylolomia pectinatellus	(Zeller, 1847)
Ancylosis albidella	Ragonot, 1888
Ancylosis cinnamomella	(Duponchel, 1836)
Ancylosis oblitella	(Zeller, 1848)
Ancylosis roscidella	(Eversmann, 1844)
Ancylosis sareptalla	(Herrich-Schäffer, 1861)
Ancylus fluviatilis	O.F. Müller, 1774
Ancyrona japonica	(Reitter, 1889)
Ancyrosoma leucogrammes	(Gmelin, 1781)
Andrena aberrans	Eversmann, 1852
Andrena aciculata	Morawitz, 1886
Andrena aeneiventris	Morawitz, 1872
Andrena agilissima	(Scoploi, 1770)
Andrena albopunctata	Rossi, 1792
Andrena alfkenella	Perkins, 1914
Andrena apicata	Smith, 1847
Andrena argentata	Smith, 1844
Andrena assimilis	Radoszkowski, 1876
Andrena assimilis gallica	Radoszkowski, 1876
Andrena atrata	Friese, 1887
Andrena atrotegularis	Hedicke, 1923
Andrena barbilabris	(Kirby, 1802)
Andrena bicolor	Fabricius, 1775
Andrena bimaculata	(Kirby, 1802)
Andrena bisulcata	Morawitz, 1877
Andrena bluethgeni	Stöckhert, 1930
Andrena braunsiana	Friese, 1887
Andrena bucephala	Stephens, 1846
Andrena chrysopus	Pérez, 1903
Andrena chrysopyga	Schenck, 1853
Andrena chrysosceles	(Kirby, 1802)
Andrena cineraria	(Linnaeus, 1758)
Andrena clarkella	(Kirby, 1802)
Andrena clypella	Strand, 1921
Andrena clypella hasitata	Strand, 1921
Andrena coitana	(Kirby, 1802)
Andrena combaella	Warncke, 1966
Andrena combinata	(Christ, 1791)
Andrena combinata mehelyi	(Christ, 1791)
Andrena congruens	Schmiedeknecht, 1883
Andrena cordialis	Morawitz, 1877
Andrena curvana	Warncke, 1965
Andrena curvungula	Thomson, 1870
Andrena decipiens	Schenck, 1859
Andrena denticulata	(Kirby, 1802)
Andrena distinguenda	Schenck, 1871
Andrena dorsalis	Brullé, 1832
Andrena dorsata	(Kirby, 1802)
Andrena dorsata propinqua	(Kirby, 1802)
Andrena enslinella	Stöckhert, 1924
Andrena erythrocnemis	Morawitz, 1870
Andrena falsifica	Perkins, 1915
Andrena ferox	Smith, 1847
Andrena figurata	Morawitz, 1866
Andrena flavipes	Panzer, 1799
Andrena florea	Fabricius, 1793
Andrena floricola	Eversmann, 1852
Andrena florivaga	Eversmann, 1852
Andrena fucata	Smith, 1847
Andrena fulva	(Müller, 1766)
Andrena fulvago	(Christ, 1791)
Andrena fulvata	Stöckhert, 1930
Andrena fulvicornis	Schenck, 1853
Andrena fulvida	Schenck, 1853
Andrena fuscipes	(Kirby, 1802)
Andrena fuscosa	Erichson, 1835
Andrena gelriae	Van Der Vecht, 1927
Andrena graecella	Warncke, 1965
Andrena granulosa	Pérez, 1902
Andrena gravida	Imhoff, 1832
Andrena haemorrhoa	(Fabricius, 1781)
Andrena hattorfiana	(Fabricius, 1775)
Andrena hedikae	Jaeger, 1934
Andrena helvola	(Linnaeus, 1758)
Andrena hesperia	Smith, 1853
Andrena humilis	Imhoff, 1832
Andrena hungarica	Friese, 1887
Andrena hypopolia	Schmiedeknecht, 1883
Andrena impunctata	Pérez, 1895
Andrena incisa	Eversmann, 1852
Andrena intermedia	Thomson, 1870
Andrena ispida	Warncke, 1965
Andrena labialis	(Kirby, 1802)
Andrena labiata	Fabricius, 1781
Andrena lagopus	Latreille, 1809
Andrena lathyri	Alfken, 1899
Andrena lepida	Schenck, 1859
Andrena limbata	Eversmann, 1852
Andrena majalis	Morawitz, 1867
Andrena marginata	Fabricius, 1776
Andrena minutula	(Kirby, 1802)
Andrena minutuloides	Perkins, 1914
Andrena mitis	Schmiedeknecht, 1883
Andrena mocsaryi	Schmiedeknecht, 1883
Andrena morio	Brullé, 1832
Andrena mucida	Kriechbaumer, 1873
Andrena nana	(Kirby, 1802)
Andrena nanaeformis	Noskiewicz, 1924
Andrena nanula	Nylander, 1848
Andrena nasuta	Giraud, 1863
Andrena nigroaenea	(Kirby, 1802)
Andrena nigrospina	Thomson, 1872
Andrena nitida	(Müller, 1776)
Andrena nitida limata	(Müller, 1776)
Andrena nitidiuscula	Schenck, 1853
Andrena niveata	Friese, 1887
Andrena nobilis	Morawitz, 1874
Andrena nuptialis	Pérez, 1902
Andrena nychtemera	Imhoff, 1866
Andrena oralis	Morawitz, 1876
Andrena orenburgensis	Schmiedeknecht, 1883
Andrena ovatula	(Kirby, 1802)
Andrena pallitarsis	Pérez, 1903
Andrena pandellei	Pérez, 1895
Andrena parviceps	Kriechbaumer, 1873
Andrena paucisquama	Noskiewicz, 1924
Andrena pellucens	Pérez, 1895
Andrena pillichi	Noskiewicz, 1939
Andrena polita	Smith, 1847
Andrena pontica	Warncke, 1972
Andrena potentillae	Panzer, 1809
Andrena praecox	(Scopoli, 1763)
Andrena proxima	(Kirby, 1802)
Andrena pusilla	Pérez, 1903
Andrena rosae	Panzer, 1801
Andrena roseipes	Alfken, 1933
Andrena ruficrus	Nylander, 1848
Andrena rufula	Schmiedeknecht, 1883
Andrena rugulosa	Stöckhert, 1935
Andrena sabulosa	(Scopoli, 1763)
Andrena sabulosa trimmerana	(Scopoli, 1763)
Andrena saxonica	Stöckhert, 1935
Andrena schencki	Morawitz, 1866
Andrena schlettereri	Friese, 1896
Andrena scita	Eversmann, 1852
Andrena semilaevis	Pérez, 1903
Andrena seminuda	Friese, 1896
Andrena sericata	Imhoff, 1866
Andrena similis	Smith, 1849
Andrena simontornyella	Noskiewicz, 1939
Andrena strohmella	Stöckhert, 1928
Andrena subopaca	Nylander, 1848
Andrena suerinensis	Friese, 1884
Andrena susterai	Alfken, 1914
Andrena symphyti	Schmiedeknecht, 1883
Andrena synadelpha	Perkins, 1914
Andrena taraxaci	Giraud, 1861
Andrena tarsata	Nylander, 1848
Andrena thoracica	(Fabricius, 1775)
Andrena tibialis	(Kirby, 1802)
Andrena tibialis vindobonensis	(Kirby, 1802)
Andrena transitoria	Morawitz, 1871
Andrena tridentata	(Kirby, 1802)
Andrena truncatilabris	Morawitz, 1877
Andrena tscheki	Morawitz, 1872
Andrena ungeri	Mavromoustakis, 1952
Andrena vaga	Panzer, 1799
Andrena variabilis	Smith, 1853
Andrena varians	(Kirby, 1802)
Andrena ventralis	Imhoff, 1832
Andrena ventricosa	Dours, 1873
Andrena viridescens	Viereck, 1916
Andrena wilkella	(Kirby, 1802)
Selimus pulchellus	(Walckenaer, 1802)
Selimus vittatus	(C.L. Koch, 1836)
Anemadus strigosus	(Kraatz, 1852)
Anerastia dubia	Gerasimov, 1929
Anerastia lotella	(Hübner, 1813)
Anergates atratulus	(Schenck, 1852)
Aneurus avenius	Dufour, 1833
Aneurus laevis	(Fabricius, 1775)
Angerona prunaria	(Linnaeus, 1758)
Anguilla anguilla	(Linnaeus, 1758)
Anguis fragilis	Linnaeus, 1758
Anguliphantes angulipalpis	(Westring, 1851)
Anidorus nigrinus	(Germar, 1831)
Anisarthron barbipes	(Schrank, 1781)
Anisodactylus binotatus	(Fabricius, 1787)
Anisodactylus nemorivagus	(Duftschmid, 1812)
Anisodactylus poeciloides	(Stephens, 1828)
Anisodactylus signatus	(Panzer, 1797)
Anisoplia agricola	(Poda, 1761)
Anisoplia austriaca	(Herbst, 1783)
Anisoplia bromicola	(Germar, 1817)
Anisoplia erichsoni	Reitter, 1889
Anisoplia tempestiva	Erichson, 1847
Anisoplia lata	Erichson, 1847
Anisorus quercus	(Götz, 1783)
Anisosticta novemdecimpunctata	(Linnaeus, 1758)
Anisotoma axillaris	(Gyllenhal, 1810)
Anisotoma castanea	(Herbst, 1792)
Anisotoma glabra	(Fabricius, 1792)
Anisotoma humeralis	(Fabricius, 1792)
Anisotoma orbicularis	(Herbst, 1792)
Anisoxya fuscula	(Illiger, 1798)
Anisus calculiformis	(Sandberger, 1874)
Anisus leucostoma	(Millet, 1813)
Anisus spirorbis	(Linnaeus, 1758)
Anisus vortex	(Linnaeus, 1758)
Anisus vorticulus	(Troschel, 1834)
Annitella obscurata	(McLachlan, 1876)
Anobium punctatum	(De Geer, 1774)
Anodonta anatina	(Linnaeus, 1758)
Anodonta cygnea	(Linnaeus, 1758)
Anogcodes seladonius	(W. Schmidt, 1846)
Anogcodes seladonius azureus	(W. Schmidt, 1846)
Anogcodes ferrugineus	(Schrank, 1776)
Anogcodes fulvicollis	(Scopoli, 1763)
Anogcodes ruficollis	(Fabricius, 1781)
Anogcodes rufiventris	(Scopoli, 1763)
Anogcodes ustulatus	(Scopoli, 1763)
Anomala dubia	(Scopoli, 1763)
Anomala solida	Erichson, 1847
Anomala vitis	(Fabricius, 1775)
Anommatus duodecimstriatus	(J.P.W. Müller, 1821)
Anommatus hungaricus	Dudich, 1922
Anommatus jelinecki	Dajoz, 1971
Anommatus pannonicus	Kaszab, 1947
Anommatus reitteri	(Ganglbauer, 1899)
Anommatus stilleri	Kaszab, 1947
Anomognathus cuspidatus	(Erichson, 1839)
Anopheles algeriensis	Theobald, 1903
Anopheles atroparvus	Van Thiel, 1927
Anopheles claviger	(Meigen, 1818)
Anopheles hyrcanus	(Pallas, 1771)
Anopheles maculipennis	Meigen, 1818
Anopheles messeae	Falleroni, 1926
Anopheles plumbeus	Stephens, 1828
Anophococcus inermis	(Green,1915)
Anopleta corvina	(Thomson, 1856)
Anopleta kochi	Roubal, 1937
Anoplius alpinobalticus	Wolf, 1956
Anoplius caviventris	(Aurivillius, 1907)
Anoplius concinnus	(Dahlbom, 1845)
Anoplius infuscatus	(Vander Linden, 1827)
Anoplius nigerrimus	(Scopoli, 1763)
Anoplius piliventris	(Morawtz, 1889)
Anoplius samariensis	(Pallas, 1771)
Anoplius tenuicornis	(Tournier, 1889)
Anoplius viaticus	(Linnaeus, 1758)
Anoplius viaticus paganus	(Linnaeus, 1758)
Anoplodera rufipes	(Schaller, 1783)
Anoplodera sexguttata	(Fabricius, 1775)
Anoplotettix fuscovenosus	(Ferrari, 1882)
Anoplotettix horvathi	Metcalf, 1955
Anoplotrupes stercorosus	(Scriba, 1791)
Anoplus plantaris	(Naezén, 1794)
Anoplus roboris	Suffrian, 1840
Anoplus setulosus	Kirsch, 1870
Perigrapha munda	(Denis & Schiffermüller, 1775)
Anoscopus albifrons	(Linnaeus, 1758)
Anoscopus albiger	(Germar, 1821)
Anoscopus flavostriatus	(Donovan, 1799)
Anoscopus histrionicus	(Fabricius, 1794)
Anoscopus serratulae	(Fabricius, 1775)
Anospilus orbitalis	(Costa, 1863)
Anostirus castaneus	(Linnaeus, 1859)
Anostirus purpureus	(Poda, 1761)
Anoterostemma ivanoffi	(Lethierry, 1876)
Anotylus bernhaueri	(Ganglbauer, 1898)
Anotylus clypeonitens	(Pandellé, 1867)
Anotylus complanatus	(Erichson, 1839)
Anotylus fairmairei	(Pandellé, 1867)
Anotylus hamatus	(Fairmaire & Laboulbene, 1856)
Anotylus hybridus	(Eppelsheim, 1878)
Anotylus insecatus	(Gravenhorst, 1806)
Anotylus intricatus	(Erichson, 1840)
Anotylus inustus	(Gravenhorst, 1806)
Anotylus mendus	(Herman, 1975)
Anotylus nitidulus	(Gravenhorst, 1802)
Anotylus politus	(Erichson, 1840)
Anotylus pumilus	(Erichson, 1839)
Anotylus rugifrons	(Hochhuth, 1849)
Anotylus rugosus	(Fabricius, 1775)
Anotylus saulcyi	(Pandellé, 1867)
Anotylus sculpturatus	(Gravenhorst, 1806)
Anotylus speculifrons	(Kraatz, 1858)
Anotylus tetracarinatus	(Block, 1799)
Anotylus tetratoma	(Czwalina, 1871)
Anoxia orientalis	(Krynicki, 1832)
Anoxia pilosa	(Fabricius, 1792)
Anser albifrons	(Scopoli, 1769)
Anser anser	(Linnaeus, 1758)
Anser anser rubrirostris	(Linnaeus, 1758)
Anser brachyrhynchus	Baillon, 1833
Anser caerulescens	(Linnaeus, 1758)
Anthidium loti	Perris, 1852
Anser caerulescens atlanticus	(Linnaeus, 1758)
Anser erythropus	(Linnaeus, 1758)
Anser fabalis	(Latham, 1787)
Anser fabalis johanseni	(Latham, 1787)
Anser fabalis rossicus	(Latham, 1787)
Anser indicus	(Latham, 1790)
Antepipona deflenda	(Saunders, 1853)
Antepipona orbitalis	(Herrich-Schaeffer, 1839)
Anthaxia candens	(Panzer, 1787)
Anthaxia cichorii	(Olivier, 1790)
Anthaxia deaurata	(Gmelin, 1787)
Anthaxia fulgurans	(Schrank, 1787)
Anthaxia funerula	(Illiger, 1803)
Anthaxia godeti	Laporte et Gory, 1839
Anthaxia hackeri	Frivaldszky, 1884
Anthaxia helvetica	Stierlin, 1868
Anthaxia hungarica	(Scopoli, 1772)
Anthaxia istriana	Rosenhauer, 1874
Anthaxia manca	(Linnaeus, 1767)
Anthaxia nitidula	(Linnaeus, 1757)
Anthaxia nitidula signaticollis	(Linnaeus, 1757)
Anthaxia olympica	Kiesenwetter, 1858
Anthaxia plicata	Kiesenwetter, 1859
Anthaxia podolica	Mannerheim, 1837
Anthaxia quadripunctata	(Linnaeus, 1758)
Anthaxia salicis	(Fabricius, 1777)
Anthaxia semicuprea	Küster, 1851
Anthaxia similis	(Saunders, 1871)
Anthaxia suzannae	Théry, 1942
Anthaxia tuerki	Ganglbauer, 1886
Anthelephila pedestris	(Rossi, 1790)
Antheminia lunulata	(Goeze, 1778)
Antheraea yamamai	(Guérin-Méneville, 1861)
Antherophagus canescens	Grouvelle, 1916
Antherophagus nigricornis	(Fabricius, 1787)
Antherophagus pallens	(Linnaeus, 1758)
Anthicus antherinus	(Linnaeus, 1761)
Anthicus ater	(Panzer, 1796)
Anthicus axillaris	Schmidt, 1842
Anthicus bimaculatus	(Illiger, 1802)
Anthicus fenestratus	Schmidt, 1842
Anthicus flavipes	/Panzer, 1797)
Anthicus schmidtii	Rosenhauer, 1847
Anthidium barbatum	Mocsáry, 1884
Anthidium cingulatum	Latreille, 1809
Anthidium florentinum	(Fabricius, 1775)
Anthidium interruptum	(Fabricius, 1781)
Anthidium laterale	Latreille, 1809
Anthidium lituratum	(Panzer, 1801)
Anthidium manicatum	(Linnaeus, 1758)
Anthidium oblongatum	(Illiger, 1806)
Anthidium pubescens	Morawitz, 1873
Anthidium punctatum	Latreille, 1809
Anthidium septemdentatum	Latreille, 1809
Anthidium septemspinosum	Lepeletier, 1841
Anthidium strigatum	(Panzer, 1805)
Anthidium tenellum	Mocsáry, 1879
Anthobium atrocephalum	(Gyllenhal, 1827)
Anthobium melanocephalum	(Illiger, 1794)
Anthocharis cardamines	(Linnaeus, 1758)
Anthocharis gruneri	Herrich-Schäffer, 1851
Anthocomus equestris	(Fabricius, 1781)
Anthocomus fasciatus	(Linnaeus, 1758)
Anthocomus rufus	(Herbst, 1786)
Anthocoris amplicollis	Horváth, 1893
Anthocoris confusus	Reuter, 1889
Anthocoris gallarumulmi	(De Geer, 1773)
Anthocoris limbatus	Fieber, 1836
Anthocoris minki	Dohrn, 1860
Anthocoris nemoralis	(Fabricius, 1794)
Anthocoris nemorum	(Linnaeus, 1761)
Anthocoris pilosus	(Jakovlev, 1877)
Anthocoris simulans	Reuter, 1884
Anthonomus bituberculatus	Thomson, 1868
Aulacobaris chevrolati	Desbrochers, 1868
Anthonomus conspersus	Desbrochers, 1868
Anthonomus humeralis	(Panzer, 1794)
Anthonomus kirschi	Desbrochers, 1868
Anthonomus pedicularius	(Linnaeus, 1758)
Anthonomus phyllocola	(Herbst, 1795)
Anthonomus pinivorax	Silfverberg, 1977
Anthonomus piri	Kollár, 1837
Anthonomus pomorum	(Linnaeus, 1758)
Anthonomus rectirostris	(Linnaeus, 1758)
Anthonomus rubi	(Herbst, 1795)
Anthonomus rubripes	Gyllenhal, 1836
Anthonomus rufus	Gyllenhal, 1836
Anthonomus sorbi	Germar, 1821
Anthonomus spilotus	L. Redtenbacher, 1849
Anthonomus ulmi	(DeGeer, 1775)
Anthonomus undulatus	Gyllenhal, 1836
Anthophagus alpestris	(Heer, 1839)
Anthophagus alpinus	(Paykull, 1790)
Anthophagus angusticollis	Mannerheim, 1830
Anthophagus bicornis	(Block, 1799)
Anthophagus caraboides	(Linnaeus, 1758)
Anthophila fabriciana	(Linnaeus, 1767)
Anthophora aestivalis	(Panzer, 1801)
Anthophora bimaculata	(Panzer, 1798)
Anthophora borealis	Morawitz, 1864
Anthophora crinipes	Smith, 1854
Anthophora furcata	(Panzer, 1798)
Anthophora plagiata	(Illiger, 1806)
Anthophora plumipes	(Pallas, 1772)
Anthophora pubescens	(Fabricius, 1781)
Anthophora quadrimaculata	(Panzer, 1798)
Anthophora retusa	(Linnaeus, 1758)
Anthracus consputus	(Duftschmid, 1812)
Anthracus longicornis	(Schaum, 1857)
Anthracus transversalis	(Schaum, 1862)
Anthrenus fuscus	Olivier, 1789
Anthrenus goliath	Mulsant et Rey, 1867
Anthrenus museorum	(Linnaeus, 1761)
Anthrenus olgae	Kalik, 1946
Anthrenus pimpinellae	Fabricius, 1775
Anthrenus polonicus	Mroczkowski, 1951
Anthrenus scrophulariae	(Linnaeus, 1758)
Anthrenus verbasci	(Linnaeus, 1767)
Anthribus fasciatus	(Forster, 1771)
Anthribus nebulosus	(Forster, 1771)
Anthropoides virgo	(Linnaeus, 1758)
Anthus campestris	(Linnaeus, 1758)
Anthus cervinus	(Pallas, 1811)
Anthus pratensis	(Linnaeus, 1758)
Anthus richardi	Vieillot, 1818
Anthus spinoletta	(Linnaeus, 1758)
Anthus trivialis	(Linnaeus, 1758)
Anticlea badiata	(Denis & Schiffermüller, 1775)
Anticlea derivata	(Denis & Schiffermüller, 1775)
Anticollix sparsata	(Treitschke, 1828)
Antigastra catalaunalis	(Duponchel, 1833)
Antispila metallella	(Denis & Schiffermüller, 1775)
Antispila treitschkiella	(Fischer v. Röslerstamm, 1843)
Antistea elegans	(Blackwall, 1841)
Antitype chi	(Linnaeus, 1758)
Thetidia smaragdaria	(Fabricius, 1787)
Antonina purpurea	Signoret,1872
Antoninella inaudita	Kiritchenko,1937
Anyphaena accentuata	(Walckenaer, 1802)
Paratinus femoralis	(Erichson, 1840)
Paratinus flavolimbatus	Mulsant & Rey, 1853
Apalus bimaculatus	(Linnaeus, 1761)
Apalus bipunctatus	Germar, 1817
Apalus necydaleus	(Pallas, 1782)
Apamea anceps	([Denis & Schiffermüller], 1775)
Apamea aquila	Donzel, 1837
Apamea crenata	(Hufnagel, 1766)
Apamea epomidion	(Haworth, 1809)
Apamea furva	([Denis & Schiffermüller], 1775)
Apamea illyria	Freyer, 1846
Apamea lateritia	(Hufnagel, 1766)
Apamea lithoxylaea	([Denis & Schiffermüller], 1775)
Apamea monoglypha	(Hufnagel, 1766)
Apamea oblonga	(Haworth, 1809)
Apamea ophiogramma	(Esper, 1794)
Apamea platinea	(Treitschke, 1825)
Apamea remissa	(Hübner, 1809)
Apamea rubrirena	(Treitschke, 1825)
Apamea scolopacina	(Esper, 1788)
Apamea sicula	Osthelder, 1933
Apamea sicula tallosi	Osthelder, 1933
Apamea sordens	(Hufnagel, 1766)
Apamea sublustris	(Esper, 1788)
Apamea unanimis	(Hübner, 1813)
Aparopion costatum	Fahraeus, 1843
Apatania muliebris	McLachlan, 1866
Apatema mediopallidum	Walsingham, 1900
Apatema whalleyi	Popescu-Gorj & Capuse, 1965
Apatetris trivittellum	(Rebel, 1903)
Apatura ilia	([Denis & Schiffermüller], 1775)
Apatura iris	(Linnaeus, 1758)
Apatura metis	Freyer, 1829
Apaustis rupicola	([Denis & Schiffermüller], 1775)
Apeira syringaria	(Linnaeus, 1758)
Aphanisticus elongatus	A. et G.B. Villa, 1835
Aphanisticus emarginatus	(Olivier 1790)
Aphanisticus pusillus	(Olivier, 1790)
Aphantaulax cincta	(L. Koch, 1866)
Aphantaulax trifasciata	(O.P. Cambridge, 1872)
Aphantopus hyperantus	(Linnaeus, 1758)
Aphanus rolandri	(Linnaeus, 1758)
Aphelia ferugana	(Hübner, 1793)
Aphelia paleana	(Hübner, 1793)
Aphelia viburnana	(Denis & Schiffermüller, 1775)
Aphelocheirus aestivalis	(Fabricius, 1803)
Aphelonema quadrivittata	(Fieber, 1876)
Aphenogaster subterranea	(Latreille, 1798)
Aphidecta obliterata	(Linnaeus, 1758)
Aphodius fimetarius	(Linnaeus, 1758)
Aphodius foetens	(Fabricius, 1787)
Aphodius foetidus	(Herbst, 1783)
Aphomia sociella	(Linnaeus, 1758)
Aphrodes bicinctus	(Schrank, 1776)
Planaphrodes furcillatus	(Sáringer, 1959)
Aphrodes makarovi	Zachvatkin, 1948
Aphrophora alni	(Fallén, 1805)
Aphrophora corticea	Germar, 1821
Aphrophora salicina	(Goeze, 1778)
Aphytobius sphaerion	(Boheman, 1845)
Apion cruentatum	Walton, 1844
Apion frumentarium	(Linnaeus, 1758)
Apion haematodes	Kirby, 1808
Apion rubens	Walton, 1844
Apion rubiginosum	Grill, 1893
Apis mellifera	Linnaeus, 1758
Aplasta ononaria	(Fuessly, 1783)
Aplexa hypnorum	(Linnaeus, 1758)
Aplocera efformata	(Guenée, 1857)
Aplocera plagiata	(Linnaeus, 1758)
Aplocera praeformata	(Hübner, 1826)
Aplocnemus basalis	(Küster, 1849)
Aplocnemus chalconatus	(Germar, 1817)
Aplocnemus impressus	(Marsham, 1802)
Aplocnemus integer	Baudi di Selve, 1873
Aplocnemus kiesenwetteri	Schilsky, 1897
Aplocnemus nigricornis	(Fabricius, 1792)
Aplocnemus pulverulentus	(Küster, 1850)
Aplocnemus serbicus	Kiesenwetter, 1863
Aplocnemus tarsalis	(C.R. Sahlberg, 1882)
Aplocnemus virens	(Suffrian, 1843)
Aploderus caelatus	(Gravenhorst, 1802)
Aploderus caesus	(Erichson, 1839)
Apocheima hispidaria	(Denis & Schiffermüller, 1775)
Phigalia pilosaria	(Denis & Schiffermüller, 1775)
Apoda limacodes	(Hufnagel, 1766)
Apodemus agrarius	(Pallas, 1771)
Apodemus flavicollis	(Melchior, 1834)
Apodemus sylvaticus	(Linnaeus, 1758)
Apodemus uralensis	(Pallas, 1811)
Apoderus coryli	(Linnaeus, 1758)
Apoderus erythropterus	([Gmelin], 1790)
Apodia bifractella	(Duponchel, 1843)
Apolygus limbatus	(Fallén, 1829)
Apolygus lucorum	(Meyer-Dür, 1843)
Apolygus spinolae	(Meyer-Dür, 1841)
Apomyelois bistriatella nephanes	Hulst, 1887
Apomyelois ceratoniae	(Zeller, 1839)
Aporia crataegi	(Linnaeus, 1758)
Aporinellus moestus	(Klug, 1835)
Aporinellus moestus sericeomaculatus	(Klug, 1835)
Aporinellus obtusus	(Gussakovskij, 1935)
Aporinellus sexmaculatus	(Spinola, 1805)
Aporodes floralis	(Hübner, 1809)
Aporophyla lutulenta	([Denis & Schiffermüller], 1775)
Dermestoides sanguinicollis	(Fabricius, 1782)
Aporus bicolor	Spinola, 1808
Aporus pollux	(Kohl, 1888)
Aporus unicolor	Spinola, 1808
Aposericoderus revelierei	(Reitter, 1878)
Apostenus fuscus	Westring, 1851
Apotomis betuletana	(Haworth, 1811)
Apotomis capreana	(Hübner, 1817)
Apotomis inundana	(Denis & Schiffermüller, 1775)
Apotomis lineana	(Denis & Schiffermüller, 1775)
Apotomis sauciana	(Frölich, 1828)
Apotomis semifasciana	(Haworth, 1811)
Apotomis sororculana	(Zetterstedt, 1839)
Apotomis turbidana	(Hübner, 1825)
Apristus subaeneus	Chaudoir, 1846
Aproaerema anthyllidella	(Hübner, 1813)
Apsis albolineata	(Fabricius, 1792)
Apterola lownii	(Saunders, 1876)
Apterona helicoidella	(Vallot, 1827) (parth. form)
Aptinus bombarda	(Illiger, 1800)
Apus apus	(Linnaeus, 1758)
Tachymarptis melba	(Linnaeus, 1758)
Apus pallidus	(Shelley, 1870)
Aquarius najas	(De Geer, 1773)
Aquarius paludum	(Fabricius, 1784)
Aquila chrysaetos	(Linnaeus, 1758)
Aquila clanga	Pallas, 1811
Aquila heliaca	Savigny, 1809
Aquila nipalensis	Hodgson, 1833
Aquila nipalensis orientalis	Hodgson, 1833
Aquila pomarina	Ch. L. Brehm, 1831
Arachnospila abnormis	(Dahlbom, 1842)
Arachnospila alvarabnormis	Wolf, 1965
Arachnospila anceps	(Wesmael, 1851)
Arachnospila ausa	(Tournier, 1890)
Arachnospila conjungens	(Kohl, 1898)
Arachnospila fumipennis	(Zetterstedt, 1838)
Arachnospila fuscomarginata	(Thomson, 1870)
Arachnospila gibbomima	(Haupt, 1929)
Arachnospila minutula	(Dahlbom, 1843)
Arachnospila opinata	(Tournier, 1890)
Arachnospila rufa	(Haupt, 1927)
Arachnospila sogdiana	(Haupt, 1927)
Arachnospila spissa	(Schiödte, 1837)
Arachnospila trivialis	(Dahlbom, 1843)
Arachnospila wesmaeli	(Thomson, 1870)
Arachnoteutes rufithorax	(Costa, 1887)
Aradus betulae	(Linnaeus, 1758)
Aradus betulinus	Fallén, 1829
Aradus bimaculatus	Reuter, 1872
Aradus brenskei	Reuter, 1884
Aradus brevicollis	Fallén, 1807
Aradus cinnamomeus	(Panzer, 1794)
Aradus conspicuus	(Herrich-Schäffer, 1835)
Aradus corticalis	Linnaeus, 1758
Aradus depressus	(Fabricius, 1794)
Aradus distinctus	Fieber, 1861
Aradus krueperi	Reuter, 1884
Aradus kuthyi	Horváth, 1899
Aradus lugubris	Fallén, 1807
Aradus mirus	Bergroth, 1894
Aradus pallescens	(Herrich-Schäffer, 1840)
Aradus ribauti	Wagner, 1956
Aradus serbicus	Horváth, 1888
Aradus signaticornis	R. F. Sahlberg, 1848
Aradus truncatus	Fieber, 1861
Aradus versicolor	(Herrich-Schäffer, 1835)
Aracerus fasciculatus	(DeGeer, 1775)
Melanopsacus grenieri	(Ch. Brisout, 1867)
Araeoncus anguineus	(L. Koch, 1869)
Araeoncus crassipes	(Westring, 1861)
Araeoncus humilis	(Blackwall, 1841)
Araneus alsine	(Walckenaer, 1802)
Araneus angulatus	Clerck, 1757
Araneus circe	(Audouin, 1826)
Araneus diadematus	Clerck, 1757
Araneus grossus	(C.L. Koch, 1844)
Araneus marmoreus	Clerck, 1757
Araneus quadratus	Clerck, 1757
Araneus sturmi	(Hahn, 1831)
Araneus triguttatus	(Fabricius, 1775)
Araniella alpica	(L. Koch, 1869)
Araniella cucurbitina	(Clerck, 1757)
Araniella displicata	(Hentz, 1847)
Araniella inconspicua	(Simon, 1874)
Araniella opisthographa	(Kulczynski, 1905)
Araschnia levana	(Linnaeus, 1758)
Arboridia erecta	(Ribaut, 1931)
Arboridia parvula	(Boheman, 1845)
Arboridia pusilla	(Ribaut, 1936)
Arboridia ribauti	(Ossiannilsson, 1937)
Arboridia spatulata	(Ribaut, 1931)
Arboridia velata	(Ribaut, 1952)
Archaeodictyna consecuta	(O.P.-Cambridge, 1872)
Archanara algae	(Esper, 1789)
Archanara dissoluta	(Treitschke, 1825)
Archanara geminipuncta	(Haworth, 1809)
Archanara neurica	(Hübner, 1808)
Archanara sparganii	(Esper, 1790)
Archarius crux	(Fabricius, 1776)
Archarius pyrrhoceras	(Marsham, 1802)
Archarius salicivorus	(Paykull, 1792)
Archiboreoiulus pallidus	(Brade-Birks, 1920)
Archiearis notha	(Hübner, 1803)
Archiearis parthenias	(Linnaeus, 1761)
Archiearis puella	(Esper, 1787)
Archinemapogon yildizae	Koçak, 1981
Archips betulana	(Hübner, 1787)
Archips crataegana	(Hübner, 1799)
Archips oporana	(Linnaeus, 1758)
Archips podana	(Scopoli, 1763)
Archips rosana	(Linnaeus, 1758)
Archips xylosteana	(Linnaeus, 1758)
Arctia caja	(Linnaeus, 1758)
Arctia festiva	(Hufnagel, 1766)
Arctia villica	(Linnaeus, 1758)
Arctodiaptomus bacillifer	(Koelbel, 1885)
Arctodiaptomus salinus	Daday, 1885
Arctodiaptomus spinosus	(Daday, 1891)
Arctodiaptomus wierzejskii	(Richard, 1888)
Arctophila bombiformis	(Fallén, 1810)
Arctophila superbiens	(Müller, 1776)
Arctornis l-nigrum	(Müller, 1764)
Arctorthezia cataphracta	(Olafsen,1772)
Arctosa cinerea	(Fabricius, 1777)
Arctosa figurata	(Simon, 1876)
Arctosa leopardus	(Sundevall, 1833)
Arctosa lutetiana	(Simon, 1876)
Arctosa maculata	Hahn, 1822
Arctosa perita	(Latreille, 1799)
Arcyptera fusca	(Pallas, 1773)
Arcyptera microptera	(Fischer-Waldheim, 1833)
Ardea cinerea	Linnaeus, 1758
Ardea purpurea	Linnaeus, 1766
Ardeola bacchus	(Bonaparte, 1855)
Ardeola ralloides	(Scopoli, 1769)
Arenaria interpres	(Linnaeus, 1758)
Arenocoris fallenii	(Schilling, 1829)
Arenostola semicana	(Esper, 1798)
Arethusana arethusa	([Denis & Schiffermüller], 1775)
Argenna patula	(Simon, 1874)
Argenna subnigra	(O.P.-Cambridge, 1861)
Argiope bruennichi	(Scopoli, 1772)
Argiope lobata	(Pallas, 1772)
Argna bielzi	(Rossmässler, 1859)
Argogorytes fargeii	(Shuckard, 1837)
Argogorytes mystaceus	(Linnaeus, 1761)
Argolamprotes micella	(Denis & Schiffermüller, 1775)
Argulus coregoni	(Thorell, 1864)
Argulus foliaceus	(Linnaeus, 1758)
Argynnis adippe	([Denis & Schiffermüller], 1775)
Argynnis aglaja	(Linnaeus, 1758)
Argynnis laodice	(Pallas, 1771)
Argynnis niobe	(Linnaeus, 1758)
Argynnis pandora	([Denis & Schiffermüller], 1775)
Argynnis paphia	(Linnaeus, 1758)
Argyresthia abdominalis	Zeller, 1839
Argyresthia albistria	(Haworth, 1828)
Argyresthia arceuthina	Zeller, 1839
Argyresthia bonnetella	(Linnaeus, 1758)
Argyresthia brockeella	(Hübner, 1813)
Argyresthia conjugella	Zeller, 1839
Argyresthia curvella	(Linnaeus, 1761)
Argyresthia dilectella	Zeller, 1847
Argyresthia glaucinella	Zeller, 1839
Argyresthia goedartella	(Linnaeus, 1758)
Argyresthia ivella	(Haworth, 1828)
Argyresthia laevigatella	(Heydenreich, 1851)
Argyresthia praecocella	Zeller, 1839
Argyresthia pruniella	(Clerck, 1759)
Argyresthia pygmaeella	(Denis & Schiffermüller, 1775)
Argyresthia retinella	Zeller, 1839
Argyresthia semifusca	(Haworth, 1828)
Argyresthia semitestacella	(Curtis, 1833)
Argyresthia sorbiella	(Treitschke, 1833)
Argyresthia spinosella	Stainton, 1849
Argyresthia thuiella	(Pacard, 1871)
Argyroneta aquatica	(Clerck, 1757)
Argyrotaenia ljungiana	(Thunberg, 1797)
Arhopalus ferus	(Mulsant, 1839)
Arhopalus rusticus	(Linnaeus, 1758)
Arianta arbustorum	(Linnaeus, 1758)
Arichanna melanaria	(Linnaeus, 1758)
Plebeius agestis	(Denis & Schiffermüller, 1775)
Plebeius artaxerxes	Balogh, 1956
Plebeius artaxerxes issekutzi	Balogh, 1956
Plebeius eumedon	(Esper, 1780)
Arion circumscriptus	Johnston, 1828
Arion distinctus	J. Mabille, 1868
Arion fasciatus	(Nilsson, 1823)
Arion fuscus	(O.F. Müller, 1774)
Arion hortensis	Férussac, 1819
Arion lusitanicus	J. Mabille, 1868
Arion rufus	(Linnaeus, 1758)
Arion silvaticus	Lohmander, 1937
Aristotelia calastomella	(Christoph,1872)
Aristotelia decurtella	(Hübner, 1813)
Aristotelia ericinella	(Zeller, 1839)
Aristotelia subdecurtella	(Staiton, 1858)
Aristotelia subericinella	(Duponchel, 1843)
Arma custos	(Fabricius, 1794)
Arma insperata	Horváth, 1899
Arocatus longiceps	Stal, 1872
Arocatus melanocephalus	(Fabricius, 1798)
Arocatus roeselii	(Schilling, 1829)
Arocephalus languidus	(Flor, 1861)
Arocephalus longiceps	(Kirschbaum, 1868)
Aroga flavicomella	(Zeller, 1839)
Aroga velocella	(Zeller, 1839)
Aromia moschata	(Linnaeus, 1758)
Arpedium quadrum	(Gravenhorst, 1806)
Arthaldeus arenarius	Remane, 1960
Arthaldeus pascuellus	(Fallén, 1826)
Arthaldeus striifrons	(Kirschbaum, 1868)
Arthrolips convexiuscula	(Motschulsky, 1849)
Arthrolips hetschkoi	(Reitter, 1913)
Arthrolips nana	(Mulsant & Rey, 1861)
Arthrolips obscura	(C.R. Sahlberg, 1833)
Arthrolips picea	(Comolli, 1837)
Artianus interstitialis	(Germar, 1821)
Artianus manderstjernii	(Kirschbaum, 1868)
Artiora evonymaria	(Denis & Schiffermüller, 1775)
Arvicola terrestris scherman	(Linnaeus, 1758)
Arytrura musculus	(Ménétriés, 1859)
Asaphidion austriacum	Schweiger, 1975
Asaphidion caraboides	(Schrank, 1781)
Asaphidion flavipes	(Linnaeus, 1761)
Asaphidion pallipes	(Duftschmid, 1812)
Asarta aethiopella	(Duponchel, 1837)
Ascalenia vanella	(Frey, 1860)
Ascotis selenaria	(Denis & Schiffermüller, 1775)
Asemum striatum	(Linnaeus, 1758)
Asianellus festivus	(C.L. Koch, 1834)
Asio flammeus	Pontoppidan, 1763)
Asio otus	(Linnaeus, 1758)
Asiraca clavicornis	(Fabricus, 1794)
Asphalia ruficollis	([Denis & Schiffermüller], 1775)
Aspidapion aeneum	(Fabricius, 1775)
Aspidapion radiolus	(Marsham, 1802)
Aspidapion validum	(Germar, 1817)
Aspidiphorus lareyniei	Jacquelin du Val, 1859
Aspidiphorus orbiculatus	(Gyllenhal, 1808)
Grapholita funebrana	Treitschke, 1835
Grapholita janthinana	(Duponchel, 1843)
Grapholita lobarzewskii	(Nowicki, 1860)
Grapholita molesta	(Busck, 1916)
Grapholita tenebrosana	Duponchel, 1843
Aspilapteryx limosella	(Duponchel, 1844)
Aspilapteryx tringipennella	(Zeller, 1839)
Aspitates gilvaria	(Denis & Schiffermüller, 1775)
Aspius aspius	(Linnaeus, 1758)
Asproparthenis punctiventris	(Germar, 1824)
Assara terebrella	(Zincken, 1818)
Astacus astacus	(Linnaeus, 1758)
Astacus leptodactylus	Eschscholz, 1823
Astata apostata	Mercet, 1910
Astata boops	(Schrank, 1781)
Astata brevitarsis	Pulawski, 1958
Astata costae	Costa, 1867
Astata gallica	Beaumont, 1942
Astata jucunda	Pulawski, 1959
Astata kashmirensis	Nurse, 1909
Astata minor	Kohl, 1885
Astata quettae	Nurse, 1903
Astata rufipes	Mocsáry, 1883
Astatopteryx laticollis	Perris, 1862
Astenus bimaculatus	(Erichson, 1840)
Astenus gracilis	(Paykull, 1789)
Astenus immaculatus	Stephens, 1833
Astenus laticeps	(Petri, 1912)
Astenus lyonessius	(Joy, 1908)
Astenus procerus	(Gravenhorst, 1806)
Astenus pulchellus	(Heer, 1839)
Astenus rutilipennis	Reitter, 1909
Astenus uniformis	Jacquelin du Val, 1856
Asterodiaspis bella	(Russell,1941)
Asterodiaspis quercicola	(Bouché,1851)
Asterodiaspis roboris	(Russell,1941)
Asterodiaspis variolosa	(Ratzeburg,1870)
Asterodiaspis viennae	(Russell,1941)
Asteroscopus sphinx	(Hufnagel, 1766)
Asteroscopus syriaca	(Warren, 1910)
Asteroscopus syriaca decipulae	(Warren, 1910)
Asthena albulata	(Hufnagel, 1767)
Asthena anseraria	(Herrich-Schäffer, 1855)
Astrapaeus ulmi	(Rossi, 1790)
Atanygnathus terminalis	(Erichson, 1839)
Ateliotum hungaricellum	Zeller, 1839
Atemelia torquatella	(Lienig & Zeller, 1846)
Aterpia corticana	(Denis & Schiffermüller, 1775)
Atethmia ambusta	([Denis & Schiffermüller], 1775)
Atethmia centrago	(Haworth, 1809)
Athene noctua	(Scopoli, 1769)
Atheta aeneicollis	(Sharp, 1869)
Atheta aquatica	(Thomson, 1852)
Atheta autumnalis	(Erichson, 1839)
Atheta basicornis	(Mulsant et Rey, 1852)
Atheta boletophila	(Thomson, 1856)
Atheta britanniae	Bernhauer et Scheerpeltz, 1926
Atheta castanoptera	(Mannerheim, 1830)
Atheta crassicornis	(Fabricius, 1792)
Atheta divisa	(Märkel, 1844)
Atheta euryptera	(Stephens, 1832)
Atheta fungicola	(Thomson, 1852)
Atheta gagatina	(Baudi, 1848)
Atheta graminicola	(Gravenhorst, 1806)
Atheta harwoodi	Williams, 1930
Atheta hybrida	(Sharp, 1869)
Atheta hypnorum	(Kiesenwetter, 1850)
Atheta intermedia	(Thomson, 1852)
Atheta liturata	(Stephens, 1832)
Atheta nidicola	(Johansen, 1914)
Atheta nigricornis	(Thomson, 1852)
Atheta nigritula	(Gravenhorst, 1802)
Atheta oblita	(Erichson, 1839)
Atheta pallidicornis	(Thomson, 1856)
Atheta pilicornis	(Thomson, 1852)
Atheta ravilla	(Erichson, 1839)
Atheta sodalis	(Erichson, 1837)
Atheta spelaea	(Erichson, 1839)
Atheta triangulum	(Kraatz, 1856)
Atheta trinotata	(Kraatz, 1856)
Atheta xanthopus	(Thomson, 1856)
Athetis furvula	(Hübner, 1808)
Athetis gluteosa	(Treitschke, 1835)
Athetis pallustris	(Hübner, 1808)
Atholus bimaculatus	(Linnaeus, 1758)
Atholus corvinus	(Germar, 1817)
Atholus duodecimstriatus	(Schrank, 1782)
Atholus duodecimstriatus quatuordecimstriatus	(Schrank, 1782)
Atholus praetermissus	(Peyron, 1856)
Athous apfelbecki	Reitter, 1905
Athous austriacus	Desbrochers des Loges, 1873
Athous bicolor	(Goeze, 1777)
Athous haemorrhoidalis	(Fabricius, 1801)
Athous kaszabi	Dolin, 1986
Athous silicensis	Laibner, 1975
Athous subfuscus	(O.F. Müller, 1764)
Athous vittatus	(Fabricius, 1792)
Athrips mouffetella	(Linnaeus, 1758)
Athrips nigricostella	(Duponchel, 1842)
Athrips rancidella	(Herrich-Schäffer, 1854)
Athripsodes albifrons	(Linnaeus, 1758)
Athripsodes aterrimus	(Stephens, 1836)
Athripsodes bilineatus	(Linnaeus, 1758)
Athripsodes cinereus	(Curtis, 1834)
Athripsodes commutatus	(Rostock, 1874)
Athysanus argentarius	Metcalf, 1955
Athysanus quadrum	Boheman, 1845
Atolmis rubricollis	(Linnaeus, 1758)
Atomaria affinis	C.R. Sahlberg, 1834
Atomaria alpina	Heer, 1841
Atomaria analis	Erichson, 1846
Atomaria apicalis	Erichson, 1846
Atomaria atra	(Herbst, 1793)
Atomaria atrata	Reitter, 1875
Atomaria atricapilla	Stephens, 1830
Atomaria attila	Reitter, 1878
Atomaria badia	Erichson, 1846
Atomaria barani	Brisout de Barneville, 1863
Atomaria basicornis	Reitter, 1887
Atomaria bella	Reitter, 1875
Atomaria clavigera	Ganglbauer, 1899
Atomaria elongatula	Erichson, 1846
Atomaria fimetarii	(Herbst, 1793)
Atomaria fuscata	(Schönherr, 1808)
Atomaria fuscicollis	Mannerheim, 1852
Atomaria fuscipes	(Gyllenhal, 1808)
Atomaria gibbula	Erichson, 1846
Atomaria gravidula	Erichson, 1846
Atomaria gutta	Stephens, 1830
Atomaria impressa	Erichson, 1846
Atomaria jonica	Reitter, 1887
Atomaria lewisi	Reitter, 1877
Atomaria linearis	Stephens, 1830
Atomaria mesomelaena	(Herbst, 1792)
Atomaria morio	Kolenati, 1846
Atomaria munda	Erichson, 1846
Atomaria nigripennis	(Kugelann, 1792)
Atomaria nigriventris	Stephens, 1830
Atomaria nitidula	(Marsham, 1802)
Atomaria peltata	Kraatz, 1853
Atomaria pulchra	Erichson, 1946
Atomaria punctithorax	Reitter, 1887
Atomaria pusilla	(Paykull, 1798)
Atomaria rubella	Heer, 1841
Atomaria rubida	Reitter, 1875
Atomaria rubricollis	Brisout de Barneville, 1862
Atomaria slavonica	Johnson, 1971
Atomaria soedermanni	Sjöberg, 1947
Atomaria testacea	Stephens 1830
Atomaria umbrina	(Gyllenhal, 1827)
Atomaria unifasciata	Erichson, 1846
Atomoscelis onusta	(Fieber, 1861)
Atractotomus magnicornis	(Fallén, 1807)
Atractotomus mali	(Meyer-Dür, 1843)
Atractotomus parvulus	Reuter, 1878
Atralata albofascialis	(Treitschke, 1829)
Atrecus affinis	(Paykull, 1789)
Atrecus longiceps	(Fauvel, 1873)
Atremaea lonchoptera	Staudinger, 1871
Atrococcus achilleae	(Kiritchenko,1936)
Atrococcus bejbienkoi	Kozár et Danzig,1976
Atrococcus cracens	Williams,1962
Atrococcus paludinus	(Green,1921)
Attaephilus arenarius	(Hampe, 1852)
Attagenus brunneus	Faldermann, 1835
Attagenus pellio	(Linnaeus, 1758)
Attagenus punctatus	(Scopoli, 1772)
Attagenus schaefferi	(Herbst, 1792)
Attagenus unicolor	(Brahm, 1791)
Attalus analis	(Panzer, 1796)
Attalus thalassimus	Abeille de Perrin, 1883
Attelabus nitens	(Scopoli, 1763)
Attheyella crassa	Sars, 1862
Attheyella trispinosa	Brady, 1880
Attheyella wierzejski	Mrazek, 1893
Atylotus fulvus	(Meigen, 1820)
Atylotus loewianus	(Villeneuve, 1920)
Atylotus rusticus	(Linné, 1767)
Atypha pulmonaris	(Esper, 1790)
Atypus affinis	Eichwald, 1830
Atypus muralis	Bertkau, 1890
Atypus piceus	(Sulzer, 1776)
Auchmis detersa	(Esper, 1787)
Augasma aeratella	(Zeller, 1839)
Aulacaspis rosae	(Bouché,1833)
Aulacobaris angusta	Brullé, 1832
Aulacobaris chlorizans	(Germar, 1824)
Aulacobaris coerulescens	(Scopoli, 1763)
Aulacobaris gudenusi	Schultze, 1901
Aulacobaris kaufmanni	(Reitter, 1897)
Aulacobaris lepidii	Germar, 1824
Aulacobaris picicornis	(Marsham, 1802)
Aulacobaris villae	Comolli, 1837
Aulacochthebius exaratus	(Mulsant, 1844)
Aulacochthebius narentinus	(Reitter, 1885)
Auletobius sanguisorbae	(Schrank, 1798)
Auleutes epilobii	(Paykull, 1800)
Aulonia albimana	(Walckenaer, 1805)
Aulonium ruficorne	(Olivier, 1790)
Aulonium trisulcum	(Geoffroy, 1785)
Aulonogyrus concinnus	(Klug, 1834)
Aulonothroscus brevicollis	(Bonvouloir, 1859)
Auplopus albifrons	(Dalman, 1823)
Auplopus carbonarius	(Scopoli, 1763)
Auplopus rectus	(Haupt, 1936)
Perotis lugubris	(Fabricius, 1777)
Austroagallia sinuata	(Mulsant et Rey, 1855)
Austroasca vittata	(Lethierry, 1884)
Austropotamobius torrentium	(Schrank, 1805)
Autalia impressa	(Olivier, 1795)
Autalia rivularis	(Gravenhorst, 1802)
Autographa bractea	([Denis & Schiffermüller], 1775)
Autographa gamma	(Linnaeus, 1758)
Autographa jota	(Linnaeus, 1758)
Autographa pulchrina	(Haworth, 1809)
Axinopalpis gracilis	(Krynicki, 1832)
Axinotarsus marginalis	(Laporte de Castelnau, 1840)
Axinotarsus pulicarius	(Fabricius, 1775)
Axinotarsus ruficollis	(Olivier, 1790)
Axylia putris	(Linnaeus, 1761)
Aythya affinis	(Eyton, 1838)
Aythya collaris	(Donovan, 1809)
Aythya ferina	(Linnaeus, 1758)
Aythya fuligula	(Linnaeus, 1758)
Aythya marila	(Linnaeus, 1761)
Aythya nyroca	(Güldenstadt, 1770)
Cyclobacanius soliman	(Marseul, 1862)
Baccha elongata	(Fabricius, 1775)
Baccha obscuripennis	Meigen, 1822
Bacotia claustrella	(Bruand, 1845)
Bactra furfurana	(Haworth, 1811)
Bactra lacteana	(Caradja, 1916)
Bactra lancealana	(Hübner, 1799)
Bactra robustana	(Christoph, 1872)
Badister bullatus	(Schrank, 1798)
Badister collaris	Motschulsky, 1844
Badister dilatatus	Chaudoir, 1837
Badister dorsiger	(Duftschmid, 1812)
Badister lacertosus	Sturm, 1815
Badister meridionalis	Puel, 1925
Badister peltatus	(Panzer, 1797)
Badister sodalis	(Duftschmid, 1812)
Badister unipustulatus	Bonelli, 1813
Badura cauta	(Erichson, 1837)
Badura ischnocera	(Thomson, 1870)
Badura macrocera	(Thomson, 1856)
Baeocrara japonica	(Matthews, 1855)
Baetis alpinus	(Pictet, 1843)
Baetis buceratus	Eaton, 1870
Baetis fuscatus	(Linnaeus, 1761)
Baetis gracilis	Bogoescu et Tabacaru, 1957
Baetis lutheri	Müller-Liebenau, 1967
Baetis muticus	(Linnaeus, 1758)
Baetis niger	(Linnaeus, 1761)
Baetis nexus	Navás, 1918
Baetis rhodani	(Pictet, 1843)
Baetis scambus	Eaton, 1870
Baetis tracheatus	Keffermüller et Machel, 1967
Baetis tricolor	Tshernova, 1928
Baetis vardarensis	Ikonomov, 1962
Baetis vernus	Curtis, 1834
Baetopus tenellus	(Albarda, 1878)
Bagoopsis globicollis	(Fairmaire, 1863)
Bagous alismatis	(Marsham, 1802)
Bagous angustus	Silfverberg, 1977
Bagous argillaceus	Gyllenhal, 1836
Bagous bagdatensis	Pic, 1904
Bagous binodulus	(Herbst, 1795)
Bagous collignensis	(Herbst, 1797)
Bagous czwalinae	Seidlitz, 1891
Bagous diglyptus	Boheman, 1845
Bagous elegans	(Fabricius, 1801)
Bagous frit	(Herbst, 1795)
Bagous frivaldszkyi	Tournier, 1874
Bagous geniculatus	(Hochhuth, 1847)
Bagous glabrirostris	(Herbst, 1795)
Bagous limosus	(Gyllenhal, 1827)
Bagous longitarsis	Thomson, 1868
Bagous lutosus	(Gyllenhal, 1813)
Bagous lutulentus	(Gyllenhal, 1813)
Bagous lutulosus	(Gyllenhal, 1827)
Bagous majzlani	Kodada, Holecová & Behne, 1992
Bagous nodulosus	Gyllenhal, 1836
Bagous petro	(Herbst, 1795)
Bagous puncticollis	Boheman, 1845
Bagous robustus	H. Brisout, 1863
Bagous rotundicollis	Boheman, 1845
Bagous subcarinatus	Gyllenhal, 1836
Bagous tempestivus	(Herbst, 1795)
Bagous tubulus	Caldara & O'brien, 1994
Bagous validus	Rosenhauer, 1847
Bagrada stolata	Horváth, 1936
Balanococcus boratynskii	Williams,1962
Balcanocerus larvatus	(Herrich-Schäffer, 1835)
Balclutha calamagrostis	Ossiannilsson, 1961
Balclutha punctata	(Fabricius, 1775)
Balclutha rhenana	Wagner, 1939
Balclutha saltuella	(Kirschbaum, 1868)
Balea biplicata	(Montagu, 1803)
Balea perversa	(Linnaeus, 1758)
Balea stabilis	(L. Pfeiffer, 1847)
Ballus chalybeius	(Walckenaer, 1802)
Ballus rufipes	(Simon, 1868)
Bangasternus orientalis	(Capiomont, 1873)
Barbastella barbastellus	(Schreber, 1774)
Barbatula barbatula	(Linnaeus, 1758)
Barbitistes constrictus	Brunner on Wattenwyl, 1878
Barbitistes serricauda	Fieber, 1798
Barbus barbus	(Linnaeus, 1758)
Barbus meridionalis	Risso, 1827
Barbus peloponnesius	Valenciennes, 1842
Barbus carpathicus	Kotlík, Tsigenopoulos, Ráb & Berrebi 2002
Baris analis	(Olivier, 1790)
Baris artemisiae	(Herbst, 1795)
Baris nesapia	Faust, 1887
Baris steppensis	Roubal, 1935
Barynotus hungaricus	(Tournier, 1876)
Barynotus moerens	(Fabricius, 1792)
Barynotus obscurus	(Fabricius, 1775)
Barypeithes araneiformis	(Schrank, 1781)
Barypeithes chevrolati	(Boheman, 1843)
Barypeithes formaneki	Fremuth, 1971
Barypeithes interpositus	(Roubal, 1920)
Barypeithes interpositus siliciensis	(Roubal, 1920)
Barypeithes liptoviensis	J. Weise, 1894
Barypeithes mollicomus	(Ahrens, 1812)
Barypeithes pellucidus	(Boheman, 1834)
Barypeithes styriacus	Seidlitz, 1868
Barypeithes tenex	(Boheman, 1843)
Basistriga flammatra	([Denis & Schiffermüller], 1775)
Batazonellus lacerticida	(Pallas, 1771)
Bathynella natans	Vejdovsky, 1882
Bathyomphalus contortus	(Linnaeus, 1758)
Bathyphantes approximatus	(O.P.-Cambridge, 1871)
Bathyphantes gracilis	(Blackwall, 1841)
Bathyphantes nigrinus	(Westring, 1851)
Bathyphantes similis	Kulczynski, 1894
Bathysolen nubilus	(Fallén, 1807)
Batia internella	Jäckh, 1972
Batia lambdella	(Donovan, 1793)
Batia lunaris	(Haworth, 1828)
Batrachedra pinicolella	(Zeller, 1839)
Batrachedra praeangusta	(Haworth, 1828)
Batracobdelloides moogi	Nesemann & Csányi, 1995
Batracomorphus allionii	(Turton, 1802)
Batracomorphus irroratus	Lewis, 1834
Batrisodes adnexus	(Hampe, 1863)
Batrisodes delaporti	(Aubé, 1833)
Batrisodes oculatus	(Aubé, 1833)
Batrisodes sulcaticeps	Besuchet, 1981
Batrisodes venustus	(Reichenbach, 1816)
Batrisus formicarius	Aubé, 1833
Bedellia ehikella	Szöcs, 1967
Bedellia somnulentella	(Zeller, 1847)
Belomicroides zimini	(Gussakovskij, 1952)
Belomicrus antennalis	Kohl, 1809
Belomicrus italicus	Costa, 1871
Bembecia albanensis	(Rebel, 1918)
Bembecia ichneumoniformis	(Denis & Schiffermüller, 1775)
Bembecia megillaeformis	(Hübner, 1813)
Bembecia puella	Lastuvka, 1918
Bembecia scopigera	(Scopoli, 1763)
Bembecia uroceriformis	(Treitschke, 1834)
Bembecinus hungaricus	(Frivaldszky, 1889)
Bembecinus tridens	(Fabricius, 1781)
Odontium argenteolum	(Ahrens, 1812)
Trepanes articulatus	(Panzer, 1796)
Trepanes assimilis	(Gyllenhal, 1810)
Emphanes azurescens	(Dalla Torre, 1877)
Philochthus biguttatus	(Fabricius, 1779)
Ocydromus cruciatus	(Jacquelin du Val, 1852)
Ocydromus cruciatus bualei	(Jacquelin du Val, 1852)
Ocydromus dalmatinus	(Dejean, 1831)
Sinechostictus decoratus	(Duftschmid, 1812)
Ocydromus decorus	(Panzer, 1800)
Ocydromus deletus	(Audinet-Serville, 1821)
Notaphus dentellus	(Thunberg, 1787)
Sinechostictus doderoi	(Ganglbauer, 1891)
Trepanes doris	(Panzer, 1797)
Sinechostictus elongatus	(Dejean, 1831)
Notaphus ephippium	(Marsham, 1802)
Ocydromus fasciolatus	(Duftschmid, 1812)
Ocydromus femoratus	(Sturm, 1825)
Ocydromus fluviatilis	(Dejean, 1831)
Odontium foraminosum	(Sturm, 1825)
Ocydromus fulvipes	(Sturm, 1827)
Trepanes fumigatus	(Duftschmid, 1812)
Bembidion geniculatum	Herr, 1837
Trepanes gilvipes	(Sturm, 1825)
Philochthus guttula	(Fabricius, 1792)
Ocydromus tetragrammus illigeri	(Netolitzky, 1914)
Philochthus inoptatus	(Schaum, 1857)
Sinechostictus inustum	(Jacquelin du Val, 1857)
Metallina lampros	(Herbst, 1784)
Odontium laticolle	(Duftschmid, 1812)
Emphanes latiplaga	(Chaudoir, 1850)
Odontium litorale	(Olivier, 1790)
Philochthus lunulatus	(Geffroy in Fourcroy, 1785)
Ocydromus lunulatus	(Fourcroy, 1785)
Philochthus mannerheimii	(C.R. Sahlberg, 1827)
Emphanes minimus	(Fabricius, 1792)
Ocydromus modestus	(Fabricius, 1801)
Ocydromus monticola	(Sturm, 1825)
Phyla obtusa	(Audinet-Serville, 1821)
Trepanes octomaculatus	(Goeze, 1777)
Plataphus prasinus	(Duftschmid, 1812)
Metallina properans	(Stephens, 1828)
Princidium punctulatum	(Drapiez, 1821)
Metallina pygmaea	(Fabricius, 1792)
Bembidion quadrimaculatum	(Linnaeus, 1761)
Bembidion quadripustulatum	Audinet-Serville, 1821
Bembidion rivulare	Gyllenhal, 1810
Sinechostictus ruficornis	(Sturm, 1825)
Trepanes schueppelii	(Dejean, 1831)
Notaphus semipunctatus	(Donovan, 1806)
Metallina splendida	(Sturm, 1825)
Notaphus starkii	(Schaum, 1860)
Ocydromus stephensii	(Crotch, 1866)
Sinechostictus stomoides	(Dejean, 1831)
Odontium striatum	(Fabricius, 1792)
Ocydromus subcostatus	(Motschulsky, 1850)
Ocydromus subcostatus javurkovae	(Fassati, 1944)
Emphanes tenellus	(Erichson, 1837)
Ocydromus testaceus	(Duftschmid, 1812)
Ocydromus tetracolus	(Say, 1823)
Ocydromus tibialis	(Duftschmid, 1812)
Notaphus varius	(Olivier, 1795)
Odontium velox	(Linnaeus, 1761)
Bembix bidentata	Vander Linden, 1829
Bembix megerlei	Dahlbom, 1845
Bembix oculata	Panzer, 1801
Bembix olivacea	Fabricius, 1787
Bembix rostrata	(Linnaeus, 1758)
Bembix tarsata	Latreille, 1809
Bena bicolorana	(Fuessly, 1775)
Benibotarus taygetanus	(Pic, 1905)
Beosus maritimus	(Scopoli, 1763)
Beosus quadripunctatus	(Müller, 1766)
Beraea maurus	(Curtis, 1834)
Beraea pullata	(Curtis, 1834)
Beraeamyia hrabei	Mayer, 1937
Beraeodes minutus	(Linnaeus, 1761)
Berginus tamarisci	(Wollaston, 1854)
Berlandina cinerea	(Menge, 1872)
Berosus affinis	Brullé, 1835
Berosus frontifoveatus	Kuwert, 1890
Berosus fulvus	Kuwert, 1888
Berosus geminus	Reiche et Saulcy, 1856
Berosus luridus	(Linnaeus, 1761)
Berosus signaticollis	(Charpentier, 1825)
Berosus spinosus	(Steven, 1808)
Berytinus clavipes	(Fabricius, 1775)
Berytinus consimilis	(Horváth, 1885)
Berytinus crassipes	(Herrich-Schäffer, 1835)
Berytinus distinguendus	(Ferrari, 1874)
Berytinus geniculatus	(Horváth, 1885)
Berytinus hirticornis	(Brullé, 1835)
Berytinus minor	(Herrich-Schäffer, 1835)
Berytinus montivagus	(Meyer-Dür, 1841)
Berytinus signoreti	(Fieber, 1859)
Berytinus striola	(Ferrari, 1874)
Besdolus ventralis	(Pictet, 1841)
Bessobia fungivora	Thomson, 1867
Bessobia occulta	(Erichson, 1837)
Bessobia spatula	(Fauvel, 1875)
Betarmon bisbimaculatus	(Fabricius, 1803)
Betulapion simile	(Kirby, 1811)
Biastes brevicornis	(Panzer, 1798)
Biastes emarginatus	(Schenck, 1853)
Bibloplectus ambiguus	(Reichenbach, 1816)
Bibloplectus delhermi	Guillebeau, 1888
Bibloplectus hungaricus	Basuchet, 1955
Bibloplectus minutissimus	(Aubé, 1833)
Bibloplectus obtusus	Guillebeau, 1888
Bibloplectus pusillus	(Denny, 1825)
Bibloplectus tenebrosus	(Reitter, 1880)
Bibloporus bicolor	(Denny, 1825)
Bibloporus mayeti	Guillebeau, 1888
Bibloporus minutus	Raffray, 1914
Bidessus grossepunctatus	Vorbringer, 1907
Bidessus nasutus	Sharp, 1887
Bidessus unistriatus	(Goeze, 1777)
Bielzia coerulans	(M. Bielz, 1851)
Bijugis bombycella	(Denis & Schiffermüller, 1775)
Bijugis pectinella	(Denis & Schiffermüller, 1775)
Biphyllus frater	Aubé, 1850
Biphyllus lunatus	(Fabricius, 1792)
Biralus satellitius	(Herbst, 1789)
Elachista utonella	Frey, 1856
Bisigna procerella	(Denis & Schiffermüller, 1775)
Bisnius binotatus	(Gravenhorst, 1802)
Bisnius cephalotes	(Gravenhorst, 1802)
Bisnius fimetarius	(Gravenhorst, 1802)
Bisnius nigriventris	(Thomson, 1867)
Bisnius nitidulus	(Gravenhorst, 1802)
Bisnius sparsus	(Lucas, 1849)
Bisnius pseudoparcus	(Brunne, 1976)
Bisnius scribae	(Fauvel, 1867)
Bisnius sordidus	(Gravenhorst, 1802)
Bisnius spermophili	(Ganglbauer, 1897)
Bisnius subuliformis	(Gravenhorst, 1802)
Biston betularia	(Linnaeus, 1758)
Biston strataria	(Hufnagel, 1767)
Bithynia leachii	(Sheppard, 1823)
Bithynia tentaculata	(Linnaeus, 1758)
Bithynia troschelii	(Paasch, 1842)
Bitoma crenata	(Fabricius, 1775)
Blaniulus guttulatus	(Fabricius, 1798)
Blaps abbreviata	Frivaldszky, 1836
Blaps halophila	W.Fischer, 1822
Blaps lethifera	Marsham, 1802
Blaps mortisaga	(Linnaeus, 1758)
Blastobasis huemeri	Sinev, 1993
Blastobasis phycidella	(Zeller, 1839)
Blastodacna atra	(Haworth, 1828)
Bothriomyrmex adriaticus	(Santchi, 1922)
Blastodacna hellerella	(Duponchel, 1838)
Bledius atricapillus	(Germar, 1825)
Bledius baudii	Fauvel, 1870
Bledius bicornis	(Germar, 1823)
Bledius crassicollis	Lacordaire, 1835
Bledius cribricollis	Heer, 1839
Bledius defensus	Fauvel, 1872
Bledius dissimilis	Erichson, 1840
Bledius erraticus	Erichson, 1839
Bledius fergussoni	(Joy, 1912)
Bledius fossor	Heer, 1839
Bledius furcatus	(Olivier, 1811)
Bledius gallicus	(Gravenhorst, 1806)
Bledius longulus	Erichson, 1839
Bledius nanus	Erichson, 1840
Bledius opacus	(Block, 1799)
Bledius pallipes	(Gravenhorst, 1806)
Bledius procerulus	Erichson, 1840
Bledius pusillus	Erichson, 1839
Bledius pygmaeus	Erichson, 1839
Bledius roubali	Horion, 1963
Bledius spectabilis	Kraatz, 1858
Bledius subterraneus	(Erichson, 1839)
Bledius tibialis	(Heer, 1839)
Bledius tricornis	(Herbst, 1784)
Bledius unicornis	(Germar, 1825)
Bledius verres	Erichson, 1840
Blepharidopterus angulatus	(Fallén, 1807)
Blepharidopterus diaphanus	(Kirschbaum, 1856)
Blera fallax	(Linnaeus, 1758)
Blethisa multipunctata	(Linnaeus, 1758)
Bobacella corvina	(Horváth, 1903)
Bodilus circumcinctus	(W. Schmidt, 1840)
Bodilus ictericus	(Laicharting, 1781)
Bodilus lugens	(Creutzer, 1799)
Bodilus punctipennis	(Erichson, 1848)
Boettgerilla pallens	Simroth, 1912
Bohemannia pulverosella	(Stainton, 1849)
Bolbelasmus unicornis	(Schrank, 1789)
Bolitobius castaneus	(Stephens, 1832)
Bolitobius cingulatus	Mannerheim, 1830
Parabolitobius formosus	(Gravenhorst, 1806)
Parabolitobius inclinans	(Gravenhorst, 1806)
Bolitochara bella	(Märkel, 1844)
Bolitochara lucida	(Gravenhorst, 1802)
Bolitochara mulsanti	(Sharp, 1875)
Bolitochara obliqua	(Erichson, 1837)
Bolitochara pulchra	(Gravenhorst, 1806)
Bolitochara reyi	(Sharp, 1875)
Bolitophagus interruptus	Illiger, 1800
Bolitophagus reticulatus	(Linnaeus, 1767)
Boloria dia	(Linnaeus, 1767)
Boloria euphrosyne	(Linnaeus, 1758)
Boloria selene	([Denis & Schiffermüller], 1775)
Bolyphantes alticeps	(Sundevall, 1833)
Bolyphantes luteolus	(Blackwall, 1833)
Bombina bombina	(Linnaeus, 1761)
Bombina variegata	(Linnaeus, 1758)
Bombus argillaceus	(Scopoli, 1763)
Bombus confusus	Schenck, 1859
Bombus distinguendus	Morawitz, 1869
Bombus elegans	Seidl, 1837
Bombus fragrans	(Pallas, 1771)
Bombus gerstaeckeri	Morawitz, 1882
Bombus haematurus	Kriechbaumer, 1870
Bombus hortorum	(Linnaeus, 1761)
Bombus humilis	Illiger, 1806
Bombus hypnorum	(Linnaeus, 1758)
Bombus hypnorum ericetorum	(Linnaeus, 1758)
Bombus laesus	Morawitz, 1875
Bombus laesus mocsaryi	Morawitz, 1875
Bombus lapidarius	(Linnaeus, 1758)
Bombus muscorum	(Linnaeus, 1758)
Bombus paradoxus	Dalla Torre, 1882
Bombus pascuorum	(Scopoli, 1763)
Bombus pomorum	(Panzer, 1805)
Bombus pratorum	(Linnaeus, 1761)
Bombus ruderarius	(Müller, 1776)
Bombus ruderatus	Fabricius, 1775
Bombus serrisquama	Morawitz, 1888
Bombus soroeensis	(Fabricius, 1776)
Bombus subterraneus	(Linnaeus, 1758)
Bombus sylvarum	(Linnaeus, 1758)
Bombus terrestris	(Linnaeus, 1758)
Bombus wurfleini	Radoszkowski, 1859
Bombus wurfleini mastrucatus	Radoszkowski, 1859
Bombycilla garrulus	(Linnaeus, 1758)
Bombyx mori	(Linnaeus, 1758)
Bonasa bonasia	(Linnaeus, 1758)
Bonasa bonasia rupestris	(Linnaeus, 1758)
Boreococcus ingricus	Danzig,1960
Boreoiulus tenuis	(Bigler, 1913)
Borkhausenia fuscescens	(Haworth, 1828)
Borkhausenia minutella	(Linnaeus, 1758)
Borysthenia naticina	(Menke, 1845)
Bosmina coregoni	Baird, 1857
Bosmina longirostris	(O.F. Müller, 1785)
Bostrichus capucinus	(Linnaeus, 1758)
Botaurus stellaris	(Linnaeus, 1758)
Bothrideres contractus	(Fabricius, 1792)
Bothrostethus annulipes	(Costa, 1847)
Bothynoderes affinis	(Schrank, 1781)
Bothynoderes declivis	(Olivier, 1807)
Bothynotus pilosus	(Boheman, 1852)
Brachida exigua	(Heer, 1839)
Brachinus bipustulatus	Quensel, 1806
Brachinus crepitans	(Linnaeus, 1758)
Brachinus ejaculans	Fischer, 1825
Brachinus explodens	Duftschmid, 1812
Brachinus elegans	Chaudoir, 1842
Brachinus nigricornis	Gebler, 1829
Brachinus plagiatus	Reiche, 1868
Brachinus psophia	Audinet-Serville, 1821
Brachionycha nubeculosa	(Esper, 1785)
Brachmia blandella	(Fabricius, 1798)
Brachmia dimidiella	(Denis & Schiffermüller, 1775)
Brachmia inornatella	(Douglas, 1850)
Brachmia procursella	Rebel, 1903
Brachodes appendiculata	(Esper, 1783)
Brachodes pumila	(Ochsenheimer, 1808)
Brachonyx pineti	(Paykull, 1792)
Brachycarenus tigrinus	(Schilling, 1817)
Brachycentrus subnubilus	(Curtis, 1834)
Tettigometra laeta	Herrich-Schäffer, 1835
Brachycercus europaeus	Kluge, 1991
Brachycercus harrisella	Curtis, 1834
Cercobrachys minutus	(Tshernova, 1952)
Brachycerus foveicollis	Gyllenhal, 1833
Brachycoleus decolor	Reuter, 1887
Brachycoleus pilicornis	(Panzer, 1806)
Brachyderes incanus	(Linnaeus, 1758)
Brachydesmus Attemsii	Verhoeff, 1895
Brachydesmus attemsii tenkesensis	Verhoeff, 1895
Brachydesmus Dadayi	Verhoeff, 1895
Brachydesmus superus	Latzel, 1884
Brachydesmus troglobius	Daday, 1889
Brachygluta fossulata	(Reichenbach, 1816)
Brachygluta haematica	(Reichenbach, 1816)
Brachygluta sinuata	(Aubé, 1833)
Brachygluta helferi	Schmidt-Goebel, 1836
Brachygluta helferi longispina	Schmidt-Goebel, 1836
Brachygluta retowskii	Simon, 1883
Brachygonus megerlei	(Lacordaire, 1835)
Brachygonus ruficeps	(Mulsant et Guillebeau, 1855)
Brachyiulus bagnalli	(Curtis, 1845)
Brachyiulus pusillus	Leach, 1815
Brachyleptus quadratus	(Sturm, 1844)
Brachylomia viminalis	(Fabricius, 1776)
Brachymyia berberina	(Fabricius, 1805)
Brachymyia floccosa	(Meigen, 1822)
Brachynotocoris puncticornis	Reuter, 1880
Brachyopa bicolor	(Fallén, 1817)
Brachyopa dorsata	Zetterstedt, 1838
Brachyopa insensilis	Collin, 1939
Brachyopa maculipennis	Thompson, 1980
Brachyopa pilosa	Collin, 1939
Brachyopa scutellaris	Robineau-Desvoidy, 1844
Brachypalpoides lentus	(Meigen, 1822)
Brachypalpus chrysites	Egger, 1859
Brachypalpus laphriformis	(Fallén, 1816)
Brachypalpus valgus	(Panzer, 1798)
Brachyplax tenuis	(Mulsant & Rey, 1852)
Brachyptera braueri	(Klapálek, 1900)
Brachyptera monilicornis	(Pictet, 1841)
Brachyptera risi	(Morton, 1896)
Brachyptera seticornis	(Klapálek, 1902)
Brachyptera trifasciata	(Pictet, 1832)
Brachypterolus antirrhini	(Murray 1864)
Brachypterolus linariae	(Stephens, 1830)
Brachypterolus pulicarius	(Kiesenwetter, 1850)
Brachypterus fulvipes	Erichson, 1843
Brachypterus glaber	Stephens, 1832
Brachypterus urticae	(Fabricius, 1792)
Brachyschendyla montana	(Attems, 1895)
Brachysomus dispar	Penecke, 1910
Brachysomus echinatus	(Bonsdorff, 1785)
Brachysomus fremuthi	Koštál, 1991
Brachysomus frivaldszkyi	(Reitter, 1884)
Brachysomus hirtus	(Boheman, 1845)
Brachysomus hispidus	(L. Redtenbacher, 1849)
Brachysomus mihoki	Penecke, 1914
Brachysomus setiger	(Gyllenhal, 1840)
Brachysomus slovacicus	Koštál, 1991
Brachysomus styriacus	Formánek, 1905
Brachysomus subnudus	(Seidlitz, 1868)
Brachysomus villosulus	(Germar, 1824)
Brachysomus zellichi	Formánek, 1907
Brachystegus scalaris	(Illiger, 1807)
Brachytemnus porcatus	(Germar, 1824)
Brachytron pratense	(Müller, 1764)
Brachyusa concolor	(Erichson, 1839)
Bradleycypris obliqua	(Brady, 1868)
Bradleystrandesia fuscata	(Jurine, 1820)
Bradleystrandesia hirsuta	(Fischer, 1851)
Bradleystrandesia reticulata	(Zaddach, 1844)
Bradybatus creutzeri	Germar, 1824
Bradybatus elongatulus	(Boheman, 1843)
Bradybatus fallax	Gerstäcker, 1860
Bradybatus kellneri	Bach, 1854
Bradybatus tomentosus	Desbrochers, 1892
Bradycellus caucasicus	(Chaudoir, 1846)
Bradycellus csikii	Laczó, 1912
Bradycellus harpalinus	(Audinet-Serville, 1821)
Bradycellus verbasci	(Duftschmid, 1812)
Bradyrrhoa trapezella	Duponchel, 1836
Branchinecta ferox	Milne-Edwards
Branchinecta orientalis	Sars, 1901
Branchipus schaefferi	Fischer, 1834
Branta bernicla	(Linnaeus, 1758)
Branta canadensis	(Linnaeus, 1758)
Branta leucopsis	(Bechstein, 1803)
Branta ruficollis	(Pallas, 1769)
Brenthis daphne	([Denis & Schiffermüller], 1775)
Brenthis hecate	([Denis & Schiffermüller], 1775)
Brenthis ino	(Rottemburg, 1775)
Brevennia pulveraria	(Newstead,1892)
Brevennia tetrapora	Goux,1940
Brintesia circe	(Fabricius, 1775)
Brommella falcigera	(Balogh, 1935)
Broscus cephalotes	(Linnaeus, 1758)
Anaproutia comitella	(Bruand, 1853)
Bruchela cana	(Küster, 1848)
Bruchela conformis	(Suffrian, 1845)
Bruchela muscula	(K. Daniel & J. Daniel, 1902)
Bruchela orientalis	Strejcek, 1982
Bruchela rufipes	(Olivier, 1790)
Bruchela schusteri	Schilsky, 1912
Bruchela suturalis	(Fabricius, 1792)
Brumus quadripustulatus	(Linnaeus, 1758)
Brundinia marina	(Mulsant et Rey, 1853)
Brundinia meridionalis	(Mulsant et Rey, 1853)
Bryaxis bulbifer	(Reichenbach, 1816)
Bryaxis carinula	Rey, 1888
Bryaxis curtisii	(Reitter, 1881)
Bryaxis curtisii orientalis	(Leach, 1817)
Bryaxis femoratus	(Aubé, 1844)
Bryaxis glabricollis	(Schmidt-Goebel, 1838)
Bryaxis nigripennis	(Aubé, 1844)
Bryaxis puncticollis	(Denny, 1825)
Brychius elevatus	(Panzer, 1794)
Bryocamptus minutus	(Claus, 1863)
Bryocamptus pygmaeus	(Sars, 1863)
Bryocamptus weberi	(Kessler, 1914)
Bryocamptus zschokkei	(Schmeil, 1893)
Bryocoris pteridis	(Fallén, 1807)
Bryophacis rufus	(Erichson, 1839)
Bryoporus cernuus	(Gravenhorst, 1806)
Bryoporus multipunctus	Hampe, 1867
Bryotropha affinis	(Haworth, 1828)
Bryotropha basaltinella	(Zeller, 1839)
Bryotropha desertella	(Douglas, 1850)
Bryotropha domestica	(Haworth, 1828)
Bryotropha galbanella	(Zeller, 1839)
Bryotropha plantariella	(Tengström, 1848)
Bryotropha senectella	(Zeller, 1839)
Bryotropha similis	(Stainton, 1857)
Bryotropha tachyptilella	(Rebel, 1916)
Bryotropha terrella	(Denis & Schiffermüller, 1775)
Bryotropha umbrosella	(Zeller, 1839)
Bubo bubo	(Linnaeus, 1758)
Bubulcus ibis	(Linnaeus, 1758)
Bucculatrix absinthii	Gartner, 1865
Bucculatrix albedinella	(Zeller, 1839)
Bucculatrix artemisiella	Herrich-Schäffer, 1855
Bucculatrix bechsteinella	(Bechstein & Scharfenberg, 1805)
Bucculatrix benacicolella	Hartig, 1937
Bucculatrix cantabricella	Chrétien, 1898
Bucculatrix cidarella	(Zeller, 1839)
Bucculatrix cristatella	(Zeller, 1839)
Bucculatrix demaryella	(Duponchel, 1840)
Bucculatrix frangutella	(Goeze, 1783)
Bucculatrix gnaphaliella	(Treitschke,1839)
Bucculatrix maritima	Stainton, 1851
Bucculatrix nigricomella	(Zeller, 1839)
Bucculatrix noltei	Petry, 1912
Bucculatrix ratisbonensis	Stainton, 1861
Bucculatrix rhamniella	Herrich-Schäffer, 1855
Bucculatrix thoracella	(Thunberg, 1794)
Bucculatrix ulmella	Zeller, 1848
Bucculatrix ulmifoliae	M. Hering, 1931
Bucephala clangula	(Linnaeus, 1758)
Bufo bufo	(Linnaeus, 1758)
Bufo viridis	Laurenti, 1768
Bulgarica cana	(Held, 1836)
Bulgarica rugicollis	(Rossmässler, 1836)
Bulgarica vetusta	(Rossmässler, 1836)
Bunops serricaudata	(Daday, 1888)
Bupalus piniaria	(Linnaeus, 1758)
Buprestis novemmaculata	Linnaeus, 1767
Buprestis octoguttata	Linnaeus, 1758
Buprestis rustica	Linnaeus, 1758
Burhinus oedicnemus	(Linnaeus, 1758)
Buteo buteo	(Linnaeus, 1758)
Buteo buteo vulpinus	(Linnaeus, 1758)
Buteo lagopus	(Pontoppidan, 1763)
Buteo rufinus	(Cretzschmar, 1827)
Denisia stroemella	(Fabricius, 1779)
Byctiscus betulae	(Linnaeus, 1758)
Byctiscus populi	(Linnaeus, 1758)
Byrrhus fasciatus	(Forster, 1771)
Byrrhus gigas	Fabricius, 1787
Byrrhus luniger	Germar, 1817
Byrrhus pilula	(Linnaeus, 1758)
Byrrhus pustulatus	(Forster, 1771)
Byrsinus fossor	(Mulsant & Rey, 1865)
Bythinella austriaca	(Frauenfeld, 1857)
Bythinus acutangulus	Reitter, 1878
Bythinus burrelli	Denny, 1825
Bythinus macropalpus	Aubé, 1833
Bythinus reichenbachi	(Machulka, 1928)
Bythinus securiger	(Reichenbach, 1817)
Bythiospeum hungaricum	(Soós, 1927)
Bythiospeum oshanovae	(L. Pintér, 1968)
Byturus ochraceus	(Scriba, 1790)
Byturus tomentosus	(De Geer, 1774)
Cabera exanthemata	(Scopoli, 1763)
Cabera pusaria	(Linnaeus, 1758)
Caccobius histeroides	(Ménétries, 1832)
Caccobius schreberi	(Linnaeus, 1767)
Cacotemnus rufipes	(Fabricius, 1792)
Cadra cautella	(Walker, 1863)
Cadra figulilella	(Gregson, 1871)
Cadra furcatella	(Herrich-Schäffer, 1849)
Caenis beskidensis	Sowa, 1973
Caenis horaria	(Linnaeus, 1758)
Caenis lactea	(Burmeister, 1839)
Caenis luctuosa	(Burmeister, 1839)
Caenis macrura	Stephens, 1835
Caenis martae	Belfiore, 1984
Caenis pseudorivulorum	Keffermüller, 1960
Caenis robusta	Eaton, 1884
Caenocara affine	(Sturm, 1837)
Caenocara bovistae	(Hoffmann, 1803)
Caenoscelis ferruginea	(C.R. Sahlberg, 1822)
Caenoscelis subdeplanata	Brisout de Barneville, 1882
Calambus bipustulatus	(Linnaeus, 1767)
Calamia tridens	(Hufnagel, 1766)
Calamobius filum	(Rossi, 1790)
Calamosternus granarius	(Linnaeus, 1767)
Calamotettix taeniatus	Horváth, 1911
Calamotropha aureliellus	(Fischer v. Röslerstamm, 1841)
Calamotropha paludella	(Hübner, 1824)
Calandrella brachydactyla	(Leisler, 1814)
Calandrella brachydactyla hungarica	(Leisler, 1814)
Calathus ambiguus	(Paykull, 1790)
Calathus cinctus	Motschulsky, 1850
Calathus erratus	(Sahlberg, 1827)
Calathus fuscipes	(Goeze, 1777)
Calathus melanocephalus	(Linnaeus, 1758)
Calcarius lapponicus	(Linnaeus, 1758)
Caliadurgus fasciatellus	(Spinola, 1808)
Calidris alba	(Pallas, 1764)
Calidris alpina	(Linnaeus, 1758)
Calidris alpina schinzii	(Linnaeus, 1758)
Calidris bairdii	(Coues, 1861)
Calidris canutus	(Linnaeus, 1758)
Calidris ferruginea	(Pontoppidan, 1763)
Calidris fuscicollis	(Vieillot, 1819)
Calidris maritima	(Brünnich, 1764)
Calidris melanotos	(Vieillot, 1819)
Calidris minuta	(Leisler, 1812)
Calidris pusilla	(Linnaeus, 1766)
Calidris temminckii	(Leisler, 1812)
Caliprobola speciosa	(Rossi, 1790)
Caliscelis wallengreni	(Stal, 1863)
Callicera aenea	(Fabricius, 1781)
Callicera rufa	Schummel, 1842
Callicera spinolae	Rondani, 1844
Callicerus obscurus	Gravenhorst, 1802
Callicerus rigidicornis	(Erichson, 1839)
Callicorixa praeusta	(Fieber, 1848)
Callidium aeneum	(De Geer, 1775)
Callidium coriaceum	Paykull, 1800
Callidium violaceum	(Linnaeus, 1758)
Calliergis ramosa	(Esper, 1786)
Calligypona reyi	(Fieber, 1866)
Callilepis nocturna	(Linnaeus, 1758)
Callilepis schuszteri	(Herman, 1879)
Callimus angulatus	(Schrank, 1789)
Callimorpha dominula	(Linnaeus, 1758)
Callimoxys gracilis	(Brullé, 1832)
Calliptamus barbarus	(Costa, 1836)
Calliptamus italicus	(Linne, 1758)
Callisto denticulella	(Thunberg, 1794)
Callistus lunatus	(Fabricius, 1775)
Calliteara abietis	([Denis & Schiffermüller], 1775)
Calliteara pudibunda	(Linnaeus, 1758)
Callobius claustrarius	(Hahn, 1833)
Callophrys rubi	(Linnaeus, 1758)
Callopistria juventina	(Stoll, 1782)
Calodera aethiops	(Gravenhorst, 1802)
Calodera ligula	Assing, 1996
Calodera nigrita	Mannerheim, 1830
Calodera riparia	Erichson, 1837
Calodera rufescens	Kraatz, 1856
Calodera uliginosa	Erichson, 1837
Calodromius spilotus	(Illiger, 1798)
Calophasia lunula	(Hufnagel, 1766)
Calophasia opalina	(Esper, 1793)
Calophasia platyptera	(Esper, 1788)
Calopteryx splendens	(Harris, 1782)
Calopteryx virgo	(Linné, 1758)
Caloptilia alchimiella	(Scopoli, 1763)
Caloptilia cuculipennella	(Hübner, 1796)
Caloptilia elongella	(Linnaeus, 1761)
Caloptilia falconipennella	(Hübner, 1813)
Caloptilia fidella	(Reutti, 1853)
Caloptilia fribergensis	(Fritzsche, 1871)
Calybites hauderi	(Rebel, 1906)
Caloptilia hemidactylella	(Denis & Schiffermüller, 1775)
Caloptilia honoratella	(Rebel, 1914)
Caloptilia robustella	Jäckh, 1972
Caloptilia roscipennella	(Hübner, 1796)
Caloptilia rufipennella	(Hübner, 1796)
Caloptilia semifascia	(Haworth, 1828)
Caloptilia stigmatella	(Fabricius, 1781)
Calopus serraticornis	(Linnaeus, 1758)
Calosirus terminatus	(Herbst, 1795)
Calosoma auropunctatum	(Herbst, 1784)
Calosoma inquisitor	(Linnaeus, 1758)
Callisthenes reticulatus	(Fabricius, 1787)
Calosoma sycophanta	(Linnaeus, 1758)
Abraxas sylvata	(Scopoli, 1763)
Calvia decemguttata	(Linnaeus, 1767)
Calvia quatuordecimguttata	Linnaeus, 1758
Calvia quindecimguttata	(Fabricius, 1777)
Calybites phasianipennella	(Hübner, 1813)
Calybites quadrisignella	(Zeller, 1839)
Calyciphora albodactylus	(Fabricius, 1794)
Calyciphora nephelodactyla	(Eversmannb, 1844)
Calyciphora xanthodactyla	(Treitschke, 1833)
Calymma communimacula	([Denis & Schiffermüller], 1775)
Calyptra thalictri	(Borkhausen, 1790)
Cameraria ohridella	Deschka & Dimic, 1986
Campaea honoraria	(Denis & Schiffermüller, 1775)
Campaea margaritaria	(Linnaeus, 1761)
Camponotus aethiops	(Latreille, 1798)
Camponotus atricolor	(Nylander, 1849)
Camponotus fallax	(Nylander, 1856)
Camponotus herculeanus	(Linnaeus, 1758)
Camponotus lateralis	Olivier, 1791
Camponotus ligniperda	Latreille, 1802
Camponotus piceus	(Leach, 1825)
Camponotus truncatus	(Spinola, 1808)
Camponotus vagus	(Scopoli, 1763)
Camptocercus lilljeborgi	Schoedler, 1862
Camptocercus rectirostris	Schoedler, 1862
Camptogramma bilineata	(Linnaeus, 1758)
Camptopoeum friesei	Mocsáry, 1894
Camptopoeum frontale	(Fabricius, 1804)
Camptopus lateralis	(Germar, 1817)
Camptorhinus simplex	Seidlitz, 1867
Camptorhinus statua	(Rossi, 1790)
Camptotelus lineolatus	(Schilling, 1829)
Camptozygum aequale	(Villers, 1789)
Campylomma annulicorne	(Signoret, 1865)
Campylomma verbasci	(Meyer-Dür, 1843)
Campyloneura virgula	(Herrich-Schäffer, 1835)
Campylosteira orientalis	Horváth, 1881
Campylosteira verna	(Fallén, 1826)
Canariphantes nanus	(Kulczynski, 1898)
Candidula intersecta	(Poiret, 1801)
Candidula unifasciata	(Poiret, 1801)
Candona balatonica	Daday, 1894
Candona candida	(O.F. Müller, 1776)
Candona improvisa	Ostermeyer, 1937
Candona muelleri	Hartwig, 1899
Candona neglecta	Sars, 1887
Candona sanociensis	Sywula, 1971
Candonopsis kingsleii	(Brady & Robertson, 1870)
Candonopsis scourfieldi	Brady, 1910
Canephora hirsuta	(Poda, 1761)
Canis aureus	(Linnaeus, 1758)
Canis aureus moreoticus	(Linnaeus, 1758)
Canis lupus	Linnaeus, 1758
Cantharis annularis	Ménetriés, 1836
Cantharis assimilis	Paykull, 1798
Cantharis bicolor	Herbst, 1784
Cantharis decipiens	Baudi di Selve, 1871
Cantharis figurata	Mannerheim, 1843
Cantharis fulvicollis	Fabricius, 1792
Cantharis fusca	Linnaeus, 1758
Cantharis lateralis	Linnaeus, 1758
Cantharis liburnica	Depoli, 1912
Cantharis livida	Linnaeus, 1758
Cordicantharis longicollis	(Kiesenwetter, 1859)
Cantharis nigricans	O.F. Müller, 1776
Cantharis obscura	Linnaeus, 1758
Cantharis pagana	Rosenhauer, 1847
Cantharis pallida	Goeze, 1777
Cantharis paludosa	Fallén, 1807
Cantharis paradoxa	Hicker, 1960
Cantharis pellucida	Fabricius, 1792
Cantharis pulicaria	Fabricius, 1781
Cantharis quadripunctata	(O.F. Müller, 1776)
Cantharis rufa	Linnaeus, 1758
Cantharis rustica	Fallén, 1807
Cantharis sudetica	Letzner, 1847
Canthocamptus microstaphylinus	Wolf, 1905
Canthocamptus northumbricus	Brady, 1880
Canthocamptus staphylinus	Jurine, 1820
Canthophorus dubius	(Scopoli, 1763)
Canthophorus melanopterus	(Herrich-Schäffer, 1835)
Canthophorus mixtus	Asanova, 1964
Capnia bifrons	(Newman, 1839)
Capnia nigra	(Pictet, 1933)
Capnodis tenebrionis	(Linnaeus, 1761)
Capperia britanniodactylus	(Gregson, 1867)
Capperia celeusi	(Frey, 1886)
Capperia trichodactyla	(Denis & Schiffermüller, 1775)
Capreolus capreolus	(Linnaeus, 1758)
Caprimulgus europaeus	Linnaeus, 1758
Caprimulgus europaeus meridionalis	Linnaeus, 1758
Capsodes gothicus	(Linnaeus, 1758)
Capsodes mat	(Rossi, 1790)
Capsus ater	(Linnaeus, 1758)
Capua vulgana	(Frölich, 1828)
Carabus arvensis	Herbst, 1784
Carabus arvensis austriae	Herbst, 1784
Carabus arvensis carpathus	Herbst, 1784
Carabus auronitens	Fabricius, 1792
Carabus auronitens kraussi	Fabricius, 1792
Carabus cancellatus	Illiger, 1798
Carabus cancellatus adeptus	Illiger, 1798
Carabus cancellatus budensis	Illiger, 1798
Carabus cancellatus durus	Illiger, 1798
Carabus cancellatus maximus	Illiger, 1798
Carabus cancellatus muehlfeldi	Illiger, 1798
Carabus cancellatus soproniensis	Illiger, 1798
Carabus cancellatus tibiscinus	Illiger, 1798
Carabus cancellatus ungensis	Illiger, 1798
Carabus clatratus	J. Müller, 1902 
Carabus clatratus auraniensis	J. Müller, 1902 
Carabus convexus	Fabricius, 1775
Carabus convexus kiskunensis	Fabricius, 1775
Carabus convexus simplicipennis	Fabricius, 1775
Carabus coriaceus	Linnaeus, 1758
Carabus coriaceus praeillyricus	Linnaeus, 1758
Carabus coriaceus pseudorugifer	Linnaeus, 1758
Carabus coriaceus rugifer	Linnaeus, 1758
Carabus germarii	Sturm, 1815
Carabus germarii exasperatus	Sturm, 1815
Carabus glabratus	Paykull, 1790
Carabus granulatus	Linnaeus, 1758
Carabus hampei	Küster, 1846
Carabus hampei ormayi	Küster, 1846
Carabus hortensis	Linnaeus, 1758
Carabus hungaricus	Fabricius, 1792
Carabus intricatus	Linnaeus, 1761
Carabus irregularis	Fabricius, 1792
Carabus irregularis cephalotes	Fabricius, 1792
Carabus linnaei	Panzer, 1812
Carabus linnaei transdanubialis	Kenyery, 1983
Carabus marginalis	Fabricius, 1794
Carabus montivagus	Palliardi, 1825
Carabus montivagus blandus	Palliardi, 1825
Carabus nemoralis	O. F. Müller, 1764
Carabus nodulosus	Creutzer, 1799
Carabus obsoletus	Sturm, 1815
Carabus problematicus	Herbst, 1786
Carabus problematicus holdhausi	Herbst, 1786
Carabus scabriusculus	Olivier, 1795
Carabus scabriusculus lippii	Olivier, 1795
Carabus scheidleri	Panzer, 1799
Carabus scheidleri baderlei	Panzer, 1799
Carabus scheidleri distinguendus	Panzer, 1799
Carabus scheidleri helleri	Panzer, 1799
Carabus scheidleri jucundus	Panzer, 1799
Carabus scheidleri pannonicus	Panzer, 1799
Carabus scheidleri praescheidleri	Panzer, 1799
Carabus scheidleri pseudopreyssleri	Panzer, 1799
Carabus scheidleri vertesensis	Panzer, 1799
Carabus ullrichii	Germar, 1824
Carabus ullrichii baranyensis	Germar, 1824
Carabus ullrichii planitiae	Germar, 1824
Carabus ullrichii sokolari	Germar, 1824
Carabus variolosus	Fabricius, 1787
Carabus violaceus	Linnaeus, 1758
Carabus violaceus betuliae	Linnaeus, 1758
Carabus violaceus pseudoviolaceus	Linnaeus, 1758
Carabus violaceus rakosiensis	Linnaeus, 1758
Carabus scheidleri zawadzkii	Panzer, 1799
Carabus scheidleri dissimilis	Panzer, 1799
Carabus scheidleri ronayi	Panzer, 1799
Caradrina morpheus	(Hufnagel, 1766)
Carassius carassius	(Linnaeus, 1758)
Carassius gibelio	(Bloch, 1782)
Carcharodus alceae	(Esper, 1780)
Carcharodus floccifera	(Zeller, 1847)
Carcharodus lavatherae	(Esper, 1783)
Carcharodus orientalis	Reverdin, 1913
Carcina quercana	(Fabricius, 1775)
Carcinops pumilio	(Erichson, 1834)
Cardiastethus fasciiventris	(Garbiglietti, 1869)
Cardiocondyla sahlbergi	Forel, 1913
Cardiophorus anticus	Erichson, 1840
Cardiophorus asellus	Erichson, 1840
Cardiophorus discicollis	(Herbst, 1806)
Cardiophorus dolini	Mardjanian, 1985
Cardiophorus ebeninus	(Germar, 1824)
Cardiophorus gramineus	(Scopoli, 1763)
Cardiophorus maritimus	Dolin, 1971
Cardiophorus nigerrimus	Erichson, 1840
Cardiophorus ruficollis	(Linnaeus, 1758)
Cardiophorus vestigialis	Erichson, 1840
Metacanthus annulosus	(Fieber, 1859)
Cardoria scutellata	(Fabricius, 1792)
Carduelis cannabina	(Linnaeus, 1758)
Carduelis carduelis	(Linnaeus, 1758)
Chloris chloris	(Linnaeus, 1758)
Carduelis flammea	(Linnaeus, 1758)
Carduelis flammea cabaret	(Linnaeus, 1758)
Carduelis flavirostris	(Linnaeus, 1758)
Carduelis hornemanni	(Holboell, 1843)
Carduelis hornemanni exilipes	(Holboell, 1843)
Carduelis spinus	(Linnaeus, 1758)
Carinatodorcadion aethiops	(Scopoli, 1763)
Carinatodorcadion fulvum cervae	(Scopoli, 1763)
Carinatodorcadion fulvum	(Scopoli, 1763)
Carpatolechia aenigma	(Haworth, 1828)
Carpatolechia alburnella	(Herrich-Schäffer, 1861)
Carpatolechia decorella	Sattler, 1983
Carpatolechia fugacella	(Zeller, 1839)
Carpatolechia fugitivella	(Haworth, 1812)
Carpatolechia notatella	(Zeller, 1839)
Carpatolechia proximella	(Zeller, 1839)
Carpelimus aceus	Gildenkov, 1997
Carpelimus anthracinus	(Mulsant et Rey, 1861)
Carpelimus bilineatus	(Stephens, 1834)
Carpelimus corticinus	(Gravenhorst, 1806)
Carpelimus elongatulus	(Erichson, 1839)
Carpelimus erichsoni	(Sharp, 1871)
Carpelimus exiguus	(Erichson, 1839)
Carpelimus foveolatus	(C.R.Sahlberg, 1832)
Carpelimus fuliginosus	(Gravenhorst, 1802)
Carpelimus ganglbaueri	Bernhauer, 1901
Carpelimus gracilis	(Mannerheim, 1830)
Carpelimus gusarovi	Gildenkov, 1997
Carpelimus heidenreichi	L.Benick, 1934
Carpelimus impressus	Lacordaire, 1835
Carpelimus lindrothi	Palm, 1942
Carpelimus manchuricus	(Bernhauer, 1938)
Carpelimus nitidus	(Baudi, 1848)
Carpelimus obesus	(Kiesenwetter, 1844)
Carpelimus opacus	(Baudi di Selve, 1848)
Carpelimus politus	(Kiesenwetter, 1850)
Carpelimus punctatellus	Erichson, 1840
Carpelimus pusillus	(Gravenhorst, 1802)
Carpelimus rivularis	(Motschulsky, 1860)
Carpelimus similis	(Smetana, 1967)
Carpelimus subtilis	Erichson, 1839
Carpelimus transversicollis	Scheerpeltz, 1947
Carphacis striatus	(Olivier, 1795)
Carpocoris fuscispinus	(Boheman, 1850)
Carpocoris mediterraneus	Tamanini, 1958
Carpocoris pudicus	(Poda, 1761)
Carpocoris purpureipennis	(De Geer, 1773)
Carpodacus erythrinus	(Pallas, 1770)
Carpodacus roseus	(Pallas, 1776)
Carpophilus bipustulatus	(Heer, 1841)
Carpophilus dimidiatus	(Fabricius, 1792)
Carpophilus hemipterus	(Linnaeus, 1758)
Carpophilus marginellus	Motschulsky, 1858
Carpophilus pilosellus	Motschulsky, 1858
Carpophilus quadrisignatus	Erichson, 1843
Carpophilus sexpustulatus	(Fabricius, 1791)
Carposina berberidella	Herrich-Schäffer, 1854
Carposina scirrhosella	Herrich-Schäffer, 1854
Carrhotus xanthogramma	(Latreille, 1819)
Carterocephalus palaemon	(Pallas, 1771)
Carterus angustipennis lutshniki	Chaudoir, 1852
Cartodere constricta	(Gyllenhal, 1827)
Cartodere nodifer	(Westwood, 1839)
Carulaspis carueli	(Signoret,1869)
Carulaspis juniperi	(Bouché,1851)
Carulaspis visci	(Schrank,1781)
Carychium minimum	O.F. Müller, 1774
Carychium tridentatum	(Risso, 1826)
Caryocolum alsinella	(Zeller, 1868)
Caryocolum amaurella	(M. Hering, 1924)
Caryocolum blandella	(Douglas, 1852)
Caryocolum blandulella	(Tutt, 1887)
Caryocolum cauligenella	(Schmid, 1863)
Caryocolum fischerella	(Treitschke, 1833)
Caryocolum huebneri	(Haworth, 1828)
Caryocolum inflativorella	(Klimesch, 1938)
Caryocolum junctella	(Douglas, 1851)
Caryocolum leucomelanella	(Zeller, 1839)
Caryocolum leucothoracellum	(Klimesch, 1953)
Caryocolum marmoreum	(Haworth, 1828)
Caryocolum petryi	(O. Hofmann, 1899)
Caryocolum proximum	(Haworth, 1828)
Caryocolum trauniella	(Zeller, 1868)
Caryocolum tricolorella	(Haworth, 1812)
Caryocolum vicinella	(Douglas, 1851)
Caryocolum viscariella	(Stainton, 1855)
Caryoscapha limbata	Erichson, 1845
Caspiobdella fadejewi	(Epstein, 1961)
Castor canadensis	Kuhl, 1820
Castor fiber	Linnaeus, 1758
Cataclysme riguata	(Hübner, 1813)
Cataclysta lemnata	(Linnaeus, 1758)
Cataglyphis aenescens	(Nylander, 1849)
Cataglyphis bicolor	Fabricius
Cataglyphis nodus	Brulle, 1846
Catapion jaffense	(Desbrochers, 1895)
Catapion meieri	(Desbrochers, 1901)
Catapion pubescens	(Kirby, 1811)
Catapion seniculus	(Kirby, 1808)
Catarhoe cuculata	(Hufnagel, 1767)
Catarhoe rubidata	(Denis & Schiffermüller, 1775)
Catastia marginea	(Denis & Schiffermüller, 1775)
Catephia alchymista	([Denis & Schiffermüller], 1775)
Catocala conversa	(Esper, 1783)
Catocala dilecta	(Hübner, 1808)
Catocala diversa	(Geyer, 1828)
Catocala electa	(Vieweg, 1790)
Catocala elocata	(Esper, 1787)
Catocala fraxini	(Linnaeus, 1758)
Catocala fulminea	(Scopoli, 1763)
Catocala hymenaea	([Denis & Schiffermüller], 1775)
Catocala nupta	(Linnaeus, 1767)
Catocala nymphagoga	(Esper, 1787)
Catocala promissa	([Denis & Schiffermüller], 1775)
Catocala puerpera	(Giorna, 1791)
Catocala sponsa	(Linnaeus, 1767)
Catoplatus carthusianus	(Goeze, 1778)
Catoplatus fabricii	(Stal, 1866)
Catoplatus horvathi	(Puton, 1879)
Catoplatus nigriceps	Horváth, 1905
Catops chrysomeloides	(Panzer, 1798)
Catops fuliginosus	Erichson, 1837
Catops fuscus	(Panzer, 1794)
Catops grandicollis	Erichson, 1837
Catops longulus	Kellner, 1846
Catops morio	(Fabricius, 1792)
Catops neglectus	Kraatz, 1852
Catops nigricans	(Spence, 1815)
Catops nigricantoides	Reitter, 1901
Catops nigriclavis	Gerhardt, 1900
Catops nigrita	Erichson, 1837
Catops picipes	(Fabricius, 1792)
Catops subfuscus	Kellner, 1846
Catops tristis	(Panzer, 1794)
Catops westi	Krogerus, 1931
Catopta thrips	(Hübner, 1818)
Catoptria confusellus	(Staudinger, 1882)
Catoptria falsella	(Denis & Schiffermüller, 1775)
Catoptria fulgidella	(Hübner, 1813)
Catoptria lythargyrella	(Hübner, 1796)
Catoptria margaritella	(Denis & Schiffermüller, 1775)
Catoptria myella	(Hübner, 1796)
Catoptria mytilella	(Hübner, 1805)
Catoptria osthelderi	(Lattin, 1950)
Catoptria permutatellus	(Herrich-Schäffer, 1848)
Catoptria pinella	(Linnaeus, 1758)
Catoptria verellus	(Zincken, 1817)
Cauchas fibulella	(Denis & Schiffermüller, 1775)
Cauchas leucocerella	(Scopoli, 1763)
Cauchas reskovitsiella	(Szenz-Ivány, 1945)
Cauchas rufifrontella	(Treitschke, 1833)
Cauchas rufimitrella	(Scopoli, 1763)
Cauchas uhrik-meszarosiella	(Szenz-Ivány, 1945)
Caulastrocecis furfurella	(Staudinger, 1871)
Cavernocypris subterranea	(Wolf, 1920)
Cecilioides acicula	(O.F. Müller, 1774)
Cecilioides petitiana	(Benoit, 1862)
Cedestis gysseleniella	Zeller, 1839
Cedestis subfasciella	(Stephens, 1834)
Celaena leucostigma	(Hübner, 1808)
Celastrina argiolus	(Linnaeus, 1758)
Celes variabilis	(Pallas, 1771)
Cellariopsis deubeli	(A.J. Wagner, 1914)
Celonites abbreviatus	(Villers, 1789)
Celypha capreolana	(Herrich-Schäffer, 1851)
Celypha cespitana	(Hübner, 1817)
Celypha flavipalpana	(Herrich-Schäffer, 1851)
Celypha rufana	(Scopoli, 1763)
Celypha rurestrana	(Duponchel, 1843)
Celypha striana	(Denis & Schiffermüller, 1775)
Celypha woodiana	(Barett, 1882)
Centricnemus leucogrammus	(Germar, 1824)
Centrocoris spiniger	(Fabricius, 1781)
Centrocoris variegatus	Kolenati, 1845
Centromerita bicolor	(Blackwall, 1833)
Centromerus brevivulvatus	Dahl, 1912
Centromerus cavernarum	(L. Koch, 1872)
Centromerus incilium	(L. Koch, 1881)
Centromerus sellarius	(Simon, 1884)
Centromerus serratus	(O.P.-Cambridge, 1875)
Centromerus silvicola	(Kulczynski, 1887)
Centromerus sylvaticus	(Blackwall, 1841)
Centroptilum luteolum	(Müller, 1776)
Pseudocentroptiloides nana	(Bogoescu, 1951)
Procloeon pennulatum	(Eaton, 1870)
Procloeon pulchrum	(Eaton, 1885)
Bythinus lucifuga	Heyden, 1849
Centrotus cornutus	(Linnaeus, 1758)
Cepaea hortensis	(O.F. Müller, 1774)
Cepaea nemoralis	(Linnaeus, 1758)
Cepaea vindobonensis	(C. Pfeiffer, 1828)
Orophia denisella	(Denis & Schiffermüller, 1775)
Orophia ferrugella	(Denis & Schiffermüller, 1775)
Orophia sordidella	(Hübner, 1796)
Cephennium affine	Besuchet, 1971
Cephennium carpathicum	(Saulcy, 1878)
Cephennium dariae	Lazorko, 1961
Cephennium delicatulum	Reitter, 1879
Cephennium majus	Reitter, 1881
Cephennium paganettii	Besuchet, 1971
Cephennium slovenicum	Machulka, 1931
Cephimallota angusticostella	(Zeller, 1839)
Cepphis advenaria	(Hübner, 1790)
Ceraclea alboguttata	(Hagen, 1860)
Ceraclea annulicornis	(Stephens, 1836)
Ceraclea aurea	(Pictet, 1834)
Ceraclea dissimilis	(Stephens, 1836)
Ceraclea fulva	(Rambur, 1842)
Ceraclea nigronervosa	(Retzius, 1783)
Ceraclea riparia	(Albarda, 1874)
Ceraclea senilis	(Burmeister, 1839)
Ceraleptus gracilicornis	(Herrich-Schäffer, 1835)
Ceraleptus lividus	Stein, 1858
Ceraleptus obtusus	(Brullé, 1839)
Cerallus rubidus	(Gyllenhal, 1817)
Cerambyx cerdo	Linnaeus, 1758
Cerambyx miles	Bonelli, 1823
Cerambyx scopolii	Füslin, 1775
Cerambyx welensii	(Küster, 1846)
Ceramica pisi	(Linnaeus, 1758)
Cerapheles terminatus	(Ménetriés, 1832)
Cerapteryx graminis	(Linnaeus, 1758)
Cerastis leucographa	([Denis & Schiffermüller], 1775)
Cerastis rubricosa	([Denis & Schiffermüller], 1775)
Ceratapion armatum	(Gerstäcker, 1854)
Ceratapion austriacum	(Wagner, 1904)
Ceratapion basicorne	(Illiger, 1807)
Ceratapion carduorum	(Kirby, 1808)
Ceratapion cylindricolle	(Gyllenhal, 1839)
Ceratapion gibbirostre	(Gyllenhal, 1813)
Ceratapion onopordi	(Kirby, 1808)
Ceratapion orientale	(Gerstäcker, 1854)
Ceratapion penetrans	(Germar, 1817)
Ceratapion transsylvanicum	(Schilsky, 1906
Ceratina acuta	Friese, 1896
Ceratina chalcites	Germar, 1839
Ceratina chalybea	Chevrier 1872
Ceratina cucurbitina	(Rossi, 1792)
Ceratina cyanea	(Kirby, 1802)
Ceratina gravidula	Gerstaecker, 1869
Ceratina nigrolabiata	Friese, 1896
Ceratinella brevipes	(Westring, 1851)
Ceratinella brevis	(Wider, 1834)
Ceratinella major	Kulczynski, 1894
Ceratinella scabrosa	(O.P.-Cambridge, 1871
Ceratocombus coleoptratus	(Zetterstedt, 1819)
Hippodamia notata	(Laicharting, 1781)
Hippodamia undecimnotata	(Schneider, 1792)
Cerceris albofasciata	(Rossi, 1790)
Cerceris arenaria	(Linnaeus, 1758)
Cerceris bicincta	Klug, 1835
Cerceris bracteata	Eversmann, 1849
Cerceris bupresticida	Dufour, 1841
Cerceris circularis	(Fabricius, 1804)
Cerceris circularis dacica	(Fabricius, 1804)
Cerceris eryngii	Marquet, 1875
Cerceris eversmanni	Schulz, 1912
Cerceris fimbriata	(Rossi, 1790)
Cerceris flavilabris	(Fabricius, 1793)
Cerceris hortivaga	Kohl, 1880
Cerceris interrupta	(Panzer, 1799)
Cerceris quadricincta	(Panzer, 1799)
Cerceris quadrifasciata	(Panzer, 1799)
Cerceris quinquefasciata	(Rossi, 1792)
Cerceris rubida	(Jurine, 1807)
Cerceris ruficornis	(Fabricius, 1793)
Cerceris rybyensis	(Linnaeus, 1771)
Cerceris sabulosa	(Panzer, 1799)
Cerceris somotorensis	Balthasar, 1956
Cerceris stratiotes	Schletterer, 1887
Cerceris tenuivittata	Dufour, 1849
Cerceris tuberculata	(Villers, 1789)
Cercidia prominens	(Westring, 1851)
Cercopis arcuata	Fieber, 1844
Cercopis sanguinolenta	(Scopoli, 1763)
Cercopis vulnerata	Rossi, 1807
Cercyon analis	(Paykull, 1798)
Cercyon bifenestratus	Küster, 1851
Cercyon convexiusculus	Stephens, 1829
Cercyon granarius	Erichson, 1837
Cercyon haemorrhoidalis	(Fabricius, 1775)
Cercyon hungaricus	Endrődy-Younga, 1968
Cercyon impressus	(Sturm, 1807)
Cercyon laminatus	Sharp, 1873
Cercyon lateralis	(Marsham, 1802)
Cercyon marinus	Thomson, 1853
Cercyon melanocephalus	(Linnaeus, 1758)
Cercyon nigriceps	(Marsham, 1802)
Cercyon obsoletus	(Gyllenhal, 1808)
Cercyon pygmaeus	(Illiger, 1801)
Cercyon quisquilius	(Linnaeus, 1761)
Cercyon sternalis	Sharp, 1918
Cercyon terminatus	(Marsham, 1802)
Cercyon tristis	(Illiger, 1801)
Cercyon unipunctatus	(Linnaeus, 1758)
Cercyon ustulatus	(Preyssler, 1790)
Ceriagrion tenellum	(Villers, 1789)
Ceriana conopsoides	(Linnaeus, 1758)
Ceriana vespiformis	(Latreille, 1804)
Ceriodaphnia dubia	Richard, 1894
Ceriodaphnia laticaudata	P.E. Müller, 1867
Ceriodaphnia megops	Sars, 1862
Ceriodaphnia pulchella	Sars, 1862
Ceriodaphnia quadrangula	(O.F. Müller, 1785)
Ceriodaphnia reticulata	(Jurine, 1820)
Ceriodaphnia rotunda	Sars, 1862
Ceriodaphnia setosa	Matile, 1890
Ceritaxa alluvialis	Ádám, 1994
Ceritaxa flavipes	(Hochhuth, 1860)
Ceritaxa hamoriae	Ádám, 1987
Ceritaxa testaceipes	(Heer, 1841)
Cernuella neglecta	(Draparnaud, 1805)
Cerococcus cycliger	Goux,1932
Cerocoma adamovichiana	(Piller et Mitterpacher, 1789)
Cerocoma muehlfeldi	Gyllenhal, 1817
Cerocoma schaefferi	(Linnaeus, 1758)
Cerocoma schreberi	Fabricius, 1781
Ceropales albicincta	(Rossi, 1790)
Ceropales cribrata	Costa, 1881
Ceropales helvetica	Tournier, 1889
Ceropales maculata	(Fabricius, 1775)
Ceropales pygmaea	Kohl, 1879
Ceropales variegata	(Fabricius, 1798)
Cerophytum elateroides	(Latreille, 1804)
Certhia brachydactyla	Ch. L. Brehm, 1820
Certhia familiaris	Linnaeus, 1758
Certhia familiaris macrodactyla	Linnaeus, 1758
Cerura erminea	(Esper, 1783)
Cerura vinula	(Linnaeus, 1758)
Cervus elaphus	Linnaeus, 1758
Cerylon deplanatum	(Gyllenhal, 1827)
Cerylon fagi	Brisout de Barneville, 1867
Cerylon ferrugineum	Stephens, 1830
Cerylon histeroides	(Fabricius, 1792)
Cetonana laticeps	(Canestrini, 1868)
Cetonia aurata	(Linnaeus, 1758)
Protaetia aeruginosa	(Linnaeus, 1767)
Cettia cetti	(Temminck, 1820)
Ceuthonectes hungaricus	Ponyi, 1958
Ceutorhynchus aeneicollis	Germar, 1824
Ceutorhynchus alliariae	H. Brisout, 1860
Ceutorhynchus arator	Gyllenhal, 1837
Ceutorhynchus assimilis	(Paykull, 1792)
Ceutorhynchus atomus	Boheman, 1845
Ceutorhynchus barbareae	Suffrian, 1847
Ceutorhynchus canaliculatus	Ch. Brisout, 1869
Ceutorhynchus carinatus	Gyllenhal, 1837
Ceutorhynchus chalibaeus	Germar, 1824
Ceutorhynchus chlorophanus	Rouget, 1857
Ceutorhynchus coarctatus	Gyllenhal, 1837
Ceutorhynchus cochleariae	(Gyllenhal, 1813)
Ceutorhynchus constrictus	(Marsham, 1802)
Ceutorhynchus dubius	Ch. Brisout, 1883
Ceutorhynchus erysimi	(Fabricius, 1787)
Ceutorhynchus fallax	Boheman, 1845
Ceutorhynchus fulvitarsis	Gougelet & H. Brisout, 1860
Ceutorhynchus gallorhenanus	F. Solari, 1949
Ceutorhynchus gerhardti	Schultze, 1900
Ceutorhynchus griseus	Ch. Brisout, 1869
Ceutorhynchus hampei	(Ch. Brisout, 1869)
Ceutorhynchus hirtulus	Germar, 1824
Ceutorhynchus ignitus	Germar, 1824
Ceutorhynchus inaffectatus	Gyllenhal, 1837
Ceutorhynchus interjectus	Schultze, 1903
Ceutorhynchus laetus	Rosenhauer, 1856
Ceutorhynchus levantinus	Schultze, 1898
Ceutorhynchus liliputanus	Schultze, 1898
Ceutorhynchus merkli	Korotyaev, 2001
Ceutorhynchus moraviensis	Dieckmann, 1966
Ceutorhynchus nanus	Gyllenhal, 1837
Ceutorhynchus napi	Gyllenhal, 1837
Ceutorhynchus nigritulus	Schultze, 1896
Ceutorhynchus niyazii	Hoffmann, 1957
Ceutorhynchus obstrictus	(Marsham, 1802)
Ceutorhynchus pallidactylus	(Marsham, 1802)
Ceutorhynchus pallipes	Crotch, 1966
Ceutorhynchus pandellei	Ch. Brisout, 1869
Ceutorhynchus parvulus	Ch. Brisout, 1869
Ceutorhynchus pectoralis	J. Weise, 1895
Ceutorhynchus pervicax	J. Weise, 1883
Ceutorhynchus picitarsis	Gyllenhal, 1837
Ceutorhynchus posthumus	Germar, 1824
Ceutorhynchus pulvinatus	(Gyllenhal, 1837)
Ceutorhynchus puncticollis	Boheman, 1845
Ceutorhynchus pyrrhorhynchus	(Marsham, 1802)
Ceutorhynchus querceti	(Gyllenhal, 1813)
Ceutorhynchus rapae	Gyllenhal, 1837
Ceutorhynchus rhenanus	(Schultze, 1895)
Ceutorhynchus roberti	Gyllenhal, 1837
Ceutorhynchus scapularis	Gyllenhal, 1837
Ceutorhynchus scrobicollis	Neresheimer & Wagner, 1924
Ceutorhynchus similis	Ch. Brisout, 1869
Ceutorhynchus sisymbrii	(Dieckmann, 1966)
Ceutorhynchus sophiae	Gyllenhal, 1837
Ceutorhynchus strejceki	Dieckmann, 1981
Ceutorhynchus striatellus	Schultze, 1900
Ceutorhynchus sulcatus	Ch. Brisout, 1869
Ceutorhynchus sulcicollis	(Paykull, 1800)
Ceutorhynchus syrites	Germar, 1824
Ceutorhynchus thlaspi	Ch. Brisout, 1869
Ceutorhynchus turbatus	Schultze, 1903
Ceutorhynchus typhae	(Herbst, 1795)
Ceutorhynchus unguicularis	Thomson, 1871
Ceutorhynchus viridanus	Gyllenhal, 1837
Ceutorhynchus wagneri	Smreczyński, 1937
Chaetabraeus globulus	(Creutzer, 1799)
Chaetarthria seminulum	(Herbst, 1797)
Chaetida longicornis	(Gravenhorst, 1802)
Chaetococcus phragmitis	(Marchal,1909)
Chaetococcus sulcii	(Green, 1934)
Chaetophora spinosa	(Rossi, 1794)
Chaetopteroplia segetum	(Herbst, 1783)
Chaetopteryx fusca	Brauer, 1857
Chaetopteryx major	McLachlan, 1876
Chaetopteryx rugulosa	Kolenati, 1848
Chaetopteryx schmidi	Botosaneanu, 1957
Chaetopteryx schmidi mecsekensis	Botosaneanu, 1957
Alburnus chalcoides	(Agassiz, 1832)
Alburnus chalcoides mento	(Agassiz, 1832)
Chalcionellus amoenus	(Erichson, 1834)
Chalcionellus decemstriatus	(Rossi, 1792)
Lestes viridis	(Vander Linden, 1825)
Chalcophora mariana	(Linnaeus, 1758)
Chalcosyrphus curvipes	(Loew, 1854)
Chalcosyrphus eunotus	(Loew, 1873)
Chalcosyrphus femoratus	(Linnaeus, 1758)
Chalcosyrphus nemorum	(Fabricius, 1805)
Chalcosyrphus piger	(Fabricius, 1794)
Chalicodoma ericetorum	(Lepeletier, 1841)
Chalicodoma hungaricum	Mocsáry, 1877
Chalicodoma lefebvrei	(Lepeletier, 1841)
Chalicodoma parietinum	(Geoffroy, 1785)
Chalybion femoratum	(Fabricius, 1781)
Chamaesphecia aerifrons	(Zeller, 1847)
Chamaesphecia alysoniformis	(Herrich-Schäffer, 1846)
Chamaesphecia anatolica	Schwingenschuss, 1938
Chamaesphecia annellata	(Zeller, 1847)
Chamaesphecia astatiformis	(Herrich-Schäffer, 1846)
Chamaesphecia bibioniformis	(Esper, 1800)
Chamaesphecia chalciformis	(Esper, 1804)
Chamaesphecia doleriformis colpiformis	Herrich-Schäffer, 1845
Chamaesphecia crassicornis	Bartel, 1912
Chamaesphecia dumonti	Le Cerf, 1922
Chamaesphecia euceraeformis	(Ochsenheimer, 1816)
Chamaesphecia hungarica	(Tomala, 1901)
Chamaesphecia leucopsiformis	(Esper, 1800)
Chamaesphecia masariformis	(Ochsenheimer, 1808)
Chamaesphecia nigrifrons	(Le Cerf, 1911)
Chamaesphecia palustris	Kautz, 1927
Chamaesphecia tenthrediniformis	(Denis & Schiffermüller, 1775)
Chamaesyrphus scaevoides	(Fallén, 1817)
Charadrius alexandrinus	Linnaeus, 1758
Charadrius dubius	Scopoli, 1786
Charadrius hiaticula	Linnaeus, 1758
Charadrius hiaticula tundrae	Linnaeus, 1758
Charadrius leschenaultii	Lesson, 1826
Charadrius leschenaultii columbinus	Lesson, 1826
Charadrius morinellus	Linnaeus, 1758
Charadrius vociferus	Linnaeus, 1758
Charagochilus gyllenhalii	(Fallén, 1807)
Charagochilus weberi	Wagner, 1953
Charanyca trigrammica	(Hufnagel, 1766)
Chariaspilates formosaria	(Eversmann, 1837)
Kemtrognophos ambiguata	(Duponchel, 1830)
Neognophina intermedia	Kovács, 1954
Neognophina intermedia budensis	Kovács, 1954
Charissa obscurata	(Denis & Schiffermüller, 1775)
Chionodes luctuella	(Hübner, 1793)
Costignophos pullata	(Denis & Schiffermüller, 1775)
Euchrognophos variegata	(Duponchel, 1830)
Charopus concolor	(Fabricius, 1801)
Charopus flavipes	(Paykull, 1798)
Charopus graminicola	(Dejean, 1833)
Charopus philoctetes	Abeille de Perrin, 1885
Charopus thoracicus	Morawitz, 1851
Chartoscirta cincta	(Herrich-Schäffer, 1842)
Chartoscirta cocksii	(Curtis, 1835)
Chartoscirta elegantula	(Fallén, 1809)
Chazara briseis	(Linnaeus, 1764)
Cheilosia aerea	Dufour, 1848
Cheilosia albipila	Meigen, 1838
Cheilosia albitarsis	(Meigen, 1822)
Cheilosia antiqua	(Meigen, 1822)
Cheilosia barbata	Loew, 1857
Cheilosia bergenstammi	Becker, 1894
Cheilosia brachysoma	Egger, 1860
Cheilosia caerulescens	(Meigen, 1822)
Cheilosia canicularis	(Panzer, 1801)
Cheilosia carbonaria	Egger, 1860
Cheilosia chloris	(Meigen, 1822)
Cheilosia chrysocoma	(Meigen, 1822)
Cheilosia cynocephala	Loew, 1840
Cheilosia fasciata	Schiner & Egger, 1853
Cheilosia flavipes	(Panzer, 1798)
Cheilosia fraterna	(Meigen, 1830)
Cheilosia frontalis	Loew, 1857
Cheilosia gigantea	(Zetterstedt, 1838)
Cheilosia griseifacies	Vujić & Claussen, 1995
Cheilosia grossa	(Fallén, 1817)
Cheilosia hypena	(Becker, 1894)
Cheilosia illustrata	(Harris, 1780)
Cheilosia impressa	Loew, 1840
Cheilosia insignis	Loew, 1857
Cheilosia intonsa	Loew, 1857
Cheilosia lasiopa	Kowarz, 1885
Cheilosia laticornis	Rondani, 1857
Cheilosia lenis	(Becker, 1894)
Cheilosia lenta	(Becker, 1894)
Cheilosia melanopa	(Zetterstedt, 1843)
Cheilosia melanura	(Becker, 1894)
Cheilosia mutabilis	(Fallén, 1817)
Cheilosia nebulosa	(Verrall, 1871)
Cheilosia nigripes	(Meigen, 1822)
Cheilosia orthotricha	Vujić & Claussen, 1994
Cheilosia pagana	(Meigen, 1822)
Cheilosia praecox	(Zetterstedt, 1843)
Cheilosia proxima	(Zetterstedt, 1843)
Cheilosia pubera	(Zetterstedt, 1838)
Cheilosia ranunculi	Doczkal, 2000
Cheilosia schnabli	(Becker, 1894)
Cheilosia scutellata	(Fallén, 1817)
Cheilosia semifasciata	(Becker, 1894)
Cheilosia soror	(Zetterstedt, 1843)
Cheilosia subpictipennis	Claussen, 1998
Cheilosia variabilis	(Panzer, 1798)
Cheilosia velutina	Loew, 1840
Cheilosia vernalis	(Fallén, 1817)
Cheilosia vicina	(Zetterstedt, 1849)
Cheilosia vulpina	(Meigen, 1822)
Cheiracanthium angulitarse	Simon, 1878
Cheiracanthium effossum	Herman, 1879
Cheiracanthium elegans	Thorell, 1875
Cheiracanthium erraticum	(Walckenaer, 1802)
Cheiracanthium mildei	L. Koch, 1864
Cheiracanthium montanum	L. Koch, 1878
Cheiracanthium oncognathum	Thorell, 1871
Cheiracanthium pennyi	O.P.-Cambridge, 1873
Cheiracanthium punctorium	(Villers, 1789)
Cheiracanthium rupestre	Herman, 1879
Cheiracanthium virescens	(Sundevall, 1833)
Cheironitis ungaricus	(Herbst, 1789)
Chelis maculosa	(Gerning, 1780)
Chelostoma campanularum	(Kirby, 1802)
Chelostoma distinctum	Stöckhert, 1929
Chelostoma emarginatum	(Nylander, 1856)
Chelostoma florisomne	(Linnaeus, 1758)
Chelostoma foveolatum	(Morawitz, 1868)
Chelostoma handlirschi	Schletterer, 1889
Chelostoma rapunculi	(Lepeletier, 1841)
Chelostoma styriacum	Schwarz & Gusenleitner, 1999
Chelostoma ventrale	Schletterer, 1889
Bythinus bituberculatum	Latreille, 1807
Chersotis cuprea	([Denis & Schiffermüller], 1775)
Chersotis fimbriola	(Esper, 1803)
Chersotis fimbriola baloghi	(Esper, 1803)
Chersotis margaritacea	(Villers, 1789)
Chersotis multangula	(Hübner, 1803)
Chersotis rectangula	([Denis & Schiffermüller], 1775)
Chesias legatella	(Denis & Schiffermüller, 1775)
Chesias rufata	(Fabricius, 1775)
Chettusia gregaria	(Pallas, 1771)
Chettusia leucura	(Lichtenstein, 1823)
Cheumatopsyche lepida	(Pictet, 1834)
Chevrolatia egregia	Reitter, 1881
Chiasmia clathrata	(Linnaeus, 1758)
Chiasmus conspurcatus	(Perris, 1857)
Chilacis typhae	(Perris, 1857)
Chilo phragmitella	(Hübner, 1805)
Chilocorus bipustulatus	(Linnaeus, 1758)
Chilocorus renipustulatus	(Scriba, 1790)
Chilodes maritima	(Tauscher, 1806)
Chilomorpha longitarsis	(Thomson, 1867)
Chilopselaphus balneariellus podolicus	Chrétien, 1907
Chilopselaphus fallax	Mann, 1867
Chilopselaphus lagopellus	(Herrich.Schaffer, 1860)
Chilothorax distinctus	(O.F.Müller, 1776)
Chilothorax melanostictus	(W. Schmidt, 1840)
Chilothorax paykulli	Bedel, 1908
Chilothorax pictus	(Sturm, 1805)
Chionaspis austriaca	Lindinger, 1912
Chionaspis lepineyi	Balachowsky,1928
Chionaspis salicis	(Linnaeus,1758)
Chionodes distinctella	(Zeller, 1839)
Chionodes electella	(Zeller, 1839)
Chionodes fumatella	(Douglas, 1850)
Chionodes ignorantella	(Herrich-Schäffer, 1854)
Chionodes lugubrella	(Fabricius, 1794)
Chionodes tragicella	(Heyden, 1865)
Chionodes viduella	(Fabricius, 1794)
Chirocephalus carnuntanus	(Brauer, 1877)
Chirocephalus diaphanus	Prevost, 1803
Chlaenius festivus	(Panzer, 1796)
Chlaeniellus nigricornis	(Fabricius, 1787)
Chlaeniellus nitidulus	(Schrank, 1781)
Chlaenius spoliatus	(Rossi, 1790)
Agostenus sulcicollis	(Paykull, 1798)
Chlaeniellus terminatus	Dejean, 1826
Chlaeniellus tibialis	Dejean, 1826
Chlaeniellus tristis	(Schaller, 1783)
Chlaeniellus vestitus	(Paykull, 1790)
Chlamydatus evanescens	(Boheman, 1852)
Chlamydatus pulicarius	(Fallén, 1807)
Chlamydatus pullus	Reuter, 1870
Chlamydatus saltitans	(Fallén, 1807)
Chlidonias hybridus	(Pallas, 1811)
Chlidonias leucopterus	(Temminck, 1815)
Chlidonias niger	(Linnaeus, 1758)
Chloantha hyperici	([Denis & Schiffermüller], 1775)
Chloothea zonata	Emeljanov, 1959
Chlorion magnificum	Morawitz, 1887
Chloriona clavata	Dlabola, 1960
Chloriona glaucescens	Fieber, 1866
Chloriona smaragdula	(Stal, 1853)
Chloriona unicolor	(Herrich-Schäffer, 1835)
Chloriona vasconica	Ribaut, 1934
Chlorionidea flava	Löw, 1885
Chlorissa cloraria	(Hübner, 1813)
Chlorissa viridata	(Linnaeus, 1758)
Chlorita paolii	(Ossiannilsson, 1939)
Chlorita tessellata	Lethierry, 1884
Chlorita viridula	(Fallén, 1806)
Dysstroma citrata	(Linnaeus, 1761)
Chloroclysta miata	(Linnaeus, 1758)
Chloroclysta siterata	(Hufnagel, 1767)
Dysstroma truncata	(Hufnagel, 1767)
Chloroclystis v-ata	(Haworth, 1809)
Chlorochroa juniperina	(Linnaeus, 1758)
Chlorochroa pinicola	(Mulsant & Rey, 1852)
Chloroperla tripunctata	(Scopoli,1763)
Chlorophanus excisus	(Fabricius, 1801)
Chlorophanus graminicola	Schönherr, 1832
Chlorophanus pollinosus	(Fabricius, 1792)
Chlorophanus viridis	(Linnaeus, 1758)
Chlorophanus viridis balcanicus	(Linnaeus, 1758)
Chlorophorus figuratus	(Scopoli, 1763)
Chlorophorus herbstii	(Brahm, 1790)
Chlorophorus hungaricus	Seidl, 1891
Chlorophorus sartor	(O. F. Müller, 1766)
Chlorophorus trifasciatus	(Fabricius, 1781)
Chlorophorus varius	(O. F. Müller, 1766)
Pulvinaria floccifera	(Westwood, 1870)
Chnaurococcus danzigae	Kozár et Kosztarab,1976
Chnaurococcus parvus	(Borchsenius,1949)
Chnaurococcus subterraneus	(Newstead,1893)
Choleva angustata	(Fabricius, 1781)
Choleva cisteloides	(Frölich, 1799)
Choleva elongata	(Paykull, 1798)
Choleva glauca	Britten, 1918
Choleva oblonga	Latreille, 1807
Choleva oresitropha	Ganglbauer, 1896
Choleva paskoviensis	Reitter, 1913
Choleva reitteri	Petri, 1915
Choleva spadicea	(Sturm, 1839)
Choleva sturmi	Brisout De Barneville, 1863
Chondrina arcadica	(Reinhardt, 1881)
Chondrina arcadica clienta	(Reinhardt, 1881)
Chondrosoma fiduciaria	Anker, 1854
Chondrostoma nasus	(Linnaeus, 1758)
Chondrula tridens	(O.F. Müller, 1774)
Choneiulus palmatus	(Nemec, 1895)
Chonostropheus tristis	(Fabricius, 1794)
Choragus horni	Wolfrum, 1930
Choragus sheppardi	Kirby, 1819
Chordeuma sylvestre	C.L.Koch, 1847
Choreutis nemorana	Hübner, 1799
Choreutis pariana	(Clerck, 1759)
Choristoneura diversana	(Hübner, 1817)
Choristoneura hebenstreitella	(Müller, 1764)
Choristoneura murinana	(Hübner, 1799)
Chorizococcus rostrellum	(Lobdell,1930)
Chorizococcus viktorina	Kozár,1983
Chorosoma gracile	Josifov, 1968
Chorosoma schillingii	(Schilling, 1829)
Choroterpes picteti	(Eaton, 1871)
Chorthippus albomarginatus	(DeGeer, 1773)
Chorthippus dichrous	Eversmann, 1895
Chorthippus dorsatus	(Zetterstedt, 1821)
Chorthippus loratus	Zetterstedt, 1821
Chorthippus alliaceus	(Fischer de Waldheim, 1846)
Chorthippus montanus	(Charpentier, 1825)
Chortinaspis subterraneus	(Lindinger,1912)
Chortodes extrema	(Hübner, 1809)
Chortodes fluxa	(Hübner, 1809)
Chortodes morrisii	(Dale, 1837)
Chortodes pygmina	(Haworth, 1809)
Chrysanthia nigricornis	Westerhauser, 1881
Chrysanthia viridissima	(Linnaeus, 1758)
Chrysidea pumila	(Klug, 1837)
Chrysis albanica	Trautmann, 1927
Chrysis angustifrons	Abeille, 1878
Chrysis angustula	Schenck, 1856
Chrysis brevitarsis	Thomson, 1870
Chrysis cerastes	Abeille, 1878
Chrysis chrysoprasina	Forster, 1853
Chrysis cingulicornis	Forster, 1853
Chrysis coeruleiventris	(Abeille, 1878)
Chrysis comparata	Lepeletier, 1806
Chrysis compta	Forster, 1853
Chrysis distincta	Mocsáry, 1887
Chrysis fasciata	Olivier, 1790
Chrysis fulgida	Linnaeus, 1761
Chrysis germari	Wesmael, 1839
Chrysis gracillima	Forster, 1853
Chrysis graelsii	Guerin, 1842
Chrysis grohmanni	Dahlbom, 1854
Chrysis ignita	(Linnaeus, 1761)
Chrysis inaequalis	Dahlbom, 1845
Chrysis indigotea	Dufour & Perris, 1840
Chrysis insperata	Chevrier, 1870
Chrysis interjecta	Buysson, 1891
Chrysis leachii	Shuckard, 1837
Chrysis longula	Abeille, 1879
Chrysis marginata	Mocsáry, 1889
Chrysis millenaris	Mocsáry, 1897
Chrysis phryne	Abeille, 1868
Chrysis pseudobrevitarsis	Linsenmaier, 1951
Chrysis pulchella	Spinola, 1808
Chrysis purpurata	Fabricius, 1787
Chrysis ragusae	Destefani, 1888
Chrysis ramburi	Dahlbom, 1854
Chrysis ruddii	Shuckard, 1837
Chrysis rufitarsis	Brullé, 1832
Chrysis rutilans	Olivier, 1790
Chrysis rutiliventris	Abeille, 1879
Chrysis scutellaris	Fabricius, 1797
Chrysis semicincta	Lepeletier, 1806
Chrysis splendidula	Rossi, 1790
Chrysis subsinuata	Marquet, 1879
Chrysis succincta	Linnaeus, 1767
Chrysis varidens	Abeille, 1878
Chrysis variegata	Olivier, 1790
Chrysis viridula	Linnaeus, 1761
Chrysis viridula cylindrica	Linnaeus, 1761
Chrysobothris affinis	(Fabricius, 1794)
Chrysobothris chrysostigma	(Linnaeus, 1758)
Chrysobothris igniventris	Reitter, 1895
Euthystira brachyptera	(Ocskay, 1826)
Chrysochraon dispar	(Germar, 1834)
Chrysoclista lathamella	Fletcher, 1936
Chrysoclista linneella	(Clerck, 1759)
Chrysocrambus craterella	(Scopoli, 1763)
Chrysocrambus linetella	(Fabricius, 1781)
Chrysodeixis chalcites	(Esper, 1789)
Chrysoesthia drurella	(Fabricius, 1775)
Chrysoesthia eppelsheimi	(Staudinger, 1885)
Chrysoesthia sexguttella	(Thunberg, 1794)
Chrysogaster cemiteriorum	(Linnaeus, 1758)
Chrysogaster solstitialis	(Fallén, 1817)
Chrysopa abbreviata	Curtis, 1834
Chrysopa commata	Kis & Újhelyi, 1965
Chrysopa dorsalis	Burmeister, 1839
Chrysopa formosa	Brauer, 1850
Chrysopa hungarica	Klapálek, 1899
Chrysopa nigricostata	Brauer, 1850
Chrysopa pallens	(Rambur, 1838)
Chrysopa perla	(Linnaeus, 1758)
Chrysopa phyllochroma	Wesmael, 1841
Chrysopa viridana	Schneider, 1845
Chrysopa walkeri	McLachlan, 1893
Chrysoperla carnea	(Stephens, 1836)
Chrysoperla lucasina	(Lacroix, 1912)
Chrysoperla mediterranea	(Hölzel, 1972)
Chrysoperla renoni	(Lacroix, 1933)
Chrysops caecutiens	(Linnaeus, 1758)
Chrysops flavipes	Meigen, 1804
Chrysops italicus	Meigen, 1804
Chrysops melicharii	Mik, 1898
Chrysops parallelogrammus	Zeller, 1842
Chrysops relictus	Meigen, 1820
Chrysops rufipes	Meigen, 1820
Chrysops viduatus	(Fabricius, 1794)
Chrysoteuchia culmella	(Linnaeus, 1758)
Chrysotoxum arcuatum	(Linnaeus, 1758)
Chrysotoxum bicinctum	(Linnaeus, 1758)
Chrysotoxum cautum	(Harris, 1776)
Chrysotoxum elegans	Loew, 1841
Chrysotoxum fasciatum	(Müller, 1764)
Chrysotoxum fasciolatum	(De Geer, 1776)
Chrysotoxum intermedium	Meigen, 1822
Chrysotoxum lineare	(Zetterstedt, 1819)
Chrysotoxum octomaculatum	Curtis, 1837
Chrysotoxum vernale	Loew, 1841
Chrysotoxum verralli	Collin, 1940
Chrysotropia ciliata	(Wesmael, 1841)
Chrysso nordica	(Chamberlin & Ivie, 1947)
Chrysura austriaca	(Fabricius, 1804)
Chrysura cuprea	(Rossi, 1790)
Chrysura dichroa	(Dahlbom, 1854)
Chrysura dichroa socia	(Dahlbom, 1854)
Chrysura hirsuta	(Gerstaecker, 1869)
Chrysura hybrida	(Lepeletier, 1806)
Chrysura ignifrons	(Brullé, 1832)
Chrysura laodamia	(Buysson, 1900)
Chrysura radians	(Harris, 1776)
Chrysura rufiventris	(Dahlbom, 1854)
Chrysura simplex	(Dahlbom, 1854)
Chrysura trimaculata	(Förster, 1853)
Chydorus gibbus	Sars, 1890
Chydorus latus	Sars, 1862
Chydorus ovalis	Kurz, 1874
Chydorus piger	Sars, 1862
Chydorus sphaericus	(O.F. Müller, 1785)
Cibiniulus phlepsii	(Verhoeff, 1897)
Cicada orni	Linnaeus, 1758
Cicadella viridis	(Linnaeus, 1758)
Cicadetta montana	(Scopoli, 1772)
Cicadetta transsylvanica	Fieber, 1876
Cicadivetta tibialis	(Panzer, 1788)
Cicadula albingensis	Wagner, 1940
Cicadula flori	(J. Sahlberg, 1871)
Cicadula frontalis	(Herrich-Schäffer, 1835)
Cicadula persimilis	(Edwards, 1920)
Cicadula placida	(Horváth, 1897)
Cicadula quadrinotata	(Fabricius, 1794)
Cicadula quinquenotata	(Boheman, 1845)
Cylindera arenaria	(Schrank, 1781)
Cylindera arenaria viennensis	(Schrank, 1781)
Cicindela campestris	Linnaeus, 1758
Cylindera germanica	(Linnaeus, 1758)
Cicindela hybrida	Linnaeus, 1758
Lophyridia littoralis	Olivier, 1790
Lophyridia littoralis nemoralis	Olivier, 1790
Cicindela soluta	Dejean, 1822
Cicindela soluta pannonica	Dejean, 1822
Cicindela sylvicola	Dejean, 1822
Cicindela hybrida transversalis	Linnaeus, 1758
Synchita undata	Guérin-Méneville, 1844
Synchita variegata	Hellwig, 1792
Ciconia ciconia	(Linnaeus, 1758)
Ciconia nigra	(Linnaeus, 1758)
Cicurina cicur	(Fabricius, 1793)
Cidaria fulvata	(Forster, 1771)
Cidnopus pilosus	(Leske, 1785)
Cidnopus platiai	Mertlik, 1996
Cidnopus ruzenae	(Laibner, 1977)
Cilea silphoides	(Linné, 1767)
Cilix glaucata	(Scopoli, 1763)
Cimberis attelaboides	(Fabricius, 1787)
Cimex dissimilis	Horváth, 1910
Cimex lectularius	Linnaeus, 1758
Cinclus cinclus	(Linnaeus, 1758)
Cinclus cinclus aquaticus	(Linnaeus, 1758)
Cionellus gibbifrons	(Kiesenwetter, 1851)
Cionus alauda	(Herbst, 1784)
Cionus clairvillei	Boheman, 1838
Cionus ganglbaueri	Wingelmüller, 1914
Cionus gebleri	Gyllenhal, 1838
Cionus hortulanus	(Fourcroy, 1785)
Cionus leonhardi	Wingelmüller, 1914
Cionus longicollis	Ch. Brisout, 1863
Cionus longicollis montanus	Ch. Brisout, 1863
Cionus nigritarsis	Reitter, 1904
Cionus olens	(Fabricius, 1792)
Cionus olivieri	Rosenschöld, 1838
Cionus pulverosus	Gyllenhal, 1838
Cionus scrophulariae	(Linnaeus, 1758)
Cionus thapsus	(Fabricius, 1792)
Cionus tuberculosus	(Scopoli, 1792)
Cionus ungulatus	Germar, 1821
Circaetus gallicus	(Gmelin, 1788)
Circus aeruginosus	(Linnaeus, 1758)
Circus cyaneus	(Linnaeus, 1766)
Circus macrourus	(S. G. Gmelin, 1771)
Circus pygargus	(Linnaeus, 1758)
Cirrorhynchus arrogans	J. Frivaldszky, 1878
Cirrorhynchus kelecsenyi	(Frivaldszky, 1892)
Cis alter	Silfverberg, 1991
Cis bidentatus	(Olivier, 1796)
Cis boleti	(Scopoli, 1763)
Cis castaneus	Mellié, 1848
Cis comptus	Gyllenhall, 1827
Cis dentatus	Mellié, 1848
Cis fagi	Waltl, 1839
Cis fissicollis	Mellié, 1848
Cis fissicornis	Mellié, 1848
Cis glabratus	Mellié, 1848
Cis hispidus	(Paykull, 1798)
Cis jacquemartii	Mellié, 1848
Cis lineatocribratus	Mellié, 1848
Cis micans	(Fabricius, 1792)
Cis punctulatus	Gyllenhall, 1827
Cis rugulosus	Mellié, 1848
Cis setiger	Mellié, 1848
Cis striatulus	Mellié, 1848
Cixidia marginicollis	(Spinola, 1839)
Cixidia parnassia	(Stal, 1859)
Cixius cunicularius	(Linnaeus, 1767)
Cixius distinguendus	Kirschbaum, 1868
Cixius dubius	Wagner, 1939
Cixius granulatus	Horváth, 1897
Cixius haupti	Dlabola, 1949
Cixius heydenii	Kirschbaum, 1868
Cixius nervosus	(Linnaeus, 1758)
Cixius pallipes	Fieber, 1876
Cixius similis	Kirschbaum, 1868
Cixius simplex	(Herrich-Schäffer, 1835)
Cixius stigmaticus	(Germar, 1818)
Clambus armadillo	(De Geer, 1774)
Clambus dux	Endrődy-Younga, 1960
Clambus evae	Endrődy-Younga, 1960
Clambus minutus	(Sturm, 1807)
Clambus nigrellus	Reitter, 1914
Clambus pallidulus	Reitter, 1911
Clambus pilosellus	Reitter, 1876
Clambus pubescens	Redtenbacher, 1849
Clambus punctulum	(Beck, 1817)
Clangula hyemalis	(Linnaeus, 1758)
Clanoptilus affinis	(Ménetriés, 1832)
Clanoptilus ambiguus	(Peyron, 1877)
Clanoptilus elegans	(Olivier, 1790)
Clanoptilus geniculatus	(Germar, 1824)
Clanoptilus marginellus	(Olivier, 1790)
Clanoptilus spinipennis	(Germar, 1824)
Clanoptilus strangulatus	(Abeille de Perrin, 1885)
Clarias gariepinus	Valenciennes, 1840
Clausilia cruciata	(S. Studer, 1820)
Clausilia dubia	Draparnaud, 1805
Clausilia dubia vindobonensis	Draparnaud, 1805
Clausilia pumila	C. Pfeiffer, 1828
Clausilia rugosa	(Draparnaud, 1801)
Clausilia rugosa parvula	(Draparnaud, 1801)
Claviger longicornis	P.W.J. Müller, 1818
Claviger nitidus	Hampe, 1863
Claviger testaceus	Preyssler, 1790
Clemmus troglodytes	Hampe, 1850
Cleoceris scoriacea	(Esper, 1789)
Cleonis pigra	(Scopoli, 1763)
Aulacobaris distinctus	(Boheman, 1845)
Cleopomiarus graminis	(Gyllenhal, 1813)
Cleopomiarus longirostris	(Gyllenhal, 1838)
Cleopomiarus medius	Desbrochers, 1893
Cleopomiarus micros	(Germar, 1821)
Cleopomiarus plantarum	(Germar, 1824)
Cleopus pulchellus	(Herbst, 1795)
Cleopus solani	(Fabricius, 1792)
Cleora cinctaria	(Denis & Schiffermüller, 1775)
Cleorodes lichenaria	(Hufnagel, 1767)
Clepsis consimilana	(Hübner, 1817)
Clepsis pallidana	(Fabricius, 1776)
Clepsis rolandriana	(Linnaeus, 1758)
Clepsis rurinana	(Linnaeus, 1758)
Clepsis senecionana	(Hübner, 1819)
Clepsis spectrana	(Treitschke, 1830)
Cleptes aerosus	Förster, 1853
Cleptes consimilis	Buysson, 1887
Cleptes elegans	Mocsáry, 1901
Cleptes ignitus	(Fabricius, 1787)
Cleptes mocsary	Semenov, 1897
Cleptes nitidulus	(Fabricius, 1793)
Cleptes orientalis	Dahlbom, 1854
Cleptes pallipes	Lepeletier, 1806
Cleptes putoni	Buysson, 1886
Cleptes saussurei	Mocsáry, 1889
Cleptes scutellaris	Mocsáry, 1889
Cleptes semiauratus	(Linnaeus, 1761)
Clerus mutillarius	Fabricius, 1775
Clethrionomys glareolus	(Schreber, 1780)
Clinopodes flavidus	C.L.Koch, 1847
Clitostethus arcuatus	(Rossi, 1794)
Clivina collaris	(Herbst, 1784)
Clivina fossor	(Linnaeus, 1758)
Clivina ypsilon	Dejean, 1829
Cloeon dipterum	(Linnaeus, 1761)
Cloeon simile	Eaton, 1870
Clostera anachoreta	([Denis & Schiffermüller], 1775)
Clostera anastomosis	(Linnaeus, 1758)
Clostera curtula	(Linnaeus, 1758)
Clostera pigra	(Hufnagel, 1766)
Calocoris affinis	(Herrich-Schäffer, 1835)
Calocoris alpestris	(Meyer-Dür, 1843)
Closterotomus biclavatus	(Herrich-Schäffer, 1835)
Closterotomus fulvomaculatus	(De Geer, 1773)
Closterotomus norwegicus	(Gmelin, 1788)
Calocoris roseomaculatus	(De Geer, 1773)
Clubiona alpicola	Kulczynski, 1882
Clubiona brevipes	Blackwall, 1841
Clubiona caerulescens	L. Koch, 1867
Clubiona comta	C.L. Koch, 1839
Clubiona corticalis concolor	(Walckenaer, 1802)
Clubiona diversa	O.P.-Cambridge, 1862
Clubiona frutetorum	L. Koch, 1866
Clubiona genevensis	L. Koch, 1866
Clubiona germanica	Thorell, 1870
Clubiona juvenis	Simon, 1878
Clubiona leucaspis	Simon, 1932
Clubiona lutescens	Westring, 1851
Clubiona marmorata	L.Koch, 1866
Clubiona pallidula	(Clerck, 1757)
Clubiona phragmitis	C.L. Koch, 1843
Clubiona pseudoneglecta	Wunderlich, 1994
Clubiona reclusa	(O.P.-Cambridge, 1863)
Clubiona rosserae	Locket, 1953
Clubiona similis	L. Koch, 1867
Clubiona stagnatilis	Kulczynski, 1897
Clubiona subtilis	L. Koch, 1867
Clubiona terrestris	Westring, 1851
Clubiona trivialis	C.L. Koch, 1843
Clypastrea brunnea	(C. Brisout de Barneville, 1863)
Clypastrea orientalis	(Reitter, 1877)
Clypastrea pusilla	(Gyllenhal, 1810)
Clytus arietis	(Linnaeus, 1758)
Clytus lama	Mulsant, 1847
Clytus rhamni	Germar, 1817
Clytus tropicus	(Panzer, 1794)
Cnaemidophorus rhododactyla	(Denis & Schiffermüller, 1775)
Cnemeplatia atropos	Costa, 1847
Cnephasia abrasana	(Duponchel, 1843)
Cnephasia alticolana	(Herrich-Schäffer, 1851)
Cnephasia asseclana	(Denis & Schiffermüller, 1775)
Cnephasia chrysantheana	(Duponchel, 1843)
Cnephasia communana	(Herrich-Schäffer, 1851)
Cnephasia cupressivorana	(Staudinger, 1871)
Cnephasia ecullyana	Réal, 1951
Cnephasia genitalana	Pierce & Metcalfe, 1922
Cnephasia incertana	(Treitschke, 1835)
Cnephasia oxyacanthana	(Herrich-Schäffer, 1851)
Cnephasia pasiuana	(Hübner, 1799)
Cnephasia stephensiana	(Doubleday, 1849)
Cobitis elongatoides	Bacescu et Maier, 1969
Coccidohystrix artemisiae	(Kiritchenko,1937)
Oxynychus rufa	(Herbst, 1783)
Oxynychus scutellata	(Herbst, 1783)
Coccinella hieroglyphica	Linnaeus, 1758
Coccinella magnifica	Redtenbacher, 1843
Coccinella quinquepunctata	Linnaeus, 1758
Coccinella saucerottii	Mulsant, 1850
Coccinella septempunctata	Linnaeus, 1758
Coccinella undecimpunctata	Linnaeus, 1758
Coccinella undecimpunctata tripunctata	Linnaeus, 1758
Anisosticta quatuordecimpustulata	(Linnaeus, 1758)
Anisosticta sinuatomarginata	(Faldermann, 1837)
Coccothraustes coccothraustes	(Linnaeus, 1758)
Coccotrypes dactyliperda	(Fabricius, 1801)
Coccura comari	(Künow,1880)
Blastesthia turionella	(Linnaeus, 1758)
Cochlicopa lubrica	(O.F. Müller, 1774)
Cochlicopa lubricella	(Rossmässler, 1834)
Cochlicopa nitens	(M. von Gallenstein, 1848)
Cochlodina cerata	(Rossmässler, 1836)
Cochlodina fimbriata	(Rossmässler, 1835)
Cochlodina laminata	(Montagu, 1803)
Cochlodina orthostoma	(Menke, 1828)
Cochylidia heydeniana	(Herrich-Schäffer, 1851)
Cochylidia implicitana	(Wocke, 1856)
Cochylidia moguntiana	(Rössler, 1864)
Cochylidia richteriana	(Fischer v. Röslerstamm, 1837)
Cochylidia rupicola	(Curtis, 1834)
Cochylidia subroseana	(Haworth, 1811)
Cochylimorpha alternana	(Stephens, 1834)
Cochylimorpha clavana	(Constant, 1888)
Cochylimorpha elongana	(Fischer v. Röslerstamm, 1839)
Cochylimorpha hilarana	(Herrich-Schäffer, 1851)
Hysterophora jaculana	Snellen, 1883
Cochylimorpha jucundana	(Treitschke, 1835)
Cochylimorpha obliquana	(Eversmann, 1844)
Cochylimorpha perfusana	(Guenée, 1845)
Cochylimorpha straminea	(Haworth, 1811)
Cochylimorpha woliniana	(Schleich, 1868)
Cochylis atricapitana	(Stephens, 1852)
Cochylis dubitana	(Hübner, 1799)
Cochylis epilinana	Duponchel, 1842
Cochylis flaviciliana	(Westwood, 1854)
Cochylis hybridella	(Hübner, 1813)
Cochylis nana	(Haworth, 1811)
Cochylis pallidana	Zeller, 1847
Cochylis posterana	Zeller, 1847
Cochylis roseana	(Haworth, 1811)
Cochylis salebrana	(Mann, 1862)
Codocera ferruginea	(Eschscholtz, 1818)
Codophila varia	(Fabricius, 1787)
Antheminia varicornis	(Jakovlev, 1874)
Coeliastes lamii	(Fabricius, 1792)
Coeliodes proximus	Schultze, 1895
Coeliodes rana	(Fabricius, 1787)
Coeliodes ruber	(Marsham, 1802)
Coeliodes transversealbofasciatus	(Goeze, 1777)
Coeliodes trifasciatus	Bach, 1854
Coeliodinus rubicundus	(Herbst, 1795)
Coelioys acanthura	Illiger, 1806
Coelioys afra	Lepeletier, 1841
Coelioys alata	Förster, 1853
Coelioys aurolimbata	Förster, 1853
Coelioys brevis	Eversmann, 1852
Coelioys caudata	Spinola, 1838
Coelioys conoidea	(Illiger, 1806)
Coelioys elongata	Lepeletier, 1841
Coelioys emarginata	Förster, 1853
Coelioys haemorrhoa	Förster, 1853
Coelioys inermis	(Kirby, 1802)
Coelioys mandibularis	Nylander, 1848
Coelioys obtusa	Pérez, 1884
Coelioys polycentris	Förster, 1853
Coelioys quadridentata	(Linnaeus, 1761)
Coelioys rufescens	Lepeletier, 1845
Coelioys rufocaudata	Lepeletier, 1841
Coelostoma orbiculare	(Fabricius, 1775)
Coelotes atropos	(Walckenaer, 1830)
Coelotes solitarius	L. Koch, 1868
Coelotes terrestris	(Wider, 1834)
Coenagrion hastulatum	(Charpentier, 1825)
Coenagrion lunulatum	(Charpentier, 1840)
Coenagrion ornatum	(Sélys-Longchamps, 1850)
Coenagrion puella	(Linné, 1758)
Coenagrion pulchellum	(Vander Linden, 1825)
Coenagrion pulchellum interruptum	(Vander Linden, 1825)
Coenagrion scitulum	(Rambur, 1842)
Coenocalpe lapidata	(Hübner, 1809)
Coenonympha arcania	(Linnaeus, 1761)
Coenonympha glycerion	(Borkhausen, 1788)
Coenonympha hero	(Linnaeus, 1761)
Coenonympha oedippus	(Fabricius, 1787)
Coenonympha pamphilus	(Linnaeus, 1758)
Coenonympha tullia	(Müller, 1764)
Colenis immunda	(Sturm, 1807)
Coleophora absinthii	Wocke, 1876
Coleophora acrisella	MilliŐre, 1872
Coleophora adelogrammella	Zeller,1849
Coleophora adjunctella	Hodgkinson, 1882
Coleophora adspersella	Benander, 1939
Coleophora ahenella	Heinemann, 1876
Coleophora albella	(Thunberg, 1788)
Coleophora albicostella	(Duponchel, 1842)
Coleophora albidella	(Denis & Schiffermüller, 1775)
Coleophora albilineella	Toll, 1960
Coleophora albitarsella	Zeller, 1849
Coleophora alcyonipennella	(Kollar, 1832)
Coleophora alticolella	Zeller, 1849
Coleophora amellivora	Baldizzone, 1979
Coleophora anatipenella	(Hübner, 1796)
Coleophora antennariella	Herrich-Schäffer, 1861
Coleophora argentula	(Stephens, 1834)
Coleophora artemisicolella	Bruand, 1855
Coleophora albicans	Zeller, 1849
Coleophora asteris	Mühlig, 1864
Coleophora astragalella	Zeller, 1849
Coleophora atriplicis	Meyrick, 1928
Coleophora auricella	(Fabricius, 1794)
Coleophora badiipennella	(Duponchel, 1843)
Coleophora ballotella	(Fischer v. Röslerstamm, 1839)
Coleophora betulella	Heinemann, 1876
Coleophora bilineatella	Zeller, 1849
Coleophora bilineella	Herrich-Schäffer, 1855
Coleophora binderella	(Kollar, 1832)
Coleophora binotapennella	(Duponchel, 1843)
Coleophora brevipalpella	Wocke, 1874
Coleophora caelebipennella	Zeller, 1839
Coleophora caespititiella	Zeller, 1839
Coleophora cartilaginella	Christoph, 1872
Coleophora cecidophorella	Oudejans, 1972
Coleophora chamaedriella	Bruand, 1852
Coleophora chrysanthemi	Hoffmann, 1896
Coleophora ciconiella	Herrich-Schäffer, 1855
Coleophora clypeiferella	O. Hofmann, 1871
Coleophora colutella	(Fabricius, 1794)
Coleophora congeriella	Staudinger, 1859
Coleophora conspicuella	Zeller, 1849
Coleophora conyzae	Zeller, 1868
Coleophora coracipennella	(Hübner, 1796)
Coleophora cornutella	Herrich-Schäffer, 1861
Coleophora coronillae	Zeller, 1849
Coleophora corsicella	Walsingham, 1898
Coleophora cracella	Vallot, 1835
Coleophora currucipennella	Zeller, 1839
Coleophora deauratella	Lienig & Zeller, 1846
Coleophora dentiferella	Toll, 1952
Coleophora dianthi	Herrich-Schäffer, 1855
Coleophora directella	Zeller, 1849
Coleophora discordella	Zeller, 1849
Coleophora ditella	Zeller, 1849
Coleophora eurasiatica	Baldizzone, 1989
Coleophora flaviella	Mann, 1857
Coleophora flavipennella	(Duponchel, 1843)
Coleophora follicularis	(Vallot, 1802)
Coleophora frankii	A. Schmidt, 1887
Coleophora fringillella	Zeller, 1839
Coleophora frischella	(Linnaeus, 1758)
Coleophora fuscociliella	Zeller, 1849
Coleophora fuscocuprella	Herrich-Schäffer, 1855
Coleophora galatellae	M. Hering, 1942
Coleophora galbulipennella	Zeller, 1838
Coleophora gallipennella	(Hübner, 1796)
Coleophora genistae	Stainton, 1857
Coleophora glaseri	Toll, 1961
Coleophora glaucicolella	Wood, 1892
Coleophora gnaphalii	Zeller, 1839
Coleophora granulatella	Zeller, 1849
Coleophora gryphipennella	(Hübner, 1796)
Coleophora halophilella	Zimmermann, 1926
Coleophora hemerobiella	(Scopoli, 1763)
Coleophora hieronella	Zeller, 1849
Coleophora hospitiella	Chrétien, 1915
Coleophora hungariae	Gozmány, 1955
Coleophora hydrolapathella	M. Hering, 1924
Coleophora ibipennella	Zeller, 1849
Coleophora inulae	Wocke, 1876
Coleophora juncicolella	Stainton, 1851
Coleophora kasyi	Toll, 1961
Coleophora klimeschiella	Toll, 1952
Coleophora kroneella	Fuchs, 1899
Coleophora kuehnella	(Goeze, 1783)
Coleophora laricella	(Hübner, 1817)
Coleophora limosipennella	(Duponchel, 1843)
Coleophora lineolea	(Haworth, 1828)
Coleophora linosyridella	Fuchs, 1880
Coleophora linosyris	M. Hering, 1937
Coleophora lithargyrinella	Zeller, 1849
Coleophora lixella	Zeller, 1849
Coleophora longicornella	Constant, 1893
Coleophora lutipennella	(Zeller, 1838)
Coleophora magyarica	Baldizzone, 1983
Coleophora mayrella	(Hübner, 1813)
Coleophora medelichensis	Krone, 1908
Coleophora millefolii	Zeller, 1849
Coleophora milvipennis	Zeller, 1839
Coleophora motacillella	Zeller, 1849
Coleophora musculella	Mühlig, 1864
Coleophora narbonensis	Baldizzone, 1990
Coleophora niveiciliella	O. Hofmann, 1877
Coleophora niveicostella	Zeller, 1839
Coleophora niveistrigella	Wocke, 1876
Coleophora nomgona	Falkovitsh, 1975
Coleophora nutantella	Mühlig & Frey, 1857
Coleophora obscenella	Herrich-Schäffer, 1855
Coleophora obtectella	Zeller, 1849
Coleophora obviella	Rebel, 1914
Coleophora ochrea	(Haworth, 1828)
Coleophora ochripennella	Zeller, 1849
Coleophora odorariella	Mühlig, 1857
Coleophora onobrychiella	Zeller, 1849
Coleophora onopordiella	Zeller, 1849
Coleophora orbitella	Zeller, 1849
Coleophora oriolella	Zeller, 1849
Coleophora ornatipennella	(Hübner, 1796)
Coleophora otidipennella	(Hübner, 1817)
Coleophora paripennella	Zeller, 1839
Coleophora partitella	Zeller, 1849
Coleophora peisoniella	Kasy, 1965
Coleophora pennella	(Denis & Schiffermüller, 1775)
Coleophora peribenanderi	Toll, 1943
Coleophora pratella	Zeller, 1871
Coleophora preisseckeri	Toll, 1942
Coleophora prunifoliae	Doets, 1944
Coleophora pseudociconiella	Toll, 1952
Coleophora pseudoditella	Baldizzone & Patzak, 1983
Coleophora pseudolinosyris	Kasy, 1979
Coleophora pseudorepentis	Toll, 1960
Coleophora ptarmicia	Walsingham, 1910
Coleophora pulmonariella	Ragonot, 1874
Coleophora punctulatella	Zeller, 1849
Coleophora ramosella	Zeller, 1849
Coleophora remizella	Baldizzone, 1983
Coleophora riffelensis	Rebel, 1913
Coleophora salicorniae	Heinemann & Wocke, 1876
Coleophora salinella	Stainton, 1858
Coleophora saponariella	Heeger, 1848
Coleophora saturatella	Stainton, 1850
Coleophora saxicolella	(Duponchel, 1843)
Coleophora serpylletorum	E. Hering, 1889
Coleophora serratella	(Linnaeus, 1761)
Coleophora siccifolia	Stainton, 1856
Coleophora silenella	Herrich-Schäffer, 1855
Coleophora solitariella	Zeller, 1849
Coleophora spiraeella	Rebel, 1916
Coleophora squalorella	Zeller, 1849
Coleophora squamosella	Stainton, 1856
Coleophora sternipennella	(Zetterstedt, 1839)
Coleophora stramentella	Zeller, 1849
Coleophora striatipennella	Nylander, 1848
Coleophora striolatella	Zeller, 1849
Coleophora sylvaticella	Wood, 1892
Coleophora taeniipennella	Herrich-Schäffer, 1855
Coleophora tamesis	Waters, 1929
Coleophora tanaceti	Mühlig, 1865
Coleophora therinella	Tengström, 1848
Coleophora thymi	M. Hering, 1942
Coleophora trifariella	Zeller, 1849
Coleophora trifolii	(Curtis, 1832)
Coleophora trigeminella	Fuchs, 1881
Coleophora trochilella	(Duponchel, 1843)
Coleophora tyrrhaenica	Amsel, 1952
Coleophora unipunctella	Zeller, 1849
Coleophora uralensis	Toll, 1961
Coleophora versurella	Zeller, 1849
Coleophora vestianella	(Linnaeus, 1758)
Coleophora vibicella	(Hübner, 1813)
Coleophora vibicigerella	Zeller, 1839
Coleophora vicinella	Zeller, 1849
Coleophora virgatella	Zeller, 1849
Coleophora vulnerariae	Zeller, 1839
Coleophora vulpecula	Zeller, 1849
Coleophora wockeella	Zeller, 1849
Coleophora zelleriella	Heinemann, 1854
Coleotechnites piceaella	(Kearfott, 1903)
Colias alfacariensis	Ribbe, 1905
Colias chrysotheme	(Esper, 1781)
Colias croceus	(Fourcroy, 1785)
Colias erate	(Esper, 1805)
Colias hyale	(Linnaeus, 1758)
Colias myrmidone	(Esper, 1780)
Colias palaeno	(Linnaeus, 1761)
Colladonus torneellus	(Zetterstedt, 1828)
Colletes anchusae	Noskiewicz, 1924
Colletes collaris	Dours, 1872
Colletes cunicularius	(Linnaeus, 1761)
Colletes daviesanus	Smith, 1846
Colletes floralis	Eversmann, 1852
Colletes fodiens	(Geoffroy, 1785)
Colletes gallicus	Radoszkowski, 1891
Colletes graeffei	Alfken, 1900
Colletes hylaeiformis	Eversmann, 1852
Colletes inexpectatus	Noskiewicz, 1936
Colletes lebedewi	Noskiewicz, 1936
Colletes marginatus	Smith, 1853
Colletes mlokossewiczi	Radoszkowski, 1891
Colletes nasutus	Smith, 1853
Colletes nigicans	Gistel, 1857
Colletes pallescens	Noskiewicz, 1936
Colletes punctatus	Mocsáry, 1877
Colletes similis	Schenck, 1853
Colletes spectabilis	Morawitz, 1868
Colletes succinctus	(Linnaeus, 1758)
Colobicus hirtus	(Rossi, 1790)
Colobochyla salicalis	([Denis & Schiffermüller], 1775)
Colobopterus erraticus	(Linnaeus, 1758)
Colocasia coryli	(Linnaeus, 1758)
Colon affine	Sturm, 1839
Colon appendiculatum	(C.R. Sahlberg, 1834)
Colon armipes	Kraatz, 1854
Colon brunneum	(Latreille, 1807)
Colon claviger	Herbst, 1797
Colon dentipes	(C.R. Sahlberg, 1834)
Colon fuscicorne	Kraatz, 1852
Colon latum	Kraatz, 1850
Colon murinum	Kraatz, 1850
Colon rufescens	Kraatz, 1850
Colon serripes	(C.R. Sahlberg, 1834)
Colon viennense	Herbst, 1797
Colostygia olivata	(Denis & Schiffermüller, 1775)
Colostygia pectinataria	(Knoch, 1781)
Colotes hampei	Redtenbacher, 1874
Colotois pennaria	(Linnaeus, 1761)
Colpa interrupta	(Fabricius, 1781)
Colpa quinquecincta	(Fabricius, 173)
Colpa quinquecincta abdominalis	(Fabricius, 173)
Colposis mutilatus	(Beck, 1817)
Coluber caspius	Gmelin, 1789
Coluber jugularis	Linnaeus, 1758
Columba livia	Gmelin, 1789
Columba livia f. domestica	Gmelin, 1789
Columba oenas	Linnaeus, 1758
Columba palumbus	Linnaeus, 1758
Columella edentula	(Draparnaud, 1805)
Colydium elongatum	Fabricius, 1792
Colydium filiforme	Fabricius, 1792
Colymbetes fuscus	(Linnaeus, 1758)
Colymbetes striatus	(Linnaeus, 1758)
Comaroma simoni	Bertkau, 1889
Combocerus glaber	(Schaller, 1783)
Comibaena bajularia	(Denis & Schiffermüller, 1775)
Saperda populnea	(Linnaeus, 1758)
Compsidolon absinthii	Scott, 1870
Compsidolon pumilum	(Jakovlev, 1876)
Compsidolon salicellum	(Meyer-Dür, 1843)
Conalia baudii	Mulsant et Rey, 1858
Coniocleonus cicatricosus	(Hoppe, 1795)
Coniocleonus excoriatus	(Gyllenhal, 1834)
Coniocleonus hollbergii	(Fahraeus, 1842)
Coniocleonus nigrosuturatus	(Goeze, 1777)
Coniocleonus turbatus	(Fahraeus, 1842)
Coniopteryx arcuata	Kis, 1965
Coniopteryx aspoecki	Kis, 1967
Coniopteryx borealis	Tjeder, 1930
Coniopteryx drammonti	Rousset, 1964
Coniopteryx esbenpeterseni	Tjeder, 1930
Coniopteryx haematica	McLachlan, 1868
Coniopteryx hoelzeli	Aspöck, 1964
Coniopteryx lentiae	H. Aspöck et U. Aspöck, 1964
Coniopteryx pygmaea	Enderlein, 1906
Coniopteryx renate	Rausch et H. Aspöck, 1977
Coniopteryx tineiformis	Curtis, 1834
Coniopteryx tjederi	Kimmins, 1934
Conisania leineri	(Freyer, 1836)
Conisania luteago	([Denis & Schiffermüller], 1775)
Conistra erythrocephala	([Denis & Schiffermüller], 1775)
Conistra ligula	(Esper, 1791)
Conistra rubiginea	([Denis & Schiffermüller], 1775)
Conistra rubiginosa	(Scopoli, 1763)
Conistra vaccinii	(Linnaeus, 1761)
Conistra veronicae	(Hübner, 1813)
Conobathra repandana	(Fabricius, 1798)
Conobathra tumidana	(Denis & Schiffermüller, 1775)
Conocephalus fuscus	(Fabricius, 1793)
Conocephalus dorsalis	(Latreille, 1804)
Conomelus anceps	(Germar, 1821)
Conomelus dehneli	Nast, 1966
Conopalpus testaceus	(Olivier, 1783)
Conosanus obsoletus	(Kirschbaum, 1858)
Conostethus hungaricus	Wagner, 1941
Conostethus roseus	(Fallén, 1807)
Conwentzia pineticola	Enderlein, 1905
Conwentzia psociformis	(Curtis, 1834)
Copelatus haemorrhoidalis	(Fabricius, 1787)
Copium clavicorne	(Linnaeus, 1758)
Copium teucrii	(Host, 1788)
Coprimorphus scrutator	(Herbst, 1789)
Copris lunaris	(Linnaeus, 1758)
Copris umbilicatus	Abeille de Perrin, 1901
Coproceramius atramentarius	(Gyllenhal, 1810)
Coproceramius cinnamopterus	(Thomson, 1856)
Coproceramius europaeus	(Likovsky, 1984)
Coproceramius laevanus	(Mulsant et Rey, 1852)
Coproceramius marcidus	(Erichson, 1837)
Coproceramius parapicipennis	(Brundin, 1954)
Coproceramius putridus	(Kraatz, 1856)
Coproceramius setigerus	(Sharp, 1869)
Coprophilus piceus	(Solsky, 1867)
Coprophilus striatulus	(Fabricius, 1792)
Coproporus colchicus	Kraatz, 1857
Coprothassa melanaria	(Mannerheim, 1830)
Coptosoma mucronatum	Seidenstücker, 1963
Coptosoma scutellatum	(Geoffroy, 1785)
Coquillettidia richiardii	(Ficalbi, 1889)
Coracias garrulus	Linnaeus, 1758
Coraebus elatus	(Fabricius, 1787)
Coraebus florentinus	(Herbst, 1801)
Coraebus rubi	(Linnaeus, 1767)
Coraebus undatus	(Fabricius, 1787)
Coranus contrarius	(Reuter, 1881)
Coranus griseus	(Rossi, 1790)
Coranus kerzhneri	P. Putshkov, 1982
Coranus laticeps	E. Wagner, 1952
Coranus subapterus	(De Geer, 1773)
Corbicula fluminalis	(O.F. Müller, 1774)
Corbicula fluminea	(O.F. Müller, 1774)
Cordalia obscura	(Gravenhorst, 1802)
Cordicomus gracilis	(Panzer, 1797)
Cordicomus sellatus	(Panzer, 1797)
Cordulegaster bidentata	Selys, 1843
Cordulegaster heros	Theischinger, 1979
Cordulia aenea	(Linné, 1758)
Cordylepherus viridis	(Fabricius, 1787)
Coregonus albula	(Linnaeus, 1758)
Coregonus lavaretus	(Linnaeus, 1758)
Coreus marginatus	(Linnaeus, 1758)
Coriarachne depressa	(C.L. Koch, 1837)
Coriomeris denticulatus	(Scopoli, 1763)
Coriomeris hirticornis	(Fabricius, 1794)
Coriomeris scabricornis	(Panzer, 1809)
Corixa affinis	(Leach, 1817)
Corixa panzeri	Fieber, 1848
Corixa punctata	(Illiger, 1807)
Corizus hyoscyami	(Linnaeus, 1758)
Cornu aspersum	(O.F. Müller, 1774)
Cornutiplusia circumflexa	(Linnaeus, 1767)
Coronella austriaca	Laurenti, 1768
Corticaria bella	Redtenbacher, 1849
Corticaria crenulata	(Gyllenhal, 1827)
Corticaria elongata	(Gyllenhal, 1827)
Corticaria fagi	Wollaston, 1854
Corticaria ferruginea	Marsham, 1802
Corticaria fulva	(Comolli, 1837)
Corticaria impressa	(Olivier, 1790)
Corticaria lapponica	(Zetterstedt, 1838)
Corticaria linearis	(Paykull, 1798)
Corticaria longicollis	(Zetterstedt, 1838)
Corticaria longicornis	(Herbst, 1793)
Corticaria obscura	Brisout de Barneville, 1863
Corticaria polypori	Sahlberg, 1900
Corticaria pubescens	(Gyllenhal, 1827)
Corticaria saginata	Mannerheim, 1844
Corticaria serrata	(Paykull, 1798)
Corticaria umbilicata	Beck, 1817
Corticarina fuscula	(Gyllenhal, 1827)
Corticarina obfuscata	A. Strand, 1937
Corticarina similata	(Gyllenhal, 1827)
Corticarina truncatella	(Mannerheim, 1844)
Hypophloeus bicolor	(Olivier, 1790)
Hypophloeus fasciatus	Fabricius, 1790
Hypophloeus fraxini	Kugelann, 1794
Hypophloeus linearis	Fabricius, 1790
Hypophloeus longulus	Gyllenhal, 1827
Hypophloeus pini	Panzer, 1799
Hypophloeus suberis	Lucas, 1846
Hypophloeus unicolor	(Piller & Mitterpacher, 1783)
Hypophloeus versipellis	Baudi, 1876
Cortinicara gibbosa	(Herbst, 1793)
Cortodera femorata	(Fabricius, 1787)
Cortodera flavimana	(Waltl, 1838)
Cortodera holosericea	(Fabricius, 1801)
Cortodera humeralis	(Schaller, 1783)
Cortodera villosa	Heyden, 1876
Corvus corax	Linnaeus, 1758
Corvus cornix	Linnaeus, 1758
Corvus corone	Linnaeus, 1758
Corvus corone sardonius	Linnaeus, 1758
Corvus frugilegus	Linnaeus, 1758
Corvus monedula	Linnaeus, 1758
Corvus monedula soemmeringi	Linnaeus, 1758
Corvus monedula spermologus	Linnaeus, 1758
Corylophus cassidoides	(Marsham, 1802)
Coryphaelus gyllenhalii	(Fallén, 1826)
Coryssomerus capucinus	(Beck, 1817)
Corythucha ciliata	(Say, 1832)
Coscinia cribaria	(Linnaeus, 1758)
Cosmardia moritzella	(Treitschke, 1835)
Cosmia affinis	(Linnaeus, 1767)
Cosmia diffinis	(Linnaeus, 1767)
Cosmia pyralina	([Denis & Schiffermüller], 1775)
Cosmia trapezina	(Linnaeus, 1758)
Elachista freyerella	(Hübner, 1825)
Elachista stabilella	Stainton, 1858
Cosmobaris scolopacea	Germar, 1824
Cosmopterix lienigiella	Lienig & Zeller, 1846
Cosmopterix orichalcea	Stainton, 1861
Cosmopterix scribaiella	Zeller, 1850
Cosmopterix zieglerella	(Hübner, 1810)
Cosmorhoe ocellata	(Linnaeus, 1758)
Cosmotettix caudatus	(Flor, 1861)
Cossonus cylindricus	C. R. Sahlberg, 1835
Cossonus linearis	(Fabricius, 1775)
Cossonus parallelepipedus	(Herbst, 1795)
Cossus cossus	(Linnaeus, 1758)
Costaconvexa polygrammata	(Borkhausen, 1794)
Cotaster uncipes	(Boheman, 1838)
Cottus gobio	Linnaeus, 1758
Cottus poecilopus	Heckel, 1837
Coturnix coturnix	(Linnaeus, 1758)
Coxelus pictus	(Sturm, 1807)
Crabro cribrarius	(Linnaeus, 1758)
Crabro loewi	Dahlbom, 1845
Crabro peltarius	(Schreber, 1784)
Crabro scutellatus	(Scheven, 1781)
Crambus ericella	(Hübner, 1813)
Crambus hamella	(Thunberg, 1788)
Crambus lathoniellus	(Zincken, 1817)
Crambus pascuella	(Linnaeus, 1758)
Crambus perlella	(Scopoli, 1763)
Crambus pratella	(Linnaeus, 1758)
Crambus silvella	(Hübner, 1813)
Crambus uliginosellus	Zeller, 1850
Craniophora ligustri	([Denis & Schiffermüller], 1775)
Craspedosoma rawlinsii	Leach, 1814
Craspedosoma transsilvanicum	(Verhoeff, 1897)
Craspedosoma transsilvanicum austriacum	(Verhoeff, 1897)
Craspedosoma transsilvanicum barcsicum	(Verhoeff, 1897)
Craspedosoma transsilvanicum pakozdense	(Verhoeff, 1897)
Crassa tinctella	(Hübner, 1796)
Crassa unitella	(Hübner, 1796)
Crataraea suturalis	(Mannerheim, 1830)
Cratosilis denticollis	(Schummel, 1844)
Crematogaster schmidti	(Mayr, 1852)
Crematogaster scutellartis	(Olivier, 1791)
Cremnocephalus alpestris	Wagner, 1941
Creoleon plumbeus	(Olivier, 1811)
Creophilus maxillosus	(Linnaeus, 1758)
Crepidophorus mutilatus	(Rosenhauer, 1847)
Crex crex	(Linnaeus, 1758)
Cricetus cricetus	(Linnaeus, 1758)
Criocoris crassicornis	(Hahn, 1834)
Criocoris nigricornis	Reuter, 1894
Criocoris nigripes	Fieber, 1861
Criocoris sulcicornis	(Kirschbaum, 1856)
Criomorphus albomarginatus	Curtis, 1833
Criomorphus williamsi	China, 1939
Criorhina asilica	(Fallén, 1816)
Criorhina pachymera	(Egger, 1858)
Criorhina ranunculi	(Panzer, 1804)
Crocallis elinguaria	(Linnaeus, 1758)
Crocallis tusciaria	(Borkhausen, 1793)
Crocidosema plebejana	Zeller, 1847
Crocidura leucodon	(Hermann, 1780)
Crocidura suaveolens	(Pallas, 1811)
Crocothemis erythraea	(Brullé, 1862)
Crombrugghia distans	(Zeller, 1847)
Crombrugghia tristis	(Zeller, 1841)
Crossobela trinotella	(Herrich-Schäffer, 1856)
Crossocerus acanthophorus	(Kohl, 1892)
Crossocerus annulipes	(Lepeletier & Brullé, 1834)
Crossocerus assimilis	(Smith, 1856)
Crossocerus barbipes	(Dahlbom, 1845)
Crossocerus binotatus	Lepeletier & Brullé, 1834
Crossocerus capitosus	(Shuckard, 1837)
Crossocerus cetratus	(Shuckard, 1837)
Crossocerus congener	(Dahlbom, 1844)
Crossocerus denticoxa	(Bischoff, 1932)
Crossocerus denticrus	Herrich-Schaeffer, 1841
Crossocerus dimidiatus	(Fabricius, 1781)
Crossocerus distinguendus	(Morawitz, 1866)
Crossocerus elongatulus	(Vander Linden, 1829)
Crossocerus exiguus	(Vander Linden, 1829)
Crossocerus megacephalus	(Rossi, 1790)
Crossocerus nigritus	(Lepeletier & Brullé, 1834)
Crossocerus ovalis	Lepeletier & Brullé, 1834
Crossocerus palmipes	(Linnaeus, 1767)
Crossocerus podagricus	(Vander Linden, 1829)
Crossocerus pusillus	Lepeletier & Brullé, 1834
Crossocerus quadrimaculatus	(Fabricius, 1793)
Crossocerus tarsatus	(Shuckard, 1837)
Crossocerus vagabundus	(Panzer, 1798)
Crossocerus walkeri	(Shuckard, 1837)
Crossocerus wesmaeli	(Vander Linden, 1829)
Crunoecia irrorata	(Curtis, 1834)
Crunoecia kempnyi	Morton, 1901
Crustulina guttata	(Wider, 1834)
Crustulina sticta	(O.P.-Cambridge, 1861)
Cryphaeus cornutus	(W.Fischer, 1823)
Cryphalus abietis	(Ratzeburg, 1837)
Cryphalus piceae	(Ratzeburg, 1837)
Cryphalus saltuarius	J. Weise, 1891
Cryphia algae	(Fabricius, 1775)
Cryphia domestica	(Hufnagel, 1766)
Cryphia ereptricula	(Treitschke, 1825)
Cryphia fraudatricula	(Hübner, 1803)
Cryphia muralis	(Forster, 1771)
Cryphia raptricula	([Denis & Schiffermüller], 1775)
Cryphia receptricula	(Hübner, 1803)
Cryphoeca silvicola	(C.L. Koch, 1834)
Crypsedra gemmea	(Treitschke, 1825)
Crypsinus angustatus	(Bärensprung, 1859)
Cryptarcha strigata	(Fabricius, 1787)
Cryptarcha undata	(Olivier, 1790)
Crypticus quisquilius	(Linnaeus, 1761)
Cryptobium collare	(Reitter, 1884)
Cryptobium fracticorne	(Paykull, 1800)
Cryptoblabes bistriga	(Haworth, 1811)
Cryptocandona dudichi	(Klie, 1930)
Cryptocandona matris	Sywula, 1976
Cryptocandona vavrai	Kaufmann, 1900
Cryptocheilus confinis	Haupt, 1937
Cryptocheilus egregius	(Lepeletier, 1845)
Cryptocheilus elegans	Spinola, 1806
Cryptocheilus fabricii	(Vander Linden, 1827)
Cryptocheilus freygessneri	(Kohl, 1883)
Cryptocheilus guttulatus	(Costa, 1887)
Cryptocheilus notatus	(Rossi, 1792)
Cryptocheilus notatus affinis	(Rossi, 1792)
Cryptocheilus richardsi	Móczár, 1953
Cryptocheilus simillimus	Haupt, 1927
Cryptocheilus variabilis	(Rossi, 1790)
Cryptocheilus versicolor	(Scopoli, 1763)
Cryptococcus aceris	Borchsenius,1937
Cryptococcus fagisuga	Lindinger,1912
Cryptocochylis conjunctana	(Mann, 1864)
Cryptocyclops bicolor	(Sars, 1863)
Cryptodrassus hungaricus	(Balogh, 1935)
Cryptolestes capensis	(Waltl, 1834)
Cryptolestes corticinus	(Erichson, 1846)
Cryptolestes duplicatus	(Waltl, 1839)
Cryptolestes ferrugineus	(Stephens, 1831)
Cryptolestes pusilloides	(Steel et Howe, 1952)
Cryptolestes pusillus	(Schönherr, 1817)
Cryptolestes spartii	(Curtis, 1834)
Cryptolestes turcicus	(Grouvelle, 1876)
Cryptophagus acutangulus	Gyllenhal, 1828
Cryptophagus affinis	Sturm, 1845
Cryptophagus badius	Sturm, 1845
Cryptophagus baldensis	Erichson, 1846
Cryptophagus cellaris	(Scopoli, 1763)
Cryptophagus confusus	Bruce, 1934
Cryptophagus croaticus	Reitter, 1879
Cryptophagus dentatus	(Herbst, 1793)
Cryptophagus deubeli	Ganglbauer, 1897
Cryptophagus distinguendus	Sturm, 1845
Cryptophagus fallax	Balfour-Browne, 1953
Cryptophagus fuscicornis	Sturm, 1845
Cryptophagus hexagonalis	Tournier, 1872
Cryptophagus immixtus	Rey, 1889
Cryptophagus labilis	Erichson, 1846
Cryptophagus lycoperdi	(Scopoli, 1763)
Cryptophagus micaceus	Rey, 1889
Cryptophagus nitidulus	Miller, 1858
Cryptophagus pallidus	Sturm, 1845
Cryptophagus pilosus	Gyllenhal, 1828
Cryptophagus pseudodentatus	Bruce, 1934
Cryptophagus pubescens	Sturm, 1845
Cryptophagus quercinus	Kraatz, 1852
Cryptophagus rotundatus	Coombs et Woodroffe, 1955
Cryptophagus saginatus	Sturm, 1845
Cryptophagus scanicus	(Linnaeus, 1758)
Cryptophagus schmidti	Sturm, 1845
Cryptophagus scutellatus	Newman, 1834
Cryptophagus setulosus	Sturm, 1845
Cryptophagus shroetteri	Reitter, 1912
Cryptophagus simplex	Miller, 1858
Cryptophagus sporadum	Bruce, 1934
Cryptophagus subdepressus	Gyllenhal, 1828
Cryptophagus subfumatus	Kraatz, 1856
Cryptophagus thomsoni	Reitter, 1875
Cryptophilus integer	(Heer, 1841)
Cryptophonus melancholicus	(Dejean, 1829)
Cryptophonus tenebrosus	Dejean, 1829
Cryptophonus tenebrosus centralis	Dejean, 1829
Cryptopleurum crenatum	(Panzer, 1794)
Cryptopleurum minutum	(Fabricius, 1775)
Cryptopleurum subtile	Sharp, 1884
Cryptopone ochraceum	(Mayr, 1855)
Cryptops anomalans	Newport, 1844
Cryptops hortensis	Leach, 1815
Cryptops parisi	Brölemann, 1920
Cryptorhynchus lapathi	(Linnaeus, 1758)
Cryptostemma alienum	(Herrich-Schäffer, 1835)
Cryptostemma pusillimum	(J. Sahlberg, 1870)
Cryptostemma waltli	Fieber, 1860
Crypturgus cinereus	(Herbst, 1793)
Crypturgus hispidulus	Thomson, 1870
Crypturgus pusillus	(Gyllenhal, 1813)
Ctenicera cuprea	(Fabricius, 1781)
Ctenicera pectinicornis	(Linnaeus, 1758)
Ctenicera virens	(Schrank, 1781)
Cteniopus sulphureus	(Linnaeus, 1758)
Cteniopus sulphuripes	(Germar, 1824)
Ctenistes palpalis	Reichenbach, 1816
Ctenopharyngodon idella	(Valenciennes, 1844)
Ctesias serra	(Fabricius, 1792)
Cucujus cinnaberinus	(Scopoli, 1763)
Cucullia absinthii	(Linnaeus, 1761)
Cucullia argentea	(Hufnagel, 1766)
Cucullia artemisiae	(Hufnagel, 1766)
Cucullia asteris	([Denis & Schiffermüller], 1775)
Cucullia balsamitae	Boisduval, 1840
Cucullia campanulae	Freyer, 1831
Cucullia chamomillae	([Denis & Schiffermüller], 1775)
Cucullia dracunculi	(Hübner, 1813)
Cucullia formosa	Rogenhofer, 1860
Cucullia fraudatrix	Eversmann, 1837
Cucullia gnaphalii	(Hübner, 1813)
Cucullia lactucae	([Denis & Schiffermüller], 1775)
Cucullia lucifuga	([Denis & Schiffermüller], 1775)
Cucullia mixta lorica	Freyer, 1841
Cucullia scopariae	Dorfmeister, 1853
Cucullia tanaceti	([Denis & Schiffermüller], 1775)
Cucullia umbratica	(Linnaeus, 1758)
Cucullia xeranthemi	Boisduval, 1840
Cuculus canorus	Linnaeus, 1758
Culex hortensis	Ficalbi, 1890
Culex martinii	Medschid, 1930
Culex mimeticus	Noé, 1899
Culex modestus	Ficalbi, 1890
Culex pipiens	Linnaeus, 1758
Culex pipiens molestus	Linnaeus, 1758
Culex territans	Walker, 1856
Culex theileri	Theobald, 1903
Culex torrentium	Martini, 1925
Culiseta alaskaensis	(Ludlow, 1906)
Culiseta annulata	(Schrank, 1776)
Culiseta glaphyroptera	(Schiner, 1864)
Culiseta longiareolata	(Macquart, 1838)
Culiseta morsitans	(Theobald, 1901)
Culiseta subochrea	(Edwards, 1921)
Cunctochrysa albolineata	(Killington, 1935)
Cupido alcetas	(Hoffmannsegg, 1804)
Cupido argiades	(Pallas, 1771)
Cupido decolorata	(Staudinger, 1886)
Cupido minimus	(Fuessly, 1775)
Cupido osiris	(Meigen, 1829)
Curculio betulae	(Stephens, 1831)
Curculio elephas	(Gyllenhal, 1836)
Curculio glandium	Marsham, 1802
Curculio nucum	Linnaeus, 1758
Curculio pellitus	(Boheman, 1843)
Curculio propinquus	(Desbrochers, 1868)
Curculio rubidus	(Gyllenhal, 1836)
Curculio venosus	(Gravenhorst, 1807)
Curculio villosus	Fabricius, 1781
Curelius dilutus	(Reitter, 1883)
Curelius exiguus	(Erichson, 1846)
Curimopsis austriaca	(Franz, 1967)
Curimopsis paleata	(Erichson, 1846)
Curimus erichsoni	Reitter, 1881
Curimus erinaceus	(Duftschmid, 1825)
Curtimorda bisignata	(Redtenbacher, 1849)
Curtimorda maculosa	(Naezen, 1794)
Cyanapion afer	(Gyllenhal, 1833)
Cyanapion alcyoneum	(Germar, 1817)
Cyanapion columbinum	(Germar, 1817)
Cyanapion gyllenhalii	(Kirby, 1808)
Cyanapion platalea	(Germar, 1817)
Cyanapion spencii	(Kirby, 1808)
Cyanostolus aeneus	(Richter, 1820)
Cybaeus angustiarum	L. Koch, 1868
Cybister lateralimarginalis	(De Geer, 1774)
Cybocephalus fodori	Endrôdy-Younga, 1965
Cybocephalus politus	(Gyllenhal, 1813)
Cybocephalus pulchellus	Erichson, 1845
Cybosia mesomella	(Linnaeus, 1758)
Cychramus luteus	(Fabricius, 1787)
Cychramus variegatus	(Herbst, 1792)
Cychrus attenuatus	(Fabricius, 1792)
Cychrus caraboides	(Linnaeus, 1758)
Cyclocypris globosa	(Sars, 1863)
Cyclocypris helocrenica	Fuhrmann & Pietrzeniuk, 1990
Cyclocypris laevis	(O.F. Müller, 1776)
Cyclocypris ovum	(Jurine, 1820)
Cycloderes pilosulus	(Herbst, 1795)
Cyclodinus dentatus	(Pic, 1895)
Cyclodinus dentatus transdanubianus	(Pic, 1895)
Cyclodinus humilis	(Germar, 1824)
Cyclophora albiocellaria	(Hübner, 1789)
Cyclophora albipunctata	(Hufnagel, 1767)
Cyclophora annularia	(Fabricius, 1775)
Cyclophora linearia	(Hübner, 1799)
Cyclophora pendularia	(Clerck, 1759)
Cyclophora porata	(Linnaeus, 1767)
Cyclophora punctaria	(Linnaeus, 1758)
Cyclophora puppillaria	(Hübner, 1799)
Cyclophora quercimontaria	(Bastelberger, 1897)
Cyclophora ruficiliaria	(Herrich-Schäffer, 1855)
Cyclophora suppunctaria	(Zeller, 1847)
Cyclops furcifer	Claus, 1857
Cyclops insignis	Claus, 1857
Cyclops kikuchii	Smirnov, 1932
Cyclops scutifer	Sars, 1863
Cyclops strenuus	Fischer, 1851
Cyclops vicinus	Uljanin, 1875
Cyclosa conica	(Pallas, 1772)
Cyclosa oculata	(Walckenaer, 1802)
Diaphora luctuosa	(Hübner, 1831)
Diaphora sordida	(Hübner, 1803)
Cydia amplana	(Hübner, 1800)
Cydia conicolana	(Heylaerts, 1874)
Cydia coniferana	(Saxesen, 1840)
Cydia corollana	(Hübner, 1823)
Cydia cosmophorana	(Treitschke, 1835)
Cydia duplicana	(Zetterstedt, 1839)
Cydia exquisitana	(Rebel, 1889)
Cydia fagiglandana	(Zeller, 1841)
Cydia illutana	(Herrich-Schäffer, 1851)
Cydia inquinatana	(Hübner, 1800)
Cydia leguminana	(Lienig & Zeller, 1846)
Cydia medicaginis	(Kuznetsov, 1962)
Cydia microgrammana	(Guenée, 1845)
Cydia nigricana	(Fabricius, 1794)
Cydia oxytropidis	(Martini, 1912)
Cydia pactolana	(Zeller, 1840)
Cydia pomonella	(Linnaeus, 1758)
Cydia pyrivora	(Danilevsky, 1947)
Cydia servillana	(Duponchel, 1836)
Cydia strobilella	(Linnaeus, 1758)
Cydia succedana	(Denis & Schiffermüller, 1775)
Cydia splendana	(Hübner, 1799)
Cydia zebeana	(Ratzeburg, 1840)
Cydnus aterrimus	(Förster, 1771)
Cygnus columbianus	(Ord, 1815)
Cygnus bewickii	Yarrell, 1830
Cygnus cygnus	(Linnaeus, 1758)
Cygnus olor	(Gmelin, 1789)
Cylindroiulus abaligetanus	Verhoeff, 1901
Cylindroiulus arborum	Verhoeff, 1928
Cylindroiulus boleti	(C.L.Koch, 1847)
Cylindroiulus horvathi	(Verhoeff, 1897)
Cylindroiulus latestriatus	(Curtis, 1845)
Cylindroiulus luridus	(C.L.Koch, 1847)
Cylindroiulus Meinerti	(Verhoeff, 1891)
Cylindroiulus parisiorum	(Brölemann & Verhoeff, 1896)
Cylindroiulus truncorum	(Silvestri, 1896)
Cylindromorphus filum	(Gyllenhal, 1817)
Cylister angustatus	(Hoffmann, 1803)
Cylister linearis	(Erichson, 1834)
Cylister oblongum	(Fabricius, 1892)
Cyllecoris histrionius	(Linnaeus, 1767)
Cyllodes ater	(Herbst, 1792)
Cymatia coleoptrata	(Fabricius, 1776)
Cymatia rogenhoferi	(Fieber, 1864)
Cymatophorina diluta	([Denis & Schiffermüller], 1775)
Cymbiodyta marginella	(Fabricius, 1792)
Cymindis angularis	Gyllenhal, 1810
Cymindis axillaris	(Fabricius, 1794)
Cymindis budensis	Csiki, 1908
Cymindis cingulata	Dejean, 1825
Cymindis humeralis	(Fourcroy, 1785)
Cymindis lineata	(Quensel, 1806)
Cymindis scapularis	Schaum, 1857
Cymindis miliaris	(Fabricius, 1801)
Cymolomia hartigiana	(Saxesen, 1840)
Cymus aurescens	Distant, 1883
Cymus claviculus	(Fallén, 1807)
Cymus glandicolor	(Hahn, 1831)
Cymus melanocephalus	Fieber, 1861
Cynaeda dentalis	(Denis & Schiffermüller, 1775)
Cynaeda gigantea	(Wocke, 1871)
Cynegetis impunctata	(Linnaeus, 1767)
Cypha discoidea	(Erichson, 1839)
Cypha longicornis	(Paykull, 1800)
Cypha seminulum	(Erichson, 1839)
Cypha tarsalis	(Luze, 1902)
Cyphea curtula	(Erichson, 1837)
Cyphocleonus achates	(Fahraeus, 1842)
Cyphocleonus dealbatus	(Gmelin, 1790)
Cyphocleonus dealbatus tigrinus	(Gmelin, 1790)
Cyphocleonus trisulcatus	(Herbst, 1795)
Cyphon coarctatus	Paykull, 1799
Cyphon laevipennis	Tournier, 1868
Cyphon ochraceus	Stephens, 1830
Cyphon padi	(Linnaeus, 1758)
Cyphon palustris	Thomson, 1855
Cyphon pubescens	(Fabricius, 1792)
Cyphon ruficeps	Tournier, 1868
Cyphon variabilis	(Thunberg, 1787)
Cyphostethus tristriatus	(Fabricius, 1787)
Cypria exsculpta	(Fischer, 1855)
Cypria lata	Sywula, 1981
Cypria ophthalmica	(Jurine, 1820)
Cypria reptans	Bronstein, 1928
Cypridopsis hartwigi	G.W. Müller, 1900
Cypridopsis vidua	(O.F. Müller, 1776)
Cyprinus carpio	Linnaeus, 1758
Cypris pubera	O.F. Müller, 1776
Cypris striata	(Jurine, 1820)
Cyprois marginata	(Straus, 1821)
Cyrnus crenaticornis	(Kolenati, 1859)
Cyrnus flavidus	McLachlan, 1864
Cyrnus trimaculatus	(Curtis, 1834)
Cyrtanaspis phalerata	(Germar, 1831)
Cyrtusa subtestacea	(Gyllenhal, 1813)
Cystobranchus fasciatus	(Kollar, 1842)
Cystobranchus respirans	(Troschel, 1850)
Cystomutilla ruficeps	(Smith, 1855)
Cytilus sericeus	(Forster, 1771)
Cyzicus tetracerus	(Krynicki, 1830)
Dacne bipustulata	(Thunberg, 1781)
Dacne notata	(Gmelin, 1788)
Dacne rufifrons	(Fabricius, 1775)
Dacrila fallax	(Kraatz, 1856)
Dadobia immersa	(Erichson, 1837)
Dahlica inconspicuella	(Stainton, 1843)
Dahlica lichenella	(Linnaeus, 1761)
Dahlica nickerlii	(Heinemann, 1870)
Dahlica triquetrella	(Hübner, 1813)
Dalopius marginatus	(Linnaeus, 1758)
Dalotia coriaria	(Kraatz, 1856)
Dama dama	(Linnaeus, 1758)
Danacea marginata	(Küster, 1851)
Danacea morosa	Kiesenwetter, 1863
Danacea nigritarsis	(Küster, 1850)
Danacea pallidipalpis	Abeille de Perrin, 1894
Danacea pallipes	(Panzer, 1793)
Danacea serbica	Kiesenwetter, 1863
Daphnia ambigua	Scourfield, 1946
Daphnia atkinsoni	Baird, 1859
Daphnia cristata	Sars, 1862
Daphnia cucullata	Sars, 1862
Daphnia curvirostris	Eylmann, 1887
Daphnia galeata	Sars, 1864
Daphnia hyalina	Leydig, 1860
Daphnia longispina	O.F. Müller, 1785
Daphnia magna	Straus, 1820
Daphnia obtusa	Kurz, 1874
Daphnia parvula	Fordyce, 1901
Daphnia pulex	Leydig, 1860
Daphnia pulicaria	Forbes, 1893
Daphnia schoedleri	Sars, 1862
Daphnia similis	Claus, 1876
Daphnis nerii	(Linnaeus, 1758)
Dapsa denticollis	(Germar, 1817)
Dapsa fodori	Csiki, 1907
Darwinula stevensoni	(Brady & Robertson, 1870)
Dascillus cervinus	(Linnaeus, 1758)
Dasumia canestrinii	(L. Koch, 1876)
Dasycerus sulcatus	Brongniart, 1800
Dasygnypeta velata	(Erichson, 1837)
Dasylabris maura	(Linnaeus, 1758)
Dasylabris regalis	(Fabricius, 1793)
Dasypoda argentata	(Panzer, 1809)
Dasypoda braccata	Eversmann, 1852
Dasypoda hirtipes	(Fabricius, 1793)
Dasypoda suripes	(Christ, 1791)
Dasysyrphus albostriatus	(Fallén, 1817)
Dasysyrphus friuliensis	(Van der Goot, 1960)
Dasysyrphus hilaris	(Zetterstedt, 1843)
Dasysyrphus pinastri	(De Geer, 1776)
Dasysyrphus tricinctus	(Fallén, 1817)
Dasysyrphus venustus	(Meigen, 1822)
Dasytes aerosus	Kiesenwetter, 1867
Dasytes buphtalmus	Baudi, 1873
Dasytes cyaneus	(Fabricius, 1775)
Dasytes fusculus	(Illiger, 1801)
Dasytes griseus	Küster, 1849
Dasytes hickeri	Kaszab, 1955
Dasytes niger	(Linnaeus, 1761)
Dasytes nigrocyaneus	Mulsant & Rey, 1868
Dasytes obscurus	Gyllenhal, 1813
Dasytes plumbeus	(O.F. Müller, 1776)
Dasytes subaeneus	Schönherr, 1817
Dasytes subalpinus	Baudi di Selve, 1873
Dasytes tardus	Schaufuss, 1872
Dasytes virens	Marsham, 1802
Dasystoma salicella	(Hübner, 1796)
Datomicra canescens	(Sharp, 1869)
Datomicra dadopora	(Thomson, 1867)
Datomicra nigra	(Kraatz, 1856)
Datomicra sordidula	(Erichson, 1837)
Datomicra zosterae	(Thomson, 1856)
Datonychus angulosus	(Boheman, 1845)
Datonychus arquata	(Herbst, 1795)
Datonychus derennei	(Guillaume, 1936)
Datonychus melanostictus	(Marsham, 1802)
Datonychus paszlavszkyi	(Kuthy, 1890)
Datonychus scabrirostris	(Hochhuth, 1847)
Datonychus transsylvanicus	(Schultze, 1897)
Datonychus urticae	(Boheman, 1845)
Daudebardia brevipes	(Draparnaud, 1805)
Daudebardia rufa	(Draparnaud, 1805)
Decantha borkhausenii	(Zeller, 1839)
Decticus verrucivorus	Linne, 1758
Deilephila elpenor	(Linnaeus, 1758)
Deilephila porcellus	(Linnaeus, 1758)
Deileptenia ribeata	(Clerck, 1759)
Deilus fugax	(Olivier, 1790)
Deinopsis erosa	(Stephens, 1832)
Deleaster dichrous	(Gravenhorst, 1802)
Delichon urbicum	(Linnaeus, 1758)
Deliphrosoma prolongatum	Rottenberg, 1873
Delphacinus mesomelas	(Boheman, 1850)
Delphacodes audrasi	Ribaut, 1954
Delphacodes capnodes	(Scott, 1870)
Delphacodes mulsanti	(Fieber, 1866)
Delphacodes venosus	(Germar, 1830)
Delphax crassicornis	(Panzer, 1796)
Delta unguiculatum	(Villers, 1789)
Deltocephalus pulicaris	(Fallén, 1806)
Deltote bankiana	(Fabricius, 1775)
Deltote deceptoria	(Scopoli, 1763)
Lithacodia uncula	(Clerck, 1759)
Demetrias imperialis	(Germar, 1824)
Demetrias atricapillus	(Linnaeus, 1758)
Demetrias monostigma	Samouelle, 1819
Dendrocopos leucotos	(Bechstein, 1803)
Dendrocopos major	(Linnaeus, 1758)
Dendrocopos major pinetorum	(Linnaeus, 1758)
Dendrocopos medius	(Linnaeus, 1758)
Dendrocopos minor	(Linnaeus, 1758)
Dendrocopos minor hortorum	(Linnaeus, 1758)
Dendrocopos syriacus	(Hemprich & Ehrenberg, 1833)
Dendroleon pantherinus	(Fabricius, 1787)
Dendrolimus pini	(Linnaeus, 1758)
Dendrophilus punctatus	(Herbst, 1792)
Dendrophilus pygmaeus	(Linnaeus, 1758)
Dendroxena quadrimaculata	(Scopoli, 1772)
Dendryphantes rudis	(Sundevall, 1832)
Denisia augustella	(Hübner, 1796)
Denisia luctuosella	(Duponchel, 1840)
Denisia similella	(Hübner, 1796)
Denisia stipella	(Linnaeus, 1758)
Denops albofasciatus	(Charpentier, 1825)
Denticollis linearis	(Paykull, 1800)
Denticollis rubens	(Piller et Mittelpacher, 1783)
Deporaus betulae	(Linnaeus, 1758)
Deporaus mannerheimii	(Hummel, 1823)
Depressaria absynthiella	Herrich-Schäffer, 1865
Depressaria albipunctella	(Denis & Schiffermüller, 1775)
Depressaria artemisiae	Nickerl, 1864
Depressaria badiella	(Hübner, 1796)
Depressaria cervicella	Herrich-Schäffer, 1854
Depressaria chaerophylli	Zeller, 1839
Depressaria corticinella	Zeller, 1854
Depressaria daucella	([Denis et Schiffermüller],1775)
Depressaria depressana	(Fabricius, 1775)
Depressaria douglasella	Stainton, 1849
Depressaria emeritella	Stainton, 1849
Depressaria libanotidella	Schläger, 1849
Depressaria marcella	Rebel, 1901
Depressaria olerella	Zeller, 1854
Depressaria heraclei	(Retzius, 1783)
Depressaria pimpinellae	Zeller, 1839
Depressaria pulcherrimella	Stainton, 1849
Depressaria ultimella	Stainton, 1849
Deraeocoris annulipes	(Herrich-Schäffer, 1845)
Deraeocoris lutescens	(Schilling, 1836)
Deraeocoris morio	(Boheman, 1852)
Deraeocoris olivaceus	(Fabricius, 1776)
Deraeocoris punctulatus	(Fallén, 1807)
Deraeocoris putoni	(Montandon, 1885)
Deraeocoris ruber	(Linnaeus, 1758)
Deraeocoris rutilus	(Herrich-Schäffer, 1839)
Deraeocoris serenus	(Douglas & Scott, 1868)
Deraeocoris trifasciatus	(Linnaeus, 1767)
Deraeocoris ventralis	Reuter, 1904
Derephysia cristata	(Panzer, 1806)
Derephysia foliacea	(Fallén, 1807)
Dermestes ater	De Geer, 1774
Dermestes bicolor	Fabricius, 1781
Dermestes carnivorus	Fabricius, 1775
Dermestes erichsoni	Ganglbauer, 1904
Dermestes frischi	Kugelann, 1792
Dermestes fuliginosus	Rossi, 1792
Dermestes gyllenhali	Laporte De Castelnau, 1840
Dermestes intermedius	Kalik, 1951
Dermestes kaszabi	Kalik, 1951
Dermestes laniarius	Illiger, 1801
Dermestes lardarius	Linnaeus, 1758
Dermestes maculatus	De Geer, 1774
Dermestes murinus	Linnaeus, 1758
Dermestes olivieri	Lepesme, 1939
Dermestes szekessyi	Kalik, 1950
Dermestes undulatus	Brahm, 1790
Deroceras agreste	(Linnaeus, 1758)
Deroceras klemmi	Grossu, 1972
Deroceras laeve	(O.F. Müller, 1774)
Deroceras reticulatum	(O.F. Müller, 1774)
Deroceras rodnae	Grossu & Lupu, 1965
Deroceras sturanyi	(Simroth, 1894)
Deroceras turcicum	(Simroth, 1894)
Derodontus macularis	(Fuss, 1850)
Deronectes latus	(Stephens, 1829)
Deroplia genei	(Aragona, 1830)
Deroxena venosulella	(Möschler, 1862)
Derula flavoguttata	Mulsant & Rey, 1856
Deutoleon lineatus	(Fabricius, 1798)
Deuterogonia pudorina	(Wocke, 1857)
Devia prospera	(Erichson, 1839)
Dexiogyia corticina	(Erichson, 1837)
Diachromus germanus	(Linnaeus, 1758)
Diachrysia chrysitis	(Linnaeus, 1758)
Diachrysia chryson	(Esper, 1789)
Diachrysia nadeja	(Oberthür, 1880)
Diachrysia stenochrysis	(Warren, 1913)
Diachrysia zosimi	(Hübner, 1822)
Diaclina fagi	(Panzer, 1799)
Diaclina testudinea	(Piller et Mitterpacher, 1783)
Diacrisia sannio	(Linnaeus, 1758)
Diacyclops bicuspidatus	(Claus, 1857)
Diacyclops bisetosus	(Rehberg, 1880)
Diacyclops crassicaudis	(Sars, 1863)
Diacyclops languidoides	(Lilljeborg, 1901)
Diacyclops languidus	(Sars, 1863)
Diacyclops nanus	(Sars, 1863)
Diaea dorsata	(Fabricius, 1777)
Diaea livens	Simon, 1876
Dialectica imperialella	(Zeller, 1847)
Dialectica soffneri	(Gregor et Povolny, 1965)
Dianous coerulescens	(Gyllenhal, 1810)
Diaperis boleti	(Linnaeus, 1758)
Diaphanosoma brachyurum	(Liévin, 1848)
Diaphanosoma lacustris	Korinek, 1981
Diaphanosoma mongolianum	Uéno, 1938
Diaphora mendica	(Clerck, 1759)
Diaptomus castor	(Jurine, 1820)
Diarsia brunnea	([Denis & Schiffermüller], 1775)
Diarsia dahlii	(Hübner, 1813)
Diarsia mendica	(Fabricius, 1775)
Diarsia rubi	(Vieweg, 1790)
Diasemia reticularis	(Linnaeus, 1761)
Diaspidiotus alni	(Marchal,1909)
Diaspidiotus bavaricus	(Lindinger,1912)
Diaspidiotus distinctus	(Leonardi,1900)
Diaspidiotus hungaricus	Kosztarab,1956
Diaspidiotus osborni	(Newell et Cockerell,1898)
Diaspidiotus wuenni	Lindinger,1911
Diastictus vulneratus	(Sturm, 1805)
Elachista kalki	Parenti, 1978
Calliteara fascelina	(Linnaeus, 1758)
Diceratura ostrinana	(Guenée, 1845)
Diceratura roseofasciana	(Mann, 1855)
Dicerca aenea	(Linnaeus, 1761)
Dicerca alni	(Fischer von Waldheim, 1824)
Dicerca berolinensis	(Herbst, 1779)
Dicerca furcata	(Thunberg, 1787)
Dichagyris candelisequa	([Denis & Schiffermüller], 1775)
Dicheirotrichus gustavii	Crotch, 1871
Dicheirotrichus lacustris	(Redtenbacher, 1858)
Dicheirotrichus rufithorax	(Sahlberg, 1827)
Dichelia histrionana	(Frölich, 1828)
Dichochrysa abdominalis	(Brauer, 1850)
Dichochrysa flavifrons	(Brauer, 1850)
Dichochrysa inornata	(Navás, 1901)
Dichochrysa prasina	(Burmeister, 1839)
Dichochrysa ventralis	(Curtis, 1834)
Dichochrysa zelleri	(Schneider, 1851)
Dichomeris barbella	(Denis & Schiffermüller, 1775)
Dichomeris derasella	(Denis & Schiffermüller, 1775)
Dichomeris limosellus	(Schläger, 1849)
Dichomeris marginella	(Fabricius, 1781)
Dichomeris rasilella	(Herrich-Schäffer, 1854)
Dichomeris ustalella	(Fabricius, 1794)
Dichonia aeruginea	(Hübner, 1808)
Griposia aprilina	(Linnaeus, 1758)
Dichonia convergens	([Denis & Schiffermüller], 1775)
Dichrooscytus gustavi	Josifov, 1981
Dichrooscytus rufipennis	(Fallén, 1807)
Dichrooscytus valesianus	Fieber, 1861
Dichrorampha acuminatana	(Lienig & Zeller, 1846)
Dichrorampha aeratana	(Pierce & Metcalfe, 1915)
Dichrorampha agilana	(Tengström, 1848)
Dichrorampha alpinana	(Treitschke, 1830)
Dichrorampha cacaleana	(Herrich-Schäffer, 1851)
Dichrorampha cinerascens	(Danilevsky, 1948)
Dichrorampha cinerosana	(Herrich-Schäffer, 1851)
Dichrorampha consortana	(Stephens, 1852)
Dichrorampha distinctana	(Heinemann, 1863)
Dichrorampha flavidorsana	Knaggs, 1867
Dichrorampha gruneriana	(Herrich-Schäffer, 1851)
Dichrorampha vancouverana	McDunnough, 1935
Dichrorampha heegerana	(Duponchel, 1843)
Dichrorampha montanana	(Duponchel, 1843)
Dichrorampha obscuratana	(Wolff, 1955)
Dichrorampha petiverella	(Linnaeus, 1758)
Dichrorampha plumbana	(Scopoli, 1763)
Dichrorampha podoliensis	(Toll, 1942)
Dichrorampha senectana	Guenée, 1845
Dichrorampha sequana	(Hübner, 1799)
Dichrorampha simpliciana	(Haworth, 1811)
Dichrostigma flavipes	(Stein, 1863)
Dicranocephalus agilis	(Scopoli, 1763)
Dicranocephalus albipes	(Fabricius, 1781)
Dicranocephalus medius	(Mulsant & Rey, 1870)
Dicranotropis divergens	Kirschbaum, 1868
Dicranotropis hamata	(Boheman, 1947)
Dicranura ulmi	([Denis & Schiffermüller], 1775)
Dicronychus cinereus	(Herbst, 1784)
Dicronychus equiseti	(Herbst, 1784)
Dryudella tricolor	(Vander Linden, 1829)
Dicronychus equisetoides	Lohse, 1976
Dicronychus rubripes	(Germar, 1824)
Dictyla convergens	(Herrich-Schäffer, 1835)
Dictyla echii	(Schrank, 1781)
Dictyla humuli	(Fabricius, 1794)
Dictyla lupuli	(Herrich-Schäffer, 1839)
Dictyla nassata	(Puton, 1874)
Dictyla platyoma	(Fieber, 1861)
Dictyla rotundata	(Herrich-Schäffer, 1835)
Dictyna arundinacea	(Linnaeus, 1758)
Dictyna civica	(Lucas, 1850)
Dictyna latens	(Fabricius, 1775)
Dictyna pusilla	Thorell, 1856
Dictyna szaboi	Chyzer, 1891
Dictyna uncinata	Thorell, 1856
Dictyna vicina	Simon, 1873
Dictyonota strichnocera	Fieber, 1844
Dictyophara europaea	(Linnaeus, 1767)
Dictyophara multireticulata	Mulsant et Rey, 1855
Dictyophara pannonica	(Germar, 1830)
Dictyoptera aurora	(Herbst, 1784)
Dicycla oo	(Linnaeus, 1758)
Dicymbium nigrum	(Blackwall, 1834)
Dicyphus constrictus	(Boheman, 1852)
Dicyphus epilobii	Reuter, 1883
Dicyphus errans	(Wolff, 1804)
Dicyphus geniculatus	(Fieber, 1858)
Dicyphus globulifer	(Fallén, 1829)
Dicyphus hyalinipennis	(Burmeister, 1835)
Dicyphus pallidus	(Herrich-Schäffer, 1835)
Dicyphus stachydis	Reuter, 1883
Dicyrtomellus luctuosus	(Mocsáry, 1879)
Didea alneti	(Fallén, 1817)
Didea fasciata	Macquart, 1834
Didea intermedia	Loew, 1854
Didineis crassicornis	Handlirsch, 1888
Didineis lunicornis	(Fabricius, 1798)
Didineis wuestneii	Viereck, 1906
Dieckmanniellus gracilis	(L. Redtenbacher, 1849)
Dieckmanniellus helveticus	(Tournier, 1867)
Dieckmanniellus nitidulus	(Gyllenhal, 1838)
Dienerella argus	(Reitter, 1884)
Dienerella clathrata	(Mannerheim, 1844)
Dienerella costulata	(Reitter, 1877)
Dienerella elegans	(Aubé, 1850)
Dienerella elongata	(Curtis, 1830)
Dienerella filiformis	(Gyllenhal, 1827)
Dienerella filum	(Aubé, 1850)
Dienerella ruficollis	(Marsham, 1802)
Digitivalva arnicella	(Heyden, 1863)
Dufourea dentiventris	(Nylander, 1848)
Digitivalva granitella	(Treitschke, 1833)
Digitivalva pulicariae	(Klimesch, 1956)
Digitivalva reticulella	(Hübner, 1796)
Digitivalva valeriella	(Snellen, 1878)
Dignathodon microcephalus	(Lucas, 1846)
Dignomus nitidus	(Duftschmid, 1825)
Dikraneura variata	Hardy, 1850
Dilacra fleischeri	(Eppelsheim, 1892)
Dilacra luteipes	(Erichson, 1837)
Dilacra vilis	(Erichson, 1837)
Diloba caeruleocephala	(Linnaeus, 1758)
Dimetrota cadaverina	(Ch.Brisout de Barneville, 1860)
Dimorphopterus doriae	(Ferrari, 1874)
Dimorphopterus spinolae	(Signoret, 1857)
Dina apathyi	Gedroyc, 1916
Dina lineata	(O.F. Müller, 1774)
Dina punctata	Johansson, 1927
Dinaraea aequata	(Erichson, 1837)
Dinaraea angustula	(Gyllenhal, 1810)
Dinaraea arcana	(Erichson, 1839)
Dinaraea linearis	(Gravenhorst, 1802)
Dinaraea petraea	Ádám, 1992
Dinarda dentata	(Gravenhorst, 1806)
Dinarda maerkelii	Kiesenwetter, 1843
Dinarda pygmaea	Wasmann, 1894
Dinetus pictus	(Fabricius, 1793)
Dinodes decipiens	(Dufour, 1820)
Dinodes decipiens ambiguus	(Dufour, 1820)
Dinoptera collaris	(Linnaeus, 1758)
Dinothenarus fossor	(Scopoli, 1772)
Dinothenarus pubescens	(De Geer, 1774)
Diodesma subterranea	(Guérin-Méneville, 1829)
Diodontus brevilabris	Beaumon, 1967
Diodontus insidiosus	Spooner, 1938
Diodontus luperus	Shuckard, 1837
Diodontus major	Kohl, 1901
Diodontus medius	Dahlbom, 1845
Diodontus minutus	(Fabricius, 1793)
Diodontus tristis	(Vander Linden, 1829)
Diomphalus hispidulus	Fieber, 1864
Dionconotus confluens	Hoberlandt, 1945
Dioryctria abietella	(Denis & Schiffermüller, 1775)
Dioryctria schuetzeella	Fuchs, 1899
Dioryctria simplicella	Heinemann, 1863
Dioryctria sylvestrella	(Ratzeburg, 1840)
Dioszeghyana schmidti	(Diószeghy, 1935)
Dioxys cincta	(Jurine, 1807)
Dioxys pannonica	Mocsáry, 1887
Dioxys tridentata	(Nylander, 1848)
Diplapion confluens	(Kirby, 1808)
Diplapion detritum	(Mulsant & Rey, 1859)
Diplapion stolidum	(Germar, 1817)
Eudonia lacustrata	(Panzer, 1804)
Diplocephalus alpinus strandi	(O.P.-Cambridge, 1872)
Diplocephalus cristatus	(Blackwall, 1833)
Diplocephalus latifrons	(O.P.-Cambridge, 1863)
Diplocephalus picinus	(Blackwall, 1841)
Diplocoelus fagi	Guérin-Ménéville, 1844
Diplocolenus bohemani	(Zetterstedt, 1840)
Diplocolenus frauenfeldi	(Fieber, 1869)
Diplodoma adspersella	Heinemann, 1870
Diplodoma laichartingella	(Goeze, 1783)
Diplostyla concolor	(Wider, 1834)
Dipoena braccata	(C.L. Koch, 1841)
Dipoena coracina	(C.L. Koch, 1837)
Dipoena erythropus	(Simon, 1881)
Phycosoma inornatum	(O. P.-Cambridge, 1861)
Dipoena melanogaster	(C.L. Koch, 1837)
Dipoena nigroreticulata	(Simon, 1879)
Dipoena torva	(Thorell, 1875)
Dipogon bifasciatus	(Geoffroy, 1785)
Dipogon monticolus	Wachis, 1972
Dipogon subintermedius	(Magretti, 1886)
Dipogon variegatus	(Linnaeus, 1758)
Dipogon vechti	Day, 1979
Dircaea australis	Fairmaire,  1856
Dirhinosia cervinella	(Eversmann, 1854)
Discoelius dufourii	Lepeletier, 1841
Discoelius zonalis	(Panzer, 1801)
Venusia blomeri	(Curtis, 1832)
Discus perspectivus	(Megerle von Mühlfeld, 1816)
Discus rotundatus	(O.F. Müller, 1774)
Discus ruderatus	(W. Hartmann, 1821)
Dismodicus bifrons	(Blackwall, 1841)
Dismodicus elevatus	(C.L. Koch, 1838)
Disparalona rostrata	(Koch, 1841)
Dissoleucas niveirostris	(Fabricius, 1798)
Distoleon tetragrammicus	(Fabricius, 1798)
Ditropis pteridis	(Spinola, 1839)
Ditropsis flavipes	(Signoret, 1865)
Diura bicaudata	(Linnaeus, 1758)
Diurnea fagella	(Denis & Schiffermüller, 1775)
Diurnea lipsiella	(Denis & Schiffermüller, 1775)
Divaena haywardi	(Tams, 1926)
Dixus clypeatus	(Rossi, 1790)
Dochmonota clancula	(Erichson, 1837)
Dochmonota rudiventris	(Eppelsheim, 1886)
Dociostaurus brevicollis	(Eversmann, 1848)
Dociostaurus maroccanus	(Thunberg, 1815)
Dufourea inermis	(Nylander, 1848)
Dodecastichus geniculatus	(Germar, 1817)
Dodecastichus inflatus	(Gyllenhal, 1834)
Dodecastichus mastix	(Olivier, 1807)
Dodecastichus pulverulentus	Germar, 1824
Dolerocypris fasciata	(O.F. Müller, 1776)
Drymus pilicornis	(Mulsant & Rey, 1852)
Dolicharthria punctalis	(Denis & Schiffermüller, 1775)
Dolichoderus quadripunctatus	(Linnaeus, 1758)
Dolichosoma femorale	Morawitz, 1861
Dolichosoma lineare	(Rossi, 1792)
Dolichosoma simile	(Brullé, 1832)
Dolichovespula adulterina	Buysson, 1905
Dolichovespula media	(Retzius, 1783)
Dolichovespula norwegica	(Fabricius, 1781)
Dolichovespula omissa	(Bischoff, 1931)
Dolichovespula saxonica	(Fabricius, 1793)
Dolichovespula sylvestris	(Scopoli, 1763)
Dolichurus corniculus	(Spinola, 1808)
Dolichus halensis	(Schaller, 1783)
Dolomedes fimbriatus	(Clerck, 1757)
Dolomedes plantarius	(Clerck, 1757)
Doloploca punctulana	(Denis & Schiffermüller, 1775)
Dolycoris baccarum	(Linnaeus, 1758)
Domene scabricollis	(Erichson, 1840)
Donacaula forficella	(Thunberg, 1794)
Donacaula mucronella	(Denis & Schiffermüller, 1775)
Donacochara speciosa	(Thorell, 1875)
Donaspastus pannonicus	Gozmány, 1952
Donus comatus	(Boheman, 1842)
Neoglanis cyrtus	(Germar, 1821)
Donus dauci	(Olivier, 1807)
Neoglanis elegans	(Boheman, 1842)
Neoglanis intermedius	(Boheman, 1842)
Pachypera kraatzi	(Capiomont, 1868)
Neoglanis maculatus	(W. Redtenbacher, 1842)
Neoglanis oxalidis	(Herbst, 1795)
Neoglanis palumbarius	(Germar, 1821)
Neoglanis rubi	(Krauss, 1900)
Neoglanis velutinus	(Boheman, 1842)
Neoglanis viennensis	(Herbst, 1795)
Donus zoilus	(Scopoli, 1763)
Doratulina pallifrons	(Horváth, 1897)
Doratura concors	Horváth, 1903
Doratura exilis	Horváth, 1903
Doratura heterophyla	Horváth, 1903
Doratura homophyla	(Flor, 1861)
Doratura impudica	Horváth, 1897
Doratura salina	Horváth, 1903
Doratura stylata	(Boheman, 1847)
Dorcatoma chrysomelina	Sturm, 1837
Dorcatoma dresdensis	Herbst, 1792
Dorcatoma flavicornis	(Fabricius, 1792)
Dorcatoma robusta	Strand, 1938
Dorcatoma serra	Panzer, 1796
Dorcatoma setosella	Mulsant Et Rey, 1864
Dorcus parallelipipedus	(Linnaeus, 1758)
Doros profuges	(Harris, [1780])
Dorycephalus baeri	Kouchakewitch, 1866
Dorypetalum degenerans	(Latzel, 1884)
Dorytomus dejeani	Faust, 1883
Dorytomus dorsalis	(Linnaeus, 1758)
Dorytomus edoughensis	Desbrochers, 1875
Dorytomus filirostris	(Gyllenhal, 1836)
Dorytomus hirtipennis	Bedel, 1884
Dorytomus ictor	(Herbst, 1795)
Dorytomus longimanus	(Forster, 1771)
Dorytomus majalis	(Paykull, 1800)
Dorytomus melanophthalmus	(Paykull, 1792)
Dorytomus minutus	(Gyllenhal, 1836)
Dorytomus nebulosus	(Gyllenhal, 1836)
Dorytomus nordenskioldi	Faust, 1883
Dorytomus occalescens	(Gyllenhal, 1836)
Dorytomus puberulus	(Boheman, 1843)
Dorytomus rufatus	(Bedel, 1888)
Dorytomus salicinus	(Gyllenhal, 1827)
Dorytomus salicis	Walton, 1851
Dorytomus schoenherri	Faust, 1883
Dorytomus suratus	(Gyllenhal, 1836)
Dorytomus taeniatus	(Fabricius, 1781)
Dorytomus tortrix	(Linnaeus, 1761)
Dorytomus tremulae	(Fabricius, 1787)
Dorytomus villosulus	(Gyllenhal, 1836)
Doydirhynchus austriacus	(Olivier, 1807)
Drapetes cinctus	(Panzer, 1796)
Drapetisca socialis	(Sundevall, 1833)
Drassodes cupreus	(Blackwall, 1834)
Drassodes heeri	(Pavesi, 1873)
Drassodes hypocrita	(Simon, 1878)
Drassodes lapidosus	(Walckenaer, 1802)
Drassodes pubescens	(Thorell, 1856)
Drassodes villosus	(Thorell, 1856)
Drassyllus lutetianus	(L. Koch, 1866)
Drassyllus praeficus	(L. Koch, 1866)
Drassyllus pumilus	(C.L. Koch, 1839)
Drassyllus pusillus	(C.L. Koch, 1833)
Drassyllus villicus	(Thorell, 1875)
Drassyllus vinealis	(Kulczynski, 1897)
Drasterius bimaculatus	(Rossi, 1790)
Dreissena polymorpha	(Pallas, 1771)
Drepana curvatula	(Borkhausen, 1790)
Drepana falcataria	(Linnaeus, 1758)
Drepanepteryx algida	(Erichson in Mindendorff, 1851)
Drepanepteryx phalaenoides	(Linnaeus, 1758)
Dreposcia umbrina	(Erichson, 1837)
Drilus concolor	Ahrens, 1812
Drobacia banatica	(Rossmässler, 1838)
Dromaeolus barnabita	(Villa, 1838)
Dromius agilis	(Fabricius, 1787)
Dromius angustus	Brullé, 1834
Dromius fenestratus	(Fabricius, 1794)
Dromius quadraticollis	Morawitz, 1862
Dromius quadrimaculatus	(Linnaeus, 1758)
Dromius schneideri	Crotch, 1871
Phyllodrepa ioptera	(Stephens, 1832)
Phyllodrepa vilis	(Erichson, 1840)
Drusilla canaliculata	(Fabricius, 1787)
Drusus annulatus	(Stephens, 1837)
Drusus biguttatus	(Pictet, 1834)
Drusus trifidus	McLachlan, 1868
Drymonia dodonaea	(Denis & Schiffermüller, 1775)
Drymonia obliterata	(Esper, 1785)
Drymonia querna	([Denis & Schiffermüller], 1775)
Drymonia ruficornis	(Hufnagel, 1766)
Drymonia velitaris	(Hufnagel, 1766)
Drymus brunneus	(F. Sahlberg, 1848)
Drymus latus	Douglas & Scott, 1871
Drymus ryeii	Saunders, 1892
Drymus sylvaticus	(Fabricius, 1775)
Dryobotodes eremita	(Fabricius, 1775)
Dryobotodes monochroma	(Esper, 1790)
Dryocoetes autographus	(Ratzeburg, 1837)
Dryocoetes hectographus	Reitter, 1913
Dryocoetes villosus	(Fabricius, 1792)
Dryocopus martius	(Linnaeus, 1758)
Dryodurgades dlabolai	Wagner, 1963
Dryodurgades reticulatus	(Herrich-Schäffer, 1834)
Dryomys nitedula	(Pallas, 1779)
Dryophilocoris flavoquadrimaculatus	(De Geer, 1773)
Dryophilocoris luteus	(Herrich-Schäffer, 1835)
Dryophilus pusillus	(Gyllenhal, 1808)
Dryophthorus corticalis	(Paykull, 1792)
Dryops anglicanus	Edwards, 1909
Dryops auriculatus	(Geoffroy, 1785)
Dryops ernesti	Des Gozis, 1886
Dryops griseus	(Erichson, 1847)
Dryops nitidulus	(Heer, 1841)
Dryops rufipes	(Krynicki, 1832)
Dryops similaris	Bollow, 1936
Dryops viennensis	(Laporte de Castelnau, 1840)
Drypta dentata	(Rossi, 1790)
Dufourea minuta	Lepeletier, 1841
Dufourea vulgaris	Schenck, 1861
Dufouriellus ater	(Dufour, 1833)
Dunhevedia crassa	King, 1853
Duponchelia fovealis	Zeller, 1879
Duvalius gebhardti	Bokor, 1926
Duvalius hungaricus	Csiki, 1903
Dypterygia scabriuscula	(Linnaeus, 1758)
Dyroderes umbraculatus	(Fabricius, 1775)
Dysauxes ancilla	(Linnaeus, 1767)
Dyschiriodes aeneus	(Dejean, 1825)
Dyschiriodes agnatus	(Motschulsky, 1844)
Dyschirius angustatus	(Ahrens, 1830)
Dyschirius arenosus	Stephens, 1827
Dyschirius benedikti	Bulirsch, 1995
Dyschiriodes bonellii	(Putzeys, 1846)
Dyschiriodes chalceus	(Erichson, 1837)
Dyschiriodes chalybaeus	(Apfelbeck, 1899)
Dyschiriodes chalybaeus gibbifrons	(Apfelbeck, 1899)
Dyschirius digitatus	(Dejean, 1825)
Dyschiriodes extensus	(Putzeys, 1846)
Dyschiriodes globosus	(Herbst, 1783)
Dyschiriodes intermedius	(Putzeys, 1846)
Dyschiriodes laeviusculus	(Putzeys, 1846)
Dyschiriodes lafertei	(Putzeys, 1846)
Dyschirius latipennis	Seidlitz, 1867
Dyschirius luticola	Chaudoir, 1850
Dyschiriodes nitidus	(Dejean, 1825)
Dyschirius obscurus	(Gyllenhal, 1827)
Dyschiriodes parallelus	(Putzeys, 1846)
Dyschiriodes parallelus ruficornis	(Putzeys, 1846)
Dyschiriodes politus	(Dejean, 1825)
Dyschiriodes pusillus	(Dejean, 1825)
Dyschiriodes rufipes	(Dejean, 1825)
Dyschiriodes salinus	(Putzeys, 1846)
Dyschiriodes salinus striatopunctatus	(Putzeys, 1846)
Dyschiriodes strumosus	(Erichson, 1837)
Dyschiriodes substriatus	(G. Müller, 1922)
Dyschiriodes substriatus priscus	(G. Müller, 1922)
Dyschiriodes tristis	(Stephens, 1827)
Dyscia conspersaria	(Denis & Schiffermüller, 1775)
Dysdera crocata	C.L. Koch, 1838
Dysdera erythrina lantosquensis	(Walckenaer, 1802)
Dysdera hungarica	Kulczynski, 1897
Dysdera longirostris	Doblika, 1853
Dysdera ninnii	Canestrini, 1868
Dysdera westringi	O.P.-Cambridge, 1872
Dysepicritus rufescens	(Costa, 1843)
Dysgonia algira	(Linnaeus, 1767)
Dysmicoccus walkeri	(Newstead,1891)
Dyspessa ulula	(Borkhausen, 1790)
Dystebenna stephensi	(Stainton, 1849)
Dytiscus circumcinctus	Ahrens, 1811
Dytiscus circumflexus	Fabricius, 1801
Dytiscus dimidiatus	Bergsträsser, 1778
Dytiscus latissimus	Linnaeus, 1758
Dytiscus marginalis	Linnaeus, 1758
Eana argentana	(Clerck, 1759)
Eana canescana	(Guenée, 1845)
Eana derivana	(La Harpe, 1858)
Eana incanana	(Stephens, 1852)
Eana osseana	(Scopoli, 1763)
Earias clorana	(Linnaeus, 1761)
Earias vernana	(Fabricius, 1787)
Ebaeus appendiculatus	Erichson, 1840
Ebaeus ater	Kiesenwetter, 1863
Ebaeus coerulescens	Erichson, 1840
Ebaeus flavicornis	Erichson, 1840
Ebaeus gibbus	(Drapiez, 1819)
Ebaeus pedicularius	(Fabricius, 1777)
Ebarrius cognatus	(Fieber, 1869)
Ebarrius interstinctus	(Fieber, 1896)
Eblisia minor	(Rossi, 1792)
Ebulea crocealis	(Hübner, 1796)
Ebulea testacealis	(Zeller, 1847)
Ecclisopteryx dalecarlica	Kolenati, 1848
Ecclisopteryx madida	(McLachlan, 1867)
Eccopisa effractella	Zeller, 1848
Ecdyonurus aurantiacus	(Burmeister, 1839)
Ecdyonurus dispar	(Curtis, 1834)
Ecdyonurus insignis	(Eaton, 1870)
Ecdyonurus siveci	Jacob et Braasch, 1984
Ecdyonurus starmachi	Sowa, 1971
Ecdyonurus subalpinus	Klapálek, 1905
Ecdyonurus submontanus	Landa, 1969
Ecdyonurus torrentis	Kimmins, 1942
Ecdyonurus venosus	(Fabricius, 1775)
Echemus angustifrons	(Westring, 1862)
Echinocamptus echinatus	(Mrazek, 1893)
Echinocamptus georgevitchi	(Chappuis, 1927)
Echinocamptus luenensis	Schmeil, 1894
Echinodera valida	(Hampe, 1864)
Ecliptopera capitata	(Herrich-Schäffer, 1839)
Ecliptopera silaceata	(Denis & Schiffermüller, 1775)
Ecnomus tenellus	(Rambur, 1842)
Ecpyrrhorrhoe rubiginalis	(Hübner, 1796)
Ectemnius borealis	(Zetterstedt, 1838)
Ectemnius cavifrons	(Thomson, 1870)
Ectemnius cephalotes	(Olivier, 1792)
Ectemnius confinis	(Walker, 1871)
Ectemnius continuus	(Fabricius, 1804)
Ectemnius crassicornis	(Spinola, 1808)
Ectemnius dives	(Lepeletier & Brullé, 1834)
Ectemnius fossorius	(Linnaeus, 1758)
Ectemnius guttatus	(Vander Linden, 1929)
Ectemnius lapidarius	(Panzer, 1804)
Ectemnius lituratus	(Panzer, 1804)
Ectemnius meridionalis	(Costa, 1871)
Ectemnius nigritarsus	(Herrich-Schaeffer, 1841)
Ectemnius rubicola	(Dufour & Perris, 1840)
Ectemnius ruficornis	(Zetterstedt, 1838)
Ectemnius rugifer	(Dahlbom, 1845)
Ectemnius schlettereri	(Kohl, 1888)
Ectemnius spinipes	(Morawitz, 1866)
Ectinosoma abrau	(Kritschagin, 1877)
Ectocyclops phaleratus	(Koch, 18389
Ectoedemia agrimoniae	(Frey, 1858)
Ectoedemia albifasciella	(Heinemann, 1871)
Ectoedemia angulifasciella	(Stainton, 1849)
Ectoedemia arcuatella	(Herrich-Schäffer, 1855)
Ectoedemia argyropeza	(Zeller, 1839)
Ectoedemia atricollis	(Stainton, 1857)
Ectoedemia atrifrontella	(Stainton, 1851)
Ectoedemia caradjai	(Groschke, 1944)
Ectoedemia cerris	(Zimmermann, 1944)
Ectoedemia contorta	van Nieukerken, 1985
Ectoedemia decentella	(Herrich-Schäffer, 1855)
Ectoedemia gilvipennella	(Klimesch, 1946)
Ectoedemia hannoverella	(Glitz, 1872)
Ectoedemia heringi	(Toll, 1934)
Ectoedemia hexapetalae	(Szöcs, 1957)
Ectoedemia intimella	(Zeller, 1848)
Ectoedemia klimeschi	(Skala, 1933)
Ectoedemia liebwerdella	(Zimmermann, 1940)
Ectoedemia liechtensteini	(Zimmermann, 1944)
Ectoedemia longicaudella	Klimesch, 1953
Ectoedemia louisella	(Sircom, 1849)
Ectoedemia mahalebella	(Klimesch, 1936)
Ectoedemia occultella	(Linnaeus, 1767)
Ectoedemia preisseckeri	(Klimesch, 1941)
Ectoedemia rubivora	(Wocke, 1860)
Ectoedemia rufifrontella	(Caradja, 1920)
Ectoedemia septembrella	(Stainton, 1849)
Ectoedemia sericopeza	(Zeller, 1839)
Ectoedemia spinosella	(Joannis, 1908)
Ectoedemia spiraeae	Gregor & Povolny, 1983
Ectoedemia subbimaculella	(Haworth, 1828)
Ectoedemia turbidella	(Zeller, 1848)
Ectohomoeosoma kasyellum	Roesler, 1965
Ectropis crepuscularia	(Denis & Schiffermüller, 1775)
Edaphus beszedesi	Reitter, 1913
Edwardsiana ampliata	(Wagner, 1947)
Edwardsiana avellanae	(Edwards, 1888)
Edwardsiana candidula	(Kirschbaum, 1868)
Edwardsiana crataegi	(Douglas, 1876)
Edwardsiana diversa	(Edwards, 1914)
Edwardsiana flavescens	(Fabricius, 1794)
Edwardsiana geometrica	(Schrank, 1801)
Edwardsiana gratiosa	(Boheman, 1852)
Edwardsiana hippocastani	(Edwards, 1888)
Edwardsiana lamellaris	(Ribaut, 1931)
Edwardsiana lethierryi	(Edwards, 1881)
Edwardsiana plebeja	(Edwards, 1914)
Edwardsiana prunicola	(Edwards, 1914)
Edwardsiana rosae	(Linnaeus, 1758)
Edwardsiana ruthenica	Zachvatkin, 1929
Edwardsiana salicicola	(Edwards, 1885)
Edwardsiana spinigera	(Edwards, 1924)
Edwardsiana staminata	(Ribaut, 1931)
Edwardsiana stehliki	Lauterer, 1958
Egira conspicillaris	(Linnaeus, 1758)
Casmerodius albus	(Linnaeus, 1758)
Egretta garzetta	(Linnaeus, 1766)
Egretta gularis	(Bosc, 1792)
Eidophasia messingiella	(Fischer v. Röslerstamm, 1840)
Eidophasia syenitella	Herrich-Schäffer, 1854
Eilema caniola	(Hübner, 1808)
Eilema complana	(Linnaeus, 1758)
Eilema depressa	(Esper, 1787)
Eilema griseola	(Hübner, 1803)
Eilema lurideola	(Zincken, 1817)
Eilema lutarella	(Linnaeus, 1758)
Eilema palliatella	(Scopoli, 1763)
Eilema pseudocomplana	(Daniel, 1939)
Eilema pygmaeola	(Doubleday, 1847)
Eilema pygmaeola pallifrons	(Doubleday, 1847)
Eilema sororcula	(Hufnagel, 1766)
Eilicrinia cordiaria	(Hübner, 1790)
Eilicrinia trinotata	(Metzner, 1845)
Elachista adscitella	Stainton, 1851
Elachista albidella	Nylander, 1848
Elachista alpinella	Stainton, 1854
Elachista anserinella	Zeller, 1839
Elachista apicipunctella	Stainton, 1849
Elachista argentella	(Clerck, 1759)
Elachista atricomella	Stainton, 1849
Elachista bedellella	(Sircom, 1848)
Elachista biatomella	(Stainton, 1848)
Elachista bisulcella	(Duponchel, 1843)
Elachista canapennella	(Hübner, 1813)
Elachista chrysodesmella	Zeller, 1850
Elachista cingillella	(Herrich-Schäffer, 1855)
Elachista collitella	(Duponchel, 1843)
Elachista contaminatella	Zeller, 1847
Elachista disemiella	Zeller, 1847
Elachista dispilella	Zeller, 1839
Elachista dispunctella	(Duponchel, 1843)
Elachista elegans	Frey, 1859
Elachista gangabella	Zeller, 1850
Elachista gleichenella	(Fabricius, 1781)
Elachista gormella	Nielsen & Traugott-Olsen, 1987
Elachista griseella	(Duponchel, 1843)
Elachista hedemanni	Rebel, 1899
Elachista heringi	Rebel, 1899
Elachista herrichii	Frey, 1859
Elachista humilis	Zeller, 1850
Elachista juliensis	Frey, 1870
Elachista kilmunella	Stainton, 1849
Elachista klimeschiella	Parenti, 2002
Elachista luticomella	Zeller, 1839
Elachista martinii	O. Hofmann, 1898
Elachista monosemiella	Rössler, 1881
Elachista nitidulella	(Herrich-Schaffer,1855)
Elachista poae	Stainton, 1855
Elachista pollinariella	Zeller, 1839
Elachista pollutella	Duponchel, 1843
Elachista pomerana	Frey, 1870
Elachista pullicomella	Zeller, 1839
Elachista quadripunctella	(Hübner, 1825)
Elachista revinctella	Zeller, 1850
Elachista rudectella	Stainton, 1851
Elachista rufocinerea	(Haworth, 1828)
Elachista scirpi	Stainton, 1887
Elachista serricornis	Stainton, 1854
Elachista spumella	Caradja, 1920
Elachista squamosella	(Duponchel, 1843)
Elachista subalbidella	Schläger, 1847
Elachista subnigrella	Douglas, 1853
Elachista subocellea	(Stephens, 1834)
Elachista szocsi	Parenti, 1978
Elachista triatomea	(Haworth, 1828)
Elachista triseriatella	Stainton, 1854
Elachista unifasciella	(Haworth, 1828)
Elampus albipennis	Mocsáry, 1889
Elampus ambiguus	(Dahlbom, 1854)
Elampus bidens	(Förster, 1853)
Elampus constrictus	(Förster, 1853)
Elampus foveatus	Mocsáry, 1914
Elampus pyrosomus	(Förster, 1853)
Elampus sanzii	Gogorza, 1887
Elampus scutellaris	(Fabricius, 1798)
Elampus spinus	Lepeletier, 1806
Elaphe longissima	(Laurenti, 1768)
Elaphoidella elaphoides	(Chappuis, 1924)
Elaphoidella gracilis	(Sars, 1863)
Elaphoidella jeanneli	(Chappuis, 1928)
Elaphoidella pseudojeanneli	Ponyi, 1956
Elaphoidella pseudojeanneli aggtelekiensis	Ponyi, 1956
Elaphoidella simplex	Chappuis, 1944
Elaphoidella simplex szegedensis	Chappuis, 1944
Elaphria venustula	(Hübner, 1790)
Tachyura diabrachys	(Kolenati, 1845)
Sphaerotachys hoemorrhoidalis	(Ponza, 1805)
Tachyura parvula	(Dejean, 1831)
Tachyura quadrisignata	(Duftschmid, 1812)
Elaphrus aureus	Ph. Müller, 1821
Elaphrus cupreus	Duftschmid, 1812
Elaphrus riparius	(Linnaeus, 1758)
Elaphrus uliginosus	Fabricius, 1792
Elaphrus ullrichii	W. Redtenbacher, 1842
Elasmostethus interstinctus	(Linnaeus, 1758)
Elasmostethus minor	Horváth, 1899
Elasmotropis testacea	(Herrich-Schäffer, 1830)
Elasmucha ferrugata	(Fabricius, 1787)
Elasmucha fieberi	(Jakovlev, 1864)
Elasmucha grisea	(Linnaeus, 1758)
Elater ferrugineus	Linnaeus, 1758
Elatobia fuliginosella	(Lienig & Zeller, 1846)
Electrogena affinis	(Eaton, 1883)
Electrogena lateralis	(Curtis, 1834)
Electrogena ujhelyii	(Sowa, 1981)
Electrophaes corylata	(Thunberg, 1792)
Eledona agricola	(Herbst, 1783)
Eledonoprius armatus	(Panzer, 1799)
Elegia atrifasciella	Ragonot, 1887
Elegia fallax	(Staudinger, 1881)
Elegia similella	(Zincken, 1818)
Eliomys quercinus	(Linnaeus, 1766)
Ellescus bipunctatus	(Linnaeus, 1758)
Ellescus infirmus	(Herbst, 1795)
Ellescus scanicus	(Paykull, 1792)
Elmis aenea	(P.W.J. Müller, 1806)
Elmis latreillei	Bedel, 1878
Elmis maugetii	Latreille, 1798
Elmis obscura	(P.W.J. Müller, 1806)
Elmis rioloides	(Kuwert, 1890)
Elodes hausmanni	(Gredler, 1857)
Elodes johni	(Klausnitzer, 1975)
Elodes marginata	(Fabricius, 1798)
Elodes minuta	(Linnaeus, 1767)
Elophila nymphaeata	(Linnaeus, 1758)
Elophila rivulalis	(Duponchel, 1834)
Elymana sulphurella	(Zetterstedt, 1828)
Ematheudes punctella	(Treitschke, 1833)
Ematurga atomaria	(Linnaeus, 1758)
Emberiza cia	Linnaeus, 1766
Emberiza cirlus	Linnaeus, 1766
Emberiza citrinella	Linnaeus, 1758
Emberiza hortulana	Linnaeus, 1758
Emberiza leucocephalos	S. G. Gmelin, 1771
Emberiza melanocephala	Scopoli, 1769
Emberiza pusilla	Pallas, 1776
Emberiza schoeniclus	(Linnaeus, 1758)
Emberiza schoeniclus intermedia	(Linnaeus, 1758)
Emberiza schoeniclus stresemanni	(Linnaeus, 1758)
Emberiza schoeniclus tschusii	(Linnaeus, 1758)
Emberiza schoeniclus ukrainae	(Linnaeus, 1758)
Emblethis brachynotus	Horváth, 1897
Emblethis ciliatus	Horváth, 1875
Emblethis denticollis	Horváth, 1878
Emblethis griseus	(Wolff, 1802)
Emblethis verbasci	(Fabricius, 1803)
Emblyna brevidens	(Kulczynski, 1897)
Emblyna mitis	(Thorell, 1875)
Emelyanoviana mollicula	(Boheman, 1845)
Emmelia trabealis	(Scopoli, 1763)
Emmelina argoteles	(Meyrick, 1922)
Emmelina monodactyla	(Linnaeus, 1758)
Coptotriche angusticollella	(Duponchel, 1843)
Coptotriche gaunacella	(Duponchel, 1843)
Coptotriche heinemanni	(Wocke, 1871)
Coptotriche marginea	(Haworth, 1828)
Coptotriche szoecsi	(Kasy, 1961)
Empicoris baerensprungi	(Dohrn, 1863)
Empicoris culiciformis	(De Geer, 1773)
Empicoris gracilentus	(Jakovlev, 1907)
Empicoris mediterraneus	Hoberlandt, 1956
Empicoris tabellarius	Ribes & P. V. Putshkov
Empicoris vagabundus	(Linnaeus, 1758)
Empoasca affinis	Nast, 1937
Empoasca decipiens	Paoli, 1930
Empoasca pteridis	Dahlbom, 1850
Empoasca vitis	(Göthe, 1875)
Emus hirtus	(Linnaeus, 1758)
Emys orbicularis	(Linnaeus, 1758)
Ena montana	(Draparnaud, 1801)
Enallagma cyathigerum	(Charpentier, 1840)
Enalodroma hepatica	(Erichson, 1839)
Enantiocephalus cornutus	(Herrich-Schäffer, 1838)
Enantiulus nanus	(Latzel, 1884)
Enantiulus tatranus	(Verhoeff, 1907)
Enantiulus tatranus evae	(Verhoeff, 1907)
Enargia abluta	(Hübner, 1808)
Enargia paleacea	(Esper, 1788)
Enarmonia formosana	(Scopoli, 1763)
Encephalus complicans	Stephens, 1832
Endomia tenuicollis	(Rossi, 1790)
Endomychus coccineus	(Linnaeus, 1758)
Endophloeus marcovichianus	(Piller & Mitterpacher, 1783)
Endothenia gentianaeana	(Hübner, 1799)
Endothenia lapideana	(Herrich-Schäffer, 1851)
Endothenia marginana	(Haworth, 1811)
Endothenia nigricostana	(Haworth, 1811)
Endothenia oblongana	(Haworth, 1811)
Endothenia quadrimaculana	(Haworth, 1811)
Endothenia sororiana	(Herrich-Schäffer, 1850)
Endothenia ustulana	(Haworth, 1811)
Endotricha flammealis	(Denis & Schiffermüller, 1775)
Endromis versicolora	(Linnaeus, 1758)
Endrosis sarcitrella	(Linnaeus, 1758)
Enedreytes hilaris	Fahraeus, 1839
Enedreytes sepicola	(Fabricius, 1792)
Enicmus brevicornis	(Mannerheim, 1844)
Enicmus fungicola	C.G. Thomson, 1868
Enicmus histrio	Joy et Tomlin, 1910
Enicmus rugosus	(Herbst, 1793)
Enicmus testaceus	(Stephens, 1830)
Enicmus transversus	(Olivier, 1790)
Enicopus hirtus	(Linnaeus, 1767)
Ennearthron cornutum	(Gyllenhal, 1827)
Ennearthron pruinosulum	(Perris, 1864)
Ennomos alniaria	(Linnaeus, 1758)
Ennomos autumnaria	(Werneburg, 1859)
Ennomos erosaria	(Denis & Schiffermüller, 1775)
Ennomos fuscantaria	(Haworth, 1809)
Ennomos quercaria	(Hübner, 1813)
Ennomos quercinaria	(Hufnagel, 1767)
Enochrus affinis	(Thunberg, 1794)
Enochrus ater	Kuwert, 1888
Enochrus bicolor	(Fabricius, 1792)
Enochrus coarctatus	(Gredler, 1863)
Enochrus fuscipennis	(Thomson, 1884)
Enochrus halophilus	(Bedel, 1878)
Enochrus hamifer	(Ganglbauer, 1901)
Enochrus melanocephalus	(Olivier, 1792)
Enochrus ochropterus	(Marsham, 1802)
Enochrus quadripunctatus	(Herbst, 1797)
Enochrus testaceus	(Fabricius, 1801)
Enoplognatha latimana	Hippa et Oksala, 1982
Enoplognatha mandibularis	(Lucas, 1846)
Enoplognatha mordax	(Thorell, 1875)
Enoplognatha oelandica	(Thorell, 1875)
Enoplognatha ovata	(Clerck, 1757)
Enoplognatha serratosignata	(L. Koch, 1879)
Enoplognatha thoracica	(Hahn, 1833)
Enoplops scapha	(Fabricius, 1794)
Enoplopus dentipes	(Rossi, 1790)
Entelecara acuminata	(Wider, 1834)
Entelecara congenera	(O.P.-Cambridge, 1879)
Entelecara flavipes	(Blackwall, 1834)
Entelecara omissa	(O.P.-Cambridge, 1902)
Entephria caesiata	(Denis & Schiffermüller, 1775)
Entephria cyanata	(Hübner, 1809)
Enteucha acetosae	(Stainton,1854)
Entomobora crassitarsis	(Costa, 1887)
Entomognathus brevis	(Vander Linden, 1829)
Entomognathus dentifer	(Noskiewicz, 1929)
Entomosericus concinnus	Dahlbom, 1845
Entomosericus kaufmanni	Radoszkowski, 1877
Eobania vermiculata	(O.F. Müller, 1774)
Eoferreola manticata	(Pallas, 1771)
Eoferreola rhombica	(Christ, 1791)
Eohardya fraudulentus	(Horváth, 1903)
Eoleptestheria ticinensis	Balsamo-Crivelli, 1859
Eosolenobia manni	(Zeller, 1852)
Epacromius coerulipes	(Ivanov, 1887)
Epacromius tergestinus	(Charpentier, 1825)
Epagoge grotiana	(Fabricius, 1781)
Epaphius secalis	(Paykull, 1790)
Epascestria pustulalis	(Hübner, 1823)
Epauloecus unicolor	(Piller Et Mitterpacher, 1783)
Epeoloides coecutiens	(Fabricius, 1775)
Epeolus cruciger	(Panzer, 1799)
Epeolus fasciatus	Friese, 1895
Epeolus schummeli	Schilling, 1849
Epeolus variegatus	(Linnaeus, 1758)
Epeorus assimilis	(Eaton, 1871)
Epermenia aequidentellus	(O. Hofmann, 1867)
Epermenia chaerophyllella	(Goeze, 1783)
Epermenia illigerella	(Hübner, 1813)
Epermenia insecurellus	(Stainton, 1854)
Epermenia petrusellus	(Heylaerts, 1883)
Epermenia pontificella	(Hübner, 1796)
Epermenia strictellus	(Wocke, 1867)
Ephemera danica	Müller, 1764
Ephemera glaucops	Pictet, 1843
Ephemera lineata	Eaton, 1870
Ephemera vulgata	Linnaeus, 1758
Serratella ignita	(Poda, 1761)
Serratella mesoleuca	(Brauer, 1857)
Ephemerella mucronata	(Bengtsson, 1909)
Ephemerella notata	Eaton, 1887
Ephestia elutella	(Hübner, 1796)
Ephestia kuehniella	Zeller, 1879
Ephestia unicolorella woodiella	Staudinger, 1881
Ephestia welseriella	(Zeller, 1848)
Ephippiger ephippiger	(Fieber, 1784)
Ephistemus globulus	(Paykull, 1798)
Ephistemus reitteri	Casey, 1900
Ephoron virgo	(Olivier, 1791)
Ephysteris inustella	(Zeller, 1847)
Ephysteris promptella	(Staudinger, 1859)
Epibactra sareptana	(Herrich-Schäffer, 1861)
Epiblema cnicicolana	(Zeller, 1847)
Epiblema confusana	(Herrich-Schäffer, 1891)
Epiblema costipunctana	(Haworth, 1811)
Epiblema foenella	(Linnaeus, 1758)
Epiblema grandaevana	(Lienig & Zeller, 1846)
Epiblema graphana	(Treitschke, 1835)
Epiblema hepaticana	(Treitschke, 1835)
Epiblema junctana	(Herrich-Schäffer, 1856)
Epiblema mendiculana	(Treitschke, 1835)
Epiblema obscurana	(Herrich-Schäffer, 1851)
Epiblema scutulana	(Denis & Schiffermüller, 1775)
Epiblema similana	(Denis & Schiffermüller, 1775)
Epiblema sticticana	(Fabricius, 1794)
Epiblema turbidana	(Treitschke, 1835)
Epicallima bruandella	(Ragonot, 1889)
Epicallima formosella	(Denis & Schiffermüller, 1775)
Epicauta rufidorsum	(Goeze, 1777)
Epichnopterix kovacsi	Sieder, 1955
Epichnopterix plumella	(Denis & Schiffermüller, 1775)
Tropinota hirta	(Poda, 1761)
Epidiaspis leperii	(Signoret,1869)
Epierus comptus	Erichson, 1834
Epilecta linogrisea	([Denis & Schiffermüller], 1775)
Epimecia ustula	(Freyer, 1835)
Epimyrma ravouxi	(André, 1896)
Epinotia abbreviana	(Fabricius, 1794)
Epinotia bilunana	(Haworth, 1811)
Epinotia brunnichana	(Linnaeus, 1767)
Epinotia cruciana	(Linnaeus, 1761)
Epinotia demarniana	(Fischer v. Röslerstamm, 1840)
Epinotia festivana	(Hübner, 1799)
Epinotia fraternana	(Haworth, 1811)
Epinotia granitana	(Herrich-Schäffer, 1851)
Capricornia boisduvaliana	(Duponchel, 1836)
Epinotia kochiana	(Herrich-Schäffer, 1851)
Epinotia maculana	(Fabricius, 1775)
Epinotia nanana	(Treitschke, 1835)
Epinotia nigricana	(Herrich-Schäffer, 1851)
Epinotia nisella	(Clerck, 1759)
Epinotia pusillana	(Peyerimhoff, 1863)
Epinotia pygmaeana	(Hübner, 1799)
Epinotia ramella	(Linnaeus, 1758)
Epinotia rhomboidella	(Geofroy, 1785)
Epinotia rubiginosana	(Herrich-Schäffer, 1851)
Epinotia signatana	(Douglas, 1845)
Epinotia solandriana	(Linnaeus, 1758)
Epinotia sordidana	(Hübner, 1824)
Epinotia subocellana	(Donovan, 1806)
Epinotia tedella	(Clerck, 1759)
Epinotia tenerana	(Denis & Schiffermüller, 1775)
Epinotia tetraquetrana	(Haworth, 1811)
Epinotia thapsiana	(Zeller, 1847)
Epinotia trigonella	(Linnaeus, 1758)
Epione repandaria	(Hufnagel, 1767)
Epione vespertaria	(Linnaeus, 1767)
Epipsilia latens	(Hübner, 1809)
Epirranthis diversata	(Denis & Schiffermüller, 1775)
Epirrhoe alternata	(Müller, 1764)
Epirrhoe galiata	(Denis & Schiffermüller, 1775)
Epirrhoe hastulata	(Hübner, 1790)
Epirrhoe molluginata	(Hübner, 1813)
Epirrhoe pupillata	(Thunberg, 1788)
Epirrhoe rivata	(Hübner, 1813)
Epirrhoe tristata	(Linnaeus, 1758)
Epirrita autumnata	(Borkhausen, 1794)
Epirrita christyi	(Allen, 1906)
Epirrita dilutata	(Denis & Schiffermüller, 1775)
Epischnia prodromella	(Hübner, 1799)
Episcythrastis tetricella	(Denis & Schiffermüller, 1775)
Episema glaucina	(Esper, 1789)
Episema tersa	([Denis & Schiffermüller], 1775)
Episernus granulatus	Weise, 1887
Episinus angulatus	(Blackwall, 1836)
Episinus truncatus	Latreille, 1809
Epistrophe cryptica	Doczkal & Schmid, 1994
Epistrophe diaphana	(Zetterstedt, 1843)
Epistrophe eligans	(Harris, 1780)
Epistrophe flava	Doczkal & Schmid, 1994
Epistrophe grossulariae	(Meigen, 1822)
Epistrophe melanostoma	(Zetterstedt, 1843)
Epistrophe nitidicollis	(Meigen, 1822)
Epistrophe ochrostoma	(Zetterstedt, 1849)
Epistrophella euchroma	(Kowarz, 1855)
Episyron albonotatus	(Vander Linden, 1827)
Episyron arrogans	(Smith, 1873)
Episyron candiotus	Wachis, 1966
Episyron gallicus	(Tournier, 1890)
Episyron gallicus tertius	(Tournier, 1890)
Episyron rufipes	(Linnaeus, 1758)
Episyrphus balteatus	(De Geer, 1776)
Epitheca bimaculata	(Charpentier, 1825)
Epomis dejeanii	Dejean, 1831
Eptesicus nilssoni	(Keyserling et Blasius, 1839)
Eptesicus serotinus	(Schreber, 1774)
Epuraea aestiva	(Linnaeus, 1758)
Epuraea angustula	Sturm, 1844
Epuraea biguttata	(Thunberg, 1784)
Epuraea distincta	(Grimmer, 1841)
Epuraea fageticola	Audisio, 1991
Epuraea fuscicollis	(Stephens, 1832)
Epuraea guttata	(Olivier, 1811)
Epuraea limbata	(Fabricius, 1787)
Epuraea longula	Erichson, 1845
Epuraea marseuli	Reitter, 1872
Epuraea melanocephala	(Marsham, 1802)
Epuraea melina	Erichson, 1843
Epuraea neglecta	(Heer, 1841)
Epuraea pallescens	(Stephens, 1832)
Epuraea pygmaea	(Gyllenhal, 1808)
Epuraea rufomarginata	(Stephens, 1830)
Epuraea silacea	(Herbst, 1784)
Epuraea terminalis	Mannerheim, 1843
Epuraea unicolor	(Olivier, 1790)
Epuraea variegata	(Herbst, 1793)
Erannis ankeraria	(Staudinger, 1861)
Erannis defoliaria	(Clerck, 1759)
Erebia aethiops	(Esper, 1777)
Erebia ligea	(Linnaeus, 1758)
Erebia medusa	([Denis & Schiffermüller], 1775)
Eremobia ochroleuca	([Denis & Schiffermüller], 1775)
Eremobina pabulatricula	(Brahm, 1791)
Eremochlorita hungarica	(Ribaut, 1933)
Eremocoris abietis	(Linnaeus, 1758)
Eremocoris fenestratus	(Herrich-Schäffer, 1839)
Eremocoris plebejus	(Fallén, 1807)
Eremocoris podagricus	(Fabricius, 1775)
Eremodrina gilva	(Donzel, 1837)
Eremophila alpestris	(Linnaeus, 1758)
Eremophila alpestris flava	(Linnaeus, 1758)
Eresus kollari	Rossi, 1846
Eretes sticticus	(Linnaeus, 1767)
Ergasilus briani	Markewitsch, 1932
Ergasilus gibbus	Nordmann, 1832
Ergasilus nanus	Van Beneden,
Ergasilus sieboldi	Nordmann, 1832
Ergates faber	(Linnaeus, 1767)
Erichsonius cinerascens	(Gravenhorst, 1802)
Erichsonius signaticornis	(Mulsant & Rey, 1853)
Erichsonius subopacus	(Hochhuth, 1851)
Erigone atra	Blackwall, 1833
Erigone dentipalpis	(Wider, 1834)
Erigonella ignobilis	(O.P.-Cambridge, 1871)
Erigonoplus globipes	(L. Koch, 1872)
Erinaceus concolor	Martin, 1838
Erinaceus concolor roumanicus	Martin, 1838
Erinaceus europaeus	Linnaeus, 1758
Eriococcus buxi	(Fonscolombe,1834)
Eriocrania semipurpurella	(Stephens,1835)
Eriocrania sparrmannella	(Bosc, 1791)
Dyseriocrania subpurpurella	(Haworth, 1828)
Eriogaster catax	(Linnaeus, 1758)
Eriogaster lanestris	(Linnaeus, 1758)
Eriogaster rimicola	([Denis & Schiffermüller], 1775)
Eriopeltis festucae	(Fonscolombe,1834)
Eriopeltis lichtensteini	Signoret,1876
Eriopeltis stammeri	Schmutterer,1952
Eriopsela quadrana	(Hübner, 1813)
Eriozona syrphoides	(Fallén, 1817)
Eristalinus aeneus	(Scopoli, 1763)
Eristalinus sepulchralis	(Linnaeus, 1758)
Eristalis abusiva	Collin, 1931
Eristalis alpina	(Panzer, 1798)
Eristalis arbustorum	(Linnaeus, 1758)
Eristalis horticola	(De Geer, 1776)
Eristalis interrupta	(Poda, 1761)
Eristalis intricaria	(Linnaeus, 1758)
Eristalis jugorum	Egger, 1858
Eristalis pertinax	(Scopoli, 1763)
Eristalis rupium	(Fabricius, 1805)
Eristalis similis	Fallén, 1817
Eristalis tenax	(Linnaeus, 1758)
Eristalis vitripennis	(Strobl, 1893)
Erithacus rubecula	(Linnaeus, 1758)
Ernobius abietinus	(Gyllenhal, 1808)
Ernobius abietis	(Fabricius, 1792)
Ernobius angusticollis	(Ratzeburg, 1847)
Ernobius kiesenwetteri	Schilsky, 1899
Ernobius longicornis	(Sturm, 1837)
Ernobius mollis	(Linnaeus, 1758)
Ernobius nigrinus	(Sturm, 1837)
Ernobius pini	(Sturm, 1837)
Ernodes articularis	(Pictet, 1834)
Ernoporicus caucasicus	(Lindemann, 1876)
Ernoporicus fagi	(Fabricius, 1798)
Ernoporus tilliae	(Panzer, 1793)
Ero aphana	(Walckenaer, 1802)
Ero cambridgei	Kulczynski, 1911
Ero furcata	(Villers, 1789)
Ero tuberculata	(De Geer, 1778)
Erotesis baltica	McLachlan, 1877
Erotettix cyane	(Boheman, 1845)
Erpobdella nigricollis	(Brandes, 1900)
Erpobdella octoculata	(Linnaeus, 1758)
Erpobdella testacea	(Savigny, 1822)
Erpobdella vilnensis	(Liskiewich, 1925)
Errastunus notatifrons	(Kirschbaum, 1868)
Errastunus ocellaris	(Fallén, 1806)
Errhomenus brachypterus	Fieber, 1866
Erynnis tages	(Linnaeus, 1758)
Erythria aureola	(Fallén, 1806)
Erythria montandoni	(Puton, 1880)
Erythromma najas	(Hansemann, 1823)
Erythromma viridulum	Charpentier, 1840
Esolus angustatus	(P.W.J. Müller, 1821)
Esolus parallelepipedus	(P.W.J. Müller, 1806)
Esox lucius	Linnaeus, 1758
Dasycera krueperella	Staudinger, 1870
Dasycera oliviella	(Fabricius, 1794)
Fagotia daudebartii thermalis	(Prevost, 1821)
Fagotia daudebartii acicularis	(A. Ferussac, 1823)
Fagotia esperi	(A. Ferussac, 1823)
Esymus merdarius	(Fabricius, 1775)
Eteobalea albiapicella	(Duponchel, 1843)
Eteobalea anonymella	(Riedl, 1965)
Eteobalea beata	(Walsingham, 1907)
Eteobalea serratella	(Treitschke, 1833)
Eteobalea intermediella	(Riedl, 1966)
Eteobalea isabellella	(O. Costa, 1836)
Eteobalea tririvella	(Staudinger, 1871)
Ethelcus denticulatus	(Schrank, 1781)
Ethmia bipunctella	(Fabricius, 1775)
Ethmia candidella	(Alphéraky, 1908)
Ethmia dodecea	(Haworth, 1828)
Ethmia fumidella	(Wocke, 1850)
Ethmia haemorrhoidella	(Eversmann, 1844)
Ethmia iranella	Zerny, 1940
Ethmia pusiella	(Linnaeus, 1758)
Ethmia quadrillella	(Goeze, 1783)
Ethmia terminella	T. Fletcher, 1938
Etiella zinckenella	(Treitschke, 1832)
Euaesthetus bipunctatus	(Ljungh, 1804)
Euaesthetus laeviusculus	(Mannerheim, 1844)
Euaesthetus ruficapillus	(Lacordaire, 1835)
Euaesthetus superlatus	Peyerimhoff, 1937
Eublemma amoena	(Hübner, 1803)
Eublemma minutata	(Fabricius, 1794)
Eublemma ostrina	(Hübner, 1808)
Eublemma panonica	(Freyer, 1840)
Eublemma parva	(Hübner, 1808)
Eublemma purpurina	([Denis & Schiffermüller], 1775)
Eublemma rosea	(Hübner, 1790)
Eubrachium pusillum	(Rossi, 1792)
Eubranchipus grubei	(Dybowski, 1860)
Eubria palustris	Germar, 1818
Eubrychius velutus	(Beck, 1817)
Euspilapteryx auroguttella	Stephens, 1835
Eucarphia vinetella	(Fabricius, 1787)
Eucarta amethystina	(Hübner, 1803)
Eucarta virgo	(Treitschke, 1835)
Eucera caspica	Morawitz, 1873
Eucera caspica perezi	Morawitz, 1873
Eucera cinerea	Lepeletier, 1841
Eucera clypeata	Erichson, 1835
Eucera crysopyga	Pérez, 1879
Eucera curvitarsis	Mocsáry, 1879
Eucera dalmatica	Lepeletier, 1841
Eucera excisa	Mocsáry, 1879
Eucera iterrupta	Baer, 1850
Eucera longicornis	(Linnaeus, 1758)
Eucera nigrifacies	Lepeletier, 1841
Eucera nitidiventris	Mocsáry, 1879
Eucera parvicornis	Mocsáry, 1878
Eucera parvula	Friese, 1895
Eucera seminuda	Brullé, 1832
Eucera similis	Lepeletier, 1841
Eucera spectabilis	Mocsáry, 1879
Eucera taurica	Morawitz, 1870
Eucera tuberculata	(Fabricius, 1793)
Euchaetias egle	(Drury, 1773)
Euchalcia consona	(Fabricius, 1787)
Euchalcia modestoides	Poole, 1989
Euchalcia variabilis	(Piller, 1783)
Euchoeca nebulata	(Scopoli, 1763)
Euchorthippus declivus	(Brisout, 1848)
Euchorthippus pulvinatus	(Fischer-Waldheim, 1846)
Euchroeus purpuratus	(Fabricius, 1787)
Euchromius bella	(Hübner, 1796)
Euchromius ocellea	(Haworth, 1811)
Euchromius superbellus	(Zeller, 1849)
Eucinetus haemorrhoidalis	(Germar, 1818)
Euclidia glyphica	(Linnaeus, 1758)
Callistege mi	(Clerck, 1759)
Gonospileia triquetra	(Denis & Schiffermüller, 1775)
Eucnemis capucina	Ahrens, 1812
Eucoeliodes mirabilis	(A. Villa & G. B. Villa, 1835)
Euconnus chrysocomus	(Saulcy, 1864)
Euconnus claviger	(P.W.J. Müller et Kunze, 1822)
Euconnus denticornis	(P.W.J. Müller et Kunze, 1822)
Euconnus fimetarius	(Chaudoir, 1845)
Euconnus hirticollis	(Illiger, 1798)
Euconnus pragensis	Machulka, 1923
Euconnus pubicollis	(P.W.J. Müller et Kunze, 1822)
Euconnus rutilipennis	(P.W.J. Müller et Kunze, 1822)
Euconnus wetterhallii	(Gyllenhal, 1813)
Euconomelus lepidus	(Boheman, 1847)
Euconulus fulvus	(O.F. Müller, 1774)
Euconulus praticola	(Reinhardt, 1883)
Eucosma aemulana	(Schläger, 1849)
Eucosma albidulana	(Herrich-Schäffer, 1851)
Eucosma aspidiscana	(Hübner, 1817)
Eucosma balatonana	(Osthelder, 1937)
Eucosma campoliliana	(Denis & Schiffermüller, 1775)
Eucosma cana	(Haworth, 1811)
Eucosma conformana	(Mann, 1872)
Eucosma conterminana	(Guenée, 1845)
Eucosma cumulana	(Guenée, 1845)
Eucosma fervidana	(Zeller, 1847)
Eucosma hohenwartiana	(Denis & Schiffermüller, 1775)
Eucosma lacteana	(Treitschke, 1835)
Eucosma lugubrana	(Treitschke, 1830)
Eucosma messingiana	(Fischer v. Röslerstamm, 1837)
Eucosma metzneriana	(Treitschke, 1830)
Eucosma obumbratana	(Lienig & Zeller, 1846)
Eucosma pupillana	(Clerck, 1759)
Eucosma tripoliana	(Barrett, 1880)
Eucosma tundrana	(Kennel, 1900)
Eucosma wimmerana	(Treitschke, 1835)
Eucosmomorpha albersana	(Hübner, 1813)
Eucrostes indigenata	(Villers, 1789)
Eucyclops denticulatus	Graeter, 1903
Eucyclops macruroides	(Lilljeborg, 1901)
Eucyclops macrurus	(Sars, 1863)
Eucyclops serrulatus	(Fischer, 1851)
Eucyclops speratus	(Lilljeborg, 1901)
Eucypris ornata	O.F. Müller, 1776
Eucypris pigra	(Fischer, 1851)
Eucypris virens	(Jurine, 1820)
Eudarcia pagenstecherella	Hübner, 1825
Eudemis porphyrana	(Hübner, 1799)
Eudemis profundana	(Denis & Schiffermüller, 1775)
Eudiaptomus gracilis	(Sars, 1863)
Eudiaptomus graciloides	(Lilljeborg, 1888)
Eudiaptomus transsylvanicus	(Daday, 189)
Eudiaptomus vulgaris	(Schmeil, 1896)
Eudiaptomus zachariasi	(Poppe, 1886)
Eudiplister planulus	(Ménétries, 1848)
Eudonia laetella	(Zeller, 1846)
Eudonia mercurella	(Linnaeus, 1758)
Eudonia murana	(Curtis, 1827)
Eudonia sudetica	(Zeller, 1839)
Eudonia truncicolella	(Stainton, 1849)
Eudontomyzon danfordi	Regan, 1911
Eudontomyzon mariae	(Berg, 1931)
Euglenes oculatus	(Panzer, 1796)
Euglenes pygmaeus	(De Geer, 1774)
Eugnorisma depuncta	(Linnaeus, 1761)
Eugnosta lathoniana	(Hübner, 1800)
Eugnosta magnificana	(Rebel, 1914)
Eugraphe sigma	([Denis & Schiffermüller], 1775)
Euheptaulacus porcellus	(J. Frivaldszky, 1879)
Euheptaulacus sus	(Herbst, 1783)
Euheptaulacus villosus	(Gyllenhal, 1806)
Euhyponomeuta stannella	(Thunberg, 1794)
Euides speciosa	(Boheman, 1845)
Eulamprotes atrella	(Denis & Schiffermüller, 1775)
Eulamprotes superbella	(Zeller, 1839)
Eulamprotes unicolorella	(Duponchel, 1843)
Eulamprotes wilkella	(Linnaeus, 1758)
Eulecanium caraganae	Borchsenius,1953
Eulecanium ciliatum	(Douglas,1891)
Eulecanium douglasi	(Sulc,1895)
Eulecanium franconicum	(Lindinger,1912)
Eulecanium sericeum	(Lindinger,1912)
Eulecanium tiliae	(Linnaeus,1758)
Eulia ministrana	(Linnaeus, 1758)
Eulithis mellinata	(Fabricius, 1787)
Eulithis populata	(Linnaeus, 1758)
Eulithis prunata	(Linnaeus, 1758)
Eulithis pyraliata	(Denis & Schiffermüller, 1775)
Eulithis testata	(Linnaeus, 1761)
Eumannia lepraria	(Rebel, 1909)
Eumenes coarctatus	(Linnaeus, 1758)
Eumenes coronatus	(Panzer, 1799)
Eumenes dubius	Saussure, 1852
Eumenes lunulatus	Fabricius, 1804
Eumenes mediterraneus	Kriechbaumer, 1879
Eumenes papillarius	(Christ, 1791)
Eumenes pedunculatus	(Panzer, 1799)
Eumenes pomiformis	(Panzer, 1781)
Eumenes sareptanus	(André, 1889)
Eumenes sareptanus insolatus	(André, 1889)
Eumenes subpomiformis	Blüthgen, 1938
Eumerus flavitarsis	Zetterstedt, 1843
Eumerus grandis	Meigen, 1822
Eumerus hungaricus	Szilády, 1940
Eumerus longicornis	Loew, 1855
Eumerus ornatus	Meigen, 1822
Eumerus ovatus	Loew, 1848
Eumerus ruficornis	Meigen, 1822
Eumerus sabulonum	(Fallén, 1817)
Eumerus sinuatus	Loew, 1855
Eumerus sogdianus	Stackelberg, 1952
Eumerus strigatus	(Fallén, 1817)
Eumerus tarsalis	Loew, 1848
Eumerus tauricus	Stackelberg, 1952
Eumerus tricolor	(Fabricius, 1798)
Eumerus tuberculatus	Rondani, 1857
Euodynerus curictensis	Blüthgen, 1940
Euodynerus dantici	(Rossi, 1790)
Euodynerus egregius	(Herrich-Schaeffer, 1839)
Euodynerus egregius unimaculatus	(Herrich-Schaeffer, 1839)
Euodynerus notatus	(Jurine, 1807)
Euodynerus notatus pubescens	(Jurine, 1807)
Euodynerus posticus	(Herrich-Schaeffer, 1841)
Euodynerus quadrifasciatus	(Fabricius, 1793)
Euodynerus quadrifasciatus simplex	(Fabricius, 1793)
Euomphalia strigella	(Draparnaud, 1801)
Euoniticellus fulvus	(Goeze, 1777)
Euoniticellus pallipes	(Fabricius, 1781)
Euonthophagus amyntas	Olivier, 1789
Euonthophagus amyntas alces	Olivier, 1789
Euophrys frontalis	(Walckenaer, 1802)
Eupelix cuspidata	(Fabricius, 1775)
Eupeodes corollae	(Fabricius, 1794)
Eupeodes flaviceps	(Rondani, 1857)
Eupeodes lapponicus	(Zetterstedt, 1830)
Eupeodes latifasciatus	(Macquart, 1829)
Eupeodes latilunulatus	(Collin, 1931)
Eupeodes luniger	(Meigen, 1822)
Eupeodes nitens	(Zetterstedt, 1843)
Euphydryas aurinia	(Rottemburg, 1775)
Euphydryas maturna	(Linnaeus, 1758)
Euphyia biangulata	(Haworth, 1809)
Euphyia frustata	(Treitschke, 1828)
Euphyia scripturata	(Hübner, 1799)
Euphyia unangulata	(Haworth, 1809)
Eupithecia abbreviata	Stephens, 1831
Eupithecia abietaria	(Goeze, 1781)
Eupithecia absinthiata	(Clerck, 1759)
Eupithecia actaeata	Walderdorff, 1869
Eupithecia addictata	Dietze, 1908
Eupithecia alliaria	Staudinger, 1870
Eupithecia analoga	Djakonov, 1926
Eupithecia assimilata	Doubleday, 1856
Eupithecia breviculata	(Donzel, 1837)
Eupithecia catharinae	Vojnits, 1969
Eupithecia cauchiata	(Duponchel, 1831)
Eupithecia centaureata	(Denis & Schiffermüller, 1775)
Eupithecia denotata	(Hübner, 1813)
Eupithecia denticulata	(Treitschke, 1828)
Eupithecia distinctaria	Herrich-Schäffer, 1848
Eupithecia dodoneata	Guenée, 1857
Eupithecia egenaria	Herrich-Schäffer, 1848
Eupithecia ericeata	(Rambur, 1833)
Eupithecia exiguata	(Hübner, 1813)
Eupithecia expallidata	Doubleday, 1856
Eupithecia extraversaria	Herrich-Schäffer, 1852
Eupithecia extremata	(Fabricius, 1787)
Eupithecia gemellata	Herrich-Schäffer, 1861
Eupithecia graphata	(Treitschke, 1828)
Eupithecia gueneata	MilliŐre, 1862
Eupithecia haworthiata	Doubleday, 1856
Eupithecia icterata	(Villers, 1789)
Eupithecia immundata	(Lienig & Zeller, 1846)
Eupithecia impurata	(Hübner, 1813)
Eupithecia indigata	(Hübner, 1813)
Eupithecia innotata	(Hufnagel, 1767)
Eupithecia insigniata	(Hübner, 1790)
Eupithecia intricata	(Zetterstedt, 1839)
Eupithecia inturbata	(Hübner, 1817)
Eupithecia irriguata	(Hübner, 1813)
Eupithecia lanceata	(Hübner, 1825)
Eupithecia laquaearia	Herrich-Schäffer, 1848
Eupithecia lariciata	(Freyer, 1841)
Eupithecia linariata	(Denis & Schiffermüller, 1775)
Eupithecia millefoliata	Rössler, 1866
Eupithecia nanata	(Hübner, 1813)
Eupithecia ochridata	Schütze & Pinker, 1968
Eupithecia orphnata	W. Petersen, 1909
Eupithecia pauxillaria	Boisduval, 1840
Eupithecia pernotata	Guenée, 1858
Eupithecia pimpinellata	(Hübner, 1813)
Eupithecia plumbeolata	(Haworth, 1809)
Eupithecia pusillata	(Denis & Schiffermüller, 1775)
Eupithecia pygmaeata	(Hübner, 1799)
Eupithecia pyreneata	Mabille, 1871
Eupithecia satyrata	(Hübner, 1813)
Eupithecia schiefereri	Bohatsch, 1893
Eupithecia selinata	Herrich-Schäffer, 1861
Eupithecia semigraphata	Bruand, 1850
Eupithecia silenicolata	Mabille, 1867
Eupithecia simpliciata	(Haworth, 1809)
Eupithecia sinuosaria	(Eversmann, 1848)
Eupithecia spadiceata	Zerny, 1933
Eupithecia subfuscata	(Haworth, 1809)
Eupithecia subumbrata	(Denis & Schiffermüller, 1775)
Eupithecia succenturiata	(Linnaeus, 1758)
Eupithecia tantillaria	Boisduval, 1840
Eupithecia tenuiata	(Hübner, 1813)
Eupithecia tripunctaria	Herrich-Schäffer, 1852
Eupithecia trisignaria	Herrich-Schäffer, 1848
Eupithecia unedonata	(Mabille, 1868)
Eupithecia valerianata	(Hübner, 1813)
Eupithecia venosata	(Fabricius, 1787)
Eupithecia veratraria	Herrich-Schäffer, 1850
Eupithecia virgaureata	Doubleday, 1861
Eupithecia vulgata	(Haworth, 1809)
Euplagia quadripunctaria	(Poda, 1761)
Euplectus bescidicus	Reitter, 1881
Euplectus bonvouloiri	Reitter, 1882
Euplectus bonvouloiri rosae	Reitter, 1882
Euplectus brunneus	Grimmer, 1841
Euplectus decipiens	Raffray, 1910
Euplectus frater	Besuchet, 1964
Euplectus frivaldszkyi	Saulcy, 1878
Euplectus karstenii	(Reichenbach, 1816)
Euplectus kirbii	Denny, 1825
Euplectus nanus	(Reichenbach, 1816)
Euplectus piceus	Motschulsky, 1835
Euplectus punctatus	Mulsant, 1861
Euplectus sanguineus	Denny, 1825
Euplectus signatus	(Reichenbach, 1816)
Eupleurus subterraneus	(Linnaeus, 1758)
Euplexia lucipara	(Linnaeus, 1758)
Euplocamus anthracinalis	(Scopoli, 1763)
Eupoecilia ambiguella	(Hübner, 1796)
Eupoecilia angustana	(Hübner, 1799)
Eupoecilia sanguisorbana	(Herrich-Schäffer, 1856)
Eupolybothrus transsylvanicus	(Latzel, 1882)
Eupolybothrus tridentinus	(Fanzago, 1874)
Protaetia affinis	(Andersch, 1797)
Euproctis chrysorrhoea	(Linnaeus, 1758)
Euproctis similis	(Fuessly, 1775)
Eupsilia transversa	(Hufnagel, 1766)
Eupterycyba jucunda	(Herrich-Schäffer, 1837)
Eupteryx adspersa	(Herrich-Schäffer, 1838)
Eupteryx artemisiae	(Kirschbaum, 1868)
Eupteryx atropunctata	(Goeze, 1778)
Eupteryx aurata	(Linnaeus, 1758)
Eupteryx calcarata	Ossiannilsson, 1936
Eupteryx collina	(Flor, 1861)
Eupteryx cyclops	Matsumura, 1906
Eupteryx florida	Ribaut, 1936
Eupteryx heydenii	(Kirschbaum, 1868)
Eupteryx immaculatifrons	(Kirschbaum, 1868)
Eupteryx lelievrei	(Lethierry, 1874)
Eupteryx melissae	Curtis, 1837
Eupteryx notata	Curtis, 1837
Eupteryx stachydearum	(Hardy, 1850)
Eupteryx tenella	(Fallén, 1806)
Eupteryx thoulessi	Edwards, 1926
Eupteryx urticae	(Fabricius, 1803)
Eupteryx vittata	(Linnaeus, 1758)
Eurhadina concinna	(Germar, 1831)
Eurhadina kirschbaumi	Wagner, 1937
Eurhadina loewii	(Then, 1886)
Eurhadina pulchella	(Fallén, 1806)
Eurhodope cirrigerella	(Zincken, 1818)
Eurhodope rosella	(Scopoli, 1763)
Euripersia europaea	(Newstead, 1897)
Euripersia tomlini	(Newstead,1892)
Eurocoelotes inermis	(L. Koch, 1855)
Eurois occulta	(Linnaeus, 1758)
Euroleon nostras	(Geoffroy in Fourcroy, 1785)
Agonum antennarium	(Duftschmid, 1812)
Agonum fuliginosum	(Panzer, 1809)
Agonum gracile	Sturm, 1824
Agonum micans	(Nicolai, 1822)
Agonum piceum	(Linnaeus, 1758)
Agonum scitulum	Dejean, 1828
Agonum thoreyi	Dejean, 1828
Europiella albipennis	(Fallén, 1829)
Europiella alpina	(Reuter, 1875)
Europiella artemisiae	(Becker, 1864)
Europiella decolor	(Uhler, 1893)
Eurrhypara hortulata	(Linnaeus, 1758)
Eurrhypis pollinalis	(Denis & Schiffermüller, 1775)
Eurybregma nigrolineata	Scott, 1875
Eurycercus lamellatus	(O.F. Müller, 1785)
Eurycolpus flaveolus	(Stal, 1858)
Eurydema dominulus	(Scopoli, 1763)
Eurydema fieberi	(Schummel, 1836)
Eurydema oleracea	(Linnaeus, 1758)
Eurydema ornata	(Linnaeus, 1758)
Eurydema ventralis	Kolenati, 1846
Eurygaster austriaca	(Schrank, 1776)
Eurygaster dilaticollis	Dohrn, 1860
Eurygaster maura	(Linnaeus, 1758)
Eurygaster testudinaria	(Geoffroy, 1785)
Eurylophella karelica	Tiensuu, 1935
Euryopicoris nitidus	(Meyer-Dür, 1843)
Euryopis flavomaculata	(C.L. Koch, 1836)
Euryopis laeta	(Westring, 1862)
Euryopis quinqueguttata	Thorell, 1875
Euryopis saukea	Levi, 1951
Euryporus picipes	(Paykull, 1800)
Euryptilium gillmeisteri	Flach, 1889
Euryptilium saxonicum	(Gillmeister, 1845)
Eurysella brunnea	(Melichar, 1896)
Eurysa lineata	(Perris, 1857)
Eurysula lurida	(Fieber, 1866)
Eurytemora velox	(Lilljeborg, 1853)
Eurythyrea aurata	(Pallas, 1773)
Eurythyrea austriaca	(Linnaeus, 1767)
Eurythyrea quercus	(Herbst, 1780)
Euryusa brachelytra	Kiesenwetter, 1851
Euryusa castanoptera	Kraatz, 1856
Euryusa coarctata	Märkel, 1844
Euryusa optabilis	Heer, 1839
Euryusa sinuata	Erichson, 1837
Eysarcoris aeneus	(Scopoli, 1763)
Eysarcoris fabricii	Kirkaldy, 1904
Eysarcoris ventralis	(Westwood, 1837)
Euscelidius schenckii	(Kirschbaum, 1868)
Euscelidius variegatus	(Kirschbaum, 1858)
Euscelis distinguendus	(Kirschbaum, 1858)
Euscelis incisus	(Kirschbaum, 1858)
Euscelis lineolatus	Brullé, 1832
Euscelis venosus	(Kirschbaum, 1868)
Eusomus ovulum	Germar, 1824
Eusphalerum limbatum	(Erichson, 1840)
Eusphalerum longipenne	(Erichson, 1839)
Eusphalerum luteum	(Marsham, 1802)
Eusphalerum marshami	(Fauvel, 1869)
Eusphalerum minutum	(Fabricius, 1792)
Eusphalerum pallens	(Heer, 1841)
Eusphalerum primulae	(Stephens, 1834)
Eusphalerum rectangulum	(Fauvel, 1870)
Eusphalerum semicoleoptratum	(Panzer, 1794)
Eusphalerum signatum	(Märkel, 1857)
Eusphalerum sorbi	(Gyllenhal, 1810)
Eusphalerum tenenbaumi	(Bernhauer, 1932)
Euspilotus perrisi	(Marseul, 1872)
Eustenancistrocerus transitorius	(Morawitz, 1867)
Eustroma reticulata	(Denis & Schiffermüller, 1775)
Eustrophus dermestoides	(Fabricius, 1792)
Eutelia adulatrix	(Hübner, 1813)
Eutheia wetterhalli	Mulsant, 1861
Eutheia plicata	(Gyllenhal, 1813)
Eutheia scydmaenoides orientalis	Stephens, 1830
Euthrix potatoria	(Linnaeus, 1758)
Eutrichapion ervi	(Kirby, 1808)
Eutrichapion facetum	(Gyllenhal, 1839)
Eutrichapion gribodoi	(Desbrochers, 1896)
Eutrichapion melancholicum	(Wencker, 1864)
Eutrichapion punctiger	(Paykull, 1792)
Eutrichapion viciae	(Paykull, 1800)
Eutrichapion vorax	(Herbst, 1797)
Euxoa aquilina	([Denis & Schiffermüller], 1775)
Euxoa birivia	([Denis & Schiffermüller], 1775)
Euxoa conspicua	(Hübner, 1827)
Euxoa decora	([Denis & Schiffermüller], 1775)
Euxoa distinguenda	(Lederer, 1857)
Euxoa eruta	(Hübner, 1827)
Euxoa hastifera pomazensis	Donzel, 1848
Euxoa nigricans	(Linnaeus, 1761)
Euxoa nigrofusca	(Esper, 1788)
Euxoa obelisca	([Denis & Schiffermüller], 1775)
Euxoa recussa	(Hübner, 1817)
Euxoa segnilis	(Duponchel, 1836)
Euxoa temera	(Hübner, 1808)
Euxoa tritici	(Linnaeus, 1761)
Euxoa vitta	(Esper, 1789)
Euzonitis auricoma	(Escherich, 1891)
Euzonitis fulvipennis	(Fabricius, 1792)
Euzonitis quadrimaculata	(Pallas, 1782)
Euzonitis sexmaculata	(Olivier, 1790)
Euzophera bigella	(Zeller, 1848)
Euzophera cinerosella	(Zeller, 1839)
Euzophera fuliginosella	(Heinemann, 1865)
Euzophera pinguis	(Haworth, 1811)
Euzopherodes charlottae	(Rebel, 1914)
Euzopherodes vapidella	(Mann, 1857)
Evacanthus acuminatus	(Fabricius, 1794)
Evacanthus interruptus	(Linnaeus, 1758)
Evagetes alamannicus	(Blüthgen, 1944)
Evagetes crassicornis	(Shuckard, 1845)
Evagetes dubius	(Vander Linden, 1827)
Evagetes elongatus	(Lepeletier, 1845)
Evagetes gibbulus	(Lepeletier, 1845)
Evagetes littoralis	(Wesmael, 1851)
Evagetes pectinipes	(Linnaeus, 1758)
Evagetes pontomoravicus	(Sustera, 1938)
Evagetes proximus	(Dahlbom, 1843)
Evagetes sahlbergi	(Morawitz, 1893)
Evagetes siculus	(Lepeletier, 1845)
Evagetes subglaber	(Haupt, 1941)
Evagetes subnudus	(Haupt, 1941)
Evagetes tumidosus	(Tournier,1890)
Evarcha arcuata	(Clerck, 1757)
Evarcha falcata	(Clerck, 1757)
Evarcha laetabunda	(C.L. Koch, 1846)
Evergestis aenealis	(Denis & Schiffermüller, 1775)
Evergestis extimalis	(Scopoli, 1763)
Evergestis forficalis	(Linnaeus, 1758)
Evergestis frumentalis	(Linnaeus, 1761)
Evergestis limbata	(Linnaeus, 1767)
Evergestis pallidata	(Hufnagel, 1767)
Evergestis politalis	(Denis & Schiffermüller, 1775)
Evodinus clathratus	(Fabricius, 1792)
Exaeretia culcitella	(Herrich-Schäffer, 1854)
Exaeretia preisseckeri	(Rebel, 1937)
Exaesiopus grossipes	(Marseul, 1855)
Exapion compactum	(Desbrochers, 1888)
Exapion corniculatum	(Germar, 1817)
Exapion difficile	(Herbst, 1797)
Exapion elongatulum	(Desbrochers, 1891)
Exapion formaneki	(Wagner, 1929)
Exapion fuscirostre	(Fabricius, 1775)
Exitianus capicola	(Stal, 1855)
Exitianus taeniaticeps	(Kirschbaum, 1868)
Exocentrus adspersus	Mulsant, 1846
Exocentrus lusitanus	(Linnaeus, 1767)
Exocentrus punctipennis	Mulsant et Guillebeau, 1856
Exocentrus stierlini	Ganglbauer, 1883
Exochomus nigromaculatus	(Goeze, 1777)
Exoteleia dodecella	(Linnaeus, 1758)
Fabaeformis wegelini	(Petrovski, 1962)
Fabaeformiscandona acuminata	(Fischer, 1851)
Fabaeformiscandona brevicornis	(Klie, 1925)
Fabaeformiscandona fabaeformis	(Fischer, 1851)
Fabaeformiscandona fragilis	(Hartwig, 1898)
Fabaeformiscandona holzkampfi	Hartwig, 1900
Fabaeformiscandona protzi	(Hartwig, 1898)
Fabiola pokornyi	(Nickerl, 1864)
Fagivorina arenaria	(Hufnagel, 1767)
Fagocyba cruenta	(Herrich-Schäffer, 1838)
Fagocyba douglasi	(Edwards, 1878)
Fagocyba inquinata	(Ribaut, 1936)
Falagria caesa	Erichson, 1837
Falagria splendens	Kraatz, 1858
Falagria sulcatula	(Gravenhorst, 1806)
Falagrioma thoracica	(Stephens, 1832)
Falcaria lacertinaria	(Linnaeus, 1758)
Falco cherrug	Gray, 1834
Falco cherrug cyanopus	Gray, 1834
Falco columbarius	Linnaeus, 1758
Falco columbarius aesalon	Linnaeus, 1758
Falco eleonorae	Gené, 1839
Falco naumanni	Fleischer, 1818
Falco peregrinus	Tunstall, 1771
Falco peregrinus calidus	Tunstall, 1771
Falco subbuteo	Linnaeus, 1758
Falco tinnunculus	Linnaeus, 1758
Falco vespertinus	Linnaeus, 1776
Falcotoya minuscula	(Horváth, 1897)
Falseuncaria degreyana	(McLachlan, 1869)
Falseuncaria ruficiliana	(Haworth, 1811)
Farsus dubius	(Piller et Mitterpacher, 1783)
Faustina faustina	(Rossmässler, 1835)
Faustina illyrica	(Stabile, 1864)
Felis silvestris	Schreber, 1775
Ferdinandea cuprea	(Scopoli, 1763)
Ferdinandea ruficornis	(Fabricius, 1775)
Ferreola diffinis	(Lepeletier, 1845)
Ferreroaspis hungarica	(Vinis, 1981)
Ferrissia clessiniana	(Jickeli, 1882)
Ficedula albicollis	(Temminck, 1815)
Ficedula hypoleuca	(Pallas, 1764)
Ficedula parva	(Bechstein, 1794)
Fieberiella florii	(Stal, 1864)
Filatima spurcella	(Duponchel, 1843)
Filatima tephritidella	(Duponchel, 1844)
Flavohelodes flavicollis	(Kiesenwetter, 1863)
Florodelphax leptosoma	(Flor, 1861)
Floronia bucculenta	(Clerck, 1757)
Forcipata citrinella	(Zetterstedt, 1828)
Forcipata forcipata	(Flor, 1861)
Formica cinerea	Mayr, 1853
Formica cunicularia	Latreille, 1798
Formica exsecta	(Nylander, 1846)
Formica fusca	Linnaeus, 1758
Formica gagates	Latreille, 1798
Formica pratensis	Retzius, 1783
Formica pressilabris	Nylander, 1846
Formica rufa	Linnaeus, 1758
Formica rufibarbis	Fabricius, 1794
Formica sanguinea	Latreille, 1798
Formica truncorum	Fabricius, 1804
Formicoxenus nitidulus	(Nylander, 1846)
Formiphantes lepthyphantiformis	(Strand, 1907)
Foucartia liturata	(Stierlin, 1884)
Foucartia ptochioides	(Bach, 1856)
Fratercula arctica	(Linnaeus, 1758)
Fratercula arctica grabae	(Linnaeus, 1758)
Friedlanderia cicatricella	(Hübner, 1824)
Fringilla coelebs	Linnaeus, 1758
Fringilla montifringilla	Linnaeus, 1758
Frontinellina frutetorum	(C.L. Koch, 1834)
Fruticicola fruticum	(O.F. Müller, 1774)
Fulica atra	Linnaeus, 1758
Fulvoclysia nerminae	Koçak, 1982
Furcula bicuspis	(Borkhausen, 1790)
Furcula bifida	(Brahm, 1787)
Furcula furcula	(Clerck, 1759)
Gabrius appendiculatus	Sharp, 1910
Gabrius astutus	(Erichson, 1840)
Gabrius breviventer	(Sperk, 1835)
Gabrius exiguus	(Nordmann, 1837)
Gabrius exspectatus	Smetana, 1952
Gabrius femoralis	(Hochhuth, 1851)
Gabrius lividipes	(Baudi, 1848)
Gabrius nigritulus	(Gravenhorst, 1802)
Gabrius osseticus	(Kolenati, 1846)
Gabrius piliger	(Mulsant et Rey, 1876)
Gabrius ravasinii	Gridelli, 1920
Gabrius splendidulus	(Gravenhorst, 1802
Gabrius suffragani	Joy, 1913
Gabrius toxotes	Joy, 1913
Gabrius trossulus	(Nordmann, 1837)
Gabronthus limbatus	(Fauvel, 1900)
Gabronthus thermarum	(Aubé, 1850)
Gagitodes sagittata	(Fabricius, 1787)
Galba truncatula	(O.F. Müller, 1774)
Galeatus affinis	(Herrich-Schäffer, 1835)
Galeatus decorus	Jakovlev, 1880
Galeatus maculatus	(Herrich-Schäffer, 1839)
Galeatus sinuatus	(Herrich-Schäffer, 1839)
Galerida cristata	(Linnaeus, 1758)
Galerida cristata tenuirostris	(Linnaeus, 1758)
Galleria mellonella	(Linnaeus, 1758)
Gallinago gallinago	(Linnaeus, 1758)
Gallinago media	(Latham, 1787)
Gallinula chloropus	(Linnaeus, 1758)
Gambusia affinis	(Baird et Girard, 1853)
Gambusia holbrooki	Girard, 1859
Gampsocleis glabra	(Herbst, 1786)
Gampsocoris culicinus	Seidenstücker, 1948
Gampsocoris punctipes	(Germar, 1822)
Gargara genistae	(Fabricius, 1775)
Garrulus glandarius	(Linnaeus, 1758)
Garrulus glandarius albipectus	(Linnaeus, 1758)
Gasterocercus depressirostris	(Fabricius, 1792)
Gasterosteus aculeatus	(Linnaeus, 1758)
Gastrallus immarginatus	(P.W.J. Müller, 1821)
Tituboea macropus	(Illiger, 1800)
Gastrallus laevigatus	(Olivier, 1790)
Gastrodes abietum	Bergroth, 1914
Gastrodes grossipes	(De Geer, 1773)
Gastropacha populifolia	([Denis & Schiffermüller], 1775)
Gastropacha quercifolia	(Linnaeus, 1758)
Gauropterus fulgidus	(Fabricius, 1787)
Gaurotes virginea	(Linnaeus, 1758)
Gavia arctica	(Linnaeus, 1758)
Gavia immer	(Brünnich, 1764)
Gavia stellata	(Pontoppidan, 1763)
Geina didactyla	(Linnaeus, 1758)
Gelechia asinella	(Hübner, 1796)
Gelechia basipunctella	Herrich-Schäffer, 1854
Gelechia muscosella	Zeller, 1839
Gelechia nigra	(Haworth, 1828)
Gelechia rhombella	(Denis & Schiffermüller, 1775)
Gelechia rhombelliformis	Staudinger, 1871
Gelechia sabinellus	(Zeller, 1839)
Gelechia scotinella	Herrich-Schäffer, 1854,
Gelechia senticetella	(Staudinger, 1859)
Gelechia sestertiella	Herrich-Schäffer, 1854
Gelechia sororculella	(Hübner, 1817)
Gelechia turpella	(Denis & Schiffermüller, 1775)
Gelochelidon nilotica	(Gmelin, 1789)
Geocoris ater	(Fabricius, 1787)
Geocoris dispar	(Waga, 1839)
Geocoris erythrocephalus	(Lepeletier & Serville, 1825)
Geocoris grylloides	(Linnaeus, 1758)
Geocoris megacephalus	(Rossi, 1790)
Geodromicus plagiatus	(Heer, 1839)
Geolycosa vultuosa	(C.L. Koch, 1838)
Geometra papilionaria	(Linnaeus, 1758)
Geophilus carpophagus	Leach, 1814
Geophilus electricus	(Linnaeus, 1758)
Geophilus flavus	(De Geer, 1778)
Harmonia axyridis	(Pallas, 1773)
Geophilus insculptus	Attems, 1895
Geophilus linearis	C.L.Koch, 1835
Geophilus proximus	C.L.Koch, 1847
Georissus crenulatus	(Rossi, 1794)
Georissus laesicollis	Germar, 1831
Geostiba chyzeri	(Biró, 1883)
Geostiba circellaris	(Gravenhorst, 1806)
Geostiba gyorffyi	(Bernhauer, 1929)
Geotomus elongatus	(Herrich-Schäffer, 1839)
Geotomus punctulatus	(Costa, 1847)
Geotrupes mutator	(Marsham, 1802)
Geotrupes spiniger	(Marsham, 1802)
Geotrupes stercorarius	(Linnaeus, 1758)
Gerris argentatus	Schummel, 1832
Gerris asper	(Fieber, 1861)
Gerris gibbifer	Schummel, 1832
Gerris lacustris	(Linnaeus, 1758)
Gerris odontogaster	(Zetterstedt, 1828)
Gerris thoracicus	Schummel, 1832
Gesneria centuriella	(Denis & Schiffermüller, 1775)
Gibbaranea bituberculata strandiana	(Walckenaer, 1802)
Gibbaranea gibbosa	(Walckenaer, 1802)
Gibbaranea omoeda	(Thorell, 1870)
Gibbaranea ullrichi	(Hahn, 1835)
Gibberifera simplana	(Fischer v. Röslerstamm, 1836)
Gibbium psylloides	(Czempinski, 1778)
Glaphyra kiesenwetteri	(Mulsant et Rey, 1861)
Glaphyra schmidti	(Ganglbauer, 1883)
Glaphyra umbellatarum	(Schreber, 1759)
Glareola nordmanni	Fischer in Nordmann, 1842
Glareola pratincola	(Linnaeus, 1766)
Glaresis rufa	Erichson, 1848
Glaucidium passerinum	(Linnaeus, 1758)
Glaucopsyche alexis	(Poda, 1761)
Glis glis	(Linnaeus, 1766)
Glischrochilus hortensis	(Fourcroy, 1775)
Glischrochilus quadriguttatus	(Fabricius, 1776)
Glischrochilus quadripunctatus	(Linnaeus, 1758)
Glischrochilus quadrisignatus	(Say, 1835)
Globiceps flavomaculatus	(Fabricius, 1794)
Globiceps fulvicollis	Jakovlev, 1877
Globiceps horvathi	Reuter, 1912
Globiceps sordidus	Reuter, 1877
Globiceps sphaegiformis	(Rossi, 1790)
Globicornis corticalis	(Eichhoff, 1863)
Globicornis emarginata	(Gyllenhal, 1808)
Globicornis nigripes	(Fabricius, 1792)
Glocianus distinctus	(Ch. Brisout, 1869)
Glocianus fennicus	(Faust, 1894)
Glocianus incisus	(Schultze, 1899)
Glocianus inhumeralis	(Schultze, 1896)
Glocianus lethierryi	(Ch. Brisout, 1866)
Glocianus moelleri	(Thomson, 1868)
Glocianus pilosellus	(Gyllenhal, 1837)
Glocianus punctiger	(Gyllenhal, 1837)
Glocianus sparsutus	Schultze, 1899
Glomeridella minima	(Latzel, 1884)
Glomeris connexa	C.L.Koch, 1847
Glomeris conspersa	C.L.Koch, 1847
Glomeris hexasticha	Brandt, 1833
Glomeris ornata	C.L.Koch, 1847
Glomeris pustulata	Fabricius, 1781
Glossiphonia complanata	(Linnaeus, 1758)
Glossiphonia concolor	(Apáthy, 1888)
Glossiphonia nebulosa	Kalbe, 1964
Glossiphonia paludosa	(Carena, 1824)
Glossiphonia verrucata	(Fr. Müller, 1844)
Glossocratus foveolatus	Fieber, 1866
Glossocratus kuthyi	(Tóth, 1938)
Glossosoma boltoni	Curtis, 1834
Glossosoma conforme	Neboiss, 1963
Gluphisia crenata	(Esper, 1785)
Glyphesis servulus	(Simon, 1881)
Glyphesis taoplesius	Wunderlich, 1969
Glyphipterix bergstraesserella	(Fabricius, 1781)
Glyphipterix equitella	(Scopoli, 1763)
Glyphipterix forsterella	(Fabricius, 1781)
Glyphipterix haworthana	(Stephens, 1834)
Glyphipterix loricatella	(Treitschke, 1833)
Glyphipterix nattani	Gozmány, 1954
Glyphipterix pygmaeella	Rebel, 1896
Glyphipterix simpliciella	(Stephens, 1834)
Glyphipterix thrasonella	(Scopoli, 1763)
Glyphotaelius pellucidus	(Retzius, 1783)
Chorthippus apricarius	(Linnaeus, 1758)
Chorthippus biguttulus	(Linnaeus, 1758)
Chorthippus brunneus	(Thunberg, 1815)
Chorthippus mollis	(Charpentier, 1825)
Chorthippus vagans	(Eversmann, 1848)
Glyptoteles leucacrinella	Zeller, 1848
Gnaphosa alpica	Simon, 1878
Gnaphosa bicolor	(Hahn, 1833)
Gnaphosa lucifuga	(Walckenaer, 1802)
Gnaphosa lugubris	(C.L. Koch, 1839)
Gnaphosa microps	(Holm, 1939)
Gnaphosa modestior	Kulczynski, 1897
Gnaphosa mongolica	Simon, 1895
Gnaphosa opaca	Herman, 1879
Gnaptor spinimanus	(Pallas, 1781)
Gnathocerus cornutus	(Fabricius, 1798)
Gnathonarium dentatum	(Wider, 1834)
Gnathoncus buyssoni	Auzat, 1917
Gnathoncus nannetensis	(Marseul, 1862)
Gnathoncus communis	(Scriba, 1790)
Gnathoncus schmidti	Reitter, 1894
Gnathoncus disjunctus suturifer	Solskiy, 1876
Gnophos furvata	(Denis & Schiffermüller, 1775)
Gnorimoschema antiquum	Povolny, 1966
Gnorimoschema herbichii	(Nowicki, 1864)
Gnorimus nobilis	(Linnaeus, 1758)
Gnorimus variabilis	(Linnaeus, 1758)
Gnypeta carbonaria	(Mannerheim, 1830)
Gnypeta ripicola	(Kiesenwetter, 1844)
Gnypeta rubrior	Tottenham, 1939
Romanogobio vladykovi	(Fang, 1943)
Gobio gobio	(Linnaeus, 1758)
Romanogobio kesslerii	(Dybowski, 1862)
Romanogobio uranoscopus	(Agassiz, 1828)
Goera pilosa	(Fabricius, 1775)
Gomphocerippus rufus	(Linnaeus, 1758)
Gomphus flavipes	(Charpentier, 1825)
Gomphus vulgatissimus	(Linné, 1758)
Gonatium hilare	(Thorell, 1875)
Gonatium paradoxum	(L. Koch, 1869
Gonatium rubellum	(Blackwall, 1841)
Gonatium rubens	(Blackwall, 1833)
Gonepteryx rhamni	(Linnaeus, 1758)
Gongylidiellum latebricola	(O.P.-Cambridge, 1871)
Gongylidiellum murcidum	Simon, 1884
Gongylidium rufipes	(Linnaeus, 1758)
Goniagnathus brevis	(Herrich-Schäffer, 1835)
Goniagnathus guttulinervis	(Kirschbaum, 1868)
Gonianotus marginepunctatus	(Wolff, 1804)
Goniodoma auroguttella	(Fischer v. Röslerstamm, 1841)
Gonocephalum granulatum pusillum	(Fabricius, 1791)
Gonocephalum pygmaeum	(Steven, 1829)
Gonocerus acuteangulatus	(Goeze, 1778)
Gonocerus juniperi	(Herrich-Schäffer, 1839)
Gonodera luperus	(Herbst, 1783)
Gortyna borelii	(Pierret, 1837)
Gortyna borelii lunata	(Pierret, 1837)
Gortyna flavago	([Denis & Schiffermüller], 1775)
Gorytes albidulus	(Lepeletier, 1832)
Gorytes fallax	Handlirsch, 1888
Gorytes laticinctus	(Lepeletier, 1832)
Gorytes nigrifacies	(Mocsáry, 1879)
Gorytes planifrons	(Wesmael, 1852)
Gorytes pleuripunctatus	(Costa, 1859)
Gorytes procrustes	Handlirsch, 1888
Gorytes quadrifasciatus	(Fabricius, 1804)
Gorytes quinquecinctus	(Fabricius, 1793)
Gorytes quinquefasciatus	(Panzer, 1798)
Gorytes sulcifrons	(Costa, 1869)
Gossyparia spuria	(Modeer,1761)
Gracilia minuta	(Fabricius, 1781)
Graciliaria inserta	(A. & J.B. Villa, 1841)
Gracillaria loriolella	Frey, 1881
Gracillaria syringella	(Fabricius, 1794)
Grammoptera abdominalis	(Stephens, 1831)
Grammoptera ruficornis	(Fabricius, 1781)
Grammoptera ustulata	(Schaller, 1783)
Grammotaulius nigropunctatus	(Retzius, 1783)
Grammotaulius nitidus	(Müller, 1764)
Granaria frumentum	(Draparnaud, 1801)
Graphiphora augur	(Fabricius, 1775)
Graphocraerus ventralis	(Fallén, 1806)
Graphoderus austriacus	(Sturm, 1834)
Graphoderus bilineatus	(De Geer, 1774)
Graphoderus cinereus	(Linnaeus, 1758)
Graphoderus zonatus	(Hoppe, 1795)
Grapholita caecana	(Schläger, 1847)
Grapholita compositella	(Fabricius, 1775)
Grapholita coronillana	(Lienig & Zeller, 1846)
Grapholita delineana	(Walker, 1863)
Grapholita difficilana	(Walsingham, 1900)
Grapholita discretana	(Wocke, 1861)
Grapholita fissana	(Frölich, 1828)
Grapholita gemmiferana	(Treitschke, 1835)
Grapholita jungiella	(Linnaeus, 1761)
Grapholita larseni	(Rebel, 1903)
Grapholita lathyrana	(Hübner, 1822)
Grapholita lunulana	(Denis & Schiffermüller, 1775)
Grapholita nebritana	(Treitschke, 1830)
Grapholita orobana	(Treitschke, 1830)
Grapholita pallifrontana	(Lienig & Zeller, 1846)
Graphosoma lineatum	(Linnaeus, 1758)
Graptodytes bilineatus	(Sturm, 1835)
Graptodytes granularis	(Linnaeus, 1767)
Graptodytes pictus	(Fabricius, 1787)
Graptoleberis testudinaria	(Fischer, 1848)
Graptopeltus lynceus	(Fabricius, 1775)
Graptopeltus validus	(Horváth, 1875)
Graptus kaufmanni	Stierlin, 1884
Graptus triguttatus	(Fabricius, 1775)
Gravesteiniella boldi	(Scott, 1870)
Gravitarmata margarotana	(Heinemann, 1863)
Greenisca brachypodii	Borchsenius et Danzig,1966
Greenisca gouxi	(Balachowsky,1954)
Greenisca placida	(Green,1921)
Gronops lunatus	(Fabricius, 1775)
Grus grus	(Linnaeus, 1758)
Gryllotalpa gryllotalpa	(Linne, 1758)
Gryllus campestris	Linne, 1758
Grynocharis oblonga	(Linnaeus, 1758)
Grypocoris sexguttatus	(Fabricius, 1776)
Tetartostylus illyricus	(Kirschbaum, 1868)
Grypotes puncticollis	(Herrich-Schäffer, 1834)
Grypus brunnirostris	(Fabricius, 1792)
Grypus equiseti	(Fabricius, 1775)
Gymnancyla canella	(Denis & Schiffermüller, 1775)
Gymnancyla hornigi	(Lederer, 1852)
Gymnetron aper	Desbrochers, 1893
Gymnetron beccabungae	(Linnaeus, 1761)
Gymnetron furcatum	Desbrochers, 1893
Gymnetron melanarium	(Germar, 1821)
Gymnetron rostellum	(Herbst, 1795)
Gymnetron stimulosum	(Germar, 1821)
Gymnetron veronicae	(Germar, 1821)
Gymnetron villosulum	Gyllenhal, 1838
Gymnocephalus baloni	Holčík et Hensel, 1974
Gymnocephalus cernuus	(Linnaeus, 1758)
Gymnocephalus schraetser	(Linnaeus, 1758)
Gymnomerus laevipes	(Shuckard, 1837)
Gymnopleurus geoffroyi	(Fuesslin, 1775)
Gymnopleurus mopsus	(Pallas, 1781)
Gymnoscelis rufifasciata	(Haworth, 1809)
Gymnusa brevicollis	(Paykull, 1800)
Gynandromorphus etruscus	(Quensel, 1806)
Gynnidomorpha luridana	(Gregson, 1870)
Gynnidomorpha minimana	Caradja, 1912
Gynnidomorpha permixtana	(Denis & Schiffermüller, 1775)
Gynnidomorpha vectisana	(Humphreys & Westwood, 1845)
Gyps fulvus	(Hablizl, 1783)
Gypsonoma aceriana	(Duponchel, 1843)
Gypsonoma dealbana	(Frölich, 1828)
Gypsonoma minutana	(Hübner, 1799)
Gypsonoma nitidulana	(Lienig & Zeller, 1846)
Gypsonoma oppressana	(Treitschke, 1835)
Gypsonoma sociana	(Haworth, 1811)
Gyraulus albus	(O.F. Müller, 1774)
Gyraulus crista	(Linnaeus, 1758)
Gyraulus laevis	(Alder, 1838)
Gyraulus parvus	(Say, 1817)
Gyraulus riparius	(Westerlund, 1865)
Gyraulus rossmaessleri	(Auerswald, 1852)
Gyrinus colymbus	Erichson, 1837
Gyrinus distinctus	Aubé, 1838
Gyrinus marinus	Gyllenhal, 1808
Gyrinus minutus	Fabricius, 1798
Gyrinus paykulli	Ochs, 1927
Gyrinus substriatus	Stephens, 1829
Gyrinus suffriani	W. Scriba, 1855
Gyrohypnus angustatus	(Stephens, 1833)
Gyrohypnus atratus	(Heer, 1839)
Gyrohypnus fracticornis	(O.F.Müller, 1776)
Gyrohypnus punctulatus	(Paykull, 1789)
Gyrophaena affinis	Mannerheim, 1830
Gyrophaena bihamata	Thomson, 1867
Gyrophaena boleti	(Linnaeus, 1758)
Gyrophaena fasciata	(Marsham, 1802)
Gyrophaena gentilis	Erichson, 1839
Gyrophaena joyi	Wendeler, 1924
Gyrophaena joyioides	Wüsthoff, 1937
Gyrophaena lucidula	Erichson, 1837
Gyrophaena manca	Erichson, 1839
Gyrophaena minima	Erichson, 1837
Gyrophaena nana	(Paykull, 1800)
Gyrophaena nitidula	(Gyllenhal, 1810)
Gyrophaena polita	(Gravenhorst, 1802)
Gyrophaena poweri	Crotch, 1866
Gyrophaena pulchella	Heer, 1839
Gyrophaena rugipennis	Mulsant et Rey, 1861
Gyrophaena strictula	Erichson, 1839
Gyrophaena transversalis	A.Strand, 1939
Gyrophaena williamsi	A.Strand, 1935
Haasea flavescens	(Latzel, 1884)
Haasea hungarica	(Verhoeff, 1928)
Habrocerus capillaricornis	(Gravenhorst, 1806)
Habroleptoides confusa	Sartori et Jacob, 1986
Habroloma geranii	Silfverberg, 1977
Habrophlebia fusca	(Curtis, 1834)
Habrophlebia lauta	Eaton, 1884
Habropoda zonatula	Smith, 1854
Habrosyne pyritoides	(Hufnagel, 1766)
Hada plebeja	(Linnaeus, 1761)
Hadena albimacula	(Borkhausen, 1792)
Hadena bicruris	(Hufnagel, 1766)
Hadena capsincola	([Denis & Schiffermüller], 1775)
Hadena christophi	(Möschler, 1862)
Hadena compta	([Denis & Schiffermüller], 1775)
Hadena confusa	(Hufnagel, 1766)
Hadena filograna	(Esper, 1788)
Hadena irregularis	(Hufnagel, 1766)
Hadena magnolii	(Boisduval, 1829)
Hadena perplexa	([Denis & Schiffermüller], 1775)
Hadena silenes	(Hübner, 1822)
Hadreule elongatulum	(Gyllenhal, 1827)
Hadrobregmus denticollis	(Creutzer, 1796)
Hadrobregmus pertinax	(Linnaeus, 1758)
Hadroplontus litura	(Fabricius, 1775)
Hadroplontus trimaculatus	(Fabricius, 1775)
Hadula dianthi	(Tauscher, 1809)
Hadula dianthi hungarica	(Tauscher, 1809)
Hadula trifolii	(Hufnagel, 1766)
Haematopota bigoti	Gobert, 1881
Haematopota crassicornis	Wahlberg, 1848
Haematopota grandis	Meigen, 1820
Haematopota italica	Meigen, 1804
Haematopota pandazisi	(Kröber, 1936)
Haematopota pluvialis	(Linnaeus, 1758)
Haematopota scutellata	(Olsufjev, Moucha & Chvála, 1964)
Haematopota subcylindrica	Pandellé, 1883
Haematopus ostralegus	Linnaeus, 1758
Haematopus ostralegus longipes	Linnaeus, 1758
Haemopis sanguisuga	(Linnaeus, 1758)
Hagenella clathrata	(Kolenati, 1848)
Hahnia helveola	Simon, 1875
Hahnia microphthalma	Snazell at Duffay, 1980
Hahnia montana	(Blackwall, 1841)
Hahnia nava	(Blackwall, 1841
Hahnia ononidum	Simon, 1875
Hahnia picta	Kulczynski, 1897
Hahnia pusilla	C.L. Koch, 1841
Halesus digitatus	(Schrank, 1781)
Halesus radiatus	(Curtis, 1834)
Halesus tesselatus	(Rambur, 1842)
Haliaeetus albicilla	(Linnaeus, 1758)
Halictus asperulus	Pérez, 1895
Halictus brunnescens	(Eversmann, 1852)
Halictus cochlearitarsis	Dours, 1872
Halictus confusus	Smith, 1853
Halictus confusus perkinsi	Smith, 1853
Halictus eurygnathus	Blüthgen, 1931
Halictus gavarnicus	Pérez, 1903
Halictus gavarnicus tataricus	Pérez, 1903
Halictus kessleri	Bramson, 1879
Halictus langobardicus	Blüthgen, 1944
Halictus leucaheneus	Ebmer, 1972
Halictus leucaheneus arenosus	Ebmer, 1972
Halictus maculatus	Smith, 1848
Halictus patellatus	Morawitz, 1873
Halictus patellatus taorminicus	Morawitz, 1873
Halictus pollinosus	Sichel, 1860
Halictus pollinosus cariniventris	Sichel, 1860
Halictus quadricinctus	(Fabricius, 1776)
Halictus rubicundus	(Christ, 1791)
Halictus sajoi	Blütgen, 1923
Halictus scabiosae	(Rossi, 1790)
Halictus seladonius	(Fabricius, 1794)
Halictus semitectus	Morawitz, 1873
Halictus sexcinctus	(Fabricius, 1775)
Halictus simplex	Blüthgen, 1923
Halictus smaragdulus	Vachal, 1895
Halictus subauratus	(Rossi, 1792)
Halictus tectus	Radoszkovski, 1875
Halictus tetrazonius	(Klug, 1817)
Halictus tumulorum	(Linnaeus, 1758)
Haliplus apicalis	Thomson, 1868
Haliplus flavicollis	Sturm, 1834
Haliplus fluviatilis	Aubé, 1836
Haliplus fulvicollis	Erichson, 1837
Haliplus fulvus	(Fabricius, 1801)
Haliplus furcatus	Seidlitz, 1887
Haliplus heydeni	Wehncke, 1875
Haliplus immaculatus	Gerhardt, 1877
Haliplus laminatus	(Schaller, 1783)
Haliplus lineatocollis	(Marsham, 1802)
Haliplus maculatus	Motschulsky, 1860
Haliplus obliquus	(Fabricius, 1787)
Haliplus ruficollis	(De Geer, 1774)
Haliplus variegatus	Sturm, 1834
Hallodapus montandoni	(Reuter, 1895)
Hallodapus rufescens	(Burmeister, 1835)
Hallodapus suturalis	(Herrich-Schäffer, 1839)
Hallomenus axillaris	(Illiger, 1807)
Hallomenus binotatus	(Quensel, 1790)
Halticus apterus	(Linnaeus, 1761)
Halticus luteicollis	(Panzer, 1805)
Halticus pusillus	(Herrich-Schäffer, 1835)
Halticus saltator	(Geoffroy, 1785)
Halyzia sedecimguttata	(Linnaeus, 1758)
Hamearis lucina	(Linnaeus, 1758)
Hammerschmidtia ferruginea	(Fallén, 1817)
Handianus cerasi	Emeljanov, 1964
Handianus flavovarius	(Herrich-Schäffer, 1835)
Handianus ignoscus	(Melichar, 1896)
Handianus modestus	(Melichar, 1896)
Handianus procerus	(Herrich-Schäffer, 1835)
Hapalaraea pygmaea	(Paykull, 1800)
Haplodrassus cognatus	(Westring, 1862
Haplodrassus dalmatensis	(L. Koch, 1866)
Haplodrassus kulczynskii	Lohmander, 1942
Haplodrassus minor	(O.P.-Cambridge, 1879)
Haplodrassus moderatus	(Kulczynski, 1897)
Haplodrassus signifer	(C.L. Koch, 1839)
Haplodrassus silvestris	(Blackwall, 1833)
Haplodrassus umbratilis	(L. Koch, 1866)
Haploglomeris multistriata	(C.L.Koch, 1844)
Haploglossa gentilis	(Märkel, 1844)
Haploglossa marginalis	(Gravenhorst, 1806)
Haploglossa nidicola	(Fairmaire, 1852)
Haploglossa picipennis	(Gyllenhal, 1827)
Haploglossa villosula	(Stephens, 1832)
Haploporatia eremita	(Verhoeff, 1909)
Haplorhynchites pubescens	(Fabricius, 1775)
Haplotinea ditella	(Pierce & Diakonoff, 1938)
Haplotinea insectella	(Fabricius, 1794)
Hardya tenuis	(Germar, 1821)
Harmonia quadripunctata	(Pontoppidan, 1763)
Harpactea hombergi	(Scopoli, 1763)
Harpactea lepida	(C.L. Koch, 1838)
Harpactea rubicunda	(C.L. Koch, 1838)
Harpactea saeva	(Herman, 1879)
Harpactus affinis	(Spinola, 1808)
Harpactus consanguineus	(Handlirsch, 1888)
Harpactus elegans	(Lepeletier, 1832)
Harpactus exiguus	(Handlirsch, 1888)
Harpactus laevis	(Latreille, 1792)
Harpactus lunatus	(Dahlbom, 1832)
Harpactus moravicus	(Snoflak, 1946)
Harpactus tauricus	(Radoszkowski, 1884)
Harpactus tumidus	(Panzer, 1801)
Harpadispar diffusalis	(Guenée, 1854)
Harpagoxenus sublaevis	Nylander, 1849
Harpalus affinis	(Schrank, 1781)
Harpalus albanicus	Reitter, 1900
Harpalus angulatus	Putzeys, 1878
Harpalus angulatus scytha	Putzeys, 1878
Harpalus anxius	(Duftschmid, 1812)
Harpalus atratus	Latreille, 1804
Harpalus attenuatus	Stephens, 1828
Harpalus autumnalis	(Duftschmid, 1812)
Harpalus caspius	(Steven, 1806)
Harpalus caspius roubali	(Steven, 1806)
Harpalus cupreus	Dejean, 1829
Harpalus cupreus fastuosus	Dejean, 1829
Harpalus dimidiatus	(Rossi, 1790)
Harpalus distinguendus	(Duftschmid, 1812)
Harpalus flavescens	(Piller et Mitterpacher, 1783)
Harpalus flavicornis	Dejean, 1829
Harpalus froelichii	Sturm, 1818
Harpalus fuscicornis	Ménétriés, 1832
Harpalus fuscipalpis	Sturm, 1818
Harpalus hirtipes	(Panzer, 1797)
Harpalus honestus	(Duftschmid, 1812)
Harpalus hospes	Sturm, 1818
Harpalus inexpectatus	Kataev, 1989
Harpalus latus	(Linnaeus, 1758)
Harpalus luteicornis	(Duftschmid, 1812)
Harpalus marginellus	Dejean, 1829
Harpalus modestus	Dejean, 1829
Harpalus neglectus	Audinet-Serville, 1821
Harpalus oblitus	Dejean, 1829
Harpalus picipennis	(Duftschmid, 1812)
Harpalus progrediens	Schauberger, 1922
Harpalus pumilus	Sturm, 1818
Harpalus pygmaeus	Dejean, 1829
Harpalus quadripunctatus	Dejean, 1829
Harpalus rubripes	(Duftschmid, 1812)
Harpalus rufipalpis	Sturm, 1818
Harpalus saxicola	Dejean, 1829
Harpalus serripes	(Quensel, 1806)
Harpalus servus	(Duftschmid, 1812)
Semiophonus signaticornis	(Duftschmid, 1812)
Harpalus smaragdinus	(Duftschmid, 1812)
Harpalus subcylindricus	Dejean, 1829
Harpalus tardus	(Panzer, 1797)
Harpalus xanthopus	Gemminger et Harold, 1868
Harpalus xanthopus winkleri	Gemminger et Harold, 1868
Harpalus zabroides	Dejean, 1829
Harpella forficella	(Scopoli, 1763)
Harpocera thoracica	(Fallén, 1807)
Harpolithobius anodus	Latzel, 1880
Harpyia milhauseri	(Fabricius, 1775)
Hasarius adansoni	(Audouin, 1827)
Hebetancylus excentricus	(Morelet, 1851)
Hebrus pusillus	(Fallén, 1807)
Hebrus ruficeps	Thomson, 1871
Hecalus glaucescens	(Fieber, 1866)
Hecatera bicolorata	(Hufnagel, 1766)
Hecatera cappa	(Hübner, 1809)
Hecatera dysodea	([Denis & Schiffermüller], 1775)
Hedobia pubescens	(Olivier, 1790)
Hedya dimidiana	(Clerck, 1759)
Hedya nubiferana	(Haworth, 1811)
Hedya ochroleucana	(Frölich, 1828)
Hedya pruniana	(Hübner, 1799)
Hedya salicella	(Linnaeus, 1758)
Hedychridium adventicium	Zimmermann, 1961
Hedychridium aereolum	Buysson, 1891
Hedychridium aheneum	(Dahlbom, 1854)
Hedychridium ardens	(Coquebert, 1801)
Hedychridium coriaceum	(Dahlbom, 1854)
Hedychridium elegantulum	Buysson, 1887
Hedychridium femoratum	(Dahlbom, 1854)
Hedychridium flavipes	(Eversmann, 1857)
Hedychridium hungaricum	Móczár, 1964
Hedychridium irregulare	Linsenmaier, 1959
Hedychridium jazygicum	Móczár, 1964
Hedychridium jucundum	(Mocsáry, 1889)
Hedychridium krajniki	Balthasar, 1946
Hedychridium monochroum	Buysson, 1888
Hedychridium parkanense	Balthasar, 1946
Hedychridium plagiatum	Mocsáry, 1889
Hedychridium roseum	(Rossi, 1790)
Hedychridium sculpturatum	(Abeille, 1877)
Hedychridium scutellare	Tournier, 1878
Hedychridium valesiense	Linsenmaier, 1959
Hedychridium zelleri	(Dahlbom, 1845)
Hedychrum aureicolle	Mocsáry, 1889
Hedychrum aureicolle niemelai	Mocsáry, 1889
Hedychrum gerstaeckeri	Chevrier, 1869
Hedychrum longicolle	Abeille, 1877
Hedychrum nobile	(Scopoli, 1763)
Hedychrum rutilans	(Dahlbom, 1854)
Heinemannia festivella	(Denis & Schiffermüller, 1775)
Heinemannia laspeyrella	(Hübner, 1796)
Helcystogramma albinervis	(Gerasimov, 1929)
Helcystogramma arulensis	(Rebel, 1929)
Helcystogramma lineolella	(Zeller, 1839)
Helcystogramma lutatella	(Herrich-Schäffer, 1854)
Helcystogramma rufescens	(Haworth, 1828)
Helcystogramma triannulella	(Herrich-Schäffer, 1854)
Helianthemapion aciculare	(Germar, 1817)
Helianthemapion velatum	(Gerstäcker, 1854)
Helicoconis lutea	(Wallangren, 1871)
Helicoconis pseudolutea	Ohm, 1965
Helicoconis transsylvanica	Kis, 1965
Helicodonta obvoluta	(O.F. Müller, 1774)
Helicopsis instabilis	(Rossmässler, 1838)
Helicopsis striata	(O.F. Müller, 1774)
Helicopsyche bacescui	Orghidan et Botosaneanu, 1953
Helicoverpa armigera	(Hübner, 1808)
Heliococcus bohemicus	Sulc,1912
Heliococcus cydoniae	Borchsenius,1949
Heliococcus danzigae	Bazarov,1974
Heliococcus nivearum	Balachowsky,1953
Heliococcus radicicola	Goux,1931
Heliococcus sulcii	Goux, 1934
Heliodines roesella	(Linnaeus, 1758)
Heliomata glarearia	(Denis & Schiffermüller, 1775)
Heliophanus aeneus	(Hahn, 1831)
Heliophanus auratus	C.L. Koch, 1835
Heliophanus cupreus	(Walckenaer, 1802)
Heliophanus dubius	C.L. Koch, 1835
Heliophanus flavipes	Hahn, 1832
Heliophanus kochii	Simon, 1868
Heliophanus lineiventris	Simon, 1868
Heliophanus patagiatus	Thorell, 1875
Heliophanus simplex	Simon, 1868
Heliophanus tribulosus	Simon, 1868
Heliothela wulfeniana	(Scopoli, 1763)
Heliothis maritima	(Graslin, 1855)
Heliothis maritima bulgarica	(Graslin, 1855)
Heliothis nubigera	Herrich-Schäffer, 1851
Heliothis ononis	([Denis & Schiffermüller], 1775)
Heliothis peltigera	([Denis & Schiffermüller], 1775)
Heliothis viriplaca	(Hufnagel, 1766)
Heliozela resplendella	(Stainton, 1851)
Heliozela sericiella	(Haworth, 1828)
Helix lucorum	Linnaeus, 1758
Helix lutescens	Rossmässler, 1837
Helix pomatia	Linnaeus, 1758
Hellinsia osteodactylus	(Zeller, 1841)
Hellula undalis	(Fabricius, 1755)
Helobdella stagnalis	(Linnaeus, 1758)
Helochares lividus	(Forster, 1771)
Helochares obscurus	(O.F.Müller, 1776)
Helophilus hybridus	Loew, 1846
Helophilus pendulus	(Linnaeus, 1758)
Helophilus trivittatus	(Fabricius, 1805)
Helophora insignis	(Blackwall, 1841)
Helophorus aequalis	Thomson, 1868
Helophorus aquaticus	(Linnaeus, 1758)
Helophorus arvernicus	Mulsant, 1846
Helophorus asperatus	Rey, 1885
Helophorus brevipalpis	Bedel, 1881
Helophorus croaticus	Kuwert, 1886
Helophorus discrepans	Rey, 1885
Helophorus dorsalis	(Marsham, 1802)
Helophorus flavipes	Fabricius, 1792
Helophorus granularis	(Linnaeus, 1761)
Helophorus griseus	Herbst, 1793
Helophorus liguricus	Angus, 1970
Helophorus longitarsis	Wollaston, 1864
Helophorus micans	Faldermann, 1835
Helophorus minutus	Fabricius, 1775
Helophorus montenegrinus	Kuwert, 1885
Helophorus nanus	Sturm, 1836
Helophorus nubilus	Fabricius, 1776
Helophorus obscurus	Mulsant, 1844
Helophorus paraminutus	Angus, 1986
Helophorus porculus	Bedel, 1881
Helophorus redtenbacheri	Kuwert, 1885
Helophorus rufipes	Bosc D`Antic, 1791
Helophorus strigifrons	Thomson, 1868
Helophorus villosus	Duftschmid, 1805
Hemaris fuciformis	(Linnaeus, 1758)
Hemaris tityus	(Linnaeus, 1758)
Hemerobius atrifrons	McLachlan, 1868
Hemerobius contumax	Tjeder, 1932
Hemerobius fenestratus	Tjeder, 1932
Hemerobius gilvus	Stein, 1863
Hemerobius handschini	Tjeder, 1957
Hemerobius humulinus	Linnaeus, 1758
Hemerobius lutescens	Fabricius, 1793
Hemerobius marginatus	Stephens, 1836
Hemerobius micans	Olivier, 1792
Hemerobius nitidulus	Fabricius, 1777
Hemerobius perelegans	Stephens, 1836
Hemerobius pini	Stephens, 1836
Hemerobius simulans	Walker, 1853
Hemerobius stigma	Stephens, 1836
Hemianax ephippiger	(Burmeister, 1839)
Hemiclepsis marginata	(O.F. Müller, 1774)
Hemicoelus costatus	(Gené, 1830)
Hemicoelus fulvicornis	(Sturm, 1837)
Hemicoelus nitidus	(Herbst, 1792)
Hemicoelus rufipennis	(Duftschmid, 1825)
Hemicrepidius hirtus	(Herbst, 1784)
Hemicrepidius niger	(Linnaeus, 1758)
Hemidiaptomus amblyodon	(Marenzeller, 1873)
Hemidiaptomus hungaricus	Kiefer, 1933
Hemipterochilus bembiciformis	(Morawitz,1867)
Hemipterochilus bembiciformis terricola	(Morawitz,1867)
Hemistola chrysoprasaria	(Esper, 1795)
Hemithea aestivaria	(Hübner, 1789)
Hemitrichapion pavidum	(Germar, 1817)
Hemitrichapion reflexum	(Gyllenhal, 1833)
Hemitrichapion waltoni	(Stephens, 1839)
Endecatomus reticulatus	(Herbst, 1793)
Henestaris halophilus	(Burmeister, 1835)
Henia bicarinata	(Meinert, 1870)
Henia illyrica	(Meinert, 1870)
Henosepilachna argus	(Fourcroy, 1758)
Henosepilachna elaterii	(Rossi, 1794)
Henoticus serratus	(Gyllenhal, 1808)
Henschia acuta	(Löw, 1885)
Hephathus freyi	(Fieber, 1868)
Hephathus nanus	(Herrich-Schäffer, 1835)
Hepialus humuli	(Linnaeus, 1758)
Heptagenia coerulans	Rostock, 1877
Heptagenia flava	Rostock, 1877
Kageronia fuscogrisea	(Retzius, 1783)
Heptagenia longicauda	(Stephens, 1836)
Heptagenia sulphurea	(Müller, 1776)
Heptatoma pellucens	(Fabricius,1776)
Heptaulacus testudinarius	(Fabricius, 1775)
Ocrasa fulvocilialis	(Duponchel, 1834)
Herculia incarnatalis	(Zeller, 1847)
Herculia rubidalis	(Denis & Schiffermüller, 1775)
Heriades crenulatus	Nylander, 1856
Heriades rubicolus	Pérez, 1890
Heriades truncorum	(Linnaeus, 1758)
Heriaeus graminicola	(Doleschall, 1852)
Heriaeus hirtus	(Latreille, 1819)
Heriaeus melloteei	Simon, 1886
Herilla ziegleri	(Küster, 1845)
Herilla ziegleri dacida	(L. Pfeiffer, 1848)
Heringia heringi	(Zetterstedt, 1843)
Heringia senilis	Sack, 1938
Herminia grisealis	([Denis & Schiffermüller], 1775)
Herminia tarsicrinalis	(Knoch, 1782)
Herminia tenuialis	(Rebel, 1899)
Herophila tristis	(Linnaeus, 1767)
Herotilapia multispinosa	(Günther, 1867)
Herpes porcellus	(Lacordaire, 1863)
Herpetocypris chevreuxi	(Sars, 1896)
Herpetocypris reptans	(Baird, 1835)
Hesium domino	(Reuter, 1880)
Hesperia comma	(Linnaeus, 1758)
Hesperocorixa linnaei	(Fieber, 1848)
Hesperocorixa sahlbergi	(Fieber, 1848)
Hesperus rufipennis	(Gravenhorst, 1802)
Haeterius ferrugineus	(Olivier, 1789)
Heterhelus scutellaris	(Heer, 1841)
Heterhelus solani	(Heer, 1841)
Heterocapillus tigripes	(Mulsant, 1852)
Heterocerus crinitus	Kiesenwetter, 1850
Heterocerus fenestratus	(Thunberg, 1784)
Heterocerus flexuosus	Stephens, 1828
Heterocerus fossor	Kiesenwetter, 1843
Heterocerus fusculus	Kiesenwetter, 1843
Augyles hispidulus	(Kiesenwetter, 1843)
Heterocerus marginatus	(Fabricius, 1787)
Heterocerus obsoletus	Curtis, 1828
Heterocerus parallelus	Gebler, 1830
Augyles pruinosus	(Kiesenwetter, 1851)
Augyles sericans	(Kiesenwetter, 1843)
Heterococcus nudus	(Green,1926)
Heterococcus tritici	(Kiritchenko,1932)
Heterocordylus erythropthalmus	(Hahn, 1833)
Heterocordylus genistae	(Scopoli, 1763)
Heterocordylus leptocerus	(Kirschbaum, 1856)
Heterocordylus tibialis	(Hahn, 1831)
Heterocordylus tumidicornis	(Herrich-Schäffer, 1835)
Heterocypris barbara	(Gauthier & Brehm, 1928)
Heterocypris incongruens	(Ramdohr, 1808)
Heterocypris rotundata	(Bronstein, 1928)
Heterocypris salina	(Brady, 1868)
Heterogaster affinis	Herrich-Schäffer, 1835
Heterogaster artemisiae	Schilling, 1829
Heterogaster cathariae	(Geoffroy, 1785)
Heterogaster urticae	(Fabricius, 1787)
Heterogenea asella	(Denis & Schiffermüller, 1775)
Heterogynis penella	(Hübner, 1819)
Heteroporatia simile	(Attems, 1895)
Heteropterus morpheus	(Pallas, 1771)
Heterothops dissimilis	(Gravenhorst, 1802)
Heterothops praevius niger	Erichson, 1839
Heterothops praevius	Erichson, 1839
Heterothops stiglundbergi	Israelson, 1979
Heterotoma merioptera	(Scopoli, 1763)
Heterotoma planicornis	(Pallas, 1772)
Hexarthrum exiguum	(Boheman, 1838)
Hieraaetus fasciatus	(Vieillot, 1822)
Hieraaetus pennatus	(Gmelin, 1788)
Himacerus apterus	(Fabricius, 1798)
Himacerus boops	Schiödte, 1870
Himacerus mirmicoides	(O. Costa, 1834)
Himantopus himantopus	(Linnaeus, 1758)
Hipparchia fagi	(Scopoli, 1763)
Hipparchia semele	(Linnaeus, 1758)
Hipparchia statilinus	(Hufnagel, 1766)
Hippeutis complanatus	(Linnaeus, 1758)
Hippodamia septemmaculata	(De Geer, 1775)
Hippodamia tredecimpunctata	(Linnaeus, 1758)
Hippodamia variegata	(Goeze, 1777)
Hippolais icterina	(Vieillot, 1817)
Iduna pallida	(Lindermayer, 1843)
Iduna pallida elaeica	(Lindermayer, 1843)
Hippotion celerio	(Linnaeus, 1758)
Hirticomus hispidus	(Rossi, 1792)
Hirudo medicinalis	Linnaeus, 1758
Hirudo verbana	Carena, 1820
Hirundo daurica	Laxmann, 1769
Hirundo rustica	Linnaeus, 1758
Hister bissexstriatus	Fabricius, 1801
Hister funestus	Erichson, 1834
Hister helluo	Truqui, 1852
Hister illigeri	Duftschmid, 1805
Hister lugubris	Truqui, 1852
Hister quadrimaculatus	Linnaeus, 1758
Hister quadrinotatus	Scriba, 1790
Hister sepulchralis	Erichson, 1834
Hister unicolor	Linnaeus, 1758
Histopona luxurians	(Kulczynski, 1897)
Histopona torpida	(C.L. Koch, 1834)
Hofmannophila pseudospretella	(Stainton, 1849)
Hogna radiata	(Latreille, 1819)
Holcocranum saturejae	(Kolenati, 1845)
Holcophora statices	Staudinger, 1871
Holcopogon bubulcellus helveolellus	(Staudinger, 1859)
Holcostethus sphacelatus	(Fabricius, 1794)
Holcostethus strictus vernalis	(Fabricius, 1803)
Holobus apicatus	(Erichson, 1837)
Holobus flavicornis	(Lacordaire, 1835)
Holocentropus dubius	(Rambur, 1842)
Holocentropus picicornis	(Stephens, 1836)
Holocentropus stagnalis	(Albarda, 1874)
Hololepta plana	(Sulzer, 1776)
Holoparamecus caularum	Aubé, 1843
Holoparamecus ragusae	Reitter, 1875
Holopedium gibberum	Zaddach, 1855
Holopyga amoenula	Dahlbom, 1854
Holopyga chrysonota	(Förster, 1853)
Holopyga fervida	(Fabricius, 1781)
Holopyga hortobagyensis	Móczár, 1983
Holopyga inflammata	Förster, 1853
Holopyga minuma	Linsenmaier, 1959
Holoscolia huebneri	Koçak, 1980
Holotrichapion ononis	(Faust, 1891)
Holotrichapion aethiops	(Herbst, 1797)
Holotrichapion gracilicolle	(Gyllenhal, 1839)
Holotrichapion pisi	(Fabricius, 1801)
Homalota plana	(Gyllenhal, 1810)
Homaloxestis briantiella	(Turati, 1879)
Homoeosoma inustella	Ragonot, 1884
Homoeosoma nebulella	(Denis & Schiffermüller, 1775)
Homoeosoma nimbella	(Duponchel, 1837)
Homoeosoma sinuella	(Fabricius, 1794)
Homoeosoma subalbatella	(Duponchel, 1837)
Homoeusa acuminata	(Märkel, 1842)
Homonotus balcanicus	Haupt, 1927
Homonotus sangiunolentus	(Fabricius, 1793)
Ruspolia nitidula	(Scopoli, 1786)
Homorosoma validirostre	(Gyllenhal, 1837)
Hoplia argentea	(Poda, 1761)
Hoplia dilutipes	Reitter, 1890
Hoplia hungarica	Burmeister, 1844
Hoplia philanthus	(Fuesslin, 1775)
Hoplia praticola	Duftschmid, 1805
Hoplisoides craverii	(Costa, 1869)
Hoplisoides latifrons	(Spinola, 1808)
Hoplisoides punctuosus	(Eversmann, 1849)
Hoplodrina ambigua	([Denis & Schiffermüller], 1775)
Hoplodrina blanda	([Denis & Schiffermüller], 1775)
Hoplodrina octogenaria	(Goeze, 1781)
Hoplodrina respersa	([Denis & Schiffermüller], 1775)
Hoplodrina superstes	(Ochsenheimer, 1816)
Hoplomachus thunbergii	(Fallén, 1807)
Hoplopholcus forskali	Thorell, 1871
Hoplopterus spinosus	(Linnaeus, 1758)
Horisme aquata	(Hübner, 1813)
Horisme corticata	(Treitschke, 1835)
Horisme radicaria	(La Harpe, 1855)
Horisme tersata	(Denis & Schiffermüller, 1775)
Horisme vitalbata	(Denis & Schiffermüller, 1775)
Horistus orientalis	Gmelin, 1790
Depressaria dictamnella	(Treitschke, 1835)
Horvathianella palliceps	(Horváth, 1897)
Horvathiolus superbus	(Pollich, 1779)
Hoshihananomia perlata	(Sulzer, 1776)
Hucho hucho	(Linnaeus, 1758)
Hungarocypris madaraszi	(Oerley, 1886)
Hungarosoma bokori	Verhoeff, 1928
Huso huso	(Linnaeus, 1758)
Hyalesthes luteipes	Fieber, 1876
Hyalesthes obsoletus	Signoret, 1865
Hyalesthes philesakis	Hoch, 1986
Hyalesthes scotti	Ferrari, 1882
Hyalochiton komaroffii	(Jakovlev, 1880)
Hybomitra acuminata	(Loew, 1858)
Hybomitra aterrima	(Meigen, 1820)
Hybomitra auripila	(Loew, 1858)
Hybomitra bimaculata	(Macquart, 1826)
Hybomitra borealis	(Fabricius, 1781)
Hybomitra ciureai	(Séguy, 1937)
Hybomitra distinguenda	(Verral, 1909)
Hybomitra expollicata	(Pandellé, 1883)
Hybomitra lundbecki	Lyneborg, 1959
Hybomitra lurida	(Fallén, 1817)
Hybomitra media	(Kröber, 1928)
Hybomitra micans	(Fallén, 1817)
Hybomitra morgani	(Surcouf, 1912)
Hybomitra muehlfeldi	(Brauer, 1880)
Hybomitra nitidifrons	(Szilády, 1914)
Hybomitra pilosa	(Loew, 1858)
Hybomitra solstitialis	(Meigen, 1820)
Hybomitra tropica	(Linnaeus, 1758)
Hybomitra ukrainica	(Olsufjev, 1952)
Hycleus polymorphus	(Pallas, 1771)
Hydaticus aruspex	Clark, 1864
Hydaticus continentalis	J.Balfour-Browne, 1944
Hydaticus grammicus	Germar, 1830
Hydaticus seminiger	(De Geer, 1774)
Hydaticus transversalis	(Pontoppidan, 1763)
Hydnobius claviger	Strand, 1943
Hydnobius latifrons	(Curtis, 1840)
Hydnobius multistriatus	(Gyllenhal, 1813)
Hydnobius punctatus	(Sturm, 1807)
Hydnobius spinipes	(Gyllenhal, 1813)
Hydraecia micacea	(Esper, 1789)
Hydraecia petasitis	Doubleday, 1847
Hydraecia ultima	Holst, 1965
Hydraena belgica	Orchymont, 1930
Hydraena britteni	Joy, 1907
Hydraena excisa	Kiesenwetter, 1849
Hydraena flavipes	Sturm, 1836
Hydraena gracilis	Germar, 1824
Hydraena hungarica	Rey, 1884
Hydraena melas	Dalla Torre, 1877
Hydraena morio	Kiesenwetter, 1894
Hydraena nigrita	Germar, 1824
Hydraena paganettii	Ganglbauer, 1901
Hydraena palustris	Erichson, 1837
Hydraena pulchella	Germar, 1824
Hydraena pygmaea	Waterhouse, 1833
Hydraena reyi	Kuwert, 1888
Hydraena riparia	Kugelann, 1794
Hydraena saga	Orchymont, 1930
Hydrelia flammeolaria	(Hufnagel, 1767)
Hydrelia sylvata	(Denis & Schiffermüller, 1775)
Hydriomena furcata	(Thunberg, 1784)
Hydriomena impluviata	(Denis & Schiffermüller, 1775)
Hydrobius fuscipes	(Linnaeus, 1758)
Hydrochara caraboides	(Linnaeus, 1758)
Hydrochara dichroma	(Fairmaire, 1892)
Hydrochara flavipes	(Steven, 1808)
Hydrochus angustatus	Germar, 1824
Hydrochus brevis	Herbst, 1793
Hydrochus crenatus	(Fabricius, 1792)
Hydrochus elongatus	(Schaller, 1783)
Hydrochus flavipennis	Küster, 1852
Hydrochus ignicollis	Motschulsky, 1860
Hydrochus megaphallus	Berge Henegouwen, 1988
Hydrocyphon deflexicollis	(P.W.J. Müller, 1821)
Hydroglyphus geminus	(Fabricius, 1792)
Hydrometra gracilenta	Horváth, 1899
Hydrometra stagnorum	(Linnaeus, 1758)
Hydrophilus aterrimus	Eschscholtz, 1822
Hydrophilus piceus	(Linnaeus, 1758)
Hydroporus angustatus	Sturm, 1835
Hydroporus discretus	Fairmaire et Brisout, 1859
Hydroporus discretus ponticus	Fairmaire et Brisout, 1859
Hydroporus dobrogeanus	Ienistea, 1963
Hydroporus erythrocephalus	(Linnaeus, 1758)
Hydroporus ferrugineus	Stephens, 1829
Hydroporus foveolatus	Heer, 1839
Hydroporus fuscipennis	Schaum, 1868
Hydroporus hebaueri	Hendrich, 1990
Hydroporus incognitus	Sharp, 1869
Hydroporus longicornis	Sharp, 1871
Hydroporus marginatus	(Duftschmid, 1805)
Hydroporus melanarius	Sturm, 1835
Hydroporus memnonius	Nicolai, 1822
Hydroporus neglectus	Schaum, 1845
Hydroporus nigrita	(Fabricius, 1792)
Hydroporus notatus	Sturm, 1835
Hydroporus palustris	(Linnaeus, 1761)
Hydroporus planus	(Fabricius, 1781)
Hydroporus rufifrons	(O.F.Müller, 1776)
Hydroporus scalesianus	Stephens, 1828
Hydroporus striola	(Gyllenhal, 1827)
Hydroporus tristis	(Paykull, 1798)
Hydroporus umbrosus	(Gyllenhal, 1808)
Hydropsyche angustipennis	(Curtis, 1834)
Hydropsyche bulbifera	McLachlan, 1878
Hydropsyche bulgaromanorum	Malicky, 1977
Hydropsyche contubernalis	McLachlan, 1865
Hydropsyche exocellata	Dufour, 1841
Hydropsyche fulvipes	(Curtis, 1834)
Hydropsyche guttata	Curtis, 1834
Hydropsyche instabilis	(Curtis, 1834)
Hydropsyche modesta	Navás, 1925
Hydropsyche ornatula	McLachlan, 1878
Hydropsyche pellucidula	(Curtis, 1834)
Hydropsyche saxonica	McLachlan, 1884
Hydropsyche siltalai	Döhler, 1963
Hydroptila angustata	Mosely, 1939
Hydroptila cornuta	Mosely, 1922
Hydroptila dampfi	Ulmer, 1929
Hydroptila forcipata	(Eaton, 1873)
Hydroptila lotensis	Mosely, 1930
Hydroptila occulta	(Eaton, 1873)
Hydroptila pulchricornis	Pictet, 1834
Hydroptila simulans	Mosely, 1920
Hydroptila sparsa	Curtis, 1834
Hydroptila tineoides	Dalman, 1819
Hydroptila vectis	Curtis, 1834
Hydrosmecta delicatula	(Sharp, 1869)
Hydrosmecta longula	(Heer, 1839)
Hydrovatus cuspidatus	(Kunze, 1818)
Hygrobia hermanni	(Fabricius, 1775)
Hygrolycosa rubrofasciata	(Ohlert, 1865)
Hygromia cinctella	(Draparnaud, 1801)
Hygronoma dimidiata	(Gravenhorst, 1806)
Hygrotus confluens	(Fabricius, 1787)
Hygrotus decoratus	(Gyllenhal, 1808)
Hygrotus enneagrammus	(Ahrens, 1833)
Hygrotus impressopunctatus	(Schaller, 1783)
Hygrotus inaequalis	(Fabricius, 1776)
Hygrotus pallidulus	(Aubé, 1850)
Hygrotus parallellogrammus	(Ahrens, 1812)
Hygrotus versicolor	(Schaller, 1783)
Hyla arborea	(Linnaeus, 1758)
Hylaea fasciaria	(Linnaeus, 1758)
Hylaeus angustatus	(Schenck, 1859)
Hylaeus annularis	(Kirby, 1802)
Hylaeus annulatus	(Linnaeus, 1758)
Hylaeus brevicornis	Nylander, 1852
Hylaeus clypearis	(Schenck, 1853)
Hylaeus communis	Nylander, 1852
Hylaeus cornutus	Curtis, 1831
Hylaeus difformis	(Eversmann, 1852)
Hylaeus duckei	(Alfken, 1904)
Hylaeus euryscapus	Förster, 1871
Hylaeus gibbus	Saunders, 1850
Hylaeus gibbus confusus	Saunders, 1850
Hylaeus gracilicornis	(Morawitz, 1867)
Hylaeus hyalinatus	Smith, 1842
Hylaeus leptocephalus	(Morawitz, 1870)
Hylaeus lineolatus	(Schenck, 1859)
Hylaeus moricei	(Friese, 1898)
Hylaeus nigritus	(Fabricius, 1798)
Hylaeus pectoralis	Förster, 1871
Hylaeus pfankuchi	(Alfken, 1919)
Hylaeus pictipes	Nylander, 1852
Hylaeus punctatus	(Brullé, 1832)
Hylaeus punctulatissimus	Smith, 1842
Hylaeus punctus	Förster, 1871
Hylaeus rinki	(Gorski, 1852)
Hylaeus signatus	(Panzer, 1798)
Hylaeus sinuatus	(Schenck, 1853)
Hylaeus styriacus	Förster, 1871
Hylaeus trinotatus	(Pérez, 1895)
Hylaeus variegatus	(Fabricius, 1798)
Hylastes angustatus	(Herbst, 1793)
Hylastes ater	(Paykull, 1800)
Hylastes attenuatus	Erichson, 1836
Hylastes brunneus	Erichson, 1836
Hylastes cunicularius	Erichson, 1836
Hylastes linearis	Erichson, 1836
Hylastes opacus	Erichson, 1836
Hylastinus obscurus	(Marsham, 1802)
Hylebainosoma tatranum	Verhoeff, 1899
Hylebainosoma tatranum josvaense	Verhoeff, 1899
Hylecoetus dermestoides	(Linnaeus, 1761)
Hyledelphax elegantulus	(Boheman, 1847)
Hyles euphorbiae	(Linnaeus, 1758)
Hyles gallii	(Rottemburg, 1775)
Hyles livornica	(Esper, 1779)
Hyles vespertilio	(Esper, 1779)
Hylesinus crenatus	(Fabricius, 1787)
Hylesinus toranio	(Danthoine & Bernard, 1788)
Hylis cariniceps	(Reitter, 1902)
Hylis foveicollis	(Thomson, 1874)
Hylis simonae	(Olexa, 1970)
Hylobius abietis	(Linnaeus, 1758)
Hylobius excavatus	(Laicharting, 1781)
Hylobius pinastri	(Gyllenhal, 1813)
Hylobius transversovittatus	(Goeze, 1777)
Sphinx pinastri	Linnaeus, 1758
Hylotrupes bajulus	(Linnaeus, 1758)
Hylurgops glabratus	(Zetterstedt, 1828)
Hylurgops palliatus	(Gyllenhal, 1813)
Hylurgus ligniperda	(Fabricius, 1792)
Hylyphantes graminicola	(Sundevall, 1830)
Hylyphantes nigritus	(Simon, 1881)
Hymenalia morio	(Redtenbacher, 1849)
Hymenalia rufipes	(Fabricius, 1792)
Hyoidea notaticeps	Reuter, 1876
Hypatima rhomboidella	(Linnaeus, 1758)
Hypatopa binotella	(Thunberg, 1794)
Hypatopa inunctella	(Zeller, 1839)
Hypebaeus flavipes	(Fabricius, 1787)
Hypena crassalis	(Fabricius, 1787)
Hypena obesalis	Treitschke, 1829
Hypena proboscidalis	(Linnaeus, 1758)
Hypena rostralis	(Linnaeus, 1758)
Hypenodes humidalis	Doubleday, 1850
Hypenodes orientalis	Staudinger, 1901
Hypera arator	(Linnaeus, 1758)
Hypera arundinis	(Paykull, 1792)
Hypera contaminata	(Herbst, 1795)
Hypera cumana	(Petri, 1901)
Hypera denominanda	(Capiomont, 1868)
Hypera diversipunctata	(Schrank, 1798)
Hypera fornicata	(Penecke, 1928)
Hypera fuscocinerea	(Marsham, 1802)
Hypera meles	(Fabricius, 1792)
Hypera nigrirostris	(Fabricius, 1775)
Hypera ononidis	(Chevrolat, 1863)
Hypera pastinacae	(Rossi, 1790)
Hypera plantaginis	(DeGeer, 1775)
Hypera pollux	(Fabricius, 1801)
Hypera postica	(Gyllenhal, 1813)
Hypera rumicis	(Linnaeus, 1758)
Hypera striata	(Boheman, 1834)
Hypera suspiciosa	(Herbst, 1795)
Hypera venusta	(Fabricius, 1781)
Hypera viciae	(Gyllenhal, 1813)
Hyperaspis campestris	(Herbst, 1783)
Hyperaspis concolor	(Suffrian, 1843)
Hyperaspis erytrocephala	(Fabricius, 1787)
Hyperaspis inexpectata	Günther, 1959
Hyperaspis pseudopustulata	Mulsant, 1853
Hyperaspis reppensis quadrimaculata	(Herbst, 1783)
Hyperaspis reppensis	(Herbst, 1783)
Hypercallia citrinalis	(Scopoli, 1763)
Hyperlais dulcinalis	(Treitschke, 1835)
Hyphantria cunea	(Drury, 1773)
Hyphoraia aulica	(Linnaeus, 1758)
Hyphydrus anatolicus	Guignot, 1957
Hyphydrus ovatus	(Linnaeus, 1761)
Hypnogyra angularis	(Ganglbauer, 1895)
Hypoborus ficus	Erichson, 1849
Hypocacculus metallescens	(Erichson, 1834)
Hypocacculus rubripes	(Erichson, 1834)
Hypocacculus rufipes	(Kugelann, 1792)
Hypocacculus spretulus	(Erichson, 1834)
Hypocaccus metallicus	(Herbst, 1792)
Hypocaccus rugiceps	(Duftschmid, 1805)
Hypocaccus rugifrons	(Paykull, 1798)
Hypochalcia ahenella	(Denis & Schiffermüller, 1775)
Hypochalcia bruandella	(Guenée, 1845)
Hypochalcia decorella	(Hübner, 1810)
Hypochalcia dignella	(Hübner, 1796)
Hypochalcia griseoaenella	Ragonot, 1887
Hypochalcia lignella	(Hübner, 1796)
Hypochrysa elegans	(Burmeister, 1839)
Hypocoprus latridioides	Motschulsky, 1839
Hypoganus inunctus	(Panzer, 1795)
Hypomecis punctinalis	(Scopoli, 1763)
Hypomecis roboraria	(Denis & Schiffermüller, 1775)
Hypomma bituberculatum	(Wider, 1834)
Hypomma cornutum	(Blackwall, 1833)
Hypomma fulvum	(Bösenberg, 1902)
Hyponephele lupinus	(O. Costa, 1836)
Hyponephele lycaon	(Rottemburg, 1775)
Hypophthalmichthys molitrix	(Valenciennes, 1844)
Hypophthalmichthys nobilis	(Richardson, 1845)
Hypoponera punctatissima	(Roger, 1859)
Hyporatasa allotriella	(Herrich-Schäffer, 1855)
Hypothenemus hampei	(Ferrari, 1867)
Hypoxystis pluviaria	(Fabricius, 1787)
Hyppa rectilinea	(Esper, 1788)
Hypseloecus visci	(Puton, 1888)
Hypsopygia costalis	(Fabricius, 1775)
Hypsosinga albovittata	(Westring, 1851)
Hypsosinga heri	(Hahn, 1831)
Hypsosinga pygmaea	(Sundevall, 1832)
Hypsosinga sanguinea	(C.L. Koch, 1844
Hypsotropa unipunctella	Ragonot, 1888
Hypsugo savii	(Bonaparte, 1837)
Hyptiotes paradoxus	(C.L. Koch, 1834)
Hypulus bifasciatus	(Fabricius, 1792)
Hypulus quercinus	(Quensel, 1790)
Hyssia cavernosa	(Eversmann, 1843)
Hysterophora maculosana	(Haworth, 1811)
Latematium latifrons	(Fieber, 1877)
Iassus lanio	(Linnaeus, 1761)
Iassus mirabilis	Orosz, 1979
Iassus scutellaris	(Fieber, 1868)
Icaris sparganii	(Gyllenhal, 1836)
Icodema infuscata	(Fieber, 1861)
Ictalurus punctatus	(Rafinesque, 1818)
Ictiobus bubalus	(Rafinesque, 1818)
Icus angularis	Fieber, 1861
Idaea aureolaria	(Denis & Schiffermüller, 1775)
Idaea aversata	(Linnaeus, 1758)
Idaea bilinearia	(Fuchs, 1878) 
Idaea biselata	(Hufnagel, 1767)
Idaea degeneraria	(Hübner, 1799)
Idaea dilutaria	(Hübner, 1799)
Idaea dimidiata	(Hufnagel, 1767)
Idaea elongaria	(Rambur, 1833)
Idaea elongaria pecharia	(Rambur, 1833)
Idaea emarginata	(Linnaeus, 1758)
Idaea filicata	(Hübner, 1799)
Idaea fuscovenosa	(Goeze, 1781)
Idaea humiliata	(Hufnagel, 1767)
Idaea inquinata	(Scopoli, 1763)
Idaea laevigata	(Scopoli, 1763)
Idaea deversaria	(Herrich-Schäffer, 1847)
Idaea moniliata	(Denis & Schiffermüller, 1775)
Idaea muricata	(Hufnagel, 1767)
Idaea nitidata	(Herrich-Schäffer, 1861)
Idaea obsoletaria	(Rambur, 1833)
Idaea ochrata	(Scopoli, 1763)
Isomira antennata	(Panzer, 1798)
Idaea pallidata	(Denis & Schiffermüller, 1775)
Idaea politaria	(Hübner, 1799)
Idaea rufaria	(Hübner, 1799)
Idaea rusticata	(Denis & Schiffermüller, 1775)
Idaea seriata	(Schrank, 1802)
Idaea sericeata	(Hübner, 1813)
Idaea serpentata	(Hufnagel, 1767)
Idaea straminata	(Borkhausen, 1794)
Idaea subsericeata	(Haworth, 1809)
Idaea sylvestraria	(Hübner, 1799)
Idaea trigeminata	(Haworth, 1809)
Idia calvaria	([Denis & Schiffermüller], 1775)
Populicerus albicans	(Kirschbaum, 1868)
Idiocerus confusus	Flor, 1861
Tremulicerus distinguendus	(Kirschbaum, 1868)
Idiocerus fasciatus	Fieber, 1868
Tremulicerus fulgidus	(Fabricius, 1775)
Idiocerus herrichii	Kirschbaum, 1868
Idiocerus heydenii	Kirschbaum, 1868
Idiocerus humilis	Horváth, 1897
Idiocerus impressifrons	Kirschbaum, 1868
Idiocerus laminatus	Flor, 1861
Idiocerus latifrons	Matsumura, 1908
Idiocerus lituratus	(Fallén, 1806)
Stenidiocerus poecilus	(Herrich-Schäffer, 1835)
Populicerus populi	(Linnaeus, 1761)
Idiocerus rotundifrons	Kirschbaum, 1868
Idiocerus similis	Kirschbaum, 1868
Idiocerus stigmaticalis	Lewis, 1834
Tremulicerus tremulae	(Estlund, 1796)
Viridicerus ustulatus	(Mulsant & Rey, 1855)
Idiocerus vittifrons	Kirschbaum, 1868
Idiodonus cruentatus	(Panzer, 1799)
Idolus picipennis	(Bach, 1852)
Iliocryptus agilis	Kurz, 1878
Iliocryptus sordidus	(Liévin, 1848)
Ilybius ater	(De Geer, 1774)
Ilybius chalconatus	(Panzer, 1796)
Ilybius crassus	Thomson, 1856
Ilybius erichsoni	(Gemminger et Harold, 1868)
Ilybius fenestratus	(Fabricius, 1781)
Ilybius fuliginosus	(Fabricius, 1792)
Ilybius guttiger	(Gyllenhal, 1808)
Ilybius neglectus	(Erichson, 1837)
Ilybius quadriguttatus	(Lacordaire, 1835)
Ilybius similis	Thomson, 1855
Ilybius subaeneus	Erichson, 1837
Ilybius subtilis	Erichson, 1837
Ilyobates bennetti	Donisthorpe, 1914
Ilyobates mech	(Baudi, 1848)
Ilyobates nigricollis	(Paykull, 1800)
Ilyobates propinquus	(Aubé, 1850)
Ilyocoris cimicoides	(Linnaeus, 1758)
Ilyocypris bradyi	Sars, 1890
Ilyocypris gibba	(Ramdohr, 1808)
Ilyocypris monstrifica	(Norman, 1862)
Imnadia yeyetta	Hertzog, 1935
Improphantes geniculatus	(Kulczynski,1898)
Inachis io	(Linnaeus, 1758)
Incestophantes crucifer	(Menge, 1866)
Incurvaria koerneriella	(Zeller, 1839)
Incurvaria masculella	(Denis & Schiffermüller, 1775)
Incurvaria oehlmanniella	(Hübner, 1796)
Incurvaria pectinea	Haworth, 1828
Incurvaria praelatella	(Denis & Schiffermüller, 1775)
Infurcitinea albicomella	(Stainton, 1851)
Infurcitinea argentimaculella	(Stainton, 1849)
Infurcitinea finalis	Gozmány, 1959
Infurcitinea roesslerella	(Heyden, 1865)
Inocellia crassicornis	(Schummel, 1832)
Involvulus cupreus	(Linnaeus, 1758)
Glaucopsyche iolas	(Ochsenheimer, 1816)
Iphiclides podalirius	(Linnaeus, 1758)
Ipidia binotata	Reitter, 1879
Ipimorpha retusa	(Linnaeus, 1761)
Ipimorpha subtusa	([Denis & Schiffermüller], 1775)
Ips acuminatus	(Gyllenhal, 1827)
Ips cembrae	(Heer, 1836)
Ips duplicatus	(C. R. Sahlberg, 1836)
Ips mannsfeldi	Wachtl, 1879
Ips sexdentatus	(Börner, 1767)
Ips typographus	(Linnaeus, 1758)
Ironoquia dubia	(Stephens, 1837)
Isauria dilucidella	(Duponchel, 1836)
Ischnocoris angustulus	(Boheman, 1853)
Ischnocoris hemipterus	(Schilling, 1829)
Ischnocoris punctulatus	Fieber, 1861
Ischnodemus sabuleti	(Fallén, 1829)
Ischnodes sanguinicollis	(Panzer, 1793)
Ischnoglossa obscura	Wunderle, 1990
Ischnoglossa prolixa	(Gravenhorst, 1802)
Ischnomera caerulea	(Linnaeus, 1758)
Ischnomera cinerascens	(Pandellé, 1867)
Ischnomera cyanea	(Fabricius, 1787)
Ischnomera sanguinicollis	(Fabricius, 1787)
Ischnopoda leucopus	(Marsham, 1802)
Ischnopoda umbratica	(Erichson, 1837)
Ischnopterapion aeneomicans	(Wencker, 1864)
Ischnopterapion fallens	(Marseul, 1888)
Ischnopterapion loti	(Kirby, 1808)
Ischnopterapion modestum	(Germar, 1817)
Ischnopterapion virens	(Herbst, 1797)
Ischnosoma longicorne	(Mäklin, 1847)
Ischnosoma splendidum	(Gravenhorst, 1806)
Ischnura elegans	(Vander Linden, 1820)
Ischnura elegans pontica	(Vander Linden, 1820)
Ischnura pumilio	(Charpentier, 1825)
Ischyrosyrphus glaucius	(Linnaeus, 1758)
Ischyrosyrphus laternarius	(Müller, 1776)
Isidiella nickerlii	(Nickerl, 1864)
Isochnus angustifrons	(West, 1916)
Isochnus foliorum	(O. F. Müller, 1776)
Isochnus populicola	(Silfverberg, 1977)
Isodontia mexicana	(Saussure, 1867)
Isogenus nubecula	Newman, 1833
Isognomostoma isognomostomos	(Schröter, 1784)
Isometopus intrusus	(Herrich-Schäffer, 1842)
Isometopus mirificus	Mulsant & Rey, 1879
Isomira murina	(Linnaeus, 1758)
Isonychia ignota	(Walker, 1853)
Isoperla difformis	(Klapálek, 1909)
Isoperla grammatica	(Poda, 1761)
Isoperla obscura	(Zetterstedt, 1840)
Isoperla pawlowskii	Wojtas, 1961
Isoperla tripartita	Illies, 1954
Isophrictis anthemidella	(Wocke, 1871)
Isophrictis striatella	(Denis & Schiffermüller, 1775)
Isophya brevipennis	Brunner on Wattenwyl, 1878
Isophya costata	Brunner on Wattenwyl, 1878
Isophya kraussii	Brunner von Wattenwyl, 1878
Isophya modesta	Frivaldszky, 1876
Isophya modestior	Brunner on Wattenwyl, 1882
Isophya stysi	Cejchan, 1957
Isoptena serricornis	(Pictet, 1881)
Isoriphis marmottani	(Bonvouloir, 1871)
Isoriphis melasoides	(Laporte de Castelnau, 1835)
Isotomus speciosus	(Schneider, 1787)
Isotrias hybridana	(Hübner, 1817)
Isotrias rectifasciana	(Haworth, 1811)
Issoria lathonia	(Linnaeus, 1758)
Issus coleoptratus	(Fabricus, 1781)
Issus muscaeformis	(Schrank, 1781)
Isturgia roraria	(Fabricius, 1776)
Italobdella ciosi	Bielecki, 1993
Macaria brunneata	(Thunberg, 1784)
Ithytrichia lamellaris	Eaton, 1873
Ityocara rubens	(Erichson, 1837)
Iwaruna klimeschi	Wolff, 1958
Ixapion variegatum	(Wencker, 1864)
Ixobrychus minutus	(Linnaeus, 1766)
Jalla dumosa	(Linnaeus, 1758)
Japananus hyalinus	(Osborn, 1900)
Jassargus distinquendus	(Flor, 1861)
Jassargus flori	(Fieber, 1869)
Jassargus obtusivalvis	(Kirschbaum, 1868)
Jassargus sursumflexus	(Then, 1902)
Jassidaeus lugubris	(Signoret, 1865)
Jassus varipennis	Herrich-Schäffer, 1838
Javesella discolor	(Boheman, 1847)
Javesella dubia	(Kirschbaum, 1868)
Javesella obscurella	(Boheman, 1847)
Javesella pellucida	(Fabricius, 1794)
Javesella salina	(Haupt, 1924)
Javesella stali	(Metcalf, 1943)
Jodia croceago	([Denis & Schiffermüller], 1775)
Jodis lactearia	(Linnaeus, 1758)
Jordanita budensis	(Ad. & Au. Speyer, 1858)
Jordanita chloros	(Hübner, 1813)
Jordanita fazekasi	Efetov, 1998
Jordanita globulariae	(Hübner, 1793)
Jordanita graeca	(Jordan, 1907)
Jordanita notata	(Zeller, 1847)
Jordanita subsolana	(Staudinger, 1862)
Jucancistrocerus jucundus	(Mocsáry, 1883)
Judolia sexmaculata	(Linnaeus, 1758)
Julus scandinavius	Latzel, 1884
Julus scanicus	(Lohmander, 1925)
Julus terrestris	Linnaeus, 1758
Julus terrestris balatonensis	Linnaeus, 1758
Jynx torquilla	Linnaeus, 1758
Kaestneria dorsalis	(Wider, 1834)
Kaestneria pullata	(O.P.-Cambridge, 1863)
Kalama henschi	Puton, 1892
Kalama tricornis	(Schrank, 1801)
Kalcapion pallipes	(Kirby, 1808)
Kalcapion semivittatum	(Gyllenhal, 1833)
Katamenes arbustorum	(Panzer, 1799)
Kateretes mixtus	Kirejtshuk, 1989
Kateretes pedicularius	(Linnaeus, 1758)
Kateretes pusillus	(Thunberg, 1794)
Kateretes rufilabris	(Latreille, 1807)
Greenisca glyceriae	(Green, 1921)
Greenisca laeticoris	Tereznikova, 1965
Kaweckia rubra	(Matesova,1960)
Keijia tincta	(Walckenaer, 1802)
Kelisia brucki	Fieber, 1878
Kelisia confusa	Linnavuori, 1957
Kelisia guttula	(Germar, 1818)
Kelisia henschii	Horváth, 1897
Kelisia irregulata	Haupt, 1935
Kelisia melanops	Fieber, 1878
Kelisia monoceros	Ribaut, 1934
Kelisia pallidula	(Boheman, 1847)
Kelisia pannonica	Matsumura, 1910
Kelisia perrieri	Ribaut, 1934
Kelisia praecox	Haupt, 1935
Kelisia punctulum	(Kirschbaum, 1868)
Kelisia vittipennis	(J. Sahlberg, 1868)
Sciota adelphella	(Fischer v. Röslerstamm, 1836)
Sciota fumella	(Eversmann, 1844)
Sciota hostilis	(Stephens, 1834)
Sciota rhenella	(Zincken, 1818)
Kermes bacciformis	Leonardi,1908
Kermes corticalis	(Nassonov,1908)
Kermes gibbosus	Signoret,1875
Kermes quercus	(Linnaeus,1758)
Kermes roboris	(Fourcroy,1785)
Khorassania compositella	(Treitschke, 1835)
Kisanthobia ariasi	(Robert, 1858)
Kishidaia conspicua	(L. Koch, 1866)
Kissophagus hederae	(Schmitt, 1843)
Kleidocerys privignus	(Horváth, 1894)
Kleidocerys resedae	(Panzer, 1797)
Klimeschia transversella	(Zeller, 1839)
Klimeschiopsis kiningerella	(Duponchel, 1843)
Kochiura aulica	(C. L. Koch, 1838)
Korscheltellus lupulinus	(Linnaeus, 1758)
Korynetes caeruleus	(De Geer, 1775)
Korynetes ruficornis	Sturm, 1837
Kosswigianella exigua	(Boheman, 1847)
Kovacsia kovacsi	(Varga & L. Pintér, 1972)
Kovalevskiella phreaticola	(Danielopol, 1965)
Kryphioiulus occultus	(C.L.Koch, 1847)
Kurzia latissima	(Kurz, 1874)
Kyboasca bipunctata	(Oshanin, 1871)
Kybos abstrusus	(Linnavuori, 1949)
Kybos butleri	(Edwards, 1908)
Kybos limpidus	(Wagner, 1955)
Kybos paraltaica	(Orosz, 1996)
Kybos populi	(Edwards, 1908)
Kybos rufescens	Melichar, 1896
Kybos smaragdulus	(Fallén, 1806)
Kybos strigilifer	(Ossiannilsson, 1941)
Kybos strobli	Wagner, 1949
Kybos virgator	(Ribaut, 1933)
Kyklioacalles suturatus	Dieckmann, 1983
Labarrus lividus	(Olivier, 1789)
Labiaticola atricolor	(Boheman, 1844)
Laburrus handlirschi	(Matsumura, 1908)
Laburrus impictifrons	(Boheman, 1852)
Laburrus pellax	(Horváth, 1903)
Lacanobia aliena	(Hübner, 1808)
Lacanobia blenna	(Hübner, 1824)
Lacanobia contigua	([Denis & Schiffermüller], 1775)
Lacanobia oleracea	(Linnaeus, 1758)
Lacanobia splendens	(Hübner, 1808)
Lacanobia suasa	([Denis & Schiffermüller], 1775)
Lacanobia thalassina	(Hufnagel, 1766)
Lacanobia w-latinum	(Hufnagel, 1766)
Laccobius albipes	Kuwert, 1890
Laccobius alternus	Motschulsky, 1855
Laccobius bipunctatus	(Fabricius, 1775)
Laccobius colon	Stephens, 1829
Laccobius gracilis	Motschulsky, 1855
Laccobius minutus	(Linnaeus, 1758)
Laccobius obscuratus	Rottenberg, 1874
Laccobius simulatrix	D Orchymont, 1932
Laccobius sinuatus	Motschulsky, 1849
Laccobius striatulus	(Fabricius, 1801)
Laccobius syriacus	Guillebeau, 1896
Laccophilus hyalinus	(De Geer, 1774)
Laccophilus minutus	(Linnaeus, 1758)
Laccophilus poecilus	Klug, 1834
Laccornis kocae	(Ganglbauer, 1904)
Laccornis oblongus	(Stephens, 1835)
Lacerta agilis	Linnaeus, 1758
Lacerta viridis	(Laurenti, 1768)
Lachnaeus crinitus	(Boheman, 1836)
Laciniaria plicata	(Draparnaud, 1801)
Laelia coenosa	(Hübner, 1808)
Laemophloeus kraussi	(Ganglbauer, 1897)
Laemophloeus monilis	(Fabricius, 1787)
Laemostenus terricola	(Herbst, 1784)
Laena reitteri	Weise, 1877
Laena viennensis	(Sturm, 1807)
Lagria atripes	Mulsant et Guillebeau, 1855
Lagria hirta	(Linnaeus, 1758)
Lamellaxis mauritianus	(L. Pfeiffer, 1852)
Lamellocossus terebra	(Denis & Schiffermüller, 1775)
Lamia textor	(Linnaeus, 1758)
Lamoria anella	(Denis & Schiffermüller, 1775)
Lampides boeticus	(Linnaeus, 1767)
Ovalisia dives	(Guillebeau, 1889)
Ovalisia mirifica	(Mulsant, 1855)
Ovalisia rutilans	(Fabricius, 1777)
Lamprinodes haematopterus	(Kraatz, 1857)
Lamprinodes saginatus	(Gravenhorst, 1806)
Lamprinus erythropterus	(Panzer, 1796)
Lamprobyrrhulus nitidus	(Schaller, 1783)
Lamprodema maura	(Fabricius, 1803)
Lamproglena pulchella	Nordmann, 1832
Lamprohiza splendidula	(Linnaeus, 1767)
Lampronia corticella	(Linnaeus, 1758)
Lampronia flavimitrella	(Hübner, 1817)
Lampronia fuscatella	(Tengström, 1848)
Lampronia morosa	Zeller, 1852
Lampronia pubicornis	(Haworth, 1828)
Lampronia rupella	(Denis & Schiffermüller, 1775)
Lamproplax picea	Flor, 1860
Lampropteryx suffumata	(Denis & Schiffermüller, 1775)
Lamprosticta culta	([Denis & Schiffermüller], 1775)
Lamprotes c-aureum	(Knoch, 1781)
Lamprotettix nitidulus	(Fabricius, 1787)
Lampyris noctiluca	(Linnaeus, 1767)
Lamyctes fulvicornis	Meinert, 1868
Langelandia anophtalma	Aubé, 1842
Lanius collurio	Linnaeus, 1758
Lanius excubitor	Linnaeus, 1758
Lanius excubitor homeyeri	Linnaeus, 1758
Lanius minor	Gmelin, 1788
Lanius senator	Linnaeus, 1758
Laodelphax striatellus	(Fallén, 1826)
Laothoe populi	(Linnaeus, 1758)
Larentia clavaria	(Haworth, 1809)
Larinia bonneti	Spassky, 1939
Larinia elegans	Spassky, 1939
Larinia jeskovi	Marusik, 1986
Larinioides cornutus	(Clerck, 1757)
Larinioides ixobolus	(Thorell, 1873)
Larinioides patagiatus	(Clerck, 1757)
Larinioides sclopetarius	(Clerck, 1757)
Larinioides suspicax	(O.P. Cambridge, 1876)
Larinus beckeri	Petri, 1907
Larinus brevis	(Herbst, 1795)
Larinus canescens	Gyllenhal, 1836
Larinus jaceae	(Fabricius, 1775)
Larinus latus	(Herbst, 1784)
Larinus minutus	Gyllenhal, 1836
Larinus obtusus	Gyllenhal, 1836
Larinus planus	(Fabricius, 1792)
Larinus rugulosus	Petri, 1907
Larinus sturnus	(Schaller, 1783)
Larinus turbinatus	Gyllenhal, 1836
Larra anathema	(Rossi, 1790)
Larus argentatus	Pontoppidan, 1763
Larus cachinnans	Pallas, 1811
Larus michahellis	Naumann, 1840
Larus canus	Linnaeus, 1758
Larus canus heinei	Linnaeus, 1758
Larus delawarensis	Ord, 1815
Larus fuscus	Linnaeus, 1758
Larus fuscus heuglini	Linnaeus, 1758
Larus genei	Bréme, 1840
Larus glaucoides	Meyer, 1822
Larus hyperboreus	Gunnerus, 1767
Larus ichthyaetus	Pallas, 1773
Larus marinus	Linnaeus, 1758
Larus melanocephalus	Temminck, 1820
Larus minutus	Pallas, 1776
Larus pipixcan	Wagler, 1831
Larus ridibundus	Linnaeus, 1766
Larus sabini	Sabine, 1818
Lasaeola prona	(Menge, 1868)
Lasaeola tristis	(Hahn, 1833)
Lasiacantha capucina	(Germar, 1836)
Lasiacantha gracilis	(Herrich-Schäffer, 1830)
Lasiacantha hermani	Vásárhelyi, 1977
Lasiocampa quercus	(Linnaeus, 1758)
Lasiocampa trifolii	([Denis & Schiffermüller], 1775)
Lasiocephala basalis	(Kolenati, 1848)
Lasioderma obscurum	(Solsky, 1867)
Lasioderma redtenbacheri	(Bach, 1852)
Lasioderma serricorne	(Fabricius, 1792)
Lasioderma thoracicum	(Morawitz, 1861)
Lasioglossum aeratum	(Kirby, 1802)
Lasioglossum albipes	(Fabricius, 1781)
Lasioglossum albocinctum	(Lucas, 1846)
Lasioglossum angusticeps	(Perkins, 1895)
Lasioglossum bluethgeni	Ebmer, 1971
Lasioglossum brevicorne	(Schenck, 1868)
Lasioglossum brevicorne aciculatum	(Schenck, 1868)
Lasioglossum buccale	(Pérez, 1903)
Lasioglossum calceatum	(Scopoli, 1763)
Lasioglossum clypeare	(Schenck, 1853)
Lasioglossum convexiusculum	(Schenck, 1853)
Lasioglossum corvinum	(Morawitz, 1878)
Lasioglossum costulatum	(Kriechbaumer, 1873)
Lasioglossum crassepunctatum	(Blüthgen, 1923)
Lasioglossum damascenum	(Pérez, 1911)
Lasioglossum discum	(Smith, 1853)
Lasioglossum elegans	(Lepeletier, 1841)
Lasioglossum euboeense	(Strand, 1909)
Lasioglossum fratellum	(Pérez, 1903)
Lasioglossum fulvicorne	(Kirby, 1802)
Lasioglossum glabriusculum	(Morawitz, 1872)
Lasioglossum griseolum	(Morawitz, 1872)
Lasioglossum intermedium	(Schenck, 1868)
Lasioglossum interruptum	(Panzer, 1798)
Lasioglossum kussariense	(Blüthgen, 1924)
Lasioglossum laeve	(Kirby, 1802)
Lasioglossum laevigatum	(Kirby, 1802)
Lasioglossum laterale	(Brullé, 1832)
Lasioglossum laticeps	(Schenck, 1868)
Lasioglossum lativentre	(Schenck, 1853)
Lasioglossum leucopum	(Kirby, 1802)
Lasioglossum leucozonium	(Schrank, 1781)
Lasioglossum limbellum	(Morawitz, 1878)
Lasioglossum lineare	(Schenck, 1868)
Lasioglossum lissonotum	(Noskiewicz, 1925)
Lasioglossum lucidulum	(Schenck, 1859)
Lasioglossum majus	(Nylander, 1852)
Lasioglossum malachurum	(Kirby, 1802)
Lasioglossum mandibulare	(Morawitz, 1866)
Lasioglossum marginatum	(Brullé, 1832)
Lasioglossum marginellum	(Schenck, 1853)
Lasioglossum mesosclerum	(Pérez, 1903)
Lasioglossum minutissimum	(Kirby, 1802)
Lasioglossum minutulum	(Schenck, 1853)
Lasioglossum morio	(Fabricius, 1793)
Lasioglossum nigripes	(Lepeletier, 1841)
Lasioglossum nitidiusculum	(Kirby, 1802)
Lasioglossum nitidulum	(Fabricius, 1804)
Lasioglossum nitidulum aeneidorsum	(Fabricius, 1804)
Lasioglossum obscuratum	(Morawitz, 1876)
Lasioglossum obscuratum acerbum	(Morawitz, 1876)
Lasioglossum pallens	(Brullé, 1832)
Lasioglossum parvulum	(Schenck, 1853)
Lasioglossum pauxillum	(Schenck, 1853)
Lasioglossum podolicum	(Noskiewicz, 1924)
Lasioglossum politum	(Schenck, 1853)
Lasioglossum pseudocaspicum	(Blüthgen, 1924)
Lasioglossum punctatissimum	(Schenck, 1853)
Lasioglossum puncticolle	(Morawitz, 1872)
Lasioglossum pygmaeum	(Schenck, 1853)
Lasioglossum quadrinotatulum	(Schenck, 1853)
Lasioglossum quadrinotatum	(Kirby, 1802)
Lasioglossum quadrisignatum	(Schenck, 1853)
Lasioglossum rufitarse	(Zetterstedt, 1838)
Lasioglossum samaricum	(Blüthgen, 1935)
Lasioglossum semilucens	(Alfken, 1914)
Lasioglossum setulellum	(Strand, 1909)
Lasioglossum setulosum	(Strand, 1909)
Lasioglossum sexnotatum	(Kirby, 1802)
Lasioglossum sexstrigatum	(Schenck, 1868)
Lasioglossum subfasciatum	(Imhoff, 1832)
Lasioglossum subhirtum	(Lepeletier, 1841)
Lasioglossum tarsatum	(Schenck, 1869)
Lasioglossum trichopygum	(Blüthgen, 1923)
Lasioglossum tricinctum	(Schenck, 1874)
Lasioglossum truncaticolle	(Morawitz, 1878)
Lasioglossum villosulum	(Kirby, 1802)
Lasioglossum xanthopum	(Kirby, 1802)
Lasioglossum zonulum	(Smith, 1848)
Lasiommata maera	(Linnaeus, 1758)
Lasiommata megera	(Linnaeus, 1767)
Lasiommata petropolitana	(Fabricius, 1787)
Lasionycta imbecilla	(Fabricius, 1794)
Lasionycta proxima	(Hübner, 1809)
Lasiorhynchites cavifrons	(Gyllenhal, 1833)
Lasiorhynchites coeruleocephalus	(Schaller, 1783)
Lasiorhynchites olivaceus	(Gyllenhal, 1833)
Lasiorhynchites praeustus	(Boheman, 1845)
Lasiorhynchites sericeus	(Herbst, 1797)
Lasiosomus enervis	(Herrich-Schäffer, 1842)
Blemus discus	(Fabricius, 1792)
Lasius alienus	(Foerster, 1850)
Lasius balcanicus	Seifert, 1988
Lasius bicornis	(Foerster, 1850)
Lasius brunneus	(Latreille, 1798)
Lasius carniolicus	Mayr, 1861
Lasius citrinus	Emery 1922
Lasius emarginatus	(Olivier, 1791)
Lasius flavus	(Fabricius, 1781)
Lasius fuliginosus	(Latreille, 1798)
Lasius meridionalis	(Bondroit, 1919)
Lasius mixtus	(Nylander, 1846)
Lasius myops	Forel,1894
Lasius neglectus	Van Loon, Boomsma & Andrásfalvy, 1990
Lasius niger	(Linnaeus, 1758)
Lasius paralienus	Seifert, 1992
Lasius platythorax	Seifert, 1992
Lasius psammophilus	Seifert, 1992
Lasius umbratus	(Nylander, 1846)
Laspeyria flexula	([Denis & Schiffermüller], 1775)
Latheticus oryzae	Waterhouse, 1880
Lathonura rectirostris	Lilljeborg, 1853
Lathrobium brunnipes	(Fabricius, 1792)
Lathrobium castaneipenne	Kolenati, 1846
Lathrobium crassipes	Mulsant et Rey, 1877
Lathrobium elegantulum	Kraatz, 1857
Lathrobium elongatum	(Linnaeus, 1767)
Lathrobium fovulum	Stephens, 1833
Lathrobium fulvipenne	(Gravenhorst, 1806)
Lathrobium furcatum	Czwalina, 1888
Lathrobium impressum	Heer, 1841
Lathrobium laevipenne	Heer, 1839
Lathrobium longulum	Gravenhorst, 1802
Lathrobium pallidum	Nordmann, 1837
Lathrobium ripicola	Czwalina, 1888
Lathrobium rufipenne	Gyllenhal, 1813
Lathrobium spadiceum	Erichson, 1840
Lathrobium testaceum	Kraatz, 1857
Lathrobium volgense	Hochhuth, 1851
Lathronympha strigana	(Fabricius, 1775)
Lathropus sepicola	(P.W.J. Müller, 1821)
Lathys humilis	(Blackwall, 1855)
Lathys stigmatisata	(Menge, 1869)
Latona setifera	(O.F. Müller, 1785)
Latridius anthracinus	Mannerheim, 1844
Latridius brevicollis	(C.G.Thomson, 1868)
Latridius consimilis	Mannerheim, 1879
Latridius hirtus	Gyllenhal, 1827
Latridius minutus	(Linnaeus, 1767)
Lamprias chlorocephalus	(J. J. Hoffmann, 1803)
Lebia cruxminor	(Linnaeus, 1758)
Lamprias cyanocephalus	(Linnaeus, 1758)
Lebia humeralis	Dejean, 1825
Lebia marginata	(Fourcroy, 1785)
Lebia scapularis	(Fourcroy, 1785)
Lebia trimaculata	(Villers, 1789)
Lecanodiaspis sardoa	Targioni-Tozzetti,1869
Lecanopsis festucae	Borchsenius,1952
Lecanopsis formicarum	Newstead,1893
Lecanopsis porifera	Borchsenius,1952
Lecanopsis terrestris	Borchsenius,1952
Lecithocera nigrana	(Duponchel, 1836)
Ledra aurita	(Linnaeus, 1758)
Legnotus limbosus	(Geoffroy, 1785)
Legnotus picipes	(Fallén, 1807)
Lehmannia marginata	(O.F. Müller, 1774)
Lehmannia nyctelia	(Bourguignat, 1861)
Lehmannia valentiana	(A. Férussac, 1822)
Leichenum pictum	(Fabricius, 1801)
Leiestes seminiger	(Gyllenhal, 1808)
Leioderes kollari	Redtenbacher, 1849
Leiodes austriaca	Daffner, 1982
Leiodes carpathica	(Sturm, 1807)
Leiodes bicolor	(W.L.E. Schmidt, 1841)
Leiodes brunneus	(Sturm, 1807)
Leiodes ciliaris	(W.L.E. Schmidt, 1841)
Leiodes cinnamomeus	(Panzer, 1793)
Leiodes dubius	(Kugelann, 1794)
Leiodes ferruginea	(Fabricius, 1787)
Leiodes flavescens	(W.L.E. Schmidt, 1841)
Leiodes furva	(Erichson, 1845)
Leiodes gallicus	(Reitter, 1884)
Leiodes gyllenhalii	Stephens, 1829
Leiodes hybrida	(Erichson, 1845)
Leiodes longipes	(W.L.E. Schmidt, 1841)
Leiodes lucens	(Fairmaire, 1855)
Leiodes lunicollis	(Rye, 1872)
Leiodes macropus	(Rye, 1873)
Leiodes obesa	(W.L.E. Schmidt, 1841)
Leiodes oblonga	(Erichson, 1845)
Leiodes pallens	(Sturm, 1807)
Leiodes polita	Marsham, 1802
Leiodes rotundatus	(Erichson, 1845)
Leiodes rubiginosus	(Schmidt, 1841)
Leiodes rugosa	Stephens, 1829
Leiodes strigipenne	Daffner, 1983
Leiodes subtilis	(Reitter, 1885)
Leiodes triepkei	(W.L.E. Schmidt, 1841)
Leiodes vladimiri	(Fleischer, 1906)
Leiopsammodius haruspex	(Ádám, 1980)
Leiopus nebulosus	(Linnaeus, 1758)
Leiopus punctulatus	(Paykull, 1800)
Leiosoma scrobifer baudii	Rottenberg, 1871
Leiosoma concinnum	Boheman, 1842
Leiosoma cribrum	(Gyllenhal, 1834)
Leiosoma deflexum	(Panzer, 1794)
Leiosoma oblongulum	(Boheman, 1842)
Leistus ferrugineus	(Linnaeus, 1758)
Leistus piceus	Frölich, 1799
Leistus piceus kaszabi	Frölich, 1799
Leistus rufomarginatus	(Duftschmid, 1812)
Leistus terminatus	(Panzer, 1793)
Lejogaster metallina	(Fabricius, 1777)
Lejogaster tarsata	(Meigen, 1822)
Lejops vittatus	(Meigen, 1822)
Lemonia dumi	(Linnaeus, 1761)
Lemonia taraxaci	([Denis & Schiffermüller], 1775)
Hylesinus wachtli orni	Fuchs, 1906
Hylesinus fraxini	(Panzer, 1779)
Lepidosaphes conchiformis	(Gmelin,1789)
Lepidosaphes granati	Koronéos,1934
Lepidosaphes juniperi	Lindinger,1912
Lepidosaphes minimus	(Newstead,1897)
Lepidosaphes newsteadi	(Sulc,1895)
Lepidosaphes populi	Savescu,1955
Lepidosaphes tiliae	Savescu,1957
Lepidosaphes ulmi	(Linnaeus,1758)
Lepidostoma hirtum	(Fabricius, 1775)
Lepidurus apus	(Linnaeus, 1758)
Lepomis gibbosus	(Linnaeus, 1758)
Leptacinus batychrus	(Gyllenhal, 1827)
Leptacinus formicetorum	Märkel, 1841
Leptacinus intermedius	Donisthorpe, 1936
Leptacinus merkli	Ádám, 1987
Leptacinus pusillus	(Stephens, 1833)
Leptacinus sulcifrons	(Stephens, 1833)
Leptestheria dahalacensis	(Rüppell, 1837)
Ipa keyserlingi	(Ausserer, 1867)
Lepthyphantes leprosus	(Ohlert, 1865)
Lepthyphantes minutus	(Blackwall, 1833)
Leptidea morsei	Fenton, 1881
Leptidea morsei major	Fenton, 1881
Leptidea reali	Reissinger, 1989
Leptidea sinapis	(Linnaeus, 1758)
Leptinus testaceus	P.W.J. Müller, 1817
Leptobium gracile	(Gravenhorst, 1802)
Leptocerus interruptus	(Fabricius, 1775)
Leptocerus tineiformis	(Curtis, 1834)
Leptochilus alpestris	(Saussure, 1855)
Leptochilus regulus	(Saussure, 1855)
Leptochilus tarsatus	(Saussure, 1855)
Leptodora kindti	(Focke, 1844)
Leptoiulus baconyensis	(Verhoeff, 1899)
Leptoiulus cibdellus	(Chamberlin, 1921)
Leptoiulus proximus	(Nemec, 1896)
Leptoiulus proximus noaranus	(Nemec, 1896)
Leptoiulus saltuvagus	(Verhoeff, 1898)
Leptoiulus simplex	(Verhoeff, 1894)
Leptoiulus simplex attenuatus	(Verhoeff, 1894)
Leptoiulus trilobatus	(Verhoeff, 1894)
Leptoiulus tussilaginis	(Verhoeff, 1907)
Leptophius flavocinctus	(Hochhuth, 1849)
Leptophlebia marginata	(Linné, 1767)
Leptophloeus alternans	(Erichson, 1846)
Leptophloeus clematidis	(Erichson, 1845)
Leptophloeus hypobori	(Perris, 1855)
Leptophloeus juniperi	(Grouvelle, 1874)
Leptophyes albovittata	(Kollar, 1838)
Leptophyes boscii	Fieber, 1878
Leptophyes discoidalis	(Frivaldszky, 1867)
Leptophyes laticauda	(Frivaldszky, 1868)
Leptophyes punctatissima	(Bosc, 1792)
Leptopterna dolabrata	(Linnaeus, 1758)
Leptopterna ferrugata	(Fallén, 1807)
Leptopus marmoratus	(Goeze, 1778)
Leptorchestes berolinensis	(C.L. Koch, 1846)
Leptotes pirithous	(Linnaeus, 1767)
Leptothorax acervorum	(Fabricius, 1793)
Leptothorax affinis	Mayr, 1855
Leptothorax clypeatus	Mayr, 1853
Leptothorax corticalis	(Schenck, 1852)
Leptothorax crassispinus	Karawajev, 1926
Leptothorax gredleri	Mayr, 1855
Leptothorax interruptus	Schenck, 1852
Leptothorax muscorum	(Nylander, 1846)
Leptothorax nigriceps	Mayr, 1855
Leptothorax parvulus	(Schenck, 1852)
Leptothorax rabaudi	Bondroit, 1918
Leptothorax sordidulus	Müller, 1923
Leptothorax sordidulus saxonicus	Müller, 1923
Leptothorax tuberum	(Fabricius, 1775)
Leptothorax unifasciatus	(Latreille, 1798)
Leptura aethiops	Poda, 1761
Leptura annularis	Fabricius, 1801
Leptura aurulenta	Fabricius, 1792
Leptura quadrifasciata	Linnaeus, 1758
Lepturobosca virens	(Linnaeus, 1758)
Leptusa flavicornis	Brancsik, 1874
Leptusa fuliginosa	(Aubé, 1850)
Leptusa fumida	(Erichson, 1839)
Leptusa pulchella	(Mannerheim, 1830)
Leptusa ruficollis	(Erichson, 1839)
Lepus europaeus	Pallas, 1778
Lepyronia coleoptrata	(Linnaeus, 1758)
Lepyrus armatus	J. Weise, 1893
Lepyrus capucinus	(Schaller, 1783)
Lepyrus palustris	(Scopoli, 1763)
Lernaea cyprinacea	Linnaeus, 1748
Lernaea esocina	Burmeister, 1832
Lestes barbarus	(Fabricius, 1798)
Lestes dryas	Kirby, 1890
Lestes macrostigma	(Eversmann, 1836)
Lestes sponsa	(Hansemann, 1823)
Lestes virens	(Charpentier, 1825)
Lestes virens vestalis	(Charpentier, 1825)
Lesteva longoelytrata	(Goeze, 1777)
Lesteva luctuosa	Fauvel, 1870
Lesteva pubescens	Mannerheim, 1830
Lesteva punctata	Erichson, 1839
Lestica alata	(Panzer, 1797)
Lestica clypeata	(Schreber, 1759)
Lestica subterranea	(Fabricius, 1775)
Lestiphorus bicinctus	(Rossi, 1794)
Lestiphorus bilunulatus	Costa, 1869
Lethrus apterus	(Laxmann, 1770)
Leucania comma	(Linnaeus, 1761)
Leucania loreyi	(Duponchel, 1827)
Leucania obsoleta	(Hübner, 1803)
Leucaspis lowi	Colvée, 1882
Leucaspis pini	(Hartig,1839)
Leucaspis pusilla	Löw,1883
Leucaspis signoreti	Targioni-Tozzetti,1868
Leucaspius delineatus	(Heckel, 1843)
Squalius cephalus	(Linnaeus, 1758)
Leuciscus idus	(Linnaeus, 1758)
Leuciscus leuciscus	(Linnaeus, 1758)
Telestes souffia	(Risso, 1827)
Leucodonta bicoloria	([Denis & Schiffermüller], 1775)
Leucohimatium jakowlewi	Semenow, 1902
Leucohimatium langei	Solsky, 1866
Leucoma salicis	(Linnaeus, 1758)
Leucophyes pedestris	(Poda, 1761)
Leucoptera aceris	(Fuchs, 1903)
Leucoptera cytisiphagella	Klimesch, 1938
Leucoptera genistae	(M. Hering, 1933)
Leucoptera heringiella	Toll, 1938
Leucoptera laburnella	(Stainton, 1851)
Leucoptera lotella	(Stainton, 1859)
Leucoptera lustratella	(Herrich-Schäffer, 1855)
Leucoptera malifoliella	(O. Costa, 1836)
Leucoptera onobrychidella	Klimesch, 1937
Leucoptera spartifoliella	(Hübner, 1813)
Leucorrhinia caudalis	(Charpentier, 1840)
Leucorrhinia pectoralis	(Charpentier, 1825)
Leucospilapteryx omissella	(Stainton, 1848)
Leucozona lucorum	(Linnaeus, 1758)
Leuctra albida	Kempny, 1899
Leuctra autumnalis	Aubert, 1948
Leuctra braueri	Kempny, 1898
Leuctra carpathica	Kis, 1966
Leuctra digitata	Kempny, 1899
Leuctra fusca	(Linnaeus, 1758)
Leuctra hippopus	Kempny, 1899
Leuctra inermis	Kempny, 1899
Leuctra major	Brinck, 1949
Leuctra mortoni	Kempny, 1899
Leuctra nigra	(Olivier, 1811)
Leuctra prima	Kempny, 1899
Leuctra pseudosignifera	Aubert, 1954
Leviellus thorelli	(Ausserer, 1871)
Leydigia acanthocercoides	(Fischer, 1854)
Leydigia leydigi	(Schoedler, 1863)
Libelloides macaronius	(Scopoli, 1763)
Libellula depressa	Linné, 1758
Libellula fulva	Müller, 1764
Libellula quadrimaculata	Linné, 1758
Libythea celtis	(Laicharting, 1782)
Lichenophanes varius	(Illiger, 1801)
Lichtensia viburni	Signoret,1873
Licinus cassideus	(Fabricius, 1792)
Licinus depressus	(Paykull, 1790)
Licinus hoffmanseggii	(Panzer, 1803)
Ligdia adustata	(Denis & Schiffermüller, 1775)
Lignyodes bischoffi	(Blatchley & Leng, 1916)
Lignyodes enucleator	(Panzer, 1798)
Lignyodes suturatus	Fairmaire, 1859
Lignyoptera fumidaria	(Hübner, 1825)
Limacus flavus	(Linnaeus, 1758)
Limarus maculatus	(Sturm, 1800)
Limax cinereoniger	Wolf, 1803
Limax maximus	Linnaeus, 1758
Limenitis camilla	(Linnaeus, 1764)
Limenitis populi	(Linnaeus, 1758)
Limenitis reducta	Staudinger, 1901
Limicola falcinellus	(Pontoppidan, 1763)
Limnadia lenticularis	(Linnaeus, 1761)
Limnaecia phragmitella	Stainton, 1851
Limnebius aluta	Bedel, 1881
Limnebius atomus	(Duftschmid, 1805)
Limnebius crinifer	Rey, 1885
Limnebius nitidus	(Marsham, 1802)
Limnebius papposus	(Mulsant, 1844)
Limnebius parvulus	(Herbst, 1797)
Limnebius stagnalis	(Guillebeau, 1890)
Limnebius truncatellus	Thunberg, 1794
Limnephilus affinis	Curtis, 1834
Limnephilus auricula	Curtis, 1834
Limnephilus bipunctatus	Curtis, 1834
Limnephilus centralis	Curtis, 1834
Limnephilus decipiens	(Kolenati, 1848)
Limnephilus elegans	Curtis, 1834
Limnephilus extricatus	McLachlan, 1865
Limnephilus flavicornis	(Fabricius, 1787)
Limnephilus fuscicornis	Rambur, 1842
Limnephilus griseus	(Linnaeus, 1758)
Limnephilus hirsutus	(Pictet, 1834)
Limnephilus ignavus	McLachlan, 1865
Limnephilus incisus	Curtis, 1834
Limnephilus lunatus	Curtis, 1834
Limnephilus nigriceps	(Zetterstedt, 1840)
Limnephilus politus	McLachlan, 1865
Limnephilus rhombicus	(Linnaeus, 1758)
Limnephilus sparsus	Curtis, 1834
Limnephilus stigma	Curtis, 1834
Limnephilus subcentralis	Brauer, 1857
Limnephilus tauricus	Schmid, 1964
Limnephilus vittatus	(Fabricius, 1798)
Limnephilus borealis	(Zetterstedt, 1840)
Limnichus incanus	Kiesenwetter, 1851
Limnichus pygmaeus	(Sturm, 1807)
Limnichus sericeus	(Duftschmid, 1825)
Limnius intermedius	Fairmaire, 1881
Limnius muelleri	(Erichson, 1847)
Limnius perrisi	(Dufour, 1843)
Limnius volckmari	(Panzer, 1793)
Limnobaris dolorosa	(Goeze, 1777)
Limnobaris t-album	(Linnaeus, 1758)
Limnocamptus hoferi	(Van Douwe, 1907)
Limnocythere inopinata	(Baird, 1843)
Limnocythere sanctipatricii	(Brady & Robertson, 1869)
Limnodromus scolopaceus	(Say, 1823)
Limnomysis benedeni	Czerniavsky, 1882
Limnoporus rufoscutellatus	(Latreille, 1807)
Limnoxenus niger	Zschach, 1788
Limobius borealis	(Paykull, 1792)
Limoniscus violaceus	(P.W.J. Müller, 1821)
Limonius minutus	(Linnaeus, 1758)
Limosa lapponica	(Linnaeus, 1758)
Limosa limosa	(Linnaeus, 1758)
Limotettix striola	(Fallén, 1806)
Lindenius albilabris	(Fabricius, 1793)
Lindenius laevis	Costa, 1871
Lindenius mesopleuralis	(Morawitz, 1890)
Lindenius panzeri	(Vander Linden, 1829)
Lindenius parkanensis	Zavadil, 1948
Lindenius pygmaeus	(Rossi, 1794)
Lindenius pygmaeus armatus	(Rossi, 1794)
Linnavuoriana decempunctata	(Fallén, 1806)
Linnavuoriana sexmaculata	(Hardy, 1850)
Linyphia hortensis	Sundevall, 1830
Linyphia tenuipalpis	Simon, 1884
Linyphia triangularis juniperina	(Clerck, 1757)
Protaetia lugubris	(Herbst, 1786)
Liocoris tripustulatus	(Fabricius, 1781)
Liocranoeca striata gracilior	(Kulczynski, 1882)
Liocranum rupicola	(Walckenaer, 1830)
Sagana rutilans	Thorell, 1875
Liocyrtusa minuta	(Ahrens, 1812)
Liocyrtusa nigriclavis	(Hlisnikovsky, 1967)
Liocyrtusa vittata	(Curtis, 1840)
Lioderina linearis	(Hampe, 1870)
Liogluta alpestris	(Heer, 1839)
Liogluta granigera	(Kiesenwetter, 1850)
Liogluta longiuscula	(Gravenhorst, 1802)
Liogluta microptera	Thomson, 1867
Liogluta pagana	(Erichson, 1839)
Liometopum microcephalum	(Panzer, 1798)
Lionychus quadrillum	(Duftschmid, 1812)
Liophloeus gibbus	Boheman, 1842
Liophloeus lentus	Germar, 1824
Liophloeus liptoviensis	J. Weise, 1894
Liophloeus pupillatus	Apfelbeck, 1928
Liophloeus tessulatus	(O. F. Müller, 1776)
Liorhyssus hyalinus	(Fabricius, 1794)
Liothorax niger	(Panzer, 1797)
Liothorax plagiatus	(Linnaeus, 1767)
Liparthrum bartschti	Mühl, 1891
Liparus coronatus	(Goeze, 1777)
Liparus dirus	(Herbst, 1795)
Liparus germanus	(Linnaeus, 1758)
Liparus glabrirostris	(Küster, 1849)
Liparus transsylvanicus	Petri, 1895
Liris nigra	(Fabricius, 1775)
Lissodema cursor	(Gyllenhal, 1813)
Lissodema denticolle	(Gyllenhal, 1813)
Litargus balteatus	Leconte, 1856
Litargus connexus	(Fourcroy, 1785)
Lithax niger	(Hagen, 1859)
Lithax obscurus	(Hagen, 1859)
Lithobius aeruginosus	L.Koch, 1862
Lithobius aeruginosus batorligetensis	L.Koch, 1862
Lithobius agilis	C.Koch, 1847
Lithobius agilis pannonicus	C.Koch, 1847
Lithobius agilis tricalcaratus	C.Koch, 1847
Lithobius austriacus	Verhoeff, 1937
Lithobius borealis	Meinert, 1868
Lithobius crassipes	L.Koch, 1862
Lithobius curtipes	C.L.Koch, 1847
Lithobius dentatus	C.Koch, 1847
Lithobius erythrocephalus	C.L.Koch, 1847
Lithobius erythrocephalus schuleri	C.L.Koch, 1847
Lithobius forficatus	(Linnaeus, 1758)
Lithobius lapidicola	Meinert, 1872
Lithobius luteus	Loksa, 1947
Lithobius macilentus	L.Koch, 1862
Lithobius melanops	Newport, 1845
Lithobius microps	Meinert, 1872
Lithobius mutabilis	C.L.Koch, 1862
Lithobius mutabilis carpathicus	C.L.Koch, 1862
Lithobius muticus	C.L.Koch, 1847
Lithobius nodulipes	Latzel, 1880
Lithobius nodulipes scarabanciae	Latzel, 1880
Lithobius parietum	Verhoeff, 1899
Lithobius parietum mecsekensis	Verhoeff, 1899
Lithobius pelidnus	Haase, 1880
Lithobius piceus	L.Koch, 1862
Lithobius punctulatus	C.Koch, 1847
Lithobius pusillus	Latzel, 1880
Lithobius pusillus novemaculatus	Latzel, 1880
Lithobius stygius	Verhoeff, 1937
Lithobius stygius infernus	Verhoeff, 1937
Lithobius tenebrosus	Meinert, 1872
Lithobius tenebrosus sulcatipes	Meinert, 1872
Lithobius tricuspis	Meinert, 1872
Lithocharis nigriceps	Kraatz, 1859
Lithocharis ochracea	(Gravenhorst, 1802)
Lithoglyphus naticoides	(C. Pfeiffer, 1828)
Lithophane consocia	(Borkhausen, 1792)
Lithophane furcifera	(Hufnagel, 1766)
Lithophane ornitopus	(Hufnagel, 1766)
Lithophane semibrunnea	(Haworth, 1809)
Lithophane socia	(Hufnagel, 1766)
Lithosia quadra	(Linnaeus, 1758)
Lithostege farinata	(Hufnagel, 1767)
Lithostege griseata	(Denis & Schiffermüller, 1775)
Lithurgus chrysurus	Fonscolombe, 1834
Lithurgus cornutus	(Fabricius, 1787)
Lithurgus cornutus fuscipennis	(Fabricius, 1787)
Lixus albomarginatus	Boheman, 1843
Lixus angustatus	(Fabricius, 1775)
Lixus angustus	(Herbst, 1795)
Lixus apfelbecki	Petri, 1904
Lixus bardanae	(Fabricius, 1787)
Lixus bituberculatus	Smreczyński, 1968
Lixus brevipes	Ch. Brisout, 1866
Lixus cardui	Olivier, 1808
Lixus cribricollis	Boheman, 1836
Lixus cylindrus	(Fabricius, 1781)
Lixus elegantulus	Boheman, 1843
Lixus euphorbiae	Capiomont, 1874
Lixus fasciculatus	Boheman, 1836
Lixus filiformis	(Fabricius, 1781)
Lixus iridis	Olivier, 1807
Lixus lateralis	(Panzer, 1793)
Lixus myagri	Olivier, 1807
Lixus neglectus	Fremuth, 1983
Lixus ochraceus	Boheman, 1843
Lixus paraplecticus	(Linnaeus, 1758)
Lixus punctirostris	Boheman, 1843
Lixus punctiventris	Boheman, 1836
Lixus rubicundus	Zoubkow, 1833
Lixus scabricollis	Boheman, 1843
Lixus subtilis	Boheman, 1836
Lixus tibialis	Boheman, 1843
Lixus vilis	(Rossi, 1790)
Lobesia abscisana	(Doubleday, 1849)
Lobesia artemisiana	(Zeller, 1847)
Lobesia bicinctana	(Duponchel, 1844)
Lobesia botrana	(Denis & Schiffermüller, 1775)
Lobesia confinitana	(Staudinger, 1870)
Lobesia euphorbiana	(Freyer, 1842)
Lobesia reliquana	(Hübner, 1825)
Lobophora halterata	(Hufnagel, 1767)
Lobrathium multipunctum	(Gravenhorst, 1802)
Locusta migratoria	(Linne, 1758)
Locustella fluviatilis	(Wolf, 1810)
Locustella luscinioides	(Savi, 1824)
Locustella naevia	(Boddaert, 1783)
Lomaspilis marginata	(Linnaeus, 1758)
Lomaspilis opis	(Butler, 1878)
Lomechusa emarginata	(Paykull, 1789)
Lomechusa paradoxa	Gravenhorst, 1806
Lomechusa pubicollis	Ch.Brisout de Barneville, 1860
Lomechusoides strumosus	(Fabricius, 1792)
Lomographa bimaculata	(Fabricius, 1775)
Lomographa temerata	(Denis & Schiffermüller, 1775)
Longicoccus festucae	(Koteja,1971)
Longicoccus psammophilus	(Koteja,1971)
Lopheros rubens	(Gyllenhal, 1817)
Lophomma punctatum	(Blackwall, 1841)
Lopinga achine	(Scopoli, 1763)
Lopus decolor	(Fallén, 1807)
Loraphodius suarius	(Faldermann, 1836)
Loraspis frater	(Mulsant et Rey, 1870)
Lordithon bimaculatus	(Schrank, 1798)
Lordithon exoletus	(Erichson, 1839)
Lordithon lunulatus	(Linnaeus, 1761)
Lordithon pulchellus	(Mannerheim, 1830)
Lordithon speciosus	(Erichson, 1839)
Lordithon thoracicus	(Fabricius, 1776)
Lordithon trimaculatus	(Fabricius, 1792)
Lordithon trinotatus	(Erichson, 1839)
Loricera pilicornis	(Fabricius, 1775)
Loricula elegantula	(Bärensprung, 1858)
Loricula pselaphiformis	Curtis, 1833
Loricula ruficeps	(Reuter, 1884)
Lota lota	(Linnaeus, 1758)
Lovenula alluaudi	de Guerne & Richard, 1890
Loxia curvirostra	Linnaeus, 1758
Loxia leucoptera	Gmelin, 1789
Loxia leucoptera bifasciata	Gmelin, 1789
Loxostege aeruginalis	(Hübner, 1796)
Loxostege deliblatica	Szent-Ivány & Uhrik-Meszáros, 1942
Loxostege manualis	(Geyer, 1832)
Loxostege sticticalis	(Linnaeus, 1761)
Loxostege turbidalis	(Treitschke, 1829)
Celypha doubledayana	(Barrett, 1872)
Celypha lacunana	(Denis & Schiffermüller, 1775)
Celypha rivulana	(Scopoli, 1763)
Celypha siderana	(Treitschke, 1835)
Lozekia transsylvanica	(Westerlund, 1876)
Lozotaenia forsterana	(Fabricius, 1781)
Lucanus cervus	(Linnaeus, 1758)
Lucilla singleyana	(Pilsbry, 1890)
Lullula arborea	(Linnaeus, 1758)
Luperina pozzii	(Curo, 1883)
Luperina testacea	([Denis & Schiffermüller], 1775)
Luperina zollikoferi	(Freyer, 1836)
Luquetia lobella	(Denis & Schiffermüller, 1775)
Luscinia luscinia	(Linnaeus, 1758)
Luscinia megarhynchos	Ch. L. Brehm, 1831
Luscinia svecica	(Linnaeus, 1758)
Luscinia svecica cyanecula	(Linnaeus, 1758)
Lutra lutra	(Linnaeus, 1758)
Luzea graeca	(Kraatz, 1857)
Luzulaspis dactylis	Green,1928
Luzulaspis frontalis	Green,1928
Luzulaspis kosztarabi	Koteja et Kozár,1979
Luzulaspis luzulae	(Dufour,1864)
Luzulaspis nemorosa	Koteja,1966
Luzulaspis pieninnica	Koteja,1966
Luzulaspis rajae	Kozár,1981
Luzulaspis scotica	Green,1926
Lycaena alciphron	(Rottemburg, 1775)
Lycaena dispar	(Haworth, 1802)
Lycaena dispar rutila	(Haworth, 1802)
Lycaena helle	([Denis & Schiffermüller], 1775)
Lycaena hippothoe	(Linnaeus, 1761)
Lycaena phlaeas	(Linnaeus, 1761)
Lycaena thersamon	(Esper, 1784)
Lycaena tityrus	(Poda, 1761)
Lycaena virgaureae	(Linnaeus, 1758)
Lycia hanoviensis	Heymons, 1891
Lycia hirtaria	(Clerck, 1759)
Lycia pomonaria	(Hübner, 1790)
Lycia zonaria	(Denis & Schiffermüller, 1775)
Lycoperdina bovistae	(Fabricius, 1792)
Lycoperdina succincta	(Linnaeus, 1767)
Lycophotia porphyrea	([Denis & Schiffermüller], 1775)
Lycosa singoriensis	(Laxmann, 1770
Lyctocoris campestris	(Fabricius, 1794)
Lyctocoris dimidiatus	(Spinola, 1837)
Lyctus linearis	(Goeze, 1777)
Lyctus pubescens	Panzer, 1793
Lydus europeus	Escherich, 1896
Lydus trimaculatus	(Fabricius, 1787)
Lygaeosoma anatolicum	Seidenstücker, 1960
Lygaeosoma sardeum	Spinola, 1837
Lygaeus equestris	(Linnaeus, 1758)
Lygaeus simulans	Deckert, 1985
Lygephila craccae	([Denis & Schiffermüller], 1775)
Lygephila ludicra	(Hübner, 1790)
Lygephila lusoria	(Linnaeus, 1758)
Lygephila pastinum	(Treitschke, 1826)
Lygephila procax	(Hübner, 1813)
Lygephila viciae	(Hübner, 1822)
Lygistopterus sanguineus	(Linnaeus, 1758)
Lygocoris contaminatus	(Fallén, 1807)
Lygocoris pabulinus	(Linnaeus, 1761)
Neolygus viridis	(Fallén, 1807)
Lygus gemellatus	(Herrich-Schäffer, 1835)
Lygus pratensis	(Linnaeus, 1758)
Lygus punctatus	(Zetterstedt, 1839)
Lygus rugulipennis	Poppius, 1911
Lymantor coryli	(Perris, 1853)
Lymantria dispar	(Linnaeus, 1758)
Lymantria monacha	(Linnaeus, 1758)
Lymexylon navale	(Linnaeus, 1758)
Lymnaea stagnalis	(Linnaeus, 1758)
Lymnastis dieneri	(Székessy, 1938)
Lymnastis galilaeus	(Piochard de la Bruleire, 1876)
Lymnocryptes minimus	(Brünnich, 1764)
Lynceus brachyurus	O.F.Müller, 1776
Lynx lynx	(Linnaeus, 1758)
Lyonetia clerkella	(Linnaeus, 1758)
Lyonetia ledi	Wocke, 1859
Lyonetia prunifoliella	(Hübner, 1796)
Lype phaeopa	(Stephens, 1836)
Lype reducta	(Hagen, 1868)
Lyprocorrhe anceps	(Erichson, 1837)
Lypusa maurella	(Denis & Schiffermüller, 1775)
Lythria cruentaria	(Hufnagel, 1767)
Lythria purpuraria	(Linnaeus, 1758)
Lytta vesicatoria	(Linnaeus, 1758)
Macaria alternata	(Denis & Schiffermüller, 1775)
Macaria artesiaria	(Denis & Schiffermüller, 1775)
Macaria liturata	(Clerck, 1759)
Macaria notata	(Linnaeus, 1758)
Macaria signaria	(Hübner, 1809)
Macaria wauaria	(Linnaeus, 1758)
Macaroeris nidicolens	(Walckenaer, 1802)
Maccevethus errans caucasicus	(Fabricius, 1794)
Macdunnoughia confusa	(Stephens, 1850)
Macrargus rufus	(Wider, 1834)
Macratria hungarica	Hampe, 1884
Macrochilo cribrumalis	(Hübner, 1793)
Macrocyclops albidus	(Jurine, 1820)
Macrocyclops distinctus	(Richard, 1887)
Macrocyclops fuscus	(Jurine, 1820)
Macrogastra borealis	(O. Boettger, 1878)
Macrogastra borealis bielzi	(O. Boettger, 1878)
Macrogastra densestriata	(Rossmässler, 1836)
Macrogastra plicatula	(Draparnaud, 1801)
Macrogastra plicatula rusiostoma	(Draparnaud, 1801)
Macrogastra ventricosa	(Draparnaud, 1801)
Macroglossum stellatarum	(Linnaeus, 1758)
Macrolophus glaucescens	Fieber, 1858
Macrolophus melanotoma	(A. Costa, 1853)
Macrolophus pygmaeus	(Rambur, 1839)
Macronemurus bilineatus	Brauer, 1868
Macronychus quadrituberculatus	P.W.J. Müller, 1806
Macrophagus robustus	Motschulsky, 1845
Macropis frivaldszkyi	Mocsáry, 1878
Macropis fulvipes	(Panzer, 1804)
Macropis labiata	(Panzer, 1804)
Macroplax fasciata	(Herrich-Schäffer, 1835)
Macroplax preyssleri	(Fieber, 1836)
Macropsidius dispar	(Fieber, 1868)
Macropsidius sahlbergi	(Flor, 1861)
Macropsis cerea	(Germar, 1837)
Macropsis eleagni	Emeljanov, 1964
Macropsis fuscinervis	(Boheman, 1845)
Macropsis fuscula	(Zetterstedt, 1828)
Macropsis glandacea	(Fieber, 1868)
Macropsis graminea	(Fabricius, 1789)
Macropsis haupti	Wagner, 1941
Macropsis impura	(Boheman, 1847)
Macropsis infuscata	(J. Sahlberg, 1871)
Macropsis marginata	(Herrich-Schäffer, 1836)
Macropsis megerlei	(Fieber, 1868)
Macropsis mendax	(Fieber, 1868)
Macropsis notata	(Prohaska, 1923)
Macropsis prasina	(Boheman, 1852)
Macropsis scutellata	(Boheman, 1845)
Macropsis vestita	Ribaut, 1952
Macropsis vicina	(Horváth, 1897)
Macropsis viridinervis	Wagner, 1950
Macrosaldula scotica	(Curtis, 1835)
Macrosaldula variabilis	(Herrich-Schäffer, 1835)
Macrosiagon tricuspidatum	(Lepechin, 1785)
Macrosteles cristatus	(Ribaut, 1927)
Macrosteles fascifrons	(Stal, 1858)
Macrosteles fieberi	(Edwards, 1889)
Macrosteles forficulus	(Ribaut, 1927)
Macrosteles frontalis	(Scott, 1875)
Macrosteles horvathi	(Wagner, 1935)
Macrosteles laevis	(Ribaut, 1927)
Macrosteles maculosus	(Then, 1897)
Macrosteles oshanini	Razvyazkina, 1957
Macrosteles quadripunctulatus	(Kirschbaum, 1868)
Macrosteles sexnotatus	(Fallén, 1806)
Macrosteles sordidipennis	(Stal, 1858)
Macrosteles variatus	(Fallen, 1806)
Macrosteles viridigriseus	(Edwards, 1922)
Macrothrix hirsuticornis	Norman & Brady, 1867
Macrothrix laticornis	(Jurine, 1820)
Macrothrix rosea	(Jurine, 1820)
Macrothylacia rubi	(Linnaeus, 1758)
Macrotylus elevatus	Fieber, 1858
Macrotylus herrichi	Reuter, 1873
Macrotylus horvathi	Reuter, 1876
Macrotylus paykullii	(Fallén, 1807)
Macrotylus solitarius	(Meyer-Dür, 1843)
Glaucopsyche alcon	(Denis & Schiffermüller, 1775)
Glaucopsyche arion	(Linnaeus, 1758)
Glaucopsyche arion ligurica	(Wagner, 1904)
Glaucopsyche nausithous	(Bergsträsser, 1779)
Glaucopsyche teleius	(Bergsträsser, 1779)
Macustus grisescens	(Zetterstedt, 1828)
Magdalis armigera	(Fourcroy, 1785)
Magdalis barbicornis	(Latreille, 1804)
Magdalis carbonaria	(Linnaeus, 1758)
Magdalis caucasica	(Tournier, 1872)
Magdalis cerasi	(Linnaeus, 1758)
Magdalis duplicata	Germar, 1818
Magdalis exarata	(Ch. Brisout, 1862)
Magdalis flavicornis	(Gyllenhal, 1836)
Magdalis frontalis	(Gyllenhal, 1827)
Magdalis fuscicornis	Desbrochers, 1870
Magdalis linearis	(Gyllenhal, 1827)
Magdalis memnonia	(Gyllenhal, 1837)
Magdalis nitida	(Gyllenhal, 1827)
Magdalis nitidipennis	(Boheman, 1843)
Magdalis opaca	Reitter, 1895
Magdalis phlegmatica	(Herbst, 1797)
Magdalis rufa	Germar, 1824
Magdalis ruficornis	(Linnaeus, 1758)
Magdalis violacea	(Linnaeus, 1758)
Malachius aeneus	(Linnaeus, 1758)
Malachius bipustulatus	(Linnaeus, 1758)
Malachius rubidus	Erichson, 1840
Malachius scutellaris	Erichson, 1840
Malacocoris chlorizans	(Panzer, 1794)
Malacolimax tenellus	(O.F. Müller, 1774)
Malacosoma castrense	(Linnaeus, 1758)
Malacosoma neustria	(Linnaeus, 1758)
Maladera holosericea	(Scopoli, 1772)
Macrocerus demissus	Kiesenwetter, 1863
Macrocerus nigrinus	(Shaufuss, 1866)
Mallota cimbiciformis	(Fallén, 1817)
Mallota fuciformis	(Fabricius, 1794)
Malthinus balteatus	Suffrian, 1851
Malthinus biguttatus	(Linnaeus, 1758)
Malthinus facialis	C.G. Thomson, 1864
Malthinus fasciatus	(Olivier, 1790)
Malthinus flaveolus	(Herbst, 1786)
Malthinus frontalis	(Marsham, 1802)
Malthinus glabellus	Kiesenwetter, 1852
Malthinus seriepunctatus	Kiesenwetter, 1851
Malthinus turcicus	Pic, 1899
Malthodes brevicollis	(Paykull, 1798)
Malthodes caudatus	Weise, 1892
Malthodes crassicornis	(Mäklin, 1846)
Malthodes debilis	Kiesenwetter, 1852
Malthodes dieneri	Kaszab, 1955
Malthodes dimidiaticollis	(Rosenhauer, 1847)
Malthodes dispar	(Germar, 1824)
Malthodes fibulatus	Kiesenwetter, 1852
Malthodes flavoguttatus	Kiesenwetter, 1852
Malthodes fuscus	(Waltl, 1838)
Malthodes guttifer	Kiesenwetter, 1852
Malthodes hexacanthus	Kiesenwetter, 1852
Malthodes holdhausi	Kaszab, 1955
Malthodes lobatus	Kiesenwetter, 1852
Malthodes marginatus	(Latreille, 1806)
Malthodes maurus	(Laporte de Castelnau, 1840)
Malthodes minimus	(Linnaeus, 1758)
Malthodes mysticus	Kiesenwetter, 1852
Malthodes pumilus	(Brébisson, 1835)
Malthodes spathifer	Kiesenwetter, 1852
Malthodes spretus	Kiesenwetter, 1852
Malvaevora timida	(Rossi, 1792)
Malvapion malvae	(Fabricius, 1775)
Mamestra brassicae	(Linnaeus, 1758)
Manda mandibularis	(Gyllenhal, 1827)
Mangora acalypha	(Walckenaer, 1802)
Manica rubida	(Latreille, 1802)
Maniola jurtina	(Linnaeus, 1758)
Mansuphantes mansuetus	(Thorell, 1875)
Mantis religiosa	Linne, 1758
Mantispa aphavexelte	U. Aspöck & H. Aspöck, 1994
Mantispa perla	(Pallas, 1772)
Mantispa styriaca	(Poda, 1761)
Marasmarcha lunaedactyla	(Haworth, 1811)
Margarinotus bipustulatus	(Schrank, 1781)
Margarinotus brunneus	(Fabricius, 1775)
Margarinotus carbonarius	(Hoffmann, 1803)
Margarinotus ignobilis	(Marseul, 1854)
Margarinotus marginatus	(Erichson, 1834)
Margarinotus merdarius	(Hoffmann, 1803)
Margarinotus neglectus	(Germar, 1813)
Margarinotus obscurus	(Kugelann, 1792)
Margarinotus punctiventer	(Marseul, 1854)
Margarinotus purpurascens	(Herbst, 1792)
Margarinotus ruficornis	(Grimm, 1852)
Margarinotus striola	Sahlberg, 1834
Margarinotus striola succicola	Sahlberg, 1834
Margarinotus terricola	(Germar, 1824)
Margarinotus ventralis	(Marseul, 1854)
Marmaronetta angustirostris	(Ménétries, 1832)
Marmaropus besseri	Gyllenhal, 1837
Maro minutus	O.P.-Cambridge, 1906
Marpissa muscosa	(Clerck, 1757)
Marpissa nivoyi	(Lucas, 1846)
Marpissa pomatia	(Walckenaer, 1802)
Marpissa radiata	(Grube, 1859)
Martes foina	(Erxleben, 1777)
Martes martes	(Linnaeus, 1758)
Marthamea vitripennis	(Burmeister, 1839)
Megachile dorsalis	Pérez, 1879
Marumba quercus	([Denis & Schiffermüller], 1775)
Maso sundevalli	(Westring, 1851)
Masoreus wetterhallii	(Gyllenhal, 1813)
Mastigona bosniensis	(Verhoeff, 1897)
Mastigona vihorlatica	(Attems, 1899)
Mastigusa arietina	(Thorell, 1871)
Mastigusa macrophthalma	(Kulczynski, 1897)
Mastus bielzi	(M. von Kimakowicz, 1890)
Matratinea rufulicaput	Sziraki & Szöcs, 1990
Matsucoccus matsumurae	(Kuwana,1905)
Mecaspis alternans	(Herbst, 1795)
Mecaspis striatella	(Fabricius, 1792)
Mecinus caucasicus	(Reitter, 1907)
Mecinus circulatus	(Marsham, 1802)
Mecinus collaris	Germar, 1821
Mecinus heydeni	Wencker, 1866
Mecinus ictericus	(Gyllenhal, 1838)
Mecinus janthinus	(Germar, 1817)
Mecinus labilis	(Herbst, 1795)
Mecinus pascuorum	(Gyllenhal, 1813)
Mecinus pirazzolii	(Stierlin, 1867)
Mecinus plantaginis	(Eppelsheim, 1875)
Mecinus pyraster	(Herbst, 1795)
Meconema meridionale	Costa, 1860
Meconema thalassinum	(De Geer, 1773)
Mecopisthes silus	(O.P.-Cambridge, 1872)
Mecorhis ungarica	(Herbst, 1784)
Stethophyma grossum	(Linnaeus, 1758)
Mecyna flavalis	(Denis & Schiffermüller, 1775)
Mecyna lutealis	(Duponchel, 1833)
Mecyna trinalis	(Denis & Schiffermüller, 1775)
Mecynotarsus serricornis	(Panzer, 1796)
Mediterranea depressa	(Sterki, 1880)
Mediterranea hydatina	(Rossmässler, 1838)
Mediterranea inopinata	(Uličný, 1887)
Medon apicalis	(Kraatz, 1857)
Medon brunneus	(Erichson, 1839)
Medon castaneus	(Gravenhorst, 1802)
Medon dilutus	(Erichson, 1839)
Medon ferrugineus	(Erichson, 1840)
Medon fusculus	(Mannerheim, 1830)
Medon ripicola	(Kraatz, 1854)
Medon rufiventris	(Nordmann, 1837)
Megachile albisecta	(Klug, 1817)
Megachile apicalis	Spinola, 1808
Megachile bicoloriventris	Mocsáry, 1878
Megachile bombycina	Radoszkovski, 1874
Megachile centuncularis	(Linnaeus, 1758)
Megachile circumcincta	(Kirby, 1802)
Megachile deceptoria	Pérez, 1890
Megachile flabellipes	Pérez, 1895
Megachile genalis	Morawitz, 1880
Megachile lagopoda	(Linnaeus, 1761)
Megachile leachella	Curtis, 1828
Megachile leucomalla	Gerstaecker, 1869
Megachile lignisca	(Kirby, 1802)
Megachile maacki	Radoszkowski, 1874
Megachile maritima	(Kirby, 1802)
Megachile melanopyga	Costa, 1863
Megachile nigriventris	Schenck, 1868
Megachile octosignata	Nylander, 1852
Megachile pilicrus	Morawitz, 1877
Megachile pilidens	Alfken, 1924
Megachile pyrenea	Pérez, 1890
Megachile rotundata	(Fabricius, 1787)
Megachile versicolor	Smith, 1844
Megachile willoughbiella	(Kirby, 1802)
Megacoelum beckeri	Fieber, 1870
Megacoelum infusum	(Herrich-Schäffer, 1839)
Megacraspedus binotella	(Duponchel, 1843)
Megacraspedus dolosellus	(Zeller, 1839)
Megacraspedus imparellus	(Fischer v. Röslerstamm, 1843)
Megacraspedus separatellus	(Fischer v. Röslerstamm, 1843)
Megacyclops gigas	(Claus, 1857)
Megacyclops latipes	(Lowndes, 1927)
Megacyclops viridis	(Jurine, 1820)
Megadelphax sordidulus	(Stal, 1853)
Megafenestra aurita	(Fischer, 1849)
Megalepthyphantes collinus	(L. Koch, 1872)
Megalepthyphantes nebulosus	(Sundevall, 1830)
Megalepthyphantes pseudocollinus	Saaristo, 1997
Megalinus glabratus	(Gravenhorst, 1802)
Megaloceroea recticornis	(Geoffroy, 1785)
Megalocoleus confusus	E. Wagner, 1958
Megalocoleus dissimilis	(Reuter, 1876)
Megalocoleus exsanguis	(Herrich-Schäffer, 1835)
Megalocoleus hungaricus	E. Wagner, 1944
Megalocoleus molliculus	(Fallén, 1829)
Megalocoleus tanaceti	(Fallén, 1807)
Megalomus hirtus	(Linnaeus, 1761)
Megalomus tortricoides	Rambur, 1842
Megalonotus antennatus	(Schilling, 1829)
Megalonotus chiragra	(Fabricius, 1787)
Megalonotus dilatatus	(Herrich-Schäffer, 1840)
Megalonotus emarginatus	Rey, 1888
Megalonotus hirsutus	Fieber, 1861
Megalonotus praetextatus	(Herrich-Schäffer, 1835)
Megalonotus sabulicola	(Thomson, 1870)
Megalophanes viciella	(Denis & Schiffermüller, 1775)
Megalotomus junceus	(Scopoli, 1763)
Megamelodes lequesnei	Wagner, 1963
Megamelodes quadrimaculatus	(Signoret, 1865)
Megamelus notulus	(Germar, 1830)
Meganephria bimaculosa	(Linnaeus, 1767)
Meganola albula	([Denis & Schiffermüller], 1775)
Meganola kolbi	(Daniel, 1935)
Meganola strigula	([Denis & Schiffermüller], 1775)
Meganola togatulalis	(Hübner, 1798)
Megapenthes lugens	(Redtenbacher, 1842)
Megaphyllum bosniense	(Verhoeff, 1897)
Megaphyllum bosniense cotinophilum	(Verhoeff, 1897)
Megaphyllum projectum	Verhoeff, 1894
Megaphyllum projectum dioritanum	Verhoeff, 1894
Megaphyllum projectum kochi	Verhoeff, 1894
Megaphyllum transsylvanicum	(Verhoeff, 1897)
Megaphyllum transsylvanicum transdanubicum	(Verhoeff, 1897)
Megaphyllum unilineatum	(C.L.Koch, 1838)
Megarthrus bellevoyei	Saulcy, 1862
Megarthrus denticollis	(Beck, 1817)
Megarthrus depressus	(Paykull, 1789)
Megarthrus hemipterus	(Illiger, 1794)
Megarthrus proseni	Schatzmayr, 1904
Megascolia maculata	(Drury, 1773)
Megascolia maculata flavifrons	(Drury, 1773)
Megasternum concinnum	(Marsham, 1802)
Megasyrphus erraticus	(Linnaeus, 1758)
Megatoma ruficornis	Aubé, 1866
Megatoma undata	(Linnaeus, 1758)
Megistopus flavicornis	(Rossi, 1790)
Megophthalmus scabripennis	Edwards, 1915
Megophthalmus scanicus	(Fallén, 1806)
Meioneta affinis	(Kulczynski, 1898)
Meioneta fuscipalpa	(C.L. Koch, 1836)
Meioneta mollis	(O.P.-Cambridge, 1871)
Meioneta rurestris	(C.L. Koch, 1836)
Meioneta saxatilis	(Blackwall, 1844)
Meioneta simplicitarsis	(Simon, 1884)
Melaleucus picturatus	(Schönherr, 1849)
Melampophylax nepos	(McLachlan, 1880)
Melanapion minimum	(Herbst, 1797)
Melanargia galathea	(Linnaeus, 1758)
Melanargia russiae	(Esper, 1783)
Melanchra persicariae	(Linnaeus, 1761)
Melandrya barbata	(Schaller, 1783)
Melandrya caraboides	(Linnaeus, 1761)
Melandrya dubia	(Schaller, 1783)
Melangyna barbifrons	(Fallén, 1817)
Melangyna compositarum	(Verrall, 1873)
Melangyna labiatarum	(Verrall, 1901)
Melangyna lasiophthalma	(Zetterstedt, 1843)
Melangyna quadrimaculata	(Verrall, 1873)
Melangyna umbellatarum	(Fabricius, 1794)
Melanimon tibiale	(Fabricius, 1781)
Melanitta fusca	(Linnaeus, 1758)
Melanitta nigra	(Linnaeus, 1758)
Melanobaris atramentaria	(Boheman, 1836)
Melanobaris carbonaria	(Boheman, 1836)
Melanobaris dalmatina	(H. Brisout, 1870)
Melanobaris laticollis	(Marsham, 1802)
Melanocorypha calandra	(Linnaeus, 1766)
Melanocoryphus albomaculatus	(Goeze, 1778)
Melanocoryphus tristrami	(Douglas & Scott, 1868)
Melanogaster aerosa	(Loew, 1843)
Melanogaster hirtella	(Loew, 1843)
Melanogaster nuda	(Macquart, 1829)
Melanogryllus desertus	(Pallas, 1771)
Melanoides tuberculatus	(O.F. Müller, 1774)
Melanophila acuminata	(De Geer, 1774)
Melanophthalma curticollis	(Mannerheim, 1844)
Melanophthalma distinguenda	(Comolli, 1837)
Melanophthalma fuscipennis	(Mannerheim, 1844)
Melanophthalma maura	Motschulsky, 1866
Melanophthalma suturalis	(Mannerheim, 1844)
Melanophthalma taurica	(Mannerheim, 1844)
Melanopsis parreyssii	(Philippi, 1847)
Melanostoma dubium	(Zetterstedt, 1837)
Melanostoma mellinum	(Linnaeus, 1758)
Melanostoma scalare	(Fabricius, 1794)
Melanotus brunnipes	(Germar, 1824)
Melanotus castanipes	(Paykull, 1800)
Melanotus crassicollis	(Erichson, 1841)
Melanotus punctolineatus	(Pelerin, 1829)
Melanotus tenebrosus	(Erichson, 1841)
Melanotus villosus	(Geoffroy, 1785)
Melanthia procellata	(Denis & Schiffermüller, 1775)
Melasis buprestoides	(Linnaeus, 1761)
Melecta albifrons	Forster, 1771
Melecta funeraria	Smith, 1854
Melecta luctuosa	(Scopoli, 1770)
Meles meles	(Linnaeus, 1758)
Meliboeus amethystinus	(Olivier, 1790)
Meliboeus graminis	(Panzer, 1789)
Meliboeus subulatus	(F. Morawitz, 1861)
Melicius cylindrus	(Boheman, 1838)
Meligethes acicularis	Brisout de Barneville, 1863
Meligethes aeneus	(Fabricius, 1775)
Meligethes assimilis	Sturm, 1845
Meligethes atramentarius	Förster, 1849
Meligethes atratus	(Olivier, 1790)
Meligethes bidens	Brisout de Barneville, 1863
Meligethes bidentatus	Brisout de Barneville, 1863
Meligethes brachialis	Erichson, 1845
Meligethes brevis	Sturm, 1845
Meligethes brunnicornis	Sturm, 1845
Meligethes buyssoni	Brisout de Barneville, 1882
Meligethes carinulatus	Förster 1849
Meligethes coeruleovirens	Förster, 1849
Meligethes coracinus	Sturm, 1845
Meligethes corvinus	Erichson, 1845
Meligethes czwalinai	Reitter, 1871
Meligethes denticulatus	(Heer, 1841)
Meligethes difficilis	(Heer, 1841)
Meligethes discoideus	Erichson, 1845
Meligethes distinctus	Sturm, 1845
Meligethes egenus	Erichson, 1845
Meligethes erichsoni	Brisout de Barneville, 1863
Meligethes exilis	Sturm, 1845
Meligethes flavimanus	Stephens, 1830
Meligethes gagathinus	Erichson, 1845
Meligethes haemorrhoidalis	Förster, 1849
Meligethes hoffmanni	Reitter, 1871
Meligethes incanus	Sturm, 1845
Meligethes jejunus	Reitter, 1913
Meligethes jelineki	Audisio, 1976
Meligethes kraatzi	Reitter, 1871
Meligethes kunzei	Erichson, 1845
Meligethes lepidii	Miller, 1852
Meligethes lugubris	Sturm, 1845
Meligethes matronalis	Audisio et Spornraft, 1990
Meligethes maurus	Sturm, 1845
Meligethes morosus	Erichson, 1845
Meligethes nanus	Erichson, 1845
Meligethes nigrescens	Stephens, 1830
Meligethes ochropus	Sturm, 1845
Meligethes ovatus	Sturm, 1845
Meligethes pedicularius	(Gyllenhal, 1809)
Meligethes persicus	Faldermann, 1837
Meligethes planiusculus	(Heer, 1841)
Meligethes rosenhaueri	Reitter, 1871
Meligethes rotundicollis	Brisout de Barneville, 1863
Meligethes ruficornis	(Marsham, 1802)
Meligethes serripes	(Gyllenhal, 1827)
Meligethes solidus	(Kugelann, 1794)
Meligethes subaeneus	Sturm, 1845
Meligethes submetallicus	Deville, 1908
Meligethes subrugosus	(Gyllenhal, 1808)
Meligethes sulcatus	Brisout de Barneville, 1863
Meligethes symphyti	(Heer, 1841)
Meligethes tristis	Sturm, 1845
Meligethes umbrosus	Sturm, 1845
Meligethes variolosus	Easton, 1964
Meligethes villosus	Brisout de Barneville, 1863
Meligethes viridescens	(Fabricius, 1787)
Meligramma cincta	(Fallén, 1817)
Meligramma guttata	(Fallén, 1817)
Meligramma triangulifera	(Zetterstedt, 1843)
Melinopterus consputus	(Creutzer, 1799)
Melinopterus prodromus	(Brahm, 1790)
Melinopterus pubescens	(Sturm, 1800)
Melinopterus punctatosulcatus	(Sturm, 1800)
Melinopterus reyi	(Reitter, 1892)
Melinopterus sphacelatus	(Panzer, 1798)
Meliscaeva auricollis	(Meigen, 1822)
Meliscaeva cinctella	(Zetterstedt, 1843)
Aphomia foedella	(Zeller, 1839)
Aphomia zelleri	Joannis, 1932
Melitaea athalia	(Rottemburg, 1775)
Melitaea aurelia	Nickerl, 1850
Melitaea britomartis	Assmann, 1847
Melitaea cinxia	(Linnaeus, 1758)
Melitaea diamina	(Lang, 1789)
Melitaea didyma	(Esper, 1778)
Melitaea phoebe	([Denis & Schiffermüller], 1775)
Melitaea punica	Oberthür, 1876
Melitaea punica telona	Oberthür, 1876
Melitaea trivia	([Denis & Schiffermüller], 1775)
Melitta budensis	(Mocsáry, 1878)
Melitta dimidiata	Morawitz, 1876
Melitta haemorrhoidalis	(Fabricius, 1775)
Melitta leporina	(Panzer, 1799)
Melitta moczari	Tanács, 1985
Melitta nigricans	Alfken, 1905
Melitta tricincta	Kirby, 1802
Melitturga clavicornis	(Latreille, 1806)
Mellinus arvensis	(Linnaeus, 1758)
Mellinus crabroneus	(Thunberg, 1791)
Meloe autumnalis	Manuel, 1792
Meloe brevicollis	Panzer, 1793
Meloe cicatricosus	Leach, 1811
Meloe decorus	Brandt et Erichson, 1832
Meloe hungarus	Schrank, 1776
Meloe mediterraneus	J. Müller, 1926
Meloe proscarabaeus	Linnaeus, 1758
Meloe rufiventris	Germar, 1817
Meloe rugosus	Marsham, 1802
Meloe scabriusculus	Brandt et Erichson, 1832
Meloe tuccius	(Rossi, 1792)
Meloe uralensis	Pallas, 1777
Meloe variegatus	Donovan, 1776
Meloe violaceus	Marsham, 1802
Melogona broelemanni	(Verhoeff, 1897)
Melogona broelemanni gebhardti	(Verhoeff, 1897)
Melogona transsilvanica	(Verhoeff, 1897)
Melogona transsilvanica hungarica	(Verhoeff, 1897)
Melolontha hippocastani	Fabricius, 1801
Melolontha melolontha	(Linnaeus, 1758)
Melolontha pectoralis	Megerle von Mühlfeld, 1812
Menaccarus arenicola	(Scholtz, 1846)
Perittia farinella	(Thunberg, 1794)
Perittia huemeri	(Traugott-Olsen, 1990)
Mendoza canestrinii	(Ninni, 1868)
Mendrausus pauxillus	(Fieber, 1869)
Menephilus cylindricus	(Herbst, 1784)
Menesia bipunctata	(Zoubkoff, 1829)
Meotica exilis	(Gravenhorst, 1806)
Meotica filiformis	(Motschulsky, 1860)
Meotica pallens	(L.Redtenbacher, 1849)
Meotica parasita	Mulsant et Rey, 1873
Meotica szeli	Ádám, 1987
Merdigera obscura	(O.F. Müller, 1774)
Mergellus albellus	(Linnaeus, 1758)
Mergus merganser	Linnaeus, 1758
Mergus serrator	Linnaeus, 1758
Meria tripunctata	(Rossi, 1790)
Meridiophila fascialis	(Hübner, 1796)
Mermitelocerus schmidtii	(Fieber, 1836)
Merodon aberrans	Egger, 1860
Merodon aeneus	Meigen, 1822
Merodon albifrons	Meigen, 1822
Merodon armipes	Rondani, 1843
Merodon auripes	Sack, 1913
Merodon avidus	(Rossi, 1790)
Merodon cinereus	(Fabricius, 1794)
Merodon clavipes	(Fabricius, 1781)
Merodon clunipes	Sack, 1913
Merodon constans	(Rossi, 1794)
Merodon equestris	(Fabricius, 1794)
Merodon nigritarsis	Rondani, 1845
Merodon ruficornis	Meigen, 1822
Merodon rufus	Meigen, 1838
Merodon tricinctus	Sack, 1913
Merops apiaster	Linnaeus, 1758
Merrifieldia baliodactylus	(Zeller, 1841)
Merrifieldia leucodactyla	(Denis & Schiffermüller, 1775)
Merrifieldia malacodactylus	(Zeller, 1847)
Merrifieldia tridactyla	(Linnaeus, 1758)
Mesagroicus obscurus	Boheman, 1840
Mesapamea didyma	(Esper, 1788)
Mesapamea secalis	(Linnaeus, 1758)
Mesembrius peregrinus	(Loew, 1846)
Mesiotelus annulipes	(Kulczynski, 1897)
Mesocoelopus niger	(P.W.J. Müller, 1821)
Mesocrambus candiellus	(Herrich-Schäffer, 1848)
Mesocyclops leuckarti	(Claus, 1857)
Mesogona acetosellae	([Denis & Schiffermüller], 1775)
Mesogona oxalina	(Hübner, 1803)
Mesoiulus paradoxus	Berlése, 1886
Mesoleuca albicillata	(Linnaeus, 1758)
Mesoligia furuncula	([Denis & Schiffermüller], 1775)
Mesoligia literosa	(Haworth, 1809)
Mesophleps silacella	(Hübner, 1796)
Mesosa curculionoides	(Linnaeus, 1761)
Mesosa nebulosa	(Fabricius, 1781)
Mesothes ferrugineus	(Mulsant et Rey, 1860)
Loborhynchapion amethystinum	(Miller, 1857)
Mesotrichapion punctirostre	(Gyllenhal, 1839)
Mesotrosta signalis	(Treitschke, 1829)
Mesotype didymata	(Linnaeus, 1758)
Mesotype parallelolineata	(Retzius, 1783)
Mesovelia furcata	Mulsant & Rey, 1852
Mesovelia thermalis	Horváth, 1915
Messor structor	(Latreille, 1798)
Meta menardi	(Latreille, 1804)
Metacantharis discoidea	(Ahrens, 1812)
Metacantharis clypeata	(Illiger, 1798)
Metacanthus meridionalis	A. Costa, 1838
Metachrostis dardouini	(Boisduval, 1840)
Metaclisa azurea	(Waltl, 1838)
Metacrambus carectellus	(Zeller, 1847)
Metacyclops gracilis	(Lilljeborg, 1853)
Metacyclops minutus	(Claus, 1863)
Metacyclops planus	(Gurney, 1909)
Metacypris cordata	(Brady & Robertson, 1870)
Metadenopus festucae	Sulc,1933
Metadonus distinguendus	Boheman, 1868
Metalampra cinnamomea	(Zeller, 1839)
Metalampra diminutella	(Rebel, 1931)
Metalimnus formosus	(Boheman, 1845)
Metalimnus steini	(Fieber, 1869)
Metanomus infuscatus	(Eschscholtz, 1829)
Metapterus caspicus	(Dohrn, 1863)
Metasia ophialis	(Treitschke, 1829)
Metatropis rufescens	(Herrich-Schäffer, 1835)
Metaxmeste phrygialis	(Hübner, 1796)
Metcalfa pruinosa	(Say, 1830)
Metellina mengei	(Blackwall, 1869)
Metellina merianae	(Scopoli, 1763)
Metellina segmentata	(Clerck, 1757)
Metendothenia atropunctana	(Zetterstedt, 1839)
Methocha ichneumonides	Latreille, 1809
Methorasa latreillei	(Duponchel, 1827)
Metoecus paradoxus	(Linnaeus, 1761)
Metopobactrus ascitus	(Kulczynski, 1894
Metopobactrus deserticola	Loksa, 1981
Metopobactrus prominulus	(O.P.-Cambridge, 1872)
Metopoplax origani	(Kolenati, 1845)
Metopsia similis	Zerche, 1998
Metreletus balcanicus	(Ulmer, 1920)
Metrioptera bicolor	(Philippi, 1830)
Metrioptera brachyptera	(Linne, 1761)
Metrioptera roeselii	(Hagenbach, 1822)
Metriotes lutarea	(Haworth, 1828)
Metropis inermis	Wagner, 1939
Metropis latifrons	(Kirschbaum, 1868)
Metropis mayri	Fieber, 1866
Metzneria aestivella	(Zeller, 1839)
Metzneria aprilella	(Herrich-Schäffer, 1854)
Metzneria artificella	(Herrich-Schäffer, 1861)
Metzneria ehikeella	Gozmány, 1954
Metzneria intestinella	(Mann, 1864)
Metzneria lappella	(Linnaeus, 1758)
Metzneria metzneriella	(Stainton, 1851)
Metzneria neuropterella	(Zeller, 1839)
Metzneria paucipunctella	(Zeller, 1839)
Metzneria santolinella	(Amsel, 1936)
Metzneria subflavella	Englert, 1974
Metzneria tristella	Rebel, 1901
Mezira tremulae	(Germar, 1822)
Mezium americanum	(Laporte De Castelnau, 1840)
Miarus ajugae	(Herbst, 1795)
Miarus banaticus	Reitter, 1907
Miarus monticola	Petri, 1912
Miarus ursinus	Abeille de Perrin, 1906
Micantulina stigmatipennis	(Mulsant et Rey, 1855)
Micaria albovittata	(Lucas, 1846)
Micaria dives	Lucas, 1846
Micaria formicaria	(Sundevall, 1832)
Micaria fulgens	(Walckenaer, 1802)
Micaria guttulata	(C.L. Koch, 1839)
Micaria nivosa	L. Koch, 1866
Micaria pulicaria	(Sundevall, 1832)
Micaria rossica	Thorell, 1875
Micaria silesiaca	L. Koch, 1875
Micaria sociabilis	Kulczynski, 1897
Micaria subopaca	Westring, 1862
Micilus murinus	(Kiesenwetter, 1843)
Micrambe abietis	(Paykull, 1798)
Micrambe bimaculata	(Panzer, 1798)
Micrambe lindbergorum	Bruce, 1934
Micrargus herbigradus	(Blackwall, 1854)
Micrargus laudatus	(O.P.-Cambridge, 1881)
Micrasema setiferum	(Pictet, 1834)
Micrelus ericae	(Gyllenhal, 1813)
Micridium vittatum	(Motschulsky, 1845)
Microcara testacea	(Linnaeus, 1767)
Microcyclops rubellus	(Lilljeborg, 1901)
Microcyclops varicans	(Sars, 1863)
Microdon devius	(Linnaeus, 1761)
Microdon eggeri	Mik, 1897
Microdon mutabilis	(Linnaeus, 1758)
Microdota aegra	(Heer, 1841)
Microdota amicula	(Stephens, 1832)
Microdota benickiella	(Brundin, 1948)
Microdota contractipennis	(G.Benick, 1970)
Microdota excisa	(Eppelsheim, 1883)
Microdota ganglbaueri	(Brundin, 1948)
Microdota indubia	(Sharp, 1869)
Microdota inquinula	(Gravenhorst, 1802)
Microdota liliputana	(Ch.Brisout de Barneville, 1860)
Microdota palleola	(Erichson, 1837)
Microdota pittionii	(Scheerpeltz, 1950)
Microdota subtilis	(W.Scriba, 1866)
Microdynerus exilis	(Herrich-Schaeffer, 1839)
Microdynerus longicollis	Morawitz, 1895
Microdynerus nugdunensis	(Saussure, 1856)
Microdynerus timidus	(Saussure, 1856)
Microhoria nectarina	(Panzer, 1795)
Microhoria unicolor	(Schmidt, 1842)
Microlestes corticalis	(Dufour, 1820)
Microlestes corticalis escorialensis	(Dufour, 1820)
Microlestes fissuralis	(Reitter, 1900)
Microlestes fulvibasis	(Reitter, 1900)
Microlestes maurus	(Sturm, 1827)
Microlestes minutulus	(Goeze, 1777)
Microlestes negrita	Wollaston, 1854
Microlestes plagiatus	(Duftschmid, 1812)
Microlestes schroederi	Holdhaus, 1912
Microlinyphia impigra	(O.P.-Cambridge, 1871)
Microlinyphia pusilla	(Sundevall, 1830)
Tettigometra diminuta	Matsumura, 1910
Micrommata virescens	(Clerck, 1757)
Micromus angulatus	(Stephens, 1836)
Micromus lanosus	(Zelený, 1962)
Micromus paganus	(Linnaeus, 1767)
Micromus variegatus	(Fabricius, 1793)
Micromys minutus	(Pallas, 1778)
Micronecta griseola	Horváth, 1899
Micronecta minutissima	(Linnaeus, 1758)
Micronecta poweri	(Douglas & Scott, 1869)
Micronecta pusilla	(Horváth, 1895)
Micronecta scholtzi	(Fieber, 1860)
Microneta viaria	(Blackwall, 1841)
Microon sahlbergi	(C. R. Sahlberg, 1835)
Micropeplus caelatus	Erichson, 1839
Micropeplus fulvus	Erichson, 1840
Micropeplus longipennis	Kraatz, 1859
Micropeplus marietti	Jacquelin du Val, 1857
Micropeplus porcatus	(Paykull, 1789)
Micropeplus tesserula	(Curtis, 1828)
Microplax interrupta	(Fieber, 1837)
Microplontus campestris	(Gyllenhal, 1837)
Microplontus edentulus	(Schultze, 1897)
Microplontus millefolii	(Schultze, 1897)
Microplontus molitor	(Gyllenhal, 1837)
Microplontus rugulosus	(Herbst, 1795)
Microplontus triangulum	(Boheman, 1845)
Microporus nigrita	(Fabricius, 1794)
Micropterix aruncella	(Scopoli, 1763)
Micropterix aureatella	(Scopoli, 1763)
Micropterix calthella	(Linnaeus, 1761)
Micropterix mansuetella	Zeller, 1844
Micropterix myrtetella	Zeller, 1850
Micropterix schaefferi	Heath, 1975
Micropterix tunbergella	(Fabricius, 1787)
Micropterna caesareica	Schmid, 1959
Micropterna lateralis	(Stephens, 1837)
Micropterna nycterobia	McLachlan, 1875
Micropterna sequax	McLachlan, 1875
Micropterna testacea	(Gmelin, 1798)
Micropterus salmoides	Lacepéde, 1802
Microptilium palustre	Kuntzen, 1914
Microptilium pulchellum	(Allibert, 1844)
Microrhagus emyi	(Rouget, 1855)
Microrhagus lepidus	(Rosenhauer, 1847)
Microrhagus palmi	Olexa, 1963
Microrhagus pygmaeus	(Fabricius, 1792)
Microscydmus nanus	(Schaum, 1844)
Microtus agrestis	(Linnaeus, 1761)
Microtus arvalis	(Pallas, 1779)
Microtus oeconomus	(Pallas, 1766)
Microtus oeconomus mehelyi	(Pallas, 1766)
Microtus subterraneus	(de Selys Longchamps, 1836)
Microvelia buenoi	Drake, 1920
Microvelia pygmaea	(Dufour, 1833)
Microvelia reticulata	(Burmeister, 1835)
Micrurapteryx kollariella	(Zeller, 1839)
Migneauxia crassiuscula	(Aubé, 1850)
Milesia crabroniformis	(Fabricius, 1775)
Milesia semiluctifera	(Villers, 1789)
Emberiza calandra	Linnaeus, 1758
Millieria dolosalis	(Heydenreich, 1851)
Miltochrista miniata	(Forster, 1771)
Holochelus aequinoctialis	(Herbst, 1790)
Holochelus nocturnus	Nonveiller, 1959
Holochelus pilicollis	(Gyllenhal, 1817)
Holochelus vernus	(Germar, 1823)
Milvus migrans	(Boddaert, 1783)
Milvus milvus	(Linnaeus, 1758)
Mimas tiliae	(Linnaeus, 1758)
Mimela aurata	(Fabricius, 1801)
Mimesa bicolor	(Jurine, 1807)
Mimesa brevis	Maidl, 1914
Mimesa bruxellensis	Bondroit, 1934
Mimesa caucasica	Maidl, 1914
Mimesa crassipes	Costa, 1871
Mimesa equestris	(Fabricius, 1804)
Mimesa lutaria	(Fabricius, 1787)
Mimetus laevigatus	(Keyserling, 1863)
Mimumesa atratina	(Morawitz, 1891)
Mimumesa beaumonti	(Lith, 1949)
Mimumesa dahlbomi	(Wesmael, 1852)
Mimumesa littoralis	(Bondriot, 1934)
Mimumesa unicolor	(Vander Linden, 1829)
Mimumesa wuestneii	(Faester, 1951)
Minetia adamczewskii	(Toll, 1956)
Minetia criella	(Treitschke, 1835)
Minetia crinitus	(Fabricius, 1798)
Minetia labiosella	(Hübner, 1810)
Minicia marginella	(Wider, 1834)
Miniopterus schreibersii	(Kuhl, 1817)
Minoa murinata	(Scopoli, 1763)
Minois dryas	(Scopoli, 1763)
Minucia lunaris	([Denis & Schiffermüller], 1775)
Minyops carinatus	(Linnaeus, 1767)
Minyriolus pusillus	(Wider, 1834)
Mirabella albifrons	(Fieber, 1879)
Miramella alpina	(Kollar, 1833)
Mirificarma cytisella	(Treitschke, 1833)
Mirificarma eburnella	(Denis & Schiffermüller, 1775)
Mirificarma lentiginosella	(Zeller, 1839)
Mirificarma maculatella	(Hübner, 1796)
Mirificarma mulinella	(Zeller, 1839)
Miris striatus	(Linnaeus, 1758)
Mirococcopsis elongatus	Borchsenius,1948
Mirococcopsis nagyi	Kozár,1981
Mirococcopsis stipae	Borchsenius,1949
Mirococcus inermis	(Hall,1925)
Miscophus ater	Lepeletier, 1845
Miscophus bicolor	Jurine, 1807
Miscophus concolor	Dahlbom, 1844
Miscophus helveticus	Kohl, 1884
Miscophus helveticus rubriventris	Kohl, 1884
Miscophus spurius	(Dahlbom, 1832)
Misgurnus fossilis	(Linnaeus, 1758)
Misumena vatia	(Clerck, 1757)
Ebrechtella tricuspidata	(Fabricius, 1775)
Tettigometra macrocephalus	Fieber, 1865
Mixodiaptomus kupelwieseri	(Brehm, 1907)
Mixodiaptomus lilljeborgi	(de Guerne et Richard, 1888)
Mixodiaptomus tatricus	(Wierzejski, 1883)
Mixtacandona transleithanica	(Löffler, 1960)
Mniotype adusta	(Esper, 1790)
Mniotype satura	([Denis & Schiffermüller], 1775)
Mocuellus collinus	(Boheman, 1850)
Mocuellus metrius	(Flor, 1861)
Mocuellus quadricornis	Dlabola, 1949
Mocydia crocea	(Herrich-Schäffer, 1837)
Mocydiopsis attenuata	(Germar, 1821)
Mocydiopsis intermedia	Remane, 1961
Mocydiopsis longicauda	Remane, 1961
Mocydiopsis monticola	Remane, 1961
Mocydiopsis parvicauda	Ribaut, 1939
Mocyta clientula	(Erichson, 1839)
Mocyta fungi	(Gravenhorst, 1806)
Mocyta fussi	(Bernhauer, 1908)
Mocyta negligens	(Mulsant et Rey, 1873)
Mocyta orbata	(Erichson, 1837)
Mocyta orphana	(Erichson, 1837)
Modicogryllus frontalis	(Fieber, 1844)
Moebelia penicillata	(Westring, 1851)
Mogulones abbreviatulus	(Fabricius, 1792)
Mogulones albosignatus	(Gyllenhal, 1837)
Mogulones amplipennis	(Schultze, 1896)
Mogulones andreae	(Germar, 1824)
Mogulones angulicollis	(Schultze, 1897)
Mogulones aratridens	(Schultze, 1896)
Mogulones asperifoliarum	(Gyllenhal, 1813)
Mogulones aubei	(Boheman, 1845)
Mogulones austriacus	(Ch. Brisout, 1869)
Mogulones beckeri	(Schultze, 1900)
Mogulones borraginis	(Fabricius, 1792)
Mogulones crucifer	(Pallas, 1776)
Mogulones cynoglossi	(Frauenfeld, 1866)
Mogulones diecki	(Ch. Brisout, 1870)
Mogulones dimidiatus	(I. Frivaldszky, 1865)
Mogulones euphorbiae	(Ch. Brisout, W. & S., 1866)
Mogulones geographicus	(Goeze, 1777)
Mogulones gibbicollis	(Schultze, 1896)
Mogulones hungaricus	(Ch. Brisout, 1869)
Mogulones javetii	(Ch. Brisout, 1869)
Mogulones larvatus	(Schultze, 1897)
Mogulones lineatus	(Gyllenhal, 1837)
Mogulones pallidicornis	Gougelet & H. Brisout, 1860
Mogulones pannonicus	(Hajóss, 1928)
Mogulones raphani	(Fabricius, 1792)
Mogulones sublineellus	(Ch. Brisout, 1869)
Mogulones venedicus	(J. Weise, 1879)
Mogulonoides radula	(Germar, 1824)
Mohelnaspis massiliensis	(Goux,1937)
Moina brachiata	(Jurine, 1820)
Moina macrocopa	(Straus, 1820)
Moina micrura	Kurz, 1874
Moina salina	Daday, 1888
Moina weismanni	Ishikawa, 1896
Molops elatus	(Fabricius, 1801)
Molops piceus	(Panzer, 1793)
Molops piceus austriacus	(Panzer, 1793)
Molorchus minor	(Linnaeus, 1758)
Moma alpium	(Osbeck, 1778)
Mompha bradleyi	Riedl, 1965
Mompha divisella	Herrich-Schaffer, 1854
Mompha epilobiella	(Denis & Schiffermüller, 1775)
Mompha idaei	(Zeller, 1839)
Mompha lacteella	(Stephens, 1834)
Mompha langiella	(Hübner, 1796)
Mompha locupletella	(Denis & Schiffermüller, 1775)
Mompha miscella	(Denis & Schiffermüller, 1775)
Mompha ochraceella	(Curtis, 1839)
Mompha propinquella	(Stainton, 1851)
Mompha raschkiella	(Zeller, 1838)
Mompha sturnipennella	(Treitschke, 1833)
Mompha subbistrigella	(Haworth, 1828)
Mompha terminella	(Humphreys & Westwood, 1845)
Monacha cartusiana	(O.F. Müller, 1774)
Monachoides incarnatus	(O.F. Müller, 1774)
Monachoides vicinus	(Rossmässler, 1842)
Monalocoris filicis	(Linnaeus, 1758)
Monanus signatus	(Frauenfeld, 1867)
Monochamus galloprovincialis	(Olivier, 1795)
Monochamus galloprovincialis pistor	(Olivier, 1795)
Monochamus rosenmuelleri	(Cederhjelm, 1798)
Monochamus saltuarius	Gebler, 1829
Monochamus sartor	(Fabricius, 1787)
Monochamus sutor	(Linnaeus, 1758)
Monochroa arundinetella	(Stainton, 1858)
Monochroa conspersella	(Herrich-Schäffer, 1854)
Monochroa cytisella	(Curtis, 1837)
Monochroa divisella	(Douglas, 1850)
Monochroa elongella	(Heinemann, 1870)
Monochroa hornigi	(Staudinger, 1883)
Monochroa lucidella	(Stephens, 1834)
Monochroa lutulentella	(Zeller, 1839)
Monochroa niphognatha	(Gozmány, 1953)
Monochroa nomadella	(Zeller, 1868)
Monochroa palustrellus	(Douglas, 1850)
Monochroa parvulata	Gozmány, 1957
Monochroa rumicetella	(O. Hofmann, 1868)
Monochroa sepicolella	(Herrich-Schäffer, 1854)
Monochroa servella	(Zeller, 1839)
Monochroa simplicella	(Lienig & Zeller, 1846)
Monochroa tenebrella	(Hübner, 1817)
Monomorium pharaonis	(Linnaeus, 1758)
Mononychus punctumalbum	(Herbst, 1784)
Monopis crocicapitella	(Clemens, 1859)
Monopis fenestratella	(Heyden, 1863)
Monopis imella	(Hübner, 1813)
Monopis laevigella	(Denis & Schiffermüller, 1775)
Monopis monachella	(Hübner, 1796)
Monopis obviella	(Denis & Schiffermüller, 1775)
Monopis weaverella	(Scott, 1858)
Monospilus dispar	Sars, 1862
Monosteira unicostata	(Mulsant & Rey, 1852)
Monosynamma bohemanni	(Fallén, 1829)
Monotoma angusticollis	Gyllenhal, 1827
Monotoma bicolor	Villa, 1835
Monotoma brevicollis	Aubé, 1837
Monotoma conicicollis	Guérin-Ménéville, 1844
Monotoma longicollis	Gyllenhal, 1827
Monotoma picipes	Herbst, 1793
Monotoma quadrifoveolata	Aubé, 1837
Monotoma spinicollis	Aubé, 1837
Monotoma testacea	Motschulsky, 1845
Montescardia tessulatellus	(Lienig & Zeller, 1846)
Monticola saxatilis	(Linnaeus, 1766)
Montifringilla nivalis	(Linnaeus, 1766)
Mordella aculeata	Linnaeus, 1758
Mordella adnexa	Ermisch, 1969
Mordella brachyura	Mulsant, 1856
Mordella holomelaena	Apfelbeck, 1914
Mordella huetheri	Ermisch, 1956
Mordella leucaspis	Küster, 1849
Mordella meridionalis	Méquignon, 1946
Mordella purpurascens	Apfelbeck, 1914
Mordella velutina panonica	Emery, 1876
Mordella viridescens	Costa, 1854
Mordellaria aurofasciata	(Comoli, 1837)
Mordellistena acuticollis	Schilsky, 1895
Mordellistena aequalica	Ermisch, 1977
Mordellistena angustula	Ermisch, 1977
Mordellistena apicerufa	Ermisch, 1977
Mordellistena bicoloripilosa	Ermisch, 1967
Mordellistena biroi	Ermisch, 1977
Mordellistena brevicauda	(Bohemann, 1849)
Mordellistena brunneispinosa	Ermisch, 1963
Mordellistena confinis	Costa, 1854
Mordellistena connata	Ermisch, 1969
Mordellistena consobrina	Ermisch, 1977
Mordellistena curticornis	Ermisch, 1977
Mordellistena csiki	Ermisch, 1977
Mordellistena dalmatica	Ermisch, 1956
Mordellistena dieckmanni	Ermisch, 1963
Mordellistena dives	Emery, 1876
Mordellistena dvoraki	Ermisch, 1956
Mordellistena erdoesi	Ermisch, 1977
Mordellistena exclusa	Ermisch, 1977
Mordellistena falsoparvula	Ermisch, 1956
Mordellistena falsoparvuliformis	Ermisch, 1963
Mordellistena feigei	Ermisch, 1956
Mordellistena festiva	Ermisch, 1977
Mordellistena flavospinulosa	Ermisch, 1977
Mordellistena fuscogemellatoides	Ermisch, 1977
Mordellistena horvathi	Ermisch, 1977
Mordellistena humeralis	(Linnaeus, 1758)
Mordellistena hungarica	Ermisch, 1977
Mordellistena intersecta	Emery, 1876
Mordellistena klapperichi	Ermisch, 1956
Mordellistena koelleri	Ermisch, 1956
Mordellistena kraatzi	Emery, 1876
Mordellistena lichtneckerti	Ermisch, 1977
Mordellistena magyarica	Ermisch, 1977
Mordellistena mediana	Ermisch, 1977
Mordellistena mediogemellata	Ermisch, 1977
Mordellistena meuseli	Ermisch, 1956
Mordellistena micans	(Germar, 1817)
Mordellistena micantoides	Ermisch, 1954
Mordellistena microscopica	Ermisch, 1977
Mordellistena mihoki	Ermisch, 1977
Mordellistena minutula	Ermisch, 1956
Mordellistena minutuloides	Ermisch, 1966
Mordellistena neglecta	Ermisch, 1977
Mordellistena neuwaldeggiana	(Panzer, 1796)
Mordellistena paraintersecta	Ermisch, 1956
Mordellistena paranana	Ermisch, 1977
Mordellistena pararhenana	Ermisch, 1977
Mordellistena parvula	(Gyllenhal, 1827)
Mordellistena parvuliformis	Scegoleva-Barovskaja, 1930
Mordellistena parvuloides	Ermisch, 1956
Mordellistena pentas	(Mulsant, 1856)
Mordellistena perparvula	Ermisch, 1966
Mordellistena pfefferi	Ermisch, 1963
Mordellistena pontica	Ermisch, 1977
Mordellistena pseudobrevicauda	Ermisch, 1963
Mordellistena pseudonana	Ermisch, 1956
Mordellistena pseudoparvula	Ermisch, 1956
Mordellistena pseudorhenana	Ermisch, 1977
Mordellistena pumila	(Gyllenhal, 1810)
Mordellistena purpureonigrans	Ermisch, 1963
Mordellistena pygmaeola	Ermisch, 1956
Mordellistena reichei	Emery, 1876
Mordellistena reitteri	Schilsky, 1894
Mordellistena rhenana	Ermisch, 1956
Mordellistena sajoi	Ermisch, 1977
Mordellistena secreta	Horak, 1983
Mordellistena semiferruginea	Reitter, 1911
Mordellistena stenidea	Mulsant, 1856
Mordellistena stoeckleini	Ermisch, 1956
Mordellistena subepisternalis	Ermisch, 1965
Mordellistena tarsata	Mulsant, 1856
Mordellistena thuringiaca	Ermisch, 1963
Mordellistena variegata	(Fabricius, 1798)
Mordellistena wankai	Ermisch, 1966
Mordellistena weisei	Schilsky, 1895
Mordellistena zoltani	Ermisch, 1977
Mordellistenula perrisi	(Mulsant, 1857)
Mordellistenula planifrons	Scegoleva-Barovskaja, 1930
Mordellochroa abdiminalis	(Fabricius, 1775)
Mordellochroa humerosa	(Rosenhauer, 1847)
Mordellochroa milleri	(Emery, 1876)
Mordellochroa tournieri	(Emery, 1876)
Morimus asper funereus	(Sulzer, 1767)
Morlina glabra	(Rossmässler, 1853)
Morlina glabra striaria	(Rossmässler, 1853)
Mormo maura	(Linnaeus, 1758)
Morophaga choragella	(Denis & Schiffermüller, 1775)
Morychus aeneus	(Fabricius, 1775)
Motacilla alba	Linnaeus, 1758
Motacilla cinerea	Tunstall, 1771
Motacilla citreola	Pallas, 1776
Motacilla citreola werae	Pallas, 1776
Motacilla flava	Linnaeus, 1758
Motacilla flava feldegg	Linnaeus, 1758
Motacilla flava thunbergi	Linnaeus, 1758
Muellerianella brevipennis	(Boheman, 1847)
Muellerianella extrusa	(Scott, 1871)
Muellerianella fairmairei	(Perris, 1857)
Muirodelphax aubei	(Perris, 1857)
Mughiphantes mughi	(Fickert, 1875)
Mus musculus	Linnaeus, 1758
Mus spicilegus	Petényi, 1882
Musaria affinis	(Harrer, 1784)
Musaria argus	(Frölich, 1793)
Muscardinus avellanarius	(Linnaeus, 1758)
Muscicapa striata	(Pallas, 1764)
Musculium lacustre	(O.F. Müller, 1774)
Mustela erminea	Linnaeus, 1758
Mustela eversmanii	Éhik, 1928
Mustela eversmanii hungarica	Lesson, 1827
Mustela nivalis vulgaris	Linnaeus, 1766
Mustela putorius	Linnaeus, 1758
Mutilla brutia	Petagna, 1787
Mutilla differens	Lepeletier, 1845
Mutilla europaea	Linnaeus, 1758
Algedonia terrealis	(Treitschke, 1829)
Myathropa florea	(Linnaeus, 1758)
Mycetaea subterranea	(Fabricius, 1801)
Mycetina cruciata	(Schaller, 1783)
Mycetochara axillaris	(Paykull, 1799)
Mycetochara flavipes	(Fabricius, 1792)
Mycetochara humeralis	(Fabricius, 1787)
Mycetochara linearis	(Illiger, 1794)
Mycetochara pygmaea	Redtenbacher, 1874
Mycetochara quadrimaculata	(Latreille, 1804)
Mycetochara roubali	Maran, 1935
Mycetoma suturale	(Panzer, 1797)
Mycetophagus ater	(Reitter, 1879)
Mycetophagus atomarius	Fabricius, 1792
Mycetophagus decempunctatus	Fabricius, 1801
Mycetophagus fulvicollis	Fabricius, 1792
Mycetophagus multipunctatus	Fabricius, 1792
Mycetophagus piceus	Fabricius, 1792
Mycetophagus populi	Fabricius, 1798
Mycetophagus quadriguttatus	J.P.W. Müller, 1821
Mycetophagus quadripustulatus	(Linnaeus, 1761)
Mycetophagus salicis	Brisout de Barneville, 1862
Mycetoporus aequalis	(Thomson, 1868)
Mycetoporus ambiguus	Luze, 1901
Mycetoporus bimaculatus	Lacordaire, 1835
Mycetoporus brucki	(Pandellé, 1869)
Mycetoporus clavicornis	(Stephens, 1832)
Mycetoporus corpulentus	Luze, 1901
Mycetoporus dispersus	Schülke & Kocian, 2000
Mycetoporus eppelsheimianus	Fagel, 1968
Mycetoporus erichsonanus	Fagel, 1965
Mycetoporus forticornis	Fauvel, 1875
Mycetoporus gracilis	Luze, 1901
Mycetoporus imperialis	Bernhauer, 1902
Mycetoporus lepidus	(Gravenhorst, 1802)
Mycetoporus longulus	(Mannerheim, 1830)
Mycetoporus mulsanti	Ganglbauer, 1895
Mycetoporus niger	(Fairmaire et Laboulbene, 1856)
Mycetoporus nigricollis	Stephens, 1835
Mycetoporus piceolus	Rey, 1883
Mycetoporus punctipennis	W.Scriba, 1868
Mycetoporus punctus	(Gravenhorst, 1806)
Mycetoporus rufescens	(Stephens, 1832)
Mycetoporus solidicornis	Wollaston, 1874
Mycetoporus solidicornis reichei	Wollaston, 1874
Mycetota fimorum	(Ch.Brisout de Barneville, 1860)
Mycetota laticollis	(Stephens, 1832)
Mychothenus minutus	(J. Frivaldszky, 1877)
Mycterodus confusus	Stal, 1861
Mycterodus immaculatus	(Fabricius, 1794)
Mycterus tibialis	Küster, 1850
Myelois circumvoluta	(Fourcroy, 1785)
Mylabris crocata	(Pallas, 1782)
Mylabris pannonica	Kaszab, 1956
Hycleus tenerus	(Germar, 1834)
Mylabris variabilis	(Pallas, 1782)
Myllaena brevicornis	(Matthews, 1838)
Myllaena dubia	(Gravenhorst, 1806)
Myllaena gracilis	(Matthews, 1838)
Myllaena infuscata	Kraatz, 1853
Myllaena intermedia	Erichson, 1837
Myllaena minuta	(Gravenhorst, 1806)
Mylopharyngodon piceus	(Richardson, 1846)
Myndus musivus	(Germar, 1825)
Myolepta dubia	(Fabricius, 1805)
Myolepta nigritarsis	Coe, 1957
Myolepta obscura	Becher, 1882
Myolepta potens	(Harris, 1780)
Myolepta vara	(Panzer, 1798)
Myotis alcathoe	von Helversen et Heller, 2001
Myotis bechsteinii	(Kuhl, 1818)
Myotis blythii oxygnathus	(Tomes, 1857)
Myotis brandtii	(Eversmann, 1845)
Myotis dasycneme	(Boie, 1825)
Myotis daubentonii	(Kuhl, 1817)
Myotis emarginatus	(Geoffroy, 1806)
Myotis myotis	(Borkhausen, 1797)
Myotis mystacinus	(Kuhl, 1819)
Myotis nattereri	(Kuhl, 1819)
Myrmarachne formicaria	(De Geer, 1778)
Myrmecaelurus trigrammus	(Pallas, 1771)
Myrmechixenus subterraneus	Chevrolat, 1835
Myrmechixenus vaporarium	Guérin-Méneville, 1843
Myrmecina graminicola	(Latreille, 1802)
Myrmecophilus acervorum	(Panzer, 1799)
Myrmecoris gracilis	(J. Sahlberg, 1848)
Myrmecozela ochraceella	(Tengstöm,1848)
Myrmedobia coleoptrata	(Fallén, 1807)
Myrmedobia exilis	(Fallén, 1807)
Myrmeleon bore	(Tjeder, 1941)
Myrmeleon formicarius	Linnaeus, 1767
Myrmeleon inconspicuus	Rambur, 1842
Myrmeleotettix antennatus	(Fieber, 1853)
Myrmeleotettix maculatus	(Thunberg, 1815)
Myrmetes paykulli	(Paykull, 1809)
Myrmica deplanata	Ruzsky, 1905
Myrmica gallienii	Bondroit, 1919
Myrmica lobicornis	(Nylander, 1846)
Myrmica microrubra	Seifert, 1993
Myrmica rubra	(Linnaeus, 1758)
Myrmica ruginodis	(Nylander, 1846)
Myrmica rugulosa	(Nylander, 1849)
Myrmica sabuleti	Meinert, 1861
Myrmica salina	Ruzsky 1905
Myrmica scabrinodis	(Nylander, 1846)
Myrmica schencki	Emery 1895
Myrmica specioides	Bondroit , 1918)
Myrmica sulcinodis	(Nylander, 1846)
Myrmilla calva	(Villers, 1789)
Myrmilla capitata	(Lucas, 1846)
Myrmilla glabrata	(Fabricius, 1775)
Myrmilla lezginica	(Radoszkowski, 1885)
Myrmilla mutica	(André, 1893)
Myrmoecia perezi	(Uhagon, 1876)
Myrmoecia plicata	(Erichson, 1837)
Myrmosa atra	Panzer, 1801
Myrmosa ephippium	(Fabricius, 1775)
Myrmosa melanocephala	(Fabricius, 1793)
Myrmus miriformis	(Fallén, 1807)
Myrrha octodecimguttata	(Linnaeus, 1758)
Mysmenella jobi	(Kraus, 1967)
Mystacides azureus	(Linnaeus, 1761)
Mystacides longicornis	(Linnaeus, 1758)
Mystacides niger	(Linnaeus, 1758)
Mythimna albipuncta	([Denis & Schiffermüller], 1775)
Mythimna conigera	([Denis & Schiffermüller], 1775)
Mythimna ferrago	(Fabricius, 1787)
Mythimna impura	(Hübner, 1808)
Mythimna l-album	(Linnaeus, 1767)
Mythimna pallens	(Linnaeus, 1758)
Mythimna pudorina	([Denis & Schiffermüller], 1775)
Mythimna straminea	(Treitschke, 1825)
Mythimna turca	(Linnaeus, 1761)
Mythimna unipuncta	(Haworth, 1809)
Mythimna vitellina	(Hübner, 1808)
Myzia oblongoguttata	(Linnaeus, 1758)
Nabis flavomarginatus	Scholtz, 1847
Nabis brevis	Scholtz, 1847
Nabis capsiformis	(Germar, 1837)
Nabis ferus	(Linnaeus, 1758)
Nabis limbatus	Dahlbom, 1851
Nabis lineata	Dahlbom, 1850
Nabis pseudoferus	Remane, 1949
Nabis punctatus	Costa, 1843
Nabis rugosus	(Linnaeus, 1758)
Nacerdes carniolica	(Gistel, 1832)
Nacerdes melanura	(Linnaeus, 1758)
Naenia typica	(Linnaeus, 1758)
Nagusta goedelii	(Kolenati, 1857)
Meliboeus fulgidicollis	(Lucas, 1846)
Nalassus convexus	(Küster, 1849)
Nalassus dermestoides	(Illiger, 1798)
Nannopus palustris	Brady, 1880
Nannospalax leucodon	(Nordmann, 1840)
Nanoclavelia leucoptera	(Dahlbom, 1843)
Nanodiscus transversus	(Aubé, 1850)
Nanomimus anulatus	(Aragona, 1830)
Nanomimus circumscriptus	(Aubé, 1864)
Nanomimus hemisphaericus	(Olivier, 1807)
Nanophyes brevis	Boheman, 1845
Nanophyes globiformis	Kiesenwetter, 1864
Nanophyes globulus	(Germar, 1821)
Nargus anisotomoides	(Spence, 1815)
Nargus badius	(Sturm, 1839)
Nargus brunneus	(Sturm, 1839)
Nargus velox	(Spence, 1815)
Nargus wilkini	(Spence, 1815)
Narraga fasciolaria	(Hufnagel, 1767)
Narraga tessularia	(Metzner, 1845)
Narycia astrella	(Herrich-Schäffer, 1851)
Narycia duplicella	(Goeze, 1783)
Nascia cilialis	(Hübner, 1796)
Nathrius brevipennis	(Mulsant, 1839)
Natrix natrix	(Linnaeus, 1758)
Natrix tessellata	(Laurenti, 1768)
Neatus picipes	(Herbst, 1797)
Nebria brevicollis	(Fabricius, 1792)
Nebria livida	(Linnaeus, 1758)
Nebria rufescens	(Stroem, 1768)
Nebrioporus canaliculatus	(Lacordaire, 1835)
Nebrioporus depressus	(Fabricius, 1775)
Nebrioporus elegans	(Panzer, 1794)
Nebula salicata	(Denis & Schiffermüller, 1775)
Necrobia ruficollis	(Fabricius, 1775)
Necrobia rufipes	(De Geer, 1775)
Necrobia violacea	(Linnaeus, 1758)
Necrodes littoralis	(Linnaeus, 1758)
Necrophilus subterraneus	(Dahl, 1807)
Necydalis major	Linnaeus, 1758
Necydalis ulmi	Chevrolat, 1863
Nedyus quadrimaculatus	(Linnaeus, 1758)
Negastrius pulchellus	(Linnaeus, 1758)
Negastrius sabulicola	(Boheman, 1852)
Nehalennia speciosa	(Charpentier, 1846)
Nehemitropia lividipennis	(Mannerheim, 1830)
Neides tipularius	(Linnaeus, 1758)
Strophosoma faber	(Herbst, 1785)
Nemadus colonoides	(Kraatz, 1851)
Nemapogon clematella	(Fabricius,1781)
Nemapogon cloacella	(Haworth, 1828)
Nemapogon falstriella	(Haas, 1881)
Nemapogon granella	(Linnaeus, 1758)
Nemapogon gravosaellus	G. Petersen, 1957
Nemapogon hungarica	Gozmány, 1960
Nemapogon inconditella	(D. Lucas, 1956)
Nemapogon nigralbella	(Zeller, 1839)
Nemapogon picarella	(Clerck, 1759)
Nemapogon variatella	(Clemens, 1859)
Nemapogon wolffiella	Karsholt & Nielsen, 1976
Nemasoma varicorne	C.L.Koch, 1847
Nematodes filum	(Fabricius, 1801)
Nematogmus sanguinolentus	(Walckenaer, 1841)
Nematopogon adansoniella	(Villers, 1789)
Nematopogon metaxella	(Hübner, 1813)
Nematopogon pilella	(Denis & Schiffermüller, 1775)
Nematopogon robertella	(Clerck, 1759)
Nematopogon schwarziellus	Zeller, 1839
Nematopogon swammerdamella	(Linnaeus, 1758)
Nemaxera betulinella	(Paykull, 1785)
Nemesia pannonica budensis	Herman, 1879
Nemobius sylvestris	(Bosc, 1792)
Nemocoris fallenii	R.F. Sahlberg, 1848
Nemolecanium graniformis	(Wünn,1921)
Nemonyx lepturoides	(Fabricius, 1801)
Nemophora prodigellus	(Zeller, 1853)
Nemophora cupriacella	(Hübner, 1819)
Nemophora degeerella	(Linnaeus, 1758)
Nemophora dumerilella	(Duponchel, 1839)
Nemophora fasciella	(Fabricius, 1775)
Nemophora metallica	(Poda, 1761)
Nemophora minimella	(Denis & Schiffermüller, 1775)
Nemophora mollella	(Hübner, 1813)
Nemophora ochsenheimerella	(Hübner, 1813)
Nemophora pfeifferella	(Hübner, 1813)
Nemophora raddaella	(Hübner, 1793)
Nemophora violellus	(Stainton, 1851)
Nemoura avicularis	Morton, 1894
Nemoura cambrica	(Stephens, 1836)
Nemoura cinerea	(Retzius, 1783)
Nemoura dubitans	Morton, 1894
Nemoura flexuosa	Aubert, 1949
Nemoura fusca	Kis, 1963
Nemoura longicauda	Kis, 1964
Nemoura marginata	Pictet, 1835
Nemoura sciurus	Aubert, 1949
Nemozoma elongatum	(Linnaeus, 1761)
Nemurella pictetii	Klapálek, 1900
Neoaliturus fenestratus	(Herrich-Schäffer, 1834)
Circulifer haematoceps	(Mulsant & Rey, 1855)
Neoascia annexa	(Müller, 1776)
Neoascia geniculata	(Meigen, 1822)
Neoascia interrupta	(Meigen, 1822)
Neoascia meticulosa	(Scopoli, 1763)
Neoascia obliqua	Coe, 1940
Neoascia podagrica	(Fabricius, 1775)
Neoascia tenur	(Harris, 1780)
Neoascia unifasciata	(Strobl, 1898)
Neobisnius lathrobioides	(Baudi, 1848)
Neobisnius procerulus	(Gravenhorst, 1806)
Neobisnius villosulus	(Stephens, 1833)
Neoclytus acuminatus	(Fabricius, 1775)
Neocnemodon brevidens	(Egger, 1865)
Neocnemodon latitarsis	(Egger, 1865)
Neocnemodon pubescens	(Delucchi & Pschorn-Walcher, 1955)
Neocnemodon vitripennis	(Meigen, 1822)
Neocoenorrhinus aeneovirens	(Marsham, 1802)
Neocoenorrhinus germanicus	(Herbst, 1797)
Neocoenorrhinus interpunctatus	(Stephens, 1831)
Neocoenorrhinus pauxillus	(Germar, 1824)
Neodorcadion bilineatum	(Germar, 1824)
Neoephemera maxima	(Joly, 1870)
Neoergasilus japonicus	(Harada, 1930)
Neofaculta ericetella	(Geyer, 1832)
Neofaculta infernella	(Herrich-Schäffer, 1854)
Neofriseria singula	(Staudinger, 1876)
Neoglocianus albovittatus	(Germar, 1824)
Neoglocianus maculaalba	(Herbst, 1795)
Neogobius fluviatilis	(Pallas, 1814)
Neogobius gymnotrachelus	(Kessler, 1857)
Neogobius kessleri	(Günther, 1861)
Neogobius melanostomus	(Pallas, 1814)
Neogobius syrman	(Nordmann, 1840)
Neohilara subterranea	(Mulsant et Rey, 1853)
Neomargarodes festucae	Archangelskaja,1935
Neomida haemorrhoidalis	(Fabricius, 1787)
Neomys anomalus	Cabrera, 1907
Neomys fodiens	(Pennant, 1771)
Neon pictus	Kulczynski, 1891
Neon rayi	(Simon, 1875)
Neon reticulatus	(Blackwall, 1853)
Neon valentulus	Falconer, 1912
Neophilaenus albipennis	(Fabricius, 1798)
Neophilaenus campestris	(Fallén, 1805)
Neophilaenus exclamationis	(Thunberg, 1784)
Neophilaenus infumatus	(Haupt, 1917)
Neophilaenus lineatus	(Linnaeus, 1758)
Neophilaenus minor	(Kirschbaum, 1868)
Neophilaenus modestus	(Haupt, 1922)
Neophron percnopterus	(Linnaeus, 1758)
Neophytobius granatus	(Gyllenhal, 1836)
Neophytobius muricatus	(Ch. Brisout, 1867)
Neophytobius quadrinodosus	(Gyllenhal, 1813)
Neoplinthus tigratus porcatus	(Rossi, 1792)
Neopristilophus insitivus	(Germar, 1824)
Neoscona adianta	(Walckenaer, 1802)
Neosphaleroptera nubilana	(Hübner, 1799)
Neottiglossa leporina	(Herrich-Schäffer, 1830)
Neottiglossa lineolata	(Mulsant & Rey, 1852)
Neottiglossa pusilla	(Gmelin, 1789)
Neottiura bimaculata	(Linnaeus, 1767)
Neottiura suaveolens	(Simon, 1879)
Neozephyrus quercus	(Linnaeus, 1758)
Nepa cinerea	Linnaeus, 1758
Nephanes titan	(Newman, 1834)
Nephopterix angustella	(Hübner, 1796)
Nephus anomus	(Mulsant, 1846)
Nephus bipunctatus	Kugelann, 1794
Nephus bisignatus	Boheman, 1850
Nephus bisignatus claudiae	Boheman, 1850
Nephus horioni	(Fürsch, 1965)
Nephus nigricans	(Weise, 1879)
Nephus quadrimaculatus	(Herbst, 1783)
Nephus redtenbacheri	(Mulsant, 1846)
Nephus semirufus	(Weise, 1885)
Nephus ulbrichi	Fürsch, 1977
Neptis rivularis	(Scopoli, 1763)
Neptis sappho	(Pallas, 1771)
Neriene clathrata	(Sundevall, 1830)
Neriene emphana	(Walckenaer, 1841)
Neriene furtiva	(O.P.-Cambridge, 1870)
Neriene montana	(Clerck, 1757)
Neriene peltata	(Wider, 1834)
Neriene radiata	(Walckenaer, 1841)
Nesovitrea hammonis	(Ström, 1765)
Nesticus cellulanus	(Clerck, 1757)
Protaetia cuprea	(Andersch, 1797)
Protaetia cuprea obscura	(Andersch, 1797)
Protaetia fieberi	(Kraatz, 1880)
Protaetia ungarica	(Herbst, 1790)
Netta rufina	(Pallas, 1773)
Nevraphes angulatus	(Müller & Kunze, 1822)
Neuraphes carinatus	(Mulsant, 1861)
Neuraphes elongatulus	(P.W.J. Müller et Kunze, 1822)
Neuraphes parallelus	(Chaudoir, 1845)
Neuraphes plicicollis	Reitter, 1879
Neuraphes talparum	Lokay, 1920
Neureclipsis bimaculata	(Linnaeus, 1758)
Neuroleon nemausiensis	(Borkhausen, 1791)
Neurothaumasia ankerella	(Mann, 1867)
Newsteadia floccosa	(de Geer,1778)
Nezara viridula	(Linnaeus, 1758)
Liothorax kraatzi	(Harold, 1868)
Nialus varians	(Duftschmid, 1805)
Nicrophorus antennatus	Reitter, 1885
Nicrophorus interruptus	Stephens, 1830
Nicrophorus germanicus	(Linnaeus, 1758)
Nicrophorus humator	Olivier, 1790
Nicrophorus investigator	Zetterstedt, 1824
Nicrophorus sepultor	Charpentier, 1825
Nicrophorus vespillo	(Linnaeus, 1758)
Nicrophorus vespilloides	Herbst, 1784
Nicrophorus vestigator	Herschel, 1807
Niditinea fuscella	(Linnaeus, 1758)
Niditinea striolella	(Matsumura, 1931)
Nigma flavescens	(Walckenaer, 1830)
Nigma walckenaeri	(Roewer, 1951)
Nimbus affinis	(Panzer, 1823)
Nimbus contaminatus	(Herbst, 1783)
Nimbus obliteratus	(Panzer, 1823)
Nineta carinthiaca	(Hölzel, 1965)
Nineta flava	(Scopoli, 1763)
Nineta guadarramensis	(Pictet, 1865)
Nineta guadarramensis principiae	(Pictet, 1865)
Nineta inpunctata	(Reuter, 1894)
Nineta pallida	(Schneider, 1851)
Nineta vittata	(Wesmael, 1841)
Niphonympha dealbatella	(Zeller, 1847)
Niptus hololeucus	(Faldermann, 1836)
Nitela fallax	Kohl, 1884
Nitela spinolae	Latreille, 1809
Nithecus jacobaeae	(Schilling, 1829)
Nitidula bipunctata	(Linnaeus, 1758)
Nitidula carnaria	(Schaller, 1783)
Nitidula flavomaculata	Rossi, 1790
Nitidula rufipes	(Linnaeus, 1767)
Nitocra inuber	(Schmankevich, 1875)
Nitocrella hibernica	(Brady, 1880)
Nivellia sanguinosa	(Gyllenhal, 1827)
Nobius serotinus	(Panzer, 1799)
Noctua comes	Hübner, 1813
Noctua fimbriata	(Schreber, 1759)
Noctua interjecta	Hübner, 1803
Noctua interposita	(Hübner, 1790)
Noctua janthe	(Borkhausen, 1792)
Noctua janthina	([Denis & Schiffermüller], 1775)
Noctua orbona	(Hufnagel, 1766)
Noctua pronuba	(Linnaeus, 1758)
Nohoveus punctulatus	(Steven in Fisher v. Waldheim, 1822)
Nola aerugula	(Hübner, 1793)
Nola chlamitulalis	(Hübner, 1813)
Nola cicatricalis	(Treitschke, 1835)
Nola confusalis	(Herrich-Schäffer, 1847)
Nola cristatula	(Hübner, 1793)
Nola cucullatella	(Linnaeus, 1758)
Nomada alboguttata	Herrich-Schaeffer, 1839
Nomada argentata	Herrich-Schaeffer, 1839
Nomada armata	Herrich-Schaeffer, 1839
Nomada atroscutellaris	Strand, 1921
Nomada baccata	Smith, 1844
Nomada baccata hrubanti	Smith, 1844
Nomada basalis	Herrich-Schaeffer, 1839
Nomada bifasciata	Olivier, 1811
Nomada bifasciata fucata	Olivier, 1811
Nomada bifasciata lepeletieri	Olivier, 1811
Nomada bifida	Thomson, 1872
Nomada bispinosa	Mocsáry, 1883
Nomada blepharipes	Schmiedeknecht, 1883
Nomada bluethgeni	Stöckhert, 1943
Nomada braunsiana	Schmiedeknecht, 1882
Nomada calimorpha	Schmiedeknecht, 1882
Nomada castellana	Dusmet, 1913
Nomada chrysopyga	Morawitz, 1871
Nomada cojungens	Herrich-Schaeffer, 1839
Nomada concolor	Schmiedeknecht, 1882
Nomada confinis	Schmiedeknecht, 1882
Nomada corcyraea	Schmiedeknecht, 1882
Nomada cruenta	Schmiedeknecht, 1882
Nomada dira	Schmiedeknecht, 1882
Nomada distinguenda	Morawitz, 1874
Nomada emarginata	Morawitz, 1877
Nomada errans	Lepeletier, 1841
Nomada erythrocephala	Morawitz, 1870
Nomada fabriciana	(Linnaeus, 1767)
Nomada facilis	Schwarz, 1967
Nomada femoralis	Morawitz, 1869
Nomada ferruginata	(Linnaeus, 1767)
Nomada flava	Panzer, 1798
Nomada flavoguttata	(Kirby, 1802)
Nomada flavopicta	(Kirby, 1802)
Nomada fulvicornis	Fabricius, 1793
Nomada furva	Panzer, 1798
Nomada furvoides	Stöckhert, 1943
Nomada fuscicornis	Nylander, 1848
Nomada glaucopis	Pérez, 1884
Nomada guttulata	Schenck, 1859
Nomada hirtipes	Pérez, 1884
Nomada hungarica	Dalla Torre & Friese, 1899
Nomada immaculata	Morawitz, 1873
Nomada imperialis	Schmiedeknecht, 1882
Nomada incisa	Schmiedeknecht, 1882
Nomada insignipes	Schmiedeknecht, 1882
Nomada integra	Brullé, 1832
Nomada kohli	Schmiedeknecht, 1882
Nomada lathburiana	(Kirby, 1802)
Nomada leucopthalma	(Kirby, 1802)
Nomada marshamella	(Kirby, 1802)
Nomada mauritanica	Lepeletier, 1841
Nomada mauritanica manni	Lepeletier, 1841
Nomada melanopyga	Schmiedeknecht, 1882
Nomada melathoracica	Imhoff, 1834
Nomada mocsaryi	Schmiedeknecht, 1882
Nomada mutabilis	Morawitz, 1870
Nomada mutica	Morawitz, 1872
Nomada nobilis	Herrich-Schaeffer, 1839
Nomada noskiewiczi	Schwarz, 1966
Nomada obscura	Zetterstedt, 1838
Nomada obtusifrons	Nylander, 1844
Nomada oculata	Friese, 1899
Nomada opaca	Alfken, 1913
Nomada panzeri	Lepeletier, 1841
Nomada panzeri glabella	Lepeletier, 1841
Nomada pectoralis	Morawitz, 1877
Nomada piccioliana	Magretti, 1883
Nomada platythorax	Schwarz, 1981
Nomada pleurosticta	Herrich-Schaeffer, 1839
Nomada posthuma	Blüthgen, 1949
Nomada pulchra	Arnold, 1900
Nomada pygidialis	Schwarz, 1981
Nomada rhenana	Morawitz, 1872
Nomada rostrata	Herrich-Schaeffer, 1839
Nomada rufipes	Fabricius, 1793
Nomada sexfasciata	Panzer, 1799
Nomada sheppardana	(Kirby, 1802)
Nomada sheppardana minuscula	(Kirby, 1802)
Nomada signata	Jurine, 1807
Nomada stigma	Fabricius, 1804
Nomada stoeckherti	Pittioni, 1953
Nomada striata	Fabricius, 1793
Nomada succincta	Panzer, 1798
Nomada sybarita	Schmiedeknecht, 1882
Nomada symphyti	Stöckhert, 1930
Nomada tenella	Mocsáry, 1883
Nomada thersites	Schmiedeknecht, 1882
Nomada trapeziformis	Schmiedeknecht, 1882
Nomada tridentirostris	Dours, 1873
Nomada trispinosa	Schmiedeknecht, 1882
Nomada verna	Schmiedeknecht, 1882
Nomada villosa	Thomson, 1870
Nomada zonata	Panzer, 1798
Nomioides minutissimus	(Rossi, 1790)
Nomioides variegatus	(Olivier, 1789)
Nomisia exornata	(C.L. Koch, 1839)
Nomius pygmaeus	(Dejean, 1831)
Nomophila noctuella	(Denis & Schiffermüller, 1775)
Nonagria typhae	(Thunberg, 1784)
Nopoiulus kochii	(Gervais, 1847)
Nosodendron fasciculare	(Olivier, 1790)
Nossidium pilosellum	(Marsham, 1802)
Notaris acridula	(Linnaeus, 1758)
Notaris aterrima	(Hampe, 1850)
Notaris maerkeli	(Boheman, 1843)
Notaris scirpi	(Fabricius, 1792)
Noterus clavicornis	(De Geer, 1774)
Noterus crassicornis	(O.F. Müller, 1776)
Nothocasis sertata	(Hübner, 1817)
Nothochrysa fulviceps	(Stephens, 1836)
Nothodes parvulus	(Panzer, 1799)
Nothris lemniscellus	(Zeller, 1839)
Nothris verbascella	(Denis & Schiffermüller, 1775)
Notidobia ciliaris	(Linnaeus, 1761)
Notiophilus biguttatus	(Fabricius, 1799)
Notiophilus germinyi	Fauvel, 1863
Notiophilus laticollis	Chaudoir, 1850
Notiophilus palustris	(Duftschmid, 1812)
Notiophilus pusillus	Waterhouse, 1833
Notiophilus rufipes	Curtis, 1829
Nyctereutes procyonoides	(Gray, 1834)
Notocelia roborana	(Denis & Schiffermüller, 1775)
Notocelia cynosbatella	(Linnaeus, 1758)
Notocelia incarnatana	(Hübner, 1800)
Notocelia trimaculana	(Haworth, 1811)
Notocelia uddmanniana	(Linnaeus, 1758)
Notodonta dromedarius	(Linnaeus, 1758)
Notodonta torva	(Hübner, 1803)
Notodonta tritophus	([Denis & Schiffermüller], 1775)
Notodonta ziczac	(Linnaeus, 1758)
Notodromas monacha	(O.F. Müller, 1776)
Notolaemus castaneus	(Erichson, 1846)
Notolaemus unifasciatus	(Latreille, 1804)
Notonecta glauca	Linnaeus, 1758
Notonecta lutea	Müller, 1776
Notonecta meridionalis	Poisson, 1926
Notonecta obliqua	Gallen, 1787
Notonecta viridis	Delcourt, 1909
Notostira elongata	(Geoffroy, 1785)
Notostira erratica	(Linnaeus, 1758)
Notothecta flavipes	(Gravenhorst, 1806)
Notoxus cavifrons appendicinus	La Ferté-Sénectère, 1849
Notoxus brachycerus	(Faldermann, 1837)
Notoxus monoceros	(Linnaeus, 1761)
Notoxus trifasciatus	Rossi, 1794
Notus flavipennis	(Zetterstedt, 1828)
Noxius curtirostris	(Mulsant & Rey, 1861)
Nucifraga caryocatactes	(Linnaeus, 1758)
Nucifraga caryocatactes macrorhynchos	(Linnaeus, 1758)
Nuctenea umbratica	(Clerck, 1757)
Dynaspidiotus abietis	(Schrank, 1776)
Nudaria mundana	(Linnaeus, 1761)
Nudobius lentus	(Gravenhorst, 1806)
Numenius arquata	(Linnaeus, 1758)
Numenius arquata orientalis	(Linnaeus, 1758)
Numenius phaeopus	(Linnaeus, 1758)
Numenius tenuirostris	Vieillot, 1817
Nychiodes dalmatina	Wagner, 1909
Nychiodes obscuraria	(Villers, 1789)
Nyctalus lasiopterus	(Schreber, 1780)
Nyctalus leisleri	(Kuhl, 1818)
Nyctalus noctula	(Schreber, 1774)
Bubo scandiaca	(Linnaeus, 1758)
Nyctegretis lineana	(Scopoli, 1786)
Nyctegretis triangulella	Ragonot, 1901
Nycteola asiatica	(Krulikovsky, 1904)
Nycteola degenerana	(Hübner, 1799)
Nycteola revayana	(Scopoli, 1772)
Nycteola siculana	(Fuchs, 1899)
Nycticorax nycticorax	(Linnaeus, 1758)
Nymphalis antiopa	(Linnaeus, 1758)
Nymphalis polychloros	(Linnaeus, 1758)
Nymphalis vaualbum	([Denis & Schiffermüller], 1775)
Nymphalis xanthomelas	(Esper, 1781)
Nymphula nitidulata	(Hufnagel, 1767)
Nysius cymoides	(Spinola, 1837)
Nysius ericae	(Schilling, 1829)
Nysius graminicola	(Kolenati, 1846)
Nysius helveticus	(Herrich-Schäffer,1850)
Nysius senecionis	(Schilling, 1829)
Nysius thymi	(Wolff, 1804)
Nysson chevrieri	Kohl, 1879
Nysson dimidiatus	Jurine, 1807
Nysson fulvipes	Costa, 1859
Nysson interruptus	(Fabricius, 1798)
Nysson maculosus	(Gmelin, 1790)
Nysson niger	Chevrier, 1868
Nysson roubali	Zavadil, 1937
Nysson spinosus	(Forster, 1771)
Nysson tridens	Gerstaecker, 1867
Nysson trimaculatus	(Rossi, 1790)
Nysson variabilis	Chevrier, 1867
Oberea erythrocephala	(Schrank, 1776)
Oberea euphorbiae	(Germar, 1813)
Oberea linearis	(Linnaeus, 1761)
Oberea moravica	Kratochvíl, 1989
Oberea oculata	(Linnaeus, 1758)
Oberea pedemontana	Chevrolat, 1856
Oberea pupillata	(Gyllenhal, 1817)
Obrium brunneum	(Fabricius, 1792)
Obrium cantharinum	(Linnaeus, 1767)
Ocalea badia	Erichson, 1837
Ocalea picata	(Stephens, 1832)
Ocalea puncticeps	(Kraatz, 1858)
Ocalea rivularis	Miller, 1852
Ochetostethus opacus	(Scholtz, 1847)
Ochina latrellii	(Bonelli, 1812)
Ochlerotatus annulipes	(Meigen, 1830)
Ochlerotatus cantans	(Meigen, 1818)
Ochlerotatus caspius	(Pallas, 1771)
Ochlerotatus cataphylla	Dyar, 1916
Ochlerotatus communis	(De Geer, 1776)
Ochlerotatus detritus	(Haliday, 1833)
Ochlerotatus dorsalis	(Meigen, 1830)
Ochlerotatus excrucians	(Walker, 1856)
Ochlerotatus flavescens	(Müller, 1764)
Ochlerotatus geniculatus	(Olivier, 1791)
Ochlerotatus hungaricus	Mihályi, 1955
Ochlerotatus leucomelas	(Meigen, 1804)
Ochlerotatus nigrinus	(Eckstein, 1918)
Ochlerotatus pulchritarsis	(Rondani, 1872)
Ochlerotatus punctor	(Kirby, 1829
Ochlerotatus refiki	Medschid, 1928
Ochlerotatus rusticus	(Rossi, 1790)
Ochlerotatus sticticus	(Meigen, 1838)
Ochlodes sylvanus	(Esper, 1777)
Ochodaeus chrysomeloides	(Schrank, 1781)
Ochodaeus thalycroides	Reitter, 1892
Ochogona caroli	(Rothenbühler, 1900)
Ochogona caroli hungaricum	(Rothenbühler, 1900)
Ochogona caroli somloense	(Rothenbühler, 1900)
Ochogona triaina	(Attems, 1895)
Ochromolopis ictella	(Hübner, 1813)
Ochropacha duplaris	(Linnaeus, 1761)
Ochropleura leucogaster	(Freyer, 1831)
Ochropleura plecta	(Linnaeus, 1761)
Ochsenheimeria capella	Möschler, 1860
Ochsenheimeria taurella	(Denis & Schiffermüller, 1775)
Ochsenheimeria urella	Fischer von Rröslerstamm, 1842
Ochsenheimeria vacculella	Fischer von Rröslerstamm, 1842
Ochthebius caudatus	J. Frivaldszky, 1883
Ochthebius colveranus	Ferro, 1979
Ochthebius flavipes	Dalla Torre, 1877
Ochthebius hungaricus	Endrődy-Younga, 1968
Ochthebius lividipennis	Peyron, 1857
Ochthebius metallescens	(Dalla Torre, 1877)
Ochthebius meridionalis	Rey, 1885
Ochthebius minimus	(Fabricius, 1792)
Ochthebius peisonis	Ganglbauer, 1901
Ochthebius pusillus	Stephens, 1835
Ochthebius striatus	(Castelnau, 1840)
Ochthebius viridis	Peyron, 1858
Ochthephilus omalinus	(Erichson, 1840)
Ocneria rubea	([Denis & Schiffermüller], 1775)
Ocnogyna parasita	(Hübner, 1790)
Octotemnus glabriculus	(Gyllenhal, 1827)
Ocypus aeneocephalus	(De Geer, 1774)
Ocypus biharicus	(J.Müller, 1926)
Ocypus brevipennis	(Heer, 1839)
Ocypus brunnipes	(Fabricius, 1781)
Ocypus fulvipennis	(Erichson, 1840)
Ocypus fuscatus	(Gravenhorst, 1802)
Ocypus macrocephalus	(Gravenhorst, 1802)
Ocypus mus	(Brullé, 1832)
Ocypus nitens	(Schrank, 1781)
Ocypus olens	(O.F.Müller, 1764)
Ocypus ophthalmicus	(Scopoli, 1763)
Ocypus picipennis	(Fabricius, 1793)
Ocypus serotinus	(Ádám, 1992)
Ocypus tenebricosus	(Gravenhorst, 1847)
Ocyusa maura	(Erichson, 1837)
Ocyusa picina	(Aubé, 1850)
Odacantha melanura	(Linnaeus, 1767)
Odezia atrata	(Linnaeus, 1758)
Odice arcuinna	(Hübner, 1790)
Odites kollarella	(O. Costa, 1832)
Odonestis pruni	(Linnaeus, 1758)
Bolboceras armiger	(Scopoli, 1772)
Odontocerum albicorne	(Scopoli, 1763)
Odontognophos dumetata	(Treitschke, 1827)
Odontopera bidentata	(Clerck, 1759)
Odontopodisma decipiens	Ramme, 1951
Odontopodisma rubripes	(Ramme, 1931)
Odontopodisma schmidtii	(Fieber, 1835)
Odontoscelis fuliginosa	(Linnaeus, 1761)
Odontoscelis hispidula	Jakovlev, 1874
Odontoscelis lineola	Rambur, 1842
Odontosia carmelita	(Esper, 1799)
Odontotarsus purpureolineatus	(Rossi, 1790)
Odontotarsus robustus	Jakovlev, 1884
Odynerus femoratus	Saussure, 1856
Odynerus melanocephalus	(Gmelin, 1790)
Odynerus poecilus	Saussure, 1856
Odynerus reniformis	(Gmelin, 1790)
Odynerus simillimus	Morawitz, 1867
Odynerus spinipes	(Linnaeus, 1758)
Oecanthus pellucens	(Scopoli, 1763)
Oecetis furva	(Rambur, 1842)
Oecetis lacustris	(Pictet, 1834)
Oecetis notata	(Rambur, 1842)
Oecetis ochracea	(Curtis, 1825)
Oecetis testacea	(Curtis, 1834)
Oecetis tripunctata	(Fabricius, 1793)
Oeciacus hirundinis	(Jenyns, 1839)
Oecismus monedula	(Hagen, 1859)
Oecophora bractella	(Linnaeus, 1758)
Oedaleus decorus	(Germar, 1826)
Oedecnemidius pictus	(Steven, 1829)
Oedemera croceicollis	(Gyllenhal, 1827)
Oedemera femoralis	(Olivier, 1803)
Oedemera femorata	(Scopoli, 1763)
Oedemera flavipes	(Fabricius, 1792)
Oedemera lateralis	(Gebler, 1830)
Oedemera lurida	(Marsham, 1802)
Oedemera podagrariae	(Linnaeus, 1767)
Oedemera pthysica	(Scopoli, 1763)
Oedemera subrobusta	(Nakane, 1954)
Oedemera virescens	(Linnaeus, 1767)
Oedipoda caerulescens	(Linnaeus, 1758)
Oedostethus quadripustulatus	(Fabricius, 1792)
Oedostethus tenuicornis	(Germar, 1824)
Oedothorax agrestis	(Blackwall, 1853)
Oedothorax apicatus	(Blackwall, 1850)
Oedothorax fuscus	(Blackwall, 1834)
Oedothorax gibbosus	(Blackwall, 1841)
Oedothorax retusus	(Westring, 1851)
Oegoconia caradjai	Popescu-Gorj & Capuse, 1965
Oegoconia deauratella	(Herrich-Schäffer, 1854)
Oegoconia uralskella	Popescu-Gorj & Capuse, 1965
Oemopteryx loewii	Albarda, 1889
Oenanthe deserti	(Temminck, 1825)
Oenanthe hispanica	(Linnaeus, 1758)
Oenanthe hispanica melanoleuca	(Linnaeus, 1758)
Oenanthe isabellina	(Temminck, 1829)
Oenanthe oenanthe	(Linnaeus, 1758)
Oenanthe pleschanka	(Lepechin, 1770)
Oenas crassicornis	(Illiger, 1800)
Oenopia conglobata	(Linnaeus, 1758)
Oenopia impustulata	(Linnaeus, 1761)
Oenopia lyncea	(Olivier, 1808)
Oenopia lyncea agnata	(Olivier, 1808)
Oiceoptoma thoracicum	(Linnaeus, 1758)
Hellinsia carphodactyla	(Hübner, 1813)
Oidaematophorus constanti	Ragonot, 1875
Hellinsia didactylites	(Ström, 1783)
Hellinsia distinctus	(Herrich-Schäffer, 1855)
Hellinsia inulae	(Zeller, 1852)
Oidaematophorus lithodactyla	(Treitschke, 1833)
Hellinsia tephradactyla	(Hübner, 1813)
Oinophila v-flava	(Haworth, 1828)
Olethreutes arcuella	(Clerck, 1759)
Argyroploce roseomaculana	(Herrich-Schäffer, 1851)
Olibrus aeneus	(Fabricius, 1792)
Olibrus affinis	(Sturm, 1807)
Olibrus baudueri	Flach, 1888
Olibrus bicolor	(Fabricius, 1792)
Olibrus bimaculatus	Küster, 1848
Olibrus bisignatus	(Ménétries, 1849)
Olibrus corticalis	(Panzer, 1792)
Olibrus gerhardti	Flach, 1888
Olibrus liquidus	Erichson, 1845
Olibrus millefolii	(Paykull, 1800)
Olibrus pygmaeus	(Sturm, 1807)
Oligella foveolata	(Allibert, 1844)
Oligia dubia	(Heydemann, 1942)
Oligia fasciuncula	(Haworth, 1809)
Oligia latruncula	([Denis & Schiffermüller], 1775)
Oligia strigilis	(Linnaeus, 1758)
Oligia versicolor	(Borkhausen, 1792)
Oligolimax annularis	(S. Studer, 1820)
Oligomerus brunneus	(Olivier, 1790)
Oligomerus ptilinoides	(Wollaston, 1854)
Oligomerus retowskii	Schilsky, 1898
Oligoneuriella keffermuellerae	Sowa, 1973
Oligoneuriella pallida	(Hagen, 1855)
Oligoneuriella polonica	Mol, 1984
Oligoneuriella rhenana	(Imhoff, 1852)
Oligostomis reticulata	(Linnaeus, 1761)
Oligota granaria	Erichson, 1837
Oligota inflata	(Mannerheim, 1830)
Oligota parva	Kraatz, 1862
Oligota pumilio	Kiesenwetter, 1858
Oligota pusillima	(Gravenhorst, 1806)
Oligota rufipennis	Kraatz, 1858
Oligotricha striata	(Linnaeus, 1758)
Olindia schumacherana	(Fabricius, 1787)
Olisthopus rotundatus	(Paykull, 1790)
Olisthopus sturmii	(Duftschmid, 1812)
Olophrum assimile	(Paykull, 1800)
Olophrum austriacum	Scheerpeltz, 1929
Olophrum piceum	(Gyllenhal, 1810)
Olophrum puncticolle	Eppelsheim, 1880
Olophrum viennense	Scheerpeltz, 1929
Omalisus fontisbellaquaei	Geoffroy, 1762
Omalium caesum	Gravenhorst, 1806
Omalium cinnamomeum	Kraatz, 1858
Omalium excavatum	Stephens, 1834
Omalium exiguum	Gyllenhal, 1810
Omalium ferrugineum	Kraatz, 1858
Omalium imitator	Luze, 1906
Omalium littorale	Kraatz, 1858
Omalium oxyacanthae	Gravenhorst, 1806
Omalium rivulare	(Paykull, 1789)
Omalium rugatum	Mulsant et Rey, 1880
Omalium strigicolle	Wankowicz, 1869
Omalium validum	Kraatz, 1858
Omaloplia marginata	(Füessly, 1775)
Omaloplia ruricola	(Fabricius, 1775)
Omaloplia spireae	(Pallas, 1773)
Omalus aeneus	(Fabricius, 1787)
Omalus biaccinctus	(Buysson, 1891)
Omiamima concinna	(Boheman, 1834)
Omiamima mollina	(Boheman, 1834)
Omiamima vindobonensis	(Formánek, 1908)
Omias globulus	(Boheman, 1843)
Omias punctatus	Angelov, 1973
Omias rotundatus	(Fabricius, 1792)
Omias seminulum	(Fabricius, 1792)
Ommatidiotus concinnus	Horváth, 1905
Ommatidiotus dissimilis	(Fallén, 1806)
Ommatidiotus falleni	Stal, 1863
Ommatidiotus inconspicuus	Stal, 1863
Ommatoiulus sabulosus	(Linnaeus, 1758)
Omocestus haemorrhoidalis	(Charpentier, 1825)
Omocestus petraeus	(Brisout, 1855)
Omocestus rufipes	(Zetterstedt, 1821)
Omocestus viridulus	(Linne, 1758)
Omonadus bifasciatus	(Rossi, 1794)
Omonadus floralis	(Linnaeus, 1758)
Omonadus formicarius	(Goeze, 1777)
Omophlus betulae	(Herbst, 1783)
Omophlus lividipes	Mulsant, 1856
Omophlus picipes	(Fabricius, 1792)
Omophlus proteus	Kirsch, 1869
Omophlus rugosicollis	(Brullé, 1832)
Omophron limbatum	(Fabricius, 1776)
Omosita colon	(Linnaeus, 1758)
Omosita depressa	(Linnaeus, 1758)
Omosita discoidea	(Fabricius, 1775)
Omphalapion dispar	(Germar, 1817)
Omphalapion hookerorum	(Kirby, 1808)
Omphalapion pseudodispar	Wanat, 1995
Omphalapion laevigatum	(Paykull, 1792)
Omphalonotus quadriguttatus	(Kirschbaum, 1856)
Omphalophana antirrhinii	(Hübner, 1803)
Alophia combustella	(Herrich-Schäffer, 1855)
Laodamia faecella	(Zeller, 1839)
Oncocera semirubella	(Scopoli, 1763)
Oncochila scapularis	(Fieber, 1844)
Oncochila simplex	(Herrich-Schäffer, 1830)
Oncodelphax pullulus	(Boheman, 1852)
Oncopsis alni	(Schrank, 1801)
Oncopsis flavicollis	(Linnaeus, 1761)
Oncopsis tristis	(Zetterstedt, 1840)
Oncorhynchus mykiss	(Walbaum, 1792)
Oncotylus setulosus	(Herrich-Schäffer, 1839)
Oncotylus viridiflavus	(Goeze, 1778)
Ondatra zibethicus	(Linnaeus, 1766)
Ontholestes haroldi	(Eppelsheim, 1884)
Ontholestes murinus	(Linnaeus, 1758)
Ontholestes tessellatus	(Geoffroy, 1785)
Onthophagus coenobita	(Herbst, 1783)
Onthophagus fracticornis	(Preyssler, 1790)
Onthophagus furcatus	(Fabricius, 1781)
Onthophagus furciceps	Marseul, 1869
Onthophagus gibbulus	(Pallas, 1781)
Onthophagus grossepunctatus	Reitter, 1905
Onthophagus illyricus	(Scopoli, 1763)
Onthophagus joannae	Goljan, 1953
Onthophagus lemur	(Fabricius, 1781)
Onthophagus lucidus	(Illiger, 1800)
Onthophagus nuchicornis	(Linnaeus, 1758)
Onthophagus ovatus	(Linnaeus, 1767)
Onthophagus ruficapillus	Brullé, 1832
Onthophagus semicornis	(Panzer, 1798)
Onthophagus similis	(Scriba, 1790)
Onthophagus taurus	(Schreber, 1759)
Onthophagus tesquorum	Semenov et Medvedev, 1927
Onthophagus vacca	(Linnaeus, 1767)
Onthophagus verticicornis	(Laicharting, 1781)
Onthophagus vitulus	(Fabricius, 1776)
Onthophilus affinis	Redtenbacher, 1849
Onthophilus punctatus	O.F. Müller, 1776
Onthophilus striatus	(Forster, 1771)
Onychogomphus forcipatus	(Linné, 1758)
Onyxacalles croaticus	H. Brisout, 1867
Onyxacalles luigionii	(A. Solari & F. Solari, 1907)
Onyxacalles pyrenaeus	Boheman, 1844
Oodes gracilis	A. villa et J. B. Villa, 1833
Oodes helopioides	(Fabricius, 1792)
Platyscelis melas	(Fischer de Waldheim, 1823)
Platyscelis polita	(Sturm, 1807)
Oonops pulcher	Templeton, 1835
Ootypus globosus	(Waltl, 1838)
Opanthribus tessellatus	(Boheman, 1829)
Opatrum riparium	Scriba, 1865
Opatrum sabulosum	(Linnaeus, 1761)
Opeas pumilum	(L. Pfeiffer, 1840)
Operophtera brumata	(Linnaeus, 1758)
Operophtera fagata	(Scharfenberg, 1805)
Opetiopalpus scutellaris	(Panzer, 1797)
Ophiogomphus cecilia	(Fourcroy, 1758)
Ophiola cornicula	(Marshall, 1866)
Ophiola decumana	(Kontkanen, 1949)
Ophiola russeola	(Fallén, 1826)
Ophiola transversa	(Fallén, 1826)
Ophonus ardosiacus	(Lutshnik, 1922)
Ophonus azureus	(Fabricius, 1775)
Cephalophonus cephalotes	(Fairmaire & Laboulbène, 1854)
Ophonus cordatus	(Duftschmid, 1812)
Ophonus cribricollis	(Dejean, 1829)
Ophonus diffinis	(Dejean, 1829)
Ophonus gammeli	(Schauberger, 1932)
Ophonus melletii	(Heer, 1837)
Ophonus laticollis	Mannerheim, 1825
Ophonus parallelus	(Dejean, 1829)
Ophonus puncticeps	(Stephens, 1828)
Ophonus puncticollis	(Paykull, 1798)
Ophonus rufibarbis	(Fabricius, 1792)
Ophonus rupicola	(Sturm, 1818)
Ophonus sabulicola	(Panzer, 1796)
Ophonus sabulicola ponticus	(Panzer, 1796)
Ophonus schaubergerianus	Puel, 1937
Ophonus stictus	Stephens, 1828
Ophonus subquadratus	(Dejean, 1829)
Ophonus subsinuatus	Rey, 1886
Ophyiulus pilosus	(Newport, 1842)
Opigena polygona	([Denis & Schiffermüller], 1775)
Opilo domesticus	(Sturm, 1837)
Opilo mollis	(Linnaeus, 1758)
Opilo pallidus	(Olivier, 1795)
Opisthograptis luteolata	(Linnaeus, 1758)
Oplosia cinerea	(Mulsant, 1839)
Oporopsamma wertheimsteini	(Rebel, 1913)
Opostega salaciella	(Treitschke, 1833)
Opostega spatulella	Herrich-Schäffer, 1855
Oprohinus consputus	(Germar, 1824)
Oprohinus suturalis	(Fabricius, 1777)
Opsibotys fuscalis	(Denis & Schiffermüller, 1775)
Opsilia coerulescens	(Scopoli, 1763)
Opsilia molybdaena	(Dalman, 1817)
Opsilia uncinata	(W. Redtenbacher, 1842)
Opsius lethierryi	Wagner, 1942
Opsius stactogalus	Fieber, 1866
Orbona fragariae	(Vieweg, 1790)
Orchesia fasciata	(Illiger, 1798)
Orchesia grandicollis	Rosenhauer, 1847
Orchesia micans	(Panzer, 1794)
Orchesia minor	Walker, 1837
Orchesia undulata	Kraatz, 1853
Orchestes fagi	(Linnaeus, 1758)
Orchestes hortorum	(Fabricius, 1792)
Orchestes rusci	(Herbst, 1795)
Orchestes subfasciatus	(Gyllenhal, 1836)
Orchestes testaceus	(O. F. Müller, 1776)
Orconectes limosus	(Rafinesque, 1817)
Orcula dolium	(Draparnaud, 1801)
Orcula jetschini	M. von Kimakowicz, 1883
Orectis proboscidata	(Herrich-Schäffer, 1851)
Orectochilus villosus	(O.F.Müller, 1776)
Oreochromis niloticus	(Linnaeus, 1758)
Orgyia antiqua	(Linnaeus, 1758)
Orgyia antiquoides	(Hübner, 1822)
Orgyia recens	(Hübner, 1819)
Oria musculosa	(Hübner, 1808)
Oriolus oriolus	(Linnaeus, 1758)
Orius horvathi	(Reuter, 1884)
Orius laticollis	(Reuter, 1884)
Orius majusculus	(Reuter, 1879)
Orius minutus	(Linnaeus, 1758)
Orius niger	Wolff, 1804
Orius vicinus	(Ribaut, 1923)
Ornativalva plutelliformis	(Staudinger, 1859)
Ornatoraphidia flavilabris	(Costa, 1855)
Ornixola caudulatella	(Zeller, 1839)
Orobitis cyanea	(Linnaeus, 1758)
Orobitis nigrina	Reitter, 1885
Orochares angustatus	(Erichson, 1840)
Parammoecius corvinus	(Erichson, 1848)
Orphilus niger	(Rossi, 1790)
Orsillus depressus	Dallas, 1852
Orthetrum albistylum	(Sélys-Longchamps, 1848)
Orthetrum brunneum	(Fonscolombe, 1837)
Orthetrum cancellatum	(Linné, 1758)
Orthetrum coerulescens	(Fabricius, 1798)
Orthezia urticae	(Linnaeus,1758)
Ortheziola vejdovskyi	Šulc, 1895
Orthocephalus bivittatus	Fieber, 1864
Orthocephalus brevis	(Panzer, 1798)
Orthocephalus saltator	(Hahn, 1835)
Orthocephalus vittipennis	(Herrich-Schäffer, 1835)
Orthocerus clavicornis	(Linnaeus, 1758)
Orthocerus crassicornis	(Erichson, 1845)
Orthochaetes setiger	(Beck, 1817)
Orthocis alni	(Gyllenhal, 1813)
Orthocis coluber	Abeille de Perrin, 1874
Orthocis festivus	(Gyllenhall, 1813)
Orthocis lucasi	(Abeille de Perrin, 1874)
Orthocis pseudolinearis	Lohse, 1964
Orthocis pygmaeus	(Marsham, 1802)
Orthocis vestitus	(Mellié, 1848)
Ortholepis betulae	(Goeze, 1778)
Ortholomus punctipennis	(Herrich-Schäffer, 1839)
Nycterosea obstipata	(Fabricius, 1794)
Orthonama vittata	(Borkhausen, 1794)
Orthonevra brevicornis	(Loew, 1843)
Orthonevra elegans	(Meigen, 1822)
Orthonevra frontalis	(Loew, 1843)
Orthonevra geniculata	(Meigen, 1830)
Orthonevra incisa	(Loew, 1843)
Orthonevra intermedia	Lundbeck, 1916
Orthonevra nobilis	(Fallén, 1817)
Orthonevra plumbago	(Loew, 1840)
Orthonevra splendens	(Meigen, 1822)
Orthonotus cylindricollis	(Costa, 1852)
Orthonotus rufifrons	(Fallén, 1807)
Orthoperus atomus	(Gyllenhal, 1808)
Orthoperus corticalis	(Redtenbacher, 1858)
Orthoperus mundus	Matthews, 1885
Orthoperus nigrescens	Stephens, 1829
Orthoperus punctatus	Wankowicz, 1865
Orthoperus rogeri	Kraatz, 1874
Orthopodomyia pulchripalpis	(Rondani, 1872)
Orthops basalis	(Costa, 1852)
Orthops campestris	(Linnaeus, 1758)
Orthops kalmii	(Linnaeus, 1758)
Ocrasa glaucinalis	(Linnaeus, 1758)
Orthosia cerasi	(Fabricius, 1775)
Orthosia cruda	([Denis & Schiffermüller], 1775)
Orthosia gothica	(Linnaeus, 1758)
Orthosia gracilis	([Denis & Schiffermüller], 1775)
Orthosia incerta	(Hufnagel, 1766)
Orthosia miniosa	([Denis & Schiffermüller], 1775)
Orthosia opima	(Hübner, 1809)
Orthosia populeti	(Fabricius, 1775)
Orthostixis cribraria	(Hübner, 1799)
Orthotaenia undulana	(Denis & Schiffermüller, 1775)
Orthotelia sparganella	(Thunberg, 1788)
Orthotomicus erosus	(Wollaston, 1857)
Orthotomicus laricis	(Fabricius, 1792)
Orthotomicus longicollis	(Gyllenhal, 1827)
Orthotomicus proximus	(Eichhoff, 1868)
Orthotomicus robustus	(Knotek, 1899)
Orthotomicus suturalis	(Gyllenhal, 1827)
Orthotrichia angustella	(McLachlan, 1865)
Orthotrichia costalis	(Curtis, 1834)
Orthotrichia tragetti	Mosely, 1930
Orthotylus bilineatus	(Fallén, 1807)
Orthotylus ericetorum	(Fallén, 1807)
Orthotylus flavinervis	(Kirschbaum, 1856)
Orthotylus flavosparsus	(F. Sahlberg, 1842)
Orthotylus fuscescens	(Kirschbaum, 1856)
Orthotylus marginalis	Reuter, 1884
Orthotylus nassatus	(Fabricius, 1787)
Orthotylus prasinus	(Fallén, 1829)
Orthotylus schoberiae	Reuter, 1876
Orthotylus tenellus	(Fallén, 1829)
Orthotylus virens	(Fallén, 1807)
Orthotylus viridinervis	(Kirschbaum, 1856)
Oryctes nasicornis	(Linnaeus, 1758)
Oryctolagus cuniculus	(Linnaeus, 1758)
Oryphantes angulatus	(O.P.-Cambridge, 1881)
Oryxolaemus flavifemoratus	(Herbst, 1797)
Oryzaephilus mercator	(Fauvel, 1889)
Oryzaephilus surinamensis	(Linnaeus, 1758)
Osmia acuticornis	Dufour & Perris, 1840
Osmia adunca	(Panzer, 1799)
Osmia andrenoides	Spinola, 1808
Osmia anthocopoides	Schenck, 1853
Osmia aurulenta	(Panzer, 1799)
Osmia bicolor	(Schrank, 1781)
Osmia bidentata	Morawitz, 1876
Osmia bisulca	Gerstaecker, 1869
Osmia brevicornis	(Fabricius, 1798)
Osmia caerulescens	(Linnaeus, 1758)
Osmia cerinthidis	Morawitz, 1876
Osmia claviventris	Thomson, 1872
Osmia cornuta	(Latreille, 1805)
Osmia dives	Mocsáry, 1877
Osmia fulva	Eversmann, 1852
Osmia gallarum	Spinola, 1808
Osmia laevifrons	Morawitz, 1872
Osmia leaiana	(Kirby, 1802)
Osmia leucomelaena	(Kirby, 1802)
Osmia ligurica	Morawitz, 1868
Osmia loti	Morawitz, 1867
Osmia manicata	Morice, 1901
Osmia melanogaster	Spinola, 1808
Osmia mitis	Nylander, 1852
Osmia mocsaryi	Friese, 1895
Osmia mustelina	Gerstaecker, 1869
Osmia niveata	(Fabricius, 1804)
Osmia papaveris	Latreille, 1799
Osmia parietina	Curtis, 1828
Osmia pilicornis	Smith, 1846
Osmia praestans	Morawitz, 1893
Osmia princeps	Morawitz, 1872
Osmia ravouxi	Pérez, 1902
Osmia rufa	(Linnaeus, 1758)
Osmia rufohirta	Latreille, 1811
Osmia scutellaris	Morawitz, 1868
Osmia spinulosa	(Kirby, 1802)
Osmia tenuispina	Alfken, 1937
Osmia tergestensis	Ducke, 1897
Osmia tridentata	Dufour & Perris, 1840
Osmoderma eremita	(Scopoli, 1763)
Osmylus fulvicephalus	(Scopoli, 1763)
Osphya bipunctata	(Fabricius, 1775)
Ossiannilssonola callosa	(Then, 1886)
Ostoma ferruginea	(Linnaeus, 1758)
Ostrinia nubilalis	(Hübner, 1796)
Ostrinia palustralis	(Hübner, 1796)
Ostrinia quadripunctalis	(Denis & Schiffermüller, 1775)
Othius brevipennis	Kraatz, 1857
Othius laeviusculus	Stephens, 1833
Othius lapidicola	(Kiesenwetter, 1848)
Othius punctulatus	(Goeze, 1777)
Othius subuliformis	Stephens, 1833
Otho sphondyloides	(Germar, 1818)
Otiorhynchus armadillo	(Rossi, 1792)
Otiorhynchus auricapillus	Germar, 1824
Otiorhynchus austriacus	(Fabricius, 1801)
Otiorhynchus bisulcatus	(Fabricius, 1781)
Otiorhynchus brunneus	Steven, 1829
Otiorhynchus cardiniger	(Host, 1798)
Otiorhynchus coarctatus	Stierlin, 1861
Otiorhynchus coecus	Germar, 1824
Otiorhynchus conspersus	(Herbst, 1795)
Otiorhynchus corruptor	(Host, 1789)
Otiorhynchus crataegi	Germar, 1824
Otiorhynchus duinensis	Germar, 1824
Otiorhynchus equestris	(Richter, 1821)
Otiorhynchus frescati	Boheman, 1843
Otiorhynchus fullo	(Schrank, 1781)
Otiorhynchus gemmatus	(Scopoli, 1763)
Otiorhynchus hungaricus	Germar, 1824
Otiorhynchus juglandis	Apfelbeck, 1896
Otiorhynchus kollari	Gyllenhal, 1834
Otiorhynchus labilis	Stierlin, 1883
Otiorhynchus laevigatus	(Fabricius, 1775)
Otiorhynchus lasius	(Germar, 1817)
Otiorhynchus lavandus	Germar, 1824
Otiorhynchus lepidopterus	(Fabricius, 1794)
Otiorhynchus ligustici	(Linnaeus, 1758)
Otiorhynchus lutosus	Stierlin, 1858
Otiorhynchus mandibularis	W. Redtenbacher, 1842
Otiorhynchus maxillosus	Gyllenhal, 1834
Otiorhynchus morio	(Fabricius, 1781)
Otiorhynchus multipunctatus	(Fabricius, 1792)
Otiorhynchus opulentus	Germar, 1837
Otiorhynchus orbicularis	(Herbst, 1795)
Otiorhynchus ovatus	(Linnaeus, 1758)
Otiorhynchus pauxillus	Rosenhauer, 1847
Otiorhynchus perdix	(Olivier, 1807)
Otiorhynchus pinastri	(Herbst, 1795)
Otiorhynchus populeti	Boheman, 1843
Otiorhynchus porcatus	(Herbst, 1795)
Otiorhynchus raucus	(Fabricius, 1776)
Otiorhynchus reichei	Stierlin, 1861
Otiorhynchus repletus	Boheman, 1843
Otiorhynchus roubali	Penecke, 1931
Otiorhynchus rugifrons	(Gyllenhal, 1813)
Otiorhynchus rugosostriatus	(Goeze, 1777)
Otiorhynchus sabulosus	Gyllenhal, 1834
Otiorhynchus scaber	(Linnaeus, 1758)
Otiorhynchus sensitivus	(Scopoli, 1763)
Otiorhynchus similis	F. Solari, 1932
Otiorhynchus sulcatus	(Fabricius, 1775)
Otiorhynchus tenebricosus	(Herbst, 1783)
Otiorhynchus tristis	(Scopoli, 1763)
Otiorhynchus velutinus	Germar, 1824
Otiorhynchus winkleri	Solari, 1937
Otis tarda	Linnaeus, 1758
Otolelus pruinosus	(Kiesenwetter, 1861)
Otophorus haemorrhoidalis	(Linnaeus, 1758)
Otus scops	(Linnaeus, 1758)
Ourapteryx sambucaria	(Linnaeus, 1758)
Hellinsia lienigianus	(Zeller, 1852)
Ovis aries	Linnaeus, 1758
Ovis aries musimon	Linnaeus, 1758
Oxicesta geographica	(Fabricius, 1787)
Oxidus gracilis	(C.L.Koch, 1847)
Oxybelus argentatus	Curtis, 1833
Oxybelus argentatus gerstaeckeri	Curtis, 1833
Oxybelus argentatus treforti	Curtis, 1833
Oxybelus aurantiacus	Mocsáry, 1883
Oxybelus bipunctatus	Olivier, 1811
Oxybelus dissectus	Dahlbom, 1845
Oxybelus dissectus elegans	Dahlbom, 1845
Oxybelus latidens	Gerstaecker, 1867
Oxybelus latro	Olivier, 1811
Oxybelus lineatus	(Fabricius, 1787)
Oxybelus maculipes	Smith, 1856
Oxybelus mandibularis	Dahlbom, 1845
Oxybelus mucronatus	(Fabricius, 1793)
Oxybelus quattuordecimnotatus	Jurine, 1807
Oxybelus subspinosus	Klug, 1835
Oxybelus trispinosus	(Fabricius, 1787)
Oxybelus uniglumis	(Linnaeus, 1758)
Oxybelus variegatus	Wesmael, 1852
Oxybelus victor	Lepeletier, 1845
Oxycarenus lavaterae	(Fabricius, 1787)
Oxycarenus modestus	(Fallén, 1829)
Oxycarenus pallens	(Herrich-Schäffer,1850)
Oxychilus draparnaudi	(H. Beck, 1837)
Oxychilus translucidus	(Mortillet, 1853)
Oxyethira falcata	Morton, 1893
Oxyethira flavicornis	(Pictet, 1834)
Oxyethira tristella	(Klapálek, 1895)
Oxylaemus cylindricus	(Panzer, 1796)
Oxyloma elegans	(Risso, 1826)
Oxymirus cursor	(Linnaeus, 1758)
Oxyomus sylvestris	(Scopoli, 1763)
Oxyopes heterophthalmus	Latreille, 1804
Oxyopes lineatus	Latreille, 1806
Oxyopes ramosus	(Panzer, 1804)
Oxypoda abdominalis	(Mannerheim, 1830)
Oxypoda acuminata	(Stephens, 1832)
Oxypoda alternans	(Gravenhorst, 1802)
Oxypoda annularis	(Mannerheim, 1830)
Oxypoda arborea	Zerche, 1994
Oxypoda brachyptera	(Stephens, 1832)
Oxypoda brevicornis	(Stephens, 1832)
Oxypoda carbonaria	(Heer, 1841)
Oxypoda doderoi	Bernhauer, 1902
Oxypoda exoleta	Erichson, 1839
Oxypoda ferruginea	Erichson, 1839
Oxypoda filiformis	(L.Redtenbacher, 1849)
Oxypoda flavicornis	(Kraatz, 1856)
Oxypoda formiceticola	(Märkel, 1841)
Oxypoda formosa	Kraatz, 1856
Oxypoda haemorrhoa	(Mannerheim, 1830)
Oxypoda induta	Mulsant et Rey, 1861
Oxypoda longipes	Mulsant et Rey, 1861
Oxypoda miranda	Roubal, 1929
Oxypoda mutata	Sharp, 1871
Oxypoda opaca	(Gravenhorst, 1802)
Oxypoda praecox	Erichson, 1839
Oxypoda rufa	Kraatz, 1856
Oxypoda soror	(Thomson, 1855)
Oxypoda spaethi	Bernhauer, 1900
Oxypoda spectabilis	Märkel, 1844
Oxypoda togata	Erichson, 1837
Oxypoda vicina	Kraatz, 1858
Oxypoda vittata	Märkel, 1842
Oxyporus maxillosus	Fabricius, 1792
Oxyporus rufus	(Linnaeus, 1758)
Oxypselaphus obscurus	(Herbst, 1784)
Oxyptilus chrysodactyla	(Denis & Schiffermüller, 1775)
Oxyptilus parvidactyla	(Haworth, 1811)
Oxyptilus pilosellae	(Zeller, 1841)
Oxystoma cerdo	(Gerstäcker, 1854)
Oxystoma craccae	(Linnaeus, 1767)
Oxystoma dimidiatum	(Desbrochers, 1897)
Oxystoma ochropus	(Germar, 1818)
Oxystoma opeticum	(Bach, 1854)
Oxystoma pomonae	(Fabricius, 1798)
Oxystoma subulatum	(Kirby, 1808)
Oxytelus fulvipes	Erichson, 1839
Oxytelus laqueatus	(Marsham, 1802)
Oxytelus migrator	Fauvel, 1904
Oxytelus piceus	(Linnaeus, 1767)
Oxytelus sculptus	Gravenhorst, 1806
Oxythyrea funesta	(Poda, 1761)
Oxytripia orbiculosa	(Esper, 1799)
Oxyura jamaicensis	(Gmelin, 1789)
Oxyura leucocephala	(Scopoli, 1769)
Oxyurella tenuicaudis	(Sars, 1862)
Ozyptila atomaria	(Panzer, 1801)
Cozyptila blackwalli	(Simon, 1875)
Ozyptila brevipes	(Hahn, 1826)
Ozyptila claveata	(Walckenaer, 1837)
Ozyptila praticola	(C.L. Koch, 1837)
Ozyptila pullata	(Thorell, 1875)
Ozyptila rauda	Simon, 1875
Ozyptila sanctuaria	(O.P.-Cambridge, 1871)
Ozyptila scabricula	(Westring, 1851)
Ozyptila simplex	(O.P.-Cambridge, 1862)
Ozyptila trux	(Blackwall, 1846)
Pachetra sagittigera	(Hufnagel, 1766)
Pachnida nigella	(Erichson, 1837)
Pachyatheta cribrata	(Kraatz, 1856)
Pachybrachius fracticollis	(Schilling, 1829)
Pachybrachius luridus	Hahn, 1826
Pachycerus madidus	(Olivier, 1807)
Pachycnemia hippocastanaria	(Hübner, 1799)
Pachygnatha clercki	Sundevall, 1823
Pachygnatha degeeri	Sundevall, 1830
Pachygnatha listeri	Sundevall, 1830
Pachylister inaequalis	(Olivier, 1789)
Pachymerium ferrugineum	(C.L.Koch, 1835)
Pachypodoiulus eurypus	(Attems, 1894)
Pachyrhinus squamulosus	(Herbst, 1795)
Pachyta lamed	(Linnaeus, 1758)
Pachyta quadrimaculata	(Linnaeus, 1758)
Pachythelia villosella	(Ochsenheimer, 1810)
Pachytodes cerambyciformis	(Schrank, 1781)
Pachytodes erraticus	(Dalman, 1817)
Pachytomella parallela	(Meyer-Dür, 1843)
Pachytrachis gracilis	(Brunner von Wattenwyl, 1861)
Pachytychius sparsutus	(Olivier, 1807)
Pactolinus major	(Linnaeus, 1767)
Paederidus carpathicola	Scheerpeltz, 1957
Paederidus rubrothoracicus	(Goeze, 1777)
Paederidus ruficollis	(Fabricius, 1781)
Paederus balcanicus	Koch, 1938
Paederus brevipennis	Lacordaire, 1835
Paederus caligatus	Erichson, 1840
Paederus fuscipes	Curtis, 1826
Paederus limnophilus	Erichson, 1840
Paederus littoralis	Gravenhorst, 1802
Paederus riparius	(Linnaeus, 1758)
Paederus schoenherri	Czwalina, 1889
Pagiphora annulata	(Brullé, 1832)
Pagodulina pagodula	(Des Moulins, 1830)
Pagodulina pagodula altilis	(Des Moulins, 1830)
Paidia rica	(Freyer, 1858)
Paidiscura pallens	(Blackwall, 1834)
Palaeolecanium bituberculatum	(Targioni-Tozzetti,1868)
Palarus variegatus	(Fabricius, 1781)
Paleococcus fuscipennis	(Burmeister,1835)
Palingenia longicauda	(Olivier, 1791)
Palliduphantes alutacius	(Simon, 1884)
Palliduphantes istrianus	(Kulczynski, 1914)
Palliduphantes liguricus	(Simon, 1929)
Palliduphantes pallidus	(O.P.-Cambridge, 1871)
Paramesia gnomana	(Clerck, 1759)
Palliduphantes pillichi	(Kulczynski, 1915)
Ovalisia festiva	(Linnaeus, 1767)
Palmitia massilialis	(Duponchel, 1832)
Palmodes occitanicus	(Lepeletier & Serville, 1828)
Palomena prasina	(Linnaeus, 1761)
Palomena viridissima	(Poda, 1761)
Palorus depressus	(Fabricius, 1790)
Palorus ratzeburgi	(Wissmann, 1848)
Palorus subdepressus	(Wollaston, 1864)
Palpares libelluloides	(Linnaeus, 1764)
Palpita vitrealis	(Rossi, 1794)
Paluda flaveola	Boheman, 1845
Pammene agnotana	Rebel, 1914
Pammene albuginana	(Guenée, 1845)
Pammene amygdalana	(Duponchel, 1843)
Pammene argyrana	(Hübner, 1799)
Pammene aurana	(Fabricius, 1775)
Pammene aurita	Razowski, 1991
Pammene christophana	(Möschler, 1862)
Pammene fasciana	(Linnaeus, 1761)
Pammene gallicana	(Guenée, 1845)
Pammene gallicolana	(Lienig & Zeller, 1846)
Pammene germmana	(Hübner, 1799)
Pammene giganteana	(Preyerimhoff, 1863)
Pammene insulana	(Guenée, 1845)
Pammene obscurana	(Stephens, 1834)
Pammene ochsenheimeriana	(Lienig & Zeller, 1846)
Pammene querceti	Gozmány, 1957
Pammene regiana	(Zeller, 1849)
Pammene rhediella	(Clerck, 1759)
Pammene spiniana	(Duponchel, 1843)
Pammene splendidulana	(Guenée, 1845)
Pammene suspectana	(Lienig & Zeller, 1846)
Pammene trauniana	(Denis & Schiffermüller, 1775)
Panagaeus bipustulatus	(Fabricius, 1775)
Panagaeus cruxmajor	(Linnaeus, 1758)
Panamomops affinis	Miller et Kratochvil, 1939
Panamomops fagei	Miller et Kratochvil, 1939
Panamomops mengei	Simon, 1926
Panamomops sulcifrons	(Wider, 1834)
Panaorus adspersus	(Mulsant & Rey, 1852)
Pancalia leuwenhoekella	(Linnaeus, 1761)
Pancalia schwarzella	(Fabricius, 1798) comb.n.
Panchrysia deaurata	(Esper, 1787)
Pandemis cerasana	(Hübner, 1786)
Pandemis cinnamomeana	(Treitschke, 1830)
Pandemis corylana	(Fabricius, 1794)
Pandemis dumetana	(Treitschke, 1835)
Pandemis heparana	(Denis & Schiffermüller, 1775)
Pandion haliaetus	(Linnaeus, 1758)
Panemeria tenebrata	(Scopoli, 1763)
Pangonius pyritosus	(Loew, 1859)
Pangus scaritides	(Sturm, 1818)
Panolis flammea	([Denis & Schiffermüller], 1775)
Pantacordis pales	Gozmány, 1954
Pantallus alboniger	(Lethierry, 1889)
Panthea coenobita	(Esper, 1785)
Pantilius tunicatus	(Fabricius, 1781)
Panurginus labiatus	(Eversmann, 1852)
Panurgus calcaratus	(Scopoli, 1763)
Panurgus dentipes	Latreille, 1811
Panurus biarmicus	(Linnaeus, 1758)
Panurus biarmicus russicus	(Linnaeus, 1758)
Paophilus afflatus	(Boheman, 1833)
Paophilus hampei	(Seidlitz, 1867)
Papilio machaon	Linnaeus, 1758
Paraboarmia viertlii	(Bohatsch, 1883)
Paracaloptenus caloptenoides	(Brunner on Wattenwyl, 1861)
Paracandona euplectella	(Robertson, 1889)
Paracardiophorus musculus	(Erichson, 1840)
Parachiona picicornis	(Pictet, 1834)
Parachronistis albiceps	(Zeller, 1839)
Paracolax tristalis	(Fabricius, 1794)
Paracorixa concinna	(Fieber, 1848)
Paracorsia repandalis	(Denis & Schiffermüller, 1775)
Paracorymbia fulva	(De Geer, 1775)
Paracorymbia maculicornis	(De Geer, 1775)
Paracyclops affinis	(Sars, 1863)
Paracyclops fimbriatus	(Fischer, 1853)
Paracyclops imminutus	Kiefer, 1929
Paracyclops poppei	(Rehberg, 1880)
Paracylindromorphus subuliformis	(Mannerheim, 1837)
Paradarisa consonaria	(Hübner, 1799)
Paradelphacodes paludosus	(Flor, 1861)
Paradorydium paradoxum	(Herrich-Schäffer, 1837)
Paradrina clavipalpis	(Scopoli, 1763)
Paradromius linearis	(Olivier, 1795)
Paradromius longiceps	(Dejean, 1826)
Parafairmairia bipartita	(Signoret,1872)
Parafairmairia gracilis	Green,1916
Parafomoria helianthemella	(Herrich-Schäffer, 1860)
Parafoucartia squamulata	(Herbst, 1795)
Paragus albifrons	(Fallén, 1817)
Paragus bicolor	(Fabricius, 1794)
Paragus cinctus	Schiner & Egger, 1853
Paragus finitimus	Goldlin de Tiefenau, 1971
Paragus haemorrhous	Meigen, 1822
Paragus majoranae	Rondani, 1857
Paragus medeae	Stanescu, 1991
Paragus quadrifasciatus	Meigen, 1822
Paragus tibialis	(Fallén, 1817)
Paragymnomerus spiricornis	(Spinola, 1808)
Parahypopta caestrum	(Hübner, 1808)
Parainocellia braueri	(Albarda, 1891)
Paraleptophlebia cincta	(Retzius, 1783)
Paraleptophlebia submarginata	(Stephens, 1835)
Paraleptophlebia werneri	Ulmer, 1920
Paraliburnia adela	(Flor, 1861)
Paralimnus phragmitis	(Boheman, 1847)
Paralimnus rotundiceps	(Lethierry, 1885)
Paralimnus zachvatkini	Emeljanov, 1964
Paralipsa gularis	(Zeller, 1877)
Leucoptera sinuella	(Reutti, 1853)
Paramecosoma melanocephalum	(Herbst, 1793)
Parameotica hydra	Ádám, 1987
Parameotica laticeps	(Thomson, 1856)
Paramesus major	Haupt, 1927
Paramesus obtusifrons	(Stal, 1853)
Paramesus taeniatus	Horváth, 1911
Parammobatodes minutus	(Mocsáry, 1878)
Paranchus albipes	(Fabricius, 1796)
Paranthrene insolita polonica	Le Cerf, 1914
Paranthrene tabaniformis	(Rottemburg, 1775)
Paraphotistus impressus	(Fabricius, 1792)
Paraphotistus nigricornis	(Panzer, 1799)
Mecostethus parapleurus	(Hagenbach, 1822)
Parapotes reticulatus	(Horváth, 1897)
Parapoynx nivalis	(Denis & Schiffermüller, 1775)
Parapoynx stratiotata	(Linnaeus, 1758)
Parapsallus vitellinus	(Scholtz, 1846)
Pararge aegeria	(Linnaeus, 1758)
Parascotia fuliginaria	(Linnaeus, 1761)
Parascythris muelleri	(Mann, 1871)
Parasemia plantaginis	(Linnaeus, 1758)
Parasemidalis fuscipennis	(Reuter, 1894)
Parasetodes respersellus	(Rambur, 1842)
Parastenocaris budapestiensis	Török, 1955
Parastenocaris entzii	Török, 1955
Parastenocaris pannonicus	Török, 1955
Parastenocaris similis	Török, 1955
Parastenocaris törökae	Ponyi, 1957
Parastichtis suspecta	(Hübner, 1817)
Parastichtis ypsillon	([Denis & Schiffermüller], 1775)
Paraswammerdamia nebulella	(Goeze, 1783)
Parasyrphus annulatus	(Zetterstedt, 1838)
Parasyrphus lineola	(Zetterstedt, 1843)
Parasyrphus macularis	(Zetterstedt, 1843)
Parasyrphus malinellus	(Collin, 1952)
Parasyrphus nigritarsis	(Zetterstedt, 1843)
Parasyrphus punctulatus	(Verrall, 1843)
Parasyrphus vittiger	(Zetterstedt, 1843)
Paratachys bistriatus	(Duftschmid, 1812)
Paratachys fulvicollis	(Dejean, 1831)
Paratachys micros	(Fischer, 1828)
Paratachys turkestanicus	(Csiki, 1928)
Paratalanta hyalinalis	(Hübner, 1796)
Paratalanta pandalis	(Hübner, 1825)
Parazuphium chevrolati praepannonicum	(Castelnau, 1833)
Parazygiella montana	(C. L. Koch, 1834)
Pardosa agrestis	(Westring, 1862)
Pardosa agricola	(Thorell, 1856)
Pardosa alacris	(C. L. Koch, 1833)
Pardosa amentata	(Clerck, 1757)
Pardosa bifasciata	(C.L. Koch, 1834)
Pardosa cribrata	Simon, 1876
Pardosa hortensis	(Thorell, 1872)
Pardosa lugubris	(Walckenaer, 1802)
Pardosa maisa	Hippa at Mannila, 1982
Pardosa monticola	(Clerck, 1757)
Pardosa morosa	(L. Koch, 1870)
Pardosa nebulosa	(Thorell, 1872)
Pardosa paludicola	(Clerck, 1757)
Pardosa palustris	(Linnaeus, 1758)
Pardosa prativaga	(L. Koch, 1870)
Pardosa proxima	(C.L. Koch, 1847)
Pardosa pullata	(Clerck, 1757)
Pardosa riparia	(C.L. Koch, 1833)
Parectopa ononidis	(Zeller, 1839)
Parectopa robiniella	Clemens, 1863
Parectropis similaria	(Hufnagel, 1767)
Paredrocoris pectoralis	Reuter, 1878
Parergasilus rylovi	Markewitsch, 1937
Parethelcus pollinarius	(Forster, 1771)
Pareulype berberata	(Denis & Schiffermüller, 1775)
Parexarnis fugax	(Treitschke, 1825)
Parhelophilus frutetorum	(Fabricius, 1775)
Parhelophilus versicolor	(Fabricius, 1794)
Parlatoria oleae	(Colvée,1880)
Parnassius apollo	(Linnaeus, 1758)
Parnassius mnemosyne	(Linnaeus, 1758)
Parnopes grandior	(Pallas, 1771)
Parocyusa longitarsis	(Erichson, 1839)
Parocyusa rubicunda	(Erichson, 1837)
Parodontodynerus ephippium	(Klug, 1817)
Oecetis struckii	Klapalek, 1903
Paromalus flavicornis	(Herbst, 1792)
Paromalus parallelepipedus	(Herbst, 1792)
Parophonus complanatus	(Dejean, 1829)
Parophonus hirsutulus	(Dejean, 1829)
Parophonus maculicornis	(Duftschmid, 1812)
Parophonus mendax	(Rossi, 1790)
Parornix anglicella	(Stainton, 1850)
Parornix anguliferella	(Zeller, 1847)
Parornix betulae	(Stainton, 1854)
Parornix carpinella	(Frey, 1863)
Parornix devoniella	(Stainton, 1850)
Parornix fagivora	(Frey, 1861)
Parornix finitimella	(Zeller, 1850)
Parornix petiolella	(Frey, 1863)
Parornix scoticella	(Stainton, 1850)
Parornix szocsi	Gozmány, 1952
Parornix tenella	(Rebel, 1919)
Parornix torquillella	(Zeller, 1850)
Parthenolecanium corni	(Bouché,1844)
Parthenolecanium fletcheri	(Cockerell,1893)
Parthenolecanium persicae	(Fabricius,1776)
Parthenolecanium pomeranicum	(Kawecki,1954)
Parthenolecanium rufulum	(Cockerell,1903)
Parthenolecanium smreczynskii	Kawecki,1967
Parus ater	Linnaeus, 1758
Parus caeruleus	Linnaeus, 1758
Parus cristatus	Linnaeus, 1758
Parus major	Linnaeus, 1758
Parus montanus	Conrad von Baldenstein, 1827
Parus montanus borealis	Conrad von Baldenstein, 1827
Parus palustris	Linnaeus, 1758
Parus palustris stagnatilis	Linnaeus, 1758
Pasiphila chloerata	(Mabille, 1870)
Pasiphila debiliata	(Hübner, 1817)
Pasiphila rectangulata	(Linnaeus, 1758)
Pasites maculatus	Jurine, 1807
Passaloecus clypealis	Faester, 1947
Passaloecus corniger	Shuckard, 1837
Passaloecus gracilis	Curtis, 1834
Passaloecus insignis	(Vander Linden, 1829)
Passaloecus singularis	Dahlbom, 1844
Passer domesticus	(Linnaeus, 1758)
Passer montanus	(Linnaeus, 1758)
Pastiroma clypeata	(Horváth, 1897)
Pastiroma odessana	(Dlabola, 1958)
Patrobus atrorufus	(Stroem, 1768)
Patrobus styriacus	Chaudoir, 1871
Polypogon strigilata	(Linnaeus, 1758)
Pedestredorcadion decipiens	(Germar, 1824)
Pedestredorcadion pedestre	(Poda, 1761)
Pedestredorcadion scopolii	(Herbst, 1784)
Pediacus depressus	(Herbst, 1797)
Pediacus dermestoides	(Fabricius, 1792)
Pediasia aridella	(Thunberg, 1788)
Pediasia contaminella	(Hübner, 1796)
Pediasia fascelinella	(Hübner, 1813)
Pediasia jucundellus	(Herrich-Schäffer, 1847)
Pediasia luteella	(Denis & Schiffermüller, 1775)
Pediasia matricella	(Treitschke, 1832)
Pediculota montandoni	(Roubal, 1909)
Pedilophorus auratus	(Duftschmid, 1825)
Pedilophorus obscurus	Fabbri et Allemand, 2003
Pedilophorus senii	Fabbri et Allemand, 2003
Pedinus fallax	Mulsant et Rey, 1853
Pedinus fallax gracilis	Mulsant et Rey, 1853
Pedinus femoralis	(Linnaeus, 1767)
Pedinus hungaricus	Seidlitz, 1893
Pediopsis tiliae	(Germar, 1831)
Pedostrangalia revestita	(Linnaeus, 1767)
Pelatea klugiana	(Freyer, 1834)
Pelecanus crispus	Bruch, 1832
Pelecanus onocrotalus	Linnaeus, 1758
Pelecanus rufescens	Gmelin, 1789
Pelecocera latifrons	Loew, 1856
Pelecocera tricincta	Meigen, 1822
Pelecopsis elongata	(Wider, 1834)
Pelecopsis loksai	Szinetár et Samu, 2003
Pelecopsis mengei	(Simon, 1894)
Pelecopsis parallela	(Wider, 1834)
Pelecopsis radicicola	(L. Koch, 1872)
Pelecotoma fennica	(Paykull, 1799)
Pelecus cultratus	(Linnaeus, 1758)
Pelenomus canaliculatus	(Fahraeus, 1843)
Pelenomus commari	(Panzer, 1794)
Pelenomus quadricorniger	Colonnelli, 1986
Pelenomus quadrituberculatus	(Fabricius, 1787)
Pelenomus velaris	(Gyllenhal, 1827)
Pelenomus waltoni	(Boheman, 1843)
Peliococcopsis parviceraria	(Goux,1937)
Peliococcus balteatus	(Green,1928)
Peliococcus perfidiosus	Borchsenius,1931
Peliococcus slavonicus	(Laing,1929)
Pellenes nigrociliatus	(Simon, 1875)
Pellenes tripunctatus	(Walckenaer, 1802)
Pelobates fuscus	(Laurenti, 1768)
Pelochares versicolor	(Waltl, 1838)
Pelochrista arabescana	(Eversmann, 1844)
Pelochrista caecimaculana	(Hübner, 1799)
Pelochrista decolorana	(Freyer, 1842)
Pelochrista hepatariana	(Herrich-Schäffer, 1851)
Pelochrista infidana	(Hübner, 1824)
Pelochrista latericiana	(Rebel, 1919)
Pelochrista modicana	(Zeller, 1847)
Pelochrista mollitana	(Zeller, 1847)
Pelochrista subtiliana	(Jäckh, 1960)
Pelosia muscerda	(Hufnagel, 1766)
Pelosia obtusa	(Herrich-Schäffer, 1847)
Peltodytes caesus	(Duftschmid, 1805)
Pelurga comitata	(Linnaeus, 1758)
Pempelia albariella	Zeller, 1839
Pempelia formosa	(Haworth, 1811)
Pempelia geminella	(Eversmann, 1844)
Pempelia obductella	Zeller, 1839
Pempelia palumbella	(Denis & Schiffermüller, 1775)
Pempeliella dilutella	(Denis & Schiffermüller, 1775)
Pempeliella ornatella	(Denis & Schiffermüller, 1775)
Pempeliella sororiella	Zeller, 1839)
Pemphredon austriacus	(Kohl, 1888)
Pemphredon balticus	Merisuo, 1972
Pemphredon clypealis	Thomson, 1870
Pemphredon inornatus	Say, 1824
Pemphredon lethifer	(Shuckard, 1837)
Pemphredon lugens	Dahlbom, 1842
Pemphredon lugubris	(Fabricius, 1793)
Pemphredon montanus	Dahlbom, 1845
Pemphredon morio	Vander Linden, 1829
Pemphredon podagricus	Chevrier, 1870
Pemphredon rugifer	Dahlbom, 1844
Pennisetia hylaeiformis	(Laspeyres, 1801)
Pentaphyllus chrysomeloides	(Pallas, 1781)
Pentaphyllus testaceus	(Hellwig, 1792)
Pentaria badia	(Rosenhauer, 1847)
Pentastira major	(Kirschbaum, 1868)
Pentastira rorida	Fieber, 1876
Pentastiridius fumatipennis	Dlabola, 1949
Pentastiridius leporinus	(Linnaeus, 1761)
Pentastiridius pallens	(Germar, 1821)
Pentatoma rufipes	(Linnaeus, 1758)
Penthimia nigra	(Goeze, 1778)
Penthophera morio	(Linnaeus, 1767)
Pentodon idiota	(Herbst, 1789)
Peponocranium orbiculatum	(O.P.-Cambridge, 1882)
Perapion affine	(Kirby, 1808)
Perapion curtirostre	(Germar, 1817)
Perapion lemoroi	(Ch. Brisout, 1880)
Perapion marchicum	(Herbst, 1797)
Perapion oblongum	(Gyllenhal, 1839)
Perapion violaceum	(Kirby, 1808)
Perca fluviatilis	(Linnaeus, 1758)
Perccottus glenii	Dybowski, 1877
Perconia strigillaria	(Hübner, 1787)
Perdix perdix	(Linnaeus, 1758)
Perforatella bidentata	(Gmelin, 1791)
Perforatella dibothrion	(M. von Kimakowicz, 1884)
Peribatodes rhomboidaria	(Denis & Schiffermüller, 1775)
Phloeopora opaca	Bernhauer, 1902
Peribatodes secundaria	(Denis & Schiffermüller, 1775)
Peribatodes umbraria	(Hübner, 1809)
Pericallia matronula	(Linnaeus, 1758)
Pericartiellus telephii	(Bedel, 1900)
Periclepsis cinctana	(Denis & Schiffermüller, 1775)
Peridea anceps	(Goeze, 1781)
Peridroma saucia	(Hübner, 1808)
Perigona nigriceps	(Dejean, 1831)
Perigrapha i-cinctum	([Denis & Schiffermüller], 1775)
Perileptus areolatus	(Creutzer, 1799)
Perinephela lancealis	(Denis & Schiffermüller, 1775)
Periphanes delphinii	(Linnaeus, 1758)
Peritelus familiaris	Boheman, 1834
Peritrechus geniculatus	(Hahn, 1831)
Peritrechus gracilicornis	(Puton, 1877)
Peritrechus lundii	(Gmelin, 1790)
Peritrechus meridionalis	Puton, 1877
Peritrechus nubilus	(Fallén, 1807)
Perittia herrichiella	(Herrich-Schäffer, 1855)
Perizoma affinitata	(Stephens, 1831)
Perizoma albulata	(Denis & Schiffermüller, 1775)
Perizoma alchemillata	(Linnaeus, 1758)
Perizoma bifaciata	(Haworth, 1809)
Perizoma blandiata	(Denis & Schiffermüller, 1775)
Perizoma flavofasciata	(Thunberg, 1792)
Perizoma hydrata	(Treitschke, 1829)
Perizoma lugdunaria	(Herrich-Schäffer, 1855)
Perizoma minorata	(Treitschke, 1828)
Perla bipunctata	Pictet, 1833
Perla burmeisteriana	Claassen, 1936
Perla marginata	(Panzer, 1799)
Perla pallida	Guérin, 1838
Perlodes dispar	(Rambur, 1842)
Perlodes microcephalus	(Pictet, 1833)
Pernis apivorus	(Linnaeus, 1758)
Petasina bakowskii	(Poliński, 1924)
Petasina filicina	(L. Pfeiffer, 1841)
Petasina unidentata	(Draparnaud, 1805)
Petrophora chlorosata	(Scopoli, 1763)
Pexicopia malvella	(Hübner, 1805)
Peyerimhoffina gracilis	(Schneider, 1841)
Pezotettix giornae	(Rossi, 1794)
Phacophallus parumpunctatus	(Gyllenhal, 1827)
Phaenops cyanea	(Fabricius, 1775)
Phaenops formaneki	(Jakobson, 1912)
Phaeocedus braccatus	(L. Koch, 1866)
Phaeochrotes pudens	(Gyllenhal, 1833)
Phaeostigma major	(Burmeister, 1839)
Phaeostigma notata	(Fabricius, 1781)
Phaeostigma setulosa	(H. Aspöck & U. Aspöck, 1967)
Phaiogramma etruscaria	(Zeller, 1849)
Phalacrocorax carbo	(Linnaeus, 1758)
Phalacrocorax carbo sinensis	(Linnaeus, 1758)
Phalacrocorax pygmeus	(Pallas, 1773)
Phalacronothus biguttatus	(Germar, 1824)
Phalacronothus citellorum	(Semenov et Medvedev, 1928)
Euorodalus paracoenosus	(Balthasar & Hrubant, 1960)
Esymus pusillus	(Herbst, 1789)
Eudolus quadriguttatus	(Herbst, 1783)
Phalacronothus quadrimaculatus	(Linnaeus, 1761)
Phalacrus caricis	Sturm, 1807
Phalacrus championi	Guillebeau, 1813
Phalacrus corruscus	(Panzer, 1797)
Phalacrus fimetarius	(Fabricius, 1775)
Phalacrus grossus	Erichson, 1845
Phalacrus substriatus	Gyllenhal, 1813
Phalaropus fulicarius	(Linnaeus, 1758)
Phalaropus lobatus	(Linnaeus, 1758)
Phalera bucephala	(Linnaeus, 1758)
Phalera bucephaloides	(Ochsenheimer, 1810)
Phalonidia affinitana	(Douglas, 1846)
Phalonidia albipalpana	(Zeller, 1847)
Phalonidia contractana	(Zeller, 1847)
Phalonidia curvistrigana	(Stainton, 1859)
Phalonidia gilvicomana	(Zeller, 1847)
Phalonidia manniana	(Fischer v. Röslerstamm, 1839)
Phaneroptera falcata	(Poda, 1761)
Phaneroptera nana	Fieber, 1853
Phaneta pauperana	(Duponchel, 1843)
Phantia subquadrata	(Herrich-Schäffer, 1838)
Pharmacis carna	(Denis & Schiffermüller, 1775)
Pharmacis fusconebulosa	(DeGeer, 1778)
Phasianus colchicus	Linnaeus, 1758
Phasianus colchicus torquatus	Linnaeus, 1758
Phaulernis rebeliella	Gaedike, 1966
Pheletes aeneoniger	(De Geer, 1774)
Pheletes quercus	(Olivier, 1790)
Phenacoccus aceris	(Signoret,1875)
Phenacoccus avenae	Borchsenius,1949
Phenacoccus bicerarius	Borchsenius,1949
Phenacoccus evelinae	(Tereznikova,1968)
Phenacoccus hordei	(Lindeman,1886)
Phenacoccus incertus	(Kiritchenko,1940)
Phenacoccus interruptus	Green,1923
Phenacoccus mespili	(Signoret,1875)
Phenacoccus phenacoccoides	(Kiritchenko,1932)
Phenacoccus piceae	Löw,1883
Phenacoccus pumilus	Kiritchenko,1935
Pheosia gnoma	(Fabricius, 1776)
Pheosia tremula	(Clerck, 1759)
Phiaris metallicana	(Hübner, 1799)
Phiaris micana	(Denis & Schiffermüller, 1775)
Phiaris obsoletana	(Zetterstedt, 1839)
Phiaris scoriana	(Guenée, 1845)
Phiaris stibiana	(Guenée, 1845)
Phiaris umbrosana	(Freyer, 1842)
Phibalapteryx virgata	(Hufnagel, 1767)
Philaenus spumarius	(Linnaeus, 1758)
Philaeus chrysops	(Poda, 1761)
Philaia jassargiforma	Dlabola, 1952
Philanthus coronatus	(Thunberg, 1784)
Philanthus triangulum	(Fabricius, 1775)
Philanthus venustus	(Rossi, 1790)
Philedone gerningana	(Denis & Schiffermüller, 1775)
Philedonides lunana	(Thunberg, 1784)
Philedonides rhombicana	(Herrich-Schäffer, 1851)
Philereme transversata	(Hufnagel, 1767)
Philereme vetulata	(Denis & Schiffermüller, 1775)
Philhygra balcanicola	(Scheerpeltz, 1968)
Philhygra elongatula	(Gravenhorst, 1802)
Philhygra hygrobia	(Thomson, 1856)
Philhygra hygrotopora	(Kraatz, 1856)
Philhygra kaiseriana	(Brundin, 1943)
Philhygra luridipennis	(Mannerheim, 1830)
Philhygra malleus	(Joy, 1913)
Philhygra melanocera	(Thomson, 1856)
Philhygra nannion	(Joy, 1931)
Philhygra palustris	(Kiesenwetter, 1844)
Philhygra scotica	(Elliman, 1919)
Philhygra sequanica	(Ch.Brisout de Barneville, 1859)
Philhygra terminalis	(Gravenhorst, 1806)
Philhygra tmolosensis	(Bernhauer, 1940)
Philhygra vindobonsensis	(Brundin, 1943)
Philhygra volans	(W.Scriba, 1859)
Philipomyia aprica	(Meigen, 1820)
Philipomyia graeca	(Fabricius, 1794)
Philodromus albidus	Kulczynski, 1911
Philodromus aureolus	(Clerck, 1757)
Philodromus buxi	Simon, 1884
Philodromus cespitum	(Walckenaer, 1802)
Philodromus collinus	C.L. Koch, 1835
Philodromus dispar	Walckenaer, 1826
Philodromus emarginatus	(Schrank, 1803)
Philodromus fallax	Sundevall, 1833
Philodromus fuscomargitatus	(Dee Geer, 1778)
Philodromus histrio	(Latreille, 1819)
Philodromus longipalpis	Simon, 1870
Philodromus margaritatus	(Clerck, 1757)
Philodromus poecilus	(Thorell, 1872)
Philodromus praedatus	O.P.-Cambridge, 1871
Philodromus rufus	Walckenaer, 1826
Philomachus pugnax	(Linnaeus, 1758)
Philonthus addendus	Sharp, 1867
Philonthus alberti	Schillhammer, 2000
Philonthus albipes	(Gravenhorst, 1802)
Philonthus alpinus	Eppelsheim, 1875
Philonthus atratus	(Gravenhorst, 1802)
Philonthus carbonarius	(Gravenhorst, 1802)
Philonthus caucasicus	(Nordmann, 1837)
Philonthus cochleatus	Scheerpeltz, 1937
Philonthus cognatus	(Stephens, 1832)
Philonthus concinnus	(Gravenhorst, 1802)
Philonthus confinis	A.Strand, 1941
Philonthus coprophilus	Jarrige, 1949
Philonthus corruscus	(Gravenhorst, 1802)
Philonthus corvinus	Erichson, 1839
Philonthus cruentatus	(Gmelin, 1790)
Philonthus cyanipennis	(Fabricius, 1792)
Philonthus debilis	(Gravenhorst, 1802)
Philonthus decorus	(Gravenhorst, 1802)
Philonthus discoideus	(Gravenhorst, 1802)
Philonthus diversiceps	(Bernhauer, 1901)
Philonthus ebeninus	(Gravenhorst, 1802)
Philonthus frigidus	Märkel et Kiesenwetter, 1848
Philonthus fumarius	(Gravenhorst, 1806)
Philonthus intermedius	(Lacordaire, 1835)
Philonthus laevicollis	(Lacordaire, 1835)
Philonthus laminatus	(Creutzer, 1799)
Philonthus lepidus	(Gravenhorst, 1802)
Philonthus longicornis	Stephens, 1832
Philonthus mannerheimi	Fauvel, 1869
Philonthus marginatus	(O.F.Müller, 1764)
Philonthus micans	(Gravenhorst, 1802)
Philonthus nigrita	(Gravenhorst, 1806)
Philonthus nitidicollis	(Lacordaire, 1835)
Philonthus nitidus	(Fabricius, 1787)
Philonthus parvicornis	(Gravenhorst, 1802)
Philonthus politus	(Linnaeus, 1758)
Philonthus puella	Nordmann, 1837
Philonthus punctus	(Gravenhorst, 1802)
Philonthus quisquiliarius	(Gyllenhal, 1810)
Philonthus rectangulus	(Sharp, 1874)
Philonthus rotundicollis	(Ménétries, 1832)
Philonthus rubripennis	Stephens, 1832
Philonthus rufimanus	Heer, 1839
Philonthus rufipes	(Stephens, 1832)
Philonthus salinus	(Kiesenwetter, 1844)
Philonthus sanguinolentus	(Gravenhorst, 1802)
Philonthus spinipes	(Sharp, 1874)
Philonthus splendens	(Fabricius, 1793)
Philonthus succicola	(Thomson, 1860)
Philonthus tenuicornis	(Rey, 1853)
Philonthus umbratilis	(Gravenhorst, 1802)
Philonthus varians	(Paykull, 1789)
Philonthus ventralis	(Gravenhorst, 1802)
Philonthus viridipennis	Fauvel, 1875
Philopedon plagiatum	(Schaller, 1783)
Philopotamus ludificatus	McLachlan, 1878
Philopotamus montanus	(Donovan, 1813)
Philopotamus variegatus	(Scopoli, 1763)
Philorhizus melanocephalus	(Dejean, 1825)
Philorhizus notatus	(Stephens, 1828)
Philorhizus quadrisignatus	(Dejean, 1825)
Philorhizus sigma	(Rossi, 1790)
Philothermus evanescens	(Reitter, 1876)
Philothermus semistriatus	(Perris, 1865)
Phimodera amblygonia	Fieber, 1863
Phimodera flori	Fieber, 1863
Phimodera humeralis	(Dalmann, 1823)
Phintella castriesiana	(Grube, 1861)
Phlegra cinereofasciata	(Simon, 1868)
Phlegra fasciata	(Hahn, 1826)
Phlepsius intricatus	(Herrich-Schäffer, 1838)
Phloeocharis subtilissima	Mannerheim, 1830
Phloeonomus minimus	(Erichson, 1839)
Phloeonomus punctipennis	Thomson, 1867
Phloeonomus pusillus	(Gravenhorst, 1806)
Phloeophagus lignarius	(Marsham, 1802)
Phloeophagus thompsoni	(Grill, 1896)
Phloeophagus turbatus	Schönherr, 1845
Phloeotribus rhododactylus	(Marsham, 1802)
Phloeopora corticalis	(Gravenhorst, 1802)
Phloeopora nitidiventris	Fauvel, 1900
Phloeopora scribae	(Eppelsheim, 1884)
Phloeopora teres	(Gravenhorst, 1802)
Phloeopora testacea	(Mannerheim, 1830)
Phloeosinus aubei	(Perris, 1855)
Phloeosinus thujae	(Perris, 1855)
Phloeostiba lapponica	(Zetterstedt, 1838)
Phloeostiba plana	(Paykull, 1792)
Phloeostichus denticollis	Redtenbacher, 1842
Phloeotribus caucasicus	Reitter, 1891
Phloeotribus scarabaeoides	(Bernard, 1788)
Phlogophora meticulosa	(Linnaeus, 1758)
Phlogophora scita	(Hübner, 1790)
Phlogotettix cyclops	(Mulsant et Rey, 1855)
Phloiophilus edwardsi	Stephens, 1830
Phloiotrya tenuis	(Hampe, 1850)
Phlyctaenia coronata	(Hufnagel, 1767)
Phlyctaenia perlucidalis	(Hübner, 1809)
Phlyctaenia stachydalis	(Germar, 1821)
Phlyctaenodes cruentalis	(Geyer, 1832)
Phoenicocoris modestus	(Meyer-Dür, 1843)
Phoenicocoris obscurellus	(Fallén, 1829)
Phoenicopterus minor	Geoffroy, 1798
Phoenicopterus ruber	Linnaeus, 1758
Phoenicopterus roseus	Pallas, 1811
Phoenicurus ochruros	(S. G. Gmelin, 1774)
Phoenicurus ochruros gibraltariensis	(S. G. Gmelin, 1774)
Phoenicurus phoenicurus	(Linnaeus, 1758)
Platnickina tincta	(Walckenaer, 1802)
Pholcus opilionoides	(Schrank, 1781)
Pholcus phalangioides	(Fuesslin, 1775)
Pholidoptera aptera	(Fabricius, 1793)
Pholidoptera fallax	(Fischer, 1853)
Pholidoptera griseoaptera	(DeGeer, 1773)
Pholidoptera littoralis	Fieber, 1853
Pholidoptera transsylvanica	(Fischer, 1853)
Pholioxenus schatzmayri	(P.W.J. Müller, 1910)
Phosphaenus hemipterus	(Geoffroy, 1762)
Phosphuga atrata	(Linnaeus, 1758)
Photedes captiuncula	(Treitschke, 1825)
Photedes captiuncula delattini	(Treitschke, 1825)
Photedes minima	(Haworth, 1809)
Phoxinus phoxinus	(Linnaeus, 1758)
Phradonoma villosulum	(Duftschmid, 1825)
Phragmataecia castaneae	(Hübner, 1790)
Phragmatiphila nexa	(Hübner, 1808)
Phragmatobia fuliginosa	(Linnaeus, 1758)
Phragmatobia luctifera	([Denis & Schiffermüller], 1775)
Phrissotrichum rugicolle	(Germar, 1817)
Phrurolithus festivus	(C.L. Koch, 1835)
Phrurolithus minimus	C.L. Koch, 1839
Phrurolithus pullatus	Kulczynski, 1897
Phrurolithus szilyi	(Herman, 1879)
Phrydiuchus speiseri	(Schultze, 1897)
Phrydiuchus tau	Warner, 1969
Phrydiuchus topiarius	(Germar, 1824)
Phryganea bipunctata	Retzius, 1783
Phryganea grandis	Linnaeus, 1758
Phtheochroa annae	Huemer,1990
Phtheochroa duponchelana	(Duponchel, 1843)
Phtheochroa fulvicinctana	(Constant, 1893)
Phtheochroa inopiana	(Haworth, 1811)
Phtheochroa procerana	(Lederer, 1853)
Phtheochroa pulvillana	(Herrich-Schäffer, 1851)
Phtheochroa purana	(Guenée, 1845)
Phtheochroa rugosana	(Hübner, 1799)
Phtheochroa schreibersiana	(Frölich, 1828)
Phtheochroa sodaliana	(Haworth, 1811)
Phthorimaea operculella	(Zeller, 1873)
Phycita meliella	(Mann, 1864)
Phycita metzneri	(Zeller, 1846)
Phycita roborella	(Denis & Schiffermüller, 1775)
Phycitodes albatella	(Ragonot, 1887)
Phycitodes binaevella	(Hübner, 1813)
Phycitodes inquinatella	(Ragonot, 1887)
Phycitodes lacteella	(Rothschild, 1915)
Phycitodes maritima	(Tengström, 1848)
Phycitodes saxicola	(Vaughan, 1870)
Phyllobius arborator	(Herbst, 1797)
Phyllobius argentatus	(Linnaeus, 1758)
Phyllobius betulinus	(Bechstein & Scharfenberg, 1805)
Phyllobius brevis	Gyllenhal, 1834
Phyllobius chloropus	(Linné, 1758)
Phyllobius dispar	L. Redtenbacher, 1849
Phyllobius glaucus	(Scopoli, 1763)
Phyllobius maculicornis	(Germar, 1824)
Phyllobius montanus	Miller, 1862)
Phyllobius oblongus	(Linnaeus, 1758)
Phyllobius pallidus	(Fabricius, 1792)
Phyllobius pilicornis	Desbrochers, 1873
Phyllobius pomaceus	Gyllenhal, 1834
Phyllobius pyri	(Linnaeus, 1758)
Phyllobius seladonius	Brullé, 1832
Phyllobius subdentatus	Boheman, 1843
Phyllobius thalassinus	Gyllenhal, 1834
Phyllobius vespertinus	(Fabricius, 1792)
Phyllobius virideaeris	(Laicharting, 1781)
Phyllocnistis labyrinthella	(Bjerkander, 1790)
Phyllocnistis saligna	(Zeller, 1839)
Phyllocnistis unipunctella	(Stephens, 1834)
Phyllocnistis xenia	M. Hering, 1936
Phyllodesma ilicifolium	(Linnaeus, 1758)
Phyllodesma tremulifolium	(Hübner, 1810)
Phyllodrepa floralis	(Paykull, 1789)
Phyllodrepa melanocephala	(Fabricius, 1787)
Phyllodrepa nigra	(Gravenhorst, 1806)
Phyllodrepa puberula	(Bernhauer, 1903)
Phyllodrepa salicis	(Gyllenhal, 1810)
Phyllodrepoidea crenata	Ganglbauer, 1895
Phyllognathopus viguieri	(Maupas, 1892)
Phyllometra culminaria	(Eversmann, 1843)
Phyllophya laciniata	(Villers, 1789)
Phyllonorycter abrasella	(Duponchel, 1843)
Phyllonorycter acaciella	(Duponchel, 1843)
Phyllonorycter acerifoliella	(Zeller, 1839)
Phyllonorycter agilella	(Zeller, 1846)
Phyllonorycter apparella	(Herrich-Schäffer, 1855)
Phyllonorycter blancardella	(Fabricius, 1781)
Phyllonorycter cavella	(Zeller, 1846)
Phyllonorycter cerasinella	(Reutti, 1852)
Phyllonorycter comparella	(Duponchel, 1843)
Phyllonorycter connexella	(Zeller, 1846)
Phyllonorycter coryli	(Nicelli, 1851)
Phyllonorycter corylifoliella	(Hübner, 1796)
Phyllonorycter cydoniella	(Denis & Schiffermüller, 1775)
Phyllonorycter delitella	(Duponchel, 1844)
Phyllonorycter distentella	(Zeller, 1846)
Phyllonorycter dubitella	(Herrich-Schäffer, 1855)
Phyllonorycter emberizaepenella	(Bouché, 1834)
Phyllonorycter esperella	(Goeze, 1783)
Phyllonorycter fraxinella	(Zeller, 1846)
Phyllonorycter froelichiella	(Zeller, 1839)
Phyllonorycter pyrifoliella	(Gerasimov, 1933)
Phyllonorycter harrisella	(Linnaeus, 1761)
Phyllonorycter heegeriella	(Zeller, 1846)
Phyllonorycter helianthemella	(Herrich-Schäffer, 1861)
Phyllonorycter hilarella	(Zetterstedt, 1839)
Phyllonorycter ilicifoliella	(Duponchel, 1843)
Phyllonorycter insignitella	(Zeller, 1846)
Phyllonorycter kleemannella	(Fabricius, 1781)
Phyllonorycter lantanella	(Schrank, 1802)
Phyllonorycter lautella	(Zeller, 1846)
Phyllonorycter leucographella	(Zeller, 1850)
Phyllonorycter maestingella	(Müller, 1764)
Phyllonorycter medicaginella	(Gerasimov, 1930)
Phyllonorycter mespilella	(Hübner, 1805)
Phyllonorycter messaniella	(Zeller, 1846)
Phyllonorycter muelleriella	(Zeller, 1839)
Phyllonorycter nicellii	(Stainton, 1851)
Phyllonorycter nigrescentella	(Logan, 1851)
Phyllonorycter oxyacanthae	(Frey, 1856)
Phyllonorycter parisiella	(Wocke, 1848)
Phyllonorycter pastorella	(Zeller, 1846)
Phyllonorycter platani	(Staudinger, 1870)
Phyllonorycter populifoliella	(Treitschke, 1833)
Phyllonorycter quercifoliella	(Zeller, 1839)
Phyllonorycter quinqueguttella	(Stainton, 1851)
Phyllonorycter rajella	(Linnaeus, 1758)
Phyllonorycter robiniella	(Clemens,1859)
Phyllonorycter roboris	(Zeller, 1839)
Phyllonorycter sagitella	(Bjerkander, 1790)
Phyllonorycter salicicolella	(Sircom, 1848)
Phyllonorycter salictella	(Zeller, 1846)
Phyllonorycter kuhlweiniella	(Zeller, 1839)
Phyllonorycter schreberella	(Fabricius, 1781)
Phyllonorycter scitulella	(Duponchel, 1843)
Phyllonorycter sorbi	(Frey, 1855)
Phyllonorycter spinicolella	(Zeller, 1846)
Phyllonorycter staintoniella	(Nicelli, 1853)
Phyllonorycter stettinensis	(Nicelli, 1852)
Phyllonorycter tenerella	(Joannis, 1915)
Phyllonorycter tristrigella	(Haworth, 1828)
Phyllonorycter ulmifoliella	(Hübner, 1817)
Phyllopertha horticola	(Linnaeus, 1758)
Phyllophila obliterata	(Rambur, 1833)
Phylloporia bistrigella	(Haworth, 1828)
Phylloscopus bonelli	(Vieillot, 1819)
Phylloscopus collybita	(Vieillot, 1817)
Phylloscopus collybita abietinus	(Vieillot, 1817)
Phylloscopus collybita tristis	(Vieillot, 1817)
Phylloscopus fuscatus	(Blyth, 1842)
Phylloscopus inornatus	(Blyth, 1842)
Phylloscopus proregulus	(Pallas, 1811)
Phylloscopus schwarzi	(Radde, 1863)
Phylloscopus sibilatrix	(Bechstein, 1793)
Phylloscopus trochilus	(Linnaeus, 1758)
Phylloscopus trochilus acredula	(Linnaeus, 1758)
Phyllostroma myrtilli	(Kaltenbach,1874)
Phyloctetes bidentulus	(Lepeletier, 1806)
Phyloctetes horvathi	(Mocsáry, 1889)
Phyloctetes truncatus	(Dahlbom, 1831)
Phylus coryli	(Linnaeus, 1758)
Phylus melanocephalus	(Linnaeus, 1767)
Phylus palliceps	Fieber, 1861
Phymata crassipes	(Fabricius, 1775)
Phymatodes testaceus	(Linnaeus, 1758)
Phymatopus hecta	(Linnaeus, 1758)
Phymatura brevicollis	(Kraatz, 1856)
Physa fontinalis	(Linnaeus, 1758)
Physatocheila confinis	Horváth, 1905
Physatocheila costata	(Fabricius, 1794)
Physatocheila dumetorum	(Herrich-Schäffer, 1838)
Physatocheila smreczynskii	China, 1952
Physella acuta	(Draparnaud, 1805)
Physocypria kraepelini	Müller, 1903
Physokermes hemicryphus	(Dalman,1826)
Physokermes inopinatus	Danzig et Kozár,1973
Physokermes piceae	(Schrank,1801)
Phytobaenus amabilis	R.F.Sahlberg, 1834
Phytobius leucogaster	(Marsham, 1802)
Phytocoris austriacus	E. Wagner, 1954
Phytocoris dimidiatus	Kirschbaum, 1856
Phytocoris incanus	Fieber, 1864
Phytocoris insignis	Reuter, 1876
Phytocoris longipennis	Flor, 1860
Phytocoris meridionalis	Herrich-Schäffer, 1835
Phytocoris nowickyi	Fieber, 1870
Phytocoris parvulus	Reuter, 1880
Phytocoris pini	Kirschbaum, 1856
Phytocoris populi	(Linnaeus, 1758)
Phytocoris reuteri	Saunders, 1875
Phytocoris singeri	Wagner, 1954
Phytocoris thrax	Josifov, 1967
Phytocoris tiliae	(Fabricius, 1776)
Phytocoris ulmi	(Linnaeus, 1758)
Phytocoris ustulatus	Herrich-Schäffer, 1835
Phytocoris varipes	Boheman, 1852
Phytoecia caerulea	(Scopoli, 1772)
Phytoecia cylindrica	(Linnaeus, 1758)
Phytoecia icterica	(Schaller, 1783)
Phytoecia nigricornis	(Fabricius, 1781)
Phytoecia pustulata	(Schrank, 1776)
Phytoecia virgula	(Charpentier, 1825)
Phytometra viridaria	(Clerck, 1759)
Pica pica	(Linnaeus, 1758)
Picromerus bidens	(Linnaeus, 1758)
Picromerus conformis	(Herrich-Schäffer, 1841)
Picus canus	Gmelin, 1788
Picus viridis	Linnaeus, 1758
Pidonia lurida	(Fabricius, 1792)
Pieris brassicae	(Linnaeus, 1758)
Pieris bryoniae	(Hübner, 1806)
Pieris bryoniae marani	(Hübner, 1806)
Pieris ergane	(Geyer, 1828)
Pieris mannii	(Mayer, 1851)
Pieris napi	(Linnaeus, 1758)
Pieris rapae	(Linnaeus, 1758)
Piesma capitatum	(Wolff, 1804)
Parapiesma kochiae	(Becker, 1867)
Piesma maculatum	(Laporte, 1832)
Parapiesma quadratum	(Fieber, 1844)
Parapiesma salsolae	(Becker, 1867)
Parapiesma silenes	(Horváth, 1888)
Piesma variabile	(Fieber, 1864)
Piezocranum simulans	Horváth, 1877
Piezodorus lituratus	(Fabricius, 1794)
Pilemia hirsutula	(Frölich, 1793)
Pilemia tigrina	(Mulsant, 1851)
Pilophorus cinnamopterus	(Kirschbaum, 1856)
Pilophorus clavatus	(Linnaeus, 1767)
Pilophorus confusus	(Kirschbaum, 1856)
Pilophorus perplexus	(Douglas & Scott, 1875)
Pilophorus simulans	Josifov, 1989
Pima boisduvaliella	(Guenée, 1845)
Pinalitus coccineus	(Horváth, 1889)
Pinalitus rubricatus	Fallén, 1807
Pinalitus viscicola	(Puton, 1888)
Pinicola enucleator	(Linnaeus, 1758)
Piniphila bifasciana	(Haworth, 1811)
Pinthaeus sanguinipes	(Fabricius, 1781)
Pinumius areatus	(Stal, 1858)
Pionosomus opacellus	Horváth, 1895
Pipistrellus kuhlii	(Kuhl, 1819)
Pipistrellus nathusii	(Keyserling et Blasius, 1839)
Pipistrellus pipistrellus	(Schreber, 1774)
Pipistrellus pygmaeus	(Leach, 1825)
Pipiza austriaca	Meigen, 1822
Pipiza bimaculata	Meigen, 1822
Pipiza fasciata	Meigen, 1822
Pipiza festiva	Meigen, 1822
Pipiza lugubris	(Fabricius, 1775)
Pipiza luteitarsis	Zetterstedt, 1843
Pipiza noctiluca	(Linnaeus, 1758)
Pipiza quadrimaculata	(Panzer, 1804)
Pipizella annulata	(Macquart, 1829)
Pipizella divicoi	(Goldlin de Tiefenau, 1974)
Pipizella maculipennis	(Meigen, 1822)
Pipizella viduata	(Linnaeus, 1758)
Pipizella virens	(Fabricius, 1805)
Pipizella zeneggenensis	(Goldlin de Tiefenau, 1974)
Pirapion immune	(Kirby, 1808)
Pirapion redemptum	(Schatzmayr, 1920)
Pirata hygrophilus	Thorell, 1872
Pirata insularis	Emerton, 1885
Pirata knorri	(Scopoli, 1763)
Pirata latitans	(Blackwall, 1841)
Pirata piraticus	(Clerck, 1757)
Pirata piscatorius	(Clerck, 1757)
Pirata tenuitarsis	Simon, 1876
Pirata uliginosus	(Thorell, 1856)
Peirates hybridus	(Scopoli, 1763)
Pisaura mirabilis	(Clerck, 1757)
Piscicola geometra	(Linnaeus, 1758)
Piscicola haranti	Jarry,1960
Pisidium amnicum	(O.F. Müller, 1774)
Pisidium casertanum	(Poli, 1791)
Pisidium henslowanum	(Sheppard, 1823)
Pisidium milium	Held, 1836
Pisidium moitessierianum	Paladilhe, 1866
Pisidium nitidum	Jenyns, 1832
Pisidium obtusale	(Lamarck, 1818)
Pisidium personatum	Malm, 1855
Pisidium pseudosphaerium	J. Favre, 1927
Pisidium pulchellum	Jenyns, 1832
Pisidium subtruncatum	Malm, 1855
Pisidium supinum	A. Schmidt, 1851
Pisidium tenuilineatum	Stelfox, 1918
Pison atrum	(Spinola, 1808)
Pissodes castaneus	(DeGeer, 1775)
Pissodes harcyniae	(Herbst, 1795)
Pissodes piceae	(Illiger, 1807)
Pissodes pini	(Linnaeus, 1758)
Pissodes piniphilus	(Herbst, 1797)
Pissodes scabricollis	Miller, 1859
Pissodes validirostris	(C. R. Sahlberg & J. Sahlberg, 1834)
Pistius truncatus	(Pallas, 1772)
Pithanus maerkelii	(Herrich-Schäffer, 1838)
Pithyotettix abietinus	(Fallén, 1806)
Pityogenes bidentatus	(Herbst, 1783)
Pityogenes bistridentatus	(Eichhoff, 1878)
Pityogenes chalcographus	(Linnaeus, 1761)
Pityogenes conjunctus	(Reitter, 1887)
Pityogenes quadridens	(Hartig, 1834)
Pityogenes trepanatus	(Nördlinger, 1848
Pityohyphantes phrygianus	(C.L. Koch, 1836)
Pityokteines curvidens	(Germar, 1824)
Pityokteines vorontzowi	(Jacobson, 1895)
Pityophagus ferrugineus	(Linnaeus, 1761)
Pityophagus laevior	Abeille de Perrin, 1872
Pityophagus quercus	Reitter, 1887
Pityophthorus carniolicus	Wichmann, 1910
Pityophthorus exsculptus	(Ratzeburg, 1837)
Pityophthorus henscheli	Seitner, 1887
Pityophthorus lichtensteinii	(Ratzeburg, 1837)
Pityophthorus micrographus	(Linnaeus, 1758)
Pityophthorus pityographus	(Ratzeburg, 1837)
Pityophthorus pubescens	(Marsham, 1802)
Placobdella costata	(Fr. Müller, 1844)
Placonotus testaceus	(Fabricius, 1787)
Placusa adscita	(Erichson, 1839)
Placusa atrata	(Mannerheim, 1830)
Placusa complanata	Erichson, 1839
Placusa depressa	Mäklin, 1845
Placusa incompleta	Sjöberg, 1934
Placusa pumilio	(Gravenhorst, 1802)
Placusa tachyporoides	(Waltl, 1838)
Plagiognathus arbustorum	(Fabricius, 1794)
Plagiognathus bipunctatus	Reuter, 1883
Plagiognathus chrysanthemi	(Wolff, 1804)
Plagiognathus fulvipennis	(Kirschbaum, 1856)
Plagiogonus putridus	(Geoffroy, 1785)
Plagiolepis pygmaea	(Latreille, 1798)
Plagiolepis vindobonensis	Lomnicki, 1925
Plagiolepis xene	Staerke, 1936
Plagionotus arcuatus	(Linnaeus, 1758)
Plagionotus detritus	(Linnaeus, 1758)
Plagionotus floralis	(Pallas, 1773)
Plagiorrhamma suturalis	(Herrich-Schäffer, 1839)
Plagodis dolabraria	(Linnaeus, 1767)
Plagodis pulveraria	(Linnaeus, 1758)
Planaphrodes bifasciatus	(Linnaeus, 1758)
Planaphrodes elongatus	(Lethierry, 1876)
Planaphrodes trifasciatus	(Fourcroy, 1785)
Planchonia arabidis	Signoret,1877
Planeustomus heydeni	(Eppelsheim, 1884)
Planeustomus kahrii	(Kraatz, 1858)
Planeustomus palpalis	(Erichson, 1839)
Planolinus uliginosus	Hardy, 1847
Planorbarius corneus	(Linnaeus, 1758)
Planorbella duryi	(Wetherby, 1879)
Planorbella nigricans	(Spix, 1827)
Planorbis carinatus	O.F. Müller, 1774
Planorbis planorbis	(Linnaeus, 1758)
Platalea leucorodia	(Linnaeus, 1758)
Platambus maculatus	(Linnaeus, 1758)
Plataraea dubiosa	(G.Benick, 1935)
Plataraea nigriceps	(Marsham, 1802)
Plataraea nigrifrons	(Erichson, 1839)
Plataraea sordida	(Kraatz, 1856)
Plataraea spaethi	(Bernhauer, 1898)
Platycerus caprea	(De Geer, 1774)
Platycerus caraboides	(Linnaeus, 1758)
Platycheirus albimanus	(Fabricius, 1781)
Platycheirus ambiguus	(Fallén, 1817)
Platycheirus angustatus	(Zetterstedt, 1843)
Platycheirus clypeatus	(Meigen, 1822)
Platycheirus discimanus	Loew, 1871
Platycheirus europaeus	Goeldlin, Maibach & Speight, 1990
Platycheirus fulviventris	(Macquart, 1829)
Platycheirus immarginatus	(Zetterstedt, 1849)
Platycheirus manicatus	(Meigen, 1822)
Platycheirus parmatus	Rondani, 1857
Platycheirus peltatus	(Meigen, 1822)
Platycheirus perpallidus	(Verrall, 1901)
Platycheirus podagratus	(Zetterstedt, 1838)
Platycheirus scambus	(Staeger, 1843)
Platycheirus scutatus	(Meigen, 1822)
Platycheirus sticticus	(Meigen, 1822)
Platycheirus tarsalis	(Schummel, 1836)
Platycis cosnardi	(Chevrolat, 1838)
Platycis minutus	(Fabricius, 1787)
Platycleis affinis	Fieber, 1853
Platycleis albopunctata	(Goeze, 1778)
Platycleis montana	Kollar, 1833
Platycleis veyseli	Koçak, 1984
Platycnemis pennipes	(Pallas, 1776)
Platydema dejeani	Laporte et Brullé, 1831
Platydema violaceum	(Fabricius, 1790)
Platyderus rufus transalpinus	Duftschmid, 1812
Platydomene bicolor	(Erichson, 1840)
Platydracus chalcocephalus	(Gmelin, 1790)
Platydracus flavopunctatus	(Latreille, 1804)
Platydracus fulvipes	(Scopoli, 1763)
Platydracus latebricola	(Gravenhorst, 1806)
Platydracus stercorarius	(Olivier, 1795)
Platyedra subcinerea	(Haworth, 1828)
Platyla banatica	(Rossmässler, 1842)
Platyla perpusilla	(Reinhardt, 1880)
Platyla polita	(W. Hartmann, 1840)
Platylomalus complanatus	(Panzer, 1797)
Platymetopius curvatus	Dlabola, 1974
Platymetopius filigranus	(Scott, 1876)
Platymetopius guttatus	Fieber, 1869
Platymetopius major	(Kirschbaum, 1868)
Platymetopius rostratus	(Herrich-Schäffer, 1834)
Platymetopius undatus	(De Geer, 1773)
Platymyrmilla quinquefasciata	(Olivier, 1811)
Platynaspis luteorubra	(Goeze, 1777)
Limodromus assimilis	(Paykull, 1790)
Limodromus krynickii	(Sperk, 1835)
Platynus livens	(Gyllenhal, 1810)
Limodromus longiventris	(Mannerheim, 1825)
Platynus scrobiculatus	(Fabricius, 1801)
Platyola austriaca	Scheerpeltz, 1959
Platyperigea aspersa	(Rambur, 1834)
Platyperigea kadenii	(Freyer, 1836)
Platyperigea terrea	(Freyer, 1840)
Platyphylax frauenfeldi	(Brauer, 1857)
Platyplax inermis	(Rambur, 1839)
Platyplax salviae	(Schilling, 1829)
Platyptilia calodactyla	(Denis & Schiffermüller, 1775)
Buzkoiana capnodactylus	(Zeller, 1841)
Platyptilia farfarellus	Zeller, 1867
Platyptilia gonodactyla	(Denis & Schiffermüller, 1775)
Gillmeria miantodactylus	(Zeller, 1841)
Platyptilia nemoralis	Zeller, 1841
Gillmeria pallidactyla	(Haworth, 1811)
Platyptilia tesseradactyla	(Linnaeus, 1761)
Gillmeria ochrodactyla	(Denis & Schiffermüller, 1775)
Platypus cylindrus	(Fabricius, 1792)
Platypus oxyurus	Dufour, 1843
Platyrhinus resinosus	(Scopoli, 1763)
Platyscelis hungarica	E. Frivaldszky, 1836
Platysoma compressum	(Herbst, 1783)
Platystethus alutaceus	Thomson, 1861
Platystethus arenarius	(Geoffroy, 1785)
Platystethus capito	Heer, 1839
Platystethus cornutus	(Gravenhorst, 1802)
Platystethus degener	Mulsant et Rey, 1878
Platystethus nitens	(C.R.Sahlberg, 1832)
Platystethus rufospinus	Hochhuth, 1849
Platystethus spinosus	Erichson, 1840
Platystomos albinus	(Linnaeus, 1758)
Platytes alpinella	(Hübner, 1813)
Platytes cerussella	(Denis & Schiffermüller, 1775)
Plea minutissima	Leach, 1818
Pleargus pygmaeus	(Horváth, 1897)
Plebeius argus	(Linnaeus, 1758)
Plebeius argyrognomon	(Bergsträsser, 1779)
Plebeius idas	(Linnaeus, 1761)
Plebeius sephirus	(Frivaldszky, 1835)
Plecotus auritus	(Linnaeus, 1758)
Plecotus austriacus	(Fischer, 1829)
Plectophloeus erichsoni	(Aubé, 1844)
Plectophloeus fischeri	(Aubé, 1833)
Plectophloeus nitidus	(Fairmaire, 1857)
Plectophloeus zoufali	(Reitter, 1876)
Plectrocnemia brevis	McLachlan, 1871
Plectrocnemia conspersa	(Curtis, 1834)
Plectrocnemia geniculata	McLachlan, 1871
Plectrocnemia minima	Klapálek, 1899
Plectrophenax nivalis	(Linnaeus, 1758)
Plectrophenax nivalis vlasowae	(Linnaeus, 1758)
Plegaderus caesus	(Herbst, 1792)
Plegaderus discisus	Erichson, 1839
Plegaderus dissectus	Erichson, 1839
Plegaderus saucius	Erichson, 1834
Plegaderus vulneratus	(Panzer, 1797)
Plegadis falcinellus	(Linnaeus, 1766)
Pleganophorus bispinosus	Hampe, 1855
Plemyria rubiginata	(Denis & Schiffermüller, 1775)
Plesiocypridopsis newtoni	(Brady & Robertson, 1870)
Plesiodema pinetella	(Zetterstedt, 1828)
Pleurophorus caesus	(Creutzer, 1796)
Pleurophorus pannonicus	Petrovitz, 1961
Pleuroptya balteata	(Fabricius, 1798)
Pleuroptya ruralis	(Scopoli, 1763)
Pleurota aristella	(Linnaeus, 1767)
Pleurota bicostella	(Clerck, 1759)
Pleurota brevispinella	(Zeller, 1847)
Pleurota marginella	(Denis & Schiffermüller, 1775)
Pleurota pungitiella	Herrich-Schäffer, 1852
Pleurota pyropella	(Denis & Schiffermüller, 1775)
Pleurotobia magnifica	Roubal, 1932
Pleuroxus aduncus	(Jurine, 1820)
Pleuroxus denticulatus	Birge, 1879
Pleuroxus laevis	Sars, 1862
Pleuroxus striatus	Schoedler, 1863
Pleuroxus trigonellus	(O.F. Müller, 1785)
Pleuroxus truncatus	(O.F. Müller, 1785)
Pleuroxus uncinatus	Baird, 1850
Plinthisus brevipennis	(Latreille, 1807)
Plinthisus longicollis	Fieber, 1861
Plinthisus pusillus	(Scholtz, 1846)
Plinthus findeli	Boheman, 1842
Plinthus megerlei	(Panzer, 1802)
Plinthus sturmii	(Germar, 1818)
Plinthus tischeri	Germar, 1824
Plodia interpunctella	(Hübner, 1813)
Plusia festucae	(Linnaeus, 1758)
Plutella porrectella	(Linnaeus, 1758)
Plutella xylostella	(Linnaeus, 1758)
Pluvialis apricaria	(Linnaeus, 1758)
Pluvialis fulva	(Gmelin, 1789)
Pluvialis squatarola	(Linnaeus, 1758)
Luzulaspis jahandiezi	Balachowsky, 1932
Pocadicnemis juncea	Locket et Millidge, 1953
Pocadicnemis pumila	(Blackwall, 1841
Pocadius adustus	Reitter, 1888
Pocadius ferrugineus	(Fabricius, 1775)
Pocota personata	(Harris, 1780)
Podabrus alpinus	(Paykull, 1798)
Podalonia affinis	(Kirby, 1798)
Podalonia hirsuta	(Scopoli, 1763)
Podalonia luffi	(Saunders, 1903)
Podalonia tydei	Le Guillou, 1841
Podarcis muralis	(Laurenti, 1768)
Podarcis taurica	(Pallas, 1841)
Podeonius acuticornis	(Germar, 1824)
Podiceps auritus	(Linnaeus, 1758)
Podiceps cristatus	(Linnaeus, 1758)
Podiceps grisegena	(Boddaert, 1783)
Podiceps nigricollis	Ch. L. Brehm, 1831
Podisma pedestris	(Linne, 1758)
Podonta nigrita	(Fabricius, 1794)
Podops curvidens	Costa, 1847
Podops inuncta	(Fabricius, 1775)
Podops rectidens	Horváth, 1883
Poecilagenia rubricans	(Lepeletier, 1845)
Poecilagenia sculpturata	(Kohl, 1898)
Poecilia reticulata	Peters, 1859
Poecilia sphenops	(Valenciennes, 1846)
Poecilimon affinis	(Frivaldszky, 1876)
Poecilimon fussii	Brunner von Wattenwyl, 1878
Poecilimon intermedius	(Fieber, 1853)
Poecilimon schmidtii	(Fieber, 1853)
Poecilium alni	(Linnaeus, 1767)
Poecilium fasciatum	(Villers, 1789)
Poecilium glabratum	(Charpentier, 1825)
Poecilium puncticolle	(Mulsant, 1862)
Poecilium pusillum	(Fabricius, 1787)
Poecilium rufipes	(Fabricius, 1776)
Poecilocampa populi	(Linnaeus, 1758)
Poecilochroa variana	(C.L. Koch, 1839)
Poeciloneta variegata	(Blackwall, 1841)
Poecilonota variolosa	(Paykull, 1799)
Poecilus cupreus	(Linnaeus, 1758)
Poecilus kekesiensis	Nyilas, 1993
Poecilus lepidus	(Leske, 1787)
Poecilus puncticollis	(Dejean, 1828)
Poecilus punctulatus	(Schaller, 1783)
Poecilus sericeus	Fischer, 1823
Poecilus striatopunctatus	(Duftschmid, 1812)
Poecilus versicolor	(Sturm, 1824)
Pogonocherus decoratus	Fairmaire, 1855
Pogonocherus fasciculatus	(De Geer, 1775)
Pogonocherus hispidulus	(Piller et Mitterpacher, 1783)
Pogonocherus hispidus	(Linnaeus, 1758)
Pogonocherus ovatus	(Göze, 1777)
Pogonus luridipennis	(Germar, 1822)
Pogonus peisonis	Ganglbauer, 1891
Polemistus abnormis	(Kohl, 1888)
Polia bombycina	(Hufnagel, 1766)
Polia hepatica	(Clerck, 1759)
Polia nebulosa	(Hufnagel, 1766)
Polistes associus	Kohl, 1898
Polistes biglumis	(Linnaeus, 1758)
Polistes biglumis bimaculatus	(Linnaeus, 1758)
Polistes bischoffi	(Weyrauch, 1937)
Polistes dominulus	(Christ, 1791)
Polistes gallicus	(Linnaeus, 1767)
Polistes nimpha	(Christ, 1791)
Polistes omissus	(Weyrauch, 1939)
Polistichus connexus	(Fourcroy, 1785)
Polochrum repandum	Spinola, 1806
Polycentropus flavomaculatus	(Pictet, 1834)
Polycentropus irroratus	Curtis, 1834
Polycentropus schmidi	Novák et Botosaneanu, 1965
Polychrysia moneta	(Fabricius, 1787)
Polydesmus collaris	C.L.Koch, 1847
Polydesmus complanatus	(Linnaeus, 1761)
Polydesmus denticulatus	C.L.Koch, 1847
Polydesmus edentulus	C.L.Koch, 1847
Polydesmus edentulus bidentatus	C.L.Koch, 1847
Polydesmus germanicus	Verhoeff, 1896
Polydesmus monticolus	(Latzel, 1884)
Polydesmus monticolus koszegensis	(Latzel, 1884)
Polydesmus polonicus	Latzel, 1884
Polydesmus schassburgensis	Verhoeff, 1898
Polydrusus amoenus	(Germar, 1824)
Polydrusus cervinus	(Linnaeus, 1758)
Polydrusus confluens	Stephens, 1831
Polydrusus corruscus	Germar, 1824
Polydrusus flavipes	(DeGeer, 1775)
Polydrusus formosus	(Mayer, 1779)
Polydrusus fulvicornis	(Fabricius, 1792)
Polydrusus impar	Des Gozis, 1882
Polydrusus impressifrons	Gyllenhal, 1834
Polydrusus marginatus	Stephens, 1831
Polydrusus mollis	(Ström, 1768)
Polydrusus pallidus	Gyllenhal, 1834
Polydrusus picus	(Fabricius, 1792)
Polydrusus pilosus	Gredler, 1866
Polydrusus pterygomalis	Boheman, 1840
Polydrusus sparsus	Gyllenhal, 1834
Polydrusus tereticollis	(DeGeer, 1775)
Polydrusus thalassinus	Gyllenhal, 1834
Polydrusus tibialis	Gyllenhal, 1834
Polydrusus viridicinctus	Gyllenhal, 1834
Polyergus rufescens	Latreille, 1798
Polygonia c-album	(Linnaeus, 1758)
Polygraphus grandiclava	Thomson, 1886
Polygraphus poligraphus	(Linnaeus, 1758)
Polygraphus subopacus	Thomson, 1871
Polymerus asperulae	(Fieber, 1861)
Polymerus brevicornis	(Reuter, 1878)
Polymerus carpathicus	Horváth, 1882
Polymerus cognatus	(Fieber, 1858)
Polymerus holosericeus	(Hahn, 1831)
Polymerus microphthalmus	E. Wagner, 1951
Polymerus nigrita	(Fallén, 1807)
Polymerus palustris	Reuter, 1905
Polymerus unifasciatus	(Fabricius, 1794)
Polymerus vulneratus	(Panzer, 1806)
Polymixis polymita	(Linnaeus, 1761)
Polymixis rufocincta	(Geyer, 1828)
Polymixis rufocincta isolata	(Geyer, 1828)
Polymixis xanthomista	(Hübner, 1819)
Polyommatus admetus	(Esper, 1783)
Polyommatus amandus	(Schneider, 1792)
Polyommatus bellargus	(Rottemburg, 1775)
Polyommatus coridon	(Poda, 1761)
Polyommatus damon	([Denis & Schiffermüller], 1775)
Polyommatus daphnis	([Denis & Schiffermüller], 1775)
Polyommatus dorylas	([Denis & Schiffermüller], 1775)
Polyommatus escheri	(Hübner, 1823)
Polyommatus icarus	(Rottemburg, 1775)
Polyommatus semiargus	(Rottemburg, 1775)
Polyommatus thersites	(Cantener, 1835)
Polyphaenis sericata	(Esper, 1787)
Polyphemus pediculus	(Linnaeus, 1761)
Polyphylla fullo	(Linnaeus, 1758)
Polyploca ridens	(Fabricius, 1787)
Polypogon gryphalis	(Herrich-Schäffer, 1851)
Polypogon tentacularia	(Linnaeus, 1758)
Polysarcus denticauda	(Charpentier, 1825)
Polysticta stelleri	(Pallas, 1769)
Polystomophora ostiaplurima	(Kiritchenko,1940)
Polyxenus lagurus	(Linnaeus, 1758)
Polyzonium germanicum	Brandt, 1837
Pomatias elegans	(O.F. Müller, 1774)
Pomatias rivularis	(Eichwald, 1829)
Pomatinus substriatus	(P.W.J. Müller, 1806)
Pompilus cinereus	(Fabricius, 1798)
Ponera coarctata	(Latreille, 1802)
Ponera testacea	Emery, 1895
Pontia edusa	(Fabricius, 1777)
Poophagus hopffgarteni	(Tournier, 1874)
Poophagus robustus	Faust, 1882
Poophagus sisymbrii	(Fabricius, 1776)
Porcinolus murinus	(Fabricius, 1794)
Porhydrus lineatus	(Fabricius, 1775)
Porhydrus obliquesignatus	(Bielz, 1852)
Poromniusa crassa	(Eppelsheim, 1883)
Poromniusa procidua	(Erichson, 1837)
Porotachys bisulcatus	(Nicolai, 1822)
Porphyrio porphyrio	(Linnaeus, 1758)
Porphyrio porphyrio seistanicus	(Linnaeus, 1758)
Porphyrophora polonica	(Linnaeus,1758)
Porrhomma convexum	(Westring, 1851)
Porrhomma errans	(Blackwall, 1841)
Porrhomma microphthalmum	(O.P.-Cambridge, 1871)
Porrhomma montanum	Jackson, 1913
Porrhomma profundum	(Dahl, 1939)
Porrhomma pygmaeum	(Blackwall, 1834)
Porrhomma rosenhaueri	(L. Koch, 1872)
Porrittia galactodactyla	(Denis & Schiffermüller, 1775)
Porthmidius austriacus	(Schrank, 1781)
Porzana parva	(Scopoli, 1769)
Porzana porzana	(Linnaeus, 1766)
Porzana pusilla	(Pallas, 1776)
Porzana pusilla intermedia	(Pallas, 1776)
Postsolenobia banatica	(M. Hering, 1922)
Postsolenobia thomanni	(Rebel, 1936)
Potamanthus luteus	(Linné, 1767)
Potamocypris arcuata	(Sars, 1903)
Potamocypris fallax	Fox, 1967
Potamocypris fulva	(Brady, 1868)
Potamocypris pallida	Alm, 1914
Potamocypris unicaudata	Schaefer, 1943
Potamocypris variegata	(Brady & Norman, 1889)
Potamocypris villosa	(Jurine, 1820)
Potamocypris zschokkei	Kaufmann, 1900
Potamophilus acuminatus	(Fabricius, 1792)
Potamophylax cingulatus	(Stephens, 1837)
Potamophylax luctuosus	(Piller et Mitterpacher, 1783)
Potamophylax nigricornis	(Pictet, 1834)
Potamophylax rotundipennis	(Brauer, 1857)
Potamopyrgus antipodarum	(J.E. Gray, 1843)
Povolnya leucapennella	(Stephens, 1835)
Praesolenobia clathrella	(Fischer v. Röslerstamm, 1837)
Praestochrysis megerlei	(Dahlbom, 1854)
Praganus hofferi	(Dlabola, 1947)
Prays fraxinella	(Bjerkander, 1784)
Prays ruficeps	(Heinemann ,1854)
Prenolepis imparis	(Say)
Prenolepis nitens	(Mayr, 1852)
Pria dulcamarae	(Scopoli, 1763)
Prinerigone vagans	(Audouin, 1826)
Priobium carpini	(Herbst, 1793)
Priocnemis agilis	(Shuckard, 1837)
Priocnemis cordivalvata	Haupt, 1927
Priocnemis coriacea	Dahlbom, 1843
Priocnemis enslini	Haupt, 1927
Priocnemis exaltata	(Fabricius, 1776)
Priocnemis fastigiata	Haupt, 1934
Priocnemis fennica	Haupt, 1927
Priocnemis gracilis	Haupt, 1927
Priocnemis hankoi	Móczár, 1944
Priocnemis hauptia	Móczár, 1955
Priocnemis hyalinata	(Fabricius, 1793)
Priocnemis melanosoma	Kohl, 1880
Priocnemis mesobrometi	Wolf, 1961
Priocnemis mimula	Wesmael, 1851
Priocnemis minuta	(Vander Linden, 1827)
Priocnemis minutalis	Wahis, 1979
Priocnemis occipitalis	Gussakovskij, 1930
Priocnemis parvula	Dahlbom, 1845
Priocnemis perturbator	(Harris, 1776)
Priocnemis pillichi	Priesner, 1960
Priocnemis propinqua	Lepeletier, 1847
Priocnemis pusilla	Schiödte, 1837
Priocnemis rugosa	Sustera, 1938
Priocnemis schioedtei	Haupt, 1927
Priocnemis sulci	Balthasar, 1943
Priocnemis susterai	Haupt, 1927
Prionocyphon serricornis	(P.W.J. Müller, 1821)
Prionocypris zenkeri	(Chyzer & Toth, 1858)
Prionus coriarius	(Linnaeus, 1758)
Prionychus ater	(Fabriciu, 1775)
Prionychus melanarius	(Germar, 1813)
Prionyx kirbyi	(Vander Linden, 1828)
Prionyx subfuscatus	(Dahlbom, 1845)
Prisistus obsoletus	(Germar, 1824)
Prisistus suturalba	(Schultze, 1903)
Pristerognatha fuligana	(Denis & Schiffermüller, 1775)
Pristerognatha penthinana	(Guenée, 1845)
Pristicephalus shadini	Smirnov, 1928
Probaticus subrugosus	(Duftschmid, 1812)
Proceratium melinum	(Roger 1860)
Prochlidonia amiantana	(Hübner, 1799)
Prochoreutis myllerana	(Fabricius, 1794)
Prochoreutis sehestediana	(Fabricius, 1776)
Prochoreutis stellaris	(Zeller, 1847)
Procloeon bifidum	(Bengtsson, 1912)
Procloeon macronyx	Kluge et Novikova, 1992
Procraerus tibialis	(Lacordaire, 1835)
Procyon lotor	(Linnaeus, 1758)
Grammodes stolida	(Fabricius, 1775)
Prolita solutella	(Zeller, 1839)
Pronocera angusta	(Kriechbaumer, 1844)
Pronomaea korgei	Lohse, 1974
Propylea quatuordecimpunctata	(Linnaeus, 1758)
Proserpinus proserpina	(Pallas, 1772)
Prosopistoma pennigerum	(Müller, 1785)
Prostemma aeneicolle	Stein, 1857
Prostemma guttula	(Fabricius, 1787)
Prostemma sanguineum	(Rossi, 1790)
Prosternon chrysocomum	(Germar, 1843)
Prosternon tessellatum	(Linnaeus, 1758)
Prostomis mandibularis	(Fabricius, 1801)
Protapion apricans	(Herbst, 1797)
Protapion assimile	(Kirby, 1808)
Protapion difforme	(Germar, 1818)
Protapion dissimile	(Germar, 1817)
Protapion filirostre	(Kirby, 1808)
Protapion fulvipes	(Fourcroy, 1785)
Protapion gracilipes	(Dietrich, 1857)
Protapion interjectum	(Desbrochers, 1895)
Protapion nigritarse	(Kirby, 1808)
Protapion ononidis	(Gyllenhal, 1827)
Protapion ruficrus	(Germar, 1817)
Protapion schoenherri	(Boheman, 1839)
Protapion trifolii	(Linnaeus, 1768)
Protapion varipes	(Germar, 1817)
Proteinus atomarius	Erichson, 1840
Proteinus brachypterus	(Fabricius, 1792)
Proteinus crenulatus	Pandellé, 1867
Proteinus laevigatus	Hochhuth, 1871
Proteinus ovalis	Stephens, 1834
Proteroiulus fuscus	(Am Stein, 1857)
Proterorhinus marmoratus	(Pallas, 1814)
Protodeltote pygarga	(Hufnagel, 1766)
Protolampra sobrina	(Duponchel, 1843)
Protonemura aestiva	Kis, 1965
Protonemura auberti	Illies, 1954
Protonemura intricata	(Ris, 1902)
Protonemura nitida	(Pictet, 1835)
Protonemura praecox	(Morton, 1894)
Protopirapion atratulum	(Germar, 1817)
Protopirapion kraatzii	(Wencker, 1859)
Proutia betulina	(Zeller, 1839)
Proxenus lepigone	(Möschler, 1860)
Prunella collaris	(Scopoli, 1769)
Prunella modularis	(Linnaeus, 1758)
Psacasta exanthematica	(Scopoli, 1763)
Psacasta neglecta	(Herrich-Schäffer, 1837)
Psacasta pallida	Reuter, 1902
Psallidium maxillosum	(Fabricius, 1792)
Psallus albicinctus	(Kirschbaum, 1856)
Psallus ambiguus	(Fallén, 1807)
Psallus anaemicus	Seidenstücker, 1966
Psallus betuleti	(Fallén, 1829)
Psallus confusus	Rieger, 1981
Psallus falleni	Reuter, 1883
Psallus flavellus	Stichel, 1933
Psallus lepidus	Fieber, 1858
Psallus mollis	(Mulsant, 1852)
Psallus nigripilis	(Reuter, 1888)
Psallus ocularis	(Mulsant & Rey, 1852)
Psallus pardalis	Seidenstücker, 1966
Psallus perrisi	(Mulsant, 1852)
Psallus quercus	(Kirschbaum, 1856)
Psallus salicis	(Kirschbaum, 1856)
Psallus variabilis	(Fallén, 1829)
Psallus varians	(Herrich-Schäffer, 1842)
Psallus vittatus	Fieber, 1861
Psamathocrita dalmatinella	Huemer et Tokar, 2000
Psammaecius punctulatus	(Vander Linden, 1829)
Psammodius asper	(Fabricius, 1775)
Psammodius canaliculatus	Mulsant, 1842
Psammodius danubialis	Ádám, 1989
Psammodius laevipennis	A. Costa, 1844
Psammoecus bipuncatatus	(Fabricius, 1792)
Psammotettix alienus	(Dahlbom, 1850)
Psammotettix asper	(Ribaut, 1925)
Psammotettix cephalotes	(Herrich-Schäffer, 1834)
Psammotettix comitans	Emeljanov, 1964
Psammotettix confinis	(Dahlbom, 1850)
Psammotettix excisus	(Matsumura, 1908)
Psammotettix helvolus	(Kirschbaum, 1868)
Psammotettix koeleriae	Zachvatkin, 1948
Psammotettix kolosvarensis	(Matsumura, 1908)
Psammotettix nodosus	(Ribaut, 1925)
Psammotettix notatus	(Melichar, 1896)
Psammotettix ornaticeps	(Horváth, 1897)
Psammotettix pallidinervis	(Dahlbom, 1850)
Psammotettix pictipennis	(Kirschbaum, 1868)
Psammotettix poecilus	Flor, 1861
Psammotettix provincialis	(Ribaut, 1925)
Psammotettix remanei	Orosz, 1999
Psammotettix slovacus	Dlabola, 1948
Psammotis pulveralis	(Hübner, 1796)
Psarus abdominalis	(Fabricius, 1794)
Psectra diptera	(Burmeister, 1839)
Pselactus spadix	(Herbst, 1795)
Pselaphaulax dresdensis	(Herbst, 1792)
Pselaphus heisei	Herbst, 1792
Pselnophorus heterodactyla	(Müller, 1764)
Psen ater	(Olivier, 1792)
Psen exaratus	(Eversmann, 1849)
Psenulus concolor	(Dahlbom, 1843)
Psenulus fuscipennis	(Dahlbom, 1843)
Psenulus laevigatus	(Schenck, 1857)
Psenulus meridionalis	Beaumont, 1937
Psenulus pallipes	(Panzer, 1798)
Psenulus schencki	(Tournier, 1889)
Pseudanodonta complanata	(Rossmässler, 1835)
Pseudanostirus globicollis	(Germar, 1843)
Pseudapion fulvirostre	(Gyllenhal, 1833)
Pseudapion rufirostre	(Fabricius, 1833)
Pseudapis diversipes	(Latreille, 1806)
Pseudapis femoralis	(Pallas, 1773)
Pseudapis unidentata	(Olivier, 1811)
Pseudargyrotoza conwagana	(Fabricius, 1775)
Pseudatemelia flavifrontella	(Denis & Schiffermüller, 1775)
Pseudatemelia josephinae	(Toll, 1956)
Pseudatemelia subochreella	(Doubleday, 1859)
Pseudaulacaspis pentagona	(Targioni-Tozzetti,1886)
Pseudepierus italicus	(Paykull, 1811)
Pseudepipona angusta	(Morawitz, 1867)
Pseudepipona herrichii	(Saussure, 1856)
Pseudeulia asinana	(Hübner, 1799)
Pseudeuophrys erratica	(Walckenaer, 1826)
Pseudeuophrys lanigera	(Simon, 1871)
Pseudeuophrys obsoleta	(Simon, 1868)
Pseudeuophrys vafra	(Blackwall, 1867)
Pseudeustrotia candidula	([Denis & Schiffermüller], 1775)
Pseudicius encarpatus	(Walckenaer, 1802)
Pseudocandona albicans	(Brady, 1864)
Pseudocandona compressa	(Koch, 1838)
Pseudocandona eremita	(Vejdovsky, 1882)
Pseudocandona insculpta	G.W. Müller, 1900
Pseudocandona marchica	(Hartwig, 1899)
Pseudocandona pratensis	(Hartwig, 1901)
Pseudocandona rostrata	(Brady & Norman, 1889)
Pseudocandona sarsi	(Hartwig, 1899)
Pseudocandona sucki	(Hartwig, 1901)
Pseudocandona szoecsi	(Farkas, 1958)
Pseudochermes fraxini	(Kaltenbach,1860)
Pseudochoragus piceus	(Schaum, 1845)
Pseudochydorus globosus	(Baird, 1843)
Pseudocistela ceramboides	(Linnaeus, 1758)
Pseudocleonus cinereus	(Schrank, 1781)
Pseudocleonus grammicus	(Panzer, 1789)
Pterostichus gracilis	(Dejean, 1828)
Pseudocoeliodes rubricus	(Gyllenhal, 1837)
Pseudofusulus varians	(C. Pfeiffer, 1828)
Pseudohermenias abietana	(Fabricius, 1787)
Pseudoips prasinana	(Linnaeus, 1758)
Pseudoloxops coccineus	(Meyer-Dür, 1843)
Pseudomalus auratus	(Linnaeus, 1758)
Pseudomalus bogdanovi	(Radoszkowski, 1877)
Pseudomalus pusillus	(Fabricius, 1804)
Pseudomalus violaceus	(Scopoli, 1763)
Pseudomedon huetheri	Hubenthal, 1927
Pseudomedon obscurellus	(Erichson, 1840)
Pseudomedon obsoletus	(Nordmann, 1837)
Pseudomicrodota jelineki	(Krása, 1914)
Pseudomicrodynerus parvulus	(Herrich-Schaeffer, 1838)
Pseudomyllocerus bisignatus	Germar, 1824
Pseudomyllocerus magnanoi	Alonso-Zarazaga & Lyal, 1999
Pseudomyllocerus periteloides	(Fuss, 1861)
Pseudomyllocerus sinuatus	(Fabricius, 1801)
Pseudomyllocerus subsignatus	(Boheman, 1834)
Pseudomyllocerus viridilimbatus	(Apfelbeck, 1898)
Pseudoophonus calceatus	(Duftschmid, 1812)
Pseudoophonus griseus	(Panzer, 1797)
Pseudoophonus rufipes	(De Geer, 1774)
Pseudopanthera macularia	(Linnaeus, 1758)
Pseudoperapion brevirostre	(Herbst, 1797)
Pseudopodisma fieberi	(Scudd, 1898)
Pseudopodisma nagyi	(Galvagni et Fontana 1996)
Pseudopostega auritella	(Hübner, 1813)
Pseudopostega crepusculella	(Zeller, 1839)
Pseudoprotapion astragali	(Paykull, 1800)
Pseudoprotapion elegantulum	(Germar, 1818)
Pseudoprotapion ergenense	(Becker, 1864)
Pseudopsis sulcata	Newman, 1834
Pseudoptilinus fissicollis	(Reitter, 1877)
Pseudorasbora parva	(Temminck et Schlegel, 1842)
Pseudorchestes cinereus	(Fahraeus, 1843)
Pseudorchestes ermischi	(Dieckmann, 1958)
Pseudorchestes horioni	(Dieckmann, 1958)
Pseudorchestes kostali	(Dieckmann, 1984)
Pseudorchestes persimilis	(Reitter, 1911)
Pseudorchestes pratensis	(Germar, 1821)
Pseudorchestes smreczynskii	(Dieckmann, 1958)
Pseudosciaphila branderiana	(Linnaeus, 1758)
Pseudosemiris kaufmanni	(Eppelsheim, 1887)
Pseudospinolia incrassata	(Spinola, 1838)
Pseudospinolia neglecta	(Shuckard, 1837)
Pseudospinolia uniformis	(Dahlbom, 1854)
Pseudostenapion simum	(Germar, 1817)
Pseudostyphlus pillumus	(Gyllenhal, 1836)
Pseudosuccinea columella	(Say, 1817)
Pseudoswammerdamia combinella	(Hübner, 1786)
Pseudotelphusa paripunctella	(Thunberg, 1794)
Pseudotelphusa scalella	(Scopoli, 1763)
Pseudotelphusa tessella	(Linnaeus, 1758)
Pseudoterpna pruinata	(Hufnagel, 1767)
Pseudotrichia rubiginosa	(Rossmässler, 1838)
Pseudotriphyllus suturalis	(Fabricius, 1801)
Pseudovadonia livida	(Fabricius, 1776)
Pseudovadonia livida pecta	(Fabricius, 1776)
Psilochorus simoni	(Berland, 1911)
Psilococcus ruber	Borchsenius,1952
Psilota anthracina	Meigen, 1822
Psilota innupta	Rondani, 1857
Psithyrus barbutellus	(Kirby, 1802)
Psithyrus barbutellus maxillosus	(Kirby, 1802)
Psithyrus bohemicus	(Seidl, 1837)
Psithyrus campestris	(Panzer, 1801)
Psithyrus quadricolor	(Lepeletier, 1832)
Psithyrus rupestris	(Fabricius, 1793)
Psithyrus sylvestris	(Lepleteier, 1832)
Psithyrus vestalis	(Geoffroy, 1785)
Psoa viennensis	Herbst, 1797
Psophus stridulus	(Linne, 1758)
Psoricoptera gibbosella	(Zeller, 1839)
Psorosa dahliella	(Treitschke, 1832)
Psyche casta	(Pallas, 1767)
Psyche crassiorella	(Bruand, 1851)
Psychidea nudella	(Ochsenheimer, 1810)
Acentra vestalis	(Staudinger, 1871)
Psychoides verhuella	Bruand, 1853
Psychomyia pusilla	(Fabricius, 1781)
Psychrodromus olivaceus	(Brady & Norman, 1889)
Psyllobora vigintiduopunctata	(Linnaeus, 1758)
Pteleobius kraatzii	(Eichhoff, 1864)
Pteleobius vittatus	(Fabricius, 1787)
Ptenidium formicetorum	Kraatz, 1851
Ptenidium fuscicorne	Erichson, 1845
Ptenidium gressneri	Erichson, 1845
Ptenidium intermedium	Wankowicz, 1869
Ptenidium laevigatum	Erichson, 1845
Ptenidium longicorne	Fuss, 1868
Ptenidium nitidum	(Heer, 1841)
Ptenidium pusillum	(Gyllenhal, 1808)
Ptenidium turgidum	C.G. Thomson, 1855
Pterapherapteryx sexalata	(Retzius, 1783)
Pterocheilus phaleratus	(Panzer, 1797)
Pterocheilus phaleratus chevrieranus	(Panzer, 1797)
Pterocles exustus	Temminck, 1825
Pterolonche albescens	Zeller, 1847
Pterolonche inspersa	Staudinger, 1859
Pteronemobius heydenii	(Fischer, 1853)
Pterophorus ischnodactyla	(Treitschke, 1835)
Pterophorus pentadactyla	(Linnaeus, 1758)
Pterostichus aethiops	(Panzer, 1797)
Pterostichus anthracinus biimpressus	Illiger, 1798
Pterostichus aterrimus	(Herbst, 1784)
Pterostichus burmeisteri	Herr, 1841
Pterostichus calvitarsis	Breit, 1912
Pterostichus chameleon	(Motschulsky, 1865)
Pterostichus cursor	(Dejean, 1828)
Pterostichus cylindricus	(Herbst, 1784)
Pterostichus diligens	(Sturm, 1824)
Pterostichus elongatus	(Duftschmid, 1812)
Pterostichus fasciatopunctatus	(Creutzer, 1799)
Pterostichus hungaricus	(Dejean, 1828)
Pterostichus incommodus	Schaum, 1858
Pedius inquinatus	(Sturm, 1824)
Pterostichus leonisi	Apfelbeck, 1904
Pedius longicollis	(Duftschmid, 1812)
Pterostichus macer	(Marsham, 1802)
Pterostichus melanarius	(Illiger, 1798)
Pterostichus melas	(Creutzer, 1799)
Pterostichus minor	(Gyllenhal, 1827)
Pterostichus niger	(Schaller, 1783)
Pterostichus nigrita	(Paykull, 1790)
Pterostichus oblongopunctatus	(Fabricius, 1787)
Pterostichus ovoideus	(Sturm, 1824)
Pterostichus piceolus	(Chaudoir, 1850)
Pterostichus piceolus latoricaensis	(Chaudoir, 1850)
Pterostichus rhaeticus	Herr, 1837
Pterostichus strenuus	(Panzer, 1797)
Pterostichus subsinuatus	(Dejean, 1828)
Pterostichus taksonyis	Csiki, 1930
Pterostichus transversalis	(Duftschmid, 1812)
Pterostichus unctulatus	(Duftschmid, 1812)
Pterostichus vernalis	(Panzer, 1796)
Pterostoma palpina	(Clerck, 1759)
Pterothrixidia impurella	(Duponchel, 1836)
Pterothrixidia rufella	(Duponchel, 1836)
Pterotmetus staphyliniformis	(Schilling, 1829)
Pterotopteryx dodecadactyla	Hübner, 1813
Pteryx suturalis	(Heer, 1841)
Ptilinus fuscus	Geoffroy, 1785
Ptilinus pectinicornis	(Linnaeus, 1758)
Ptiliola brevicollis	(Matthews, 1860)
Ptiliola kunzei	(Heer, 1841)
Ptiliolum caledonicum	(Sharp, 1871)
Ptiliolum fuscum	(Erichson, 1845)
Ptiliolum marginatum	(Aubé, 1850)
Ptiliolum schwarzi	(Flach, 1887)
Ptiliolum spencei	(Allibert, 1844)
Ptilium affine	Erichson, 1845
Ptilium caesum	Erichson, 1845
Ptilium exaratum	(Allibert, 1844)
Ptilium minutissimum	(Ljungh, 1804)
Ptilium modestum	Wankowicz, 1869
Ptilium myrmecophilum	(Allibert, 1844)
Ptilocephala muscella	(Denis & Schiffermüller, 1775)
Ptilocephala plumifera	(Ochsenheimer, 1810)
Ptilocolepus granulatus	(Pictet, 1834)
Ptilodon capucina	(Linnaeus, 1758)
Ptilodon cucullina	([Denis & Schiffermüller], 1775)
Ptilophora plumigera	([Denis & Schiffermüller], 1775)
Ptilophorus dufouri	(Latreille, 1817)
Ptinella aptera	(Guérin-Ménéville, 1839)
Ptinella britannica	Matthews, 1858
Ptinella limbata	(Heer, 1841)
Ptinella tenella	(Erichson, 1845)
Ptinomorphus imperialis	(Linnaeus, 1767)
Ptinomorphus regalis	(Duftschmid, 1825)
Ptinus bicinctus	Sturm, 1837
Ptinus calcaratus	Kiesenwetter, 1877
Ptinus capellae	Reitter, 1879
Ptinus clavipes	Panzer, 1792
Ptinus coarcticollis	Sturm, 1837
Ptinus dubius	Sturm, 1795
Ptinus fur	(Linnaeus, 1758)
Ptinus latro	Fabricius, 1775
Ptinus pilosus	P.W.J. Müller, 1821
Ptinus podolicus	Khnzorian et Karapetyan, 1991
Ptinus rufipes	(Olivier, 1790)
Ptinus schlerethi	Reitter, 1884
Ptinus sexpunctatus	Panzer, 1795
Ptinus subpilosus	Sturm, 1837
Ptinus tectus	Boieldieu, 1856
Ptinus variegatus	Rossi, 1794
Ptinus villiger	Reitter, 1884
Ptocheuusa abnormella	(Herrich-Schäffer, 1854)
Ptocheuusa inopella	(Zeller, 1839)
Ptocheuusa paupella	(Zeller, 1847)
Ptomaphagus sericatus	(Chaudoir, 1845)
Ptomaphagus subvillosus	(Goeze, 1777)
Ptomaphagus validus	(Kraatz, 1852)
Ptomaphagus varicornis	(Rosenhauer, 1847)
Ptosima undecimmaculata	(Herbst, 1784)
Ptycholoma lecheana	(Linnaeus, 1758)
Ptycholomoides aeriferana	(Herrich-Schäffer, 1851)
Pulvinaria betulae	(Linnaeus,1758)
Pulvinaria populi	Signoret,1873
Pulvinaria ribesiae	Signoret,1873
Pulvinaria vitis	(Linnaeus,1758)
Puncha ratzeburgi	(Brauer, 1876)
Punctum pygmaeum	(Draparnaud, 1801)
Pungeleria capreolaria	(Denis & Schiffermüller, 1775)
Pupilla muscorum	(Linnaeus, 1758)
Pupilla sterrii	(Voith, 1840)
Pupilla triplicata	(S. Studer, 1820)
Purpuricenus budensis	(Götz, 1783)
Purpuricenus globulicollis	Mulsant, 1839
Purpuricenus kaehleri	(Linnaeus, 1758)
Puto antennatus	Signoret,1875
Puto janetscheki	Balachowsky,1953
Puto pilosellae	(Sulc,1898)
Puto superbus	(Leonardi,1907)
Pycnomerus terebrans	(Olivier, 1750)
Pycnota paradoxa	(Mulsant et Rey, 1861)
Pygolampis bidentata	(Goeze, 1778)
Pygolampis laticaput	Benedek, 1968
Matilella fusca	(Haworth, 1811)
Pyncostola bohemiella	(Nickerl, 1864)
Pyralis farinalis	(Linnaeus, 1758)
Pyralis perversalis	(Herrich-Schäffer, 1849)
Pyralis regalis	(Denis & Schiffermüller, 1775)
Pyramidula pusilla	(Vallot, 1801)
Pyrausta aurata	(Scopoli, 1763)
Pyrausta castalis	Treitschke, 1829
Pyrausta cingulata	(Linnaeus, 1758)
Pyrausta coracinalis	Leraut, 1982
Pyrausta despicata	(Scopoli, 1763)
Pyrausta falcatalis	Guenée, 1854
Pyrausta ledereri	(Staudinger, 1870)
Pyrausta nigrata	(Scopoli, 1763)
Pyrausta obfuscata	(Scopoli, 1763)
Pyrausta ostrinalis	(Hübner, 1796)
Pyrausta porphyralis	(Denis & Schiffermüller, 1775)
Pyrausta purpuralis	(Linnaeus, 1758)
Pyrausta rectefascialis	Toll, 1936
Pyrausta sanguinalis	(Linnaeus, 1767)
Pyrausta virginalis	Duponchel, 1832
Pyrgus alveus	(Hübner, 1803)
Pyrgus armoricanus	(Oberthür, 1910)
Pyrgus carthami	(Hübner, 1813)
Pyrgus malvae	(Linnaeus, 1758)
Pyrgus serratulae	(Rambur, 1839)
Pyrochroa coccinea	(Linnaeus, 1761)
Pyrochroa serraticornis	(Scopoli, 1763)
Pyroderces argyrogrammos	(Zeller, 1847)
Pyroderces klimeschi	Rebel, 1938
Pyrois cinnamomea	(Goeze, 1781)
Pyronia tithonus	(Linnaeus, 1767)
Pyrophaena granditarsa	(Forster, 1771)
Pyrophaena rosarum	(Fabricius, 1787)
Pyropterus nigroruber	(De Geer, 1774)
Pyrrhia purpurina	(Esper, 1804)
Pyrrhia umbra	(Hufnagel, 1766)
Pyrrhidium sanguineum	(Linnaeus, 1758)
Pyrrhocorax graculus	(Linnaeus, 1766)
Pyrrhocorax pyrrhocorax	(Linnaeus, 1758)
Pyrrhocorax pyrrhocorax erythrorhamphus	(Linnaeus, 1758)
Pyrrhocoris apterus	(Linnaeus, 1758)
Pyrrhocoris marginatus	(Kolenati, 1845)
Pyrrhosoma nymphula	(Sulzer, 1776)
Pyrrhosoma nymphula interposita	(Sulzer, 1776)
Pyrrhula pyrrhula	(Linnaeus, 1758)
Pytho depressus	(Linnaeus, 1767)
Diaspidiotus gigas	(Thiem & Gerneck, 1934)
Quadraspidiotus labiatarum	(Marchal,1909)
Diaspidiotus lenticularis	(Lindinger, 1912)
Diaspidiotus marani	(Zahradník, 1952)
Diaspidiotus ostreaeformis	(Curtis, 1843)
Diaspidiotus perniciosus	(Comstock, 1881)
Diaspidiotus pyri	(Lichtenstein, 1881)
Diaspidiotus sulci	(Balachowsky, 1950)
Diaspidiotus zonatus	(Fauenfeld, 1868)
Kervillea conspurcata	(Spinola, 1839)
Quasimus minutissimus	(Germar, 1817)
Quedius balticus	Korge, 1960
Quedius boops	(Gravenhorst, 1802)
Quedius brevicornis	Thomson, 1860
Quedius brevis	(Erichson, 1840)
Quedius cincticollis	(Kraatz, 1847)
Quedius cinctus	(Paykull, 1790)
Quedius cruentus	(Olivier, 1795)
Quedius curtipennis	Bernhauer, 1908
Quedius dubius	(Heer, 1839)
Quedius dubius fimbriatus	(Heer, 1839)
Quedius fulgidus	(Fabricius, 1793)
Quedius fuliginosus	(Gravenhorst, 1802)
Quedius fumatus	Stephens, 1833
Quedius lateralis	(Gravenhorst, 1802)
Quedius levicollis	(Brullé, 1832)
Quedius limbatus	(Heer, 1839)
Quedius longicornis	(Kraatz, 1857)
Quedius lucidulus	Erichson, 1839
Quedius maurorufus	(Gravenhorst, 1806)
Quedius maurus	(C.R.Sahlberg, 1830)
Quedius meridiocarpathicus	Smetana, 1958
Quedius mesomelinus	(Marsham, 1802)
Quedius microps	(Gravenhorst, 1847)
Quedius molochinus	(Gravenhorst, 1806)
Quedius nemoralis	(Baudi, 1848)
Quedius nigriceps	(Kraatz, 1857)
Quedius nitipennis	Stephens, 1833
Quedius ochripennis	(Menetries, 1832)
Quedius ochropterus	(Erichson, 1840)
Quedius paradisianus	(Heer, 1839)
Quedius picipes	(Mannerheim, 1830)
Quedius plagiatus	Mannerheim, 1843
Quedius puncticollis	Thomson, 1867
Quedius riparius	Kellner, 1843
Quedius scintillans	(Gravenhorst, 1806)
Quedius scitus	(Gravenhorst, 1806)
Quedius semiaeneus	Stephens, 1833
Quedius semiobscurus	(Marsham, 1802)
Quedius skoraszewskyi	(Korge, 1961)
Quedius suturalis	(Kiesenwetter, 1845)
Quedius truncicola	(Fairmaire et Laboulbene, 1856)
Quedius umbrinus	(Erichson, 1839)
Quedius vexans	(Eppelsheim, 1881)
Quedius xanthopus	(Erichson, 1839)
Rabigus pullus	(Nordmann, 1837)
Rabigus tenuis	(Fabricius, 1792)
Rabdocerus foveolatus	(Ljungh, 1823)
Rabdocerus gabrieli	(Gerhardt, 1901)
Radix ampla	(W. Hartmann, 1821)
Radix auricularia	(Linnaeus, 1758)
Radix balthica	(Linnaeus, 1758)
Radix labiata	(Rossmässler, 1835)
Raglius alboacuminatus	(Goeze, 1778)
Raglius confusus	(Reuter, 1886)
Rhyparochromus vulgaris	(Schilling, 1829)
Rallus aquaticus	Linnaeus, 1758
Rana arvalis	Nilsson, 1842
Rana arvalis wolterstorffi	Nilsson, 1842
Rana dalmatina	Bonaparte, 1840
Rana kl. esculenta	Linnaeus, 1758
Rana lessonae	Camerano, 1882
Rana ridibunda	Pallas, 1771
Rana temporaria	Linnaeus, 1758
Ranatra linearis	(Linnaeus, 1758)
Ranunculiphilus faeculentus	(Gyllenhal, 1837)
Ranunculiphilus italicus	(Ch. Brisout, 1869)
Raphidia ophiopsis	Linnaeus, 1758
Raphidia ophiopsis mediterranea	Linnaeus, 1758
Raphidia ulrikae	H. Aspöck, 1964
Rattus norvegicus	(Berkenhout, 1769)
Rattus rattus	(Linnaeus, 1758)
Rebelia herrichiella	Strand, 1912
Rebelia sapho	(Millire, 1864)
Rebelia surientella	(Bruand, 1858)
Recilia coronifera	(Marshall, 1866)
Recilia schmidtgeni	(Wagner, 1939)
Recurvaria leucatella	(Clerck, 1759)
Recurvaria nanella	(Denis & Schiffermüller, 1775)
Recurvirostra avosetta	Linnaeus, 1758
Reduvius personatus	(Linnaeus, 1758)
Reesa vespulae	(Milliron, 1939)
Regulus ignicapillus	(Temminck, 1820)
Regulus regulus	(Linnaeus, 1758)
Reisserita relicinella	Zeller, 1839
Reitterelater bouyoni	(Chassain, 1992)
Reitterelater dubius	Platia et Cate, 1990
Remiz pendulinus	(Linnaeus, 1758)
Reptalus apiculatus	(Fieber, 1876)
Reptalus melanochaetus	(Fieber, 1876)
Reptalus panzeri	(Löw, 1883)
Reptalus quinquecostatus	(Dufour, 1833)
Evergestis alborivulalis	(Eversmann, 1844)
Retinia resinella	(Linnaeus, 1758)
Reuteria marqueti	Puton, 1875
Rhabdiopteryx acuminata	Klapálek, 1905
Rhabdiopteryx hamulata	Klapálek, 1902
Rhabdomiris striatellus	(Fabricius, 1794)
Rhabdorrhynchus varius	(Herbst, 1795)
Pterolepis germanica	(Herrich-Schäffer, 1840)
Rhacognathus punctatus	(Linnaeus, 1758)
Dirrhagofarsus attenuatus	(Mäklin, 1845)
Rhacopus sahlbergi	(Mannerheim, 1823)
Rhadicoleptus alpestris	(Kolenati, 1848)
Rhadicoleptus alpestris sylvanocarpathicus	(Kolenati, 1848)
Rhagades pruni	(Denis & Schiffermüller, 1775)
Rhagium bifasciatum	Fabricius, 1775
Rhagium inquisitor	(Linnaeus, 1758)
Rhagium mordax	(De Geer, 1775)
Rhagium sycophanta	(Schrank, 1781)
Rhagocneme subsinuata	(Erichson, 1839)
Rhagonycha atra	(Linnaeus, 1767)
Rhagonycha elongata	(Fallén, 1807)
Rhagonycha femoralis	(Brullé, 1832)
Rhagonycha fulva	(Scopoli, 1763)
Rhagonycha gallica	Pic, 1923
Rhagonycha lignosa	(O.F. Müller, 1764)
Rhagonycha limbata	(C.G. Thomson, 1864)
Rhagonycha lutea	(O.F. Müller, 1764)
Rhagonycha nigriceps	Waltl, 1838
Rhagonycha nigripes	Redtenbacher, 1842
Rhagonycha rorida	Kiesenwetter, 1867
Rhagonycha testacea	(Linnaeus, 1758)
Rhagonycha translucida	(Krynicky, 1832)
Rhamnusium bicolor	(Schrank, 1781)
Rhamphus oxyacanthae	(Marsham, 1802)
Rhamphus pulicarius	(Herbst, 1795)
Rhamphus subaeneus	Illiger, 1807
Rhantus bistriatus	(Bergsträsser, 1778)
Rhantus consputus	(Sturm, 1834)
Rhantus frontalis	(Marsham, 1802)
Rhantus grapii	(Gyllenhal, 1808)
Rhantus latitans	Sharp, 1882
Rhantus suturalis	(Macleay, 1825)
Rhantus suturellus	(Harris, 1828)
Rhaphigaster nebulosa	(Poda, 1761)
Rhaphitropis marchica	(Herbst, 1797)
Hydria cervinalis	(Scopoli, 1763)
Rheumaptera hastata	(Linnaeus, 1758)
Hydria undulata	(Linnaeus, 1758)
Rhigognostis hufnagelii	(Zeller, 1839)
Rhigognostis incarnatella	(Steudel, 1873)
Rhigognostis kovacsi	(Gozmány, 1952)
Rhigognostis senilella	(Zetterstedt, 1839)
Rhingia campestris	Meigen, 1822
Rhingia rostrata	(Linnaeus, 1758)
Rhinocyllus conicus	(Frölich, 1792)
Rhinolophus euryale	Blasius, 1853
Rhinolophus ferrumequinum	(Schreber, 1774)
Rhinolophus hipposideros	(Bechstein, 1800)
Rhinomias austriacus	Reitter, 1894
Rhinomias forticornis	(Boheman, 1843)
Rhinomias maxillosus	Petri, 1904
Rhinomias viertli	(J. Weise, 1886)
Rhinoncus albicinctus	Gyllenhal, 1837
Rhinoncus bosnicus	Schultze, 1900
Rhinoncus bruchoides	(Herbst, 1784)
Rhinoncus castor	(Fabricius, 1792)
Rhinoncus inconspectus	(Herbst, 1795)
Rhinoncus pericarpius	(Linnaeus, 1758)
Rhinoncus perpendicularis	(Reich, 1797)
Rhinusa antirrhini	(Paykull, 1800)
Rhinusa asellus	(Gravenhorst, 1807)
Rhinusa bipustulata	(Rossi, 1792)
Rhinusa collinum	(Gyllenhal, 1813)
Rhinusa herbarum	H. Brisout, 1862
Rhinusa hispidum	Brullé, 1832
Rhinusa linariae	(Panzer, 1792)
Rhinusa littoreum	H. Brisout, 1862
Rhinusa melas	Boheman, 1838
Aulacobaris neta	(Germar, 1821)
Rhinusa smreczynskii	Fremuth, 1972
Rhinusa tetra	(Fabricius, 1792)
Rhinusa thapsicola	(Germar, 1821)
Rhithrogena beskidensis	Alba-Tercedor et Sowa, 1987
Rhithrogena germanica	Eaton, 1885
Rhithrogena iridina	(Kolenati, 1859)
Rhithrogena picteti	Sowa, 1971
Rhithrogena puytoraci	Sowa et Degrange, 1987
Rhithrogena semicolorata	(Curtis, 1834)
Rhizaspidiotus canariensis	(Lindinger,1912)
Rhizedra lutosa	(Hübner, 1803)
Acanthococcus agropyri	(Borchsenius, 1949)
Rhizococcus cingulatus	(Kiritchenko,1940)
Acanthococcus cynodontis	(Kiritshenko, 1940)
Rhizococcus herbaceus	Danzig,1962
Acanthococcus insignis	(Newstead, 1891)
Rhizococcus palustris	Dziedzicka et Koteja,1971
Acanthococcus pseudinsignis	(Green, 1921)
Rhizoecus albidus	Goux,1936
Rhizoecus caesii	Schmutterer,1956
Rhizoecus franconiae	Schmutterer,1956
Rhizoecus halophilus	(Hardy,1868)
Rhizoecus poltavae	Laing,1929
Rhizophagus bipustulatus	(Fabricius, 1792)
Rhizophagus brancsiki	Reitter, 1905
Rhizophagus cribratus	Gyllenhal, 1827
Rhizophagus depressus	(Fabricius, 1792)
Rhizophagus dispar	(Paykull, 1800)
Rhizophagus ferrugineus	(Paykull, 1800)
Rhizophagus nitidulus	(Fabricius, 1798)
Rhizophagus parallelocollis	Gyllenhal, 1827
Rhizophagus parvulus	(Paykull, 1800)
Rhizophagus perforatus	Erichson, 1845
Rhizophagus picipes	(Olivier, 1790)
Rhizopulvinaria artemisiae	(Signoret,1873)
Rhizopulvinaria spinifera	Borchsenius,1952
Holochelus aestivus	(Olivier, 1789)
Rhoananus hypochlorus	(Fieber, 1896)
Rhodania occulta	Schmutterer,1952
Rhodania porifera	Goux,1935
Rhodeus amarus	(Bloch, 1782)
Rhodococcus perornatus	(Cockerell et Parrott,1899)
Rhodococcus spiraeae	(Borchsenius, 1949)
Rhodometra sacraria	(Linnaeus, 1767)
Rhodostrophia vibicaria	(Clerck, 1759)
Rhopalapion longirostre	(Olivier, 1807)
Rhopalocerina clavigera	(W.Scriba, 1859)
Rhopalocerus rondanii	Villa, 1833
Rhopalodontus baudueri	Abeille de Perrin, 1874
Rhopalodontus perforatus	(Panzer, 1809)
Rhopalopyx brachyanus	Orosz, 1999
Rhopalopyx preyssleri	(Herrich-Schäffer, 1838)
Rhopalopyx vitripennis	(Flor, 1861)
Rhopalotella validiuscula	(Kraatz, 1856)
Rhopalum austriacum	(Kohl, 1899)
Rhopalum beaumonti	Moczár, 1957
Rhopalum clavipes	(Linnaeus, 1758)
Rhopalum coarctatum	(Scopoli, 1763)
Rhopalum gracile	Wesmael, 1852
Rhopalus conspersus	(Fieber, 1837)
Rhopalus distinctus	(Signoret, 1859)
Rhopalus maculatus	(Fieber, 1837)
Rhopalus parumpunctatus	(Schilling, 1817)
Rhopalus subrufus	(Gmelin, 1788)
Rhophitoides canus	(Eversmann, 1852)
Rhopobota myrtillana	(Humphreys & Westwood, 1845)
Rhopobota naevana	(Hübner, 1817)
Rhopobota stagnana	(Denis & Schiffermüller, 1775)
Rhyacia lucipeta	([Denis & Schiffermüller], 1775)
Rhyacia simulans	(Hufnagel, 1766)
Rhyacionia buoliana	(Denis & Schiffermüller, 1775)
Rhyacionia duplana	(Hübner, 1813)
Rhyacionia piniana	(Herrich-Schäffer, 1851)
Rhyacionia pinicolana	(Doubleday, 1849)
Rhyacionia pinivorana	(Lienig & Zeller, 1846)
Rhyacophila dorsalis persimilis	(Curtis, 1834)
Rhyacophila fasciata	Hagen, 1859
Rhyacophila hirticornis	McLachlan, 1879
Rhyacophila laevis	Pictet, 1834
Rhyacophila nubila	(Zetterstedt, 1840)
Rhyacophila obliterata	McLachlan, 1863
Rhyacophila pascoei	McLachlan, 1879
Rhyacophila polonica	McLachlan, 1879
Rhyacophila pubescens	Pictet, 1834
Rhyacophila tristis	Pictet, 1834
Rhynchaenus alni	(Linnaeus, 1758)
Aulacobaris erythropus	(Germar, 1821)
Orchestes hungaricus	(Hajóss, 1938)
Rhynchaenus jota	(Fabricius, 1787)
Rhynchaenus lonicerae	(Herbst, 1795)
Rhynchaenus pilosus	(Fabricius, 1781)
Orchestes quedenfeldtii	Gerhardt, 1865
Orchestes quercus	(Linnaeus, 1758)
Rhynchaenus rufus	(Schrank, 1781)
Rhynchaenus sparsus	Fahraeus, 1843
Rhynchites auratus	(Scopoli, 1763)
Rhynchites bacchus	(Linnaeus, 1758)
Rhynchites giganteus	Krynicki, 1832
Rhynchites lenaeus	(Faust, 1891)
Rhynchotalona falcata	(Sars, 1862)
Rhyncolus ater	(Linnaeus, 1758)
Rhyncolus elongatus	(Gyllenhal, 1827)
Rhyncolus punctatulus	Boheman, 1838
Rhyncolus reflexus	(Boheman, 1838)
Rhyncolus sculpturatus	Waltl, 1839
Rhynocoris annulatus	(Linnaeus, 1758)
Rhynocoris iracundus	(Poda, 1761)
Rhynocoris niger	(Herrich-Schäffer, 1844)
Rhyparia purpurata	(Linnaeus, 1758)
Rhyparioides metelkana	(Lederer, 1861)
Rhyparochromus pini	(Linnaeus, 1758)
Rhyparochromus sanguineus	Douglas & Scott
Rhyssemus germanus	(Linnaeus, 1767)
Rhytidodus decimusquartus	(Schrank, 1776)
Rhytidodus nobilis	Fieber, 1868
Oxynychus chrysomeloides	(Herbst, 1792)
Oxynychus litura	(Fabricius, 1787)
Rhyzopertha dominica	(Fabricius, 1792)
Ribautiana alces	(Ribaut, 1931)
Ribautiana ognevi	(Zachvatkin, 1948)
Ribautiana scalaris	(Ribaut, 1931)
Ribautiana tenerrima	(Herrich-Schäffer, 1834)
Ribautiana ulmi	(Linnaeus, 1758)
Ribautodelphax affinis	Logvinenko, 1970
Ribautodelphax albostriatus	(Fieber, 1866)
Ribautodelphax angulosus	(Ribaut, 1953)
Ribautodelphax collinus	(Boheman, 1847)
Ribautodelphax imitans	(Ribaut, 1953)
Ribautodelphax pungens	(Ribaut, 1953)
Rileyiana fovea	(Treitschke, 1825)
Riolus cupreus	(P.W.J. Müller, 1806)
Riolus subviolaceus	(P.W.J. Müller, 1817)
Riparia riparia	(Linnaeus, 1758)
Rissa tridactyla	(Linnaeus, 1758)
Ritsemia pupifera	Lichtenstein,1879
Rivula sericealis	(Scopoli, 1763)
Robertus arundineti	(O.P.-Cambridge, 1871)
Robertus lividus	(Blackwall, 1836)
Robertus neglectus	(O.P.-Cambridge, 1871)
Roeslerstammia erxlebella	(Fabricius, 1787)
Roeslerstammia pronubella	(Denis & Schiffermüller, 1775)
Ropalopus clavipes	(Fabricius, 1775)
Ropalopus femoratus	(Linnaeus, 1758)
Ropalopus insubricus	(Germar, 1824)
Ropalopus macropus	(Germar, 1824)
Ropalopus ungaricus	(Herbst, 1784)
Ropalopus varini	(Bedel, 1870)
Rophites algirus	Pérez, 1903
Rophites algirus trispinosus	Pérez, 1903
Rophites hartmanni	Friese, 1902
Rophites quinquespinosus	Spinola, 1808
Rosalia alpina	(Linnaeus, 1758)
Rubiconia intermedia	(Wolff, 1811)
Rugathodes bellicosus	(Simon, 1873)
Rugathodes instabilis	O.P.-Cambridge, 1871
Rugilus angustatus	(Geoffroy, 1785)
Rugilus erichsonii	(Fauvel, 1867)
Rugilus mixtus	(Lohse, 1956)
Rugilus orbiculatus	(Paykull, 1789)
Rugilus rufipes	Germar, 1835
Rugilus similis	(Erichson, 1839)
Rugilus subtilis	(Erichson, 1840)
Runcinia grammica	(C.L. Koch, 1837)
Rupicapra rupicapra	Linnaeus, 1758
Rushia parreyssii	(Mulsant, 1856)
Rusina ferruginea	(Esper, 1785)
Ruteria hypocrita	(Boheman, 1837)
Ruthenica filograna	(Rossmässler, 1836)
Rutidosoma fallax	(Otto, 1897)
Rutidosoma globulus	(Herbst, 1795)
Rutilus frisii	(Nordmann, 1840)
Rutilus virgo	(Heckel, 1852)
Rutilus rutilus	(Linnaeus, 1758)
Rutpela maculata	(Poda, 1761)
Rybaxis longicornis	(Leach, 1817)
Rypobius praetermissus	Bowestead, 1999
Sabanejewia aurata	(Filippi, 1863)
Sabanejewia aurata balcanica	(Filippi, 1863)
Sabanejewia aurata bulgarica	(Filippi, 1863)
Sabra harpagula	(Esper, 1786)
Saccharicoccus penium	Williams,1962
Bythinella pannonica	(Frauenfeld, 1865)
Saga pedo	(Pallas, 1771)
Sagatus punctifrons	(Fallén, 1826)
Sahlbergotettix salicicola	(Flor, 1861)
Salamandra salamandra	(Linnaeus, 1758)
Salda muelleri	(Gmelin, 1788)
Saldula arenicola	(Scholtz, 1846)
Saldula c-album	(Fieber, 1859)
Saldula melanoscela	(Fieber, 1859)
Saldula nitidula	(Puton, 1880)
Saldula opacula	(Zetterstedt, 1839)
Saldula orthochila	(Fieber, 1859)
Saldula pallipes	(Fabricius, 1794)
Saldula palustris	(Douglas & Scott, 1874)
Saldula pilosella	(Thomson, 1871)
Saldula saltatoria	(Linnaeus, 1758)
Saldula xanthochila	(Fieber, 1859)
Salebriopsis albicilla	(Herrich-Schäffer, 1849)
Salicarus roseri	(Herrich-Schäffer, 1839)
Salmincola salmonea	(Linnaues, 1758)
Salmo trutta	Linnaeus, 1758
Saloca diceros	(O.P.-Cambridge, 1871)
Saloca kulczynskii	Miller et Kratochvil, 1939
Salpingus aeneus	(Olivier, 1897)
Salpingus planirostris	(Fabricius, 1787)
Salpingus ruficollis	(Linnaeus, 1761)
Salticus cingulatus	(Panzer, 1797)
Salticus quagga	Miller, 1971
Salticus scenicus	(Clerck, 1757)
Salticus zebraneus	(C.L. Koch, 1837)
Salvelinus fontinalis	(Mitchill, 1814)
Sander lucioperca	(Linnaeus, 1758)
Sander volgensis	(Gmelin, 1788)
Saperda octopunctata	(Scopoli, 1772)
Saperda perforata	(Pallas, 1773)
Saperda punctata	(Linnaeus, 1767)
Saperda scalaris	(Linnaeus, 1758)
Saphanus piceus	(Laicharting, 1784)
Saprinus aegialius	Brenske et Reitter, 1884
Saprinus aeneus	(Fabricius, 1775)
Saprinus cribellatus	Marseul, 1855
Saprinus furvus	Erichson, 1834
Saprinus georgicus	Marseul, 1862
Saprinus immundus	(Gyllenhal, 1827)
Saprinus lautus	Erichson, 1839
Saprinus maculatus	(Rossi, 1792)
Saprinus pharao	Marseul, 1855
Saprinus planiusculus	Motschulsky, 1849
Saprinus quadristriatus	(Thunberg, 1794)
Saprinus semipunctatus	(Fabricius, 1792)
Saprinus semistriatus	(Scriba, 1790)
Saprinus subnitescens	Bickhardt, 1909
Saprinus tenuistrius	Marseul, 1855
Saprinus tenuistrius sparsutus	Marseul, 1855
Saprinus vermiculatus	Reichardt, 1923
Saprinus virescens	(Paykull, 1798)
Sapyga clavicornis	(Linnaeus, 1758)
Sapyga quinquepunctata	(Fabricius, 1781)
Sapygina decemguttata	(Jurine, 1807)
Saragossa porosa	(Eversmann, 1854)
Saragossa porosa kenderesiensis	(Eversmann, 1854)
Sarscypridopsis aculeata	(Costa, 1847)
Satrapes sartorii	(Redtenbacher, 1858)
Saturnia pavonia	(Linnaeus, 1758)
Saturnia pyri	([Denis & Schiffermüller], 1775)
Saturnia spini	([Denis & Schiffermüller], 1775)
Satyrium acaciae	(Fabricius, 1787)
Satyrium ilicis	(Esper, 1779)
Satyrium pruni	(Linnaeus, 1758)
Satyrium spini	([Denis & Schiffermüller], 1775)
Satyrium w-album	(Knoch, 1782)
Sauron rayi	(Simon, 1881)
Sauterina hofmanniella	(Schleich, 1867)
Saxicola rubetra	(Linnaeus, 1758)
Saxicola torquata	(Linnaeus, 1766)
Saxicola rubicola	(Linnaeus, 1758)
Scaeva dignota	(Rondani, 1857)
Scaeva pyrastri	(Linnaeus, 1758)
Scaeva selenitica	(Meigen, 1822)
Scaphidema metallicum	(Fabricius, 1792)
Scaphidium quadrimaculatum	Olivier, 1790
Scaphisoma agaricinum	(Linnaeus, 1758)
Scaphisoma assimile	Erichson, 1845
Scaphisoma balcanicum	Tamanini, 1954
Scaphisoma boleti	(Panzer, 1793)
Scaphisoma boreale	Lundblad, 1952
Scaphisoma inopinatum	Löbl, 1967
Scaphisoma obenbergeri	Lőbl, 1963
Scaphisoma subalpinum	Reitter, 1881
Scaphium immaculatum	(Olivier, 1790)
Scapholeberis erinaceus	Daday, 1903
Scapholeberis mucronata	(O.F. Müller, 1785)
Scapholeberis rammneri	Dumont & Pensaert, 1983
Scarabaeus pius	(Illiger, 1803)
Scarabaeus typhon	(Fischer von Waldheim, 1823)
Scardia boletella	(Fabricius, 1794)
Scardinius erythrophthalmus	(Linnaeus, 1758)
Parallelomorphus terricola	(Bonelli, 1813)
Scarodytes halensis	(Fabricius, 1787)
Sceliphron curvatum	(Smith, 1870)
Sceliphron destillatorium	(Illiger, 1807)
Sceliphron spirifex	(Linnaeus, 1758)
Schellencandona insueta	(Klie, 1938)
Schendyla nemorensis	(C.L.Koch, 1837)
Schendyla zonalis	Brölemann & Ribaut, 1911
Schiffermuelleria schaefferella	(Linnaeus, 1758)
Schiffermuelleria grandis	(Desvignes, 1842)
Schinia cardui	(Hübner, 1790)
Schinia cognata	(Freyer, 1833)
Protoschinia scutosa	(Denis & Schiffermüller, 1775)
Schistoglossa gemina	(Erichson, 1837)
Schistoglossa viduata	(Erichson, 1837)
Schistostege decussata	(Denis & Schiffermüller, 1775)
Schizotus pectinicornis	(Linnaeus, 1758)
Schoenobius gigantella	(Denis & Schiffermüller, 1775)
Schrankia costaestrigalis	(Stephens, 1834)
Schrankia taenialis	(Hübner, 1809)
Schreckensteinia festaliella	(Hübner, 1819)
Sciaphilus asperatus	(Bonsdorff, 1785)
Sciaphobus barbatulus	(Germar, 1824)
Sciaphobus caesius	(Hampe, 1870)
Sciaphobus scitulus	(Germar, 1824)
Sciaphobus squalidus	(Gyllenhal, 1834)
Scymbalium anale	(Nordmann, 1837)
Sciocoris cursitans	(Fabricius, 1794)
Sciocoris deltocephalus	Fieber, 1861
Sciocoris distinctus	Fieber, 1851
Sciocoris homalonotus	Fieber, 1851
Sciocoris macrocephalus	Fieber, 1851
Sciocoris microphthalmus	Flor, 1860
Sciocoris sulcatus	Fieber, 1851
Sciodrepoides fumatus	(Spence, 1815)
Sciodrepoides watsoni	(Spence, 1815)
Scirpophaga praelata	(Scopoli, 1763)
Scirtes hemisphaericus	(Linnaeus, 1767)
Scirtes orbicularis	(Panzer, 1793)
Sciurus vulgaris	Linnaeus, 1758
Sclerocona acutella	(Eversmann, 1842)
Scleropterus serratus	(Germar, 1824)
Scobicia chevrieri	(Villa, 1835)
Scolia hirta	(Schrank, 1781)
Scolia insubrica	(Scopoli, 1786)
Scolia sexmaculata	(Müller, 1776)
Scolia sexmaculata quadripunctata	(Müller, 1776)
Scoliopteryx libatrix	(Linnaeus, 1758)
Scolitantides orion	(Pallas, 1771)
Pseudophilotes vicrama schiffermuelleri	(Moore, 1865)
Scolopax rusticola	Linnaeus, 1758
Scolopendra cingulata	Latreille, 1829
Scolopostethus affinis	(Schilling, 1829)
Scolopostethus cognatus	Fieber, 1861
Scolopostethus decoratus	(Hahn, 1831)
Scolopostethus grandis	Horváth, 1880
Scolopostethus lethierryi	Jakovlev, 1877
Scolopostethus pictus	(Schilling, 1829)
Scolopostethus pilosus	Reuter, 1874
Scolopostethus puberulus	Horváth, 1887
Scolopostethus thomsoni	Reuter, 1874
Scolytus carpini	(Ratzeburg, 1837)
Scolytus ensifer	Eichhoff, 1881
Scolytus intricatus	(Ratzeburg, 1837)
Scolytus kirschii	Skalitzky, 1876
Scolytus koenigi	Schewyrew, 1890
Scolytus laevis	Chapuis, 1863
Scolytus mali	(Bechstein, 1805)
Scolytus multistriatus	(Marsham, 1802)
Scolytus pygmaeus	(Fabricius, 1787)
Scolytus ratzeburgii	Janson, 1856
Scolytus rugulosus	(Ph. W. J. Müller, 1818)
Scolytus scolytus	(Fabricius, 1775)
Scopaeus bicolor	Baudi, 1848
Scopaeus debilis	Hochhuth, 1851
Scopaeus didymus	Erichson, 1840
Scopaeus furcatus	Binaghi, 1935
Scopaeus gracilis	(Sperk, 1835)
Scopaeus laevigatus	(Gyllenhal, 1827)
Scopaeus minimus	(Erichson, 1839)
Scopaeus minutus	Erichson, 1840
Scopaeus pusillus	Kiesenwetter, 1843
Scopaeus ryei	Wollaston, 1872
Scopaeus sulcicollis	(Stephens, 1833)
Scoparia ambigualis	(Treitschke, 1829)
Scoparia ancipitella	(La Harpe, 1855)
Scoparia basistrigalis	Knaggs, 1866
Scoparia conicella	(La Harpe, 1863)
Scoparia ingratella	(Zeller, 1846)
Scoparia luteolaris	(Scopoli, 1772)
Scoparia manifestella	(Herrich-Schäffer, 1848)
Scoparia pyralella	(Denis & Schiffermüller, 1775)
Scoparia subfusca	Haworth, 1811
Scopula caricaria	(Reutti, 1853)
Scopula corrivalaria	(Kretschmar, 1862)
Scopula decorata	(Denis & Schiffermüller, 1775)
Scopula flaccidaria	(Zeller, 1852)
Scopula floslactata	(Haworth, 1809)
Scopula immorata	(Linnaeus, 1758)
Scopula immutata	(Linnaeus, 1758)
Scopula incanata	(Linnaeus, 1758)
Scopula marginepunctata	(Goeze, 1781)
Scopula nemoraria	(Hübner, 1799)
Scopula nigropunctata	(Hufnagel, 1767)
Scopula ornata	(Scopoli, 1763)
Scopula rubiginata	(Hufnagel, 1767)
Scopula subpunctaria	(Herrich-Schäffer, 1847)
Scopula umbelaria	(Hübner, 1813)
Scopula virgulata	(Denis & Schiffermüller, 1775)
Scotina celans	(Blackwall, 1841)
Scotochrosta pulla	([Denis & Schiffermüller], 1775)
Scotophaeus blackwalli	(Thorell, 1871)
Scotophaeus quadripunctatus	(Linnaeus, 1758)
Scotophaeus scutulatus	(L. Koch, 1866)
Scotopteryx bipunctaria	(Denis & Schiffermüller, 1775)
Scotopteryx chenopodiata	(Linnaeus, 1758)
Scotopteryx coarctaria	(Denis & Schiffermüller, 1775)
Scotopteryx luridata	(Hufnagel, 1767)
Scotopteryx moeniata	(Scopoli, 1763)
Scotopteryx mucronata	(Scopoli, 1763)
Scottia pseudobrowniana	Kempf, 1971
Scraptia dubia	Olivier, 1790
Scraptia ferruginea	Kiesenwetter, 1861
Scraptia fuscula	Müller, 1821
Scrobipalpa acuminatella	(Sircom, 1850)
Scrobipalpa artemisiella	(Treitschke, 1833)
Scrobipalpa atriplicella	(Fischer v. Röslerstamm, 1841)
Scrobipalpa chrysanthemella	(Hoffmann, 1867)
Scrobipalpa erichi	Povolny, 1964
Scrobipalpa gallicella	(Constant, 1885)
Scrobipalpa halonella	(Herrich-Schäffer, 1854)
Scrobipalpa hungariae	(Staudinger, 1871)
Scrobipalpa instabilella	(Douglas, 1846)
Scrobipalpa pauperella	(Heinemann, 1870)
Scrobipalpa nitentella	(Fuchs, 1902)
Scrobipalpa obsoletella	(Fischer v. Röslerstamm, 1841)
Scrobipalpa ocellatella	(Boyd, 1858)
Scrobipalpa proclivella	(Fuchs, 1886)
Scrobipalpa reiprichi	Povolny, 1984
Scrobipalpa salinella	(Zeller, 1847)
Scrobipalpa samadensis plantaginella	Pfaffenzeller, 1870
Scrobipalpula psilella	(Herrich-Schäffer, 1854)
Scrobipalpula tussilaginis	(Frey, 1867)
Scutigera coleoptrata	(Linnaeus, 1758)
Scydmaenus hellwigi	(Herbst, 1792)
Scydmaenus perrisi	Reitter, 1881
Scydmaenus rufus	(P.W.J. Müller et Kunze, 1822)
Scydmaenus tarsatus	(P.W.J. Müller et Kunze, 1822)
Scydmoraphes geticus	Saulcy, 1877
Scydmoraphes helvolus	(Schaum, 1844)
Scymnus abietis	(Paykull, 1798)
Scymnus apetzi	Mulsant, 1846
Scymnus ater	Kugelann, 1794
Scymnus auritus	Thunberg, 1795
Scymnus doriai	Capra, 1924
Scymnus femoralis	(Gyllenhal, 1827)
Scymnus ferrugatus	(Moll, 1785)
Scymnus fraxini	Mulsant, 1850
Scymnus frontalis	(Fabricius, 1787)
Scymnus haemorrhoidalis	Herbst, 1797
Scymnus impexus	Mulsant, 1850
Scymnus interruptus	(Goeze, 1777)
Scymnus limbatus	Stephens, 1831
Scymnus marginalis	(Rossi, 1794)
Scymnus mediterraneus	Iablokov-Khnzorian, 1972
Scymnus mimulus	Capra et Fürsch, 1967
Scymnus nigrinus	Kugelann, 1794
Scymnus pallipediformis	Günther, 1958
Scymnus pallipediformis apetzoides	Günther, 1958
Scymnus quadriguttatus	Fürsch et Kreissl, 1967
Scymnus rubromaculatus	(Goeze, 1777)
Scymnus sacium	Roubal, 1928
Scymnus silesiacus	Weise, 1902
Scymnus subvillosus	(Goeze, 1777)
Scymnus suturalis	Thunberg, 1795
Scythia craniumequinum	Kiritchenko,1938
Scythia festuceti	(Sulc,1941)
Scythris aerariella	(Herrich-Schäffer, 1855)
Scythris bengtssoni	Patocka & Liska, 1989
Scythris bifissella	(O. Hofmann, 1889)
Scythris crassiuscula	(Herrich-Schäffer, 1855)
Scythris cuspidella	(Denis & Schiffermüller, 1775)
Scythris emichi	(Anker, 1870)
Scythris fallacella	(Schläger, 1847)
Scythris flaviventrella	(Herrich-Schäffer, 1855)
Scythris fuscoaenea	(Haworth, 1828)
Scythris gozmanyi	Passerin d'EntrŐves, 1986
Scythris hungaricella	Rebel, 1917
Scythris laminella	(Denis & Schiffermüller, 1775)
Scythris limbella	(Fabricius, 1775)
Scythris obscurella	(Scopoli, 1763)
Scythris palustris	(Zeller, 1855)
Scythris pascuella	(Zeller, 1855)
Scythris paullella	(Herrich-Schäffer, 1855)
Scythris picaepennis	(Haworth, 1828)
Scythris podoliensis	Rebel, 1938
Scythris productella	(Zeller, 1839)
Scythris seliniella	(Zeller, 1839)
Scythris subseliniella	(Heinemann, 1876)
Scythris tabidella	(Herrich-Schäffer, 1855)
Scythris tributella	(Zeller, 1847)
Scythris vittella	(O. Costa, 1834)
Scythropia crataegella	(Linnaeus, 1767)
Scytodes thoracica	(Latreille, 1802)
Sedina buettneri	(E. Hering, 1858)
Segestria bavarica	C.L. Koch, 1843
Segestria florentina	(Rossi, 1790)
Segestria senoculata	(Linnaeus, 1758)
Segmentina nitida	(O.F. Müller, 1774)
Sehirus luctuosus	Mulsant & Rey, 1866
Sehirus morio	(Linnaeus, 1761)
Sehirus ovatus	(Herrich-Schäffer, 1840)
Sehirus parens	Mulsant & Rey, 1866
Selagia argyrella	(Denis & Schiffermüller, 1775)
Selagia spadicella	(Hübner, 1796)
Selania leplastriana	(Curtis, 1831)
Selatosomus aeneus	(Linnaeus, 1758)
Selatosomus cruciatus	(Linnaeus, 1758)
Selatosomus gravidus	(Germar, 1843)
Selenia dentaria	(Fabricius, 1775)
Selenia lunularia	(Hübner, 1788)
Selenia tetralunaria	(Hufnagel, 1767)
Selenocephalus obsoletus	(Germar, 1817)
Selenocephalus stenopterus	Signoret, 1880
Selenodes karelica	(Tengström, 1875)
Selidosema brunnearia	(Villers, 1789)
Selidosema plumaria	(Denis & Schiffermüller, 1775)
Semanotus russicus	(Fabricius, 1776)
Semanotus undatus	(Linnaeus, 1758)
Semidalis aleyrodiformis	(Stephens, 1836)
Semilimax semilimax	(J. Férussac, 1802)
Semioscopis avellanella	(Hübner, 1793)
Semioscopis oculella	(Thunberg, 1794)
Semioscopis steinkellneriana	(Denis & Schiffermüller, 1775)
Semioscopis strigulana	(Fabricius, 1787)
Senta flammea	(Curtis, 1828)
Sepedophilus binotatus	(Gravenhorst, 1802)
Sepedophilus bipunctatus	(Gravenhorst, 1802)
Sepedophilus bipustulatus	(Gravenhorst, 1802)
Sepedophilus constans	(Fowler, 1888)
Sepedophilus immaculatus	(Stephens, 1832)
Sepedophilus littoreus	(Linnaeus, 1758)
Sepedophilus lividus	(Erichson, 1839)
Sepedophilus marshami	(Stephens, 1832)
Sepedophilus obtusus	(Luze, 1902)
Sepedophilus pedicularius	(Gravenhorst, 1802)
Sepedophilus testaceus	(Fabricius, 1792)
Serica brunnea	(Linnaeus, 1758)
Sericoderus lateralis	(Gyllenhal, 1827)
Sericomyia silentis	(Harris, [1776])
Sericostoma flavicorne	Schneider, 1845
Sericostoma personatum	(Kirby et Spence, 1869)
Sericus brunneus	(Linnaeus, 1758)
Serinus serinus	(Linnaeus, 1766)
Serropalpus barbatus	(Schaller, 1783)
Insalebria gregella	(Zeller, 1839)
Insalebria serraticornella	(Zeller, 1839)
Sesia apiformis	(Clerck, 1759)
Sesia bembeciformis	(Hübner, 1806)
Sesia melanocephala	Dalman, 1816
Reptalus cuspidatus	(Fieber, 1876)
Setina irrorella	(Linnaeus, 1758)
Setina roscida	([Denis & Schiffermüller], 1775)
Setodes punctatus	(Fabricius, 1793)
Setodes viridis	(Fourcroy, 1785)
Shargacucullia gozmanyi	G. & L. Ronkay, 1994
Shargacucullia lychnitis	(Rambur, 1833)
Shargacucullia prenanthis	(Boisduval, 1840)
Shargacucullia scrophulariae	([Denis & Schiffermüller], 1775)
Shargacucullia lanceolata	(Villers, 1789)
Shargacucullia verbasci	(Linnaeus, 1758)
Siagonium humerale	Germar, 1835
Siagonium quadricorne	Kirby et Spence, 1815
Sialis fuliginosa	Pictet, 1836
Sialis lutaria	(Linnaeus, 1758)
Sialis morio	Klingstedt, 1932
Sialis nigripes	Pictet, 1865
Sibianor aurocinctus	(Ohlert, 1865)
Sibinia beckeri	Desbrochers, 1873
Sibinia femoralis	Germar, 1824
Sibinia hopffgarteni	Tournier, 1873
Sibinia pellucens	(Scopoli, 1772)
Sibinia phalerata	(Gyllenhal, 1836)
Sibinia primita	(Herbst, 1795)
Sibinia pyrrhodactyla	(Marsham, 1802)
Sibinia subelliptica	(Desbrochers, 1873)
Sibinia tibialis	(Gyllenhal, 1836)
Sibinia unicolor	(Fahraeus, 1843)
Sibinia variata	(Gyllenhal, 1836)
Sibinia viscariae	(Linnaeus, 1761)
Sibinia vittata	Germar, 1824
Sicista subtilis	(Pallas, 1773)
Sicista subtilis trizona	(Pallas, 1773)
Sida crystallina	(O.F. Müller, 1776)
Sideridis implexa	(Hübner, 1809)
Sideridis lampra	(Schawerda, 1913)
Sideridis reticulata	(Goeze, 1781)
Sideridis rivularis	(Fabricius, 1775)
Sideridis turbida	(Esper, 1790)
Siederia listerella	(Linnaeus,1758)
Sigara assimilis	(Fieber, 1848)
Sigara distincta	(Fieber, 1848)
Sigara falleni	(Fieber, 1848)
Sigara fossarum	(Leach, 1818)
Sigara lateralis	(Leach, 1818)
Sigara limitata	(Fieber, 1848)
Sigara nigrolineata	(Fieber, 1848)
Sigara semistriata	(Fieber, 1848)
Sigara striata	(Linnaeus, 1775)
Sigorus porcus	(Fabricius, 1792)
Silis nitidula	(Fabricius, 1792)
Crudosilis ruficollis	(Fabricius, 1775)
Silo nigricornis	(Pictet, 1834)
Silo pallipes	(Fabricius, 1781)
Silo piceus	(Brauer, 1857)
Silometopus curtus	(Simon, 1881)
Silometopus elegans	(O.P.-Cambridge, 1872)
Silometopus reussi	(Thorell, 1871)
Silpha carinata	Herbst, 1783
Silpha obscura	Linnaeus, 1758
Silpha tristis	Illiger, 1798
Silurus glanis	Linnaeus, 1758
Silusa rubiginosa	Erichson, 1837
Silusa rubra	Erichson, 1839
Silvanoprus fagi	(Guérin-Ménéville, 1844)
Silvanus bidentatus	(Fabricius, 1792)
Silvanus unidentatus	(Olivier, 1790)
Silvius alpinus	(Scopoli, 1763)
Simitidion simile	(C.L. Koch, 1836)
Simo hirticornis	(Herbst, 1795)
Simo variegatus	(Boheman, 1843)
Simocephalus exspinosus	(Koch, 1841)
Simocephalus serrulatus	(Koch, 1841)
Simocephalus vetulus	(O.F. Müller, 1776)
Simplicia rectalis	(Eversmann, 1842)
Simplimorpha promissa	(Staudinger, 1871)
Spudaea ruticilla	(Esper, 1791)
Simplocaria semistriata	(Fabricius, 1794)
Simyra albovenosa	(Goeze, 1781)
Simyra nervosa	([Denis & Schiffermüller], 1775)
Sinanodonta woodiana	(Lea, 1834)
Singa hamata	(Clerck, 1757)
Singa lucina	Audouin, 1826
Singa nitidula	C.L. Koch, 1844
Sinodendron cylindricum	(Linnaeus, 1758)
Sinoxylon perforans	(Schrank, 1789)
Sinoxylon sexdentatum	(Olivier, 1790)
Sintula corniger	(Blackwall, 1856)
Sintula retroversus	(O.P.-Cambridge, 1875)
Sintula spiniger	(Balogh, 1935)
Siona lineata	(Scopoli, 1763)
Siphlonurus aestivalis	(Eaton, 1903)
Siphlonurus armatus	(Eaton, 1870)
Siphlonurus lacustris	(Eaton, 1870)
Siphonoperla burmeisteri	(Pictet, 1841)
Siphonoperla neglecta	(Rostock, 1881)
Siphonoperla taurica	(Pictet, 1841)
Siphonoperla torrentium	(Pictet, 1841)
Siphonoperla transsylvanica	(Kis, 1963)
Sirocalodes depressicollis	(Gyllenhal, 1813)
Sirocalodes quercicola	(Paykull, 1792)
Sisyphus schaefferi	(Linnaeus, 1758)
Sisyra jutlandica	Esben-Petersen, 1915
Sisyra nigra	(Retzius, 1783)
Sisyra terminalis	Curtis, 1854
Sitaris muralis	(Forster, 1771)
Sitochroa palealis	(Denis & Schiffermüller, 1775)
Sitochroa verticalis	(Linnaeus, 1758)
Sitona ambiguus	Gyllenhal, 1834
Sitona callosus	Gyllenhal, 1834
Sitona cambricus	Stephens, 1831
Sitona cinerascens	(Fahraeus, 1840)
Sitona cylindricollis	(Fahraeus, 1840)
Sitona gressorius	(Fabricius, 1792)
Sitona griseus	(Fabricius, 1775)
Sitona hispidulus	(Fabricius, 1776)
Sitona humeralis	Stephens, 1831
Sitona inops	Gyllenhal, 1832
Sitona languidus	Gyllenhal, 1834
Sitona lateralis	Gyllenhal, 1834
Sitona lepidus	Gyllenhal, 1834
Sitona lineatus	(Linnaeus, 1758)
Sitona longulus	Gyllenhal, 1834
Sitona macularius	(Marsham, 1802)
Sitona puncticollis	Stephens, 1831
Sitona striatellus	Gyllenhal, 1834
Sitona sulcifrons	(Thunberg, 1798)
Sitona suturalis	Stephens, 1831
Sitona waterhousei	Walton, 1846
Sitophilus granarius	(Linnaeus, 1758)
Sitophilus oryzae	(Linnaeus, 1763)
Sitophilus zeamais	Motschulsky, 1855
Sitotroga cerealella	(Olivier, 1789)
Sitta europaea	Linnaeus, 1758
Sitta europaea caesia	Linnaeus, 1758
Sitticus caricis	(Westring, 1861)
Sitticus distinguendus	(L.Koch, 1870)
Sitticus dzieduszyckii	(L. Koch, 1870)
Sitticus floricola	(C.L. Koch, 1837)
Sitticus inexpectus	Logunov et Kronestedt, 1997
Sitticus penicillatus	(Simon, 1875)
Sitticus pubescens	(Fabricius, 1775)
Sitticus rupicola	(C.L. Koch, 1837) sensu
Sitticus saltator	(O. P.-Cambridge in Simon, 1868)
Sitticus saxicola	(C.L. Koch, 1848)
Sitticus zimmermanni	(Simon, 1877)
Smerinthus ocellatus	(Linnaeus, 1758)
Smicromyrme catanensis	(Rossi, 1790)
Smicromyrme cingulata	(Costa, 1858)
Smicromyrme curtiventris	(André, 1901)
Smicromyrme halensis	(Fabricius, 1787)
Smicromyrme padella	Nagy, 1978
Smicromyrme pliginskiji	Lelej, 1984
Smicromyrme pusilla	(Klug, 1835)
Smicromyrme ruficollis	(Fabricius, 1793)
Smicromyrme rufipes	(Fabricius, 1787)
Smicromyrme scutellaris	(Latreille, 1792)
Smicromyrme sericeiceps	(André, 1901)
Smicromyrme sicana	(Destefani, 1887)
Smicromyrme subcomata	(Wesmael, 1852)
Smicromyrme viduata	(Pallas, 1773)
Smicronyx brevicornis	F. Solari, 1952
Smicronyx cyaneus	(Dejean, 1821)
Smicronyx jungermanniae	(Reich, 1797)
Smicronyx pygmaeus	Curtis, 1840
Smicronyx reichii	(Gyllenhal, 1836)
Smicronyx smreczynskii	F. Solari, 1952
Smicronyx striatipennis	Tournier, 1874
Smicronyx swertiae	Voss, 1953
Smicrus filicornis	Faimaire et Laboulbene, 1855
Smithistruma baudueri	(Emery, 1875)
Solenopsis fugax	(Latreille, 1798)
Solenoxyphus fuscovenosus	(Fieber, 1864)
Solierella compedita	(Piccioli, 1869)
Somateria mollissima	(Linnaeus, 1758)
Somateria spectabilis	(Linnaeus, 1758)
Somatochlora flavomaculata	(Van Der Linden, 1825)
Somatochlora metallica	(Van Der Linden, 1825)
Sophronia ascalis	Gozmány, 1951
Sophronia chilonella	(Treitschke, 1833)
Sophronia consanguinella	Herrich-Schäffer, 1854
Sophronia humerella	(Denis & Schiffermüller, 1775)
Sophronia illustrella	(Hübner, 1796)
Sophronia marginella	Toll, 1936
Sophronia semicostella	(Hübner, 1813)
Sophronia sicariellus	(Zeller, 1839)
Sorex alpinus	Schinz, 1837
Sorex fodiens	Linnaeus, 1758
Sorex minutus	Linnaeus, 1766
Sorhagenia janiszewskae	Riedl, 1962
Sorhagenia lophyrella	(Douglas, 1846)
Sorhagenia rhamniella	(Zellar, 1839)
Sorhoanus assimilis	(Fallén, 1806)
Soronia grisea	(Linnaeus, 1758)
Soronia punctatissima	(Illiger, 1794)
Anisosticta vigintiguttata	(Linnaeus, 1758)
Sosticus loricatus	(L. Koch, 1866)
Spaelotis ravida	([Denis & Schiffermüller], 1775)
Spaniophaenus laticollis	(Miller, 1858)
Sparedrus testaceus	(Andersch, 1797)
Spargania luctuata	(Denis & Schiffermüller, 1775)
Sparganothis pilleriana	(Denis & Schiffermüller, 1775)
Spatalia argentina	([Denis & Schiffermüller], 1775)
Spatalistis bifasciana	(Hübner, 1787)
Spathocera laticornis	(Schilling, 1829)
Spathocera lobata	(Herrich-Schäffer, 1840)
Spathocera obscura	(Germar, 1842)
Spathocera tuberculata	Horváth, 1882
Spavius glaber	(Gyllenhal, 1808)
Spazigaster ambulans	(Fabricius, 1798)
Specodes albilabris	(Fabricius, 1793)
Specodes alternatus	Smith, 1853
Specodes crassus	Thomson, 1870
Specodes cristatus	Hagens, 1882
Specodes croaticus	Meyer, 1922
Specodes ephippius	(Linnaeus, 1767)
Specodes ferruginatus	Hagens, 1882
Specodes geofrellus	(Kirby, 1802)
Specodes gibbus	(Linnaeus, 1758)
Specodes hyalinatus	Hagens, 1882
Specodes intermedius	Blüthgen, 1923
Specodes longulus	Hagens, 1882
Specodes majalis	Pérez, 1903
Specodes miniatus	Hagens, 1882
Specodes monilicornis	(Kirby, 1802)
Specodes niger	Hagens, 1874
Specodes pellucidus	Smith, 1845
Specodes pseudofasciatus	Blüthgen, 1924
Specodes puncticeps	Thomson, 1870
Specodes reticulatus	Thomson, 1870
Specodes rubicundus	Hagens, 1875
Specodes rufiventris	(Panzer, 1798)
Specodes scabricollis	Wesmael, 1865
Specodes spinulosus	Hagens, 1875
Spelaeodiscus triarius	(Rossmässler, 1839)
Spercheus emarginatus	(Schaller, 1783)
Spermophilus citellus	(Linnaeus, 1766)
Spermophora senoculata	(Duges, 1836)
Speudotettix subfusculus	(Fallén, 1806)
Sphaeridium bipustulatum	Fabricius, 1781
Sphaeridium lunatum	Fabricius, 1792
Sphaeridium marginatum	Fabricius, 1787
Sphaeridium scarabaeoides	(Linnaeus, 1758)
Sphaeridium substriatum	Faldermann, 1838
Sphaeriestes castaneus	(Panzer, 1796)
Sphaeriestes stockmanni	(Biström, 1977)
Sphaerium corneum	(Linnaeus, 1758)
Sphaerium nucleus	(S. Studer, 1820)
Sphaerium rivicola	(Lamarck, 1818)
Sphaerium solidum	(Normand, 1844)
Sphaerolecanium prunastri	(Fonscolombe,1834)
Sphaerophoria batava	Goldlin de Tiefenau, 1974
Sphaerophoria fatarum	Goeldlin de Tiefenau, 1989
Sphaerophoria interrupta	(Fabricius, 1805)
Sphaerophoria loewi	Zetterstedt, 1843
Sphaerophoria philanthus	(Meigen, 1822)
Sphaerophoria rueppelli	(Wiedemann, 1830)
Sphaerophoria scripta	(Linnaeus, 1758)
Sphaerophoria shirchan	Violovitsh, 1957
Sphaerophoria taeniata	(Meigen, 1822)
Sphaerophoria virgata	Goldlin de Tiefenau, 1974
Sphaerosoma globosum	(Sturm, 1807)
Sphaerosoma pilosum	(Panzer, 1793)
Sphegina clavata	(Scopoli, 1763)
Sphegina clunipes	(Fallén, 1816)
Sphegina elegans	Schummel, 1843
Sphegina latifrons	Egger, 1865
Sphegina montana	Becker, 1921
Sphegina sibirica	Stackelberg, 1953
Sphegina verecunda	Collin, 1937
Sphenophorus abbreviatus	(Fabricius, 1787)
Sphenophorus piceus	(Pallas, 1771)
Sphenophorus striatopunctatus	(Goeze, 1777)
Sphenoptera antiqua	(Illiger, 1803)
Sphenoptera basalis	F. Morawitz, 1861
Sphenoptera parvula	(Fabricius, 1798)
Sphenoptera petriceki	Obenberger, 1949
Sphenoptera substriata	Krynicki, 1834
Sphex atropilosus	Kohl, 1885
Sphex flavipennis	Fabricius, 1793
Sphex rufocinctus	Brullé, 1833
Sphindus dubius	(Gyllenhal, 1808)
Sphindus grandis	Hampe, 1861
Sphinginus coarctatus	(Erichson, 1840)
Sphingonotus caerulans	(Linne, 1758)
Sphinx ligustri	Linnaeus, 1758
Sphiximorpha binominata	(Verrall, 1901)
Sphiximorpha subsessilis	(Rossi, 1807)
Sphodrus leucophthalmus	(Linnaeus, 1758)
Sphragisticus nebulosus	(Fallén, 1807)
Sphyradium doliolum	(Bruguiere, 1792)
Spialia orbifer	(Hübner, 1823)
Spialia sertorius	(Hoffmannsegg, 1804)
Spilococcus nanae	Schmutterer,1957
Spilomena mocsaryi	Kohl, 1898
Spilomena troglodytes	(Vander Linden, 1829)
Spilomyia diophthalma	(Linnaeus, 1758)
Spilomyia manicata	(Rondani, 1865)
Spilomyia saltuum	(Fabricius, 1794)
Spilonota laricana	(Heinemann, 1863)
Spilonota ocellana	(Denis & Schiffermüller, 1775)
Spilosoma lubricipeda	(Linnaeus, 1758)
Spilosoma lutea	(Hufnagel, 1766)
Spilosoma urticae	(Esper, 1789)
Spilosoma virginica	(Fabricius, 1758)
Spilostethus pandurus	(Scopoli, 1763)
Spilostethus saxatilis	(Scopoli, 1763)
Spinococcus callunetti	(Lindinger,1912)
Spinococcus marrubii	(Kiritchenko,1935)
Spinococcus morrisoni	(Kiritchenko,1935)
Spinolia insignis	Lucas, 1849
Spinolia unicolor	(Dahlbom, 1831)
Spintharina versicolor	(Spinola, 1808)
Coscinia striata	(Linnaeus, 1758)
Spodoptera exigua	(Hübner, 1808)
Spondylis buprestoides	(Linnaeus, 1758)
Spuleria flavicaput	(Haworth, 1828)
Spulerina simploniella	(Fischer v. Röslerstamm, 1840)
Squamapion atomarium	(Kirby, 1808)
Squamapion cineraceum	(Wencker, 1864)
Squamapion elongatum	(Germar, 1817)
Squamapion flavimanum	(Gyllenhal, 1833)
Squamapion hoffmanni	(Wagner, 1930)
Squamapion oblivium	(Schilsky, 1902)
Squamapion vicinum	(Kirby, 1808)
Stactobiella risi	(Felber, 1908)
Stagetus pilula	(Aubé, 1861)
Stagmatophora heydeniella	(Fischer v. Röslerstamm, 1841)
Stagnicola corvus	(Gmelin, 1791)
Stagnicola fuscus	(C. Pfeiffer, 1821)
Stagnicola palustris	(O.F. Müller, 1774)
Stagnicola turricula	(Held, 1836)
Stagonomus amoenus	(Brullé, 1832)
Stagonomus bipunctatus	(Linnaeus, 1758)
Stagonomus pusillus	(Herrich-Schäffer, 1830)
Stangeia siceliota	Zeller, 1847
Staphylinus caesareus	Cederhjelm, 1798
Staphylinus dimidiaticornis	Gemminger, 1851
Staphylinus erythropterus	Linnaeus, 1758
Staphylinus rubricornis	Ádám, 1987
Staria lunata	(Hahn, 1834)
Stathmopoda pedella	(Linnaeus, 1761)
Staudingeria deserticola	(Staudinger, 1870)
Stauroderus scalaris	(Fischer de Waldheim, 1846)
Staurophora celsia	(Linnaeus, 1758)
Stauropus fagi	(Linnaeus, 1758)
Steatoda albomaculata	(Dee Geer, 1778)
Steatoda bipunctata	(Linnaeus, 1758)
Steatoda castanea	(Clerck, 1757)
Steatoda grossa	(C.L. Koch, 1838)
Steatoda meridionalis	(Kulczynski, 1894)
Steatoda phalerata	(Panzer, 1801)
Steatoda triangulosa	(Walckenaer, 1802)
Stegania cararia	(Hübner, 1790)
Stegania dilectaria	(Hübner, 1790)
Stegobium paniceum	(Linnaeus, 1758)
Steingelia gorodetskia	Nassonov,1908
Stelis annulata	(Lepeletier, 1841)
Stelis breviuscula	(Nylander, 1848)
Stelis jugae	Noskiewicz, 1962
Stelis minuta	Lepeletier & Serville, 1825
Stelis nasuta	(Latreille, 1809)
Stelis odontopyga	Noskiewicz, 1925
Stelis ornatula	(Klug, 1807)
Stelis phaeoptera	(Kirby, 1802)
Stelis punctulatissima	(Kirby, 1802)
Stelis signata	(Latreille, 1809)
Stemonyphantes lineatus	(Linnaeus, 1758)
Stenagostus rhombeus	(Olivier, 1790)
Stenagostus rufus	(De Geer, 1774)
Stenalia escherichi	Schilsky,  1899
Stenamma debile	(Foerster, 1850)
Stenhomalus bicolor	(Kraatz, 1862)
Stenichnus carpathicus	Lokay, 1918
Stenichnus collaris	(P.W.J. Müller et Kunze, 1822)
Stenichnus godarti	(Latreille, 1806)
Stenichnus scutellaris	(P.W.J. Müller et Kunze, 1822)
Stenistoderus nothus	(Erichson, 1839)
Stenobothrus crassipes	(Charpentier, 1825)
Stenobothrus eurasius	Zubowski, 1898
Stenobothrus Fischeri	(Eversmann, 1848)
Stenobothrus lineatus	(Panzer, 1796)
Stenobothrus nigromaculatus	(Herrich-Schaeffer, 1840)
Stenobothrus stigmaticus	(Rambour, 1838)
Stenocarus cardui	(Herbst, 1784)
Stenocarus ruficornis	(Stephens, 1831)
Stenocorus meridianus	(Linnaeus, 1758)
Stenocranus fuscovittatus	(Stal, 1858)
Stenocranus major	(Kirschbaum, 1868)
Stenocranus minutus	(Fabricius, 1787)
Stenocypria fischeri	(Lilljeborg, 1883)
Stenodema calcarata	(Fallén, 1807)
Stenodema holsata	(Fabricius, 1787)
Stenodema laevigata	(Linnaeus, 1758)
Stenodema sericans	Fieber, 1861
Stenodema virens	(Linnaeus, 1767)
Stenodynerus bluethgeni	Van Der Vecht, 1971
Stenodynerus chevrieranus	(Saussure, 1855)
Stenodynerus clypeopictus	(Kostylev, 1940)
Stenodynerus orenburgensis	(André, 1884)
Stenodynerus punctifrons	(Thomson, 1874)
Stenodynerus steckianus	(Schultess, 1897)
Stenodynerus xanthomelas	(Herrich-Schaeffer, 1839)
Stenolechia gemmella	(Linnaeus, 1758)
Parastenolechia nigrinotella	(Zeller, 1847)
Stenolechiodes pseudogemmellus	Elsner, 1996
Stenolophus discophorus	Fischer, 1824
Stenolophus mixtus	(Herbst, 1784)
Stenolophus persicus	Mannerheim, 1844
Stenolophus proximus	Dejean, 1829
Stenolophus skrimshiranus	(Stephens, 1828)
Stenolophus steveni	Krynicki, 1832
Stenolophus teutonus	(Schrank, 1781)
Stenomax aeneus incurvus	(Scopoli, 1763)
Stenopelmus rufinasus	Gyllenhal, 1836
Stenophylax meridiorientalis	Malicky, 1980
Stenophylax permistus	McLachlan, 1895
Stenophylax vibex	(Curtis, 1834)
Stenopterapion intermedium	(Eppelsheim, 1875)
Stenopterapion meliloti	(Kirby, 1808)
Stenopterapion tenue	(Kirby, 1808)
Stenopterus flavicornis	Küster, 1846
Stenopterus rufus	(Linnaeus, 1767)
Stenoptilia annadactyla	Sutter, 1988
Stenoptilia bipunctidactyla	(Scopoli, 1763)
Stenoptilia coprodactylus	(Stainton, 1851)
Stenoptilia graphodactyla	(Treitschke, 1833)
Stenoptilia gratiolae	Gibeaux & Nel, 1990
Stenoptilia pelidnodactyla	(Stein, 1837)
Stenoptilia pneumonanthes	(Bütter,1880)
Stenoptilia pterodactyla	(Linnaeus, 1761)
Stricticomus tobias	(Marseul, 1879)
Stenoptilia stigmatodactylus	(Zeller, 1852)
Stenoptilia stigmatoides	Sutter & Skyva, 1992
Stenoptilia zophodactylus	(Duponchel, 1840)
Stenoptinea cyaneimarmorella	(MilliŐre, 1854)
Stenoria apicalis	(Latreille, 1804)
Stenostola dubia	(Laicharting, 1784)
Stenostola ferrea	(Schrank, 1776)
Stenotus binotatus	(Fabricius, 1794)
Stenurella bifasciata	(O. F. Müller, 1776)
Stenurella melanura	(Linnaeus, 1758)
Stenurella nigra	(Linnaeus, 1758)
Stenurella septempunctata	(Fabricius, 1792)
Stenus ampliventris	J.Sahlberg, 1890
Stenus annulipes	Heer, 1839
Stenus argus	Gravenhorst, 1806
Stenus asphaltinus	Erichson, 1840
Stenus assequens	Rey, 1884
Stenus ater	Mannerheim, 1830
Stenus aterrimus	Erichson, 1839
Stenus atratulus	Erichson, 1839
Stenus bifoveolatus	Gyllenhal, 1827
Stenus biguttatus	(Linnaeus, 1758)
Stenus bimaculatus	Gyllenhal, 1810
Stenus binotatus	Ljungh, 1804
Stenus boops	Ljungh, 1810
Stenus brunnipes	Stephens, 1833
Stenus canaliculatus	Gyllenhal, 1827
Stenus carbonarius	Gyllenhal, 1827
Stenus carpathicus	Ganglbauer, 1896
Stenus cautus	Erichson, 1839
Stenus cicindeloides	(Schaller, 1783)
Stenus circularis	Gravenhorst, 1802
Stenus claritarsis	Puthz, 1971
Stenus clavicornis	(Scopoli, 1763)
Stenus comma	Le Conte, 1863
Stenus crassus	Stephens, 1833
Stenus doderoi	Bondroit, 1912
Stenus eumerus	Kiesenwetter, 1850
Stenus europaeus	Puthz, 1966
Stenus excubitor	Erichson, 1839
Stenus exspectatus	Puthz, 1965
Stenus flavipalpis	Thomson, 1860
Stenus flavipes	Stephens, 1833
Stenus formicetorum	Mannerheim, 1843
Stenus fornicatus	Stephens, 1833
Stenus fossulatus	Erichson, 1840
Stenus fuscicornis	Erichson, 1840
Stenus fuscipes	Gravenhorst, 1802
Stenus glabellus	Thomson, 1870
Stenus guttula	Ph.W.J.Müller, 1821
Stenus horioni	Puthz, 1971
Stenus humilis	Erichson, 1839
Stenus hypoproditor	Puthz, 1965
Stenus impressus	Germar, 1824
Stenus incanus	Erichson, 1839
Stenus incrassatus	Erichson, 1839
Stenus indifferens	Puthz, 1967
Stenus intermedius	Rey, 1884
Stenus intricatus	Erichson, 1840
Stenus intricatus zoufali	Erichson, 1840
Stenus josefkrali	Hromádka, 1981
Stenus juno	(Paykull, 1789)
Stenus kiesenwetteri	Rosenhauer, 1856
Stenus kolbei	Gerhardt, 1893
Stenus latifrons	Erichson, 1839
Stenus longipes	Heer, 1839
Stenus longitarsis	Thomson, 1858
Stenus ludyi	Fauvel, 1886
Stenus lustrator	Erichson, 1839
Stenus maculiger	Weise, 1875
Stenus melanarius	Stephens, 1833
Stenus melanopus	(Marsham, 1802)
Stenus morio	Gravenhorst, 1806
Stenus nanus	Stephens, 1833
Stenus nigritulus	Gyllenhal, 1827
Stenus nitens	Stephens, 1833
Stenus obscuripalpis	Hubenthal, 1911
Stenus ochropus	Kiesenwetter, 1858
Stenus opticus	Gravenhorst, 1806
Stenus pallipes	Gravenhorst, 1802
Stenus pallitarsis	Stephens, 1833
Stenus palposus	Zetterstedt, 1828
Stenus palustris	Erichson, 1839
Stenus phyllobates	Penecke, 1901
Stenus picipennis	Erichson, 1840
Stenus picipes brevipennis	Stephens, 1833
Stenus planifrons	Rey, 1884
Stenus proditor	Erichson, 1839
Stenus providus	Erichson, 1839
Stenus pseudoboops	Puthz, 1966
Stenus pubescens	Stephens, 1833
Stenus pumilio	Erichson, 1839
Stenus pusillus	Stephens, 1833
Stenus ruralis	Erichson, 1840
Stenus scrutator	Erichson, 1840
Stenus similis	(Herbst, 1784)
Stenus solutus	Erichson, 1840
Stenus stigmula	Erichson, 1840
Stenus subaeneus	Erichson, 1840
Stenus sylvester	Erichson, 1839
Stenus tarsalis	Ljungh, 1810
Stenus vastus	L.Benick, 1925
Stephanitis pyri	(Fabricius, 1822)
Stephanocleonus tetragrammus	(Pallas, 1781)
Stephensia brunnichella	(Linnaeus, 1767)
Stephostethus alternans	(Mannerheim, 1844)
Stephostethus angusticollis	(Gyllenhal, 1827)
Stephostethus lardarius	(De Geer, 1775)
Stephostethus pandellei	(Brisout de Barneville, 1863)
Stephostethus rugicollis	(Olivier, 1790)
Stephostethus rybinskii	(Reitter, 1894)
Stephostethus sinuatocollis	(Faldermann, 1837)
Stercorarius longicaudus	Vieillot, 1819
Stercorarius parasiticus	(Linnaeus, 1758)
Stercorarius pomarinus	(Temminck, 1815)
Catharacta skua	Brünnich, 1764
Stereocorynes truncorum	(Germar, 1824)
Stereonychus fraxini	(De Geer, 1775)
Sterna albifrons	Pallas, 1764
Sterna caspia	Pallas, 1770
Sterna hirundo	Linnaeus, 1758
Sterna paradisaea	Pontoppidan, 1763
Sterna sandvicensis	Latham, 1787
Sternodontus binodulus	Jakovlev, 1893
Sternodontus obtusus	Mulsant & Rey, 1856
Sterrhopterix fusca	(Haworth, 1809)
Stethoconus pyri	(Mella, 1869)
Stethorus punctillum	Weise, 1891
Sthenarus rotermundi	Scholtz, 1846
Stibaropus henkei	(Jakovlev, 1874)
Stichoglossa semirufa	(Erichson, 1839)
Stictocephala bisonia	Kopp & Yonke, 1977
Stictocoris picturatus	(C. Sahlberg, 1842)
Stictoleptura erythroptera	(Hagenbach, 1822)
Stictoleptura rubra	(Linnaeus, 1758)
Stictoleptura scutellata	(Fabricius, 1781)
Stictopleurus abutilon	(Rossi, 1790)
Stictopleurus crassicornis	(Linnaeus, 1758)
Stictopleurus pictus	Fieber, 1861
Stictopleurus punctatonervosus	(Goeze, 1778)
Stictopleurus subtomentosus	(Rey, 1888)
Stigmella aceris	(Frey, 1857)
Stigmella aeneofasciella	(Herrich-Schäffer, 1855)
Stigmella alnetella	(Stainton, 1856)
Stigmella anomalella	(Goeze, 1783)
Stigmella assimilella	(Zeller, 1848)
Stigmella atricapitella	(Haworth, 1828)
Stigmella aurella	(Fabricius, 1775)
Stigmella basiguttella	(Heinemann, 1862)
Stigmella benanderella	(Wolff, 1955)
Stigmella betulicola	(Stainton, 1856)
Stigmella carpinella	(Heinemann, 1862)
Stigmella catharticella	(Stainton, 1853)
Stigmella centifoliella	(Zeller, 1848)
Stigmella confusella	(Wood & Walsingham, 1894)
Stigmella continuella	(Stainton, 1856)
Stigmella crataegella	(Klimesch, 1936)
Stigmella desperatella	(Frey, 1856)
Stigmella dorsiguttella	(Johansson, 1971)
Stigmella eberhardi	(Johansson, 1971)
Stigmella filipendulae	(Wocke, 1871)
Stigmella floslactella	(Haworth, 1828)
Stigmella freyella	(Heyden, 1858)
Stigmella glutinosae	(Stainton, 1858)
Stigmella hahniella	(Wörtz, 1890)
Stigmella hemargyrella	(Kollar, 1832)
Stigmella hybnerella	(Hübner, 1813)
Stigmella incognitella	(Herrich-Schäffer, 1855)
Stigmella lemniscella	(Zeller, 1839)
Stigmella lonicerarum	(Frey, 1856)
Stigmella luteella	(Stainton, 1857)
Stigmella magdalenae	(Klimesch, 1950)
Stigmella malella	(Stainton, 1854)
Stigmella mespilicola	(Frey, 1856)
Stigmella microtheriella	(Stainton, 1854)
Stigmella minusculella	(Herrich-Schäffer, 1855)
Stigmella naturnella	(Klimesch, 1936)
Stigmella nivenburgensis	(Preissecker, 1942)
Stigmella nylandriella	(Tengström, 1848)
Stigmella obliquella	(Heinemann, 1862)
Stigmella oxyacanthella	(Stainton, 1854)
Stigmella paradoxa	(Frey, 1858)
Stigmella perpygmaeella	(Doubleday, 1859)
Stigmella plagicolella	(Stainton, 1854)
Stigmella poterii	(Stainton, 1857)
Stigmella prunetorum	(Stainton, 1855)
Stigmella pyri	(Glitz, 1865)
Stigmella regiella	(Herrich-Schäffer, 1855)
Stigmella rhamnella	(Herrich-Schäffer, 1860)
Stigmella roborella	(Johansson, 1971)
Stigmella rolandi	(Nieukerken, 1990)
Stigmella ruficapitella	(Haworth, 1828)
Stigmella sakhalinella	Puplesis, 1984
Stigmella salicis	(Stainton, 1854)
Stigmella samiatella	(Zeller, 1839)
Stigmella sanguisorbae	(Wocke, 1865)
Stigmella speciosa	(Frey, 1857)
Stigmella splendidissimella	(Herrich-Schäffer, 1855)
Stigmella svenssoni	(Johansson, 1971)
Stigmella szoecsiella	(Borkowski, 1972)
Stigmella thuringiaca	(Petry, 1904)
Stigmella tiliae	(Frey, 1856)
Stigmella tityrella	(Stainton, 1854)
Stigmella tormentillella	(Herrich-Schäffer, 1860)
Stigmella torminalis	(Wood, 1890)
Stigmella trimaculella	(Haworth, 1828)
Stigmella ulmiphaga	(Preissecker, 1942)
Stigmella ulmivora	(Fologne, 1860)
Stigmella viscerella	(Stainton, 1853)
Stigmella zangherii	(Klimesch, 1951)
Stigmus pendulus	Panzer, 1804
Stigmus solskyi	Morawitz, 1864
Stilbum calens	(Fabricius, 1781)
Stilbum calens zimmermanni	(Fabricius, 1781)
Stilbum cyanurum	Forster, 1771
Stilbus atomarius	(Linnaeus, 1767)
Stilbus oblongus	(Erichson, 1845)
Stilbus pannonicus	Franz, 1968
Stilbus testaceus	(Panzer, 1797)
Stiroma affinis	Fieber, 1866
Stiroma bicarinata	(Herrich-Schäffer, 1835)
Stiromoides maculiceps	(Horváth, 1903)
Stizoides tridentatus	(Fabricius, 1775)
Stizus perrisii	Dufour, 1838
Stomis pumicatus	(Panzer, 1796)
Stomodes gyrosicollis	(Boheman, 1843)
Stomopteryx detersella	(Zeller, 1847)
Stomopteryx hungaricella	Gozmány, 1957
Stomopteryx remissella	(Zeller, 1847)
Stosatea italica	(Latzel, 1886)
Strangalia attenuata	(Linnaeus, 1758)
Streblocerus serricaudatus	(Fischer, 1849))
Streptanus aemulans	(Kirschbaum, 1868)
Streptanus marginatus	(Kirschbaum, 1858)
Streptanus sordidus	(Zetterstedt, 1828)
Streptocephalus torvicornis	(Waga, 1842)
Streptopelia decaocto	(Frivaldszky, 1838)
Streptopelia orientalis	(Latham, 1790)
Streptopelia orientalis meena	(Latham, 1790)
Streptopelia turtur	(Linnaeus, 1758)
Streyella anguinella	(Hübner, 1813)
Strigamia acuminata	(Leach, 1815)
Strigamia crassipes	(C.L.Koch, 1835)
Strigamia trannsylvanica	(Verhoeff, 1935)
Strix aluco	Linnaeus, 1758
Strix uralensis	Pallas, 1771
Strix uralensis macroura	Pallas, 1771
Stroemiellus stroemi	(Thorell, 1870)
Stroggylocephalus agrestis	(Fallén, 1806)
Stroggylocephalus livens	(Zetterstedt, 1840)
Stromatium unicolor	(Olivier, 1795)
Strongylocoris leucocephalus	(Linnaeus, 1758)
Strongylocoris luridus	(Fallén, 1807)
Strongylocoris niger	(Herrich-Schäffer, 1835)
Strongylognathus testaceus	Schenck, 1852
Strongylosoma stigmatosum	(Eichwald, 1830)
Strophedra nitidana	(Fabricius, 1794)
Strophedra weirana	(Douglas, 1850)
Strophomorphus porcellus	(Schönherr, 1832)
Strophosoma capitatum	(DeGeer, 1775)
Strophosoma melanogrammum	(Forster, 1771)
Struebingianella lugubrina	(Boheman, 1847)
Sturnus roseus	(Linnaeus, 1758)
Sturnus vulgaris	Linnaeus, 1758
Stygnocoris faustus	Horváth, 1888
Stygnocoris fuligineus	(Geoffroy, 1785)
Stygnocoris pygmaeus	(F. Sahlberg, 1848)
Stygnocoris rusticus	(Fallén, 1807)
Stygnocoris sabulosus	(Schilling, 1829)
Styloctetor romanus	(O.P.-Cambridge, 1872)
Styloctetor stativus	(Simon, 1881)
Styrioiulus pelidnus	(Latzel, 1884)
Styrioiulus pelidnus orientalis	(Latzel, 1884)
Styrioiulus styricus	(Verhoeff, 1896)
Subcoccinella vigintiquatuorpunctata	(Linnaeus, 1758)
Subilla confinis	(Stephens, 1836)
Subrinus sturmi	(Harold, 1870)
Succinea putris	(Linnaeus, 1758)
Succinella oblonga	(Draparnaud, 1801)
Sulcacis affinis	(Gyllenhal, 1827)
Sulcacis bidentulus	(Rosenhauer, 1847)
Sulcacis fronticornis	(Panzer, 1809)
Sulcopolistes atrimandibularis	(Zimmermann, 1930)
Sulcopolistes semenowi	(Morawitz, 1889)
Sulcopolistes sulcifer	(Zimmermann, 1930)
Sunius fallax	(Lokay, 1919)
Sunius melanocephalus	(Fabricius, 1792)
Suphrodytes dorsalis	(Fabricius, 1787)
Surnia ulula	(Linnaeus, 1758)
Sus scrofa	Linnaeus, 1758
Swammerdamia caesiella	(Hübner, 1796)
Swammerdamia compunctella	(Henrich-Schaffer,1855)
Swammerdamia pyrella	(Villers, 1789)
Syedra gracilis	(Menge, 1869)
Sylvia atricapilla	(Linnaeus, 1758)
Sylvia borin	(Boddaert, 1783)
Sylvia cantillans	(Pallas, 1764)
Sylvia cantillans albistriata	(Pallas, 1764)
Sylvia communis	Latham, 1787
Sylvia curruca	(Linnaeus, 1758)
Sylvia melanocephala	(Gmelin, 1771)
Sylvia nisoria	(Bechstein, 1795)
Symbiomyrma karawajevi	Arnoldi 1930
Symbiotes gibberosus	(Lucas, 1849)
Symmorphus angustatus	(Zetterstedt, 1838)
Symmorphus bifasciatus	(Linnaeus, 1761)
Symmorphus connexus	(Curtis, 1826)
Symmorphus crassicornis	(Panzer, 1798)
Symmorphus debilitatus	(Saussure, 1855)
Symmorphus declivis	Harttig, 1932
Symmorphus gracilis	(Brullé, 1832)
Symmorphus murarius	(Linnaeus, 1758)
Sympecma fusca	(Van Der Linden, 1823)
Sympetrum danae	(Sulzer, 1776)
Sympetrum depressiusculum	(Sélys-Longchamps, 1841)
Sympetrum flaveolum	(Linné, 1758)
Sympetrum fonscolombii	(Sélys-Longchamps, 1840)
Sympetrum meridionale	(Sélys-Longchamps, 1841)
Sympetrum pedemontanum	(Allioni, 1766)
Sympetrum sanguineum	(Müller, 1764)
Sympetrum striolatum	(Charpentier, 1840)
Sympetrum vulgatum	(Linné, 1758)
Sympherobius elegans	(Stephens, 1836)
Sympherobius fuscescens	(Wallengren, 1863)
Sympherobius klapaleki	Zelený, 1963
Sympherobius pellucidus	(Walker, 1853)
Sympherobius pygmaeus	(Rambur, 1842)
Synema globosum	(Fabricius, 1775)
Synema ornatum	(Thorell, 1875)
Synagapetus armatus	(McLachlan, 1879)
Synagapetus iridipennis	McLachlan, 1879
Synagapetus krawanyi	Ulmer, 1938
Synagapetus moselyi	(Ulmer, 1938)
Sitticus hilarulus	(C.L. Koch, 1846)
Sitticus subcingulatus	(Simon, 1878)
Sitticus venator	(Lucas, 1836)
Pyropteron affine	(Staudinger, 1856)
Pyropteron muscaeforme	(Esper, 1783)
Pyropteron triannuliforme	(Freyer, 1843)
Synanthedon andrenaeformis	(Laspeyres, 1801)
Synanthedon cephiformis	(Ochsenheimer, 1808)
Synanthedon conopiformis	(Esper, 1782)
Synanthedon culiciformis	(Linnaeus, 1758)
Synanthedon flaviventris	(Staudinger, 1883)
Synanthedon formicaeformis	(Esper, 1783)
Synanthedon loranthi	(Králicek, 1966)
Synanthedon melliniformis	(Laspeyres, 1801)
Synanthedon mesiaeformis	(Herrich-Schäffer, 1846)
Synanthedon myopaeformis	(Borkhausen, 1789)
Synanthedon spheciformis	(Denis & Schiffermüller, 1775)
Synanthedon spuleri	(Fuchs, 1908)
Synanthedon stomoxiformis	(Hübner, 1790)
Synanthedon tipuliformis	(Clerck, 1759)
Synanthedon vespiformis	(Linnaeus, 1761)
Synaphe antennalis	(Fabricius, 1794)
Synaphe bombycalis	(Denis & Schiffermüller, 1775)
Synaphe moldavica	(Esper, 1794)
Synaphe punctalis	(Fabricius, 1775)
Synapion ebeninum	(Kirby, 1808)
Synaptus filiformis	(Fabricius, 1781)
Synchita humeralis	(Fabricius, 1792)
Synchita mediolanensis	Villa, 1836
Synchita separanda	(Reitter, 1881)
Synclisis baetica	(Rambur, 1842)
Syncopacma albifrontella	(Heinemann, 1870)
Syncopacma albipalpella	(Herrich-Schäffer, 1857)
Syncopacma captivella	(Herrich-Schäffer, 1854)
Syncopacma cinctella	(Clerck, 1759)
Syncopacma cincticulella	(Bruand, 1850)
Syncopacma coronillella	(Treitschke, 1833)
Syncopacma linella	(Chrétien, 1904)
Syncopacma ochrofasciella	(Toll, 1936)
Syncopacma patruella	(Mann, 1857)
Syncopacma sangiella	(Stainton, 1863)
Syncopacma taeniolella	(Zeller, 1839)
Syncopacma vinella	(Bankes, 1898)
Syndemis musculana	(Hübner, 1799)
Parlatoria parlatoriae	(Šulc, 1895)
Syngrapha ain	(Hochenwarth, 1785)
Syngrapha interrogationis	(Linnaeus, 1758)
Synopsia sociaria	(Hübner, 1799)
Syntomium aeneum	(Ph.W.J.Müller, 1821)
Syntomus foveatus	(Fourcroy, 1785)
Syntomus obscuroguttatus	(Duftschmid, 1812)
Syntomus pallipes	(Dejean, 1825)
Syntomus truncatellus	(Linnaeus, 1761)
Synuchus vivalis	(Illiger, 1798)
Syritta pipiens	(Linnaeus, 1758)
Syromastus rhombeus	(Linnaeus, 1767)
Syrphus ribesii	(Linnaeus, 1758)
Syrphus sexmaculatus	(Zetterstedt, 1838)
Syrphus torvus	Osten-Sacken, 1875
Syrphus vitripennis	Meigen, 1822
Syrrhaptes paradoxus	(Pallas, 1773)
Systellonotus triguttatus	(Linnaeus, 1767)
Systropha curvicornis	(Scopoli, 1770)
Systropha planidens	Giraud, 1861
Tabanus autumnalis	Linnaeus, 1761
Tabanus bifarius	Loew, 1858
Tabanus bovinus	Linnaeus, 1758
Tabanus bromius	Linnaeus, 1758
Tabanus cordiger	Meigen, 1820
Tabanus exclusus	Pandelle, 1883
Tabanus glaucopis	Meigen, 1820
Tabanus maculicornis	Zetterstedt, 1842
Tabanus miki	Brauer, 1880
Tabanus paradoxus	Jaennicke, 1866
Tabanus quatuornotatus	Meigen, 1820
Tabanus regularis	Jaennicke, 1866
Tabanus spectabilis	Loew, 1858
Tabanus spodopterus	Meigen, 1820
Tabanus sudeticus	Zeller, 1842
Tabanus tergestinus	Egger, 1859
Tabanus unifasciatus	Loew, 1858
Tachinus bipustulatus	(Fabricius, 1792)
Tachinus bonvouloiri	PandellÉ, 1869
Tachinus corticinus	Gravenhorst, 1802
Tachinus discoideus	Erichson, 1839
Tachinus fimetarius	Gravenhorst, 1802
Tachinus humeralis	Gravenhorst, 1802
Tachinus laticollis	Gravenhorst, 1802
Tachinus lignorum	(Linnaeus, 1758)
Tachinus marginatus	Gyllenhal, 1810
Tachinus marginellus	(Fabricius, 1781)
Tachinus pallipes	(Gravenhorst, 1806)
Tachinus rufipennis	Gyllenhal, 1810
Tachinus scapularis	Stephens, 1832
Tachinus signatus	Gravenhorst, 1802
Tachinus subterraneus	(Linnaeus, 1758)
Tachyagetes dudichi	Móczár, 1944
Tachyagetes filicornis	(Tournier, 1890)
Tachybaptus ruficollis	(Pallas, 1764)
Tachycixius desertorum	(Fieber, 1876)
Tachycixius pilosus	(Olivier, 1791)
Tachycixius soosi	Orosz, 1999
Tachyerges decoratus	(Germar, 1821)
Tachyerges pseudostigma	(Tempere, 1982)
Tachyerges rufitarsis	(Germar, 1821)
Tachyerges salicis	(Linnaeus, 1758)
Tachyerges stigma	(Germar, 1821)
Tachyporus abdominalis	(Fabricius, 1781)
Tachyporus atriceps	Stephens, 1832
Tachyporus caucasicus	Kolenati, 1846
Tachyporus chrysomelinus	(Linnaeus, 1758)
Tachyporus corpulentus	J.Sahlberg, 1876
Tachyporus dispar	(Paykull, 1789)
Tachyporus formosus	Matthews, 1838
Tachyporus hypnorum	(Fabricius, 1775)
Tachyporus nitidulus	(Fabricius, 1781)
Tachyporus obtusus	(Linnaeus, 1767)
Tachyporus pallidus	Sharp, 1871
Tachyporus pulchellus	Mannerheim, 1841
Tachyporus pusillus	Gravenhorst, 1806
Tachyporus quadriscopulatus	Pandellé, 1869
Tachyporus ruficollis	Gravenhorst, 1802
Tachyporus scitulus	Erichson, 1839
Tachyporus solutus	Erichson, 1839
Tachyporus tersus	Erichson, 1839
Tachyporus transversalis	Gravenhorst, 1806
Tachyporus vafer	Schülke, 1996
Tachys scutellaris	Stephens, 1828
Tachysphex bicolor	Brullé, 1856
Tachysphex fugax	(Radoszkowski, 1877)
Tachysphex fulvitarsis	(Costa, 1867)
Tachysphex grandii	Beaumont, 1965
Tachysphex helveticus	Kohl, 1885
Tachysphex incertus	(Radoszkowski, 1877)
Tachysphex mediterraneus	Kohl, 1883
Tachysphex mocsaryi	Kohl, 1884
Tachysphex nitidus	(Spinola, 1805)
Tachysphex obscuripennis	(Schenck, 1857)
Tachysphex panzeri	(Vander Linden, 1829)
Tachysphex plicosus	(Costa, 1867)
Tachysphex pompiliformis	(Spinola, 1804)
Tachysphex psammobius	(Kohl, 1880)
Tachysphex rugosus	Gussakovskij, 1952
Tachysphex tarsinus	(Lepeletier, 1845)
Tachyta nana	(Gyllenhal, 1810)
Tachytes etruscus	(Rossi, 1790)
Tachytes europaeus	Kohl, 1884
Tachytes obsoletus	(Rossi, 1792)
Tachyusa coarctata	Erichson, 1837
Tachyusa concinna	(Heer, 1839)
Tachyusa constricta	Erichson, 1837
Tachyusa exarata	(Mannerheim, 1830)
Tachyusa nitella	Fauvel, 1895
Tachyusa objecta	Mulsant et Rey, 1870
Tachyusa scitula	Erichson, 1837
Tadorna ferruginea	(Pallas, 1764)
Tadorna tadorna	(Linnaeus, 1758)
Taeniapion rufulum	(Wencker, 1864)
Taeniapion urticarium	(Herbst, 1784)
Taeniopteryx araneoides	Klapálek, 1902
Taeniopteryx nebulosa	(Linnaeus, 1758)
Taeniopteryx schoenemundi	Mertens, 1923
Sitticus aequipes	(O. P.-Cambridge, 1871)
Sitticus monticola	(Kulczynski, 1884)
Sitticus petrensis	(C.L. Koch, 1837)
Sitticus thorelli	(Kulczynski, 1891)
Taleporia politella	(Ochsenheimer, 1816)
Taleporia tubulosa	(Retzius, 1783)
Talis quercella	(Denis & Schiffermüller, 1775)
Tallusia experta	(O.P.-Cambridge, 1871)
Tallusia vindobonensis	(Kulczynski, 1898)
Talpa europaea	Linnaeus, 1758
Tandonia budapestensis	(Hazay, 1881)
Tandonia rustica	(Millet, 1843)
Tanymastix lacunae	(Linnaeus, 1758)
Tanymecus dilaticollis	Gyllenhal, 1834
Tanymecus palliatus	(Fabricius, 1787)
Tanysphyrus ater	Blatchley, 1928
Tanysphyrus lemnae	(Paykull, 1792)
Tapeinotus sellatus	(Fabricius, 1794)
Taphropeltus contractus	(Herrich-Schäffer, 1835)
Taphropeltus hamulatus	Thomson, 1870
Taphrorychus bicolor	(Herbst, 1793)
Taphrocoetes hirtellus	(Eichhoff, 1878)
Taphrorychus mecedanus	Reitter, 1913
Taphrorychus villifrons	Dufour, 1843
Taphrotopium sulcifrons	(Herbst, 1797)
Tapinocyba affinis	(Lessert, 1907)
Tapinocyba insecta	(L. Koch, 1869)
Tapinocyboides pygmaeus	(Menge, 1869)
Tapinoma ambiguum	Emery, 1925
Tapinoma erraticum	(Latreille, 1798)
Tapinopa longidens	(Wider, 1834
Taranucnus setosus	(O.P.-Cambridge, 1863)
Targionia vitis	(Signoret,1876)
Tarsostenus univittatus	(Rossi, 1792)
Eumodicogryllus bordigalensis	(Latreille, 1804)
Tasgius ater	(Gravenhorst, 1802)
Tasgius melanarius	(Heer, 1839)
Tasgius morsitans	(Rossi, 1790)
Tasgius pedator	(Gravenhorst, 1802)
Tasgius winkleri	(Bernhauer, 1906)
Tatianaerhynchites aequatus	(Linnaeus, 1767)
Taxicera deplanata	(Gravenhorst, 1802)
Taxicera sericophila	(Baudi di Selve, 1869)
Tebenna bjerkandrella	(Thunberg, 1784)
Tebenna micalis	(Mann, 1857)
Tecmerium perplexum	(Gozmány, 1957)
Tegenaria agrestis	(Walckenaer, 1802)
Malthonica campestris	(C.L. Koch, 1834)
Tegenaria domestica	Clerck, 1757
Malthonica ferruginea	(Panzer, 1804)
Malthonica nemorosa	(Simon, 1916)
Malthonica silvestris	(L. Koch, 1872)
Telechrysis tripuncta	(Haworth, 1828)
Teleiodes flavimaculella	(Hübner, 1796)
Teleiodes luculella	Herrich-Scheffer, 1854
Teleiodes saltuum	(Hübner, 1813)
Teleiodes sequax	(Zeller, 1878)
Teleiodes vulgella	(Haworth, 1828)
Teleiodes wagae	(Nowicki, 1860)
Teleiopsis diffinis	(Haworth, 1828)
Telmatophilus brevicollis	Aubé, 1862
Telmatophilus caricis	(Olivier, 1790)
Telmatophilus schoenherri	(Gyllenhal, 1808)
Telmatophilus sparganii	(Ahrens, 1812)
Telmatophilus typhae	(Fallén, 1802)
Teloleuca pellucens	(Fabricius, 1779)
Telostegus inermis	(Brullé, 1832)
Temnocerus longiceps	(Thomson, 1888)
Temnocerus nanus	(Paykull, 1792)
Temnocerus tomentosus	(Gyllenhal, 1839)
Temnochila coerulea	(Olivier, 1790)
Temnostethus dacicus	Puton, 1888
Temnostethus gracilis	Horváth, 1907
Temnostethus longirostris	(Horváth, 1908)
Temnostethus pusillus	(Herrich-Schäffer, 1835)
Temnostethus reduvinus	(Herrich-Schäffer, 1835)
Temnostoma apiforme	(Fabricius, 1794)
Temnostoma bombylans	(Fabricius, 1805)
Temnostoma meridionale	Krivosheina & Mamev, 1962
Temnostoma vespiforme	(Linnaeus, 1758)
Tenebrio molitor	Linnaeus, 1758
Tenebrio obscurus	Fabricius, 1792
Tenebrio opacus	Duftschmid, 1812
Tenebroides fuscus	(Goeze, 1777)
Tenebroides mauritanicus	(Linnaeus, 1758)
Tenuiphantes alacris	(Blackwall, 1853)
Tenuiphantes cristatus	(Menge, 1866)
Tenuiphantes flavipes	(Blackwall, 1854)
Tenuiphantes mengei	(Kulczynski, 1887)
Tenuiphantes tenebricola	(Wider, 1834)
Tenuiphantes tenuis	(Blackwall, 1852)
Tenuiphantes zimmermanni	(Bertkau, 1890)
Isturgia arenacearia	(Denis & Schiffermüller, 1775)
Isturgia murinaria	(Denis & Schiffermüller, 1775)
Tephronia sepiaria	(Hufnagel, 1767)
Teplinus velatus	(Mulsant et Rey, 1861)
Teratocoris antennatus	(Boheman, 1852)
Involvulus aethiops	(Bach, 1854)
Haplorhynchites caeruleus	(De Geer, 1775)
Teretrius fabricii	Mazur, 1972
Haplochrois albanica	(Rebel & Zerny, 1932)
Haplochrois ochraceella	(Rebel, 1903)
Tetartopeus quadratus	(Paykull, 1789)
Tetartopeus rufonitidus	(Reitter, 1909)
Tetartopeus scutellaris	(Nordmann, 1837)
Tetartopeus terminatus	(Gravenhorst, 1802)
Tetartostylus pellucidus	Wagner, 1951
Tethea ocularis	(Linnaeus, 1767)
Tethea or	([Denis & Schiffermüller], 1775)
Tetheella fluctuosa	(Hübner, 1803)
Tetrabrachys connatus	(Panzer, 1796)
Tetragnatha dearmata	Thorell, 1873
Tetragnatha extensa pulchra	(Linnaeus, 1758)
Tetragnatha montana	Simon, 1874
Tetragnatha nigrita	Lendl, 1886
Tetragnatha obtusa	C.L. Koch, 1837
Tetragnatha pinicola	L. Koch, 1870
Tetragnatha reimoseri	(Rosca, 1939)
Tetragnatha shoshone	Levi, 1981
Tetragnatha striata	L. Koch, 1862
Tetralonia alternans	Brullé, 1832
Tetralonia alticincta	(Lepeletier, 1841)
Tetralonia armeniaca	Morawitz, 1878
Tetralonia dentata	(Klug, 1835)
Tetralonia fulvescens	Giraud, 1863
Tetralonia graja	(Eversmann, 1852)
Tetralonia hungarica	(Friesse, 1895)
Tetralonia lyncea	Mocsáry, 1879
Tetralonia macroglossa	(Illiger, 1806)
Tetralonia nana	Morawitz, 1873
Tetralonia pollinosa	(Lepeletier, 1841)
Tetralonia salicariae	(Lepeletier, 1841)
Tetralonia scabiosae	Mocsáry, 1879
Tetralonia tricincta	(Erichson, 1835)
Tetramorium caespitum	(Linnaeus, 1758)
Tetramorium hungaricum	Röszler, 1935
Tetramorium impurum	Foerster, 1850
Tetramorium semilaeve	André, 1881
Tetrao tetrix	Linnaeus, 1758
Tetrao urogallus	Linnaeus, 1758
Tetrao urogallus major	Linnaeus, 1758
Tetraphleps bicuspis	(Herrich-Schäffer, 1835)
Tetratoma ancora	Fabricius, 1790
Tetratoma desmarestii	Latreille, 1807
Tetratoma fungorum	Fabricius, 1790
Tetrax tetrax	(Linnaeus, 1758)
Tetrix bipunctata	(Linne, 1758)
Tetrix depressa	(Brisout, 1848)
Tetrix tenuicornis	(Sahlberg, 1893)
Tetrix subulata	(Linne, 1758)
Tetrix undulata	(Sowerby, 1806)
Tetropium castaneum	(Linnaeus, 1758)
Tetropium fuscum	(Fabricius, 1787)
Tetropium gabrieli	J. Weise, 1905
Tetrops praeustus	(Linnaeus, 1758)
Tetrops starkii	Chevrolat, 1859
Tettigometra atra	Hagenbach, 1825
Tettigometra atrata	Fieber, 1872
Tettigometra concolor	Fieber, 1865
Tettigometra depressa	Fieber, 1865
Tettigometra fusca	Fieber, 1865
Tettigometra impressopunctata	Dufour, 1846
Tettigometra obliqua	(Panzer, 1799)
Tettigometra sordida	Fieber, 1865
Tettigometra sulphurea	Mulsant et Rey, 1855
Tettigometra virescens	(Panzer, 1799)
Tettigonia cantans	(Fuessly, 1775)
Tettigonia caudata	(Charpentier, 1845)
Tettigonia viridissima	Linne, 1758
Teuchestes fossor	(Linnaeus, 1758)
Textrix denticulata	(Olivier, 1789)
Thalassophilus longicornis	(Sturm, 1825)
Thalera fimbrialis	(Scopoli, 1763)
Thalpophila matura	(Hufnagel, 1766)
Thalycra fervida	(Olivier, 1790)
Thambus frivaldskyi	Bonvouloir, 1871
Thamiaraea cinnamomea	(Gravenhorst, 1802)
Thamiaraea hospita	(Märkel, 1844)
Thamiocolus imperialis	Schultze, 1895
Thamiocolus kraatzi	(Ch. Brisout, 1869)
Thamiocolus nubeculosus	(Gyllenhal, 1837)
Thamiocolus pubicollis	(Gyllenhal, 1837)
Thamiocolus signatus	(Gyllenhal, 1837)
Thamiocolus sinapis	(Desbrochers, 1893)
Thamiocolus viduatus	(Gyllenhal, 1837)
Thamiocolus virgatus	(Gyllenhal, 1837)
Thamnotettix confinis	Zetterstedt, 1828
Thamnotettix dilutior	(Kirschbaum, 1868)
Thamnotettix exemptus	Melichar, 1896
Thamnotettix liberatus	Matsumura, 1908
Thamnurgus kaltenbachii	(Bach, 1849)
Thamnurgus varipes	Eichhoff, 1878
Thanasimus formicarius	(Linnaeus, 1758)
Thanasimus rufipes	(Brahm, 1797)
Thanatophilus dispar	(Herbst, 1793)
Thanatophilus rugosus	(Linnaeus, 1758)
Thanatophilus sinuatus	(Fabricius, 1775)
Thanatus arenarius	Thorell, 1872
Thanatus atratus	Simon, 1875
Thanatus formicinus	(Clerck, 1757)
Thanatus pictus	L. Koch, 1881
Thanatus sabulosus	(Menge, 1875)
Thanatus striatus	C.L. Koch, 1845
Thanatus vulgaris	Simon, 1870
Thaumetopoea processionea	(Linnaeus, 1758)
Theba pisana	(O.F. Müller, 1774)
Thecla betulae	(Linnaeus, 1758)
Thecturota marchii	(Dodero, 1922)
Theodoxus danubialis	(C. Pfeiffer, 1828)
Theodoxus danubialis stragulatus	(C. Pfeiffer, 1828)
Theodoxus fluviatilis	(Linnaeus, 1758)
Theodoxus prevostianus	(C. Pfeiffer, 1828)
Theodoxus transversalis	(C. Pfeiffer, 1828)
Theonina cornix	(Simon, 1881)
Theophilea subcylindricollis	Hladil, 1988
Thera britannica	(Turner, 1925)
Thera cognata	(Thunberg, 1792)
Thera firmata	(Hübner, 1822)
Thera juniperata	(Linnaeus, 1758)
Thera obeliscata	(Hübner, 1787)
Thera variata	(Denis & Schiffermüller, 1775)
Thera vetustata	(Denis & Schiffermüller, 1775)
Therapis flavicaria	(Denis & Schiffermüller, 1775)
Theresimima ampellophaga	(Bayle-Barelle, 1808)
Theria rupicapraria	(Denis & Schiffermüller, 1775)
Sardinidion blackwalli	(O. P.-Cambridge, 1871)
Theridion familiare	O.P.-Cambridge, 1871
Theridion hemerobius	Simon, 1914
Phylloneta impressum	L. Koch, 1881
Theridion melanurum	Hahn, 1831
Theridion mystaceum	L. Koch, 1870
Heterotheridion nigrovariegatum	Simon, 1873
Theridion pictum	(Walckenaer, 1802)
Theridion pinastri	L. Koch, 1872
Phylloneta sisyphia	(Clerck, 1757)
Theridion varians	Hahn, 1833
Theridiosoma gemmosum	(L. Koch, 1877)
Therioplectes gigas	(Herbst, 1787)
Thermocyclops crassus	(Fischer, 1853)
Thermocyclops dybowskii	(Lande, 1890)
Thermocyclops oithonoides	(Sars, 1863)
Theromyzon tessulatum	(O.F. Müller, 1774)
Thes bergrothi	(Reitter, 1880)
Thiasophila angulata	(Erichson, 1837)
Thinobius brevipennis	Kiesenwetter, 1850
Thinobius ciliatus	Kiesenwetter, 1844
Thinobius rambouseki	Smetana, 1959
Thinodromus arcuatus	(Stephens, 1834)
Thinodromus dilatatus	(Erichson, 1839)
Thinodromus hirticollis	Mulsant et Rey, 1878
Thinonoma atra	(Gravenhorst, 1806)
Thiodia citrana	(Hübner, 1799)
Thiodia lerneana	Treitschke, 1835
Thiodia torridana	(Lederer, 1859)
Thiodia trochilana	(Frölich, 1828)
Thiotricha subocellea	(Stephens, 1834)
Thisanotia chrysonuchella	(Scopoli, 1763)
Tholera cespitis	([Denis & Schiffermüller], 1775)
Tholera decimalis	(Poda, 1761)
Thomisus onustus	Walckenaer, 1806
Thoracophorus corticinus	Motschulsky, 1837
Thryogenes atrirostris	Lohse, 1992
Thryogenes festucae	(Herbst, 1795)
Thryogenes nereis	(Paykull, 1800)
Thryogenes scirrhosus	(Gyllenhal, 1836)
Thumatha senex	(Hübner, 1808)
Thyatira batis	(Linnaeus, 1758)
Thymallus thymallus	(Linnaeus, 1758)
Thymalus limbatus	(Fabricius, 1787)
Thymelicus acteon	(Rottemburg, 1775)
Thymelicus lineola	(Ochsenheimer, 1808)
Thymelicus sylvestris	(Poda, 1761)
Thyreocoris fulvipennis	(Dallas, 1851)
Thyreocoris scarabaeoides	(Linnaeus, 1758)
Thyreosthenius biovatus	(O.P.-Cambridge, 1875)
Thyreosthenius parasiticus	(Westring, 1851)
Thyreus affinis	(Morawitz, 1873)
Thyreus histrionicus	(Illiger, 1806)
Thyreus orbatus	(Lepeletier, 1841)
Thyreus ramosus	(Lepeletier, 1841)
Thyreus truncatus	(Pérez, 1883)
Thyris fenestrella	(Scopoli, 1763)
Tibellus macellus	Simon, 1875
Tibellus maritimus	(Menge, 1875)
Tibellus oblongus	(Walckenaer, 1802)
Lyristes plebejus	(Scopoli, 1763)
Tibicina haematodes	(Scopoli, 1763)
Tichodroma muraria	(Linnaeus, 1766)
Tiliacea aurago	([Denis & Schiffermüller], 1775)
Tiliacea citrago	(Linnaeus, 1758)
Tiliacea sulphurago	([Denis & Schiffermüller], 1775)
Tilloidea unifasciata	(Fabricius, 1787)
Tillus elongatus	(Linnaeus, 1758)
Tillus pallidipennis	Bielz, 1850
Timandra comae	A. Schmidt, 1931
Tinagma balteolella	(Fischer v. Röslerstamm, 1841)
Tinagma ocnerostomella	(Stainton, 1850)
Tinagma perdicella	Zeller, 1839
Tinca tinca	(Linnaeus, 1758)
Tinea columbariella	Wocke, 1877
Tinea dubiella	Stainton, 1859
Tinea nonimella	(Zagulajev, 1955)
Tinea pallescentella	Stainton, 1851
Tinea pellionella	Linnaeus, 1758
Tinea semifulvella	Haworth, 1828
Tinea translucens	Meyrick, 1917
Tinea trinotella	Thunberg, 1794
Tineola bisselliella	(Hummel, 1823)
Tingis ampliata	(Herrich-Schäffer, 1839)
Tingis angustata	(Herrich-Schäffer, 1838)
Tingis auriculata	(Costa, 1843)
Tingis cardui	(Linnaeus, 1758)
Tingis caucasica	(Jakovlev, 1880)
Tingis crispata	(Herrich-Schäffer, 1838)
Tingis geniculata	(Fieber, 1844)
Tingis grisea	Germar, 1885
Tingis maculata	(Herrich-Schäffer, 1838)
Tingis marrubii	Vallot, 1829
Tingis pilosa	Hummel, 1825
Tingis ragusana	(Fieber, 1861)
Tingis reticulata	(Herrich-Schäffer, 1835)
Tinicephalus hortulanus	(Meyer-Dür, 1843)
Tinodes pallidulus	McLachlan, 1878
Tinodes rostocki	McLachlan, 1878
Tinodes unicolor	(Pictet, 1834)
Tinodes waeneri	(Linnaeus, 1758)
Tinotus morion	(Gravenhorst, 1802)
Tinthia brosiformis	(Hübner, 1813)
Tinthia tineiformis	(Esper, 1789)
Tiphia femorata	Fabricius, 1775
Tiphia fulvipennis	Smith, 1879
Tiphia lativentris	Tournier, 1889
Tiphia minuta	Vander Linden, 1827
Tiphia morio	Fabricius, 1787
Tiphia ruficornis	Klug, 1810
Tischeria decidua	Wocke, 1876
Tischeria dodonaea	Stainton, 1858
Tischeria ekebladella	(Bjerkander, 1795)
Tiso aestivus	(L. Koch, 1802)
Tiso vagans	(Blackwall, 1834)
Titanio normalis	(Hübner, 1796)
Titanoeca quadriguttata	(Hahn, 1833)
Titanoeca schineri	(L. Koch, 1872)
Titanoeca tristis	L. Koch, 1872
Titanoeca veteranica	Herman, 1879
Tmarus piger	(Walckenaer, 1802)
Tmarus stellio	Simon, 1875
Tolypotheca emese	Ádám, 1992
Tomicus minor	(Hartig, 1834)
Tomicus piniperda	(Linnaeus, 1758)
Tomoglossa luteicornis	(Erichson, 1837)
Tomoxia bucephala	(Costa, 1854)
Tonnacypris lutaria	(Koch, 1838)
Torleya major	(Klapálek, 1905)
Tortricodes alternella	(Denis & Schiffermüller, 1775)
Tortrix viridana	Linnaeus, 1758
Tournotaris bimaculata	(Fabricius, 1787)
Tournotaris granulipennis	(Tournier, 1874)
Toya propinqua	(Fieber, 1866)
Trachea atriplicis	(Linnaeus, 1758)
Trachelas maculatus	Thorell, 1875
Tracheliastes chondrostomi	Hanek, 1969
Tracheliastes maculatus	Kollar, 1836
Tracheliastes polycolpus	Nordmann, 1832
Tracheliodes curvitarsis	(Herrich-Schaeffer, 1841)
Trachodes hispidus	(Linnaeus, 1758)
Trachonitis cristella	(Denis & Schiffermüller, 1775)
Trachycera advenella	(Zincken, 1818)
Trachycera dulcella	(Zeller, 1848)
Trachycera legatea	(Haworth, 1811)
Trachycera marmorea	(Haworth, 1811)
Trachycera suavella	(Zincken, 1818)
Trachyphloeus alternans	Gyllenhal, 1834
Trachyphloeus angustisetulus	Hansen, 1915
Trachyphloeus aristatus	(Gyllenhal, 1827)
Trachyphloeus asperatus	Boheman, 1843
Trachyphloeus bifoveolatus	(Beck, 1817)
Trachyphloeus frivaldszkyi	Kuthy, 1887
Trachyphloeus inermis	Boheman, 1843
Trachyphloeus parallelus	Seidlitz, 1868
Trachyphloeus scabriculus	(Linnaeus, 1771)
Trachyphloeus spinimanus	Germar, 1824
Trachyphloeus ventricosus	Germar, 1824
Trachypteris picta	(Pallas, 1773)
Trachypteris picta decostigma	(Fabricius, 1787)
Trachys coruscus	Ponza, 1805
Trachys fragariae	Brisout de Barneville, 1874
Trachys minutus	(Linnaeus, 1874)
Trachys problematicus	Obenberger, 1918
Trachys puncticollis	Abeille de Perrin, 1897
Trachys puncticollis rectilineatus	Abeille de Perrin, 1897
Trachys scrobiculatus	Kiesenwetter, 1857
Trachys troglodytes	Schönherr, 1817
Trachysphaera costata	(Waga, 1858)
Trachysphaera gibbula	(Latzel, 1884)
Trachysphaera noduligera	(Verhoeff, 1906)
Trachysphaera noduligera hungarica	(Verhoeff, 1906)
Trachyzelotes pedestris	(C.L. Koch, 1837)
Tragosoma depsarium	(Linnaeus, 1767)
Trajancypris clavata	(Baird, 1838)
Trajancypris laevis	(G.W. Müller, 1900)
Trajancypris serrata	(G.W. Müller, 1900)
Trapezonotus arenarius	(Linnaeus, 1758)
Trapezonotus dispar	Stal, 1872
Trapezonotus ullrichi	(Fieber, 1836)
Traumoecia picipes	(Thomson, 1856)
Trechoblemus micros	(Herbst, 1784)
Trechus austriacus	Dejean, 1831
Trechus obtusus	Erichson, 1837
Trechus pilisensis	Csiki, 1917
Trechus quadristriatus	(Schrank, 1781)
Trematocephalus cristatus	(Wider, 1834)
Tretocephala ambigua	(Lilljeborg, 1900)
Triaenodes bicolor	(Curtis, 1834)
Triarthron maerkeli	Maerkel, 1840
Triaxomasia caprimulgella	(Stainton, 1851)
Triaxomera fulvimitrella	(Sodoffsky, 1830)
Triaxomera parasitella	(Hübner, 1796)
Tribolium castaneum	(Herbst,  1797)
Tribolium confusum	Jacquelin Du Val, 1868
Tribolium destructor	Uyttenboogaart, 1933
Tribolium madens	(Charpentier, 1825)
Trichia erjaveci	(Brusina, 1870)
Trichia hispida	(Linnaeus, 1758)
Trichia lubomirskii	(Ślósarski, 1881)
Trichia striolata	(C. Pfeiffer, 1825)
Trichia striolata danubialis	(C. Pfeiffer, 1825)
Trichiura crataegi	(Linnaeus, 1758)
Trichius fasciatus	(Linnaeus, 1758)
Trichius sexualis	Bedel, 1906
Trichiusa immigrata	Lohse, 1984
Trichoceble floralis	(Olivier, 1790)
Dicheirotrichus placidus	(Gyllenhal, 1827)
Trichodes apiarius	(Linnaeus, 1758)
Trichodes favarius	Illiger, 1802
Trichoferus holosericeus	(Rossi, 1790)
Trichoferus pallidus	(Olivier, 1790)
Tricholeiochiton fagesii	(Guinard, 1879)
Trichoncoides piscator	(Simon, 1884)
Trichoncus affinis	Kulczynski, 1894
Trichoncus auritus	(C. L. Koch, 1869)
Trichoncus scrofa	Simon, 1884
Trichonotulus scrofa	(Fabricius, 1787)
Trichonyx sulcicollis	(Reichenbach, 1816)
Trichophaga tapetzella	(Linnaeus, 1758)
Trichophya pilicornis	(Gyllenhal, 1810)
Trichoplusia ni	(Hübner, 1803)
Trichopsomyia carbonaria	(Meigen, 1822)
Trichopsomyia flavitarsis	(Meigen, 1822)
Trichopterapion holosericeum	(Gyllenhal, 1833)
Trichopterna cito	(O.P.-Cambridge, 1872)
Trichopternoides thorelli	(Westring, 1861)
Trichopteryx carpinata	(Borkhausen, 1794)
Trichopteryx polycommata	(Denis & Schiffermüller, 1775)
Trichosirocalus barnevillei	(Grenier, 1867)
Trichosirocalus campanella	(Schultze, 1895)
Trichosirocalus horridus	(Panzer, 1801)
Trichosirocalus rufulus	(Dufour, 1851)
Trichosirocalus thalammeri	(Schultze, 1906)
Trichosirocalus troglodytes	(Fabricius, 1787)
Trichostegia minor	(Curtis, 1834)
Trichotichnus laevicollis	(Duftschmid, 1812)
Trichrysis cyanea	(Linnaeus, 1758)
Xya pfaendleri	(Harz, 1970)
Xya variegata	Latreille, 1809
Triepeolus tristis	Smith, 1854
Trifurcula beirnei	Puplesis, 1984
Trifurcula chamaecytisi	Z. & A. Lastuvka, 1994
Trifurcula cryptella	(Stainton, 1856)
Trifurcula eurema	(Tutt, 1899)
Trifurcula headleyella	(Stainton, 1854)
Trifurcula josefklimeschi	Nieukerken, 1990
Trifurcula melanoptera	van Nieukerken & Puplesis, 1991
Trifurcula ortneri	(Klimesch, 1951)
Trifurcula pallidella	(Duponchel, 1843)
Trifurcula thymi	(Szöcs, 1965)
Triglyphus primus	Loew, 1840
Trigonogenius globosus	Solier, 1849
Trigonotylus caelestialium	(Kirkaldy, 1902)
Trigonotylus pulchellus	(Hahn, 1834)
Trigonotylus ruficornis	(Geoffroy, 1785)
Trimium brevicorne	(Reichenbach, 1816)
Trimium carpathicum	Saulcy, 1875
Tringa erythropus	(Pallas, 1764)
Tringa flavipes	(Gmelin, 1789)
Tringa glareola	Linnaeus, 1758
Tringa nebularia	(Gunnerus, 1767)
Tringa ochropus	Linnaeus, 1758
Tringa stagnatilis	(Bechstein, 1803)
Tringa totanus	(Linnaeus, 1758)
Trinodes hirtus	(Fabricius, 1781)
Triodia amasina	(Herrich-Schäffer, 1852)
Triodia sylvina	(Linnaeus, 1761)
Trionymus aberrans	Goux,1938
Trionymus elymi	(Borchsenius,1949)
Trionymus hamberdi	(Borchsenius,1949)
Trionymus isfarensis	(Borchsenius,1949)
Trionymus luzensis	Komosinska,1980
Trionymus multivorus	(Kiritchenko,1935)
Trionymus newsteadi	(Green,1917)
Trionymus perrisii	(Signoret,1875)
Trionymus phalaridis	Green,1925
Trionymus phragmitis	(Hall,1923)
Trionymus placatus	(Borchsenius,1949)
Trionymus radicum	(Newstead,1895)
Trionymus singularis	Schmutterer,1952
Trionymus thulensis	Green,1931
Trionymus tomlini	Green,1925
Triops cancriformis	(Bosc, 1801)
Triphosa dubitata	(Linnaeus, 1758)
Triphyllus bicolor	(Fabricius, 1792)
Triplax aenea	(Schaller, 1783)
Triplax collaris	(Schaller, 1783)
Triplax elongata	Lacordaire, 1842
Triplax lacordairii	Crotch, 1870
Triplax lepida	Faldermann, 1835
Triplax melanocephala	(Latreille, 1804)
Triplax pygmaea	Kraatz, 1871
Triplax rufipes	(Fabricius, 1775)
Triplax russica	(Linnaeus, 1758)
Triplax scutellaris	Charpentier, 1825
Trisateles emortualis	([Denis & Schiffermüller], 1775)
Trissemus antennatus	(Aubé, 1833)
Trissemus antennatus serricornis	(Aubé, 1833)
Trissemus impressus	(Panzer, 1805)
Tritoma bipustulata	Fabricius, 1775
Tritomegas bicolor	(Linnaeus, 1758)
Tritomegas sexmaculatus	(Rambur, 1842)
Triturus alpestris	(Laurenti, 1768)
Triturus carnifex	(Laurenti, 1768)
Triturus cristatus	(Laurenti, 1768)
Triturus dobrogicus	Kiritzescu, 1903
Triturus vulgaris	(Linnaeus, 1758)
Trixagus duvalii	(Bonvouloir, 1859)
Trixagus carinifrons	(Bonvouloir, 1859)
Trixagus dermestoides	(Linnaeus, 1767)
Trixagus elateroides	(Heer, 1841)
Trixagus exul	(Bonvouloir, 1859)
Trocheta bykowskii	Gedroyc, 1913
Trocheta cylindrica	Örley, 1886
Trocheta riparia	Nesemann, 1993
Trochosa robusta	(Simon, 1876)
Trochosa ruricola	(Dee Geer, 1778)
Trochosa spinipalpis	(O.P.-Cambridge, 1895)
Trochosa terricola	Thorell, 1856
Troglodytes troglodytes	(Linnaeus, 1758)
Troglops albicans	(Linnaeus, 1767)
Troglops cephalotes	(Olivier, 1790)
Troglops silo	Erichson, 1840
Trogoderma glabrum	(Herbst, 1783)
Trogoderma granarium	Evert, 1898
Trogoderma megatomoides	Reitter, 1881
Trogoderma versicolor	(Creutzer, 1799)
Trogoxylon impressum	(Comolli, 1837)
Troilus luridus	(Fabricius, 1775)
Tropideres albirostris	(Herbst, 1784)
Tropideres dorsalis	(Gyllenhal, 1813)
Tropidia fasciata	Meigen, 1822
Tropidia scita	(Harris, 1780)
Tropidocephala andropogonis	Horváth, 1895
Tropidodynerus interruptus	(Brullé, 1832)
Tropidophlebia costalis	(Herrich-Schäffer,1850)
Tropidothorax leucopterus	(Goeze, 1778)
Tropidotilla littoralis	(Petagna, 1787)
Tropiphorus cucullatus	Fauvel, 1888
Tropiphorus elevatus	(Herbst, 1795)
Tropiphorus micans	Boheman, 1842
Tropistethus holosericus	(Scholtz, 1846)
Tropocyclops prasinus	(Fischer, 1860)
Trox cadaverinus	Illiger, 1802
Trox eversmanni	Krynicky, 1832
Trox hispidus	(Pontoppidan, 1763)
Trox niger	Rossi, 1792
Trox perrisii	Fairmaire, 1868
Trox sabulosus	(Linnaeus, 1758)
Trox scaber	(Linnaeus, 1758)
Troxochrus scabriculus	(Westring, 1851)
Truncatellina callicratis	(Scacchi, 1833)
Truncatellina claustralis	(Gredler, 1856)
Truncatellina cylindrica	(A. Férussac, 1807)
Tryngites subruficollis	(Vieillot, 1819)
Trypetimorpha fenestrata	A. Costa, 1862
Trypocopris vernalis	(Linnaeus, 1758)
Trypodendron domesticum	(Linnaeus, 1758)
Trypodendron lineatum	(Olivier, 1795)
Trypodendron signatum	(Fabricius, 1787)
Trypophloeus asperatus	(Gyllenhal, 1813)
Trypophloeus granulatus	(Ratzeburg, 1837)
Trypophloeus rybinskii	(Reitter, 1895)
Trypoxylon attenuatum	Smith, 1851
Trypoxylon clavicerum	Lepeletier & Serville, 1828
Trypoxylon figulus	(Linnaeus, 1758)
Trypoxylon fronticorne	Gussakovskij, 1936
Trypoxylon kolazyi	Kohl, 1893
Trypoxylon scutatum	Chevrier, 1867
Tuponia elegans	(Jakovlev, 1867)
Tuponia hippophaes	(Fieber, 1861)
Tuponia mixticolor	(A. Costa, 1862)
Tuponia prasina	(Fieber, 1864)
Turdus iliacus	Linnaeus, 1766
Turdus merula	Linnaeus, 1758
Turdus naumanni	Temminck, 1820
Turdus philomelos	Ch. L. Brehm, 1831
Turdus pilaris	Linnaeus, 1758
Turdus torquatus	Linnaeus, 1758
Turdus torquatus alpestris	Linnaeus, 1758
Turdus viscivorus	Linnaeus, 1758
Turrutus socialis	(Flor, 1861)
Tychius aureolus	Kiesenwetter, 1851
Tychius breviusculus	Desbrochers, 1873
Tychius caldarai	Dieckmann, 1986
Tychius crassirostris	Kirsch, 1871
Tychius cuprifer	(Panzer, 1799)
Tychius flavus	Becker, 1864
Tychius junceus	(Reich, 1797)
Tychius kulzeri	Penecke, 1934
Tychius lineatulus	Stephens, 1831
Tychius medicaginis	Ch. Brisout, 1862
Tychius meliloti	Stephens, 1831
Tychius parallelus	(Panzer, 1794)
Tychius picirostris	(Fabricius, 1787)
Tychius polylineatus	(Germar, 1824)
Tychius pumilus	Ch. Brisout, 1862
Tychius pusillus	Germar, 1842
Aulacobaris quinquepunctatus	(Linnaeus, 1758)
Tychius rufipennis	Ch. Brisout, 1862
Tychius schneideri	(Herbst, 1795)
Tychius sharpi	Tournier, 1873
Tychius squamulatus	Gyllenhal, 1836
Tychius stephensi	Schönherr, 1836
Tychius striatulus	Gyllenhal, 1836
Tychius subsulcatus	Tournier, 1873
Tychius tibialis	Boheman, 1843
Tychius tridentinus	Penecke, 1922
Tychius trivialis	Boheman, 1843
Tychus dalmatinus	Reitter, 1880
Tychus niger	(Paykull, 1789)
Tychus rufus	Motschulsky, 1851
Typhaea stercorea	(Linnaeus, 1758)
Zonocyba bifasciata	(Boheman, 1851)
Typhlocyba quercus	(Fabricius, 1777)
Typhloiulus polypodus	(Loksa, 1960)
Typhochrestus digitatus	(O.P.-Cambridge, 1872)
Tyria jacobaeae	(Linnaeus, 1758)
Tyrus mucronatus	(Panzer, 1805)
Tyta luctuosa	([Denis & Schiffermüller], 1775)
Tyto alba	(Scopoli, 1769)
Tyto alba guttata	(Scopoli, 1769)
Tytthaspis sedecimpunctata	(Linnaeus, 1758)
Tytthus pygmaeus	(Zetterstedt, 1839)
Udea accolalis	(Zeller, 1867)
Udea ferrugalis	(Hübner, 1796)
Udea fulvalis	(Hübner, 1809)
Udea inquinatalis	(Lienig & Zeller, 1846)
Udea institalis	(Hübner, 1819)
Udea lutealis	(Hübner, 1809)
Udea nebulalis	(Hübner, 1796)
Udea olivalis	(Denis & Schiffermüller, 1775)
Udea prunalis	(Denis & Schiffermüller, 1775)
Uleiota planata	(Linnaeus, 1761)
Ulmicola spinipes	(Fallén, 1807)
Uloborus plumipes	Lucas, 1846
Uloborus walckenaerius	(Latreille, 1806)
Uloma culinaris	(Linnaeus, 1758)
Uloma rufa	(Piller et Mitterpacher, 1783)
Ulopa reticulata	(Fabricius, 1794)
Ulorhinus bilineatus	(Germar, 1818)
Umbra krameri	Walbaum, 1792
Unaspis euonymi	(Comstock,1881)
Unciger foetidus	(C.L.Koch, 1838)
Unciger transsilvanicus	Verhoeff, 1900
Unio crassus	Philipsson, 1788
Unio pictorum	(Linnaeus, 1758)
Unio tumidus	Philipsson, 1788
Upupa epops	Linnaeus, 1758
Uranotaenia unguiculata	Edwards, 1913
Uresiphita gilvata	(Fabricius, 1794)
Urocoras longispinus	(Kulczynski, 1897)
Urophorus rubripennis	(Heer, 1841)
Ursus arctos	Linnaeus, 1758
Urticicola umbrosus	(C. Pfeiffer, 1828)
Utecha lugens	(Germar, 1821)
Utecha trivia	(Germar, 1821)
Utetheisa pulchella	(Linnaeus, 1758)
Vadonia steveni	(Sperk, 1835)
Vadonia unipunctata	(Fabricius, 1787)
Valeria oleagina	([Denis & Schiffermüller], 1775)
Valgus hemipterus	(Linnaeus, 1758)
Vallonia costata	(O.F. Müller, 1774)
Vallonia enniensis	(Gredler, 1856)
Vallonia pulchella	(O.F. Müller, 1774)
Valvata cristata	O.F. Müller, 1774
Valvata macrostoma	Mörch, 1864
Valvata piscinalis	(O.F. Müller, 1774)
Vanellus vanellus	(Linnaeus, 1758)
Vanessa atalanta	(Linnaeus, 1758)
Vanessa cardui	(Linnaeus, 1758)
Variimorda basalis	(Costa, 1854)
Variimorda briantea	(Comoli, 1837)
Variimorda villosa	(Schrank, 1781)
Variimorda mendax	(Méquignon, 1946)
Velia affinis	Kolenati, 1857
Velia affinis filippii	Kolenati, 1857
Velia caprai	Tamanini, 1947
Velia saulii	Tamanini, 1947
Velleius dilatatus	(Fabricius, 1787)
Ventocoris trigonus	(Krynicki, 1871)
Venustoraphidia nigricollis	(Albarda, 1891)
Verdanus abdominalis	(Fabricius, 1803)
Diplocolenus nigrifrons	(Kirschbaum, 1868)
Diplocolenus parcanicus	Dlabola, 1948
Vertigo alpestris	Alder, 1838
Vertigo angustior	Jeffreys, 1830
Vertigo antivertigo	(Draparnaud, 1801)
Vertigo moulinsiana	(Dupuy, 1849)
Vertigo pusilla	O.F. Müller, 1774
Vertigo pygmaea	(Draparnaud, 1801)
Vertigo substriata	(Jeffreys, 1833)
Vespa crabro	Linnaeus, 1758
Vespertilio murinus	Linnaeus, 1758
Vespula austriaca	(Panzer, 1799)
Vespula germanica	(Fabricius, 1793)
Vespula rufa	(Linnaeus, 1758)
Vespula vulgaris	(Linnaeus, 1758)
Vestia gulo	(E.A. Bielz, 1859)
Vestia turgida	(Rossmässler, 1836)
Vibidia duodecimguttata	(Poda, 1761)
Viguierella coeca	(Maupas, 1892)
Viguierella paludosa	(Mrazek, 1894)
Vilpianus galii	(Wolff, 1802)
Vimba vimba	(Linnaeus, 1758)
Vincenzellus ruficollis	(Panzer, 1794)
Vipera berus	(Linnaeus, 1758)
Vipera ursinii	(Bonaparte, 1835)
Vipera ursinii rakosiensis	(Bonaparte, 1835)
Vitrea contracta	(Westerlund, 1871)
Vitrea crystallina	(O.F. Müller, 1774)
Vitrea diaphana	(S. Studer, 1820)
Vitrea subrimata	(Reinhardt, 1871)
Vitrea transsylvanica	(Clessin, 1877)
Vitrina pellucida	(O.F. Müller, 1774)
Vittacoccus longicornis	(Green,1916)
Vitula biviella	(Zeller, 1848)
Viviparus acerosus	(Bourguignat, 1862)
Viviparus contectus	(Millet, 1813)
Volinus sticticus	(Panzer, 1798)
Volucella bombylans	(Linnaeus, 1758)
Volucella inanis	(Linnaeus, 1758)
Volucella inflata	(Fabricius, 1794)
Volucella pellucens	(Linnaeus, 1758)
Volucella zonaria	(Poda, 1761)
Vulcaniella extremella	(Wocke, 1871)
Vulcaniella pomposella	(Zeller, 1839)
Vulpes vulpes	Linnaeus, 1758
Wagneriala sinuata	(Then, 1897)
Walckenaeria acuminata	Blackwall, 1833
Walckenaeria antica	(Wider, 1834)
Walckenaeria atrotibialis	(O.P.-Cambridge, 1878)
Walckenaeria capito	(Westring, 1861)
Walckenaeria corniculans	(O.P.-Cambridge, 1875)
Walckenaeria cucullata	C.L. Koch, 1836)
Walckenaeria cuspidata obsoleta	Blackwall, 1833
Walckenaeria dysderoides	(Wider, 1834)
Walckenaeria furcillata	(Menge, 1869)
Walckenaeria incisa	(O.P.-Cambridge, 1871)
Walckenaeria kochi	(O.P.-Cambridge, 1872)
Walckenaeria mitrata	(Menge, 1868)
Walckenaeria nodosa	O.P.-Cambridge, 1873
Walckenaeria nudipalpis	(Westring, 1851)
Walckenaeria obtusa	Blackwall, 1836
Walckenaeria simplex	Chyzer, 1894
Walckenaeria unicornis	O.P.-Cambridge, 1861
Walckenaeria vigilax	(Blackwall, 1853)
Watsonalla binaria	(Hufnagel, 1767)
Watsonalla cultraria	(Fabricius, 1775)
Watsonarctia casta	(Esper, 1785)
Wesmaelius concinnus	(Stephens, 1836)
Wesmaelius helveticus	(H. Aspöck & U. Aspöck, 1964)
Wesmaelius malladai	(Navás, 1925)
Wesmaelius mortoni	(McLachlan, 1899)
Wesmaelius nervosus	(Fabricius, 1793)
Wesmaelius quadrifasciatus	(Reuter, 1894)
Wesmaelius ravus	(Withycombe, 1923)
Wesmaelius subnebulosus	(Stephens, 1836)
Wesmaelius tjederi	(Kimmins, 1963)
Wheeleria obsoletus	(Zeller, 1841)
Whittleia paveli	Uhrik,1846
Whittleia undulella	(Fischer v. Röslerstamm, 1837)
Witlesia pallida	(Curtis, 1827)
Wlassicsia pannonica	Daday, 1903
Wockia asperipunctella	(Bruand, 1851)
Wormaldia occipitalis	(Pictet, 1834)
Xanthandrus comtus	(Harris, 1780)
Xanthia gilvago	([Denis & Schiffermüller], 1775)
Xanthia icteritia	(Hufnagel, 1766)
Xanthia ocellaris	(Borkhausen, 1792)
Xanthia togata	(Esper, 1788)
Xanthochilus quadratus	(Fabricius, 1798)
Xanthocrambus lucellus	(Herrich-Schäffer, 1848)
Xanthocrambus saxonellus	(Zincken, 1821)
Xanthodelphax flaveolus	(Flor, 1861)
Xanthodelphax stramineus	(Stal, 1858)
Xanthogramma dives	(Rondani, 1857)
Xanthogramma festivum	(Linnaeus, 1758)
Xanthogramma laetum	(Fabricius, 1794)
Xanthogramma pedissequum	(Harris, 1776)
Xantholinus azuganus	Reitter, 1908
Xantholinus coiffaiti	Franz, 1966
Xantholinus decorus	Erichson, 1839
Xantholinus dvoraki	Coiffait, 1956
Xantholinus elegans	(Olivier, 1795)
Xantholinus kaszabi	Bordoni, 1972
Xantholinus laevigatus	Jacobsen, 1849
Xantholinus linearis	(Olivier, 1795)
Xantholinus longiventris	Heer, 1839
Xantholinus tricolor	(Fabricius, 1787)
Xanthoperla apicalis	(Newmann, 1836)
Xanthorhoe biriviata	(Borkhausen, 1794)
Xanthorhoe designata	(Hufnagel, 1767)
Xanthorhoe ferrugata	(Clerck, 1759)
Xanthorhoe fluctuata	(Linnaeus, 1758)
Xanthorhoe montanata	(Denis & Schiffermüller, 1775)
Xanthorhoe quadrifasiata	(Clerck, 1759)
Xanthorhoe spadicearia	(Denis & Schiffermüller, 1775)
Xanthosphaera barnevillii	Fairmaire, 1859
Xanthostigma xanthostigma	(Schummel, 1832)
Xenostrongylus arcuatus	Kiesenwetter, 1859
Xenota myrmecobia	(Kraatz, 1856)
Xenus cinereus	(Güldenstädt, 1775)
Xerasia meschniggi	(Reitter, 1905)
Xerochlorita dumosa	(Ribaut, 1933)
Xerochlorita mendax	(Ribaut, 1933)
Xerochlorita prasina	(Fieber, 1884)
Xerocnephasia rigana	(Sodoffsky, 1829)
Xerolenta obvia	(Menke, 1828)
Xerolycosa miniata	(C.L. Koch, 1834)
Xerolycosa nemoralis	(Westring, 1861)
Xestia ashworthii	(Doubleday, 1855)
Xestia ashworthii candelarum	(Doubleday, 1855)
Xestia baja	([Denis & Schiffermüller], 1775)
Xestia castanea	(Esper, 1798)
Xestia c-nigrum	(Linnaeus, 1758)
Xestia ditrapezium	([Denis & Schiffermüller], 1775)
Xestia sexstrigata	(Haworth, 1809)
Xestia stigmatica	(Hübner, 1813)
Xestia triangulum	(Hufnagel, 1766)
Xestia xanthographa	([Denis & Schiffermüller], 1775)
Xestobium plumbeum	(Illiger, 1801)
Xestobium rufovillosum	(De Geer, 1774)
Xestoiulus imbecillus	(Latzel, 1884)
Xestoiulus laeticollis	(Porat, 1889)
Xestoiulus laeticollis dudichi	(Porat, 1889)
Xestoiulus laeticollis evae	(Porat, 1889)
Xiphophorus helleri	Heckel, 1848
Xyleborinus saxesenii	(Ratzeburg, 1837)
Xyleborus alni	Niijima, 1909
Xyleborus cryptographus	(Ratzeburg, 1837)
Xyleborus dispar	(Fabricius, 1792)
Xyleborus dryographus	(Ratzeburg, 1837)
Xyleborus eurygraphus	(Ratzeburg, 1837)
Xyleborus monographus	(Fabricius, 1792)
Xyleborus pfeilii	(Ratzeburg, 1837)
Xylena exsoleta	(Linnaeus, 1758)
Xylena vetusta	(Hübner, 1813)
Xyletinus ater	(Creutzer, 1796)
Xyletinus distinguendus	Kofler, 1970
Xyletinus fibyensis	Lundblad, 1949
Xyletinus laticollis	(Duftschmid, 1825)
Xyletinus longitarsis	Jansson, 1942
Xyletinus moraviensis	Gottwald, 1977
Xyletinus pectinatus	(Fabricius, 1792)
Xyletinus pseudooblongulus	Gottwald, 1977
Xyletinus subrotundatus	Lareynie, 1852
Xyletinus vaederoeensis	Lundberg, 1969
Xylita laevigata	(Hellen, 1786)
Xylita livida	Sahlberg, 1834
Xylocleptes bispinus	(Duftschmid, 1825)
Xylococcus filiferus	Löw,1882
Xylocopa iris	(Christ, 1791)
Xylocopa valga	Gerstaecker, 1873
Xylocopa violacea	(Linnaeus, 1758)
Xylocoridea brevipennis	Reuter, 1876
Xylocoris cursitans	(Fallén, 1807)
Xylocoris formicetorum	(Boheman, 1844)
Xylocoris galactinus	(Fieber, 1836)
Xylocoris obliquus	Costa, 1852
Xylodromus affinis	(Gerhardt, 1877)
Xylodromus concinnus	(Marsham, 1802)
Xylodromus depressus	(Gravenhorst, 1802)
Xylodromus testaceus	(Erichson, 1840)
Xylographus bostrichoides	(Dufour, 1843)
Xylopertha retusa	(Olivier, 1790)
Xylophilus testaceus	(Herbst, 1806)
Xylostiba bosnica	(Bernhauer, 1902)
Xylostiba monilicornis	(Gyllenhal, 1810)
Xylota abiens	Meigen, 1822
Xylota florum	(Fabricius, 1805)
Xylota ignava	(Panzer, 1798)
Xylota meigeniana	Stackelberg, 1964
Xylota segnis	(Linnaeus, 1758)
Xylota sylvarum	(Linnaeus, 1758)
Xylota tarda	Meigen, 1822
Xylota xanthocnema	Collin, 1939
Xylotrechus antilope	(Schönherr, 1817)
Xylotrechus arvicola	(Olivier, 1795)
Xylotrechus pantherinus	(Savenius, 1825)
Xylotrechus rusticus	(Linnaeus, 1758)
Xysticus acerbus	Thorell, 1872
Xysticus albomaculatus	(Kulczynski, 1891)
Xysticus audax	(Schrank, 1803)
Xysticus bifasciatus	C.L. Koch, 1837
Xysticus cristatus	(Clerck, 1857)
Xysticus embriki	Kolosváry, 1935
Xysticus erraticus	(Blackwall, 1834)
Xysticus gallicus	Simon, 1875
Xysticus graecus	C.L. Koch, 1837
Xysticus kempeleni	Thorell, 1872
Xysticus kochi	Thorell, 1872
Xysticus lanio	C.L. Koch, 1835
Xysticus lendli	Kulczynski, 1897
Xysticus lineatus	(Westring, 1851)
Xysticus luctator	L. Koch, 1870
Xysticus luctuosus	(Blackwall, 1836)
Xysticus marmoratus	(Thorell, 1875)
Xysticus ninnii	Thorell, 1872
Xysticus robustus	(Hahn, 1832)
Xysticus sabulosus	(Hahn, 1832)
Xysticus striatipes	L. Koch, 1870
Xysticus ulmi	(Hahn, 1831)
Xystophora carchariella	(Zeller, 1839)
Xystophora pulveratella	(Herrich-Schäffer, 1854)
Dichagyris forcipula	(Denis & Schiffermüller, 1775)
Dichagyris nigrescens	(Hofner, 1888)
Dichagyris signifera	(Denis & Schiffermüller, 1775)
Sitticus arenarius	Menge, 1868
Sitticus horvathi	Chyzer, 1891
Sitticus vittatus	Thorell, 1875
Ylodes kawraiskii	(Martynov, 1909)
Ylodes simulans	(Tjeder, 1929)
Yponomeuta cagnagella	(Hübner, 1813)
Yponomeuta evonymella	(Linnaeus, 1758)
Yponomeuta irrorella	(Hübner, 1796)
Yponomeuta malinellus	Zeller, 1838
Yponomeuta padella	(Linnaeus, 1758)
Yponomeuta plumbella	(Denis & Schiffermüller, 1775)
Yponomeuta rorrella	(Hübner, 1796)
Yponomeuta sedella	Treitschke, 1833
Ypsolopha alpella	(Denis & Schiffermüller, 1775)
Ypsolopha asperella	(Linnaeus, 1761)
Ypsolopha chazariella	(Mann, 1866)
Ypsolopha dentella	(Fabricius, 1775)
Ypsolopha falcella	(Denis & Schiffermüller, 1775)
Ypsolopha horridella	(Treitschke, 1835)
Ypsolopha lucella	(Fabricius, 1775)
Ypsolopha mucronella	(Scopoli, 1763)
Ypsolopha parenthesella	(Linnaeus, 1761)
Ypsolopha persicella	(Fabricius, 1787)
Ypsolopha scabrella	(Linnaeus, 1761)
Ypsolopha sequella	(Clerck, 1759)
Ypsolopha sylvella	(Linnaeus, 1767)
Ypsolopha ustella	(Clerck, 1759)
Ypsolopha vittella	(Linnaeus, 1758)
Zabrus spinipes	(Fabricius, 1798)
Zabrus tenebrioides	(Goeze, 1777)
Zacladus asperatus	(Gyllenhal, 1837)
Zacladus exiguus	(Olivier, 1807)
Zacladus geranii	(Paykull, 1800)
Zanclognatha lunalis	(Scopoli, 1763)
Zanclognatha tarsipennalis	Treitschke, 1835
Zanclognatha zelleralis	(Wocke, 1850)
Zeadolopus latipes	(Erichson, 1845)
Zebrina detrita	(O.F. Müller, 1774)
Zeiraphera griseana	(Hübner, 1799)
Zeiraphera isertana	(Fabricius, 1794)
Zeiraphera rufimitrana	(Herrich-Schäffer, 1851)
Zelotes aeneus	(Simon, 1878)
Zelotes apricorum	(L. Koch, 1876
Zelotes aurantiacus	Miller, 1967
Zelotes caucasius	(L. Koch, 1866)
Zelotes clivicola	(L. Koch, 1870)
Zelotes electus	(C.L. Koch, 1839)
Zelotes erebeus	(Thorell, 1870)
Zelotes gracilis	Canestrini, 1868
Zelotes hermani	(Chyzer, 1878)
Zelotes latreillei	(Simon, 1878)
Zelotes longipes	(L. Koch, 1866)
Zelotes mundus	(Kulczynski, 1897)
Zelotes petrensis	(C.L. Koch, 1839)
Zelotes pygmaeus	Miller, 1943
Zelotes segrex	(Simon, 1878)
Zelotes subterraneus	(C.L. Koch, 1833)
Zerynthia polyxena	([Denis & Schiffermüller], 1775)
Zeteotomus brevicornis	(Erichson, 1839)
Zeuzera pyrina	(Linnaeus, 1761)
Zicrona caerulea	(Linnaeus, 1758)
Zilla diodia	(Walckenaer, 1802)
Zilora obscura	(Fabricius, 1794)
Zingel streber	(Siebold, 1863)
Zingel zingel	(Linnaeus, 1766)
Zodarion germanicum	(C.L. Koch, 1837)
Zodarion rubidum	Simon, 1914
Zonitis immaculata	Olivier, 1790
Zonitis nana	Ragusa, 1882
Zonitis praeusta	Fabricius, 1792
Zonitoides arboreus	(Say, 1816)
Zonitoides nitidus	(O.F. Müller, 1774)
Zootoca vivipara	(Jacquin, 1787)
Zootoca vivipara pannonica	(Jacquin, 1787)
Zophodia grossulariella	(Hübner, 1809)
Zora manicata	Simon, 1878
Zora nemoralis	(Blackwall, 1861)
Zora pardalis	Simon, 1878
Zora silvestris	Kulczynski, 1897
Zora spinimana	(Sundevall, 1833)
Zorochros demustoides	(Herbst, 1806)
Zorochros meridionalis	(Laporte de Castelnau, 1840)
Zorochros quadriguttatus	(Laporte de Castelnau, 1840)
Zuphium olens	(Rossi, 1790)
Zygaena angelicae	Ochsenheimer, 1808
Zygaena brizae	(Esper, 1800)
Zygaena carniolica	(Scopoli, 1763)
Zygaena cynarae	(Esper, 1789)
Zygaena ephialtes	(Linnaeus, 1767)
Zygaena fausta	(Linnaeus, 1767)
Zygaena filipendulae	(Linnaeus, 1758)
Zygaena laeta	(Hübner, 1790)
Zygaena lonicerae	(Scheven, 1777)
Zygaena loti	(Denis & Schiffermüller, 1775)
Zygaena minos	(Denis & Schiffermüller, 1775)
Zygaena osterodensis	Reiss, 1921
Zygaena punctum	Ochsenheimer, 1808
Zygaena purpuralis	(Brünnich, 1763)
Zygaena viciae	(Denis & Schiffermüller, 1775)
Zygiella atrica	(C.L. Koch, 1845)
Zygiella x-notata	(Clerck, 1757)
Zygina angusta	Lethierry, 1874
Zygina discolor	Horváth, 1897
Zygina dorsalis	Horváth, 1897
Zygina flammigera	(Fourcroy, 1785)
Zygina frauenfeldi	Lethierry, 1880
Zygina hyperici	(Herrich-Schäffer, 1836)
Zygina lunaris	(Mulsant et Rey, 1855)
Zygina nivea	(Mulsant et Rey, 1855)
Zygina ordinaria	(Ribaut, 1936)
Zygina rhamni	Ferrari, 1882
Zygina rorida	(Mulsant et Rey, 1855)
Zygina rosea	(Flor, 1861)
Zygina suavis	Rey, 1891
Zygina tiliae	(Fallén, 1806)
Zygina tithide	Ferrari, 1882
Zyginidia pullula	(Boheman, 1845)
Zyginidia scutellaris	(Herrich-Schäffer, 1838)
Zyras cognatus	(Märkel, 1842)
Zyras collaris	(Paykull, 1800)
Zyras fulgidus	(Gravenhorst, 1806)
Zyras funestus	(Gravenhorst, 1806)
Zyras hampei	(Kraatz, 1862)
Zyras haworthi	(Stephens, 1832)
Zyras humeralis	(Gravenhorst, 1802)
Zyras laticollis	(Märkel, 1844)
Zyras limbatus	(Paykull, 1789)
Zyras lugens	(Gravenhorst, 1802)
Zyras ruficollis	(Grimm, 1845)
Zyras similis	(Märkel, 1844)
Trachemys scripta elegans	(Wied-Neuwied 1839)
Trachemys scripta	(Wied-Neuwied 1839)
Eresus moravicus	Tišnovsko, 2013
Aix galericulata	Linné, 1758
Rhysodes sulcatus	(Fabricius, 1787)
Omoglymmius germari	(Ganglbauer, 1891)
Bombina bombina × variegata	(Linnaeus, 1761)
Anguis colchica	(Berg, 1916)
Cydalima perspectalis	(Walker, 1859)
Knipowitschia caucasica	(Berg, 1916)
Carassius auratus	(Linnaeus, 1758)
Buprestis haemorrhoidalis	Herbst, 1780
Ctenophora guttata	Meigen 1818
Halyomorpha halys	(Stål, 1855)
Leptoglossus occidentalis	Heidemann, 1910
Bythinella thermophila	Glöer, Varga & Mrkvicka, 2015
Gasterosteus gymnurus	Cuvier, 1829
Rhyssa persuasoria	(Linnaeus, 1758)
Pisum elatius	Stev.
Clematis viticella	L.
Myosurus minimus	L.
Batrachium rhipiphyllum	(Bast.) Dum.
Batrachium trichophyllum	(Chaix) van den Bosch
Batrachium rionii	Lagger
Lathyrus pannonicus	(Jacq.) Garcke
Isopyrum thalictroides	L.
Actaea spicata	L.
Cimicifuga europaea	Schipczinskij
Aquilegia vulgaris	L.
Aquilegia vulgaris subsp. nigricans	L.
Consolida regalis	S.F. Gray
Consolida regalis subsp. paniculata	S.F. Gray
Paeonia officinalis subsp. banatica	L.
Acer saccharinum	L.
Cerastium tomentosum	L.
Phytolacca esculenta	Van Houtte
Lathyrus pisiformis	L.
Lathyrus pratensis	L.
Lathyrus aphaca	L.
Lathyrus nissolia	L.
Lathyrus tuberosus	L.
Clematis integrifolia	L.
Clematis alpina	(L.) Mill.
Clematis flammula	L.
Portulaca grandiflora	Hook.
Thymelaea passerina	(L.) Coss. et Germ.
Daphne laureola	L.
Daphne mezereum	L.
Daphne cneorum	L.
Paeonia officinalis	L.
Caltha palustris	L.
Caltha palustris subsp. laeta	L.
Lathyrus latifolius	L.
Lathyrus hirsutus	L.
Lathyrus sphaericus	Retz.
Lathyrus sativus	L.
Rosa rugosa	Thunb.
Padus serotina	(Ehrh.) Borkh.
Glycine soja	(L.) S. et Z.
Daphne cneorum subsp. arbusculoides	L.
Hippophaë rhamnoides subsp. carpatica	L.
Lathyrus sylvestris	L.
Pulsatilla pratensis subsp. zimmermannii	(L.) Mill.
Hepatica nobilis	Mill.
Anemone sylvestris	L.
Anemone nemorosa	L.
Anemone ranunculoides	L.
Anemone trifolia	L.
Clematis recta	L.
Clematis vitalba	L.
Caltha palustris subsp. cornuta	L.
Trollius europaeus	L.
Consolida orientalis	(J. Gay) Schrödinger
Aconitum anthora	L.
Aconitum variegatum subsp. gracilis	L.
Aconitum vulparia	Rchb.
Aconitum moldavicum	Hacq.
Lythrum salicaria	L.
Berberis vulgaris	L.
Mahonia aquifolium	(Pursh) Nutt.
Pisum sativum	L.
Batrachium baudotii	(Godr.) F. Schultz
Trollius europaeus subsp. demissorum	L.
Trollius europaeus subsp. tatrae	L.
Phaseolus coccineus	L.
Ammannia verticillata	(Ard.) Lam.
Peplis portula	L.
Lythrum thesioides	M.B.
Lythrum linifolium	Kar. et Kir.
Lythrum hyssopifolia	L.
Lythrum tribracteatum	Salzm.
Lythrum virgatum	L.
Helleborus dumetorum	W. et K.
Helleborus odorus	W. et K.
Helleborus purpurascens	W. et K.
Helleborus viridis	L.
Eranthis hyemalis	(L.) Salisb.
Nigella arvensis	L.
Nigella damascena	L.
Lathyrus pannonicus subsp. collinus	(Jacq.) Garcke
Lathyrus pallescens	(M.B.) C. Koch
Lathyrus palustris	L.
Phaseolus vulgaris	L.
Elaeagnus angustifolia	L.
Ceratocephalus testiculatus	(Cr.) Roth
Batrachium aquatile	(L.) Dum.
Cerasus vulgaris	(Dum.) Dost.
Batrachium radians	(Rével) Rével
Lathyrus niger	(L.) Bernh.
Lathyrus venetus	(Mill.) Wohlf.
Lathyrus vernus	(L.) Bernh.
Lathyrus linifolius var. montanus	(Reich.) Bassler
Pulsatilla patens	(L.) Mill.
Pulsatilla grandis	Wender.
Pulsatilla × mixta	Halácsy
Pulsatilla pratensis subsp. nigricans	(L.) Mill.
Pulsatilla pratensis subsp. nigricans lus. rubra	(L.) Mill.
Pulsatilla pratensis subsp. hungarica	(L.) Mill.
Batrachium circinatum	(Sibth.) Spach
Batrachium fluitans	(Lam.) Wimm.
Batrachium peltatum	(Bercht et Presl) Schrank
Rapistrum rugosum	(L.) All.
Ficaria verna	Huds.
Ficaria verna subsp. bulbifera	Huds.
Ficaria verna subsp. calthifolia	Huds.
Ranunculus polyphyllus	W. et K. ex Willd.
Ranunculus flammula	L.
Ranunculus lingua	L.
Ranunculus sceleratus	L.
Ranunculus repens	L.
Ranunculus bulbosus	L.
Ranunculus sardous	Cr.
Ranunculus polyanthemos	L.
Ranunculus lanuginosus	L.
Ranunculus nemorosus	DC.
Ranunculus acris	L.
Ranunculus strigulosus	Schur
Ranunculus auricomus	L.
Ranunculus cassubicus	L.
Ranunculus fallax	(Wimmer et Grab.) Kerner
Ranunculus arvensis	L.
Ranunculus lateriflorus	DC.
Ranunculus pedatus	W. et K.
Ranunculus illyricus	L.
Ranunculus psilostachys	Gris.
Ranunculus parviflorus	L.
Ranunculus cymbalaria	Pursh.
Thalictrum aquilegiifolium	L.
Thalictrum foetidum	L.
Thalictrum minus	L.
Thalictrum minus subsp. pseudominus	L.
Thalictrum simplex	L.
Sorbus × thaiszii	(Soó) Kárp.
Sorbus × pseudodanubialis	Kárp.
Sorbus × ulmifolia	Kárp.
Sorbus × vajdae	Bor.
Thalictrum simplex subsp. galioides	L.
Thalictrum flavum	L.
Thalictrum lucidum	L.
Adonis vernalis	L.
Adonis × hybrida	Wolf
Adonis flammea	Jacq.
Adonis aestivalis	L.
Nymphaea alba	L.
Nuphar lutea	(L.) Sm. ex Sibth. et Soiv.
Ceratophyllum submersum	L.
Ceratophyllum demersum	L.
Asarum europaeum	L.
Aristolochia clematitis	L.
Spiraea salicifolia	L.
Spiraea crenata	L.
Spiraea media	Fr. Schm.
Aruncus sylvestris	Kostel.
Cotoneaster tomentosus	(Ait.) Lindl.
Cotoneaster integerrimus	Medik.
Cotoneaster matrensis	Domokos em. Soó
Cotoneaster niger	Fr.
Cydonia oblonga	Mill.
Pyrus communis	L.
Pyrus pyraster	Burgsd.
Pyrus nivalis	Jacq.
Pyrus nivalis subsp. salviifolia	Jacq.
Pyrus × austriaca	Kern.
Pyrus magyarica	Terpó
Pyrus × mecsekensis	Terpó
Pyrus × karpatiana	Terpó
Malus sylvestris	(L.) Mill.
Malus sylvestris subsp. dasyphylla	(L.) Mill.
Malus domestica	Borkh.
Sorbus domestica	L.
Sorbus aucuparia	L.
Sorbus aria	(L.) Cr. s. str.
Sorbus aria agg.	(L.) Cr.
Sorbus graeca	(Spach) Lodd. ex Schauer
Sorbus austriaca subsp. hazslinszkyana	(Beck) Hedlund
Sorbus × danubialis	(Jáv.) Kárp.
Sorbus torminalis	(L.) Cr.
Sorbus × pseudosemiincisa	Boros
Sorbus × pseudobakonyensis	Kárp.
Sorbus × pseudolatifolia	Boros
Sorbus × simonkaiana	Kárp.
Sorbus × vertesensis	Boros
Sorbus × semiincisa	Borb.
Sorbus × pseudovertesensis	Boros
Sorbus × redliana	Kárp.
Sorbus × latissima	Kárp.
Sorbus × balatonica	Kárp.
Sorbus × barthae	Kárp.
Sorbus × borosiana	Kárp.
Sorbus × adamii	Kárp.
Sorbus × andreanszkyana	Kárp.
Sorbus × bakonyensis	(Jáv.) Kárp.
Sorbus × gayeriana	Kárp.
Rubus pyramidalis	Kaltenb.
Sorbus × gerecseensis	Boros et Kárp.
Sorbus × karpatii	Boros
Sorbus × decipientiformis	Kárp.
Sorbus × degenii	Jáv.
Sorbus × eugenii-kelleri	Kárp.
Amelanchier ovalis	Medik.
Mespilus germanica	L.
Crataegus oxyacantha	L.
Crataegus monogyna	Jacq.
Crataegus monogyna subsp. nordica	Jacq.
Crataegus curvisepala subsp. calycina	Lindm.
Crataegus nigra	W. et K.
Rubus saxatilis	L.
Rubus idaeus	L.
Rubus caesius	L.
Rubus canescens	DC.
Rubus nessensis	W. Hall.
Rubus fruticosus agg.	L.
Rubus sulcatus	Vest
Rubus plicatus	Wh. et N.
Rubus divaricatus	P.J. Müll.
Rubus rhombifolius	Weihe
Rubus silesiacus	Weihe
Rubus macrophyllus	Wh. et N.
Rubus sylvaticus	Wh. et N.
Rubus rhamnifolius	Wh. et N.
Rubus bifrons	Vest
Rubus discolor	Wh. et N.
Rubus candicans	Weihe
Rubus vestitus	Wh. et N.
Rubus radula	Weihe
Rubus thyrsiflorus	Wh. et N.
Rubus rudis	Wh. et N.
Rubus koehleri	Wh. et N.
Rubus scaber	Wh. et N.
Rubus schleicheri	Weihe
Rubus bellardii	Wh. et N.
Rubus serpens	Weihe et Court.
Rubus hirtus	W. et K.
Fragaria vesca	L.
Fragaria moschata	Duch.
Fragaria viridis	Duch.
Comarum palustre	L.
Potentilla rupestris	L.
Potentilla alba	L.
Potentilla micrantha	Ram.
Potentilla supina	L.
Potentilla anserina	L.
Potentilla erecta	(L.) Räuschel
Potentilla reptans	L.
Potentilla argentea	L.
Potentilla argentea subsp. grandiceps	L.
Potentilla argentea subsp. tenuiloba	L.
Potentilla argentea subsp. demissa	L.
Potentilla argentea subsp. decumbens	L.
Potentilla argentea subsp. macrotoma	L.
Potentilla impolita	Wahlb.
Potentilla collina	Wibel
Potentilla thyrsiflora	Hülsen
Pisum sativum subsp. arvense	L.
Potentilla wiemanniana var. javorkae	Günth. et Schumm.
Potentilla leucopolitana	P.J. Müll.
Potentilla inclinata	Vill.
Potentilla recta	L.
Potentilla recta subsp. tuberosa	L.
Potentilla recta subsp. laciniosa	L.
Potentilla recta subsp. pilosa	L.
Potentilla recta subsp. crassa	L.
Potentilla recta subsp. obscura	L.
Potentilla recta subsp. leucotricha	L.
Potentilla pedata	Willd.
Potentilla heptaphylla	L.
Potentilla patula	W. et K.
Potentilla arenaria	Borkh.
Potentilla neumanniana	Rchb.
Potentilla pusilla	Host
Waldsteinia geoides	Willd.
Geum urbanum	L.
Geum aleppicum	Jacq.
Geum rivale	L.
Filipendula ulmaria	(L.) Maxim.
Filipendula vulgaris	Mönch
Agrimonia eupatoria	L.
Agrimonia procera	Wallr.
Aremonia agrimonioides	(L.) DC.
Sanguisorba officinalis	L.
Sanguisorba minor	Scop.
Sanguisorba minor subsp. muricata	Scop.
Aphanes arvensis	L.
Aphanes microcarpa	(Boiss. et Reut.) Rothm.
Alchemilla glaucescens	Wallr.
Alchemilla hungarica	Soó
Alchemilla glabra	Neygenfind
Alchemilla xanthochlora	Rothm.
Alchemilla gracilis	Opiz
Alchemilla acutiloba	Opiz
Alchemilla crinita	Buser
Alchemilla subcrenata	Buser
Alchemilla monticola	Opiz
Alchemilla filicaulis	Buser
Rosa pendulina	L.
Rosa spinosissima	L.
Rosa gallica	L.
Rosa arvensis	Huds.
Rosa stylosa	Desv.
Rosa × reversa	W. et K.
Rosa livescens	Bess.
Rosa sancti-andreae	Deg. et Trtm.
Rosa tomentosa	Sm.
Rosa micrantha	Borrer
Rosa hungarica	Kern.
Rosa polyacantha	(Borb.) Deg.
Rosa rubiginosa	L.
Rosa agrestis	Savi
Rosa inodora	Fr.
Rosa albiflora	Opiz
Rosa gizellae	Borb.
Rosa elliptica	Tausch
Rosa zalana	Wiesb.
Rosa zagrabiensis	Vukot. et H. Br.
Rosa szaboi	(Borb.) Facsar
Rosa obtusifolia	Desv.
Rosa canina	L.
Rosa corymbifera	Borkh.
Rosa deseglisei	Bor.
Rosa dumalis	Bechst.
Rosa subcanina	(Christ) D.-T. et Sarnth.
Rosa caesia	Sm.
Rosa subcollina	(Christ) D.-T. et Sarnth.
Rosa glauca	Pourr.
Rosa sherardii	Davies
Rosa kmetiana	Borb.
Rosa scarbriuscula	Sm.
Padus avium	Mill.
Cerasus mahaleb	(L.) Mill.
Cerasus fruticosa	Pall.
Cerasus avium	(L.) Mönch
Cerasus × mohacsyana	(Kárp.) Janchen
Amygdalus communis	L.
Amygdalus nana	L.
Prunus cerasifera	Ehrh.
Prunus domestica	L.
Prunus domestica subsp. insititia	L.
Prunus spinosa	L.
Prunus spinosa subsp. fruticans	L.
Prunus spinosa subsp. dasyphylla	L.
Sedum caespitosum	(Cav.) DC.
Sedum spurium	M.B.
Sedum maximum	(L.) Hoffm.
Sedum hispanicum	L.
Sedum album	L.
Sorbus × zolyomii	(Soó) Kárp.
Sorbus × huljakii	Kárp.
Sedum reflexum	L.
Sedum reflexum subsp. glaucum	L.
Sedum acre	L.
Sedum neglectum subsp. sopianae	Ten.
Sedum sexangulare	L.
Sedum sartorianum subsp. hillebrandtii	Boiss.
Sempervivum tectorum	L.
Sempervivum marmoreum	Gris.
Jovibarba hirta	(L.) Opiz
Jovibarba sobolifera	(Sims) Opiz
Saxifraga paniculata	Mill.
Saxifraga bulbifera	L.
Saxifraga granulata	L.
Saxifraga tridactylites	L.
Saxifraga adscendens	L.
Chrysosplenium alternifolium	L.
Parnassia palustris	L.
Philadelphus coronarius	L.
Ribes uva-crispa	L.
Ribes uva-crispa subsp. reclinatum	L.
Ribes uva-crispa subsp. grossularia	L.
Ribes alpinum	L.
Ribes nigrum	L.
Ribes petraeum	Wulf.
Ribes rubrum	L.
Ribes rubrum subsp. rubrum	L.
Ribes rubrum subsp. sylvestre	L.
Ribes aureum	Pursh
Cercis siliquastrum	L.
Gleditsia triacanthos	L.
Lupinus polyphyllus	Lindl.
Lupinus albus	L.
Lupinus angustifolius	L.
Lupinus luteus	L.
Genista germanica	L.
Genista pilosa	L.
Genista tinctoria	L.
Genista tinctoria subsp. elatior	L.
Genista ovata subsp. nervata	W. et K.
Chamaespartium sagittale	(L.) P. Gibbs
Laburnum anagyroides	Medik.
Sarothamnus scoparius	(L.) Wimm.
Cytisus procumbens	(W. et K.) Spreng.
Lembotropis nigricans	(L.) Gris.
Chamaecytisus supinus	(L.) Link
Chamaecytisus supinus subsp. aggregatus	(L.) Link
Chamaecytisus supinus subsp. pseudorochelii	(L.) Link
Salvia verbenacea	L.
Chamaecytisus supinus subsp. pannonicus	(L.) Link
Chamaecytisus albus	(Hacq.) Rothm.
Chamaecytisus austriacus	(L.) Link
Chamaecytisus heuffelii	(Wierzb.) Rothm.
Chamaecytisus ratisbonensis	(Schaeffer) Rothm.
Chamaecytisus ciliatus	(Wahlb.) Rothm.
Chamaecytisus hirsutus subsp. leucotrichus	(L.) Link
Ononis pusilla	L.
Ononis spinosa	L.
Ononis spinosa subsp. austriaca	L.
Ononis arvensis	L.
Ononis spinosiformis	Simk.
Ononis spinosiformis subsp. semihircina	Simk.
Trigonella procumbens	(Bess.) Rchb.
Trigonella caerulea	(L.) Ser.
Trigonella foenum-graecum	L.
Trigonella gladiata	Stev.
Trigonella monspeliaca	L.
Medicago lupulina	L.
Medicago sativa	L.
Medicago falcata	L.
Medicago × varia	Martyn
Medicago prostrata	Jacq.
Medicago minima	(L.) L.
Medicago arabica	(L.) Huds.
Medicago orbicularis	(L.) Bartal.
Medicago rigidula	(L.) All.
Medicago nigra	(L.) Krock.
Melilotus dentatus	(W. et K.) Pers.
Melilotus altissimus subsp. macrorrhizus	Thuill.
Melilotus officinalis	(L.) Pall.
Melilotus albus	Desr.
Trifolium campestre	Schreb.
Trifolium aureum	Poll.
Trifolium patens	Schreb.
Trifolium dubium	Sibth.
Trifolium micranthum	Viv.
Trifolium montanum	L.
Trifolium hybridum	L.
Trifolium hybridum subsp. elegans	L.
Trifolium repens	L.
Trifolium strictum	L.
Trifolium retusum	L.
Trifolium angulatum	W. et K.
Trifolium fragiferum	L.
Trifolium fragiferum subsp. bonannii	L.
Trifolium resupinatum	L.
Trifolium ornithopodioides	(L.) Sm.
Trifolium vesiculosum	Savi
Trifolium diffusum	Ehrh.
Trifolium medium	L.
Trifolium medium subsp. sarosiense	L.
Trifolium medium subsp. banaticum	L.
Trifolium alpestre	L.
Trifolium rubens	L.
Trifolium ochroleucon	Huds.
Trifolium pannonicum	Jacq.
Trifolium incarnatum	L.
Trifolium subterraneum var. brachycladum	L.
Trifolium pratense	L.
Trifolium pratense subsp. expansum	L.
Trifolium pratense subsp. sativum	L.
Trifolium pallidum	W. et K.
Trifolium arvense	L.
Trifolium striatum	L.
Anthyllis vulneraria	L.
Anthyllis vulneraria subsp. polyphylla	L.
Anthyllis vulneraria subsp. alpestris	L.
Anthyllis vulneraria subsp. carpatica	L.
Dorycnium germanicum	(Gremli) Rikli
Dorycnium herbaceum	Vill.
Tetragonolobus maritimus subsp. siliquosus	(L.) Roth
Lotus angustissimus	L.
Lotus uliginosus	Schkuhr
Lotus borbasii	Ujhelyi
Lotus corniculatus	L.
Lotus tenuis	W. et K.
Amorpha fruticosa	L.
Galega officinalis	L.
Robinia pseudo-acacia	L.
Colutea arborescens	L.
Astragalus contortuplicatus	L.
Astragalus sulcatus	L.
Astragalus glycyphyllos	L.
Astrantia major	L.
Astragalus dasyanthus	Pall.
Astragalus exscapus	L.
Astragalus cicer	L.
Astragalus asper	Wulf.
Astragalus austriacus	Jacq.
Astragalus onobrychis	L.
Astragalus varius	S.G. Gmel.
Astragalus vesicarius subsp. albidus	L.
Oxytropis pilosa subsp. hungarica	(L.) DC.
Glycyrrhiza echinata	L.
Glycyrrhiza glabra	L.
Vitis vulpina	L.
Vitis vinifera	L.
Parthenocissus tricuspidata	(S. et Z.) Planch.
Parthenocissus quinquefolia	(L.) Planch.
Parthenocissus inserta	(Kern.) Fritsch
Hedera helix	L.
Cornus mas	L.
Cornus sanguinea	L.
Cornus sanguinea subsp. hungarica	L.
Hydrocotyle vulgaris	L.
Sanicula europaea	L.
Eryngium campestre	L.
Eryngium planum	L.
Physocaulis nodosus	(L.) Tausch
Chaerophyllum aromaticum	L.
Chaerophyllum hirsutum	L.
Chaerophyllum aureum	L.
Chaerophyllum temulum	L.
Chaerophyllum bulbosum	L.
Anthriscus caucalis	M.B.
Anthriscus cerefolium subsp. trichosperma	(L.) Hoffm.
Anthriscus sylvestris	(L.) Hoffm.
Anthriscus nitida	(Wahlenb.) Garcke
Scandix pecten-veneris	L.
Torilis arvensis	(Huds.) Link.
Torilis japonica	(Houtt.) DC.
Torilis ucranica	Spr.
Caucalis latifolia	(L.) L.
Caucalis platycarpos	L.
Vaccinium microcarpum	(Turcz.  ex Rupr.) Schmalh.
Centaurium erythraea subsp. austriacum	Rafn.
Galium rubioides	L.
Caucalis platycarpos subsp. muricata	L.
Orlaya grandiflora	(L.) Hoffm.
Coriandrum sativum	L.
Bifora radians	M.B.
Smyrnium perfoliatum	L.
Physospermum cornubiense	(L.) DC.
Conium maculatum	L.
Pleurospermum austriacum	(L.) Hoffm.
Bupleurum rotundifolium	L.
Bupleurum longifolium	L.
Bupleurum falcatum	L.
Bupleurum falcatum subsp. dilatatum	L.
Bupleurum praealtum	L.
Peucedanum cervaria	(L.) Lap.
Peucedanum oreoselinum	(L.) Mönch
Peucedanum alsaticum	L.
Peucedanum carvifolia	Vill.
Peucedanum officinale	L.
Peucedanum arenarium	W. et K.
Pastinaca sativa subsp. pratensis	L.
Heracleum sphondylium	L.
Heracleum sphondylium subsp. trachycarpum	L.
Heracleum sphondylium subsp. flavescens	L.
Heracleum sphondylium subsp. chlorathum	L.
Heracleum mantegazzianum	Somm. et Lev.
Tordylium maximum	L.
Laser trilobum	(L.) Borkh.
Laserpitium prutenicum	L.
Laserpitium latifolium	L.
Daucus carota	L.
Sherardia arvensis	L.
Asperula taurina subsp. leucanthera	L.
Asperula arvensis	L.
Asperula orientalis	Boiss. et Hohen.
Asperula tinctoria	L.
Asperula cynanchica	L.
Asperula cynanchica subsp. montana	L.
Cruciata pedemontana	(Bell.) Ehrend.
Cruciata glabra	(L.) Ehrend.
Cruciata laevipes	Opiz
Galium boreale	L.
Galium boreale subsp. pseudorubioides	L.
Galium rotundifolium	L.
Galium odoratum	(L.) Scop.
Galium rivale	(Sibth. et Sm.) Gris.
Galium aparine	L.
Galium spurium	L.
Coronilla emerus	L.
Coronilla varia	L.
Coronilla elegans	Panc.
Coronilla coronata	L.
Coronilla vaginalis	Lam.
Hippocrepis comosa	L.
Onobrychis viciifolia	Scop.
Onobrychis arenaria	(Kit.) Ser.
Vicia hirsuta	(L.) S.F. Gray
Vicia tetrasperma	(L.) Schreb.
Vicia tenuissima	(M.B.) Sch. et Th.
Vicia articulata	Hornem.
Vicia ervilia	(L.) Willd.
Vicia pisiformis	L.
Vicia dumetorum	L.
Vicia sparsiflora	Ten.
Vicia cassubica	L.
Vicia sylvatica	L.
Aethusa cynapium	L.
Vicia biennis	L.
Vicia villosa	Roth
Vicia villosa subsp. pseudovillosa	Roth
Vicia cracca	L.
Vicia tenuifolia	Roth
Vicia tenuifolia subsp. stenophylla	Roth
Vicia oroboides	Wulf.
Vicia sepium	L.
Vicia lathyroides	L.
Vicia grandiflora	Scop.
Vicia lutea	L.
Vicia peregrina	L.
Vicia sativa	L.
Vicia sativa subsp. cordata	L.
Vicia angustifolia	L.
Vicia angustifolia subsp. segetalis	L.
Vicia pannonica	Cr.
Vicia pannonica subsp. striata	Cr.
Vicia narbonensis subsp. serratifolia	L.
Vicia faba	L.
Lens culinaris	Medik.
Lathyrus transsilvanicus	(Spr.) Fritsch
Lythrum × scrabrum	Simk.
Ludwigia palustris	(L.) Elliot
Epilobium hirsutum	L.
Epilobium parviflorum	Schreb.
Epilobium lanceolatum	Seb. et Mauri
Epilobium montanum	L.
Epilobium collinum	C.C. Gmel.
Epilobium roseum	Schreb.
Epilobium palustre	L.
Epilobium obscurum	Schreb.
Epilobium tetragonum	L.
Epilobium tetragonum subsp. lamyi	L.
Epilobium adenocaulon	Hausskn.
Epilobium angustifolium	(L.) Holub
Epilobium dodonaei	(Vill.) Holub
Oenothera biennis	L.
Oenothera suaveolens	Desf.
Oenothera rubricaulis	Klebahn
Oenothera hoelscheri	Renner
Oenothera erythrosepala	Borb.
Oenothera salicifolia	Desf.
Oenothera syrticola	Bartlett
Circaea lutetiana	L.
Circaea alpina	L.
Circaea × intermedia	Ehrh.
Trapa natans	L.
Myriophyllum verticillatum	L.
Myriophyllum spicatum	L.
Hippuris vulgaris	L.
Dictamnus albus	L.
Ailanthus altissima	(Mill.) Swingle
Polygala major	Jacq.
Polygala comosa	Schkuhr
Polygala vulgaris	L.
Polygala vulgaris subsp. oxyptera	L.
Polygala nicaeensis subsp. carniolica	Risso
Polygala amara	L.
Polygala amara subsp. brachyptera	L.
Polygala amarella	Cr.
Polygala amarella subsp. austriaca	Cr.
Cotinus coggygria	Scop.
Rhus hirta	(L.) Sudworth
Acer tataricum	L.
Urtica dioica	L.
Acer pseudo-platanus	L.
Acer platanoides	L.
Acer campestre	L.
Acer negundo	L.
Aesculus hippocastanum	L.
Impatiens noli-tangere	L.
Impatiens parviflora	DC.
Impatiens glandulifera	Royle
Impatiens balfouri	Hook.
Ilex aquifolium	L.
Euonymus verrucosus	Scop.
Euonymus europaea	L.
Staphylea pinnata	L.
Rhamnus catharticus	L.
Rhamnus saxatilis	Jacq.
Frangula alnus	Mill.
Vitis sylvestris	C.C. Gmel.
Vitis rupestris	Scheele
Bupleurum pachnospermum	Panc.
Bupleurum affine	Sadler
Bupleurum tenuissimum	L.
Trinia glauca	(L.) Dum.
Trinia ramosissima	(Fischer ) Koch
Apium graveolens	L.
Apium repens	(Jacq.) Lagasca
Cicuta virosa	L.
Falcaria vulgaris	Bernh.
Carum carvi	L.
Pimpinella major	(L.) Huds.
Pimpinella saxifraga	L.
Pimpinella saxifraga subsp. nigra	L.
Aegopodium podagraria	L.
Berula erecta	(Huds.) Coville
Sium latifolium	L.
Sium sisaroideum	DC.
Seseli hippomarathrum	Jacq.
Seseli annuum	L.
Seseli leucospermum	W. et K.
Seseli varium	Trev.
Seseli osseum	Cr. em. Simk.
Libanotis pyrenaica	(L.) Bourg.
Libanotis pyrenaica subsp. intermedia	(L.) Bourg.
Oenanthe aquatica	(L.) Poir.
Oenanthe fistulosa	L.
Oenanthe silaifolia	M.B.
Oenanthe banatica	Heuff.
Aethusa cynapium subsp. agrestis	L.
Aethusa cynapium subsp. cynapioides	L.
Foeniculum vulgare	Mill.
Anethum graveolens var. hortorum	L.
Silaum silaus	(L.) Sch. et Th.
Silaum peucedanoides	(M.B.) Kozo-Poljanski
Cnidium dubium	(Schkuhr) Thell.
Selinum carvifolia	(L.) L.
Angelica palustris	(Bess.) HoffM.
Angelica sylvestris	L.
Angelica archangelica	L.
Ferula sadleriana	Ledeb.
Peucedanum verticillare	(L.) Koch
Peucedanum palustre	(L.) Mönch
Galium spurium subsp. infestum	L.
Galium tricornutum	Dandy
Galium tenuissimum	M.B.
Galium parisiense	L.
Galium parisiense subsp. anglicum	L.
Galium divaricatum	Pourr.
Salix triandra	L.
Galium uliginosum	L.
Galium palustre	L.
Galium elongatum	C. Presl
Galium sylvaticum	L.
Galium schultesii	Vest
Galium glaucum	L.
Galium glaucum subsp. hirsutum	L.
Galium verum	L.
Galium verum subsp. wirtgenii	L.
Galium abaujense	Borb.
Galium mollugo	L.
Galium album	Mill.
Galium album subsp. picnotrichum	Mill.
Galium lucidum	All.
Galium pumilum	Murr.
Galium austriacum	Jacq.
Rubia tinctorum	L.
Sambucus ebulus	L.
Sambucus nigra	L.
Campanula patula subsp. neglecta	L.
Sambucus racemosa	L.
Viburnum opulus	L.
Viburnum lantana	L.
Symphoricarpos rivularis	Sukdorf.
Lonicera caprifolium	L.
Lonicera xylosteum	L.
Lonicera nigra	L.
Adoxa moschatellina	L.
Valerianella coronata	(L.) DC.
Valerianella dentata	(L.) Poll.
Valerianella rimosa	Bast. et Desv.
Valerianella pumila	(L.) DC.
Valerianella locusta	(L.) Latterade
Fraxinus pennsylvanica	Marsh.
Syringa vulgaris	L.
Scabiosa columbaria subsp. pseudobanatica	L.
Scabiosa triandra subsp. agrestis	L.
Tilia tomentosa	Mönch
Tilia platyphyllos	Scop.
Tilia platyphyllos subsp. cordifolia	Scop.
Tilia platyphyllos subsp. pseudorubra	Scop.
Tilia rubra	DC.
Tilia cordata	Mill.
Abutilon theophrasti	Medik.
Lavatera thuringiaca	L.
Lavathera trimestris	L.
Althaea hirsuta	L.
Althaea cannabina	L.
Althaea officinalis	L.
Althaea officinalis subsp. pseudoarmeniaca	L.
Ligustrum vulgare	L.
Centaurium littorale subsp. uliginosum	(Turn.) Gilmour
Centaurium erythraea	Rafn
Centaurium erythraea subsp. turcicum	Rafn
Centaurium pulchellum	(Sw.) Druce
Blackstonia acuminata	(Koch et Ziz) Dom.
Gentianella ciliata	(L.) Borkh.
Gentiana cruciata	L.
Gentiana asclepiadea	L.
Gentiana pneumonanthe	L.
Valerianella carinata	Lois.
Valeriana excelsa	Poir.
Valeriana stolonifera	Wallr.
Valeriana officinalis	L.
Valeriana dioica	L.
Valeriana simplicifolia	(Reich.) Kabath
Valeriana tripteris subsp. austriaca	L.
Aster amellus	L.
Aster tripolium subsp. pannonicus	L.
Aster novae-angliae	L.
Aster novi-belgii	L.
Aster × versicolor	Willd.
Aster × salignus	Willd.
Aster × lanceolatus	Willd.
Aster tradescantii	L.
Aster bellidiastrum	(L.) Scop.
Stenactis annua	(L.) Nees
Crepis rhoeadifolia	M.B.
Crepis tectorum	L.
Crepis biennis	L.
Crepis pannonica	(Jacq.) C. Koch
Crepis nicaeensis	Balbis
Crepis capillaris	(L.) Wallr.
Crepis polymorpha	Thuill.
Crepis setosa	Hall. f.
Cyclamen purpurascens	Mill.
Armeria elongata	(Hoffm.) C. Koch
Gagea szovitsii	(Láng) Bess.
Limonium gmelini subsp. hungaricum	(Willd.) O. Kuntze
Rumex acetosella	L.
Rumex acetosella subsp. tenuifolius	L.
Rumex acetosa	L.
Rumex thyrsiflorus	Fingerhut
Rumex scutatus	L.
Rumex pseudonatronatus	Borb.
Rumex aquaticus	L.
Rumex confertus	Willd.
Rumex patientia	L.
Rumex patientia subsp. recurvatus	L.
Rumex patientia subsp. orientalis	L.
Rumex kerneri	Borb.
Rumex crispus	L.
Rumex stenophyllus	Ledeb.
Rumex conglomeratus	Murr.
Rumex sanguineus	L.
Rumex hydrolapathum	Huds.
Rumex obtusifolius	L.
Hibiscus trionum	L.
Rumex obtusifolius subsp. obtusifolius	L.
Rumex obtusifolius subsp. sylvestris	L.
Rumex obtusifolius subsp. transiens	L.
Rumex obtusifolius subsp. subalpinus	L.
Rumex pulcher	L.
Rumex dentatus subsp. halacsyi	L.
Rumex palustris	Sm.
Rumex maritimus	L.
Polygonum bistorta	L.
Polygonum amphibium	L.
Polygonum lapathifolium	L.
Polygonum lapathifolium subsp. danubiale	L.
Polygonum lapathifolium subsp. incanum	L.
Polygonum persicaria	L.
Polygonum hydropiper	L.
Dipsacus laciniatus	L.
Dipsacus sylvestris	Huds.
Dipsacus × pseudosylvester	Schur
Cephalaria pilosa	(L.) Gren. et Godr.
Cephalaria transsylvanica	(L.) Schrad.
Succisa pratensis	Mönch
Succisella inflexa	(Kluk) Beck
Knautia arvensis	(L.) Coult.
Knautia arvensis subsp. pannonica	(L.) Coult.
Knautia arvensis subsp. rosea	(L.) Coult.
Knautia kitaibelii subsp. tomentella	(Schult.) Borb.
Knautia dipsacifolia	(Schrank) Kreutzer
Knautia drymeia	Heuff.
Knautia × ramosissima	Szabó
Scabiosa canescens	W. et K.
Scabiosa ochroleuca	L.
Scabiosa columbaria	L.
Alcea biennis	Winterl
Alcea rosea	L.
Malva alcea	L.
Malva sylvestris	L.
Malva neglecta	Wallr.
Malva pusilla	Sm.
Malva moschata	L.
Malva crispa	L.
Nonea lutea	(Desr.) DC.
Radiola linoides	Roth
Linum catharticum	L.
Linum flavum	L.
Linum dolomiticum	Borb.
Linum trigynum	L.
Linum hirsutum	L.
Linum hirsutum subsp. glabrescens	L.
Linum tenuifolium	L.
Linum austriacum	L.
Linum perenne	L.
Linum usitatissimum	L.
Oxalis acetosella	L.
Oxalis fontana	Bunge
Oxalis corniculata	L.
Oxalis dillenii	Jacq.
Geranium phaeum	L.
Geranium robertianum	L.
Geranium lucidum	L.
Geranium bohemicum	L.
Geranium divaricatum	Ehrh.
Geranium sibiricum	L.
Geranium molle	L.
Geranium molle subsp. stripulare	L.
Geranium columbinum	L.
Geranium dissectum	L.
Geranium rotundifolium	L.
Geranium pusillum	Burm.f.
Geranium pyrenaicum	Burm.f.
Geranium palustre	L.
Geranium sanguineum	L.
Geranium sylvaticum	L.
Geranium pratense	L.
Erodium neilreichii	Janka
Erodium ciconium	(L.) L'Hérit.
Erodium cicutarium	(L.) L'Hérit.
Tribulus terrestris	L.
Tribulus terrestris subsp. orientalis	L.
Mercurialis annua	L.
Mercurialis perennis	L.
Mercurialis ovata	Sternb. et Hoppe
Mercurialis × paxii	Gräbn.
Ricinus communis	L.
Euphorbia nutans	Lag.
Euphorbia humifusa	Willd.
Euphorbia maculata	L.
Euphorbia palustris	L.
Euphorbia villosa	W. et K.
Euphorbia epithymoides	L.
Euphorbia dulcis	L.
Euphorbia dulcis subsp. incompta	L.
Euphorbia angulata	Jacq.
Euphorbia verrucosa	L. em. L.
Euphorbia platyphyllos	L.
Euphorbia platyphyllos subsp. literata	L.
Euphorbia stricta	L.
Euphorbia helioscopia	L.
Euphorbia amygdaloides	L.
Euphorbia salicifolia	Host
Euphorbia cyparissias	L.
Euphorbia esula	L.
Euphorbia esula subsp. pinifolia	L.
Euphorbia lucida	W. et K.
Euphorbia virgata	W. et K.
Euphorbia seguierana	Necker
Euphorbia pannonica	Host
Euphorbia falcata	L.
Euphorbia falcata subsp. acuminata	L.
Euphorbia peplus	L.
Euphorbia taurinensis	All.
Euphorbia exigua	L.
Euphorbia segetalis	L.
Callitriche palustris	L.
Callitriche cophocarpa	Sendtn.
Fraxinus ornus	L.
Fraxinus excelsior	L.
Fraxinus angustifolia subsp. pannonica	Vahl
Gentianella austriaca	(A. et J. Kern.) Holub
Gentianella livonica	(Eschsch.) Soó
Prunella × spuria	Knaf
Menyanthes trifoliata	L.
Nymphoides peltata	(S.G. Gmel.) Ktze.
Asclepias syriaca	L.
Vincetoxicum hirundinaria	Medik.
Vincetoxicum pannonicum	(Borhidi) Holub
Vinca minor	L.
Vinca herbacea	W. et K.
Vinca major	L.
Cuscuta lupuliformis	Krocker
Cuscuta europaea	L.
Cuscuta epithymum subsp. kotschyi	(L.) Nath.
Cuscuta trifolii	Bab.
Cuscuta epilinum	Weihe
Cuscuta campestris	Yuncker
Cuscuta australis	R. Br.
Cuscuta australis subsp. tinei	R. Br.
Cuscuta australis subsp. cesatiana	R. Br.
Convolvulus arvensis	L.
Convolvulus cantabrica	L.
Calystegia sepium	(L.) R. Br.
Ipomoea purpurea	(L.) Roth
Phacelia tanacetifolia	Benth.
Phacelia congesta	Hook.
Heliotropium europaeum	L.
Heliotropium supinum	L.
Omphalodes scorpioides	(Hänke ex Jacq.) Schrank
Omphalodes verna	Mönch
Cynoglossum officinale	L.
Cynoglossum hungaricum	Simk.
Lappula patula	(Lehm) Menyh.
Lappula squarrosa	(Retz.) Dum.
Ajuga reptans	L.
Lappula heteracantha	(Ledeb.) Borb.
Lappula deflexa	(Wahlb.) Garcke
Asperugo procumbens	L.
Veronica chamaedrys	L.
Orobanche cernua	L.
Symphytum tuberosum subsp. angustifolium	L.
Symphytum officinale	L.
Symphytum officinale subsp. uliginosum	L.
Symphytum officinale subsp. bohemicum	L.
Borago officinalis	L.
Anchusa barrelieri	(All.) Vitm.
Anchusa officinalis	L.
Anchusa ochroleuca subsp. pustulata	M.B.
Anchusa azurea	Mill.
Lycopsis arvensis	L.
Nonea pulla	(L.) Lam. et DC.
Alkanna tinctoria	(L.) Tausch
Pulmonaria angustifolia	L.
Pulmonaria officinalis	L.
Pulmonaria obscura	Dum.
Pulmonaria mollis	Wolff
Myosotis sparsiflora	Mikan
Myosotis caespitosa	K.F. Schultz
Myosotis palustris	L.
Myosotis nemorosa	Bess.
Myosotis sylvatica	(Ehrh.) Hoffm.
Myosotis stenophylla	Knaf
Myosotis arvensis	(L.) Hill
Myosotis ramosissima	Roch.
Myosotis stricta	Link
Myosotis discolor	Pers.
Lithospermum officinale	L.
Lithospermum purpureo-coeruleum	L.
Lithospermum arvense	L.
Veronica beccabunga	L.
Veronica scutellata	L.
Veronica montana	L.
Veronica officinalis	L.
Veronica chamaedrys subsp. vindobonensis	L.
Veronica prostrata	L.
Veronica austriaca	L.
Veronica austriaca subsp. dentata	L.
Veronica teucrium	L.
Veronica asutriaca subsp. bihariense	L.
Veronica paniculata	L.
Veronica longifolia	L.
Veronica pallens	Host
Veronica spicata	L.
Veronica spicata subsp. nitens	L.
Veronica orchidea	Cr.
Veronica serpyllifolia	L.
Solidago graminifolia	(L.) Salisb.
Xanthium albinum subsp. riparium	(Widder) Scholz et Sukopp
Veronica verna	L.
Veronica dillenii	Cr.
Veronica triphyllos	L.
Veronica praecox	All.
Veronica arvensis	L.
Veronica acinifolia	L.
Veronica agrestis	L.
Veronica polita	Fr.
Veronica opaca	Fr.
Euphrasia rostkoviana	Hayne
Euphrasia kerneri	Wettst.
Xanthium saccharatum	Wallr.
Euphrasia stricta	D. Wolff
Euphrasia tatarica	Fisch.
Orthantha lutea	(L.) Kern.
Odontites vernus	(Bell.) Dum.
Odontites vulgaris	Mönch
Rhinanthus minor	L.
Veronica persica	Poir.
Veronica hederifolia	L.
Veronica hederifolia subsp. triloba	L.
Veronica hederifolia subsp. lucorum	L.
Veronica peregrina	L.
Veronica filiformis	Sm.
Digitalis grandiflora	Mill.
Digitalis lanata	Ehrh.
Digitalis ferruginea	L.
Digitalis purpurea	L.
Melampyrum cristatum	L.
Melampyrum arvense	L.
Melampyrum barbatum	W. et K.
Melampyrum nemorosum	L.
Lactuca quercina subsp. sagittata	L.
Luzula divulgata	Kirsch.
Melampyrum bihariense prol. romanicum	Kern.
Melampyrum pratense	L.
Lithospermum arvense subsp. coerulescens	L.
Onosma visianii	Clem.
Onosma arenarium	W. et K.
Festuca javorkae	Májosvský
Onosma arenarium subsp. tuberculatum	W. et K.
Onosma tornense	Jáv.
Cerinthe minor	L.
Echium italicum	L.
Echium russicum	J.F. Gmel.
Echium vulgare	L.
Verbena officinalis	L.
Verbena supina	L.
Ajuga chamaepitys	(L.) Schreb.
Ajuga chamaepitys subsp. ciliata	(L.) Schreb.
Ajuga laxmannii	(L.) Benth.
Ajuga genevensis	L.
Teucrium scorodonia	L.
Teucrium montanum	L.
Teucrium montanum subsp. subvillosum	L.
Teucrium botrys	L.
Teucrium chamaedrys	L.
Teucrium scordium	L.
Lavandula angustifolia	Mill.
Scutellaria hastifolia	L.
Scutellaria galericulata	L.
Scutellaria columnae	L.
Scutellaria altissima	L.
Marrubium vulgare	L.
Marrubium peregrinum	L.
Marrubium × paniculatum	Desr.
Sideritis montana	L.
Nepeta pannonica	L.
Nepeta cataria	L.
Nepeta parviflora	M.B.
Glechoma hederacea	L.
Glechoma hirsuta	W. et K.
Dracocephalum austriacum	L.
Dracocephalum ruyschiana	L.
Dracocephalum moldavicum	L.
Prunella grandiflora	(L.) Scholler
Prunella vulgaris	L.
Prunella laciniata	(L.) Nath.
Festuca vojtkoi	Penksza
Poa stiriaca	Fritsch et Hay.
Prunella × intermedia	Link
Prunella × bicolor	Beck
Melittis melissophyllum	L.
Melittis carpatica	Klokov
Phlomis tuberosa	L.
Galeopsis ladanum	L.
Bromus carinatus	Hook. et Arn.
Galeopsis ladanum subsp. angustifolia	L.
Galeopsis speciosa	Mill.
Galeopsis pubescens	Bess.
Galeopsis tetrahit	L.
Galeopsis bifida	Boenn.
Galeopsis segetum	Necker
Galeobdolon luteum	Huds.
Galeobdolon argentatum	Smejkal
Galeobdolon luteum subsp. montanum	Huds.
Lamium orvala	L.
Lamium amplexicaule	L.
Lamium purpureum	L.
Lamium album	L.
Lamium maculatum	(L.) L.
Leonurus cardiaca	L.
Leonurus cardiaca subsp. villosus	L.
Leonurus marrubiastrum	L.
Ballota nigra	L.
Betonica officinalis	L.
Stachys annua	(L.) L.
Stachys recta	L.
Stachys sylvatica	L.
Stachys palustris	L.
Stachys alpina	L.
Stachys germanica	L.
Stachys byzantina	C.Koch
Salvia glutinosa	L.
Salvia verticillata	L.
Salvia aethiops	L.
Salvia austriaca	Jacq.
Salvia nutans	L.
Salvia nemorosa	L.
Salvia pratensis	L.
Salvia sclarea	L.
Salvia officinalis	L.
Melissa officinalis	L.
Satureja hortensis	L.
Clinopodium vulgare	L.
Acinos arvensis	(Lam.) Dandy
Calamintha menthifolia subsp. sylvatica	Host
Calamintha einseleana	F. Schultz
Calamintha thymifolia	(Scop.) Rchb.
Hyssopus officinalis	L.
Origanum vulgare	L.
Origanum vulgare subsp. prismaticum	L.
Thymus pannonicus	All.
Thymus glabrescens	Willd.
Thymus glabrescens subsp. decipiens	Willd.
Thymus praecox	Opiz
Thymus serpyllum	L.
Thymus pulegioides	L.
Bromus lanceolatus	Roth
Thymus pulegioides subsp. montanus	L.
Thymus vulgaris	L.
Lycopus europaeus	L.
Lycopus europaeus subsp. mollis	L.
Anthoxanthum aristatum	Boiss.
Scolochloa festucacea	(Willd.) Link
Lycopus exaltatus	L.
Lycopus × intercendens	Rech.
Mentha pulegium	L.
Mentha longifolia	(L.) Nath.
Mentha × dumetorum	Schult.
Mentha × dalmatica	Tausch
Mentha × carinthiaca	Host
Eragrostis multicaulis	Steud.
Mentha aquatica	L.
Mentha arvensis	L.
Mentha × gentilis	L.
Mentha × verticillata	L.
Nicandra physalodes	(L.) Gärtn.
Lycium barbarum	L.
Lycium chinense	Mill.
Atropa bella-donna	L.
Scopolia carniolica	Jacq.
Hyoscyamus niger	L.
Physalis alkekengi	L.
Solanum dulcamara	L.
Solanum nigrum	L.
Solanum nigrum subsp. schultesii	L.
Solanum alatum	Mönch
Solanum luteum	Mill.
Solanum rostratum	Dun.
Datura stramonium	L.
Nicotiana rustica	L.
Eragrostis mexicana	(Hornem.) Link
Nicotiana tabacum	L.
Verbascum phoeniceum	L.
Verbascum blattaria	L.
Verbascum thapsus	L.
Verbascum densiflorum	Bert.
Verbascum phlomoides	L.
Verbascum lychnitis	L.
Verbascum speciosum	Schrad.
Verbascum pulverulentum	Vill.
Verbascum austriacum	Schott
Verbascum nigrum	L.
Cymbalaria muralis	G.M. Sch.
Kickxia spuria	(L.) Dum.
Kickxia elatine	(L.) Dum.
Kickxia × confinus	(Lacroix) Soó
Linaria arvensis	(L.) Desf.
Linaria genistifolia	(L.) Mill.
Linaria vulgaris	Mill.
Linaria angustissima	(Lois.) Re
Linaria × kocianovichii	Asch.
Misopates orontium	(L.) Raf.
Antirrhinum majus	L.
Chaenorhinum minus	(L.) Lange
Scrophularia vernalis	L.
Scrophularia scopolii	Hoppe
Scrophularia nodosa	L.
Scrophularia umbrosa	Dum.
Scrophularia umbrosa subsp. neesii	Dum.
Gratiola officinalis	L.
Limosella aquatica	L.
Lindernia procumbens	(Krock.) Borb.
Veronica anagallis-aquatica	L.
Veronica anagalloides	Guss.
Veronica catenata	Pennell
Veronica scardica	Gris.
Rhinanthus serotinus	(Schönheit) Oborny em. Hyl.
Panicum dichotomiflorum	Michx.
Lemna minuta	Kunth
Lemna turionifera	Landolt
Lemna aequinoctalis	Welw.
Rhinanthus borbasii	(Dörfl.) Soó
Carex demissa	Hornem.
Rhinanthus rumelicus	Vel.
Rhinanthus wagneri	Deg.
Rhinanthus alectorolophus	Poll.
Pedicularis palustris	L.
Lathraea squamaria	L.
Globularia cordifolia	L.
Globularia punctata	Lap.
Orobanche ramosa	L.
Orobanche nana	Noë
Orobanche caesia	Rchb.
Orobanche arenaria	Borkh.
Orobanche purpurea	Jacq.
Orobanche coerulescens subsp. occidentalis	Steph.
Orobanche cernua subsp. cumana	Löefl.
Orobanche alba	Stephan
Orobanche reticulata	Wallr.
Orobanche loricata	Rchb.
Orobanche picridis	F. Schultz
Cardamine amara	L.
Cardamine pratensis	L.
Cardamine pratensis subsp. dentata	L.
Cardamine pratensis subsp. paludosus	L.
Cardamine pratensis subsp. matthioli	L.
Cardamine trifolia	L.
Dentaria bulbifera	L.
Dentaria enneaphyllos	L.
Dentaria glandulosa	W. et K.
Dentaria trifolia	W. et K.
Barbarea stricta	Andrz.
Barbarea vulgaris	R. Br.
Orobanche minor	Sutton
Orobanche hederae	Duby
Orobanche caryophyllacea	Sm.
Orobanche teucrii	Holandre
Orobanche lutea	Baumg.
Orobanche elatior	Sutton
Orobanche alsatica	Kirschl.
Orobanche flava	Martius
Orobanche gracilis	Sm.
Pinguicula vulgaris	L.
Pinguicula alpina	L.
Utricularia vulgaris	L.
Utricularia australis	R. Br.
Utricularia minor	L.
Utricularia bremii	Heer
Plantago arenaria	W. et K.
Plantago tenuiflora	W. et K.
Plantago maritima	L.
Plantago schwarzenbergiana	Schur
Plantago argentea	Chaix
Plantago lanceolata	L.
Plantago lanceolata subsp. dubia	L.
Plantago lanceolata subsp. sphaerostachya	L.
Plantago altissima	L.
Plantago maxima	Juss.
Plantago media	L.
Plantago major	L.
Plantago major subsp. intermedia	L.
Plantago major subsp. winteri	L.
Chelidonium majus	L.
Glaucium flavum	Cr.
Glaucium corniculatum	(L.) Rudolph
Papaver argemone	L.
Papaver hybridum	L.
Papaver dubium	L.
Papaver rhoeas	L.
Papaver somniferum	L.
Corydalis cava	(L.) Schw.
Corydalis solida	(L.) Clairville
Corydalis solida subsp. slivenensis	(L.) Clairville
Corydalis intermedia	(L.) Mérat
Corydalis pumila	(Host) Rchb.
Corydalis × campylochila	Teyb.
Corydalis lutea	(L.) Lam. et DC.
Fumaria rostellata	Knaf
Fumaria officinalis	L.
Fumaria schleicheri	Soy.-Will.
Fumaria vaillantii	Lois.
Fumaria parviflora	Lam.
Brassica elongata	Ehrh.
Brassica elongata subsp. armoracioides	Ehrh.
Brassica × juncea	(L.) Czern.
Brassica nigra	(L.) Koch
Brassica rapa subsp. sylvestris	(L.) Janch.
Brassica × napus	L.
Brassica oleracea	L.
Erucastrum gallicum	(Willd.) Schultz
Erucastrum nasturtiifolium	(Poir.) Schultz
Sinapis arvensis	L.
Sinapis alba	L.
Sinapis alba subsp. dissecta	L.
Eruca sativa	Mill.
Diplotaxis muralis	(L.) DC.
Diplotaxis tenuifolia	(Jusl.) DC.
Diplotaxis erucoides	(Torn.) DC.
Raphanus raphanistrum	L.
Raphanus sativus	L.
Calepina irregularis	(Asso) Thell. ex Sch. et K.
Crambe tataria	Sebeók
Rapistrum perenne	(L.) All.
Conringia orientalis	(L.) Dum.
Conringia austriaca	(Jacq.) Sweet
Lepidium campestre	(L.) R. Br.
Lepidium perfoliatum	L.
Lepidium crasssifolium	W. et K.
Lepidium ruderale	L.
Lepidium graminifolium	L.
Lepidium densiflorum	Schrad.
Lepidium virginicum	L.
Cardaria draba	(L.) Desv.
Coronopus squamatus	(Forskal) Asch.
Coronopus didymus	(L.) Sm.
Isatis tinctoria	L.
Isatis tinctoria subsp. praecox	L.
Biscutella laevigata	L.
Biscutella laevigata subsp. kerneri	L.
Biscutella laevigata subsp. hungarica	L.
Biscutella laevigata subsp. austriaca	L.
Biscutella laevigata subsp. illyrica	L.
Aethionema saxatile	(L.) R. Br.
Thlaspi arvense	L.
Thlaspi alliaceum	L.
Sorbus × budaiana	Kárp.
Thlaspi perfoliatum	L.
Tulipa sylvestris	L.
Thlaspi coerulescens	J. et C. Presl
Thlaspi montanum	L.
Thlaspi goesingense	Hal.
Thlaspi jankae	Kern.
Thlaspi kovatsii subsp. schudichii	Heuff.
Teesdalia nudicaulis	(L.) R. Br.
Capsella bursa-pastoris	(L.) Medik
Capsella rubella	Reut.
Hornungia petraea	(L.) Rchb.
Myagrum perfoliatum	L.
Neslea paniculata	(L.) Desv.
Bunias orientalis	L.
Bunias erucago	L.
Euclidium syriacum	(L.) R. Br.
Lunaria rediviva	L.
Lunaria annua	L.
Lunaria annua subsp. pachyrrhiza	L.
Peltaria perennis	(Ard.) Markgr.
Alyssum montanum	L.
Alyssum montanum subsp. brymii	L.
Alyssum montanum subsp. gmelinii	L.
Alyssum tortuosum	W. et K.
Alyssum tortuosum subsp. tortuosum	W. et K.
Alyssum tortuosum subsp. heterophyllum	W. et K.
Alyssum alyssoides	(L.) L.
Alyssum desertorum	Stapf
Alyssum saxatile	L.
Berteroa incana	(L.) DC.
Draba lasiocarpa	Roch.
Draba muralis	L.
Sorbus × tobani	Cs. Németh
Draba nemorosa	L.
Erophila praecox	(Stev.) DC.
Erophila spathulata	Láng
Erophila verna	(L.) Chev.
Sorbus × majeri	Barabits
Sorbus × dracofolius	Cs. Németh
Sorbus × bodajkensis	Barabits
Sorbus × vallerubusensis	Cs. Németh
Sorbus × veszpremensis	Barabits
Sorbus × acutiserratus	Cs. Németh
Turritis glabra	L.
Filipendula ulmaria subsp. denudata	(L.) Maxim.
Tilia rubra ssp. caucasica	DC.
Armoracia lapathifolia	Usteri
Armoracia macrocarpa	(W. et K.) Baumg.
Cardamine impatiens	L.
Cardamine hirsuta	L.
Cardamine flexuosa	With.
Cardamine parviflora	L.
Barbarea verna	(Mill.) Asch.
Cardaminopsis petraea	(L.) Hiit.
Cardaminopsis arenosa	(L.) Hay.
Cardaminopsis arenosa subsp. petrogena	(L.) Hay.
Cardaminopsis arenosa subsp. borbasii	(L.) Hay.
Arabis turrita	L.
Arabis alpina	L.
Arabis auriculata	Lam.
Arabis hirsuta	(L.) Scop.
Arabis hirsuta subsp. sagittata	(L.) Scop.
Arabis hirsuta subsp. gerardii	(Bess.) Hartm. f.
Nasturtium officinale	R. Br.
Rorippa palustris	(L.) Bess.
Rorippa austriaca	(Cr.) Bess.
Rorippa amphibia	(L.) Bess.
Rorippa sylvestris	(L.) Bess.
Rorippa sylvestris subsp. sylvestris	(L.) Bess.
Rorippa sylvestris subsp. kerneri	(L.) Bess.
Rorippa × anceps	(Wahlbg.) Rchb.
Rorippa × astylis	(Rchb.) Rchb.
Rorippa × armoracioides	(Tausch) Fuss
Rorippa × hungarica	Borb.
Matthiola incana	(L.) R. Br.
Matthiola longipetala subsp. bicornis	(Vent.) DC.
Malcolmia africana	(L.) R. Br.
Chorispora tenella	(Pall.) DC.
Hesperis tristis	L.
Hesperis matronalis	L.
Hesperis matronalis subsp. spontanea	L.
Hesperis matronalis subsp. candida	L.
Camelina sativa	(L.) Cr.
Hesperis matronalis subsp. vrabelyiana	L.
Hesperis sylvestris	Cr.
Erysimum cheiranthoides	L.
Erysimum repandum	Höjer
Erysimum crepidifolium	Rchb.
Erysimum hieracifolium	Jusl.
Erysimum odoratum	Ehrh.
Erysimum odoratum subsp. buekkense	(Boros) Soó
Erysimum pallidiflorum	Szépligeti
Erysimum diffusum	Ehrh.
Syrenia cana	(Pill. et Mitterp.) Neilr.
Alliaria petiolata	(M.B.) Cavara et Grande
Descurainia sophia	(L.) Webb in Prantl
Sisymbrium officinale	(L.) Scop.
Sisymbrium strictissimum	L.
Sisymbrium polymorphum	(Murr.) Roth
Sisymbrium loeselii	Jusl.
Sisymbrium altissimum	L.
Sisymbrium orientale	Torn.
Arabidopsis thaliana	(L.) Heynh.
Camelina microcarpa	Andrz.
Camelina microcarpa subsp. pilosa	Andrz.
Camelina alyssum	(Mill.) Thell.
Camelina rumelica	Vel.
Reseda luteola	L.
Reseda lutea	L.
Reseda inodora	Rchb.
Reseda phyteuma	L.
Drosera rotundifolia	L.
Drosera anglica	Huds.
Aldrovanda vesiculosa	L.
Myricaria germanica	(L.) Desv.
Tamarix tetrandra	Pall.
Tamarix gallica	L.
Tamarix ramosissima	Ledeb.
Helianthemum canum	(L.) Baumg.
Helianthemum nummularium	(L.) Mill.
Helianthemum ovatum	(Viv.) Dun.
Fumana procumbens	(Dun.) Gren. et Godr.
Viola biflora	L.
Viola odorata	L.
Viola suavis	M.B.
Viola alba	Bess.
Viola collina	Bess.
Viola ambigua	W. et K.
Viola hirta	L.
Viola mirabilis	L.
Viola rupestris	F.W. Schm.
Viola sylvestris	Lam.
Viola riviniana	Rchb.
Viola canina	L. em. Rchb.
Viola montana	L.
Viola stagnina	Kit.
Viola pumila	Chaix
Viola elatior	Fr.
Viola elatior subsp. jordanii	Fr.
Viola tricolor	L.
Viola tricolor subsp. subalpina	L.
Viola tricolor subsp. polychroma	L.
Viola arvensis	Murr.
Viola kitaibeliana	R. et Sch.
Elatine alsinastrum	L.
Elatine triandra	Schkuhr
Elatine hungarica	Moesz
Elatine hydropiper	L.
Elatine hexandra	L. em. Oeder
Thladiantha dubia	Bunge
Bryonia alba	L.
Bryonia dioica	Jacq.
Ecballium elaterium	(L.) Rich.
Cucurbita pepo	L.
Sicyos angulatus	L.
Echinocystis lobata	(Michx.) Torr.
Hypericum humifusum	L.
Hypericum tetrapterum	Fr.
Hypericum perforatum	L.
Hypericum maculatum	Cr.
Hypericum maculatum subsp. obtusiusculum	Cr.
Hypericum hirsutum	L.
Campanula latifolia	L.
Campanula rapunculoides	L.
Campanula trachelium	L.
Campanula bononiensis	L.
Campanula rotundifolia	L.
Campanula moravica	(Spitzner) Kovanda
Campanula xylocarpa	Kovanda
Campanula persicifolia	L.
Campanula rapunculus	L.
Campanula patula	L.
Adenophora liliifolia	(L.) Bess.
Legousia speculum-veneris	(L.) Chaix
Asyneuma canescens	(W. et K.) G. et Sch.
Asyneuma canescens subsp. salicifolium	(W. et K.) G. et Sch.
Phyteuma spicatum	L.
Phyteuma orbiculare	L.
Jasione montana	L.
Eupatorium cannabinum	L.
Solidago virgaurea	L.
Solidago canadensis	L.
Solidago gigantea subsp. serotina	Ait.
Bellis perennis	L.
Aster linosyris	(L.) Bernh.
Aster oleifolius	(Lam.) Wagenitz
Aster sedifolius	L.
Aster sedifolius subsp. canus	L.
Stenactis annua subsp. strigosa	(L.) Nees
Erigeron canadensis	L.
Erigeron acris	L.
Xeranthemum annuum	L.
Xeranthemum cylindraceum	Sibth. et Sm.
Carlina acaulis	L.
Carlina vulgaris	L.
Carlina vulgaris subsp. intermedia	L.
Carlina vulgaris subsp. longifolia	L.
Arctium tomentosum	Mill.
Arctium lappa	L.
Arctium minus	(Hill) Bernh.
Arctium nemorosum	Lej. et Court.
Arctium nemorosum subsp. pubens	Lej. et Court.
Jurinea mollis	(L.) Rchb.
Jurinea mollis subsp. macrocalathia	(L.) Rchb.
Hypericum barbatum	Jacq.
Hypericum elegans	Stpehan
Hypericum montanum	L.
Hypericum × desetangsii	Lam.
Chimaphila umbellata	(L.) Barton
Moneses uniflora	(L.) A. Gray
Orthilia secunda	(L.) House
Pyrola rotundifolia	L.
Pyrola chlorantha	Sw.
Pyrola minor	L.
Pyrola media	Sw.
Monotropa hypopitys	L.
Monotropa hypopitys subsp. hypophegea	L.
Calluna vulgaris	(L.) Hull.
Andromeda polifolia	L.
Vaccinium oxycoccos	L.
Vaccinium vitis-idaea	L.
Vaccinium myrtillus	L.
Campanula glomerata	L.
Campanula glomerata subsp. farinosa	L.
Campanula glomerata subsp. elliptica	L.
Campanula macrostachya	Kit.
Campanula cervicaria	L.
Campanula sibirica	L.
Campanula sibirica subsp. divergentiformis	L.
Erigeron acris subsp. angulosus	L.
Erigeron acris subsp. macrophyllus	L.
Micropus erectus	L.
Filago vulgaris	Lam.
Filago vulgaris subsp. lutescens	Lam.
Filago arvensis	L.
Filago minima	(Sm.) Pers.
Antennaria dioica	(L.) Gärtn.
Gnaphalium sylvaticum	L.
Gnaphalium uliginosum	L.
Gnaphalium luteo-album	L.
Helichrysum arenarium	(L.) Mönch
Inula helenium	L.
Inula conyza	DC.
Inula ensifolia	L.
Inula salicina	L.
Inula salicina subsp. aspera	L.
Inula salicina subsp. denticulata	L.
Inula spiraeifolia	L.
Inula hirta	L.
Inula germanica	L.
Inula britannica	L.
Inula oculus-christi	L.
Pulicaria vulgaris	Gärtn.
Pulicaria dysenterica	(L.) Bernh.
Carpesium cernuum	L.
Carpesium abrotanoides	L.
Buphthalmum salicifolium	L.
Telekia speciosa	(Schreb.) Baumg.
Ambrosia artemisifolia	L.
Iva xanthiifolia	Nutt.
Xanthium spinosum	L.
Xanthium strumarium	L.
Xanthium italicum	Moretti
Rudbeckia laciniata	L.
Rudbeckia hirta	L.
Helianthus annuus	L.
Helianthus tuberosus	L.
Helianthus rigidus	(Cass.) Desf.
Bidens tripartita	L.
Bidens cernua	L.
Bidens frondosa	L.
Galinsoga parviflora	Cav.
Galinsoga ciliata	(Raf.) Blake
Tagetes patula	L.
Anthemis cotula	L.
Anthemis tinctoria	L.
Anthemis austriaca	Jacq.
Anthemis arvensis	L.
Anthemis ruthenicus	M.B.
Achillea ptarmica	L.
Achillea ochroleuca	Ehrh.
Achillea horanszkyi	Ujhelyi
Achillea nobilis subsp. neilreichii	L.
Achillea distans	W. et K.
Achillea distans subsp. stricta	W. et K.
Achillea crithmifolia	W. et K.
Achillea tuzsonii	Ujhelyi
Achillea asplenifolia	Vent.
Achillea millefolium	L.
Achillea collina	J. Becker
Achillea pannonica	Scheele
Achillea setacea	W. et K.
Matricaria discoidea	DC.
Matricaria chamomilla	L.
Matricaria maritima subsp. inodora	L.
Matricaria tenuifolia	(Kit.) Simk.
Chrysanthemum leucanthemum	L.
Chrysanthemum leucanthemum subsp. sylvestre	L.
Artemisia × gayeriana	Kárp.
Chrysanthemum lanceolatum	Pers.
Chrysanthemum serotinum	L.
Chrysanthemum corymbosum	L.
Chrysanthemum parthenium	(L.) Bernh.
Chrysanthemum segetum	L.
Tanacetum vulgare	L.
Artemisia vulgaris	L.
Artemisia pontica	L.
Artemisia austriaca	Jacq.
Artemisia absinthium	L.
Artemisia alba	Turra
Artemisia alba subsp. saxatilis	Turra
Artemisia alba subsp. canescens	Turra
Artemisia campestris	L.
Artemisia campestris subsp. lednicensis	L.
Artemisia scoparia	W. et K.
Artemisia annua	L.
Artemisia santonicum	L.
Artemisia santonicum subsp. patens	L.
Artemisia abrotanum	L.
Tussilago farfara	L.
Petasites hybridus	(L.) G.M. Sch.
Petasites albus	(L.) Gärtn.
Erechtites hieraciifolia	(L.) Raf.
Arnica montana	L.
Doronicum hungaricum	(Sadl. in Gr. et Sch.) Rchb.
Doronicum austriacum	Jacq.
Doronicum orientale	Hoffm.
Doronicum × sopianae	Gáyer et Szita
Senecio integrifolius	(L.) Clairv.
Senecio aurantiacus	(Hoppe) Less.
Senecio ovirensis	(Koch) DC.
Senecio ovirensis subsp. gaudinii	(Koch) DC.
Senecio rivularis	(W. et K.) DC.
Senecio vulgaris	L.
Senecio sylvaticus	L.
Senecio viscosus	L.
Senecio vernalis	W. et K.
Senecio rupestris	W. et K.
Senecio erucifolius	L.
Senecio erucifolius subsp. tenuifolius	L.
Senecio jacobaea	L.
Senecio erraticus subsp. barbareifolius	Bertol.
Senecio aquaticus	Hill
Senecio paludosus	L.
Senecio paludosus subsp. lanatus	L.
Senecio fluviatilis	Wallr.
Senecio nemorensis	L.
Senecio nemorensis subsp. fuchsii	L.
Senecio doria	L.
Senecio umbrosus	W. et K.
Senecio inaequidens	DC.
Ligularia sibirica	(L.) Cass.
Calendula officinalis	L.
Echinops sphaerocephalus	L.
Echinops ruthenicus	(Fisch.) M.B.
Jurinea mollis subsp. dolomitica	(L.) Rchb.
Carduus nutans subsp. macrolepis	L.
Carduus acanthoides	L.
Carduus hamulosus	Ehrh.
Carduus crispus	L.
Serratula tinctoria	L.
Carduus glaucus	Holub
Carduus collinus	W. et K.
Carduus × orthocephalus	Wallr.
Carduus × solteszii	Budai
Cirsium vulgare	(Savi) Ten.
Cirsium eriophorum	(L.) Scop.
Cirsium boujarti	(Pill. et Mitterp.) C.H. Schultz
Cirsium furiens	Gr. et Sch.
Cirsium arvense	(L.) Scop.
Cirsium brachycephalum	Jur.
Cirsium palustre	(L.) Scop.
Cirsium canum	(L.) All.
Cirsium pannonicum	(L. f.) Link
Cirsium rivulare	(Jacq.) All.
Cirsium erisithales	(Jacq.) Scop.
Cirsium × linkianum	M. Loehr.
Cirsium oleraceum	(L.) Scop.
Cirsium × tataricum	(Jacq.) All.
Cirsium × hybridum	Koch
Cirsium × silesiacum	C. H. Schultz
Onopordum acanthium	L.
Crupina vulgaris	Pers.
Serratula lycopifolia	(Vill.) Kern.
Serratula radiata	(W. et K.) M.B.
Centaurea calcitrapa	L.
Centaurea solstitialis	L.
Centaurea cyanus	L.
Centaurea triumfetti	All.
Centaurea triumfetti subsp. aligera	All.
Centaurea triumfetti subsp. stricta	All.
Centaurea mollis	W. et K.
Centaurea salonitana var. taurica	Vis.
Centaurea scabiosa	L.
Centaurea fritschii	Hay.
Centaurea spinulosa	Roch.
Centaurea sadleriana	Janka
Centaurea diffusa	Lam.
Centaurea arenaria	M.B.
Centaurea arenaria subsp. borysthenica	M.B.
Centaurea arenaria subsp. tauscheri	M.B.
Centaurea micranthos	S.G. Gmel.
Centaurea rhenana	Bor.
Centaurea jacea	L.
Centaurea jacea subsp. subjacea	L.
Centaurea pannonica	(Heuff.) Simk.
Centaurea banatica	Roch.
Centaurea macroptilon	Borb.
Centaurea macroptilon subsp. oxylepis	Borb.
Centaurea nigrescens	Willd.
Centaurea nigrescens subsp. vochinensis	Willd.
Centaurea pseudophrygia	C. A. Mey.
Centaurea stenolepis	Kern.
Centaurea indurata	Janka
Carthamus lanatus	L.
Cichorium intybus	L.
Lapsana communis	L.
Hypochoeris maculata	L.
Hypochoeris radicata	L.
Leontodon saxatilis	Lam.
Melandrium sylvestre	(Schkuhr) Roehl
Silene nemoralis	W. et K.
Silene nutans	L.
Silene viridiflora	L.
Silene longiflora	Ehrh.
Silene multiflora	(Ehrh.) Pers.
Silene otites	(L.) Wibel
Silene otites subsp. wolgensis	(L.) Wibel
Silene otites subsp. pseudo-otites	(L.) Wibel
Silene borysthenica	(Gruner) Walters
Silene vulgaris	(Mönch) Garcke
Silene flavescens	W. et K.
Silene armeria	L.
Silene dichotoma	Ehrh.
Silene gallica	L.
Silene conica	L.
Cucubalus baccifer	L.
Gypsophila muralis	L.
Gypsophila fastigiata subsp. arenaria	L.
Gypsophila paniculata	L.
Petrorhagia saxifraga	(L.) Link
Petrorhagia prolifera	(L.) Ball et Heyw.
Petrorhagia glumacea var. obcordata	(Chaub. et Bory) Ball et Heyw.
Vaccaria hispanica	(Mill.) Rauschert
Vaccaria hispanica subsp. grandiflora	(Mill.) Rauschert
Dianthus collinus	W. et K.
Dianthus collinus subsp. glabriusculus	W. et K.
Dianthus carthusianorum subsp. carthusianorum	L.
Dianthus barbatus	L.
Dianthus plumarius subsp. praecox	L.
Dianthus plumarius subsp. lumnitzeri	L.
Dianthus plumarius subsp. regis-stephani	L.
Dianthus arenarius subsp. borussicus	L.
Cerastium fontanum subsp. macrocarpum	Baumg.
Cerastium arvense	L.
Cerastium arvense subsp. calcicola	L.
Holosteum umbellatum	L.
Holosteum umbellatum subsp. glutinosum	L.
Moenchia mantica	(L.) Bartl.
Sagina procumbens	L.
Sagina micropetala	Rauschert
Sagina ciliata	Fr.
Sagina nodosa	(L.) Fenzl
Sagina subulata	(Sw.) C. Presl
Sagina sabuletorum	(Gay) Lange
Sagina saginoides	(L.) Karsten
Minuartia viscosa	(Schreb.) Sch. et Th.
Minuartia fastigiata	(Sm.) Rchb.
Minuartia glomerata	(M.B.) Deg.
Minuartia setacea	(Thuill.) Hay.
Minuartia verna	(L.) Hiern
Minuartia verna subsp. ramosissima	(L.) Hiern
Minuartia frutescens	(Kit.) Tuzson
Arenaria procera subsp. glabra	Spr.
Arenaria serpyllifolia	L.
Leontodon incanus	(L.) Schrank
Leontodon autumnalis	L.
Leontodon hispidus	L.
Helminthia echioides	(L.) Gärtn.
Picris hieracioides	L.
Lactuca perennis	L.
Picris hieracioides subsp. crepoides	L.
Picris hieracioides subsp. spinulosa	L.
Tragopogon floccosus	W. et K.
Tragopogon dubius	Scop.
Tragopogon dubius subsp. major	Scop.
Tragopogon orientalis	L.
Scorzonera purpurea	L.
Scorzonera hispanica	L.
Scorzonera austriaca	Willd.
Scorzonera humilis	L.
Scorzonera parviflora	Jacq.
Podospermum canum	C.A. Mey.
Podospermum laciniatum	(L.) DC.
Chondrilla juncea	L.
Taraxacum serotinum	(W. et K.) Poir.
Taraxacum bessarabicum	(Hornem.) Hand.-Mazz.
Taraxacum laevigatum	(Willd.) DC.
Taraxacum palustre	(Lyons) Simons
Taraxacum officinale	Weber
Mycelis muralis	(L.) Dum.
Lactuca viminea	(L.) J. et C. Presl
Lactuca quercina	L.
Lactuca saligna	L.
Lactuca serriola	L.
Sonchus palustris	L.
Sonchus arvensis	L.
Sonchus arvensis subsp. uliginosus	L.
Sonchus oleraceus	L.
Sonchus asper	(L.) Hill
Prenanthes purpurea	L.
Crepis paludosa	(L.) Mönch
Crepis praemorsa	(L.) Tausch
Crepis pulchra	L.
Hieracium × viridifolium	Peter
Hieracium hoppeanum	Schult.
Hieracium × schultesii	F. Schultz
Hieracium × bifurcum	M.B.
Hieracium × laschii	(F.W. et C. H. Schultz) Z.
Hieracium × brachiatum	Bert.
Hieracium rothianum	(Wallr.) Soó
Hieracium pilosella	L.
Hieracium lactucella	Wallr.
Hieracium aurantiacum	L.
Hieracium caespitosum	Dum.
Hieracium × flagellare	Willd.
Hieracium × floribundum	W. et Gr.
Hieracium × piloselliflorum	N. et P.
Hieracium bauhinii	Schult. ex Bess.
Hieracium × leptophyton	N. et P.
Hieracium × koernickeanum	(N. et P.) Z.
Hieracium × polymastix	Oeter
Hieracium × densiflorum	Tausch
Hieracium × fallacinum	F. Schultz
Hieracium auriculoides	Láng
Hieracium × euchaetium	N. et P.
Hieracium cymosum	L.
Hieracium × sciadophorum	N. et P.
Hieracium × zizianum	Tausch
Hieracium piloselloides	Vill.
Hieracium × florentinoides	A.-T.
Hieracium × sulphureum	Döll
Hieracium × arvicola	N. et P.
Hieracium × calodon	Tausch
Hieracium echioides	Lumm.
Hieracium × fallax	Willd.
Hieracium staticifolium	All.
Hieracium bupleuroides subsp. tatrae	Gmel.
Hieracium pallidum	Biv.
Hieracium × saxifraga	Fr.
Hieracium kossuthianum	Soó
Hieracium × glaucinum	Jord.
Hieracium × wiesbaurianum	Uechtr.
Hieracium sylvaticum	(L.) Grufbg.
Hieracium × praecurrens	Vukot.
Hieracium × diaphanoides	Lindb.
Hieracium lachenalii	C.C. Gmel.
Hieracium maculatum	Schrank
Hieracium bifidum	Kit.
Hieracium caesium	(Fr.) Fr.
Hieracium × laevicaule	Jord.
Hieracium × ramosum	W. et K.
Hieracium laevigatum	Willd.
Hieracium umbellatum	L.
Hieracium × latifolium	Spr.
Hieracium × laurinum	A.-T.
Hieracium sabaudum	L.
Hieracium racemosum	W. et K.
Loranthus europaeus	Jacq.
Viscum album	L.
Viscum album subsp. abietis	L.
Viscum album subsp. austriacum	L.
Thesium bavarum	Schrank
Thesium linophyllon	L.
Thesium arvense	Horvátovszky
Thesium dollineri	Murb.
Thesium dollineri subsp. simplex	Murb.
Phytolacca americana	L.
Oxybaphus nyctagineus	(Michx.) Sweet
Montia fontana subsp. minor	L.
Portulaca oleracea	L.
Agrostemma githago	L.
Viscaria vulgaris	Bernh.
Lychnis coronaria	(L.) Desr.
Lychnis flos-cuculi	L.
Melandrium noctiflorum	Fr.
Melandrium viscosum	(L.) Celak.
Melandrium album	(Mill.) Garcke
Dianthus serotinus	W. et K.
Dianthus superbus	L.
Dianthus deltoides	L.
Dianthus armeria	L.
Dianthus armeria subsp. armeriastrum	L.
Dianthus diutinus	Kit.
Dianthus giganteiformis	Borb.
Dianthus pontederae	Kern.
Dianthus carthusianorum	L.
Dianthus carthusianorum subsp. latifolius	L.
Dianthus carthusianorum subsp. saxigenus	L.
Dianthus × hellwigii	Asch.
Saponaria officinalis	L.
Stellaria nemorum	L.
Stellaria media	(L.) Cyr.
Stellaria media subsp. neglecta	(L.) Cyr.
Stellaria media subsp. pallida	(L.) Cyr.
Stellaria holostea	L.
Stellaria uliginosa	Murr.
Stellaria graminea	L.
Stellaria palustris	Ehrh.
Myosoton aquaticum	(L.) Mönch
Cerastium dubium	(Bast.) Guépin
Cerastium sylvaticum	W. et K.
Cerastium glomeratum	Thuill.
Cerastium brachypetalum	Desp.
Cerastium brachypetalum subsp. tauricum	Desp.
Cerastium brachypetalum subsp. tenoreanum	Desp.
Cerastium subtetrandrum	(Lange) Murb.
Cerastium semidecandrum	L.
Cerastium pumilum	Curt.
Cerastium pumilum subsp. pallens	Curt.
Cerastium fontanum	Baumg.
Arenaria leptoclados	(Rchb.) Guss.
Moehringia trinervia	(L.) Clairv.
Moehringia muscosa	L.
Spergula arvensis	L.
Spergula pentandra	L.
Spergularia maritima	(All.) Chiov.
Spergularia marina	(L.) Gris.
Spergularia rubra	(L.) J. et C. Presl
Scleranthus perennis	L.
Scleranthus polycarpos	L.
Scleranthus verticillatus	Tausch
Scleranthus annuus	L.
Paronychia cephalotes	(M.B.) Bess.
Herniaria glabra	L.
Herniaria hirsuta	L.
Herniaria incana	Lam.
Polycnemum heuffelii	Láng
Polycnemum verrucosum	Láng
Polycnemum arvense	L.
Polycnemum majus	A. Br.
Beta trigyna	W. et K.
Chenopodium aristatum	L.
Chenopodium ambrosioides	L.
Chenopodium botrys	L.
Chenopodium schraderanum	Schult.
Chenopodium bonus-henricus	L.
Chenopodium polyspermum	L.
Chenopodium vulvaria	L.
Chenopodium hybridum	L.
Chenopodium murale	L.
Chenopodium urbicum	L.
Chenopodium rubrum	L.
Chenopodium botryoides	Sm.
Chenopodium glaucum	L.
Chenopodium ficifolium	Sm.
Chenopodium opulifolium	Schrad.
Chenopodium strictum	Roth
Chenopodium album	L.
Chenopodium suecicum	J. Murr
Chenopodium giganteum	D. Don
Chenopodium foliosum	(Mönch) Asch.
Chenopodium pumilio	R. Br.
Atriplex hortensis	L.
Atriplex acuminata	W. et K.
Atriplex littoralis	L.
Atriplex oblongifolia	W. et K.
Atriplex patula	L.
Atriplex prostrata	Boucher em. Rauschert
Atriplex tatarica	L.
Atriplex rosea	L.
Ceratoides latens	(J.F. Gmel.) Reveal et Holmgren
Camphorosma annua	Pall.
Bassia sedoides	(Pall.) Asch.
Kochia scoparia	Schrad.
Kochia prostrata	(L.) Schrad.
Kochia laniflora	(S.G. Gmel.) Borb.
Corispermum nitidum	Kit.
Corispermum canescens	Kit.
Salicornia prostrata	Pall.
Salicornia prostrata subsp. simonkaiana	Pall.
Suaeda maritima subsp. salinaria	(L.) Dum.
Suaeda prostrata subsp. prostrata	(L.) Dum.
Suaeda pannonica	Beck
Salsola kali subsp. ruthenica	L.
Salsola soda	L.
Amaranthus retroflexus	L.
Amaranthus chlorostachys	Willd.
Amaranthus bouchonii	Thell.
Amaranthus patulus	Bert.
Amaranthus paniculatus	L.
Amaranthus blitoides	S. Wats.
Amaranthus albus	L.
Amaranthus crispus	(Lespinasse et Théveneau) Terrac.
Amaranthus graecizans subsp. sylvestris	L.
Amaranthus deflexus	L.
Amaranthus lividus	L.
Amaranthus lividus subsp. ascendens	L.
Primula vulgaris	Huds.
Primula elatior	(L.) Hill
Primula veris	L.
Primula veris subsp. inflata	L.
Primula auricula subsp. hungarica	L.
Primula farinosa subsp. alpigena	L.
Primula × brevistyla	DC.
Androsace maxima	L.
Androsace elongata	L.
Hottonia palustris	L.
Samolus valerandi	L.
Lysimachia nummularia	L.
Lysimachia nemorum	L.
Lysimachia thyrsiflora	L.
Lysimachia vulgaris	L.
Lysimachia punctata	L.
Glaux maritima	L.
Anagallis arvensis	L.
Anagallis foemina	Mill.
Centunculus minimus	L.
Polygonum mite	Schrank
Polygonum minus	Huds.
Polygonum arenarium	W. et K.
Polygonum graminifolium	Wierzb.
Polygonum patulum	M.B.
Polygonum aviculare	L.
Polygonum neglectum	Bess.
Fallopia convolvulus	(L.) A. Löve
Fallopia dumetorum	(L.) Holub
Reynoutria japonica	Houttuyn
Reynoutria sachalinensis	(Fr. Schm.) Nakai
Fagopyrum esculentum	Mönch
Morus alba	L.
Morus nigra	L.
Ficus carica	L.
Humulus lupulus	L.
Humulus scandens	(Loureiro) Merrill
Cannabis sativa	L.
Cannabis sativa subsp. spontanea	L.
Urtica kioviensis	Rogowitsch
Urtica urens	L.
Urtica pilulifera	L.
Parietaria officinalis	L.
Ulmus laevis	Pall.
Ulmus minor	Mill.
Ulmus glabra	Huds. non Mill.
Celtis australis	L.
Celtis occidentalis	L.
Carpinus orientalis	Mill.
Carpinus betulus	L.
Ostrya carpinifolia	Scop.
Corylus avellana	L.
Corylus colurna	L.
Betula pendula	Roth
Betula pubescens	Ehrh.
Alnus viridis	(Chaix) DC.
Alnus glutinosa	(L.) Gärtn.
Alnus incana	(L.) Mönch
Fagus sylvatica	L.
Fagus sylvatica subsp. moesiaca	L.
Castanea sativa	Mill.
Quercus cerris	L.
Quercus farnetto	Ten.
Quercus pubescens	Willd.
Quercus virgiliana	Ten.
Quercus petraea	(Mattuschka) Lieblein
Quercus dalechampii	Ten.
Quercus polycarpa	Schur
Quercus robur	L.
Quercus robur subsp. asterotricha	L.
Ulmus pumila	L.
Maclura pomifera	(Raf.) C.K. Schneid.
Quercus robur subsp. pilosa	L.
Quercus robur subsp. slavonica	L.
Quercus rubra	L.
Juglans regia	L.
Juglans nigra	L.
Populus tremula	L.
Populus alba	L.
Populus nigra	L.
Populus × canescens	(Ait.) Sm.
Populus simonii	Carr.
Populus deltoides	Marsh.
Populus × canadensis	Mönch
Populus × gileadensis	Rouleau
Salix pentandra	L.
Salix fragilis	L.
Salix triandra subsp. discolor	L.
Salix alba	L.
Salix alba subsp. vitellina	L.
Salix nigricans	Sm.
Salix caprea	L.
Salix aurita	L.
Salix cinerea	L.
Salix eleagnos	Scop.
Salix viminalis	L.
Salix rosmarinifolia	L.
Salix purpurea	L.
Salix × multinervis	Döll
Alisma plantago-aquatica	L.
Alisma lanceolatum	With.
Alisma gramineum	C.C. Gmel.
Caldesia parnassifolia	(L.) Parl.
Sagittaria sagittifolia	L.
Butomus umbellatus	L.
Elodea canadensis	L.C. Rich.
Elodea densa	(Planchon) Caspary
Elodea nuttallii	(Planchon) St. John
Vallisneria spiralis	L.
Stratiotes aloides	L.
Hydrocharis morsus-ranae	L.
Triglochin maritimum	L.
Triglochin palustre	L.
Potamogeton natans	L.
Potamogeton nodosus	Poir.
Potamogeton coloratus	Vahl
Potamogeton lucens	L.
Potamogeton × zizii	Koch
Potamogeton gramineus	L.
Potamogeton perfoliatus	L.
Potamogeton crispus	L.
Potamogeton acutifolius	Link
Potamogeton obtusifolius	M. et K.
Potamogeton berchtoldii	Fieber
Potamogeton panormitanus	Bivona
Potamogeton trichoides	Cham. et Schlechtd.
Potamogeton pectinatus	L.
Potamogeton pectinatus subsp. balatonicus	L.
Potamogeton filiformis	Pers.
Groenlandia densa	(L.) Fourr.
Zannichellia palustris	L.
Zannichellia palustris subsp. polycarpa	L.
Zannichellia palustris subsp. pedicellata	L.
Najas marina	L. em. Asch.
Najas minor	All.
Veratrum nigrum	L.
Veratrum album	L.
Veratrum album subsp. lobelianum	L.
Bulbocodium versicolor	(Ker-Gawl.) Spreng.
Colchicum hungaricum	Janka
Colchicum arenarium	W. et K.
Colchicum autumnale	L.
Asphodelus albus	Vill.
Anthericum liliago	L.
Anthericum ramosum	L.
Hemerocallis lilio-asphodelus	L. em. Soó
Hemerocallis fulva	L.
Gagea villosa	(M.B.) Duby
Gagea bohemica	(Zauschner) R. et Sch.
Gagea minima	(L.) Ker-Gawl.
Gagea spathacea	(Hayne) Salisb.
Gagea pratensis	(Pers.) Dum.
Gagea lutea	(L.) Ker-Gawl.
Gagea pusilla	(F.W. Schmidt) R. et Sch.
Allium ursinum	L.
Allium victorialis	L.
Allium atropurpureum	W. et K.
Allium angulosum	L.
Allium montanum	F.W. Schmidt
Allium suaveolens	Jacq.
Allium moschatum	L.
Allium carinatum	L.
Allium oleraceum	L.
Allium paniculatum subsp. marginatum	L.
Allium flavum	L.
Allium vineale	L.
Allium sphaerocephalon	L.
Allium sphaerocephalon subsp. amethystinum	L.
Allium rotundum subsp. waldsteinii	L.
Allium scorodoprasum	L.
Allium atroviolaceum	Boiss.
Lilium martagon	L.
Lilium bulbiferum	L.
Fritillaria meleagris	L.
Erythronium dens-canis	L.
Scilla spetana	Kereszty
Scilla drunensis	Speta
Scilla kladnii	Schur
Scilla vindobonensis	Speta
Scilla autumnalis	L.
Scilla siberica	Haw.
Ornithogalum boucheanum	(Kunth) Asch.
Ornithogalum degenianum	Polgár
Ornithogalum pyramidale	L.
Ornithogalum sphaerocarpum	Kern.
Ornithogalum comosum	L.
Ornithogalum orthophyllum	Ten.
Ornithogalum umbellatum	L.
Ornithogalum refractum	Kit.
Muscari comosum	(L.) Mill.
Muscari tenuiflorum	Tausch
Muscari racemosum	(L.) Mill.
Muscari botryoides	(L.) Mill.
Muscari botryoides subsp. transsilvanicum	(L.) Mill.
Crocus albiflorus	Kit.
Muscari botryoides subsp. kerneri	(L.) Mill.
Asparagus officinalis	L.
Ruscus hypoglossum	L.
Ruscus aculeatus	L.
Majanthemum bifolium	L.
Polygonatum verticillatum	(L.) All.
Polygonatum latifolium	(Jacq.) Desf.
Polygonatum odoratum	(Mill.) Druce
Polygonatum multiflorum	(L.) All.
Convallaria majalis	L.
Paris quadrifolia	L.
Tofieldia calyculata	(L.) Wahlenb.
Galanthus nivalis	L.
Leucojum vernum	L.
Leucojum aestivum	L.
Sternbergia colchiciflora	W. et K.
Narcissus angustifolius	Curt.
Narcissus pseudonarcissus	L.
Narcissus poëticus	L.
Tamus communis	L.
Crocus reticulatus	Stev.
Crocus heuffelianus	Herbert
Crocus tommasinianus	Herbert
Gladiolus palustris	Gaud.
Gladiolus imbricatus	L.
Iris pumila	L.
Iris arenaria	W. et K.
Iris aphylla subsp. hungarica	L.
Iris variegata	L.
Iris germanica	L.
Iris pseudacorus	L.
Iris spuria	L.
Iris sibirica	L.
Iris graminea	L.
Iris graminea subsp. pseudocyperus	L.
Juncus bufonius	L.
Juncus bufonius subsp. nastanthus	L.
Juncus sphaerocarpus	Nees
Juncus tenageia	Ehrh.
Juncus compressus	Jacq.
Juncus gerardii	Lois.
Juncus tenuis	Willd.
Juncus inflexus	L.
Juncus conglomeratus	L.
Juncus effusus	L.
Juncus capitatus	Weigel
Juncus maritimus	Lam.
Juncus bulbosus	L.
Juncus subnodulosus	Schrank
Juncus atratus	Krocker
Juncus alpinus	Vill.
Juncus articulatus	L.
Luzula forsteri	(Sm.) DC.
Luzula pilosa	(L.) Willd.
Luzula luzuloides	(Lam.) Dandy
Luzula campestris	(L.) Lam. et DC.
Luzula multiflora	(Retz.) Lej.
Luzula pallescens	(Wahlbg.) Sw.
Cypripedium calceolus	L.
Cephalanthera rubra	(L.) Rich.
Cephalanthera damasonium	(Mill.) Druce
Cephalanthera × schulzei	Cam. et Ber.
Cephalanthera longifolia	(Huds.) Fritsch
Epipactis palustris	(Mill.) Cr.
Carex caespitosa	L.
Epipactis atrorubens	(Hoffm.) Bess.
Epipactis atrorubens subsp. borbasii	(Hoffm.) Bess.
Epipactis × graberi	Cam.
Epipactis microphylla	(Ehrh.) Sw.
Epipactis purpurata	Sm.
Epipactis purpurata var. rosea	Sm.
Epipactis helleborine	(L.) Cr.
Epipactis pontica	Taubenheim
Epipactis gracilis	B. et H. Baumann
Epipactis muelleri	Godfr.
Epipactis leptochila	(Godfr.) Godfr.
Epipactis leptochila subsp. neglecta	(Godfr.) Godfr.
Epipactis bugacensis	Robatsch
Epipactis albensis	Novakova et Rydlo
Epipactis nordeniorum	Robatsch
Epipactis tallosii	Molnar et Robatsch
Epipactis helleborine agg.	(L.) Cr. s. str.
Limodorum abortivum	(L.) Sw.
Carex elata	All.
Listera ovata	(L.) R. Br.
Neottia nidus-avis	(L.) Rich.
Spiranthes spiralis	(L.) Chevall.
Spiranthes aestivalis	(Poir.) Rich.
Goodyera repens	(L.) R. Br.
Epipogium aphyllum	(F.W. Schmidt) Sw.
Herminium monorchis	(L.) R. Br.
Coeloglossum viride	(L.) Hartm.
Platanthera bifolia	(L.) Rich.
Platanthera chlorantha	(Custer) Rchb. ex Mössler
Platanthera × hybrida	Bruegg.
Gymnadenia conopsea	(L.) R. Br.
Gymnadenia conopsea subsp. densiflora	(L.) R. Br.
Dactylorhiza incarnata subsp. haematodes	(L.) Soó
Gymnadenia odoratissima	(L.) Rich.
Ophrys insectifera	L.
Ophrys sphecodes	Mill.
Ophrys scolopax subsp. cornuta	Cav.
Ophrys × nelsonii	Cont. et Del.
Ophrys fuciflora	(F.W. Schmidt) Mönch
Ophrys holubyana	Andrasovszky
Ophrys apifera	Huds.
Orchis morio	L.
Orchis coriophora	L.
Orchis × timbalii	Velen.
X Orchidactyla Drudei	(Schulze) Borsos et Soó
Orchis ustulata	L.
Orchis ustulata subsp. aestivalis	L.
Orchis tridentata	Scop.
Orchis simia	Lam.
Orchis militaris	L.
Orchis purpurea	Huds.
Orchis × hybrida	Boen. ex Rchb.
Orchis mascula subsp. signifera	L.
Orchis pallens	L.
Orchis laxiflora subsp. palustris	Lam.
Orchis laxiflora subsp. elegans	Lam.
Traunsteinera globosa	(L.) Rchb.
Dactylorhiza sambucina	(L.) Soó
Dactylorhiza incarnata	(L.) Soó
Dactylorhiza incarnata var. hyphaematodes	(L.) Soó
Dactylorhiza incarnata subsp. serotina	(L.) Soó
Dactylorhiza majalis	(Rchb.) Hunt et Summerhayes
Dactylorhiza × aschersoniana	(Hausskn.) Borsos et Soó
Dactylorhiza maculata	(L.) Soó
Dactylorhiza fuchsii	(Druce) Soó
Dactylorhiza fuchsii subsp. sooiana	(Druce) Soó
Orchis × angusticruris	Fr.
Dactylorhiza maculata subsp. transsylvanica	(L.) Soó
Anacamptis pyramidalis	(L.) Rchb.
Himantoglossum caprinum	(M.B.) Spr.
Himantoglossum adriaticum	Baumann
Liparis loeselii	(L.) Rich.
Corallorhiza trifida	Chatelain
Malaxis monophyllos	(L.) Sw.
Hammarbya paludosa	(L.) O. Kuntze
Scirpus sylvaticus	L.
Scirpus radicans	Schkuhr
Bolboschoenus maritimus	(L.) Palla
Blysmus compressus	(L.) Panzer
Holoschoenus romanus subsp. holoschoenus	(L.) Fritsch em. Becherer
Schoenoplectus lacustris	(L.) Palla
Schoenoplectus tabernaemontani	(C.C. Gmel.) Palla
Schoenoplectus litoralis	(Schrad.) Palla
Schoenoplectus triqueter	(L.) Palla
Schoenoplectus americanus subsp. triangularis	(Pers.) Volkart
Schoenoplectus mucronatus	(L.) Palla
Schoenoplectus supinus	(L.) Palla
Schoenoplectus setaceus	(L.) Palla
Eleocharis acicularis	(L.) R. et Sch.
Eleocharis quinqueflora	(F.X. Hartmann) O. Schwarz
Eleocharis ovata	(Roth) R. et Sch.
Eleocharis carniolica	Koch
Eleocharis palustris	(L.) R. et Sch.
Eleocharis mamillata	Lindb. f.
Cynosurus echinatus	L.
Eleocharis austriaca	Hay.
Eleocharis uniglumis	(Link) Schult.
Trichophorum alpinum	(L.) Pers.
Trichophorum caespitosum	(L.) Hartm.
Eriophorum vaginatum	L.
Eriophorum gracile	Koch
Eriophorum latifolium	Hoppe
Eriophorum angustifolium	Honckeny
Cyperus fuscus	L.
Cyperus difformis	L.
Chlorocyperus glomeratus	(L.) Palla
Chlorocyperus longus	(L.) Palla
Chlorocyperus glaber	(L.) Palla
Dichostylis micheliana	(L.) Nees
Acorellus pannonicus	(Jacq.) Palla
Pycreus flavescens	(L.) Rchb.
Schoenus nigricans	L.
Schoenus ferrugineus	L.
Cladium mariscus	(L.) Pohl
Cladium mariscus subsp. martii	(L.) Pohl
Rhynchospora alba	(L.) Vahl
Carex davalliana	Sm.
Carex bohemica	Schreb.
Carex stenophylla	Wahlbg.
Carex divisa	Huds.
Carex diandra	Schrank
Carex appropinquata	Schumacher
Carex paniculata	Jusl.
Carex vulpina	L.
Carex cuprina	(Sándor) Nendtvich
Carex spicata	Huds.
Carex pairae	F. Schultz
Carex pairae subsp. leersiana	F. Schultz
Carex divulsa	Stokes
Carex praecox	Schreb.
Carex brizoides	L.
Carex elongata	L.
Carex disticha	Huds.
Carex repens	Bell.
Carex leporina	L.
Carex canescens	L.
Carex echinata	Murr.
Carex remota	L.
Carex nigra	(L.) Reichard
Carex gracilis	Curt.
Carex gracilis subsp. intermedia	Curt.
Carex buekii	Wimm.
Carex hartmannii	Cajander
Carex buxbaumii	Wahlenb.
Carex limosa	L.
Carex flacca	Schreb.
Carex pendula	Huds.
Carex pallescens	L.
Carex tomentosa	L.
Carex ericetorum	Poll.
Carex montana	L.
Carex pilulifera	L.
Carex fritschii	Waisb.
Carex supina	Wahlbg.
Carex liparicarpos	Gaud.
Carex caryophyllea	Latour.
Carex umbrosa	Host
Carex hallerana	Asso
Carex humilis	Leyss.
Carex digitata	L.
Carex alba	Scop.
Carex panicea	L.
Carex sylvatica	Huds.
Carex strigosa	Huds.
Carex brevicollis	DC.
Carex michelii	Host
Carex pilosa	Scop.
Carex distans	L.
Carex hostiana	DC.
Carex flava	L.
Carex lepidocarpa	Tausch
Carex serotina	Mérat
Carex hordeistichos	Vill.
Carex secalina	Wahlbg.
Carex pseudocyperus	L.
Carex rostrata	Stokes
Carex vesicaria	L.
Carex acutiformis	Ehrh.
Carex riparia	Curt.
Carex melanostachya	Willd.
Carex lasiocarpa	Ehrh.
Carex hirta	L.
Commelina communis	L.
Bromus ramosus	Huds.
Bromus benekenii	(Lange) Trimen
Bromus erectus	Huds.
Bromus pannonicus	Kummer et Sendtner
Bromus inermis	Leyss.
Bromus rigidus	Roth
Bromus sterilis	L.
Bromus tectorum	L.
Bromus arvensis	L.
Bromus racemosus	L.
Bromus commutatus	Schrad.
Bromus mollis	L.
Bromus lepidus	Holmberg
Bromus squarrosus	L.
Bromus japonicus	Thunb.
Bromus secalinus	L.
Bromus brachystachys	Hornung
Bromus madritensis	L.
Bromus willdenowii	Kunth
Brachypodium pinnatum	(L.) P.B.
Brachypodium pinnatum subsp. rupestre	(L.) P.B.
Brachypodium sylvaticum	(Huds.) R. et Sch.
Festuca amethystina	L.
Festuca ovina	L.
Festuca tenuifolia	Sibth.
Festuca pallens	Host
Festuca pallens subsp. pannonica	Host
Festuca vaginata	W. et K.
Festuca vaginata subsp. dominii	W. et K.
Festuca × wagneri	Deg., Thaisz et Flatt em. Soó
Festuca dalmatica	(Hack.) K. Richt.
Festuca pseudodalmatica	Kraj.
Festuca valesiaca	Schleich.
Festuca rupicola	Heuff.
Festuca pseudovina	Hack.
Festuca heterophylla	Lam.
Festuca rubra	L.
Festuca nigrescens	Lam.
Festuca gigantea	(L.) Vill.
Festuca arundinacea	Schreb.
Festuca arundinacea subsp. uechtritziana	Schreb.
Festuca pratensis	Huds.
Festuca altissima	All.
Festuca drymeia	M. et K.
Vulpia myuros	(L.) C.C. Gmel.
Vulpia bromoides	(L.) S.F. Gray
Glyceria maxima	(Hartm.) Holmberg
Glyceria fluitans	(L.) R. Br.
Glyceria plicata	Fr.
Glyceria declinata	Bréb.
Glyceria nemoralis	Uechtr. et Koern.
Melica ciliata	L.
Glyceria × pedicellata	Towns.
Puccinellia distans	(Jacq.) Parl.
Puccinellia limosa	(Schur) Holmberg
Puccinellia peisonis	(Beck) Jáv.
Puccinellia pannonica	(Hack.) Holmberg
Sclerochloa dura	(L.) P.B.
Poa remota	Forselles
Poa pratensis	L.
Poa angustifolia	L.
Poa subcoerulea	Sm.
Poa trivialis	L.
Poa nemoralis	L.
Poa palustris	L.
Poa pannonica subsp. scabra	Kern.
Poa compressa	L.
Poa badensis	Haenke
Poa bulbosa	L.
Poa annua	L.
Poa supina	Schrad.
Briza media	L.
Catabrosa aquatica	(L.) P.B.
Dactylis glomerata	L.
Dactylis polygama	Horvátovszky
Cynosurus cristatus	L.
Melica transsilvanica	Schur
Melica altissima	L.
Melica uniflora	Retz.
Melica nutans	L.
Melica picta	C. Koch
Sesleria heuflerana	Schurs.
Sesleria hungarica	(Ujhelyi) Deyl
Sesleria sadlerana	Janka
Sesleria varia	(Jacq.) Wettst.
Sesleria uliginosa	Opiz
Lolium perenne	L.
Lolium multiflorum	Lam.
Lolium temulentum	L.
Lolium remotum	Schrank
Molinia caerulea	Mönch
Molinia caerulea subsp. hungarica	Mönch
Molinia caerulea subsp. horanszkyi	Mönch
Molinia caerulea subsp. simonii	Mönch
Molinia arundinacea	(Schrank) Domin
Molinia arundinacea subsp. ujhelyii	(Schrank) Domin
Molinia arundinacea subsp. pocsii	(Schrank) Domin
Cephalozia lacinulata	Jack ex Spruce
Agropyron pectinatum	(M.B.) R. et Sch.
Agropyron caninum	(L.) P.B.
Agropyron repens	(L.) P.B.
Agropyron intermedium	Host
Agropyron intermedium subsp. trichophorum	Host
Haynaldia villosa	(L.) Schur
Aegilops cylindrica	Host
Secale sylvestre	Host
Hordeum murinum	L.
Hordeum murinum subsp. leporinum	L.
Hordeum hystrix	Roth
Hordeum marinum	Huds.
Hordelymus europaeus	(L.) Jessen
Taeniatherum asperum	(Simk.) Nevski
Phragmites australis	(Cav.) Trin.
Beckmannia eruciformis	(L.) Host
Pholiurus pannonicus	(Host) Trin.
Nardus stricta	L.
Aira caryophyllea	L.
Aira elegantissima	Schur
Deschampsia flexuosa	(L.) Trin.
Phleum paniculatum	Huds.
Deschampsia caespitosa	(L.) P.B.
Deschampsia caespitosa subsp. parviflora	(L.) P.B.
Holcus mollis	L.
Holcus lanatus	L.
Arrhenatherum elatius	(L.) J. et C. Presl
Trisetum flavescens	(L.) P.B.
Ventenata dubia	(Leers) Coss.
Avena fatua	L.
Avena sativa	L.
Avena nuda	Höjer
Avena sterilis subsp. ludoviciana	L.
Avena strigosa	Schreb.
Avena barbata	Brot.
Helictotrichon pubescens	(Huds.) Pilger
Helictotrichon compressum	(Heuff.) Henrard
Helictotrichon pratense	(L.) Bess.
Helictotrichon praeustum	(Rchb.) Tzvelev
Gaudinia fragilis	(L.) P.B.
Danthonia alpina	Vest.
Sieglingia decumbens	(L.) Lam. et DC.
Corynephorus canescens	(L.) P.B.
Koeleria glauca	(Schkuhr) DC.
Koeleria glauca subsp. rochelii	(Schkuhr) DC.
Koeleria pyramidata	(Lam.) P.B.
Koeleria grandis	Bess.
Koeleria majoriflora	(Borb.) Borb.
Koeleria javorkae	Ujhelyi
Koeleria cristata	(L.) Pers.
Apera spica-venti	(L.) P.B.
Apera interrupta	(L.) P.B.
Agrostis canina	L.
Agrostis vinealis	Salisb.
Agrostis capillaris	L.
Agrostis stolonifera	L.
Calamagrostis arundinacea	(L.) Roth
Calamagrostis varia	(Schrad.) Host
Calamagrostis stricta	(Timm. ) Koeler
Calamagrostis canescens	(Weber) Roth
Calamagrostis pseudophragmites	(Hall.f.) Koel.
Calamagrostis epigeios	(L.) Roth
Calamagrostis purpurea	(Trin.) Trin.
Phleum pratense	L.
Phleum hubbardii	D. Kováts
Phleum phleoides	(L.) Karsten
Alopecurus pratensis	L.
Alopecurus myosuroides	Huds.
Alopecurus geniculatus	L.
Alopecurus aequalis	Sobol.
Stipa bromoides	(L.) Dörfler
Stipa capillata	L.
Stipa tirsa	Stev.
Stipa dasyphylla	(Czern.) Trautv.
Stipa borysthenica	Klokov
Stipa joannis	Celak.
Stipa joannis subsp. puberula	Celak.
Stipa eriocaulis	Borb.
Stipa pulcherrima	C. Koch
Piptatherum virescens	(Trin.) Boiss.
Piptatherum miliaceum	(L.) Cosson
Milium effusum	L.
Hierochloe repens	(Host) P.B.
Hierochloe australis	(Schrad.) R. et Sch.
Anthoxanthum odoratum	L.
Phalaris canariensis	L.
Phalaroides arundinacea	(L.) Rauschert
Eragrostis pilosa	(L.) P.B.
Eragrostis minor	Host
Eragrostis megastachya	(Koeler) Link
Eragrostis parviflora	(R.Br.) Trin
Cleistogenes serotina	(L.) Keng
Cynodon dactylon	(L.) Pers.
Eleusine indica	(L.) Gärtn.
Crypsis aculeata	(L.) Ait.
Heleochloa alopecuroides	(Pill. et Mitterp.) Host
Heleochloa schoenoides	(L.) Host
Tragus racemosus	(L.) Desf.
Leersia oryzoides	(L.) Sw.
Cenchrus incertus	M.A. Curtis
Panicum capillare	L.
Panicum miliaceum	L.
Panicum miliaceum subsp. ruderale	L.
Panicum philadelphicum	Bernh.
Digitaria ischaemum	(Schreb.) Mühlenb.
Digitaria sanguinalis	(L.) Scop.
Digitaria ciliaris	(Retz.) Koel.
Echinochloa crus-galli	(L.) P.B.
Echinochloa occidentalis	(Wiegand) Rydb.
Echinochloa phyllopogon	(Stapf) Carvalho-Vasconcellos
Echinochloa oryzoides	(Ard.) Fritsch
Echinochloa eruciformis	(Sibth. et Sm.) Rchb.
Setaria verticillata	(L.) P.B.
Setaria × decipiens	Schimper
Setaria pumila	(Poir.) R. et Sch.
Setaria viridis	L.
Setaria italica	(L.) P.B.
Bothriochloa ischaemum	(L.) Keng
Chrysopogon gryllus	(L.) Trin.
Sorghum halepense	(L.) Pers.
Sorghum sudanense	(Piper) Stapf
Sorghum bicolor	(L.) Mönch
Acorus calamus	L.
Arum maculatum	L.
Arum orientale subsp. bessarabicum	M.B.
Calla palustris	L.
Lemna trisulca	L.
Lemna minor	L.
Lemna gibba	L.
Spirodela polyrhiza	(L.) Schleiden
Wolffia arrhiza	(L.) Horkel
Sparganium minimum	Wallr.
Sparganium emersum	Rehmann
Sparganium erectum	L.
Sparganium erectum subsp. microcarpum	L.
Sparganium erectum subsp. neglectum	L.
Typha minima	Funck
Typha laxmannii	Lepechin
Typha angustifolia	L.
Typha latifolia	L.
Typha shuttleworthii	Koch et Sonder ex Koch
Huperzia selago	(L.) Bernh. ex Schrank et Martius
Lycopodium annotinum	L.
Lycopodium clavatum	L.
Diphasium complanatum	(L.) Rothm.
Diphasium tristachyum	(Pursh) Rothm.
Diphasium issleri	(Rouy) Holub
Selaginella helvetica	(L.) Link
Equisetum telmateia	Ehrh.
Equisetum arvense	L.
Equisetum sylvaticum	L.
Equisetum fluviatile	L. em. Ehrh.
Equisetum palustre	L.
Equisetum hyemale	L.
Equisetum × moorei	Newman
Equisetum ramosissimum	Desf.
Equisetum variegatum	Schleicher in Weber et Mohr
Botrychium lunaria	(L.) Sw. in Schrad.
Botrychium matricariifolium	(Retz.) A. Br. ex Döll
Botrychium multifidum	(S.G. Gmel.) Rupr.
Botrychium virginianum subsp. europaeum	(L.) Sw.
Ophioglossum vulgatum	L.
Osmunda regalis	L.
Pteridium aquilinum	(L.) Kuhn ex Decken
Cheilanthes marantae	(L.) Dom.
Polypodium vulgare	L.
Polypodium interjectum	Shivas
Phyllitis scolopendrium	(L.) Newman
Asplenium adiantum-nigrum	L.
Asplenium ruta-muraria	L.
Asplenium lepidum	C. Presl
Asplenium septentrionale	(L.) Hoffm.
Asplenium fontanum	(L.) Bernh. ex Schrad.
Asplenium trichomanes	L.
Asplenium trichomanes subsp. quadrivalens	L.
Asplenium viride	Huds.
Ceterach officinarum	DC. in Lam. et DC.
Ceterach javorkaeanum	(Vida) Soó
Oreopteris limbosperma	(All.) Holub
Thelypteris palustris	Schott
Gymnocarpium dryopteris	(L.) Newman
Gymnocarpium robertianum	(Hoffm.) Newman
Phegopteris connectilis	(Michx.) Watt
Athyrium filix-femina	(L.) Roth
Matteuccia struthiopteris	(L.) Tod.
Cystopteris fragilis	(L.) Bernh. ex Schrad.
Woodsia ilvensis	(L.) R. Br.
Woodsia alpina	(Bolton) S.F. Gray
Polystichum lonchitis	(L.) Roth
Polystichum aculeatum	(L.) Roth
Polystichum braunii	(Spenner) Fée
Polystichum setiferum	(Forskal) Moore ex Woynar
Jungermannia subulata	Evans
Dryopteris cristata	(L.) A. Gray
Dryopteris filix-mas	(L.) Schott
Dryopteris pseudo-mas	(Wollaston) Holub et Pouzar
Dryopteris carthusiana	(Vill.) H.P. Fuchs
Dryopteris dilatata	(Hoffm.) A. Gray
Dryopteris expansa	(C.B. Presl) Fraser-Jenkins
Dryopteris × tavelii	Rothm.
Blechnum spicant	(L.) Roth
Marsilea quadrifolia	L.
Salvinia natans	(L.) All.
Azolla caroliniana	Willd.
Taxus baccata	L.
Abies alba	Mill.
Picea abies	(L.) Karst.
Larix decidua	Mill.
Pinus sylvestris	L.
Pinus nigra	Arnold
Juniperus communis	L.
Platycladus orientalis	(L.) Franco
Ephedra distachya	L.
Azolla filiculoides	Lam.
Pyrus pyraster subsp. anchras	Burgsd.
Crataegus curvisepala	Lindm.
Lotus corniculatus var. villosus	L.
Vicia grandiflora subsp. sordida	Scop.
Vicia grandiflora subsp. biebersteinii	Scop.
Acer acuminatilobum	J. Papp
Lindernia dubia	(L.) Pennell
Asarum europaeum subsp. caucasicum	L.
Myriophyllum brasiliense	Camb.
Chamaecytisus virescens	(Neilr.) Dostál
Polygonum arenastrum	Boreau
Scilla drunensis subsp. buekkensis	Speta
Scilla paratheticum	(Speta) Valdés
Epipactis placentina	Bongiorni et Grünanger
Cyperus esculentus	L.
Carex curvata	Knaf
Agropyron elongatum	(Host) P.B.
Sphagnum palustre	L.
Mannia triandra	(Scop.) Grolle
Riccia frostii	Aust.
Riccia huebeneriana	Lindenb.
Lophozia ascendens	(Warnst.) Schust.
Frullania inflata	Gott.
Sphagnum compactum	DC.
Sphagnum subsecundum	Nees
Sphagnum contortum	Schultz
Sphagnum platyphyllum	(Lindb.) Warnst.
Sphagnum obtusum	Warnst.
Sphagnum recurvum	P. Beauv.
Sphagnum squarrosum	Crome
Sphagnum teres	(Schimp.) Ångstr.
Sphagnum quinquefarium	(Lindb.) Warnst.
Sphagnum fimbriatum	Wilson
Sphagnum girgensohnii	Russ.
Sphagnum russowii	Warnst.
Sphagnum subnitens	Russ. et Warnst.
Sphagnum centrale	C. Jens
Sphagnum magellanicum	Brid.
Sphagnum auriculatum	Schimp.
Sphagnum capillifolium	(Ehrh.) Hedw.
Sphagnum cuspidatum	Ehrh. ex Hoffm.
Sphagnum warnstorfi	Russ.
Buxbaumia viridis	(DC.) Moug. et Nestl.
Dicranella humilis	Ruthe
Dicranum viride	(Sull. et Lesq.) Lindb.
Fissidens arnoldii	Ruthe
Fissidens exiguus	Sull.
Fissidens algarvicus	Solms.
Weissia rostellata	(Bird.) Lindb.
Phascum floerkeanum	Web. et Mohr.
Aloina bifrons	(De Not.) Delg
Pterygoneurum lamellatum	(Lindb.) Jur.
Tortula brevissima	Schiff.
Grimmia plagiopodia	Hedw.
Grimmia sessitana	De Not.
Grimmia teretinervis	Limpr.
Ephemerum cohaerens	(Hedw.) Hampe
Ephemerum recurvifolium	(Dicks.) Boul
Physcomitrium sphaericum	Bird.
Bryum neodamense	Itzigs
Bryum warneum	(Roehl.) Bird.
Bryum stirtonii	Schimp.
Bryum versicolor	B. et S.
Meesia triquetra	(Hook. et Tayl.) Ångstr.
Campylostelium saxicola	(Web. et Mohr) B. S. G.
Orthotrichum stellatum	Bird.
Orthotrichum rogeri	Bird.
Orthotrichum scanicum	Groenv.
Neckera pennata	Hedw.
Anacamptodon splachnoides	(Bird.) Bird.
Anomodon rostratus	(Hedw.) Schimp.
Calliergon stramineum	(Bird.) Kindb.
Calliergon giganteum	(Schimp.) Kindb.
Scorpidium scorpioides	(Hedw.) Limpr.
Campylium elodes	(Lindb.) Kindb.
Drepanocladus sendtneri	(Schimp.) Warnst.
Drepanocladus exannulatus	(B. S. G.) Warnst.
Drepanocladus vernicosus	(Lindb.) Warnst.
Drepanocladus revolvens	(Sw.) Warnst.
Drepanocladus lycopodioides	(Bird.) Warnst.
Amblystegium saxatile	Schimp.
Brachythecium geheebii	Milde
Brachythecium oxycladum	(Bird.) Jaeg.
Rynchostegium rotundifolium	(Scop.) B. S. G.
Rynchostegiella jacquinii	(Garov.) Limpr.
Taxiphyllum densifolium	(Broth.) Reim.
Asterella saccata	(Wahlenb.) Evans
Brachydontium trichoides	(Web.) Fuernr.
Desmatodon cernuus	(Hueb.) B. S. G.
Didymodon glaucus	Ryan
Enthostodon hungaricus	(Boros) Loeske
Pyramidula tetragona	(Bird.) Bird.
Hilpertia velenovskyi	(Schiffn.) Zander
Reynoutria × bohemica	Chrtek et Chrtkova
Myosotis sicula	Guss.
Leucobryum glaucum	(Hedw.) Angstr.
Asplenium × alternifolium	Wulf.
Crocus vittatus	Schloss. et Vuk.
Dryopteris × sarvelii	Fraser-Jenk. et Jermy
Duchesnea indica	(Andr.) Focke
Ptelea trifoliata	L.
Heracleum sosnowskyi	Manden.
Reynoutria aubertii	(Henry) Meldenke
Sorbus × semipinnata	(Roth) Hedl.
Sorbus × rotundifolia	(Bechst.) Hedl.
Sorbus × buekkensis	Soó
Anogramma leptophylla	(L.) Link
Calamagrostis villosa	(Chaix ex Vill.) J. F. Gmel.
Cardaminopsis halleri	(L.) Hay.
Carex transsylvanica	Schur
Dactylorhiza lapponica	(Laest. ex Hartm.) Soó
Dactylorhiza ochroleuca	(Wüstn. ex Boll) Holub
Epipactis voethii	Robatsch
Lilium martagon subsp. alpinum	L.
Sorbus × javorkae	(Soó) Kárp.
Sorbus × pannonica	Kárp.
Sorbus incisa	(Rchb.) Hedl.
Sorbus × subdanubialis	(Soó) Kárp.
Hypericum mutilum	L.
Dactylorhiza incarnata var. ochrantha	(L.) Soó
Ageratina altissima	(L.) King et Robinson
Orobanche pancicii	Beck
Paeonia tenuifolia	L.
Robinia hispida	L.
Aster laevis	L.
Ceratocephalus falcata	(L.) Pers.
Bolboschoenus glaucus	(Lam.) S. G. Sm.
Bolboschoenus laticarpus	Hroud., Zákr. et Duch.
Bolboschoenus planiculmis	(F. Schmidt) T. V. Egorova
Orobanche bartlingii	Gris.
Berberis julianae	C. K. Schneid.
Epipactis futakii	Mereďa et Potůček
Epipactis helleborine subsp. minor	(L.) Cr.
Epipactis helleborine subsp. orbicularis	(L.) Cr.
Epipactis latina	(W. Rossi et E. Klein) B. Baumann et H. Baumann
Parietaria diffusa	Mert. et W.D.J. Koch
Corispermum marschallii	Steven
Corispermum leptopterum	(Asch.) Iljin
Salsola collina	Pall.
Montia linearis	(Douglas) Greene
Soldanella hungarica	Simk.
Sorbus × vrabelyiana	nom. prov.
Agropyron elongatum cv. szarvasi-1	(Host) P.B.
Sorbus × pseudograeca	nom. prov.
Spiraea × van-houttei	Zab.
× Asplenoceterach badense	D. E. Meyer
Paulownia tomentosa	(Thunb.) Steud.
Euphorbia lathyris	L.
Asplenium × murbeckii	Dörfl.
Rubus armeniacus	Focke
Brunnera macrophylla	(Adm.) Johnston
Sorbus carpinifolia	nom. prov.
Petunia × atkinsiana	D. Don
Koelreuteria paniculata	Laxm.
Pyracantha coccinea	M. Roem.
Carduus × budaianus	Jáv.
Carduus × littoralis	Borb.
Cirsium × candolleanum	Nägeli
Silene donetzica	Kleopow
Sporobolus cryptandrus	A.Gray
Viola sororia	Willd.
Silphium perfoliatum	L.
Yucca filamentosa	L.
Catalpa bignonioides	Walt.
Campsis radicans	(L.) Seem.
Arundo donax	L.
Gaillardia pulchella	Foug.
Lycopersicon esculentum	Mill.
Datura wrightii	Regel
Persica vulgaris	Mill.
Cornus alba	L.
Amaranthus cruentus	L.
Gypsophila perfoliata	L.
Rubus praecox	Bertol.
Broussonetia papyrifera	(L.) Vent.
Heliopsis helianthoides	(L.) Sweet
Cardamine occulta	Hornem.
Euphorbia prostrata	Aiton
Platanus × hybrida	Brot.
Thuja orientalis	L.
Gaillardia aristata	Pursh
Euphorbia myrsinites	L.
Chamaecyparis lawsoniana	(A. Murray) Parl.
\.


--
-- Name: ssp_auctor ssp_auctor_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_auctor
    ADD CONSTRAINT ssp_auctor_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_auctor ssp_auctor_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_auctor
    ADD CONSTRAINT ssp_auctor_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

