# SuperSpecies

Superspecies

What is this? 
Scientific species names and additional information in a "modular" format for easy data management.

It is automatically checking input data sources and create a new database and an export after each update of source files

Source files (_atomic tables_) always have two columns:
Scientific_name additional_information

These simple tables merged into a big table in the database creating process and in this process and before this process all tables and connections checked automatically to ensure the final output is valid and correct.

## Get SuperSpecies output
https://superspecies.gitlab.io/superspecies/


## Build the development environment for major updates, e.g. adding new rules
At first time run these commands to get database up and running:

```
mkdir ~/.dbt && cat ./profiles.yml >> ~/.dbt/profiles.yml
docker-compose up -d
dbt deps
dbt seed
dbt compile
dbt run
```

Without setting dbt profiles
```
docker-compose up -d
dbt deps --profiles-dir ./
dbt seed --profiles-dir ./
dbt compile --profiles-dir ./
dbt run --profiles-dir ./
```

Than open http://localhost:36433 in browser (look up login credentials in profiles.yml)

### Try running the following commands:
- dbt seed
- dbt run
- dbt test


## UPDATE Datasets
Visit the https://gitlab.com/SuperSpecies/ssp_clients/ for client applications
