FROM  mdillon/postgis:11 AS postgres11



FROM postgres11 AS superspecies

COPY ./create_superspecies_db.sql /docker-entrypoint-initdb.d/00-CreateDatabase.sql
#COPY ./SQL/01_ssp_speciesnames.sql /docker-entrypoint-initdb.d/01_ssp_speciesnames.sql
#COPY ./SQL/ssp_*.sql /docker-entrypoint-initdb.d/
#COPY ./OUTPUT/output_process.sql /docker-entrypoint-initdb.d/ZZ-OutputProcess.sql

FROM superspecies AS superspecies-test

ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD changeMe

RUN sed -i '$ d' /usr/local/bin/docker-entrypoint.sh
